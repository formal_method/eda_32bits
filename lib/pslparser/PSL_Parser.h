// -*- C++ -*-
//! \author Patrick Pastoor, Raik Brinkmann
//! \copyright (c) Infineon Technologies AG, 2005

#ifndef __PSL_PARSER_H__
#define __PSL_PARSER_H__

#include <string>
#include <map>
#include <list>
#include <vector>
#include <iostream>

#include "PslFactoryI.h"

class PslScanner;

//! \brief This is the PSL Parser class. It uses a \a PslFactoryI to
//! to tell the user what has been parsed. On errors a \a PSL_Exception is thrown.
class PslParser {
public:	//constructors
	//! Constructs an PSL_Parser using the factory interface (\a PslFactoryI).
	//! \param factory An implementation of \a PslFactoryI to be used during \a parse().
	PslParser(PslFactoryI* factory);
	~PslParser();

public: //methods
	//! Parse PSL file format from stream and drive \a PslFactoryI.
	//! \param inputstream Input stream containing PSL file.
	//! \return true on case of success.
	//! \throw PSL_Exception in case of an error.
	bool parse(std::istream& inputstream);

private: //methods
	int yylex();
	int yyparse(void);
	void yyerror(const char*) const;
        std::string IntToStr(int i);
private: //members
	PslFactoryI*  mFactory;
	PslScanner*  mScanner;
};


#endif
