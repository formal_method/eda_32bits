// -*- C++ -*-
//! \author Patrick Pastoor, Raik Brinkmann
//! \copyright (c) Infineon Technologies AG, 2005
%{
#include <cassert>
#include <iostream>
#include <list>
#include <string>
#include <map>
#include <vector>
#include "PSL_Parser.h"
#include "PSL_Scanner.h"
#include "PSL_Exception.h"

using namespace std;

#undef yyparse
#define yyparse PslParser::yyparse

#undef yylval
#define yylval PslParserlval
#define yychar Pslyychar
#define yynerrs	Pslyynerrs
#define yydestruct	Pslyydestruct
%}

/*	Bison declarations */

%union
{
        unsigned FckId;
        int integer;
        const char *string;
}

%type <integer> EXT_TIME_EXPR
%type <FckId> PROPERTY EXPRESSION COND_EXPR CONSTANT
%token <integer> LEX_NUMBER
%token <string> LEX_NOT LEX_MINUS LEX_REDUCE LEX_ANDTYPE LEX_ORTYPE LEX_ARITH LEX_EXPR LEX_PROP LEX_BRACKETOPEN LEX_BRACKETCLOSE LEX_BRACKETOPEN2 LEX_BRACKETCLOSE2 LEX_BRACKETOPEN3 LEX_BRACKETCLOSE3 LEX_TIME_EXPR LEX_COND LEX_BIN_CONST LEX_COMPARE LEX_SLICING LEX_CONCATENATE LEX_IF LEX_THEN LEX_ELSE LEX_ENDIF LEX_SHIFT LEX_EXTEND LEX_KOMMA LEX_TEMPORAL
%left LEX_ORTYPE
%left LEX_ANDTYPE
%left LEX_NOT LEX_MINUS LEX_REDUCE
%left LEX_COMPARE
%left LEX_CONCATENATE
%left LEX_BRACKETOPEN3
%left LEX_COND
%nonassoc PRE_TEMPORAL
%nonassoc PRE_SLICING
%nonassoc PRE_SIMPLIFY

%%

/*	Grammar */

PROPERTY:
	PROPERTY PROPERTY %prec PRE_SIMPLIFY { 
				$$ = 0; } |
	LEX_PROP COND_EXPR {
				std::string str;
				str = $1;
				str.erase(0,9);
				mFactory->SetPropertyName(str);
				mFactory->FinishProperty();
				$$ = 0; } ;
COND_EXPR:
	COND_EXPR COND_EXPR %prec PRE_SIMPLIFY { 
				$$ = 0; } |
				
	LEX_BRACKETOPEN COND_EXPR LEX_BRACKETCLOSE { 
				$$ = 0; } |
				
	LEX_COND EXPRESSION {
				std::string in1;
				in1 = $1;
				mFactory->MakeCondition($2,in1);
				$$ = 0; } ;
CONSTANT:
	LEX_BIN_CONST {
				std::string in1;
				in1 =$1;
				$$ = mFactory->MakeConst(in1);} |
	LEX_NUMBER {
				std::string in1;
				in1 = IntToStr($1);
				$$ = mFactory->MakeConst(in1);
 } ;
EXPRESSION:
	LEX_EXPR {	
				std::string in1;
				in1=$1;
				$$ = mFactory->MakeExpression(in1);

				 } |
	LEX_BIN_CONST {
				std::string in1;
				in1=$1;
				$$ = mFactory->MakeConst(in1);

				} |
	LEX_BRACKETOPEN EXPRESSION LEX_BRACKETCLOSE {	
				$$ = $2;
				} |
	EXT_TIME_EXPR EXPRESSION %prec PRE_TEMPORAL {	
				mFactory->MakeTimeFrameChange("next",$1);
				$$ = $2;
				} |
	LEX_IF EXPRESSION LEX_THEN EXPRESSION LEX_ELSE EXPRESSION LEX_ENDIF {	
				$$ = mFactory->MakeIte($2,$4,$6,"");
				} |
	LEX_IF EXPRESSION LEX_THEN EXPRESSION LEX_ENDIF {	
				$$ = mFactory->MakeIt($2,$4,"");
				} |
	LEX_TEMPORAL EXPRESSION {
		                std::string str;
                                str = $1;
		                $$ = mFactory->MakeTemporal($2, str, "");
				} |
	LEX_NOT EXPRESSION {	
				$$ = mFactory->MakeNot($2,"");
				} |
	LEX_REDUCE EXPRESSION {	
				std::string str;
				str=$1;
				$$ = mFactory->MakeReduce($2,str,"");
				} |
	LEX_MINUS EXPRESSION {	
				std::string str;
				str=$1;
				$$ = mFactory->MakeMinus($2,"");
				} |
	LEX_SHIFT LEX_BRACKETOPEN3 EXPRESSION LEX_KOMMA CONSTANT LEX_BRACKETCLOSE3 {	
				std::string str;
				str=$1;
				$$ = mFactory->MakeShift($3,$5,str,"");
				 } |
	LEX_EXTEND LEX_BRACKETOPEN3 EXPRESSION LEX_KOMMA CONSTANT LEX_BRACKETCLOSE3 {	
				std::string str;
				str=$1;
				$$ = mFactory->MakeExtend($3,$5,str,"");
				 } |
	EXPRESSION LEX_ANDTYPE EXPRESSION	{	
				std::string str;
				str=$2;
				$$ = mFactory->MakeAndType($1,$3,str,"");
                                 } |
	EXPRESSION LEX_ORTYPE EXPRESSION	{	
				std::string str;
				str=$2;
				$$ = mFactory->MakeOrType($1,$3,str,"");
                                 } |
	EXPRESSION LEX_ARITH EXPRESSION	{	
				std::string str;
				str=$2;
				$$ = mFactory->MakeArith($1,$3,str,"");
                                 } |
	EXPRESSION LEX_COMPARE EXPRESSION	{
				std::string str;
				str=$2;
				$$ = mFactory->MakeCompare($1,$3,str,"");
                                 } |
	EXPRESSION LEX_CONCATENATE EXPRESSION	{
				$$ = mFactory->MakeConcat($1,$3,"");
                                 } |
	EXPRESSION LEX_BRACKETOPEN3 LEX_NUMBER LEX_SLICING LEX_NUMBER LEX_BRACKETCLOSE3 %prec PRE_SLICING {
				$$ = mFactory->MakeSlice($1,$3,$5,"");
                                 } |
	EXPRESSION LEX_BRACKETOPEN3 LEX_NUMBER LEX_BRACKETCLOSE3 {
                                FckId id1 = mFactory->MakeConst("1");  
				$$ = mFactory->MakeSlice($1,$3,id1,"");
                                 } ;


EXT_TIME_EXPR:
	LEX_TIME_EXPR LEX_BRACKETOPEN2 LEX_NUMBER LEX_BRACKETCLOSE2 {	
				std::string str;
                                str = $1;
				int i = $3;
				$$ = mFactory->MakeTimeFrameChange(str, i);
 } ;

;
%%
