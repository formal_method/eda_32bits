# -*- Makefile -*-
#*****************************************************************************
#
# DESCRIPTION: Verilator: Makefile for verilog source
#
# Code available from: http://www.veripool.org/verilator
#
#*****************************************************************************
#
# Copyright 2003-2013 by Wilson Snyder.  This program is free software; you can
# redistribute it and/or modify it under the terms of either the GNU
# Lesser General Public License Version 3 or the Perl Artistic License
# Version 2.0.
#
# Verilator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
#****************************************************************************/

#### Start of system configuration section. ####

srcdir = .

PERL = /usr/bin/perl
EXEEXT = 

#### End of system configuration section. ####


default: dbg opt obj
debug: dbg
optimize: opt

ifneq ($(words $(CURDIR)),1)
 $(error Unsupported: GNU Make cannot build in directories containing spaces, build elsewhere: '$(CURDIR)')
endif

UNDER_GIT = $(wildcard ${srcdir}/../.git/logs/HEAD)

#*********************************************************************

obj_opt:
	mkdir $@
obj_dbg:
	mkdir $@
 
PSL_ScannerFunction.o: PSL_ScannerFunction.C
PSL_ScannerFunction.C: PSL_ScannerFct.l FlexLexer.h
PSL_ParserFunction.o: PSL_ParserFunction.C
PSL_ParserFunction.C: PSL_ParserFct.y
# quandvk54-28/03
# Dung flex va bison de tao PSL_ScannerFunction.C PSL_ParserFunction.C
obj:
	(echo "#define FlexLexer CVE_PslFlexLexer" ; flex -L -8 -f -t -i PSL_ScannerFct.l) > PSL_ScannerFunction.C
	bison  -l -d -v -o PSL_ParserFunction.c PSL_ParserFct.y
	sed -e "s/int yyparse (void \*);/\/\/@/" \
	-e "s/int yyparse (void);/\/\/@/" \
	-e "s/^YYPARSE_RETURN_TYPE yyparse/\/\/&/" \
	-e "s/^yyparse (void);/thisisadirtytrickPSL;/" \
	-e "s/^yyparse (void \*);/thisisadirtytrickPSL;/" \
	-e "s/__attribute__ ((__unused__))/\/\/&/" \
		< PSL_ParserFunction.c > PSL_ParserFunction.cn
	rm -f PSL_ParserFunction.c
	mv PSL_ParserFunction.cn PSL_ParserFunction.C \
		|| ( rm PSL_ParserFunction.C && exit 1 )	
# end
%.o: %.C
	g++ -g -c -o $@ $?


.PHONY: ../lib/pslparser.a ../lib/pslparser.ad

opt: ../lib/pslparser.a
ifeq ($(VERILATOR_NO_OPT_BUILD),1)	# Faster laptop development... One build
../lib/pslparser.a: ../lib/pslparser.ad
	-rm -rf $@ #$@.exe
	-cp -p $<$(EXEEXT) $@$(EXEEXT)
else
../lib/pslparser.a: obj_opt #prefiles
	cd obj_opt && $(MAKE) -j 1  TGT=../$@ -f ../Makefile_obj #serial
	cd obj_opt && $(MAKE)       TGT=../$@ -f ../Makefile_obj
	
endif

dbg: ../lib/pslparser.ad
../lib/pslparser.ad: obj_dbg #prefiles
	# quandvk54-28/03
	# Dung flex va bison de tao PSL_ScannerFunction.C PSL_ParserFunction.C
	(echo "#define FlexLexer CVE_PslFlexLexer" ; flex -L -8 -f -t -i PSL_ScannerFct.l) > PSL_ScannerFunction.C
	bison  -l -d -v -o PSL_ParserFunction.c PSL_ParserFct.y
	sed -e "s/int yyparse (void \*);/\/\/@/" \
	-e "s/int yyparse (void);/\/\/@/" \
	-e "s/^YYPARSE_RETURN_TYPE yyparse/\/\/&/" \
	-e "s/^yyparse (void);/thisisadirtytrickPSL;/" \
	-e "s/^yyparse (void \*);/thisisadirtytrickPSL;/" \
	-e "s/__attribute__ ((__unused__))/\/\/&/" \
		< PSL_ParserFunction.c > PSL_ParserFunction.cn
	rm -f PSL_ParserFunction.c
	mv PSL_ParserFunction.cn PSL_ParserFunction.C \
		|| ( rm PSL_ParserFunction.C && exit 1 )
	# end
	cd obj_dbg && $(MAKE) -j 1  TGT=../$@ VL_DEBUG=1 -f ../Makefile_obj #serial
	cd obj_dbg && $(MAKE)       TGT=../$@ VL_DEBUG=1 -f ../Makefile_obj

prefiles::
prefiles:: config_rev.h
ifneq ($(UNDER_GIT),)	# If local git tree... Else don't burden users
# This output goes into srcdir if locally configured, as we need to distribute it as part of the kit.
config_rev.h:	${srcdir}/config_rev.pl ${srcdir}/../.git/logs/HEAD
	$(PERL) ${srcdir}/config_rev.pl ${srcdir} >$@
else
config_rev.h:	${srcdir}/config_rev.pl
	$(PERL) ${srcdir}/config_rev.pl ${srcdir} >$@
endif

maintainer-copy::
clean mostlyclean distclean maintainer-clean::
	-rm -rf obj_* *.log *.dmp *.vpd core
	-rm -f *.o *.d perlxsi.c *_gen_*
	-rm -f *__gen*
	-rm -f .objcache*
	-rm -f PSL_ParserFunction.C PSL_ParserFunction.h PSL_ParserFunction.c PSL_ParserFunction.output

distclean maintainer-clean::
	-rm -f Makefile Makefile_obj config_build.h

maintainer-clean::
	-rm -f config_rev.h
