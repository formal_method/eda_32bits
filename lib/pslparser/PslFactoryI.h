/*! \file PslFactoryI.h
    \brief This file provdes an abstract interface for creating
    Psl Objects. It is to be implemented by users of the rtp-Parser.

    \author  Raik Brinkmann, Patrick Pastoor

	\copyright (c) Infineon Technologies AG, 2005
*/

#ifndef PSL_FACTORY_I_H
#define PSL_FACTORY_I_H

#include <string>
#include <vector>
#include <exception>
#include "../../src/circuitbuilder.h"
#include "../../src/circuit.h"
#include "../../src/module.h"

/*!
  \class PslFactoryI
  \brief This class is an abstract interface which is driven by
  the PSL_Parser and must be implemented by its users.
  It defines abstract (pure virtual) methods for each element
  the parser is reading from an PSL-File.
  There are two sides of the interface:
  a) The parser side, driving the interface (using its methods) which has
  knowledge about the content of an PSL-file.
  b) The user side, implementing the interface and providing the
  parser with results (Ids) of the interface methods.
  There are methods belonging to each section of the file.
  In particular, there are functions defined for generating
  constraints, literals, properties, equivalences, and clauses.
  Each function must return a unique Id corresponding to the result
  on the user side. The parser will never call a method with an
  Id unknown to the user side, but only with Ids the user has generated
  (returned) before.
*/
class PslFactoryI {

public:
	PslFactoryI(){}
    virtual ~PslFactoryI(){} // keep GCC happy

	//! \brief This exception may be thrown by each of the factory functions
	//! of \a PslFactoryI and is catched within \a PSL_Parser, which in
	//! turn throws \a PSL_Exception, containing the line number and token,
	//! that must be catched by the user of the parser.
	class Exception : public std::exception
	{
	public:
		Exception(const std::string what) : m_What(what) {}
		~Exception() throw() {}
		const char* what() const throw() { return m_What.c_str(); }

	private:
		const std::string	m_What;
	};
	
	/**
                      * Create a "and" or "nand" for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the first fanin 
                      * @param id2 the id of the second fanin 
                      * @param type "and" or "nand"
                      * @param name the name of the assignment
                      */
        virtual FckId MakeAndType(FckId id1,FckId id2, std::string type, std::string name) {return 0;};
        
        /**
                      * Create a "or" , "nor", "xor", or "xnor" for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the first fanin 
                      * @param id2 the id of the second fanin 
                      * @param type "or" , "nor", "xor", or "xnor"
                      * @param name the name of the assignment
                      */
        virtual FckId MakeOrType(FckId id1,FckId id2, std::string type, std::string name) {return 0;};
        
        /**
                      * Create a "add" or "mult" for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the first fanin 
                      * @param id2 the id of the second fanin 
                      * @param type "add" or "mult"
                      * @param name the name of the assignment
                      */
        virtual FckId MakeArith(FckId id1,FckId id2, std::string type, std::string name) {return 0;};
        
        /**
                      * All assumptions and commitments are added. Combine these to the property and maybe begin a new property
                      */
        virtual void FinishProperty() {};
        
        /**
                      * The timeframe changes.
                      * @return if type="next" then -change else change
                      * @param type "next" or "prev"
                      * @param change the amount of change 
                      */
        virtual int MakeTimeFrameChange(std::string type, int change) {return 0;};
        
        /**
                      * Create a if-then-else for the signals id1, id2, id3
                      * @return id of the assignment
                      * @param id1 if-id 
                      * @param id2 then-id
                      * @param id3 else-id
                      * @param name the name of the assignment
                      */
        virtual FckId MakeIte(FckId id1,FckId id2,FckId id3, std::string name) {return 0;};
        
        /**
                      * Create a if-then for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 if-id 
                      * @param id2 then-id
                      * @param name the name of the assignment
                      */
        virtual FckId MakeIt(FckId id1,FckId id2, std::string name) {return 0;};
        
        /**
                      * Create a "fell", "rose" or "stable" for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the fanin 
                      * @param type "fell", "rose" or "stable"
                      * @param name the name of the assignment
                      */
        virtual FckId MakeTemporal(FckId id1, std::string type, std::string name) {return 0;};
        
        /**
                      * Create a id for the signal id
                      * @return id of the variable
                      * @param id the name of the signal in the circuit 
                      */
        virtual FckId MakeExpression(std::string id) {return 0;};
        
        /**
                      * Create a constant for the binary or integer value in id
                      * @return id of the constant
                      * @param id constant value in binary or integer form
                      */
        virtual FckId MakeConst(std::string id) {return 0;};
        
        /**
                      * Create a "shl" or "shr" for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the fanin 
                      * @param id2 the id of the shift amount 
                      * @param type "shl" or "shr"
                      * @param name the name of the assignment
                      */
        virtual FckId MakeShift(FckId id1,FckId id2, std::string type, std::string name) {return 0;};
        
        /**
                      * Create a "zero_extend" or "sign_extend" for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the first fanin 
                      * @param id2 the id of the second fanin 
                      * @param type "zero_extend" or "sign_extend"
                      * @param name the name of the assignment
                      */
        virtual FckId MakeExtend(FckId id1,FckId id2, std::string type, std::string name) {return 0;};
        
        /**
                      * Create a comparison for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the first fanin 
                      * @param id2 the id of the second fanin 
                      * @param type ">",  "<",  ">=",  "<=",  "==" or "!="
                      * @param name the name of the assignment
                      */
        virtual FckId MakeCompare(FckId id1,FckId id2,std::string type, std::string name) {return 0;};
        
        /**
                      * Create a concatenation of the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the first fanin 
                      * @param id2 the id of the second fanin 
                      * @param name the name of the assignment
                      */
        virtual FckId MakeConcat(FckId id1,FckId id2, std::string name) {return 0;};
        
        /**
                      * Create a slice for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the fanin 
                      * @param id2 the upper bound
                      * @param id3 the the lower bound
                      * @param name the name of the assignment
                      */
        virtual FckId MakeSlice(FckId id1,int id2,int id3, std::string name) {return 0;};
        
        /**
                      * add a assumption or a commitment
                      * @return id of the assignment
                      * @param id the id of the assumption/commitment
                      * @param type "assume" or "assert"
                      */
        virtual void MakeCondition(FckId id, std::string type, int repeat) {};
        
        /**
                      * Create a "not" for the signal id1
                      * @return id of the assignment
                      * @param id1 the id of the fanin 
                      * @param name the name of the assignment
                      */
        virtual FckId MakeNot(FckId id1, std::string name) {return 0;};
        
         /**
                      * Create a negation for the signal id1
                      * @return id of the assignment
                      * @param id1 the id of the fanin 
                      * @param name the name of the assignment
                      */
        virtual FckId MakeMinus(FckId id1, std::string name) {return 0;};
        
        /**
                      * Create a unary operation for the signal id1
                      * @return id of the assignment
                      * @param id1 the id of the fanin 
                      * @param type "reduce_or", "reduce_and" or "reduce_xor"
                      * @param name the name of the assignment
                      */
        virtual FckId MakeReduce(FckId id1, std::string type, std::string name) {return 0;};
        
        /**
                      * Name the current property
                       * @param name the name of the property
                      */
        virtual void SetPropertyName(std::string name) {};
    protected:

    private:
};

#endif
