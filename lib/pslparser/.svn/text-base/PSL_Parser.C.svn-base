//! \author Patrick Pastoor, Raik Brinkmann
//! \copyright (c) Infineon Technologies AG, 2005

#include <fstream>
#include <iostream>
#include "PSL_Scanner.h"
#include "PSL_Parser.h"
#include "PSL_Exception.h"

using namespace std;

PslParser::PslParser(PslFactoryI* factory)
	: mFactory(factory)
{
    mScanner = new PslScanner;
}

PslParser::~PslParser()
{ delete mScanner; }

int PslParser::yylex()
{ return mScanner->yylex();}

bool
PslParser::parse(std::istream& inputstream)
{
   mScanner->switch_streams(&inputstream, 0);
   return yyparse() == 0;
}

void
PslParser::yyerror(const char* msg) const
{
	throw PslException(PslException::PARSER_ERROR, msg, mScanner->lineno(), mScanner->YYText());
}

std::string PslParser::IntToStr(int i)
{
   std::stringstream stream;
   stream << i;
   return stream.str();
}
