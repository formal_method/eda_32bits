//! \author Patrick Pastoor, Raik Brinkmann
//! \copyright (c) Infineon Technologies AG, 2005

#ifndef __PSL_SCANNER_H__
#define __PSL_SCANNER_H__

#ifndef yyFlexLexer
//#undef yyFlexLexer
#define yyFlexLexer PslParserFlexLexer
#define FlexLexer psllibFlexLexer
#include <FlexLexer.h>
#undef yyFlexLexer
#endif // yyFlexLexer



#include <iostream>
#include <string>
class PslParserFlexLexer;
//! \brief This is the base class for \a PSL_Parser, and it is not intended
//! for dircect use.
class PslScanner : public PslParserFlexLexer {

public: // constructors
	PslScanner() {}
	~PslScanner(){}

public: // methods
	virtual int yylex();

protected: // members

};

#endif
