%{
//! \author Patrick Pastoor, Raik Brinkmann
//! \copyright (c) Infineon Technologies AG, 2005
#include <string>
#include <stdlib.h>
#include "PSL_Scanner.h"
#include "PslFactoryI.h"
#include "PSL_Exception.h"
#include "PSL_ParserFunction.h"
#include <stdio.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <math.h>
#include <fstream>
#include <cassert>

extern YYSTYPE PslParserlval;
%}

%option c++
%option prefix="PslParser"
%option yyclass="PslScanner"
%option noyywrap

LEX_DIGIT                [0-9]
LEX_LETTER               [A-Za-z_]
LEX_IF                   (IF)
LEX_THEN                 (THEN)
LEX_ELSE                 (ELSE)
LEX_ENDIF                (ENDIF)
LEX_NOT                  (NOT)
LEX_REDUCE               (REDUCE_OR)|(REDUCE_AND)|(REDUCE_XOR)
LEX_MINUS                (MINUS)
LEX_ANDTYPE              (AND)|(NAND)
LEX_ORTYPE               (OR)|(NOR)|(XOR)|(XNOR)
LEX_ARITH                (ADD)|(MULT)
LEX_COND                 (assume)|(assert)
LEX_PROP                 (property)({LEX_WHITESPACE}(({LEX_LETTER})({LEX_LETTER}|{LEX_DIGIT})*))?
LEX_SHIFT                (shr)|(shl)
LEX_EXTEND               (zero_extend)|(sign_extend)
LEX_WHITESPACE           [ \t\n]+
LEX_TIME_EXPR            (next)|(prev)
LEX_SLICING              (DOWNTO)
LEX_TEMPORAL             (fell)|(rose)|(stable)
LEX_BRACKETOPEN          [{]
LEX_BRACKETOPEN2         [[]
LEX_BRACKETOPEN3         [(]
LEX_BRACKETCLOSE         [}]
LEX_BRACKETCLOSE2        []]
LEX_BRACKETCLOSE3        [)]
LEX_NUMBER               ({LEX_DIGIT})+
LEX_BIN_CONST            ["]((0)|(1))+["]
LEX_COMPARE              (==)|(!=)|(>=)|(>)|(<)|(<=)
LEX_CONCATENATE          (;)
LEX_KOMMA                (,)
/*LEX_EXPR                 ((input)|(state)|(internal)|(output))[(]["](({LEX_LETTER})({LEX_LETTER}|{LEX_DIGIT})*)["][)]*/
LEX_EXPR                 (({LEX_LETTER})({LEX_LETTER}|{LEX_DIGIT})*)
%%

{LEX_NOT}               { PslParserlval.string=strdup(yytext); printf("Lexer found NOT %s\n",yytext); return LEX_NOT;             }
{LEX_REDUCE}            { PslParserlval.string=strdup(yytext); printf("Lexer found REDUCE %s\n",yytext); return LEX_REDUCE;          }
{LEX_MINUS}             { PslParserlval.string=strdup(yytext); printf("Lexer found MINUS %s\n",yytext); return LEX_MINUS;           }
{LEX_ANDTYPE}           { PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_ANDTYPE;         }
{LEX_ORTYPE}            { PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_ORTYPE;          }
{LEX_ARITH}             { PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_ARITH;           }
{LEX_PROP}              { PslParserlval.string=strdup(yytext); printf("Lexer found PROP %s\n",yytext); return LEX_PROP;            }
{LEX_BRACKETOPEN}       { PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_BRACKETOPEN;     }
{LEX_BRACKETCLOSE}      { PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_BRACKETCLOSE;    }
{LEX_BRACKETOPEN2}	{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_BRACKETOPEN2;    }
{LEX_BRACKETCLOSE2}	{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_BRACKETCLOSE2;   }
{LEX_BRACKETOPEN3}	{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_BRACKETOPEN3;    }
{LEX_BRACKETCLOSE3}	{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_BRACKETCLOSE3;   }
{LEX_SLICING}		{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_SLICING;         }
{LEX_TIME_EXPR}		{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_TIME_EXPR;       }
{LEX_NUMBER}		{ PslParserlval.integer=atoi(yytext);  printf("Lexer found %s\n",yytext); return LEX_NUMBER;          }
{LEX_COND}		{ PslParserlval.string=strdup(yytext); printf("Lexer found COND %s\n",yytext); return LEX_COND;            }
{LEX_IF}		{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_IF;              }
{LEX_THEN}		{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_THEN;            }
{LEX_ELSE}		{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_ELSE;            }
{LEX_ENDIF}		{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_ENDIF;           }
{LEX_SHIFT}		{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_SHIFT;           }
{LEX_EXTEND}		{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_EXTEND;          }
{LEX_KOMMA}		{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_KOMMA;           }
{LEX_BIN_CONST}		{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_BIN_CONST;       }
{LEX_COMPARE}		{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_COMPARE;         }
{LEX_CONCATENATE}	{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_CONCATENATE;     }
{LEX_TEMPORAL}		{ PslParserlval.string=strdup(yytext); printf("Lexer found %s\n",yytext); return LEX_TEMPORAL;     }
{LEX_EXPR}		{ PslParserlval.string=strdup(yytext); printf("Lexer found EXPR %s\n",yytext); return LEX_EXPR;            }
{LEX_WHITESPACE}	{ };

%%
