/* A Bison parser, made by GNU Bison 1.875.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     LEX_NUMBER = 258,
     LEX_NOT = 259,
     LEX_MINUS = 260,
     LEX_REDUCE = 261,
     LEX_ANDTYPE = 262,
     LEX_ORTYPE = 263,
     LEX_ARITH = 264,
     LEX_EXPR = 265,
     LEX_PROP = 266,
     LEX_BRACKETOPEN = 267,
     LEX_BRACKETCLOSE = 268,
     LEX_BRACKETOPEN2 = 269,
     LEX_BRACKETCLOSE2 = 270,
     LEX_BRACKETOPEN3 = 271,
     LEX_BRACKETCLOSE3 = 272,
     LEX_TIME_EXPR = 273,
     LEX_COND = 274,
     LEX_BIN_CONST = 275,
     LEX_COMPARE = 276,
     LEX_SLICING = 277,
     LEX_CONCATENATE = 278,
     LEX_IF = 279,
     LEX_THEN = 280,
     LEX_ELSE = 281,
     LEX_ENDIF = 282,
     LEX_SHIFT = 283,
     LEX_EXTEND = 284,
     LEX_KOMMA = 285,
     LEX_TEMPORAL = 286,
     PRE_TEMPORAL = 287,
     PRE_SLICING = 288,
     PRE_SIMPLIFY = 289
   };
#endif
#define LEX_NUMBER 258
#define LEX_NOT 259
#define LEX_MINUS 260
#define LEX_REDUCE 261
#define LEX_ANDTYPE 262
#define LEX_ORTYPE 263
#define LEX_ARITH 264
#define LEX_EXPR 265
#define LEX_PROP 266
#define LEX_BRACKETOPEN 267
#define LEX_BRACKETCLOSE 268
#define LEX_BRACKETOPEN2 269
#define LEX_BRACKETCLOSE2 270
#define LEX_BRACKETOPEN3 271
#define LEX_BRACKETCLOSE3 272
#define LEX_TIME_EXPR 273
#define LEX_COND 274
#define LEX_BIN_CONST 275
#define LEX_COMPARE 276
#define LEX_SLICING 277
#define LEX_CONCATENATE 278
#define LEX_IF 279
#define LEX_THEN 280
#define LEX_ELSE 281
#define LEX_ENDIF 282
#define LEX_SHIFT 283
#define LEX_EXTEND 284
#define LEX_KOMMA 285
#define LEX_TEMPORAL 286
#define PRE_TEMPORAL 287
#define PRE_SLICING 288
#define PRE_SIMPLIFY 289




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)

typedef union YYSTYPE {
        unsigned FckId;
        int integer;
        const char *string;
} YYSTYPE;
/* Line 1204 of yacc.c.  */

# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;



