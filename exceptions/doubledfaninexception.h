/*! ************************************************************************
    \file doubledfaninexception.h
    \brief Description of the DoubledFaninException

    This file describes the attributes and operations a DoubledFaninException 
 * has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 15 2006

Modification history:


**************************************************************************/
#ifndef DOUBLEDFANINEXCEPTION_H
#define DOUBLEDFANINEXCEPTION_H
#include "exception.h"
#include "../src/component.h"
#include <vector>
#include <iostream>

/**
 * Class to describe the behaviour of the DoubledFaninException.
 * The DoubledFaninException is used for handling exceptions that occur if a
 * fanin component is already there or not needed.
 */
class DoubledFaninException : public Exception
{
    
/**
 * Public stuff
 */
public:    
    /**
     * Constructors/Destructors
     */
    /**
     * Constructor accepting parameters for details about the exception.
     * @param msg the message for the exception
     * @param caller the calling Component 
     * @param faninToAdd the fanin component that shall be added
     * @param faninHave the fanin component that is already there
     * @param pos the position at which the fanin shall be inserted
     */
    DoubledFaninException(std::string msg, Component* caller, 
        Component* faninToAdd, Component* faninHave, int pos): mCaller(caller),
        mFaninToAdd(faninToAdd), mFaninHave(faninHave), mPos(pos)
        {SetMMessage(msg);}
    /**
     * Destructor
     */
    ~DoubledFaninException(){}
    /**
     * Accessor Methods
     */
            
    /**
     * Operations
     */
    /**
     * Print the exception message to the standard output
     */
    void GetMessage()
    {
        // print the message
        std::cout << mMessage << std::endl;
        // print the type of the component invoking the exception
        std::cout << "Component invoking the exception: "  
                << mCaller->GetMId() << " of Type "
                << mCaller->GetMType() << std::endl;
        //print the type of the fanin component that shall be added
        std::cout << "The component " << mCaller->GetMId() 
                << " of Type "<< mFaninToAdd->GetMType() 
                << " shall be added at position " << mPos << std::endl;
        //and the component that is already there
        std::cout << "However, at position " << mPos 
                << " there is already the fanin component " 
                << mCaller->GetMId() << " of Type "
                << mFaninHave->GetMType() << std::endl;
    } // End getMessage()
    
/**
 * Protected stuff
 */
    protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * A variable for the Component invoking the Exception. 
     */
    Component* mCaller;
    /**
     * A variable for the fanin Component that shall be added
     */
    Component* mFaninToAdd;
    /**
     * A variable for the fanin component that is already there
     */
    Component* mFaninHave;
    /**
     * A variable for the position at which this shall happen
     */
    int mPos;
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */    
}; //End DoubledFaninException
#endif //DOUBLEDFANINEXCEPTION_H
