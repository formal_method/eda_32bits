/*! ************************************************************************
    \file fanoutmapmiss.h
    \brief Description of the FanoutMapMiss

    This file describes the attributes and operations a FanoutMapMiss 
 * has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 15 2006

Modification history:


**************************************************************************/
#ifndef FANOUTMAPMISS_H
#define FANOUTMAPMISS_H
#include "exception.h"
#include "../src/component.h"
#include <vector>
#include <iostream>

/**
 * Class to describe the behaviour of a FanoutMapMiss.
 * The FanoutMapMiss is an Exception used for handling exceptions that occur 
 * if a Component shall be added whose fanin Components are not yet available.
 */
class FanoutMapMiss : public Exception
{
    
/**
 * Public stuff
 */
public:    
    /**
     * Constructors/Destructors
     */
    /**
     * Constructor accepting parameters for details about the exception.
     * @param msg the message for the exception
     * @param callerName The name of the calling Component
     * @param callerType The type of the calling Component
     * @param fanin vector with the names of the known fanin components
     */
    FanoutMapMiss(std::string msg, FckId callerId, int callerType, 
                  FckId fanin): mCallerId(callerId), 
                  mCallerType(callerType), mFanin(fanin){SetMMessage(msg);}
    /**
     * Destructor
     */
    ~FanoutMapMiss(){}
    /**
     * Accessor Methods
     */
            
    /**
     * Operations
     */
    /**
     * Print the exception message to the standard output
     */
    void GetMessage()
    {
        // print the message
        std::cout << mMessage << std::endl;
        // print the type of the component invoking the exception
        std::cout << "Component invoking the exception: " 
                << mCallerId << " of type " << mCallerType << std::endl;
        // print the fanout we haven't found yet
        std::cout << "The following id could not be matched: " 
                << mFanin << std::endl;
    } // End getMessage()
    
/**
 * Protected stuff
 */
    protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * A variable for the Name of the Component invoking the exception
     */
    FckId mCallerId;
    /**
     * A variable for the Component invoking the Exception. 
     */
    int mCallerType;
    /**
     * A vector for the fanin Components
     */
    FckId mFanin;
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */    
}; //End FanoutMapMiss
#endif //FANOUTMAPMISS_H
