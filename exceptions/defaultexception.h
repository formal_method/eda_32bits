/*! ************************************************************************
    \file defaultexception.h
    \brief Description of the DefaultException

    This file describes the attributes and operations a DefaultException 
 * has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 15 2006

Modification history:


**************************************************************************/
#ifndef DEFAULTEXCEPTION_H
#define DEFAULTEXCEPTION_H
#include "exception.h"
#include "../src/component.h"
#include <vector>

/**
 * Class to describe the behaviour of the DefaultException.
 * The DefaultException is used for handling exceptions that only pass a 
 * string as a message
 */
class DefaultException : public Exception
{
    
/**
 * Public stuff
 */
public:    
    /**
     * Constructors/Destructors
     */
    /**
     * Constructor accepting parameters for details about the exception.
     * @param msg the message for the exception
     */
    DefaultException(std::string msg) {SetMMessage(msg);}
    /**
     * Destructor
     */
    ~DefaultException(){}
    /**
     * Accessor Methods
     */
            
    /**
     * Operations
     */
    /**
     * Print the exception message to the standard output
     */
    void GetMessage()
    {
        // print the message
        std::cout << mMessage << std::endl;
    } // End getMessage()
    
/**
 * Protected stuff
 */
    protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */    
}; //End DefaultException
#endif //DEFAULTEXCEPTION_H
