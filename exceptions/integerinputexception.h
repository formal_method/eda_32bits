/*! ************************************************************************
    \file integerinputexception.h
    \brief Description of the IntegerInputException

    This file describes the attributes and operations a IntegerInputException 
 * has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 15 2006

Modification history:


**************************************************************************/
#ifndef INTEGERINPUTEXCEPTION_H
#define INTEGERINPUTEXCEPTION_H
#include "exception.h"
#include "../src/component.h"
#include <vector>

/**
 * Class to describe the behaviour of the IntegerInputException.
 * The IntegerInputException is used for handling exceptions that occur if an 
 * input representing an integer (e.g. shift amount etc.) has a value that is
 * either unspecified or out of range.
 */
class IntegerInputException : public Exception
{
    
/**
 * Public stuff
 */
public:    
    /**
     * Constructors/Destructors
     */
    /**
     * Constructor accepting parameters for details about the exception.
     * @param msg the message for the exception
     * @param caller the calling Component 
     * @param fanins the fanin components of the caller
     * @param inputs the value of the inputs of the caller
     */
    IntegerInputException(std::string msg, Component* caller, 
        std::vector<Component*> fanins, 
        std::vector<std::vector<satlib::SatI::lValue> > inputs): mCaller(caller), 
        mFanins(fanins), mInputs(inputs){SetMMessage(msg);}
   
    /**
     * Destructor
     */
    ~IntegerInputException(){}
    /**
     * Accessor Methods
     */
            
    /**
     * Operations
     */
    /**
     * Print the exception message to the standard output
     */
    void GetMessage()
    {
        // print the message
        std::cout << mMessage << std::endl;
        // print the type of the component invoking the exception
        std::cout << "Component invoking the exception: " 
                << mCaller->GetMId() << " of type "
                << mCaller->GetMType() << std::endl;
        //print the fanins and the corresponding inputs values
        for (unsigned int i=0;i<mFanins.size();i++)
        {
            std::cout << "Caused by fanin" << mFanins[i]->GetMId()
                    << "of type " << mFanins[i]->GetMType() << "of value "; 
            for (unsigned int j=0;j<mInputs[i].size();j++)
            {
                std::cout << mInputs[i][j];
            }
            std::cout << std::endl;
        }
    } // End getMessage()
    
/**
 * Protected stuff
 */
    protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * A variable for the Component invoking the Exception. 
     */
    Component* mCaller;
    /**
     * A variable for the fanin Component that shall be added
     */
    std::vector<Component*> mFanins;
    /**
     * A variable for the fanin component that is already there
     */
    std::vector<std::vector<satlib::SatI::lValue> > mInputs;
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */    
}; //End IntegerInputException
#endif //INTEGERINPUTEXCEPTION_H
