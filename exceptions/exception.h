/*! ************************************************************************
    \file exception.h
    \brief Description of the Exception

    This file describes the attributes and operations an Exception 
 * has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Fri Feb 17 2006

Modification history:


**************************************************************************/
#ifndef EXCEPTION_H
#define EXCEPTION_H
#include "../src/component.h"
#include <vector>

/**
 * Class to describe the behaviour of an Exception.
 * The Exception is used as an Interface to catch all possible Exceptions that
 * are implemented as subclasses of this.
 */
class Exception
{
    
/**
 * Public stuff
 */
public:    
    /**
     * Constructors/Destructors
     */
    /**
     * Constructor accepting parameters for details about the exception.
     */
    Exception(){}
    /**
     * Destructor
     */
    virtual ~Exception(){}
    /**
     * Accessor Methods
     */
            
    /**
     * Operations
     */
    /**
     * Print the exception message to the standard output.
     * Pure virtual has to be implemented by subclass
     */
    virtual void GetMessage()=0;
        
/**
 * Protected stuff
 */
    protected:
    /**
     * Fields
     */
    /**
     * A variable for the message of the exception
     */
    std::string mMessage;
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Set the message of the exception
     * @param message The message this exception shall throw.
     */
    inline void SetMMessage(std::string message){mMessage=message;}
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */    
}; //End Exception
#endif //EXCEPTION_H
