/*! ************************************************************************
    \file generalfanexception.h
    \brief Description of the GeneralFanException

    This file describes the attributes and operations a GeneralFanException 
 * has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 15 2006

Modification history:


**************************************************************************/
#ifndef GENERALFANEXCEPTION_H
#define GENERALFANEXCEPTION_H
#include "exception.h"
#include "../src/component.h"
#include <vector>
#include <iostream>

/**
 * Class to describe the behaviour of the GeneralFanException.
 * The GeneralFanException is used for handling exceptions that occur if a
 * fanin/fanout is either not available or of a type that is not accepted in 
 * that place.
 */
class GeneralFanException : public Exception
{
    
/**
 * Public stuff
 */
public:    
    /**
     * Constructors/Destructors
     */
    /**
     * Constructor accepting parameters for details about the exception.
     * @param msg The message for the exception
     * @param caller The calling Component 
     * @param fan Vector with the known fanin/fanout components
     */
    GeneralFanException(std::string msg, Component* caller, 
        std::vector<Component*> fan): mCallerId(caller->GetMId()), 
        mCallerType(caller->GetMType()), mFan(fan){SetMMessage(msg);}
    
    /**
     * Constructor accepting parameters for details about the exception.
     * @param msg The message for the exception
     * @param callerName The name of the calling Component 
     * @param callerType The type of the calling component
     * @param fan Vector with the known fanin/fanout components
     */                    
    GeneralFanException(std::string msg, FckId callerId, 
        int callerType, std::vector<Component*> fan): mCallerId(callerId),
        mCallerType(callerType), mFan(fan) {SetMMessage(msg);}
    
    /**
     * Destructor
     */
    ~GeneralFanException(){}
    /**
     * Accessor Methods
     */
            
    /**
     * Operations
     */
    /**
     * Print the exception message to the standard output
     */
    void GetMessage()
    {
        // print the message
        std::cout << mMessage << std::endl;
        // print the type of the component invoking the exception
        std::cout << "Component invoking the exception: " 
                << mCallerId << " of Type " << mCallerType << std::endl;
        // if we don't have a fanin variable print that
        if (mFan.size()==0) std::cout << "No Fanin available" << std::endl;
        //otherwise print the type of the fanin components that are available
        else for (unsigned int i=0; i<mFan.size();i++)
        {
            std::cout << "Fanin at position " << i << " "
                    << mFan[i]->GetMId() << " of type " 
                    << mFan[i]->GetMType() << std::endl;
        } // End else for (unsigned int i=0; i<mFan.size();i++)
    } // End getMessage()
    
/**
 * Protected stuff
 */
    protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * A variable for the name of the component invoking the exception
     */
    FckId mCallerId;
    /**
     * A variable for the Component invoking the Exception. 
     */
    int mCallerType;
    /**
     * A vector for the fanin Components
     */
    std::vector<Component*> mFan;
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */    
}; //End GeneralFanException
#endif //GENERALFANEXCEPTION_H
