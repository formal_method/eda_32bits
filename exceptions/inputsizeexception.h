/*! ************************************************************************
    \file inputsizeexception.h
    \brief Description of the InputSizeException

    This file describes the attributes and operations a InputSizeException has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 15 2006

Modification history:


**************************************************************************/
#ifndef INPUTSIZEEXCEPTION_H
#define INPUTSIZEEXCEPTION_H
#include "exception.h"
#include "../src/component.h"
#include <vector>

/**
 * Class to describe the behaviour of the InputSizeException.
 * The InputSizeException is used for handling exceptions that occur if two 
 * inputs that should have the same size do not have the same size.
 */
class InputSizeException : public Exception
{
    
/**
 * Public stuff
 */
public:    
    /**
     * Constructors/Destructors
     */
    /**
     * Constructor accepting parameters for details about the exception.
     * @param msg the message for the exception
     * @param caller the calling Component 
     * @param fanins the fanin components of the inputs
     */
    InputSizeException(std::string msg, Component* caller, 
        std::vector<Component*> fanins): mCaller(caller), 
        mFanins(fanins){SetMMessage(msg);}
        
    /**
     * Constructor accepting parameters for details about the exception.
     * @param msg the message for the exception
     * @param caller the calling Component 
     * @param inputs the value of the inputs
     */
        InputSizeException(std::string msg, Component* caller, 
                           std::vector<std::vector<satlib::SatI::lValue>* > inputs): mCaller(caller), 
                           mInputs(inputs){SetMMessage(msg);}
                           
    /**
     * Destructor
     */
    ~InputSizeException(){}
    /**
     * Accessor Methods
     */
            
    /**
     * Operations
     */
    /**
     * Print the exception message to the standard output
     */
    void GetMessage()
    {
        // print the message
        std::cout << mMessage << std::endl;
        // print the type of the component invoking the exception
        std::cout << "Component invoking the exception: " 
                << mCaller->GetMId() << " of type "
                << mCaller->GetMType() << std::endl;
        //if the caller is not an Input
        if (mCaller->GetMType()!=INPUT)
        {
            //print the fanins and the size of the corresponding inputs
            for (unsigned int i=0;i<mFanins.size();i++)
            {
                std::cout << "Caused by fanin " << mFanins[i]->GetMId() 
                        << "of type " << mFanins[i]->GetMType() << " of width " 
                        << mFanins[i]->GetMFanoutWidth() << std::endl;
            }// End for (unsigned int i=0;i<mFanins.size();i++)
        }// End if (mCaller->GetMType()!=INPUT)
        else
        {
            std::cout << " requires " << mCaller->GetMFanoutWidth() 
                    << " input bits but got " << mInputs[0]->size() << std::endl; 
        }
    } // End getMessage()
    
/**
 * Protected stuff
 */
    protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * A variable for the Component invoking the Exception. 
     */
    Component* mCaller;
    /**
     * A variable for the fanin Component that shall be added
     */
    std::vector<Component*> mFanins;
    /**
     * A variable for the fanin component that is already there
     */
    std::vector<std::vector<satlib::SatI::lValue>* > mInputs;
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */    
}; //End InputSizeException
#endif //INPUTSIZEEXCEPTION_H

