/*! ************************************************************************
    \file faninnumberexception.h
    \brief Description of the FaninNumberException

    This file describes the attributes and operations a FaninNumberException 
 * has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 16 2006

Modification history:


**************************************************************************/
#ifndef FANINNUMBEREXCEPTION_H
#define FANINNUMBEREXCEPTION_H
#include "exception.h"
#include "../src/component.h"
#include <vector>
#include <iostream>

/**
 * Class to describe the behaviour of the FaninNumberException.
 * The FaninNumberException is used for handling exceptions that occur if a
 * the number of fanins given is smaller than the number of fanins required.
 */
class FaninNumberException : public Exception
{
    
/**
 * Public stuff
 */
public:    
    /**
     * Constructors/Destructors
     */
    /**
     * Constructor accepting parameters for details about the exception.
     * @param msg the message for the exception
     * @param callerName the name of the calling Component
     * @param callerType the type of the calling component
     * @param reqFanin The number of required fanin components
     * @param fanin vector with the known fanin names
     */
    FaninNumberException(std::string msg, const FckId& callerId, 
        int callerType, int reqFanin, const std::vector<FckId>& fanin): 
        mCallerId(callerId), mCallerType(callerType), mReqFanin(reqFanin),
        mFanin(fanin){SetMMessage(msg);}
    /**
     * Destructor
     */
    ~FaninNumberException(){}
    /**
     * Accessor Methods
     */
            
    /**
     * Operations
     */
    /**
     * Print the exception message to the standard output
     */
    void GetMessage()
    {
        // print the message
        std::cout << mMessage << std::endl;
        // print the type of the component invoking the exception
        std::cout << "Component invoking the exception: "
                << mCallerId << " of Type " << mCallerType << std::endl;
        std::cout << "A Component of this type requires " 
                << mReqFanin << " fanin components"
                << std::endl;
        // print the fanin we have
        std::cout << "The following components were passed: " << std::endl;
        for (unsigned int i=0; i<mFanin.size(); i++)
        {
            std::cout << mFanin[i] << std::endl;
        }
    } // End getMessage()
    
/**
 * Protected stuff
 */
    protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * A variable for the Component invoking the Exception. 
     */
    FckId mCallerId;
    /**
     * A variable for the Component invoking the Exception. 
     */
    int mCallerType;
    /**
     * A variable to store the number of fanin components required by
     * the component invoking the exceptions
     */
    int mReqFanin;
    /**
     * A vector for the fanin Components
     */
    std::vector<FckId> mFanin;
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */    
}; //End FanoutMapMiss
#endif //FANOUTMAPMISS_H
