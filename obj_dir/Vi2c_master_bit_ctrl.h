// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _Vi2c_master_bit_ctrl_H_
#define _Vi2c_master_bit_ctrl_H_

#include "verilated.h"
class Vi2c_master_bit_ctrl__Syms;

//----------

VL_MODULE(Vi2c_master_bit_ctrl) {
  public:
    // CELLS
    // Public to allow access to /*verilator_public*/ items;
    // otherwise the application code can consider these internals.
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_IN8(wb_clk_i,0,0);
    VL_IN8(wb_rst_i,0,0);
    VL_IN8(wb_adr_i,2,0);
    VL_IN8(wb_dat_i,7,0);
    VL_IN8(wb_we_i,0,0);
    VL_IN8(wb_stb_i,0,0);
    VL_OUT8(wb_ack_o,0,0);
    VL_OUT8(wb_inta_o,0,0);
    VL_OUT8(wb_dat_o,7,0);
    VL_OUT8(scl_pad_io,0,0);
    VL_OUT8(sda_pad_io,0,0);
    //char	__VpadToAlign11[1];
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    VL_SIG8(v__DOT__scl_padoen_o,0,0);
    VL_SIG8(v__DOT__sda_padoen_o,0,0);
    VL_SIG8(v__DOT__top__DOT__ctr,7,0);
    VL_SIG8(v__DOT__top__DOT__txr,7,0);
    VL_SIG8(v__DOT__top__DOT__cr,7,0);
    VL_SIG8(v__DOT__top__DOT__sr,7,0);
    VL_SIG8(v__DOT__top__DOT__done,0,0);
    VL_SIG8(v__DOT__top__DOT__irxack,0,0);
    VL_SIG8(v__DOT__top__DOT__rxack,0,0);
    VL_SIG8(v__DOT__top__DOT__tip,0,0);
    VL_SIG8(v__DOT__top__DOT__irq_flag,0,0);
    VL_SIG8(v__DOT__top__DOT__i2c_busy,0,0);
    VL_SIG8(v__DOT__top__DOT__i2c_al,0,0);
    VL_SIG8(v__DOT__top__DOT__al,0,0);
    VL_SIG8(v__DOT__top__DOT__wb_wacc,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__core_cmd,3,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__core_txd,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__core_ack,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__core_rxd,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__sr,7,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__shift,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__ld,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__dcnt,2,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__c_state,4,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSCL,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSDA,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__bitc__DOT__dscl_oen,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__bitc__DOT__clk_en,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__bitc__DOT__dSCL,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__bitc__DOT__dSDA,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__bitc__DOT__sta_condition,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__bitc__DOT__sto_condition,0,0);
    VL_SIG8(v__DOT__top__DOT__bytec__DOT__bitc__DOT__cmd_stop,0,0);
    VL_SIG16(v__DOT__top__DOT__prer,15,0);
    VL_SIG16(v__DOT__top__DOT__bytec__DOT__bitc__DOT__cnt,15,0);
    //char	__VpadToAlign54[2];
    VL_SIG(v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state,16,0);
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    VL_SIG8(__Vclklast__TOP__wb_clk_i,0,0);
    //char	__VpadToAlign65[3];
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    Vi2c_master_bit_ctrl__Syms*	__VlSymsp;		// Symbol table
    
    // PARAMETERS
    // Parameters marked /*verilator public*/ for use by application code
    
    // CONSTRUCTORS
  private:
    Vi2c_master_bit_ctrl& operator= (const Vi2c_master_bit_ctrl&);	///< Copying not allowed
    Vi2c_master_bit_ctrl(const Vi2c_master_bit_ctrl&);	///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible WRT DPI scope names.
    Vi2c_master_bit_ctrl(const char* name="TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~Vi2c_master_bit_ctrl();
    
    // USER METHODS
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(Vi2c_master_bit_ctrl__Syms* symsp, bool first);
  private:
    static IData	_change_request(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp);
  public:
    static void	_combo__TOP__3(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp);
    static void	_eval(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp);
    static void	_eval_initial(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp);
    static void	_eval_settle(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__1(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp);
    static void	_settle__TOP__2(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
