// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vi2c_master_bit_ctrl.h for the primary calling header

#include "Vi2c_master_bit_ctrl.h" // For This
#include "Vi2c_master_bit_ctrl__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(Vi2c_master_bit_ctrl) {
    Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp = __VlSymsp = new Vi2c_master_bit_ctrl__Syms(this, name());
    Vi2c_master_bit_ctrl* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    wb_clk_i = 0;
    wb_rst_i = 0;
    wb_adr_i = VL_RAND_RESET_I(3);
    wb_dat_i = VL_RAND_RESET_I(8);
    wb_we_i = 0;
    wb_stb_i = 0;
    wb_ack_o = 0;
    wb_inta_o = 0;
    wb_dat_o = VL_RAND_RESET_I(8);
    scl_pad_io = VL_RAND_RESET_I(1);
    sda_pad_io = VL_RAND_RESET_I(1);
    v__DOT__scl_padoen_o = VL_RAND_RESET_I(1);
    v__DOT__sda_padoen_o = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__prer = VL_RAND_RESET_I(16);
    v__DOT__top__DOT__ctr = VL_RAND_RESET_I(8);
    v__DOT__top__DOT__txr = VL_RAND_RESET_I(8);
    v__DOT__top__DOT__cr = VL_RAND_RESET_I(8);
    v__DOT__top__DOT__sr = VL_RAND_RESET_I(8);
    v__DOT__top__DOT__done = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__irxack = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__rxack = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__tip = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__irq_flag = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__i2c_busy = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__i2c_al = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__al = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__wb_wacc = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__core_cmd = VL_RAND_RESET_I(4);
    v__DOT__top__DOT__bytec__DOT__core_txd = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__core_ack = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__core_rxd = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__sr = VL_RAND_RESET_I(8);
    v__DOT__top__DOT__bytec__DOT__shift = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__ld = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__dcnt = VL_RAND_RESET_I(3);
    v__DOT__top__DOT__bytec__DOT__c_state = VL_RAND_RESET_I(5);
    v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSCL = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSDA = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__bitc__DOT__dscl_oen = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__bitc__DOT__clk_en = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__bitc__DOT__cnt = VL_RAND_RESET_I(16);
    v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = VL_RAND_RESET_I(17);
    v__DOT__top__DOT__bytec__DOT__bitc__DOT__dSCL = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__bitc__DOT__dSDA = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__bitc__DOT__sta_condition = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__bitc__DOT__sto_condition = VL_RAND_RESET_I(1);
    v__DOT__top__DOT__bytec__DOT__bitc__DOT__cmd_stop = VL_RAND_RESET_I(1);
    __Vclklast__TOP__wb_clk_i = 0;
}

void Vi2c_master_bit_ctrl::__Vconfigure(Vi2c_master_bit_ctrl__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

Vi2c_master_bit_ctrl::~Vi2c_master_bit_ctrl() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void Vi2c_master_bit_ctrl::eval() {
    Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    Vi2c_master_bit_ctrl* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate Vi2c_master_bit_ctrl::eval\n"); );
    int __VclockLoop = 0;
    IData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void Vi2c_master_bit_ctrl::_eval_initial_loop(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    IData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

void Vi2c_master_bit_ctrl::_sequent__TOP__1(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vi2c_master_bit_ctrl::_sequent__TOP__1\n"); );
    Vi2c_master_bit_ctrl* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    VL_SIG8(__Vdly__wb_ack_o,0,0);
    VL_SIG8(__Vdly__v__DOT__top__DOT__cr,7,0);
    VL_SIG8(__Vdly__v__DOT__top__DOT__al,0,0);
    VL_SIG8(__Vdly__v__DOT__top__DOT__irq_flag,0,0);
    VL_SIG8(__Vdly__v__DOT__top__DOT__bytec__DOT__sr,7,0);
    VL_SIG8(__Vdly__v__DOT__top__DOT__bytec__DOT__dcnt,2,0);
    VL_SIG8(__Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd,3,0);
    VL_SIG8(__Vdly__v__DOT__top__DOT__bytec__DOT__core_txd,0,0);
    VL_SIG8(__Vdly__v__DOT__top__DOT__done,0,0);
    VL_SIG8(__Vdly__v__DOT__top__DOT__bytec__DOT__c_state,4,0);
    VL_SIG8(__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSCL,0,0);
    VL_SIG8(__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSDA,0,0);
    VL_SIG8(__Vdly__v__DOT__top__DOT__i2c_busy,0,0);
    VL_SIG8(__Vdly__v__DOT__scl_padoen_o,0,0);
    VL_SIG8(__Vdly__v__DOT__sda_padoen_o,0,0);
    VL_SIG8(__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk,0,0);
    VL_SIG16(__Vdly__v__DOT__top__DOT__prer,15,0);
    VL_SIG16(__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__cnt,15,0);
    VL_SIG(__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state,16,0);
    // Body
    __Vdly__wb_ack_o = vlTOPp->wb_ack_o;
    __Vdly__v__DOT__top__DOT__prer = vlTOPp->v__DOT__top__DOT__prer;
    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSDA 
	= vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSDA;
    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSCL 
	= vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSCL;
    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__cnt 
	= vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__cnt;
    __Vdly__v__DOT__top__DOT__i2c_busy = vlTOPp->v__DOT__top__DOT__i2c_busy;
    __Vdly__v__DOT__top__DOT__cr = vlTOPp->v__DOT__top__DOT__cr;
    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state 
	= vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state;
    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk 
	= vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk;
    __Vdly__v__DOT__scl_padoen_o = vlTOPp->v__DOT__scl_padoen_o;
    __Vdly__v__DOT__sda_padoen_o = vlTOPp->v__DOT__sda_padoen_o;
    __Vdly__v__DOT__top__DOT__bytec__DOT__core_txd 
	= vlTOPp->v__DOT__top__DOT__bytec__DOT__core_txd;
    __Vdly__v__DOT__top__DOT__done = vlTOPp->v__DOT__top__DOT__done;
    __Vdly__v__DOT__top__DOT__bytec__DOT__c_state = vlTOPp->v__DOT__top__DOT__bytec__DOT__c_state;
    __Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd 
	= vlTOPp->v__DOT__top__DOT__bytec__DOT__core_cmd;
    __Vdly__v__DOT__top__DOT__irq_flag = vlTOPp->v__DOT__top__DOT__irq_flag;
    __Vdly__v__DOT__top__DOT__al = vlTOPp->v__DOT__top__DOT__al;
    __Vdly__v__DOT__top__DOT__bytec__DOT__dcnt = vlTOPp->v__DOT__top__DOT__bytec__DOT__dcnt;
    __Vdly__v__DOT__top__DOT__bytec__DOT__sr = vlTOPp->v__DOT__top__DOT__bytec__DOT__sr;
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_top.v:169
    __Vdly__wb_ack_o = ((IData)(vlTOPp->wb_stb_i) & 
			(~ (IData)(vlTOPp->wb_ack_o)));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_top.v:294
    vlTOPp->wb_inta_o = ((~ (IData)(vlTOPp->wb_rst_i)) 
			 & ((IData)(vlTOPp->v__DOT__top__DOT__irq_flag) 
			    & ((IData)(vlTOPp->v__DOT__top__DOT__ctr) 
			       >> 6)));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_top.v:173
    vlTOPp->wb_dat_o = (0xff & ((4 & (IData)(vlTOPp->wb_adr_i))
				 ? ((2 & (IData)(vlTOPp->wb_adr_i))
				     ? ((1 & (IData)(vlTOPp->wb_adr_i))
					 ? 0 : (IData)(vlTOPp->v__DOT__top__DOT__cr))
				     : ((1 & (IData)(vlTOPp->wb_adr_i))
					 ? (IData)(vlTOPp->v__DOT__top__DOT__txr)
					 : (IData)(vlTOPp->v__DOT__top__DOT__sr)))
				 : ((2 & (IData)(vlTOPp->wb_adr_i))
				     ? ((1 & (IData)(vlTOPp->wb_adr_i))
					 ? (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__sr)
					 : (IData)(vlTOPp->v__DOT__top__DOT__ctr))
				     : ((1 & (IData)(vlTOPp->wb_adr_i))
					 ? ((IData)(vlTOPp->v__DOT__top__DOT__prer) 
					    >> 8) : (IData)(vlTOPp->v__DOT__top__DOT__prer)))));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_bit_ctrl.v:300
    __Vdly__v__DOT__top__DOT__i2c_busy = ((~ (IData)(vlTOPp->wb_rst_i)) 
					  & (((IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sta_condition) 
					      | (IData)(vlTOPp->v__DOT__top__DOT__i2c_busy)) 
					     & (~ (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sto_condition))));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_top.v:211
    if (vlTOPp->wb_rst_i) {
	__Vdly__v__DOT__top__DOT__cr = 0;
    } else {
	if (vlTOPp->v__DOT__top__DOT__wb_wacc) {
	    if ((((IData)(vlTOPp->v__DOT__top__DOT__ctr) 
		  >> 7) & (4 == (IData)(vlTOPp->wb_adr_i)))) {
		__Vdly__v__DOT__top__DOT__cr = vlTOPp->wb_dat_i;
	    }
	} else {
	    if (((IData)(vlTOPp->v__DOT__top__DOT__done) 
		 | (IData)(vlTOPp->v__DOT__top__DOT__i2c_al))) {
		__Vdly__v__DOT__top__DOT__cr = (0xf 
						& (IData)(__Vdly__v__DOT__top__DOT__cr));
	    }
	    __Vdly__v__DOT__top__DOT__cr = ((0xf0 & (IData)(__Vdly__v__DOT__top__DOT__cr)) 
					    | (8 & (IData)(vlTOPp->v__DOT__top__DOT__cr)));
	}
    }
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_top.v:270
    if (vlTOPp->wb_rst_i) {
	__Vdly__v__DOT__top__DOT__al = 0;
	vlTOPp->v__DOT__top__DOT__rxack = 0;
	vlTOPp->v__DOT__top__DOT__tip = 0;
	__Vdly__v__DOT__top__DOT__irq_flag = 0;
    } else {
	__Vdly__v__DOT__top__DOT__al = ((IData)(vlTOPp->v__DOT__top__DOT__i2c_al) 
					| ((IData)(vlTOPp->v__DOT__top__DOT__al) 
					   & (~ ((IData)(vlTOPp->v__DOT__top__DOT__cr) 
						 >> 7))));
	__Vdly__v__DOT__top__DOT__irq_flag = ((((IData)(vlTOPp->v__DOT__top__DOT__done) 
						| (IData)(vlTOPp->v__DOT__top__DOT__i2c_al)) 
					       | (IData)(vlTOPp->v__DOT__top__DOT__irq_flag)) 
					      & (~ (IData)(vlTOPp->v__DOT__top__DOT__cr)));
	vlTOPp->v__DOT__top__DOT__rxack = vlTOPp->v__DOT__top__DOT__irxack;
	vlTOPp->v__DOT__top__DOT__tip = (1 & (((IData)(vlTOPp->v__DOT__top__DOT__cr) 
					       >> 5) 
					      | ((IData)(vlTOPp->v__DOT__top__DOT__cr) 
						 >> 4)));
    }
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_byte_ctrl.v:189
    if (vlTOPp->wb_rst_i) {
	__Vdly__v__DOT__top__DOT__bytec__DOT__dcnt = 0;
    } else {
	if (vlTOPp->v__DOT__top__DOT__bytec__DOT__ld) {
	    __Vdly__v__DOT__top__DOT__bytec__DOT__dcnt = 7;
	} else {
	    if (vlTOPp->v__DOT__top__DOT__bytec__DOT__shift) {
		__Vdly__v__DOT__top__DOT__bytec__DOT__dcnt 
		    = (7 & ((IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__dcnt) 
			    - (IData)(1)));
	    }
	}
    }
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_byte_ctrl.v:178
    if (vlTOPp->wb_rst_i) {
	__Vdly__v__DOT__top__DOT__bytec__DOT__sr = 0;
    } else {
	if (vlTOPp->v__DOT__top__DOT__bytec__DOT__ld) {
	    __Vdly__v__DOT__top__DOT__bytec__DOT__sr 
		= vlTOPp->v__DOT__top__DOT__txr;
	} else {
	    if (vlTOPp->v__DOT__top__DOT__bytec__DOT__shift) {
		__Vdly__v__DOT__top__DOT__bytec__DOT__sr 
		    = ((0xfe & ((IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__sr) 
				<< 1)) | (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__core_rxd));
	    }
	}
    }
    vlTOPp->wb_ack_o = __Vdly__wb_ack_o;
    vlTOPp->v__DOT__top__DOT__i2c_busy = __Vdly__v__DOT__top__DOT__i2c_busy;
    vlTOPp->v__DOT__top__DOT__irq_flag = __Vdly__v__DOT__top__DOT__irq_flag;
    vlTOPp->v__DOT__top__DOT__al = __Vdly__v__DOT__top__DOT__al;
    vlTOPp->v__DOT__top__DOT__sr = ((0xfc & (IData)(vlTOPp->v__DOT__top__DOT__sr)) 
				    | (((IData)(vlTOPp->v__DOT__top__DOT__tip) 
					<< 1) | (IData)(vlTOPp->v__DOT__top__DOT__irq_flag)));
    vlTOPp->v__DOT__top__DOT__sr = ((3 & (IData)(vlTOPp->v__DOT__top__DOT__sr)) 
				    | (((IData)(vlTOPp->v__DOT__top__DOT__rxack) 
					<< 7) | (((IData)(vlTOPp->v__DOT__top__DOT__i2c_busy) 
						  << 6) 
						 | ((IData)(vlTOPp->v__DOT__top__DOT__al) 
						    << 5))));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_byte_ctrl.v:206
    if (((IData)(vlTOPp->wb_rst_i) | (IData)(vlTOPp->v__DOT__top__DOT__i2c_al))) {
	__Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd = 0;
	__Vdly__v__DOT__top__DOT__bytec__DOT__core_txd = 0;
	vlTOPp->v__DOT__top__DOT__bytec__DOT__shift = 0;
	vlTOPp->v__DOT__top__DOT__bytec__DOT__ld = 0;
	__Vdly__v__DOT__top__DOT__done = 0;
	__Vdly__v__DOT__top__DOT__bytec__DOT__c_state = 0;
	vlTOPp->v__DOT__top__DOT__irxack = 0;
    } else {
	__Vdly__v__DOT__top__DOT__bytec__DOT__core_txd 
	    = (1 & ((IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__sr) 
		    >> 7));
	vlTOPp->v__DOT__top__DOT__bytec__DOT__shift = 0;
	vlTOPp->v__DOT__top__DOT__bytec__DOT__ld = 0;
	__Vdly__v__DOT__top__DOT__done = 0;
	if ((0 == (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__c_state))) {
	    if ((1 & (((((IData)(vlTOPp->v__DOT__top__DOT__cr) 
			 >> 5) | ((IData)(vlTOPp->v__DOT__top__DOT__cr) 
				  >> 4)) | ((IData)(vlTOPp->v__DOT__top__DOT__cr) 
					    >> 6)) 
		      & (~ (IData)(vlTOPp->v__DOT__top__DOT__done))))) {
		if ((0x80 & (IData)(vlTOPp->v__DOT__top__DOT__cr))) {
		    __Vdly__v__DOT__top__DOT__bytec__DOT__c_state = 1;
		    __Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd = 1;
		} else {
		    if ((0x20 & (IData)(vlTOPp->v__DOT__top__DOT__cr))) {
			__Vdly__v__DOT__top__DOT__bytec__DOT__c_state = 2;
			__Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd = 8;
		    } else {
			if ((0x10 & (IData)(vlTOPp->v__DOT__top__DOT__cr))) {
			    __Vdly__v__DOT__top__DOT__bytec__DOT__c_state = 4;
			    __Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd = 4;
			} else {
			    __Vdly__v__DOT__top__DOT__bytec__DOT__c_state = 0x10;
			    __Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd = 2;
			}
		    }
		}
		vlTOPp->v__DOT__top__DOT__bytec__DOT__ld = 1;
	    }
	} else {
	    if ((1 == (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__c_state))) {
		if (vlTOPp->v__DOT__top__DOT__bytec__DOT__core_ack) {
		    if ((0x20 & (IData)(vlTOPp->v__DOT__top__DOT__cr))) {
			__Vdly__v__DOT__top__DOT__bytec__DOT__c_state = 2;
			__Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd = 8;
		    } else {
			__Vdly__v__DOT__top__DOT__bytec__DOT__c_state = 4;
			__Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd = 4;
		    }
		    vlTOPp->v__DOT__top__DOT__bytec__DOT__ld = 1;
		}
	    } else {
		if ((4 == (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__c_state))) {
		    if (vlTOPp->v__DOT__top__DOT__bytec__DOT__core_ack) {
			if ((0 != (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__dcnt))) {
			    __Vdly__v__DOT__top__DOT__bytec__DOT__c_state = 4;
			    __Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd = 4;
			    vlTOPp->v__DOT__top__DOT__bytec__DOT__shift = 1;
			} else {
			    __Vdly__v__DOT__top__DOT__bytec__DOT__c_state = 8;
			    __Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd = 8;
			}
		    }
		} else {
		    if ((2 == (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__c_state))) {
			if (vlTOPp->v__DOT__top__DOT__bytec__DOT__core_ack) {
			    if ((0 != (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__dcnt))) {
				__Vdly__v__DOT__top__DOT__bytec__DOT__c_state = 2;
				__Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd = 8;
			    } else {
				__Vdly__v__DOT__top__DOT__bytec__DOT__c_state = 8;
				__Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd = 4;
			    }
			    vlTOPp->v__DOT__top__DOT__bytec__DOT__shift = 1;
			    __Vdly__v__DOT__top__DOT__bytec__DOT__core_txd 
				= (1 & ((IData)(vlTOPp->v__DOT__top__DOT__cr) 
					>> 3));
			}
		    } else {
			if ((8 == (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__c_state))) {
			    if (vlTOPp->v__DOT__top__DOT__bytec__DOT__core_ack) {
				if ((0x40 & (IData)(vlTOPp->v__DOT__top__DOT__cr))) {
				    __Vdly__v__DOT__top__DOT__bytec__DOT__c_state = 0x10;
				    __Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd = 2;
				} else {
				    __Vdly__v__DOT__top__DOT__bytec__DOT__c_state = 0;
				    __Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd = 0;
				    __Vdly__v__DOT__top__DOT__done = 1;
				}
				vlTOPp->v__DOT__top__DOT__irxack 
				    = vlTOPp->v__DOT__top__DOT__bytec__DOT__core_rxd;
				__Vdly__v__DOT__top__DOT__bytec__DOT__core_txd = 1;
			    } else {
				__Vdly__v__DOT__top__DOT__bytec__DOT__core_txd 
				    = (1 & ((IData)(vlTOPp->v__DOT__top__DOT__cr) 
					    >> 3));
			    }
			} else {
			    if ((0x10 == (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__c_state))) {
				if (vlTOPp->v__DOT__top__DOT__bytec__DOT__core_ack) {
				    __Vdly__v__DOT__top__DOT__bytec__DOT__c_state = 0;
				    __Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd = 0;
				    __Vdly__v__DOT__top__DOT__done = 1;
				}
			    }
			}
		    }
		}
	    }
	}
    }
    vlTOPp->v__DOT__top__DOT__bytec__DOT__c_state = __Vdly__v__DOT__top__DOT__bytec__DOT__c_state;
    vlTOPp->v__DOT__top__DOT__done = __Vdly__v__DOT__top__DOT__done;
    vlTOPp->v__DOT__top__DOT__bytec__DOT__dcnt = __Vdly__v__DOT__top__DOT__bytec__DOT__dcnt;
    vlTOPp->v__DOT__top__DOT__bytec__DOT__sr = __Vdly__v__DOT__top__DOT__bytec__DOT__sr;
    vlTOPp->v__DOT__top__DOT__cr = __Vdly__v__DOT__top__DOT__cr;
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_bit_ctrl.v:331
    if (((IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSCL) 
	 & (~ (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__dSCL)))) {
	vlTOPp->v__DOT__top__DOT__bytec__DOT__core_rxd 
	    = vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSDA;
    }
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_bit_ctrl.v:337
    if (((IData)(vlTOPp->wb_rst_i) | (IData)(vlTOPp->v__DOT__top__DOT__i2c_al))) {
	__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0;
	vlTOPp->v__DOT__top__DOT__bytec__DOT__core_ack = 0;
	__Vdly__v__DOT__scl_padoen_o = 1;
	__Vdly__v__DOT__sda_padoen_o = 1;
	__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
    } else {
	vlTOPp->v__DOT__top__DOT__bytec__DOT__core_ack = 0;
	if (vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__clk_en) {
	    if (((((((((0 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state) 
		       | (1 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) 
		      | (2 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) 
		     | (4 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) 
		    | (8 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) 
		   | (0x10 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) 
		  | (0x20 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) 
		 | (0x40 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state))) {
		if ((0 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
		    __Vdly__v__DOT__scl_padoen_o = vlTOPp->v__DOT__scl_padoen_o;
		    __Vdly__v__DOT__sda_padoen_o = vlTOPp->v__DOT__sda_padoen_o;
		    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state 
			= ((8 & (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__core_cmd))
			    ? ((4 & (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__core_cmd))
			        ? 0 : ((2 & (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__core_cmd))
				        ? 0 : ((1 & (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__core_cmd))
					        ? 0
					        : 0x200)))
			    : ((4 & (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__core_cmd))
			        ? ((2 & (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__core_cmd))
				    ? 0 : ((1 & (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__core_cmd))
					    ? 0 : 0x2000))
			        : ((2 & (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__core_cmd))
				    ? ((1 & (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__core_cmd))
				        ? 0 : 0x20)
				    : ((1 & (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__core_cmd))
				        ? 1 : 0))));
		    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
		} else {
		    if ((1 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
			__Vdly__v__DOT__scl_padoen_o 
			    = vlTOPp->v__DOT__scl_padoen_o;
			__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 2;
			__Vdly__v__DOT__sda_padoen_o = 1;
			__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
		    } else {
			if ((2 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
			    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 4;
			    __Vdly__v__DOT__scl_padoen_o = 1;
			    __Vdly__v__DOT__sda_padoen_o = 1;
			    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
			} else {
			    if ((4 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
				__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 8;
				__Vdly__v__DOT__scl_padoen_o = 1;
				__Vdly__v__DOT__sda_padoen_o = 0;
				__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
			    } else {
				if ((8 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
				    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0x10;
				    __Vdly__v__DOT__scl_padoen_o = 1;
				    __Vdly__v__DOT__sda_padoen_o = 0;
				    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
				} else {
				    if ((0x10 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
					__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0;
					vlTOPp->v__DOT__top__DOT__bytec__DOT__core_ack = 1;
					__Vdly__v__DOT__scl_padoen_o = 0;
					__Vdly__v__DOT__sda_padoen_o = 0;
					__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
				    } else {
					if ((0x20 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
					    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0x40;
					    __Vdly__v__DOT__scl_padoen_o = 0;
					    __Vdly__v__DOT__sda_padoen_o = 0;
					    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
					} else {
					    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0x80;
					    __Vdly__v__DOT__scl_padoen_o = 1;
					    __Vdly__v__DOT__sda_padoen_o = 0;
					    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
					}
				    }
				}
			    }
			}
		    }
		}
	    } else {
		if (((((((((0x80 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state) 
			   | (0x100 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) 
			  | (0x200 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) 
			 | (0x400 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) 
			| (0x800 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) 
		       | (0x1000 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) 
		      | (0x2000 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) 
		     | (0x4000 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state))) {
		    if ((0x80 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
			__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0x100;
			__Vdly__v__DOT__scl_padoen_o = 1;
			__Vdly__v__DOT__sda_padoen_o = 0;
			__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
		    } else {
			if ((0x100 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
			    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0;
			    vlTOPp->v__DOT__top__DOT__bytec__DOT__core_ack = 1;
			    __Vdly__v__DOT__scl_padoen_o = 1;
			    __Vdly__v__DOT__sda_padoen_o = 1;
			    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
			} else {
			    if ((0x200 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
				__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0x400;
				__Vdly__v__DOT__scl_padoen_o = 0;
				__Vdly__v__DOT__sda_padoen_o = 1;
				__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
			    } else {
				if ((0x400 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
				    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0x800;
				    __Vdly__v__DOT__scl_padoen_o = 1;
				    __Vdly__v__DOT__sda_padoen_o = 1;
				    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
				} else {
				    if ((0x800 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
					__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0x1000;
					__Vdly__v__DOT__scl_padoen_o = 1;
					__Vdly__v__DOT__sda_padoen_o = 1;
					__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
				    } else {
					if ((0x1000 
					     == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
					    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0;
					    vlTOPp->v__DOT__top__DOT__bytec__DOT__core_ack = 1;
					    __Vdly__v__DOT__scl_padoen_o = 0;
					    __Vdly__v__DOT__sda_padoen_o = 1;
					    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
					} else {
					    if ((0x2000 
						 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
						__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0x4000;
						__Vdly__v__DOT__scl_padoen_o = 0;
						__Vdly__v__DOT__sda_padoen_o 
						    = vlTOPp->v__DOT__top__DOT__bytec__DOT__core_txd;
						__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
					    } else {
						__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0x8000;
						__Vdly__v__DOT__scl_padoen_o = 1;
						__Vdly__v__DOT__sda_padoen_o 
						    = vlTOPp->v__DOT__top__DOT__bytec__DOT__core_txd;
						__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 1;
					    }
					}
				    }
				}
			    }
			}
		    }
		} else {
		    if ((0x8000 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
			__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0x10000;
			__Vdly__v__DOT__scl_padoen_o = 1;
			__Vdly__v__DOT__sda_padoen_o 
			    = vlTOPp->v__DOT__top__DOT__bytec__DOT__core_txd;
			__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 1;
		    } else {
			if ((0x10000 == vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state)) {
			    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0;
			    vlTOPp->v__DOT__top__DOT__bytec__DOT__core_ack = 1;
			    __Vdly__v__DOT__scl_padoen_o = 0;
			    __Vdly__v__DOT__sda_padoen_o 
				= vlTOPp->v__DOT__top__DOT__bytec__DOT__core_txd;
			    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk = 0;
			} else {
			    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state = 0;
			}
		    }
		}
	    }
	}
    }
    vlTOPp->v__DOT__top__DOT__bytec__DOT__core_txd 
	= __Vdly__v__DOT__top__DOT__bytec__DOT__core_txd;
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_bit_ctrl.v:321
    vlTOPp->v__DOT__top__DOT__i2c_al = ((~ (IData)(vlTOPp->wb_rst_i)) 
					& ((((IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk) 
					     & (~ (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSDA))) 
					    & (IData)(vlTOPp->v__DOT__sda_padoen_o)) 
					   | (((0 != vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state) 
					       & (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sto_condition)) 
					      & (~ (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__cmd_stop)))));
    vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk 
	= __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sda_chk;
    vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state 
	= __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__c_state;
    vlTOPp->v__DOT__sda_padoen_o = __Vdly__v__DOT__sda_padoen_o;
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_bit_ctrl.v:282
    if (vlTOPp->wb_rst_i) {
	vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sta_condition = 0;
	vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sto_condition = 0;
    } else {
	vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sta_condition 
	    = (((~ (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSDA)) 
		& (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__dSDA)) 
	       & (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSCL));
	vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sto_condition 
	    = (((IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSDA) 
		& (~ (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__dSDA))) 
	       & (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSCL));
    }
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_bit_ctrl.v:313
    if (vlTOPp->wb_rst_i) {
	vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__cmd_stop = 0;
    } else {
	if (vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__clk_en) {
	    vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__cmd_stop 
		= (2 == (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__core_cmd));
	}
    }
    vlTOPp->v__DOT__top__DOT__bytec__DOT__core_cmd 
	= __Vdly__v__DOT__top__DOT__bytec__DOT__core_cmd;
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_bit_ctrl.v:254
    if (vlTOPp->wb_rst_i) {
	__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSCL = 1;
	__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSDA = 1;
	vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__dSCL = 1;
	vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__dSDA = 1;
    } else {
	vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__dSCL 
	    = vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSCL;
	vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__dSDA 
	    = vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSDA;
	__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSCL 
	    = vlTOPp->scl_pad_io;
	__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSDA 
	    = vlTOPp->sda_pad_io;
    }
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_bit_ctrl.v:221
    if (vlTOPp->wb_rst_i) {
	__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__cnt = 0;
	vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__clk_en = 1;
    } else {
	if ((1 & ((~ (IData)((0 != (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__cnt)))) 
		  | (~ ((IData)(vlTOPp->v__DOT__top__DOT__ctr) 
			>> 7))))) {
	    if (((IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__dscl_oen) 
		 & (~ (IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSCL)))) {
		__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__cnt 
		    = vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__cnt;
		vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__clk_en = 0;
	    } else {
		__Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__cnt 
		    = vlTOPp->v__DOT__top__DOT__prer;
		vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__clk_en = 1;
	    }
	} else {
	    __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__cnt 
		= (0xffff & ((IData)(vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__cnt) 
			     - (IData)(1)));
	    vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__clk_en = 0;
	}
    }
    vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSDA 
	= __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSDA;
    vlTOPp->sda_pad_io = vlTOPp->v__DOT__sda_padoen_o;
    vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__cnt 
	= __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__cnt;
    vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSCL 
	= __Vdly__v__DOT__top__DOT__bytec__DOT__bitc__DOT__sSCL;
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_bit_ctrl.v:214
    vlTOPp->v__DOT__top__DOT__bytec__DOT__bitc__DOT__dscl_oen 
	= vlTOPp->v__DOT__scl_padoen_o;
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/I2C/i2c_master_top.v:188
    if (vlTOPp->wb_rst_i) {
	__Vdly__v__DOT__top__DOT__prer = 0xffff;
	vlTOPp->v__DOT__top__DOT__ctr = 0;
	vlTOPp->v__DOT__top__DOT__txr = 0;
    } else {
	if (vlTOPp->v__DOT__top__DOT__wb_wacc) {
	    if ((0 == (IData)(vlTOPp->wb_adr_i))) {
		__Vdly__v__DOT__top__DOT__prer = ((0xff00 
						   & (IData)(vlTOPp->v__DOT__top__DOT__prer)) 
						  | (IData)(vlTOPp->wb_dat_i));
	    } else {
		if ((1 == (IData)(vlTOPp->wb_adr_i))) {
		    __Vdly__v__DOT__top__DOT__prer 
			= (((IData)(vlTOPp->wb_dat_i) 
			    << 8) | (0xff & (IData)(vlTOPp->v__DOT__top__DOT__prer)));
		} else {
		    if ((2 == (IData)(vlTOPp->wb_adr_i))) {
			vlTOPp->v__DOT__top__DOT__ctr 
			    = vlTOPp->wb_dat_i;
		    } else {
			if ((3 == (IData)(vlTOPp->wb_adr_i))) {
			    vlTOPp->v__DOT__top__DOT__txr 
				= vlTOPp->wb_dat_i;
			}
		    }
		}
	    }
	}
    }
    vlTOPp->v__DOT__scl_padoen_o = __Vdly__v__DOT__scl_padoen_o;
    vlTOPp->v__DOT__top__DOT__prer = __Vdly__v__DOT__top__DOT__prer;
    vlTOPp->scl_pad_io = vlTOPp->v__DOT__scl_padoen_o;
}

void Vi2c_master_bit_ctrl::_settle__TOP__2(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vi2c_master_bit_ctrl::_settle__TOP__2\n"); );
    Vi2c_master_bit_ctrl* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__top__DOT__sr = ((0xfc & (IData)(vlTOPp->v__DOT__top__DOT__sr)) 
				    | (((IData)(vlTOPp->v__DOT__top__DOT__tip) 
					<< 1) | (IData)(vlTOPp->v__DOT__top__DOT__irq_flag)));
    vlTOPp->v__DOT__top__DOT__sr = ((3 & (IData)(vlTOPp->v__DOT__top__DOT__sr)) 
				    | (((IData)(vlTOPp->v__DOT__top__DOT__rxack) 
					<< 7) | (((IData)(vlTOPp->v__DOT__top__DOT__i2c_busy) 
						  << 6) 
						 | ((IData)(vlTOPp->v__DOT__top__DOT__al) 
						    << 5))));
    vlTOPp->sda_pad_io = vlTOPp->v__DOT__sda_padoen_o;
    vlTOPp->v__DOT__top__DOT__wb_wacc = ((IData)(vlTOPp->wb_stb_i) 
					 & (IData)(vlTOPp->wb_we_i));
    vlTOPp->scl_pad_io = vlTOPp->v__DOT__scl_padoen_o;
}

void Vi2c_master_bit_ctrl::_combo__TOP__3(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vi2c_master_bit_ctrl::_combo__TOP__3\n"); );
    Vi2c_master_bit_ctrl* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__top__DOT__wb_wacc = ((IData)(vlTOPp->wb_stb_i) 
					 & (IData)(vlTOPp->wb_we_i));
}

void Vi2c_master_bit_ctrl::_eval(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vi2c_master_bit_ctrl::_eval\n"); );
    Vi2c_master_bit_ctrl* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (((IData)(vlTOPp->wb_clk_i) & (~ (IData)(vlTOPp->__Vclklast__TOP__wb_clk_i)))) {
	vlTOPp->_sequent__TOP__1(vlSymsp);
    }
    vlTOPp->_combo__TOP__3(vlSymsp);
    // Final
    vlTOPp->__Vclklast__TOP__wb_clk_i = vlTOPp->wb_clk_i;
}

void Vi2c_master_bit_ctrl::_eval_initial(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vi2c_master_bit_ctrl::_eval_initial\n"); );
    Vi2c_master_bit_ctrl* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vi2c_master_bit_ctrl::final() {
    VL_DEBUG_IF(VL_PRINTF("    Vi2c_master_bit_ctrl::final\n"); );
    // Variables
    Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp = this->__VlSymsp;
    Vi2c_master_bit_ctrl* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vi2c_master_bit_ctrl::_eval_settle(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vi2c_master_bit_ctrl::_eval_settle\n"); );
    Vi2c_master_bit_ctrl* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_settle__TOP__2(vlSymsp);
}

IData Vi2c_master_bit_ctrl::_change_request(Vi2c_master_bit_ctrl__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vi2c_master_bit_ctrl::_change_request\n"); );
    Vi2c_master_bit_ctrl* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    IData __req = false;  // Logically a bool
    return __req;
}
