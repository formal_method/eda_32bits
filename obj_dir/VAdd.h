// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _VAdd_H_
#define _VAdd_H_

#include "verilated.h"
class VAdd__Syms;

//----------

VL_MODULE(VAdd) {
  public:
    // CELLS
    // Public to allow access to /*verilator_public*/ items;
    // otherwise the application code can consider these internals.
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_IN8(clock,0,0);
    VL_IN8(reset,0,0);
    VL_IN8(Interrupts,4,0);
    VL_IN8(NMI,0,0);
    VL_IN8(InstMem_Ready,0,0);
    VL_IN8(DataMem_Ready,0,0);
    VL_OUT8(DataMem_Read,0,0);
    VL_OUT8(DataMem_Write,3,0);
    VL_OUT8(InstMem_Read,0,0);
    VL_OUT8(IP,7,0);
    //char	__VpadToAlign10[2];
    VL_IN(DataMem_In,31,0);
    VL_IN(InstMem_In,31,0);
    VL_OUT(DataMem_Address,29,0);
    VL_OUT(DataMem_Out,31,0);
    VL_OUT(InstMem_Address,29,0);
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    VL_SIG8(v__DOT__IF_Stall,0,0);
    VL_SIG8(v__DOT__IF_EXC_AdIF,0,0);
    VL_SIG8(v__DOT__ID_Stall,0,0);
    VL_SIG8(v__DOT__ID_PCSrc,1,0);
    VL_SIG8(v__DOT__ID_RsFwdSel,1,0);
    VL_SIG8(v__DOT__ID_RtFwdSel,1,0);
    VL_SIG8(v__DOT__ID_Mfc0,0,0);
    VL_SIG8(v__DOT__ID_Mtc0,0,0);
    VL_SIG8(v__DOT__ID_Eret,0,0);
    VL_SIG8(v__DOT__ID_NextIsDelay,0,0);
    VL_SIG8(v__DOT__ID_KernelMode,0,0);
    VL_SIG8(v__DOT__ID_EXC_Sys,0,0);
    VL_SIG8(v__DOT__ID_EXC_Bp,0,0);
    VL_SIG8(v__DOT__ID_Exception_Flush,0,0);
    VL_SIG8(v__DOT__ID_CmpEQ,0,0);
    VL_SIG8(v__DOT__ID_IsBDS,0,0);
    VL_SIG8(v__DOT__ID_IsFlushed,0,0);
    VL_SIG8(v__DOT__EX_Stall,0,0);
    VL_SIG8(v__DOT__EX_RsFwdSel,1,0);
    VL_SIG8(v__DOT__EX_RtFwdSel,1,0);
    VL_SIG8(v__DOT__EX_Link,0,0);
    VL_SIG8(v__DOT__EX_LinkRegDst,1,0);
    VL_SIG8(v__DOT__EX_ALUSrcImm,0,0);
    VL_SIG8(v__DOT__EX_ALUOp,4,0);
    VL_SIG8(v__DOT__EX_Movn,0,0);
    VL_SIG8(v__DOT__EX_Movz,0,0);
    VL_SIG8(v__DOT__EX_LLSC,0,0);
    VL_SIG8(v__DOT__EX_MemRead,0,0);
    VL_SIG8(v__DOT__EX_MemWrite,0,0);
    VL_SIG8(v__DOT__EX_MemByte,0,0);
    VL_SIG8(v__DOT__EX_MemHalf,0,0);
    VL_SIG8(v__DOT__EX_MemSignExtend,0,0);
    VL_SIG8(v__DOT__EX_RegWrite,0,0);
    VL_SIG8(v__DOT__EX_MemtoReg,0,0);
    VL_SIG8(v__DOT__EX_Rs,4,0);
    VL_SIG8(v__DOT__EX_Rt,4,0);
    VL_SIG8(v__DOT__EX_WantRsByEX,0,0);
    VL_SIG8(v__DOT__EX_NeedRsByEX,0,0);
    VL_SIG8(v__DOT__EX_WantRtByEX,0,0);
    VL_SIG8(v__DOT__EX_NeedRtByEX,0,0);
    VL_SIG8(v__DOT__EX_Trap,0,0);
    VL_SIG8(v__DOT__EX_TrapCond,0,0);
    VL_SIG8(v__DOT__EX_CanErr,0,0);
    VL_SIG8(v__DOT__EX_EX_CanErr,0,0);
    VL_SIG8(v__DOT__EX_M_CanErr,0,0);
    VL_SIG8(v__DOT__EX_KernelMode,0,0);
    VL_SIG8(v__DOT__EX_ReverseEndian,0,0);
    VL_SIG8(v__DOT__EX_Exception_Flush,0,0);
    VL_SIG8(v__DOT__EX_RtRd,4,0);
    VL_SIG8(v__DOT__EX_EXC_Ov,0,0);
    VL_SIG8(v__DOT__EX_IsBDS,0,0);
    VL_SIG8(v__DOT__EX_Left,0,0);
    VL_SIG8(v__DOT__EX_Right,0,0);
    VL_SIG8(v__DOT__M_Stall,0,0);
    VL_SIG8(v__DOT__M_Stall_Controller,0,0);
    VL_SIG8(v__DOT__M_LLSC,0,0);
    VL_SIG8(v__DOT__M_MemRead,0,0);
    VL_SIG8(v__DOT__M_MemWrite,0,0);
    VL_SIG8(v__DOT__M_MemByte,0,0);
    VL_SIG8(v__DOT__M_MemHalf,0,0);
    VL_SIG8(v__DOT__M_MemSignExtend,0,0);
    VL_SIG8(v__DOT__M_RegWrite,0,0);
    VL_SIG8(v__DOT__M_MemtoReg,0,0);
    VL_SIG8(v__DOT__M_EXC_AdEL,0,0);
    VL_SIG8(v__DOT__M_EXC_AdES,0,0);
    VL_SIG8(v__DOT__M_M_CanErr,0,0);
    VL_SIG8(v__DOT__M_KernelMode,0,0);
    VL_SIG8(v__DOT__M_ReverseEndian,0,0);
    VL_SIG8(v__DOT__M_Trap,0,0);
    VL_SIG8(v__DOT__M_TrapCond,0,0);
    VL_SIG8(v__DOT__M_EXC_Tr,0,0);
    VL_SIG8(v__DOT__M_RtRd,4,0);
    VL_SIG8(v__DOT__M_IsBDS,0,0);
    VL_SIG8(v__DOT__M_Left,0,0);
    VL_SIG8(v__DOT__M_Right,0,0);
    VL_SIG8(v__DOT__WB_RegWrite,0,0);
    VL_SIG8(v__DOT__WB_RtRd,4,0);
    VL_SIG8(v__DOT__ID_DP_Hazards,7,0);
    VL_SIG8(v__DOT__HAZ_DP_Hazards,7,0);
    VL_SIG8(v__DOT__WB_MemtoReg,0,0);
    VL_SIG8(v__DOT__Controller__DOT__Unaligned_Mem,0,0);
    VL_SIG8(v__DOT__Controller__DOT__DP_Exceptions,2,0);
    VL_SIG8(v__DOT__HazardControl__DOT__Rs_IDMEM_Match,0,0);
    VL_SIG8(v__DOT__HazardControl__DOT__Rt_IDMEM_Match,0,0);
    VL_SIG8(v__DOT__HazardControl__DOT__Rs_EXMEM_Match,0,0);
    VL_SIG8(v__DOT__HazardControl__DOT__Rt_EXMEM_Match,0,0);
    VL_SIG8(v__DOT__CP0__DOT__EXC_CpU,0,0);
    VL_SIG8(v__DOT__CP0__DOT__EXC_Int,0,0);
    VL_SIG8(v__DOT__CP0__DOT__CP0_WriteCond,0,0);
    VL_SIG8(v__DOT__CP0__DOT__Cause_ExcCode_bits,3,0);
    VL_SIG8(v__DOT__CP0__DOT__reset_r,0,0);
    VL_SIG8(v__DOT__CP0__DOT__Status_CU_0,0,0);
    VL_SIG8(v__DOT__CP0__DOT__Status_RE,0,0);
    VL_SIG8(v__DOT__CP0__DOT__Status_BEV,0,0);
    VL_SIG8(v__DOT__CP0__DOT__Status_NMI,0,0);
    VL_SIG8(v__DOT__CP0__DOT__Status_IM,7,0);
    VL_SIG8(v__DOT__CP0__DOT__Status_UM,0,0);
    VL_SIG8(v__DOT__CP0__DOT__Status_ERL,0,0);
    VL_SIG8(v__DOT__CP0__DOT__Status_EXL,0,0);
    VL_SIG8(v__DOT__CP0__DOT__Status_IE,0,0);
    VL_SIG8(v__DOT__CP0__DOT__Cause_BD,0,0);
    VL_SIG8(v__DOT__CP0__DOT__Cause_CE,1,0);
    VL_SIG8(v__DOT__CP0__DOT__Cause_IV,0,0);
    VL_SIG8(v__DOT__CP0__DOT__Cause_IP,7,0);
    VL_SIG8(v__DOT__CP0__DOT__Cause_ExcCode30,3,0);
    VL_SIG8(v__DOT__CP0__DOT__M_Exception_Detect,0,0);
    VL_SIG8(v__DOT__CP0__DOT__ID_Exception_Detect,0,0);
    VL_SIG8(v__DOT__CP0__DOT__EX_Exception_Mask,0,0);
    VL_SIG8(v__DOT__CP0__DOT__ID_Exception_Mask,0,0);
    VL_SIG8(v__DOT__CP0__DOT__IF_Exception_Mask,0,0);
    VL_SIG8(v__DOT__CP0__DOT__M_Exception_Ready,0,0);
    VL_SIG8(v__DOT__CP0__DOT__EX_Exception_Ready,0,0);
    VL_SIG8(v__DOT__CP0__DOT__ID_Exception_Ready,0,0);
    VL_SIG8(v__DOT__CP0__DOT__IF_Exception_Ready,0,0);
    VL_SIG8(v__DOT__IDEX__DOT__EX_RegDst,0,0);
    VL_SIG8(v__DOT__ALU__DOT__div_fsm,0,0);
    VL_SIG8(v__DOT__ALU__DOT__HILO_Commit,0,0);
    VL_SIG8(v__DOT__ALU__DOT__Divider__DOT__active,0,0);
    VL_SIG8(v__DOT__ALU__DOT__Divider__DOT__neg,0,0);
    VL_SIG8(v__DOT__ALU__DOT__Divider__DOT__cycle,4,0);
    VL_SIG8(v__DOT__DataMem_Controller__DOT__BE,0,0);
    VL_SIG8(v__DOT__DataMem_Controller__DOT__EXC_KernelMem,0,0);
    VL_SIG8(v__DOT__DataMem_Controller__DOT__EXC_Word,0,0);
    VL_SIG8(v__DOT__DataMem_Controller__DOT__EXC_Half,0,0);
    VL_SIG8(v__DOT__DataMem_Controller__DOT__LLSC_Atomic,0,0);
    VL_SIG8(v__DOT__DataMem_Controller__DOT__RW_Mask,0,0);
    VL_SIG8(v__DOT__DataMem_Controller__DOT__Half_Access_L,0,0);
    VL_SIG8(v__DOT__DataMem_Controller__DOT__Half_Access_R,0,0);
    VL_SIG8(v__DOT__DataMem_Controller__DOT__Byte_Access_LL,0,0);
    VL_SIG8(v__DOT__DataMem_Controller__DOT__Byte_Access_LM,0,0);
    VL_SIG8(v__DOT__DataMem_Controller__DOT__Byte_Access_RM,0,0);
    //char	__VpadToAlign167[1];
    VL_SIG16(v__DOT__Controller__DOT__Datapath,15,0);
    //char	__VpadToAlign170[2];
    VL_SIG(v__DOT__Instruction,31,0);
    VL_SIG(v__DOT__IF_PCOut,31,0);
    VL_SIG(v__DOT__ID_PCAdd4,31,0);
    VL_SIG(v__DOT__ID_ReadData1_End,31,0);
    VL_SIG(v__DOT__ID_ReadData2_End,31,0);
    VL_SIG(v__DOT__ID_SignExtImm,29,0);
    VL_SIG(v__DOT__ID_RestartPC,31,0);
    VL_SIG(v__DOT__EX_ReadData1_PR,31,0);
    VL_SIG(v__DOT__EX_ReadData1_Fwd,31,0);
    VL_SIG(v__DOT__EX_ReadData2_PR,31,0);
    VL_SIG(v__DOT__EX_ReadData2_Fwd,31,0);
    VL_SIG(v__DOT__EX_ReadData2_Imm,31,0);
    VL_SIG(v__DOT__EX_SignExtImm,31,0);
    VL_SIG(v__DOT__EX_RestartPC,31,0);
    VL_SIG(v__DOT__M_ALUResult,31,0);
    VL_SIG(v__DOT__M_ReadData2_PR,31,0);
    VL_SIG(v__DOT__M_RestartPC,31,0);
    VL_SIG(v__DOT__M_WriteData_Pre,31,0);
    VL_SIG(v__DOT__WB_ReadData,31,0);
    VL_SIG(v__DOT__WB_ALUResult,31,0);
    VL_SIG(v__DOT__WB_WriteData,31,0);
    VL_SIG(v__DOT__CP0__DOT__BadVAddr,31,0);
    VL_SIG(v__DOT__CP0__DOT__Count,31,0);
    VL_SIG(v__DOT__CP0__DOT__Compare,31,0);
    VL_SIG(v__DOT__CP0__DOT__EPC,31,0);
    VL_SIG(v__DOT__CP0__DOT__ErrorEPC,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers1,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers2,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers3,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers4,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers5,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers6,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers7,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers8,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers9,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers10,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers11,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers12,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers13,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers14,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers15,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers16,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers17,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers18,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers19,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers20,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers21,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers22,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers23,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers24,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers25,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers26,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers27,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers28,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers29,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers30,31,0);
    VL_SIG(v__DOT__RegisterFile__DOT__registers31,31,0);
    VL_SIG(v__DOT__IDEX__DOT__EX_SignExtImm_pre,16,0);
    VL_SIG(v__DOT__ALU__DOT__AddSub_Result,31,0);
    VL_SIG(v__DOT__ALU__DOT__Divider__DOT__result,31,0);
    VL_SIG(v__DOT__ALU__DOT__Divider__DOT__denom,31,0);
    VL_SIG(v__DOT__ALU__DOT__Divider__DOT__work,31,0);
    VL_SIG(v__DOT__DataMem_Controller__DOT__LLSC_Address,29,0);
    VL_SIG64(v__DOT__ALU__DOT__HILO,63,0);
    VL_SIG64(v__DOT__ALU__DOT__Mult_Result,63,0);
    VL_SIG64(v__DOT__ALU__DOT__Multu_Result,63,0);
    VL_SIG64(v__DOT__ALU__DOT__Divider__DOT__sub,32,0);
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    VL_SIG8(__Vclklast__TOP__clock,0,0);
    //char	__VpadToAlign461[3];
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    //char	__VpadToAlign468[4];
    VAdd__Syms*	__VlSymsp;		// Symbol table
    
    // PARAMETERS
    // Parameters marked /*verilator public*/ for use by application code
    
    // CONSTRUCTORS
  private:
    VAdd& operator= (const VAdd&);	///< Copying not allowed
    VAdd(const VAdd&);	///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible WRT DPI scope names.
    VAdd(const char* name="TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~VAdd();
    
    // USER METHODS
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(VAdd__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(VAdd__Syms* symsp, bool first);
  private:
    static IData	_change_request(VAdd__Syms* __restrict vlSymsp);
  public:
    static void	_combo__TOP__12(VAdd__Syms* __restrict vlSymsp);
    static void	_combo__TOP__14(VAdd__Syms* __restrict vlSymsp);
    static void	_combo__TOP__16(VAdd__Syms* __restrict vlSymsp);
    static void	_combo__TOP__18(VAdd__Syms* __restrict vlSymsp);
    static void	_combo__TOP__3(VAdd__Syms* __restrict vlSymsp);
    static void	_combo__TOP__6(VAdd__Syms* __restrict vlSymsp);
    static void	_eval(VAdd__Syms* __restrict vlSymsp);
    static void	_eval_initial(VAdd__Syms* __restrict vlSymsp);
    static void	_eval_settle(VAdd__Syms* __restrict vlSymsp);
    static void	_initial__TOP(VAdd__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__1(VAdd__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__11(VAdd__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__4(VAdd__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__7(VAdd__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__9(VAdd__Syms* __restrict vlSymsp);
    static void	_settle__TOP__10(VAdd__Syms* __restrict vlSymsp);
    static void	_settle__TOP__13(VAdd__Syms* __restrict vlSymsp);
    static void	_settle__TOP__15(VAdd__Syms* __restrict vlSymsp);
    static void	_settle__TOP__17(VAdd__Syms* __restrict vlSymsp);
    static void	_settle__TOP__19(VAdd__Syms* __restrict vlSymsp);
    static void	_settle__TOP__2(VAdd__Syms* __restrict vlSymsp);
    static void	_settle__TOP__5(VAdd__Syms* __restrict vlSymsp);
    static void	_settle__TOP__8(VAdd__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
