// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table internal header
//
// Internal details; most calling programs do not need this header

#ifndef _Vi2c_master_bit_ctrl__Syms_H_
#define _Vi2c_master_bit_ctrl__Syms_H_

#include "verilated.h"

// INCLUDE MODULE CLASSES
#include "Vi2c_master_bit_ctrl.h"

// SYMS CLASS
class Vi2c_master_bit_ctrl__Syms : public VerilatedSyms {
  public:
    
    // LOCAL STATE
    const char* __Vm_namep;
    bool	__Vm_activity;		///< Used by trace routines to determine change occurred
    bool	__Vm_didInit;
    //char	__VpadToAlign10[6];
    
    // SUBCELL STATE
    Vi2c_master_bit_ctrl*          TOPp;
    
    // COVERAGE
    
    // SCOPE NAMES
    
    // CREATORS
    Vi2c_master_bit_ctrl__Syms(Vi2c_master_bit_ctrl* topp, const char* namep);
    ~Vi2c_master_bit_ctrl__Syms() {};
    
    // METHODS
    inline const char* name() { return __Vm_namep; }
    inline bool getClearActivity() { bool r=__Vm_activity; __Vm_activity=false; return r;}
    
} VL_ATTR_ALIGNED(64);

#endif  /*guard*/
