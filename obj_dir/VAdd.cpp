// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VAdd.h for the primary calling header

#include "VAdd.h"              // For This
#include "VAdd__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VAdd) {
    VAdd__Syms* __restrict vlSymsp = __VlSymsp = new VAdd__Syms(this, name());
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    clock = 0;
    reset = 0;
    Interrupts = VL_RAND_RESET_I(5);
    NMI = 0;
    InstMem_Ready = 0;
    DataMem_In = VL_RAND_RESET_I(32);
    DataMem_Ready = 0;
    InstMem_In = VL_RAND_RESET_I(32);
    DataMem_Read = 0;
    DataMem_Write = VL_RAND_RESET_I(4);
    DataMem_Address = VL_RAND_RESET_I(30);
    DataMem_Out = VL_RAND_RESET_I(32);
    InstMem_Address = VL_RAND_RESET_I(30);
    InstMem_Read = 0;
    IP = VL_RAND_RESET_I(8);
    v__DOT__Instruction = VL_RAND_RESET_I(32);
    v__DOT__IF_Stall = VL_RAND_RESET_I(1);
    v__DOT__IF_EXC_AdIF = VL_RAND_RESET_I(1);
    v__DOT__IF_PCOut = VL_RAND_RESET_I(32);
    v__DOT__ID_Stall = VL_RAND_RESET_I(1);
    v__DOT__ID_PCSrc = VL_RAND_RESET_I(2);
    v__DOT__ID_RsFwdSel = VL_RAND_RESET_I(2);
    v__DOT__ID_RtFwdSel = VL_RAND_RESET_I(2);
    v__DOT__ID_Mfc0 = VL_RAND_RESET_I(1);
    v__DOT__ID_Mtc0 = VL_RAND_RESET_I(1);
    v__DOT__ID_Eret = VL_RAND_RESET_I(1);
    v__DOT__ID_NextIsDelay = VL_RAND_RESET_I(1);
    v__DOT__ID_KernelMode = VL_RAND_RESET_I(1);
    v__DOT__ID_EXC_Sys = VL_RAND_RESET_I(1);
    v__DOT__ID_EXC_Bp = VL_RAND_RESET_I(1);
    v__DOT__ID_Exception_Flush = VL_RAND_RESET_I(1);
    v__DOT__ID_PCAdd4 = VL_RAND_RESET_I(32);
    v__DOT__ID_ReadData1_End = VL_RAND_RESET_I(32);
    v__DOT__ID_ReadData2_End = VL_RAND_RESET_I(32);
    v__DOT__ID_CmpEQ = VL_RAND_RESET_I(1);
    v__DOT__ID_SignExtImm = VL_RAND_RESET_I(30);
    v__DOT__ID_RestartPC = VL_RAND_RESET_I(32);
    v__DOT__ID_IsBDS = VL_RAND_RESET_I(1);
    v__DOT__ID_IsFlushed = VL_RAND_RESET_I(1);
    v__DOT__EX_Stall = VL_RAND_RESET_I(1);
    v__DOT__EX_RsFwdSel = VL_RAND_RESET_I(2);
    v__DOT__EX_RtFwdSel = VL_RAND_RESET_I(2);
    v__DOT__EX_Link = VL_RAND_RESET_I(1);
    v__DOT__EX_LinkRegDst = VL_RAND_RESET_I(2);
    v__DOT__EX_ALUSrcImm = VL_RAND_RESET_I(1);
    v__DOT__EX_ALUOp = VL_RAND_RESET_I(5);
    v__DOT__EX_Movn = VL_RAND_RESET_I(1);
    v__DOT__EX_Movz = VL_RAND_RESET_I(1);
    v__DOT__EX_LLSC = VL_RAND_RESET_I(1);
    v__DOT__EX_MemRead = VL_RAND_RESET_I(1);
    v__DOT__EX_MemWrite = VL_RAND_RESET_I(1);
    v__DOT__EX_MemByte = VL_RAND_RESET_I(1);
    v__DOT__EX_MemHalf = VL_RAND_RESET_I(1);
    v__DOT__EX_MemSignExtend = VL_RAND_RESET_I(1);
    v__DOT__EX_RegWrite = VL_RAND_RESET_I(1);
    v__DOT__EX_MemtoReg = VL_RAND_RESET_I(1);
    v__DOT__EX_Rs = VL_RAND_RESET_I(5);
    v__DOT__EX_Rt = VL_RAND_RESET_I(5);
    v__DOT__EX_WantRsByEX = VL_RAND_RESET_I(1);
    v__DOT__EX_NeedRsByEX = VL_RAND_RESET_I(1);
    v__DOT__EX_WantRtByEX = VL_RAND_RESET_I(1);
    v__DOT__EX_NeedRtByEX = VL_RAND_RESET_I(1);
    v__DOT__EX_Trap = VL_RAND_RESET_I(1);
    v__DOT__EX_TrapCond = VL_RAND_RESET_I(1);
    v__DOT__EX_CanErr = VL_RAND_RESET_I(1);
    v__DOT__EX_EX_CanErr = VL_RAND_RESET_I(1);
    v__DOT__EX_M_CanErr = VL_RAND_RESET_I(1);
    v__DOT__EX_KernelMode = VL_RAND_RESET_I(1);
    v__DOT__EX_ReverseEndian = VL_RAND_RESET_I(1);
    v__DOT__EX_Exception_Flush = VL_RAND_RESET_I(1);
    v__DOT__EX_ReadData1_PR = VL_RAND_RESET_I(32);
    v__DOT__EX_ReadData1_Fwd = VL_RAND_RESET_I(32);
    v__DOT__EX_ReadData2_PR = VL_RAND_RESET_I(32);
    v__DOT__EX_ReadData2_Fwd = VL_RAND_RESET_I(32);
    v__DOT__EX_ReadData2_Imm = VL_RAND_RESET_I(32);
    v__DOT__EX_SignExtImm = VL_RAND_RESET_I(32);
    v__DOT__EX_RtRd = VL_RAND_RESET_I(5);
    v__DOT__EX_EXC_Ov = VL_RAND_RESET_I(1);
    v__DOT__EX_RestartPC = VL_RAND_RESET_I(32);
    v__DOT__EX_IsBDS = VL_RAND_RESET_I(1);
    v__DOT__EX_Left = VL_RAND_RESET_I(1);
    v__DOT__EX_Right = VL_RAND_RESET_I(1);
    v__DOT__M_Stall = VL_RAND_RESET_I(1);
    v__DOT__M_Stall_Controller = VL_RAND_RESET_I(1);
    v__DOT__M_LLSC = VL_RAND_RESET_I(1);
    v__DOT__M_MemRead = VL_RAND_RESET_I(1);
    v__DOT__M_MemWrite = VL_RAND_RESET_I(1);
    v__DOT__M_MemByte = VL_RAND_RESET_I(1);
    v__DOT__M_MemHalf = VL_RAND_RESET_I(1);
    v__DOT__M_MemSignExtend = VL_RAND_RESET_I(1);
    v__DOT__M_RegWrite = VL_RAND_RESET_I(1);
    v__DOT__M_MemtoReg = VL_RAND_RESET_I(1);
    v__DOT__M_EXC_AdEL = VL_RAND_RESET_I(1);
    v__DOT__M_EXC_AdES = VL_RAND_RESET_I(1);
    v__DOT__M_M_CanErr = VL_RAND_RESET_I(1);
    v__DOT__M_KernelMode = VL_RAND_RESET_I(1);
    v__DOT__M_ReverseEndian = VL_RAND_RESET_I(1);
    v__DOT__M_Trap = VL_RAND_RESET_I(1);
    v__DOT__M_TrapCond = VL_RAND_RESET_I(1);
    v__DOT__M_EXC_Tr = VL_RAND_RESET_I(1);
    v__DOT__M_ALUResult = VL_RAND_RESET_I(32);
    v__DOT__M_ReadData2_PR = VL_RAND_RESET_I(32);
    v__DOT__M_RtRd = VL_RAND_RESET_I(5);
    v__DOT__M_RestartPC = VL_RAND_RESET_I(32);
    v__DOT__M_IsBDS = VL_RAND_RESET_I(1);
    v__DOT__M_WriteData_Pre = VL_RAND_RESET_I(32);
    v__DOT__M_Left = VL_RAND_RESET_I(1);
    v__DOT__M_Right = VL_RAND_RESET_I(1);
    v__DOT__WB_RegWrite = VL_RAND_RESET_I(1);
    v__DOT__WB_ReadData = VL_RAND_RESET_I(32);
    v__DOT__WB_ALUResult = VL_RAND_RESET_I(32);
    v__DOT__WB_RtRd = VL_RAND_RESET_I(5);
    v__DOT__WB_WriteData = VL_RAND_RESET_I(32);
    v__DOT__ID_DP_Hazards = VL_RAND_RESET_I(8);
    v__DOT__HAZ_DP_Hazards = VL_RAND_RESET_I(8);
    v__DOT__WB_MemtoReg = 0;
    v__DOT__Controller__DOT__Unaligned_Mem = VL_RAND_RESET_I(1);
    v__DOT__Controller__DOT__Datapath = VL_RAND_RESET_I(16);
    v__DOT__Controller__DOT__DP_Exceptions = VL_RAND_RESET_I(3);
    v__DOT__HazardControl__DOT__Rs_IDMEM_Match = VL_RAND_RESET_I(1);
    v__DOT__HazardControl__DOT__Rt_IDMEM_Match = VL_RAND_RESET_I(1);
    v__DOT__HazardControl__DOT__Rs_EXMEM_Match = VL_RAND_RESET_I(1);
    v__DOT__HazardControl__DOT__Rt_EXMEM_Match = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__EXC_CpU = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__EXC_Int = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__CP0_WriteCond = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__Cause_ExcCode_bits = VL_RAND_RESET_I(4);
    v__DOT__CP0__DOT__reset_r = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__BadVAddr = VL_RAND_RESET_I(32);
    v__DOT__CP0__DOT__Count = VL_RAND_RESET_I(32);
    v__DOT__CP0__DOT__Compare = VL_RAND_RESET_I(32);
    v__DOT__CP0__DOT__Status_CU_0 = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__Status_RE = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__Status_BEV = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__Status_NMI = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__Status_IM = VL_RAND_RESET_I(8);
    v__DOT__CP0__DOT__Status_UM = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__Status_ERL = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__Status_EXL = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__Status_IE = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__Cause_BD = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__Cause_CE = VL_RAND_RESET_I(2);
    v__DOT__CP0__DOT__Cause_IV = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__Cause_IP = VL_RAND_RESET_I(8);
    v__DOT__CP0__DOT__Cause_ExcCode30 = VL_RAND_RESET_I(4);
    v__DOT__CP0__DOT__EPC = VL_RAND_RESET_I(32);
    v__DOT__CP0__DOT__ErrorEPC = VL_RAND_RESET_I(32);
    v__DOT__CP0__DOT__M_Exception_Detect = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__ID_Exception_Detect = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__EX_Exception_Mask = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__ID_Exception_Mask = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__IF_Exception_Mask = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__M_Exception_Ready = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__EX_Exception_Ready = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__ID_Exception_Ready = VL_RAND_RESET_I(1);
    v__DOT__CP0__DOT__IF_Exception_Ready = VL_RAND_RESET_I(1);
    v__DOT__RegisterFile__DOT__registers1 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers2 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers3 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers4 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers5 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers6 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers7 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers8 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers9 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers10 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers11 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers12 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers13 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers14 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers15 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers16 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers17 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers18 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers19 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers20 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers21 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers22 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers23 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers24 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers25 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers26 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers27 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers28 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers29 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers30 = VL_RAND_RESET_I(32);
    v__DOT__RegisterFile__DOT__registers31 = VL_RAND_RESET_I(32);
    v__DOT__IDEX__DOT__EX_SignExtImm_pre = VL_RAND_RESET_I(17);
    v__DOT__IDEX__DOT__EX_RegDst = VL_RAND_RESET_I(1);
    v__DOT__ALU__DOT__HILO = VL_RAND_RESET_Q(64);
    v__DOT__ALU__DOT__div_fsm = VL_RAND_RESET_I(1);
    v__DOT__ALU__DOT__HILO_Commit = VL_RAND_RESET_I(1);
    v__DOT__ALU__DOT__AddSub_Result = VL_RAND_RESET_I(32);
    v__DOT__ALU__DOT__Mult_Result = VL_RAND_RESET_Q(64);
    v__DOT__ALU__DOT__Multu_Result = VL_RAND_RESET_Q(64);
    v__DOT__ALU__DOT__Divider__DOT__active = VL_RAND_RESET_I(1);
    v__DOT__ALU__DOT__Divider__DOT__neg = VL_RAND_RESET_I(1);
    v__DOT__ALU__DOT__Divider__DOT__cycle = VL_RAND_RESET_I(5);
    v__DOT__ALU__DOT__Divider__DOT__result = VL_RAND_RESET_I(32);
    v__DOT__ALU__DOT__Divider__DOT__denom = VL_RAND_RESET_I(32);
    v__DOT__ALU__DOT__Divider__DOT__work = VL_RAND_RESET_I(32);
    v__DOT__ALU__DOT__Divider__DOT__sub = VL_RAND_RESET_Q(33);
    v__DOT__DataMem_Controller__DOT__BE = VL_RAND_RESET_I(1);
    v__DOT__DataMem_Controller__DOT__EXC_KernelMem = VL_RAND_RESET_I(1);
    v__DOT__DataMem_Controller__DOT__EXC_Word = VL_RAND_RESET_I(1);
    v__DOT__DataMem_Controller__DOT__EXC_Half = VL_RAND_RESET_I(1);
    v__DOT__DataMem_Controller__DOT__LLSC_Address = VL_RAND_RESET_I(30);
    v__DOT__DataMem_Controller__DOT__LLSC_Atomic = VL_RAND_RESET_I(1);
    v__DOT__DataMem_Controller__DOT__RW_Mask = VL_RAND_RESET_I(1);
    v__DOT__DataMem_Controller__DOT__Half_Access_L = VL_RAND_RESET_I(1);
    v__DOT__DataMem_Controller__DOT__Half_Access_R = VL_RAND_RESET_I(1);
    v__DOT__DataMem_Controller__DOT__Byte_Access_LL = VL_RAND_RESET_I(1);
    v__DOT__DataMem_Controller__DOT__Byte_Access_LM = VL_RAND_RESET_I(1);
    v__DOT__DataMem_Controller__DOT__Byte_Access_RM = VL_RAND_RESET_I(1);
    __Vclklast__TOP__clock = 0;
}

void VAdd::__Vconfigure(VAdd__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VAdd::~VAdd() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void VAdd::eval() {
    VAdd__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate VAdd::eval\n"); );
    int __VclockLoop = 0;
    IData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void VAdd::_eval_initial_loop(VAdd__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    IData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

void VAdd::_initial__TOP(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_initial__TOP\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // INITIAL at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Processor.v:674
    vlTOPp->InstMem_Read = 0;
    // INITIAL at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Register.v:26
    vlTOPp->v__DOT__IF_PCOut = 0x10;
}

void VAdd::_sequent__TOP__1(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_sequent__TOP__1\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    VL_SIG8(__Vdly__v__DOT__CP0__DOT__Status_BEV,0,0);
    VL_SIG8(__Vdly__v__DOT__CP0__DOT__Status_NMI,0,0);
    VL_SIG8(__Vdly__v__DOT__CP0__DOT__Status_ERL,0,0);
    VL_SIG8(__Vdly__v__DOT__CP0__DOT__Status_CU_0,0,0);
    VL_SIG8(__Vdly__v__DOT__CP0__DOT__Status_RE,0,0);
    VL_SIG8(__Vdly__v__DOT__CP0__DOT__Status_IM,7,0);
    VL_SIG8(__Vdly__v__DOT__CP0__DOT__Status_UM,0,0);
    VL_SIG8(__Vdly__v__DOT__CP0__DOT__Status_IE,0,0);
    VL_SIG8(__Vdly__v__DOT__CP0__DOT__Cause_IV,0,0);
    VL_SIG8(__Vdly__v__DOT__CP0__DOT__Cause_IP,7,0);
    VL_SIG8(__Vdly__v__DOT__CP0__DOT__Cause_BD,0,0);
    VL_SIG8(__Vdly__v__DOT__CP0__DOT__Cause_CE,1,0);
    VL_SIG8(__Vdly__v__DOT__CP0__DOT__Cause_ExcCode30,3,0);
    VL_SIG8(__Vdly__v__DOT__CP0__DOT__Status_EXL,0,0);
    VL_SIG8(__Vdly__v__DOT__ID_IsBDS,0,0);
    VL_SIG8(__Vdly__v__DOT__ID_IsFlushed,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_Link,0,0);
    VL_SIG8(__Vdly__v__DOT__IDEX__DOT__EX_RegDst,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_ALUSrcImm,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_ALUOp,4,0);
    VL_SIG8(__Vdly__v__DOT__EX_Movn,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_Movz,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_LLSC,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_MemRead,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_MemWrite,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_MemByte,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_MemHalf,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_MemSignExtend,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_Left,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_Right,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_RegWrite,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_MemtoReg,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_ReverseEndian,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_IsBDS,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_Trap,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_TrapCond,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_EX_CanErr,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_M_CanErr,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_Rs,4,0);
    VL_SIG8(__Vdly__v__DOT__EX_Rt,4,0);
    VL_SIG8(__Vdly__v__DOT__EX_WantRsByEX,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_NeedRsByEX,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_WantRtByEX,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_NeedRtByEX,0,0);
    VL_SIG8(__Vdly__v__DOT__EX_KernelMode,0,0);
    VL_SIG8(__Vdly__v__DOT__ALU__DOT__div_fsm,0,0);
    VL_SIG8(__Vdly__v__DOT__ALU__DOT__Divider__DOT__active,0,0);
    VL_SIG8(__Vdly__v__DOT__ALU__DOT__Divider__DOT__cycle,4,0);
    VL_SIG8(__Vdly__v__DOT__M_RegWrite,0,0);
    VL_SIG8(__Vdly__v__DOT__M_MemtoReg,0,0);
    VL_SIG8(__Vdly__v__DOT__M_ReverseEndian,0,0);
    VL_SIG8(__Vdly__v__DOT__M_LLSC,0,0);
    VL_SIG8(__Vdly__v__DOT__M_MemRead,0,0);
    VL_SIG8(__Vdly__v__DOT__M_MemWrite,0,0);
    VL_SIG8(__Vdly__v__DOT__M_MemByte,0,0);
    VL_SIG8(__Vdly__v__DOT__M_MemHalf,0,0);
    VL_SIG8(__Vdly__v__DOT__M_MemSignExtend,0,0);
    VL_SIG8(__Vdly__v__DOT__M_Left,0,0);
    VL_SIG8(__Vdly__v__DOT__M_Right,0,0);
    VL_SIG8(__Vdly__v__DOT__M_KernelMode,0,0);
    VL_SIG8(__Vdly__v__DOT__M_IsBDS,0,0);
    VL_SIG8(__Vdly__v__DOT__M_Trap,0,0);
    VL_SIG8(__Vdly__v__DOT__M_TrapCond,0,0);
    VL_SIG8(__Vdly__v__DOT__M_M_CanErr,0,0);
    VL_SIG8(__Vdly__v__DOT__M_RtRd,4,0);
    VL_SIG8(__Vdly__v__DOT__DataMem_Controller__DOT__LLSC_Atomic,0,0);
    VL_SIG8(__Vdly__v__DOT__DataMem_Controller__DOT__RW_Mask,0,0);
    VL_SIG8(__Vdly__v__DOT__WB_RegWrite,0,0);
    VL_SIG8(__Vdly__v__DOT__WB_MemtoReg,0,0);
    VL_SIG8(__Vdly__v__DOT__WB_RtRd,4,0);
    //char	__VpadToAlign94[2];
    VL_SIG(__Vdly__v__DOT__CP0__DOT__ErrorEPC,31,0);
    VL_SIG(__Vdly__v__DOT__CP0__DOT__Count,31,0);
    VL_SIG(__Vdly__v__DOT__CP0__DOT__Compare,31,0);
    VL_SIG(__Vdly__v__DOT__CP0__DOT__EPC,31,0);
    VL_SIG(__Vdly__v__DOT__CP0__DOT__BadVAddr,31,0);
    VL_SIG(__Vdly__v__DOT__IF_PCOut,31,0);
    VL_SIG(__Vdly__v__DOT__Instruction,31,0);
    VL_SIG(__Vdly__v__DOT__ID_PCAdd4,31,0);
    VL_SIG(__Vdly__v__DOT__ID_RestartPC,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers1,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers2,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers3,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers4,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers5,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers6,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers7,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers8,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers9,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers10,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers11,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers12,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers13,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers14,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers15,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers16,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers17,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers18,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers19,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers20,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers21,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers22,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers23,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers24,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers25,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers26,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers27,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers28,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers29,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers30,31,0);
    VL_SIG(__Vdly__v__DOT__RegisterFile__DOT__registers31,31,0);
    VL_SIG(__Vdly__v__DOT__EX_RestartPC,31,0);
    VL_SIG(__Vdly__v__DOT__EX_ReadData1_PR,31,0);
    VL_SIG(__Vdly__v__DOT__EX_ReadData2_PR,31,0);
    VL_SIG(__Vdly__v__DOT__IDEX__DOT__EX_SignExtImm_pre,16,0);
    VL_SIG(__Vdly__v__DOT__ALU__DOT__Divider__DOT__result,31,0);
    VL_SIG(__Vdly__v__DOT__ALU__DOT__Divider__DOT__work,31,0);
    VL_SIG(__Vdly__v__DOT__M_RestartPC,31,0);
    VL_SIG(__Vdly__v__DOT__M_ALUResult,31,0);
    VL_SIG(__Vdly__v__DOT__M_ReadData2_PR,31,0);
    VL_SIG(__Vdly__v__DOT__DataMem_Controller__DOT__LLSC_Address,29,0);
    VL_SIG(__Vdly__v__DOT__WB_ReadData,31,0);
    VL_SIG(__Vdly__v__DOT__WB_ALUResult,31,0);
    VL_SIG64(__Vdly__v__DOT__ALU__DOT__HILO,63,0);
    // Body
    __Vdly__v__DOT__ALU__DOT__div_fsm = vlTOPp->v__DOT__ALU__DOT__div_fsm;
    __Vdly__v__DOT__ALU__DOT__Divider__DOT__cycle = vlTOPp->v__DOT__ALU__DOT__Divider__DOT__cycle;
    __Vdly__v__DOT__ALU__DOT__Divider__DOT__active 
	= vlTOPp->v__DOT__ALU__DOT__Divider__DOT__active;
    __Vdly__v__DOT__ALU__DOT__Divider__DOT__work = vlTOPp->v__DOT__ALU__DOT__Divider__DOT__work;
    __Vdly__v__DOT__ALU__DOT__Divider__DOT__result 
	= vlTOPp->v__DOT__ALU__DOT__Divider__DOT__result;
    __Vdly__v__DOT__ALU__DOT__HILO = vlTOPp->v__DOT__ALU__DOT__HILO;
    __Vdly__v__DOT__DataMem_Controller__DOT__RW_Mask 
	= vlTOPp->v__DOT__DataMem_Controller__DOT__RW_Mask;
    __Vdly__v__DOT__DataMem_Controller__DOT__LLSC_Address 
	= vlTOPp->v__DOT__DataMem_Controller__DOT__LLSC_Address;
    __Vdly__v__DOT__RegisterFile__DOT__registers19 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers19;
    __Vdly__v__DOT__RegisterFile__DOT__registers18 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers18;
    __Vdly__v__DOT__RegisterFile__DOT__registers17 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers17;
    __Vdly__v__DOT__RegisterFile__DOT__registers16 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers16;
    __Vdly__v__DOT__RegisterFile__DOT__registers15 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers15;
    __Vdly__v__DOT__RegisterFile__DOT__registers14 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers14;
    __Vdly__v__DOT__RegisterFile__DOT__registers13 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers13;
    __Vdly__v__DOT__RegisterFile__DOT__registers12 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers12;
    __Vdly__v__DOT__RegisterFile__DOT__registers11 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers11;
    __Vdly__v__DOT__RegisterFile__DOT__registers10 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers10;
    __Vdly__v__DOT__RegisterFile__DOT__registers1 = vlTOPp->v__DOT__RegisterFile__DOT__registers1;
    __Vdly__v__DOT__RegisterFile__DOT__registers9 = vlTOPp->v__DOT__RegisterFile__DOT__registers9;
    __Vdly__v__DOT__RegisterFile__DOT__registers2 = vlTOPp->v__DOT__RegisterFile__DOT__registers2;
    __Vdly__v__DOT__RegisterFile__DOT__registers8 = vlTOPp->v__DOT__RegisterFile__DOT__registers8;
    __Vdly__v__DOT__RegisterFile__DOT__registers3 = vlTOPp->v__DOT__RegisterFile__DOT__registers3;
    __Vdly__v__DOT__RegisterFile__DOT__registers7 = vlTOPp->v__DOT__RegisterFile__DOT__registers7;
    __Vdly__v__DOT__RegisterFile__DOT__registers4 = vlTOPp->v__DOT__RegisterFile__DOT__registers4;
    __Vdly__v__DOT__RegisterFile__DOT__registers6 = vlTOPp->v__DOT__RegisterFile__DOT__registers6;
    __Vdly__v__DOT__RegisterFile__DOT__registers5 = vlTOPp->v__DOT__RegisterFile__DOT__registers5;
    __Vdly__v__DOT__RegisterFile__DOT__registers20 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers20;
    __Vdly__v__DOT__RegisterFile__DOT__registers31 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers31;
    __Vdly__v__DOT__RegisterFile__DOT__registers30 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers30;
    __Vdly__v__DOT__RegisterFile__DOT__registers29 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers29;
    __Vdly__v__DOT__RegisterFile__DOT__registers28 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers28;
    __Vdly__v__DOT__RegisterFile__DOT__registers27 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers27;
    __Vdly__v__DOT__RegisterFile__DOT__registers26 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers26;
    __Vdly__v__DOT__RegisterFile__DOT__registers25 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers25;
    __Vdly__v__DOT__RegisterFile__DOT__registers24 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers24;
    __Vdly__v__DOT__RegisterFile__DOT__registers23 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers23;
    __Vdly__v__DOT__RegisterFile__DOT__registers22 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers22;
    __Vdly__v__DOT__RegisterFile__DOT__registers21 
	= vlTOPp->v__DOT__RegisterFile__DOT__registers21;
    __Vdly__v__DOT__DataMem_Controller__DOT__LLSC_Atomic 
	= vlTOPp->v__DOT__DataMem_Controller__DOT__LLSC_Atomic;
    __Vdly__v__DOT__CP0__DOT__ErrorEPC = vlTOPp->v__DOT__CP0__DOT__ErrorEPC;
    __Vdly__v__DOT__CP0__DOT__Status_NMI = vlTOPp->v__DOT__CP0__DOT__Status_NMI;
    __Vdly__v__DOT__CP0__DOT__Status_BEV = vlTOPp->v__DOT__CP0__DOT__Status_BEV;
    __Vdly__v__DOT__CP0__DOT__Status_ERL = vlTOPp->v__DOT__CP0__DOT__Status_ERL;
    __Vdly__v__DOT__IF_PCOut = vlTOPp->v__DOT__IF_PCOut;
    __Vdly__v__DOT__CP0__DOT__Status_RE = vlTOPp->v__DOT__CP0__DOT__Status_RE;
    __Vdly__v__DOT__CP0__DOT__Compare = vlTOPp->v__DOT__CP0__DOT__Compare;
    __Vdly__v__DOT__CP0__DOT__Count = vlTOPp->v__DOT__CP0__DOT__Count;
    __Vdly__v__DOT__CP0__DOT__Cause_IV = vlTOPp->v__DOT__CP0__DOT__Cause_IV;
    __Vdly__v__DOT__CP0__DOT__Status_CU_0 = vlTOPp->v__DOT__CP0__DOT__Status_CU_0;
    __Vdly__v__DOT__CP0__DOT__Status_IM = vlTOPp->v__DOT__CP0__DOT__Status_IM;
    __Vdly__v__DOT__CP0__DOT__Status_IE = vlTOPp->v__DOT__CP0__DOT__Status_IE;
    __Vdly__v__DOT__CP0__DOT__Cause_IP = vlTOPp->v__DOT__CP0__DOT__Cause_IP;
    __Vdly__v__DOT__CP0__DOT__Status_UM = vlTOPp->v__DOT__CP0__DOT__Status_UM;
    __Vdly__v__DOT__CP0__DOT__Cause_ExcCode30 = vlTOPp->v__DOT__CP0__DOT__Cause_ExcCode30;
    __Vdly__v__DOT__CP0__DOT__Cause_BD = vlTOPp->v__DOT__CP0__DOT__Cause_BD;
    __Vdly__v__DOT__CP0__DOT__EPC = vlTOPp->v__DOT__CP0__DOT__EPC;
    __Vdly__v__DOT__CP0__DOT__Cause_CE = vlTOPp->v__DOT__CP0__DOT__Cause_CE;
    __Vdly__v__DOT__CP0__DOT__BadVAddr = vlTOPp->v__DOT__CP0__DOT__BadVAddr;
    __Vdly__v__DOT__CP0__DOT__Status_EXL = vlTOPp->v__DOT__CP0__DOT__Status_EXL;
    __Vdly__v__DOT__WB_RtRd = vlTOPp->v__DOT__WB_RtRd;
    __Vdly__v__DOT__WB_RegWrite = vlTOPp->v__DOT__WB_RegWrite;
    __Vdly__v__DOT__WB_ALUResult = vlTOPp->v__DOT__WB_ALUResult;
    __Vdly__v__DOT__WB_ReadData = vlTOPp->v__DOT__WB_ReadData;
    __Vdly__v__DOT__WB_MemtoReg = vlTOPp->v__DOT__WB_MemtoReg;
    __Vdly__v__DOT__M_IsBDS = vlTOPp->v__DOT__M_IsBDS;
    __Vdly__v__DOT__M_RestartPC = vlTOPp->v__DOT__M_RestartPC;
    __Vdly__v__DOT__M_MemtoReg = vlTOPp->v__DOT__M_MemtoReg;
    __Vdly__v__DOT__M_MemSignExtend = vlTOPp->v__DOT__M_MemSignExtend;
    __Vdly__v__DOT__M_ReadData2_PR = vlTOPp->v__DOT__M_ReadData2_PR;
    __Vdly__v__DOT__M_LLSC = vlTOPp->v__DOT__M_LLSC;
    __Vdly__v__DOT__M_M_CanErr = vlTOPp->v__DOT__M_M_CanErr;
    __Vdly__v__DOT__M_TrapCond = vlTOPp->v__DOT__M_TrapCond;
    __Vdly__v__DOT__M_Trap = vlTOPp->v__DOT__M_Trap;
    __Vdly__v__DOT__M_ReverseEndian = vlTOPp->v__DOT__M_ReverseEndian;
    __Vdly__v__DOT__M_Left = vlTOPp->v__DOT__M_Left;
    __Vdly__v__DOT__M_Right = vlTOPp->v__DOT__M_Right;
    __Vdly__v__DOT__M_MemByte = vlTOPp->v__DOT__M_MemByte;
    __Vdly__v__DOT__M_KernelMode = vlTOPp->v__DOT__M_KernelMode;
    __Vdly__v__DOT__M_MemHalf = vlTOPp->v__DOT__M_MemHalf;
    __Vdly__v__DOT__M_MemRead = vlTOPp->v__DOT__M_MemRead;
    __Vdly__v__DOT__M_MemWrite = vlTOPp->v__DOT__M_MemWrite;
    __Vdly__v__DOT__M_ALUResult = vlTOPp->v__DOT__M_ALUResult;
    __Vdly__v__DOT__ID_PCAdd4 = vlTOPp->v__DOT__ID_PCAdd4;
    __Vdly__v__DOT__ID_IsBDS = vlTOPp->v__DOT__ID_IsBDS;
    __Vdly__v__DOT__ID_RestartPC = vlTOPp->v__DOT__ID_RestartPC;
    __Vdly__v__DOT__M_RegWrite = vlTOPp->v__DOT__M_RegWrite;
    __Vdly__v__DOT__M_RtRd = vlTOPp->v__DOT__M_RtRd;
    __Vdly__v__DOT__ID_IsFlushed = vlTOPp->v__DOT__ID_IsFlushed;
    __Vdly__v__DOT__Instruction = vlTOPp->v__DOT__Instruction;
    __Vdly__v__DOT__EX_Right = vlTOPp->v__DOT__EX_Right;
    __Vdly__v__DOT__EX_Movn = vlTOPp->v__DOT__EX_Movn;
    __Vdly__v__DOT__EX_MemWrite = vlTOPp->v__DOT__EX_MemWrite;
    __Vdly__v__DOT__EX_KernelMode = vlTOPp->v__DOT__EX_KernelMode;
    __Vdly__v__DOT__EX_MemByte = vlTOPp->v__DOT__EX_MemByte;
    __Vdly__v__DOT__EX_Movz = vlTOPp->v__DOT__EX_Movz;
    __Vdly__v__DOT__EX_MemHalf = vlTOPp->v__DOT__EX_MemHalf;
    __Vdly__v__DOT__EX_MemSignExtend = vlTOPp->v__DOT__EX_MemSignExtend;
    __Vdly__v__DOT__EX_Left = vlTOPp->v__DOT__EX_Left;
    __Vdly__v__DOT__EX_LLSC = vlTOPp->v__DOT__EX_LLSC;
    __Vdly__v__DOT__EX_MemRead = vlTOPp->v__DOT__EX_MemRead;
    __Vdly__v__DOT__EX_TrapCond = vlTOPp->v__DOT__EX_TrapCond;
    __Vdly__v__DOT__EX_MemtoReg = vlTOPp->v__DOT__EX_MemtoReg;
    __Vdly__v__DOT__EX_Trap = vlTOPp->v__DOT__EX_Trap;
    __Vdly__v__DOT__EX_ReverseEndian = vlTOPp->v__DOT__EX_ReverseEndian;
    __Vdly__v__DOT__EX_IsBDS = vlTOPp->v__DOT__EX_IsBDS;
    __Vdly__v__DOT__EX_RegWrite = vlTOPp->v__DOT__EX_RegWrite;
    __Vdly__v__DOT__IDEX__DOT__EX_RegDst = vlTOPp->v__DOT__IDEX__DOT__EX_RegDst;
    __Vdly__v__DOT__EX_EX_CanErr = vlTOPp->v__DOT__EX_EX_CanErr;
    __Vdly__v__DOT__EX_M_CanErr = vlTOPp->v__DOT__EX_M_CanErr;
    __Vdly__v__DOT__EX_ALUOp = vlTOPp->v__DOT__EX_ALUOp;
    __Vdly__v__DOT__EX_ALUSrcImm = vlTOPp->v__DOT__EX_ALUSrcImm;
    __Vdly__v__DOT__EX_RestartPC = vlTOPp->v__DOT__EX_RestartPC;
    __Vdly__v__DOT__EX_ReadData1_PR = vlTOPp->v__DOT__EX_ReadData1_PR;
    __Vdly__v__DOT__EX_ReadData2_PR = vlTOPp->v__DOT__EX_ReadData2_PR;
    __Vdly__v__DOT__IDEX__DOT__EX_SignExtImm_pre = vlTOPp->v__DOT__IDEX__DOT__EX_SignExtImm_pre;
    __Vdly__v__DOT__EX_Link = vlTOPp->v__DOT__EX_Link;
    __Vdly__v__DOT__EX_Rs = vlTOPp->v__DOT__EX_Rs;
    __Vdly__v__DOT__EX_Rt = vlTOPp->v__DOT__EX_Rt;
    __Vdly__v__DOT__EX_NeedRsByEX = vlTOPp->v__DOT__EX_NeedRsByEX;
    __Vdly__v__DOT__EX_WantRtByEX = vlTOPp->v__DOT__EX_WantRtByEX;
    __Vdly__v__DOT__EX_NeedRtByEX = vlTOPp->v__DOT__EX_NeedRtByEX;
    __Vdly__v__DOT__EX_WantRsByEX = vlTOPp->v__DOT__EX_WantRsByEX;
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/ALU.v:673
    if (vlTOPp->reset) {
	__Vdly__v__DOT__ALU__DOT__div_fsm = 0;
    } else {
	if (vlTOPp->v__DOT__ALU__DOT__div_fsm) {
	    if (vlTOPp->v__DOT__ALU__DOT__div_fsm) {
		__Vdly__v__DOT__ALU__DOT__div_fsm = vlTOPp->v__DOT__ALU__DOT__Divider__DOT__active;
	    }
	} else {
	    __Vdly__v__DOT__ALU__DOT__div_fsm = (((5 
						   == (IData)(vlTOPp->v__DOT__EX_ALUOp)) 
						  | (6 
						     == (IData)(vlTOPp->v__DOT__EX_ALUOp))) 
						 & (IData)(vlTOPp->v__DOT__ALU__DOT__HILO_Commit));
	}
    }
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/ALU.v:624
    __Vdly__v__DOT__ALU__DOT__HILO = ((IData)(vlTOPp->reset)
				       ? VL_ULL(0) : 
				      (((IData)(vlTOPp->v__DOT__ALU__DOT__div_fsm) 
					& (~ (IData)(vlTOPp->v__DOT__ALU__DOT__Divider__DOT__active)))
				        ? (((QData)((IData)(vlTOPp->v__DOT__ALU__DOT__Divider__DOT__work)) 
					    << 0x20) 
					   | (QData)((IData)(
							     ((IData)(vlTOPp->v__DOT__ALU__DOT__Divider__DOT__neg)
							       ? 
							      VL_NEGATE_I(vlTOPp->v__DOT__ALU__DOT__Divider__DOT__result)
							       : vlTOPp->v__DOT__ALU__DOT__Divider__DOT__result))))
				        : ((IData)(vlTOPp->v__DOT__ALU__DOT__HILO_Commit)
					    ? ((0x10 
						& (IData)(vlTOPp->v__DOT__EX_ALUOp))
					        ? (
						   (8 
						    & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						    ? vlTOPp->v__DOT__ALU__DOT__HILO
						    : 
						   ((4 
						     & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						     ? vlTOPp->v__DOT__ALU__DOT__HILO
						     : 
						    ((2 
						      & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						      ? vlTOPp->v__DOT__ALU__DOT__HILO
						      : 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? vlTOPp->v__DOT__ALU__DOT__Multu_Result
						       : vlTOPp->v__DOT__ALU__DOT__Mult_Result))))
					        : (
						   (8 
						    & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						    ? 
						   ((4 
						     & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						     ? 
						    ((2 
						      & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						      ? 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? vlTOPp->v__DOT__ALU__DOT__HILO
						       : 
						      (vlTOPp->v__DOT__ALU__DOT__HILO 
						       - vlTOPp->v__DOT__ALU__DOT__Multu_Result))
						      : 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? 
						      (vlTOPp->v__DOT__ALU__DOT__HILO 
						       - vlTOPp->v__DOT__ALU__DOT__Mult_Result)
						       : 
						      (((QData)((IData)(
									(vlTOPp->v__DOT__ALU__DOT__HILO 
									 >> 0x20))) 
							<< 0x20) 
						       | (QData)((IData)(vlTOPp->v__DOT__EX_ReadData2_Imm)))))
						     : 
						    ((2 
						      & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						      ? 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? 
						      (((QData)((IData)(vlTOPp->v__DOT__EX_ReadData1_Fwd)) 
							<< 0x20) 
						       | (QData)((IData)(vlTOPp->v__DOT__ALU__DOT__HILO)))
						       : vlTOPp->v__DOT__ALU__DOT__HILO)
						      : 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? vlTOPp->v__DOT__ALU__DOT__HILO
						       : 
						      (vlTOPp->v__DOT__ALU__DOT__HILO 
						       + vlTOPp->v__DOT__ALU__DOT__Multu_Result))))
						    : 
						   ((4 
						     & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						     ? 
						    ((2 
						      & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						      ? 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? 
						      (vlTOPp->v__DOT__ALU__DOT__HILO 
						       + vlTOPp->v__DOT__ALU__DOT__Mult_Result)
						       : vlTOPp->v__DOT__ALU__DOT__HILO)
						      : vlTOPp->v__DOT__ALU__DOT__HILO)
						     : vlTOPp->v__DOT__ALU__DOT__HILO)))
					    : vlTOPp->v__DOT__ALU__DOT__HILO)));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/MemControl.v:621
    __Vdly__v__DOT__DataMem_Controller__DOT__RW_Mask 
	= (1 & ((IData)(vlTOPp->reset) ? 0 : ((((IData)(vlTOPp->v__DOT__M_MemWrite) 
						| (IData)(vlTOPp->v__DOT__M_MemRead)) 
					       & (IData)(vlTOPp->DataMem_Ready))
					       ? 1 : 
					      ((1 & 
						((~ (IData)(vlTOPp->v__DOT__M_Stall_Controller)) 
						 & (~ (IData)(vlTOPp->v__DOT__IF_Stall))))
					        ? 0
					        : (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__RW_Mask)))));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/MemControl.v:596
    __Vdly__v__DOT__DataMem_Controller__DOT__LLSC_Address 
	= (0x3fffffff & ((IData)(vlTOPp->reset) ? 0
			  : (((IData)(vlTOPp->v__DOT__M_MemRead) 
			      & (IData)(vlTOPp->v__DOT__M_LLSC))
			      ? (vlTOPp->v__DOT__M_ALUResult 
				 >> 2) : vlTOPp->v__DOT__DataMem_Controller__DOT__LLSC_Address)));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/RegisterFile.v:76
    if (vlTOPp->reset) {
	__Vdly__v__DOT__RegisterFile__DOT__registers1 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers2 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers3 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers4 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers5 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers6 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers7 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers8 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers9 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers10 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers11 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers12 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers13 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers14 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers15 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers16 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers17 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers18 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers19 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers20 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers21 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers22 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers23 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers24 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers25 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers26 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers27 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers28 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers29 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers30 = 0;
	__Vdly__v__DOT__RegisterFile__DOT__registers31 = 0;
    } else {
	if ((0 != (IData)(vlTOPp->v__DOT__WB_RtRd))) {
	    if (((((((((1 == (IData)(vlTOPp->v__DOT__WB_RtRd)) 
		       | (2 == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
		      | (3 == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
		     | (4 == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
		    | (5 == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
		   | (6 == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
		  | (7 == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
		 | (8 == (IData)(vlTOPp->v__DOT__WB_RtRd)))) {
		if ((1 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
		    __Vdly__v__DOT__RegisterFile__DOT__registers1 
			= ((IData)(vlTOPp->v__DOT__WB_RegWrite)
			    ? vlTOPp->v__DOT__WB_WriteData
			    : vlTOPp->v__DOT__RegisterFile__DOT__registers1);
		} else {
		    if ((2 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
			__Vdly__v__DOT__RegisterFile__DOT__registers2 
			    = ((IData)(vlTOPp->v__DOT__WB_RegWrite)
			        ? vlTOPp->v__DOT__WB_WriteData
			        : vlTOPp->v__DOT__RegisterFile__DOT__registers2);
		    } else {
			if ((3 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
			    __Vdly__v__DOT__RegisterFile__DOT__registers3 
				= ((IData)(vlTOPp->v__DOT__WB_RegWrite)
				    ? vlTOPp->v__DOT__WB_WriteData
				    : vlTOPp->v__DOT__RegisterFile__DOT__registers3);
			} else {
			    if ((4 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
				__Vdly__v__DOT__RegisterFile__DOT__registers4 
				    = ((IData)(vlTOPp->v__DOT__WB_RegWrite)
				        ? vlTOPp->v__DOT__WB_WriteData
				        : vlTOPp->v__DOT__RegisterFile__DOT__registers4);
			    } else {
				if ((5 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
				    __Vdly__v__DOT__RegisterFile__DOT__registers5 
					= ((IData)(vlTOPp->v__DOT__WB_RegWrite)
					    ? vlTOPp->v__DOT__WB_WriteData
					    : vlTOPp->v__DOT__RegisterFile__DOT__registers5);
				} else {
				    if ((6 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
					__Vdly__v__DOT__RegisterFile__DOT__registers6 
					    = ((IData)(vlTOPp->v__DOT__WB_RegWrite)
					        ? vlTOPp->v__DOT__WB_WriteData
					        : vlTOPp->v__DOT__RegisterFile__DOT__registers6);
				    } else {
					if ((7 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
					    __Vdly__v__DOT__RegisterFile__DOT__registers7 
						= ((IData)(vlTOPp->v__DOT__WB_RegWrite)
						    ? vlTOPp->v__DOT__WB_WriteData
						    : vlTOPp->v__DOT__RegisterFile__DOT__registers7);
					} else {
					    __Vdly__v__DOT__RegisterFile__DOT__registers8 
						= ((IData)(vlTOPp->v__DOT__WB_RegWrite)
						    ? vlTOPp->v__DOT__WB_WriteData
						    : vlTOPp->v__DOT__RegisterFile__DOT__registers8);
					}
				    }
				}
			    }
			}
		    }
		}
	    } else {
		if (((((((((9 == (IData)(vlTOPp->v__DOT__WB_RtRd)) 
			   | (0xa == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
			  | (0xb == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
			 | (0xc == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
			| (0xd == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
		       | (0xe == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
		      | (0xf == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
		     | (0x10 == (IData)(vlTOPp->v__DOT__WB_RtRd)))) {
		    if ((9 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
			__Vdly__v__DOT__RegisterFile__DOT__registers9 
			    = ((IData)(vlTOPp->v__DOT__WB_RegWrite)
			        ? vlTOPp->v__DOT__WB_WriteData
			        : vlTOPp->v__DOT__RegisterFile__DOT__registers9);
		    } else {
			if ((0xa == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
			    __Vdly__v__DOT__RegisterFile__DOT__registers10 
				= ((IData)(vlTOPp->v__DOT__WB_RegWrite)
				    ? vlTOPp->v__DOT__WB_WriteData
				    : vlTOPp->v__DOT__RegisterFile__DOT__registers10);
			} else {
			    if ((0xb == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
				__Vdly__v__DOT__RegisterFile__DOT__registers11 
				    = ((IData)(vlTOPp->v__DOT__WB_RegWrite)
				        ? vlTOPp->v__DOT__WB_WriteData
				        : vlTOPp->v__DOT__RegisterFile__DOT__registers11);
			    } else {
				if ((0xc == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
				    __Vdly__v__DOT__RegisterFile__DOT__registers12 
					= ((IData)(vlTOPp->v__DOT__WB_RegWrite)
					    ? vlTOPp->v__DOT__WB_WriteData
					    : vlTOPp->v__DOT__RegisterFile__DOT__registers12);
				} else {
				    if ((0xd == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
					__Vdly__v__DOT__RegisterFile__DOT__registers13 
					    = ((IData)(vlTOPp->v__DOT__WB_RegWrite)
					        ? vlTOPp->v__DOT__WB_WriteData
					        : vlTOPp->v__DOT__RegisterFile__DOT__registers13);
				    } else {
					if ((0xe == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
					    __Vdly__v__DOT__RegisterFile__DOT__registers14 
						= ((IData)(vlTOPp->v__DOT__WB_RegWrite)
						    ? vlTOPp->v__DOT__WB_WriteData
						    : vlTOPp->v__DOT__RegisterFile__DOT__registers14);
					} else {
					    if ((0xf 
						 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
						__Vdly__v__DOT__RegisterFile__DOT__registers15 
						    = 
						    ((IData)(vlTOPp->v__DOT__WB_RegWrite)
						      ? vlTOPp->v__DOT__WB_WriteData
						      : vlTOPp->v__DOT__RegisterFile__DOT__registers15);
					    } else {
						__Vdly__v__DOT__RegisterFile__DOT__registers16 
						    = 
						    ((IData)(vlTOPp->v__DOT__WB_RegWrite)
						      ? vlTOPp->v__DOT__WB_WriteData
						      : vlTOPp->v__DOT__RegisterFile__DOT__registers16);
					    }
					}
				    }
				}
			    }
			}
		    }
		} else {
		    if (((((((((0x11 == (IData)(vlTOPp->v__DOT__WB_RtRd)) 
			       | (0x12 == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
			      | (0x13 == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
			     | (0x14 == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
			    | (0x15 == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
			   | (0x16 == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
			  | (0x17 == (IData)(vlTOPp->v__DOT__WB_RtRd))) 
			 | (0x18 == (IData)(vlTOPp->v__DOT__WB_RtRd)))) {
			if ((0x11 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
			    __Vdly__v__DOT__RegisterFile__DOT__registers17 
				= ((IData)(vlTOPp->v__DOT__WB_RegWrite)
				    ? vlTOPp->v__DOT__WB_WriteData
				    : vlTOPp->v__DOT__RegisterFile__DOT__registers17);
			} else {
			    if ((0x12 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
				__Vdly__v__DOT__RegisterFile__DOT__registers18 
				    = ((IData)(vlTOPp->v__DOT__WB_RegWrite)
				        ? vlTOPp->v__DOT__WB_WriteData
				        : vlTOPp->v__DOT__RegisterFile__DOT__registers18);
			    } else {
				if ((0x13 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
				    __Vdly__v__DOT__RegisterFile__DOT__registers19 
					= ((IData)(vlTOPp->v__DOT__WB_RegWrite)
					    ? vlTOPp->v__DOT__WB_WriteData
					    : vlTOPp->v__DOT__RegisterFile__DOT__registers19);
				} else {
				    if ((0x14 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
					__Vdly__v__DOT__RegisterFile__DOT__registers20 
					    = ((IData)(vlTOPp->v__DOT__WB_RegWrite)
					        ? vlTOPp->v__DOT__WB_WriteData
					        : vlTOPp->v__DOT__RegisterFile__DOT__registers20);
				    } else {
					if ((0x15 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
					    __Vdly__v__DOT__RegisterFile__DOT__registers21 
						= ((IData)(vlTOPp->v__DOT__WB_RegWrite)
						    ? vlTOPp->v__DOT__WB_WriteData
						    : vlTOPp->v__DOT__RegisterFile__DOT__registers21);
					} else {
					    if ((0x16 
						 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
						__Vdly__v__DOT__RegisterFile__DOT__registers22 
						    = 
						    ((IData)(vlTOPp->v__DOT__WB_RegWrite)
						      ? vlTOPp->v__DOT__WB_WriteData
						      : vlTOPp->v__DOT__RegisterFile__DOT__registers22);
					    } else {
						if (
						    (0x17 
						     == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
						    __Vdly__v__DOT__RegisterFile__DOT__registers23 
							= 
							((IData)(vlTOPp->v__DOT__WB_RegWrite)
							  ? vlTOPp->v__DOT__WB_WriteData
							  : vlTOPp->v__DOT__RegisterFile__DOT__registers23);
						} else {
						    __Vdly__v__DOT__RegisterFile__DOT__registers24 
							= 
							((IData)(vlTOPp->v__DOT__WB_RegWrite)
							  ? vlTOPp->v__DOT__WB_WriteData
							  : vlTOPp->v__DOT__RegisterFile__DOT__registers24);
						}
					    }
					}
				    }
				}
			    }
			}
		    } else {
			if ((0x19 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
			    __Vdly__v__DOT__RegisterFile__DOT__registers25 
				= ((IData)(vlTOPp->v__DOT__WB_RegWrite)
				    ? vlTOPp->v__DOT__WB_WriteData
				    : vlTOPp->v__DOT__RegisterFile__DOT__registers25);
			} else {
			    if ((0x1a == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
				__Vdly__v__DOT__RegisterFile__DOT__registers26 
				    = ((IData)(vlTOPp->v__DOT__WB_RegWrite)
				        ? vlTOPp->v__DOT__WB_WriteData
				        : vlTOPp->v__DOT__RegisterFile__DOT__registers26);
			    } else {
				if ((0x1b == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
				    __Vdly__v__DOT__RegisterFile__DOT__registers27 
					= ((IData)(vlTOPp->v__DOT__WB_RegWrite)
					    ? vlTOPp->v__DOT__WB_WriteData
					    : vlTOPp->v__DOT__RegisterFile__DOT__registers27);
				} else {
				    if ((0x1c == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
					__Vdly__v__DOT__RegisterFile__DOT__registers28 
					    = ((IData)(vlTOPp->v__DOT__WB_RegWrite)
					        ? vlTOPp->v__DOT__WB_WriteData
					        : vlTOPp->v__DOT__RegisterFile__DOT__registers28);
				    } else {
					if ((0x1d == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
					    __Vdly__v__DOT__RegisterFile__DOT__registers29 
						= ((IData)(vlTOPp->v__DOT__WB_RegWrite)
						    ? vlTOPp->v__DOT__WB_WriteData
						    : vlTOPp->v__DOT__RegisterFile__DOT__registers29);
					} else {
					    if ((0x1e 
						 == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
						__Vdly__v__DOT__RegisterFile__DOT__registers30 
						    = 
						    ((IData)(vlTOPp->v__DOT__WB_RegWrite)
						      ? vlTOPp->v__DOT__WB_WriteData
						      : vlTOPp->v__DOT__RegisterFile__DOT__registers30);
					    } else {
						if (
						    (0x1f 
						     == (IData)(vlTOPp->v__DOT__WB_RtRd))) {
						    __Vdly__v__DOT__RegisterFile__DOT__registers31 
							= 
							((IData)(vlTOPp->v__DOT__WB_RegWrite)
							  ? vlTOPp->v__DOT__WB_WriteData
							  : vlTOPp->v__DOT__RegisterFile__DOT__registers31);
						}
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}
    }
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/MemControl.v:600
    __Vdly__v__DOT__DataMem_Controller__DOT__LLSC_Atomic 
	= (1 & ((~ (IData)(vlTOPp->reset)) & ((IData)(vlTOPp->v__DOT__M_MemRead)
					       ? ((IData)(vlTOPp->v__DOT__M_LLSC)
						   ? 1
						   : (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__LLSC_Atomic))
					       : ((~ 
						   ((IData)(vlTOPp->v__DOT__ID_Eret) 
						    | ((((~ (IData)(vlTOPp->v__DOT__M_Stall_Controller)) 
							 & (~ (IData)(vlTOPp->v__DOT__IF_Stall))) 
							& (IData)(vlTOPp->v__DOT__M_MemWrite)) 
						       & ((0x3fffffff 
							   & (vlTOPp->v__DOT__M_ALUResult 
							      >> 2)) 
							  == vlTOPp->v__DOT__DataMem_Controller__DOT__LLSC_Address)))) 
						  & (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__LLSC_Atomic)))));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/CPZero.v:863
    if (vlTOPp->reset) {
	__Vdly__v__DOT__CP0__DOT__Status_BEV = 1;
	__Vdly__v__DOT__CP0__DOT__Status_NMI = 0;
	__Vdly__v__DOT__CP0__DOT__Status_ERL = 1;
	__Vdly__v__DOT__CP0__DOT__ErrorEPC = 0;
    } else {
	if (((IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Ready) 
	     & (IData)(vlTOPp->NMI))) {
	    __Vdly__v__DOT__CP0__DOT__Status_BEV = 1;
	    __Vdly__v__DOT__CP0__DOT__Status_NMI = 1;
	    __Vdly__v__DOT__CP0__DOT__Status_ERL = 1;
	    __Vdly__v__DOT__CP0__DOT__ErrorEPC = vlTOPp->v__DOT__ID_RestartPC;
	} else {
	    __Vdly__v__DOT__CP0__DOT__Status_BEV = 
		(1 & ((((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
			& (0xc == (0x1f & (vlTOPp->v__DOT__Instruction 
					   >> 0xb)))) 
		       & (0 == (7 & vlTOPp->v__DOT__Instruction)))
		       ? (vlTOPp->v__DOT__ID_ReadData2_End 
			  >> 0x16) : (IData)(vlTOPp->v__DOT__CP0__DOT__Status_BEV)));
	    __Vdly__v__DOT__CP0__DOT__Status_NMI = 
		(1 & ((((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
			& (0xc == (0x1f & (vlTOPp->v__DOT__Instruction 
					   >> 0xb)))) 
		       & (0 == (7 & vlTOPp->v__DOT__Instruction)))
		       ? (vlTOPp->v__DOT__ID_ReadData2_End 
			  >> 0x13) : (IData)(vlTOPp->v__DOT__CP0__DOT__Status_NMI)));
	    __Vdly__v__DOT__CP0__DOT__Status_ERL = 
		(1 & ((((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
			& (0xc == (0x1f & (vlTOPp->v__DOT__Instruction 
					   >> 0xb)))) 
		       & (0 == (7 & vlTOPp->v__DOT__Instruction)))
		       ? (1 & (vlTOPp->v__DOT__ID_ReadData2_End 
			       >> 2)) : ((((IData)(vlTOPp->v__DOT__CP0__DOT__Status_ERL) 
					   & (IData)(vlTOPp->v__DOT__ID_Eret)) 
					  & (~ (IData)(vlTOPp->v__DOT__ID_Stall)))
					  ? 0 : (IData)(vlTOPp->v__DOT__CP0__DOT__Status_ERL))));
	    __Vdly__v__DOT__CP0__DOT__ErrorEPC = ((
						   ((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
						    & (0x1e 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0xb)))) 
						   & (0 
						      == 
						      (7 
						       & vlTOPp->v__DOT__Instruction)))
						   ? vlTOPp->v__DOT__ID_ReadData2_End
						   : vlTOPp->v__DOT__CP0__DOT__ErrorEPC);
	}
    }
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Register.v:29
    __Vdly__v__DOT__IF_PCOut = ((IData)(vlTOPp->reset)
				 ? 0x10 : (((IData)(vlTOPp->v__DOT__IF_Stall) 
					    | (IData)(vlTOPp->v__DOT__ID_Stall))
					    ? vlTOPp->v__DOT__IF_PCOut
					    : (((((
						   ((IData)(vlTOPp->reset) 
						    | ((IData)(vlTOPp->v__DOT__ID_Eret) 
						       & (~ (IData)(vlTOPp->v__DOT__ID_Stall)))) 
						   | (IData)(vlTOPp->v__DOT__CP0__DOT__IF_Exception_Ready)) 
						  | (IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Ready)) 
						 | (IData)(vlTOPp->v__DOT__CP0__DOT__EX_Exception_Ready)) 
						| (IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Ready))
					        ? ((IData)(vlTOPp->reset)
						    ? 0x10
						    : 
						   (((IData)(vlTOPp->v__DOT__ID_Eret) 
						     & (~ (IData)(vlTOPp->v__DOT__ID_Stall)))
						     ? 
						    ((IData)(vlTOPp->v__DOT__CP0__DOT__Status_ERL)
						      ? vlTOPp->v__DOT__CP0__DOT__ErrorEPC
						      : vlTOPp->v__DOT__CP0__DOT__EPC)
						     : 
						    (((((((((IData)(vlTOPp->v__DOT__IF_EXC_AdIF) 
							    | (IData)(vlTOPp->v__DOT__M_EXC_AdEL)) 
							   | (IData)(vlTOPp->v__DOT__M_EXC_AdES)) 
							  | (IData)(vlTOPp->v__DOT__EX_EXC_Ov)) 
							 | (IData)(vlTOPp->v__DOT__M_EXC_Tr)) 
							| (IData)(vlTOPp->v__DOT__ID_EXC_Sys)) 
						       | (IData)(vlTOPp->v__DOT__ID_EXC_Bp)) 
						      | (IData)(vlTOPp->v__DOT__CP0__DOT__EXC_CpU))
						      ? 0
						      : 
						     ((IData)(vlTOPp->NMI)
						       ? 0x10
						       : 
						      (((IData)(vlTOPp->v__DOT__CP0__DOT__EXC_Int) 
							& (IData)(vlTOPp->v__DOT__CP0__DOT__Cause_IV))
						        ? 8
						        : 0)))))
					        : (
						   (2 
						    & (IData)(vlTOPp->v__DOT__ID_PCSrc))
						    ? 
						   ((1 
						     & (IData)(vlTOPp->v__DOT__ID_PCSrc))
						     ? vlTOPp->v__DOT__ID_ReadData1_End
						     : 
						    (vlTOPp->v__DOT__ID_PCAdd4 
						     + 
						     (vlTOPp->v__DOT__ID_SignExtImm 
						      << 2)))
						    : 
						   ((1 
						     & (IData)(vlTOPp->v__DOT__ID_PCSrc))
						     ? 
						    ((0xf0000000 
						      & vlTOPp->v__DOT__ID_PCAdd4) 
						     | (0xffffffc 
							& (vlTOPp->v__DOT__Instruction 
							   << 2)))
						     : 
						    ((IData)(4) 
						     + vlTOPp->v__DOT__IF_PCOut))))));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/CPZero.v:885
    if (vlTOPp->reset) {
	__Vdly__v__DOT__CP0__DOT__Count = 0;
	__Vdly__v__DOT__CP0__DOT__Compare = 0;
	__Vdly__v__DOT__CP0__DOT__Status_CU_0 = 0;
	__Vdly__v__DOT__CP0__DOT__Status_RE = 0;
	__Vdly__v__DOT__CP0__DOT__Status_IM = 0;
	__Vdly__v__DOT__CP0__DOT__Status_UM = 0;
	__Vdly__v__DOT__CP0__DOT__Status_IE = 0;
	__Vdly__v__DOT__CP0__DOT__Cause_IV = 0;
	__Vdly__v__DOT__CP0__DOT__Cause_IP = 0;
    } else {
	__Vdly__v__DOT__CP0__DOT__Status_RE = (1 & 
					       ((((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
						  & (0xc 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0xb)))) 
						 & (0 
						    == 
						    (7 
						     & vlTOPp->v__DOT__Instruction)))
						 ? 
						(vlTOPp->v__DOT__ID_ReadData2_End 
						 >> 0x19)
						 : (IData)(vlTOPp->v__DOT__CP0__DOT__Status_RE)));
	__Vdly__v__DOT__CP0__DOT__Status_IM = (0xff 
					       & ((((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
						    & (0xc 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0xb)))) 
						   & (0 
						      == 
						      (7 
						       & vlTOPp->v__DOT__Instruction)))
						   ? 
						  (vlTOPp->v__DOT__ID_ReadData2_End 
						   >> 8)
						   : (IData)(vlTOPp->v__DOT__CP0__DOT__Status_IM)));
	__Vdly__v__DOT__CP0__DOT__Status_UM = (1 & 
					       ((((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
						  & (0xc 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0xb)))) 
						 & (0 
						    == 
						    (7 
						     & vlTOPp->v__DOT__Instruction)))
						 ? 
						(vlTOPp->v__DOT__ID_ReadData2_End 
						 >> 4)
						 : (IData)(vlTOPp->v__DOT__CP0__DOT__Status_UM)));
	__Vdly__v__DOT__CP0__DOT__Status_IE = (1 & 
					       ((((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
						  & (0xc 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0xb)))) 
						 & (0 
						    == 
						    (7 
						     & vlTOPp->v__DOT__Instruction)))
						 ? vlTOPp->v__DOT__ID_ReadData2_End
						 : (IData)(vlTOPp->v__DOT__CP0__DOT__Status_IE)));
	__Vdly__v__DOT__CP0__DOT__Cause_IV = (1 & (
						   (((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
						     & (0xd 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0xb)))) 
						    & (0 
						       == 
						       (7 
							& vlTOPp->v__DOT__Instruction)))
						    ? 
						   (vlTOPp->v__DOT__ID_ReadData2_End 
						    >> 0x17)
						    : (IData)(vlTOPp->v__DOT__CP0__DOT__Cause_IV)));
	__Vdly__v__DOT__CP0__DOT__Count = ((((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
					     & (9 == 
						(0x1f 
						 & (vlTOPp->v__DOT__Instruction 
						    >> 0xb)))) 
					    & (0 == 
					       (7 & vlTOPp->v__DOT__Instruction)))
					    ? vlTOPp->v__DOT__ID_ReadData2_End
					    : ((vlTOPp->v__DOT__CP0__DOT__Count 
						== vlTOPp->v__DOT__CP0__DOT__Compare)
					        ? 0
					        : ((IData)(1) 
						   + vlTOPp->v__DOT__CP0__DOT__Count)));
	__Vdly__v__DOT__CP0__DOT__Compare = ((((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
					       & (0xb 
						  == 
						  (0x1f 
						   & (vlTOPp->v__DOT__Instruction 
						      >> 0xb)))) 
					      & (0 
						 == 
						 (7 
						  & vlTOPp->v__DOT__Instruction)))
					      ? vlTOPp->v__DOT__ID_ReadData2_End
					      : vlTOPp->v__DOT__CP0__DOT__Compare);
	__Vdly__v__DOT__CP0__DOT__Status_CU_0 = (1 
						 & ((((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
						      & (0xc 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0xb)))) 
						     & (0 
							== 
							(7 
							 & vlTOPp->v__DOT__Instruction)))
						     ? 
						    (vlTOPp->v__DOT__ID_ReadData2_End 
						     >> 0x1c)
						     : (IData)(vlTOPp->v__DOT__CP0__DOT__Status_CU_0)));
	__Vdly__v__DOT__CP0__DOT__Cause_IP = ((0x7f 
					       & (IData)(__Vdly__v__DOT__CP0__DOT__Cause_IP)) 
					      | (0x80 
						 & (((((((IData)(vlTOPp->v__DOT__CP0__DOT__Status_CU_0) 
							 | (IData)(vlTOPp->v__DOT__ID_KernelMode)) 
							& (IData)(vlTOPp->v__DOT__ID_Mfc0)) 
						       & (9 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0xb)))) 
						      & (0 
							 == 
							 (7 
							  & vlTOPp->v__DOT__Instruction)))
						      ? 0
						      : 
						     ((0 
						       == 
						       (1 
							& ((IData)(vlTOPp->v__DOT__CP0__DOT__Cause_IP) 
							   >> 7)))
						       ? 
						      (vlTOPp->v__DOT__CP0__DOT__Count 
						       == vlTOPp->v__DOT__CP0__DOT__Compare)
						       : 
						      (1 
						       & ((IData)(vlTOPp->v__DOT__CP0__DOT__Cause_IP) 
							  >> 7)))) 
						    << 7)));
	__Vdly__v__DOT__CP0__DOT__Cause_IP = ((0x83 
					       & (IData)(__Vdly__v__DOT__CP0__DOT__Cause_IP)) 
					      | ((IData)(vlTOPp->Interrupts) 
						 << 2));
	__Vdly__v__DOT__CP0__DOT__Cause_IP = ((0xfc 
					       & (IData)(__Vdly__v__DOT__CP0__DOT__Cause_IP)) 
					      | (3 
						 & ((((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
						      & (0xd 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0xb)))) 
						     & (0 
							== 
							(7 
							 & vlTOPp->v__DOT__Instruction)))
						     ? 
						    (vlTOPp->v__DOT__ID_ReadData2_End 
						     >> 8)
						     : (IData)(vlTOPp->v__DOT__CP0__DOT__Cause_IP))));
    }
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/CPZero.v:919
    if (vlTOPp->reset) {
	__Vdly__v__DOT__CP0__DOT__Cause_BD = 0;
	__Vdly__v__DOT__CP0__DOT__Cause_CE = 0;
	__Vdly__v__DOT__CP0__DOT__Cause_ExcCode30 = 0;
	__Vdly__v__DOT__CP0__DOT__Status_EXL = 0;
	__Vdly__v__DOT__CP0__DOT__EPC = 0;
	__Vdly__v__DOT__CP0__DOT__BadVAddr = 0;
    } else {
	if (vlTOPp->v__DOT__CP0__DOT__M_Exception_Ready) {
	    __Vdly__v__DOT__CP0__DOT__Cause_BD = ((IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL)
						   ? (IData)(vlTOPp->v__DOT__CP0__DOT__Cause_BD)
						   : (IData)(vlTOPp->v__DOT__M_IsBDS));
	    __Vdly__v__DOT__CP0__DOT__EPC = ((IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL)
					      ? vlTOPp->v__DOT__CP0__DOT__EPC
					      : vlTOPp->v__DOT__M_RestartPC);
	    __Vdly__v__DOT__CP0__DOT__Cause_CE = ((0x13 
						   == 
						   (0x3f 
						    & (vlTOPp->v__DOT__Instruction 
						       >> 0x1a)))
						   ? 3
						   : 
						  ((0x12 
						    == 
						    (0x3f 
						     & (vlTOPp->v__DOT__Instruction 
							>> 0x1a)))
						    ? 2
						    : 
						   ((0x11 
						     == 
						     (0x3f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x1a)))
						     ? 1
						     : 0)));
	    __Vdly__v__DOT__CP0__DOT__Cause_ExcCode30 
		= vlTOPp->v__DOT__CP0__DOT__Cause_ExcCode_bits;
	    __Vdly__v__DOT__CP0__DOT__BadVAddr = vlTOPp->v__DOT__M_ALUResult;
	    __Vdly__v__DOT__CP0__DOT__Status_EXL = 1;
	} else {
	    if (vlTOPp->v__DOT__CP0__DOT__EX_Exception_Ready) {
		__Vdly__v__DOT__CP0__DOT__Cause_BD 
		    = ((IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL)
		        ? (IData)(vlTOPp->v__DOT__CP0__DOT__Cause_BD)
		        : (IData)(vlTOPp->v__DOT__EX_IsBDS));
		__Vdly__v__DOT__CP0__DOT__BadVAddr 
		    = vlTOPp->v__DOT__CP0__DOT__BadVAddr;
		__Vdly__v__DOT__CP0__DOT__EPC = ((IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL)
						  ? vlTOPp->v__DOT__CP0__DOT__EPC
						  : vlTOPp->v__DOT__EX_RestartPC);
		__Vdly__v__DOT__CP0__DOT__Cause_CE 
		    = ((0x13 == (0x3f & (vlTOPp->v__DOT__Instruction 
					 >> 0x1a)))
		        ? 3 : ((0x12 == (0x3f & (vlTOPp->v__DOT__Instruction 
						 >> 0x1a)))
			        ? 2 : ((0x11 == (0x3f 
						 & (vlTOPp->v__DOT__Instruction 
						    >> 0x1a)))
				        ? 1 : 0)));
		__Vdly__v__DOT__CP0__DOT__Cause_ExcCode30 
		    = vlTOPp->v__DOT__CP0__DOT__Cause_ExcCode_bits;
		__Vdly__v__DOT__CP0__DOT__Status_EXL = 1;
	    } else {
		if (vlTOPp->v__DOT__CP0__DOT__ID_Exception_Ready) {
		    __Vdly__v__DOT__CP0__DOT__Cause_BD 
			= ((IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL)
			    ? (IData)(vlTOPp->v__DOT__CP0__DOT__Cause_BD)
			    : (IData)(vlTOPp->v__DOT__ID_IsBDS));
		    __Vdly__v__DOT__CP0__DOT__BadVAddr 
			= vlTOPp->v__DOT__CP0__DOT__BadVAddr;
		    __Vdly__v__DOT__CP0__DOT__EPC = 
			((IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL)
			  ? vlTOPp->v__DOT__CP0__DOT__EPC
			  : vlTOPp->v__DOT__ID_RestartPC);
		    __Vdly__v__DOT__CP0__DOT__Cause_CE 
			= ((0x13 == (0x3f & (vlTOPp->v__DOT__Instruction 
					     >> 0x1a)))
			    ? 3 : ((0x12 == (0x3f & 
					     (vlTOPp->v__DOT__Instruction 
					      >> 0x1a)))
				    ? 2 : ((0x11 == 
					    (0x3f & 
					     (vlTOPp->v__DOT__Instruction 
					      >> 0x1a)))
					    ? 1 : 0)));
		    __Vdly__v__DOT__CP0__DOT__Cause_ExcCode30 
			= vlTOPp->v__DOT__CP0__DOT__Cause_ExcCode_bits;
		    __Vdly__v__DOT__CP0__DOT__Status_EXL = 1;
		} else {
		    if (vlTOPp->v__DOT__CP0__DOT__IF_Exception_Ready) {
			__Vdly__v__DOT__CP0__DOT__Cause_BD 
			    = ((IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL)
			        ? (IData)(vlTOPp->v__DOT__CP0__DOT__Cause_BD)
			        : (IData)(vlTOPp->v__DOT__ID_NextIsDelay));
			__Vdly__v__DOT__CP0__DOT__EPC 
			    = ((IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL)
			        ? vlTOPp->v__DOT__CP0__DOT__EPC
			        : vlTOPp->v__DOT__IF_PCOut);
			__Vdly__v__DOT__CP0__DOT__Cause_CE 
			    = ((0x13 == (0x3f & (vlTOPp->v__DOT__Instruction 
						 >> 0x1a)))
			        ? 3 : ((0x12 == (0x3f 
						 & (vlTOPp->v__DOT__Instruction 
						    >> 0x1a)))
				        ? 2 : ((0x11 
						== 
						(0x3f 
						 & (vlTOPp->v__DOT__Instruction 
						    >> 0x1a)))
					        ? 1
					        : 0)));
			__Vdly__v__DOT__CP0__DOT__Cause_ExcCode30 
			    = vlTOPp->v__DOT__CP0__DOT__Cause_ExcCode_bits;
			__Vdly__v__DOT__CP0__DOT__BadVAddr 
			    = vlTOPp->v__DOT__IF_PCOut;
			__Vdly__v__DOT__CP0__DOT__Status_EXL = 1;
		    } else {
			__Vdly__v__DOT__CP0__DOT__Cause_CE 
			    = vlTOPp->v__DOT__CP0__DOT__Cause_CE;
			__Vdly__v__DOT__CP0__DOT__Cause_ExcCode30 
			    = vlTOPp->v__DOT__CP0__DOT__Cause_ExcCode30;
			__Vdly__v__DOT__CP0__DOT__Status_EXL 
			    = (1 & ((((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
				      & (0xc == (0x1f 
						 & (vlTOPp->v__DOT__Instruction 
						    >> 0xb)))) 
				     & (0 == (7 & vlTOPp->v__DOT__Instruction)))
				     ? (1 & (vlTOPp->v__DOT__ID_ReadData2_End 
					     >> 1))
				     : ((((IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL) 
					  & (IData)(vlTOPp->v__DOT__ID_Eret)) 
					 & (~ (IData)(vlTOPp->v__DOT__ID_Stall)))
					 ? 0 : (IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL))));
			__Vdly__v__DOT__CP0__DOT__EPC 
			    = ((((IData)(vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond) 
				 & (0xe == (0x1f & 
					    (vlTOPp->v__DOT__Instruction 
					     >> 0xb)))) 
				& (0 == (7 & vlTOPp->v__DOT__Instruction)))
			        ? vlTOPp->v__DOT__ID_ReadData2_End
			        : vlTOPp->v__DOT__CP0__DOT__EPC);
			__Vdly__v__DOT__CP0__DOT__BadVAddr 
			    = vlTOPp->v__DOT__CP0__DOT__BadVAddr;
			__Vdly__v__DOT__CP0__DOT__Cause_BD = 0;
		    }
		}
	    }
	}
    }
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/MEMWB_Stage.v:65
    __Vdly__v__DOT__WB_RegWrite = (1 & ((IData)(vlTOPp->reset)
					 ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
						 ? (IData)(vlTOPp->v__DOT__WB_RegWrite)
						 : 
						(((IData)(vlTOPp->v__DOT__M_Stall) 
						  | (IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect))
						  ? 0
						  : (IData)(vlTOPp->v__DOT__M_RegWrite)))));
    __Vdly__v__DOT__WB_MemtoReg = (1 & ((IData)(vlTOPp->reset)
					 ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
						 ? (IData)(vlTOPp->v__DOT__WB_MemtoReg)
						 : (IData)(vlTOPp->v__DOT__M_MemtoReg))));
    __Vdly__v__DOT__WB_ReadData = ((IData)(vlTOPp->reset)
				    ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					    ? vlTOPp->v__DOT__WB_ReadData
					    : ((IData)(vlTOPp->v__DOT__M_MemByte)
					        ? ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_LL)
						    ? 
						   (((IData)(vlTOPp->v__DOT__M_MemSignExtend) 
						     & (vlTOPp->DataMem_In 
							>> 0x1f))
						     ? 
						    (0xffffff00 
						     | (0xff 
							& (vlTOPp->DataMem_In 
							   >> 0x18)))
						     : 
						    (0xff 
						     & (vlTOPp->DataMem_In 
							>> 0x18)))
						    : 
						   ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_LM)
						     ? 
						    (((IData)(vlTOPp->v__DOT__M_MemSignExtend) 
						      & (vlTOPp->DataMem_In 
							 >> 0x17))
						      ? 
						     (0xffffff00 
						      | (0xff 
							 & (vlTOPp->DataMem_In 
							    >> 0x10)))
						      : 
						     (0xff 
						      & (vlTOPp->DataMem_In 
							 >> 0x10)))
						     : 
						    ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_RM)
						      ? 
						     (((IData)(vlTOPp->v__DOT__M_MemSignExtend) 
						       & (vlTOPp->DataMem_In 
							  >> 0xf))
						       ? 
						      (0xffffff00 
						       | (0xff 
							  & (vlTOPp->DataMem_In 
							     >> 8)))
						       : 
						      (0xff 
						       & (vlTOPp->DataMem_In 
							  >> 8)))
						      : 
						     (((IData)(vlTOPp->v__DOT__M_MemSignExtend) 
						       & (vlTOPp->DataMem_In 
							  >> 7))
						       ? 
						      (0xffffff00 
						       | (0xff 
							  & vlTOPp->DataMem_In))
						       : 
						      (0xff 
						       & vlTOPp->DataMem_In)))))
					        : ((IData)(vlTOPp->v__DOT__M_MemHalf)
						    ? 
						   ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_L)
						     ? 
						    (((IData)(vlTOPp->v__DOT__M_MemSignExtend) 
						      & (vlTOPp->DataMem_In 
							 >> 0x1f))
						      ? 
						     (0xffff0000 
						      | (0xffff 
							 & (vlTOPp->DataMem_In 
							    >> 0x10)))
						      : 
						     (0xffff 
						      & (vlTOPp->DataMem_In 
							 >> 0x10)))
						     : 
						    (((IData)(vlTOPp->v__DOT__M_MemSignExtend) 
						      & (vlTOPp->DataMem_In 
							 >> 0xf))
						      ? 
						     (0xffff0000 
						      | (0xffff 
							 & vlTOPp->DataMem_In))
						      : 
						     (0xffff 
						      & vlTOPp->DataMem_In)))
						    : 
						   (((IData)(vlTOPp->v__DOT__M_LLSC) 
						     & (IData)(vlTOPp->v__DOT__M_MemWrite))
						     ? 
						    (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__LLSC_Atomic) 
						      & ((0x3fffffff 
							  & (vlTOPp->v__DOT__M_ALUResult 
							     >> 2)) 
							 == vlTOPp->v__DOT__DataMem_Controller__DOT__LLSC_Address))
						      ? 1
						      : 0)
						     : 
						    ((IData)(vlTOPp->v__DOT__M_Left)
						      ? 
						     ((2 
						       & vlTOPp->v__DOT__M_ALUResult)
						       ? 
						      ((1 
							& vlTOPp->v__DOT__M_ALUResult)
						        ? 
						       ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
							 ? 
							((0xff000000 
							  & (vlTOPp->DataMem_In 
							     << 0x18)) 
							 | (0xffffff 
							    & vlTOPp->v__DOT__M_WriteData_Pre))
							 : vlTOPp->DataMem_In)
						        : 
						       ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
							 ? 
							((0xffff0000 
							  & (vlTOPp->DataMem_In 
							     << 0x10)) 
							 | (0xffff 
							    & vlTOPp->v__DOT__M_WriteData_Pre))
							 : 
							((0xffffff00 
							  & (vlTOPp->DataMem_In 
							     << 8)) 
							 | (0xff 
							    & vlTOPp->v__DOT__M_WriteData_Pre))))
						       : 
						      ((1 
							& vlTOPp->v__DOT__M_ALUResult)
						        ? 
						       ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
							 ? 
							((0xffffff00 
							  & (vlTOPp->DataMem_In 
							     << 8)) 
							 | (0xff 
							    & vlTOPp->v__DOT__M_WriteData_Pre))
							 : 
							((0xffff0000 
							  & (vlTOPp->DataMem_In 
							     << 0x10)) 
							 | (0xffff 
							    & vlTOPp->v__DOT__M_WriteData_Pre)))
						        : 
						       ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
							 ? vlTOPp->DataMem_In
							 : 
							((0xff000000 
							  & (vlTOPp->DataMem_In 
							     << 0x18)) 
							 | (0xffffff 
							    & vlTOPp->v__DOT__M_WriteData_Pre)))))
						      : 
						     ((IData)(vlTOPp->v__DOT__M_Right)
						       ? 
						      ((2 
							& vlTOPp->v__DOT__M_ALUResult)
						        ? 
						       ((1 
							 & vlTOPp->v__DOT__M_ALUResult)
							 ? 
							((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
							  ? vlTOPp->DataMem_In
							  : 
							 ((0xffffff00 
							   & vlTOPp->v__DOT__M_WriteData_Pre) 
							  | (0xff 
							     & (vlTOPp->DataMem_In 
								>> 0x18))))
							 : 
							((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
							  ? 
							 ((0xff000000 
							   & vlTOPp->v__DOT__M_WriteData_Pre) 
							  | (0xffffff 
							     & (vlTOPp->DataMem_In 
								>> 8)))
							  : 
							 ((0xffff0000 
							   & vlTOPp->v__DOT__M_WriteData_Pre) 
							  | (0xffff 
							     & (vlTOPp->DataMem_In 
								>> 0x10)))))
						        : 
						       ((1 
							 & vlTOPp->v__DOT__M_ALUResult)
							 ? 
							((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
							  ? 
							 ((0xffff0000 
							   & vlTOPp->v__DOT__M_WriteData_Pre) 
							  | (0xffff 
							     & (vlTOPp->DataMem_In 
								>> 0x10)))
							  : 
							 ((0xff000000 
							   & vlTOPp->v__DOT__M_WriteData_Pre) 
							  | (0xffffff 
							     & (vlTOPp->DataMem_In 
								>> 8))))
							 : 
							((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
							  ? 
							 ((0xffffff00 
							   & vlTOPp->v__DOT__M_WriteData_Pre) 
							  | (0xff 
							     & (vlTOPp->DataMem_In 
								>> 0x18)))
							  : vlTOPp->DataMem_In)))
						       : vlTOPp->DataMem_In)))))));
    __Vdly__v__DOT__WB_ALUResult = ((IData)(vlTOPp->reset)
				     ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					     ? vlTOPp->v__DOT__WB_ALUResult
					     : vlTOPp->v__DOT__M_ALUResult));
    __Vdly__v__DOT__WB_RtRd = ((IData)(vlTOPp->reset)
			        ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
				        ? (IData)(vlTOPp->v__DOT__WB_RtRd)
				        : (IData)(vlTOPp->v__DOT__M_RtRd)));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/EXMEM_Stage.v:91
    __Vdly__v__DOT__M_RegWrite = (1 & ((IData)(vlTOPp->reset)
				        ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					        ? (IData)(vlTOPp->v__DOT__M_RegWrite)
					        : (
						   ((IData)(vlTOPp->v__DOT__EX_Stall) 
						    | (IData)(vlTOPp->v__DOT__EX_Exception_Flush))
						    ? 0
						    : (IData)(vlTOPp->v__DOT__EX_RegWrite)))));
    __Vdly__v__DOT__M_RegWrite = (1 & ((IData)(vlTOPp->reset)
				        ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					        ? (IData)(vlTOPp->v__DOT__M_RegWrite)
					        : (
						   ((IData)(vlTOPp->v__DOT__EX_Stall) 
						    | (IData)(vlTOPp->v__DOT__EX_Exception_Flush))
						    ? 0
						    : 
						   (((IData)(vlTOPp->v__DOT__EX_Movn) 
						     | (IData)(vlTOPp->v__DOT__EX_Movz))
						     ? 
						    (((IData)(vlTOPp->v__DOT__EX_Movn) 
						      & (0 
							 != vlTOPp->v__DOT__EX_ReadData2_Imm)) 
						     | ((IData)(vlTOPp->v__DOT__EX_Movz) 
							& (0 
							   == vlTOPp->v__DOT__EX_ReadData2_Imm)))
						     : (IData)(vlTOPp->v__DOT__EX_RegWrite))))));
    __Vdly__v__DOT__M_MemtoReg = (1 & ((IData)(vlTOPp->reset)
				        ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					        ? (IData)(vlTOPp->v__DOT__M_MemtoReg)
					        : (IData)(vlTOPp->v__DOT__EX_MemtoReg))));
    __Vdly__v__DOT__M_ReverseEndian = (1 & ((IData)(vlTOPp->reset)
					     ? 0 : 
					    ((IData)(vlTOPp->v__DOT__M_Stall)
					      ? (IData)(vlTOPp->v__DOT__M_ReverseEndian)
					      : (IData)(vlTOPp->v__DOT__EX_ReverseEndian))));
    __Vdly__v__DOT__M_LLSC = (1 & ((IData)(vlTOPp->reset)
				    ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					    ? (IData)(vlTOPp->v__DOT__M_LLSC)
					    : (IData)(vlTOPp->v__DOT__EX_LLSC))));
    __Vdly__v__DOT__M_MemRead = (1 & ((IData)(vlTOPp->reset)
				       ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					       ? (IData)(vlTOPp->v__DOT__M_MemRead)
					       : (((IData)(vlTOPp->v__DOT__EX_Stall) 
						   | (IData)(vlTOPp->v__DOT__EX_Exception_Flush))
						   ? 0
						   : (IData)(vlTOPp->v__DOT__EX_MemRead)))));
    __Vdly__v__DOT__M_MemWrite = (1 & ((IData)(vlTOPp->reset)
				        ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					        ? (IData)(vlTOPp->v__DOT__M_MemWrite)
					        : (
						   ((IData)(vlTOPp->v__DOT__EX_Stall) 
						    | (IData)(vlTOPp->v__DOT__EX_Exception_Flush))
						    ? 0
						    : (IData)(vlTOPp->v__DOT__EX_MemWrite)))));
    __Vdly__v__DOT__M_MemByte = (1 & ((IData)(vlTOPp->reset)
				       ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					       ? (IData)(vlTOPp->v__DOT__M_MemByte)
					       : (IData)(vlTOPp->v__DOT__EX_MemByte))));
    __Vdly__v__DOT__M_MemHalf = (1 & ((IData)(vlTOPp->reset)
				       ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					       ? (IData)(vlTOPp->v__DOT__M_MemHalf)
					       : (IData)(vlTOPp->v__DOT__EX_MemHalf))));
    __Vdly__v__DOT__M_MemSignExtend = (1 & ((IData)(vlTOPp->reset)
					     ? 0 : 
					    ((IData)(vlTOPp->v__DOT__M_Stall)
					      ? (IData)(vlTOPp->v__DOT__M_MemSignExtend)
					      : (IData)(vlTOPp->v__DOT__EX_MemSignExtend))));
    __Vdly__v__DOT__M_Left = (1 & ((IData)(vlTOPp->reset)
				    ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					    ? (IData)(vlTOPp->v__DOT__M_Left)
					    : (IData)(vlTOPp->v__DOT__EX_Left))));
    __Vdly__v__DOT__M_Right = (1 & ((IData)(vlTOPp->reset)
				     ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					     ? (IData)(vlTOPp->v__DOT__M_Right)
					     : (IData)(vlTOPp->v__DOT__EX_Right))));
    __Vdly__v__DOT__M_KernelMode = (1 & ((IData)(vlTOPp->reset)
					  ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
						  ? (IData)(vlTOPp->v__DOT__M_KernelMode)
						  : (IData)(vlTOPp->v__DOT__EX_KernelMode))));
    __Vdly__v__DOT__M_RestartPC = ((IData)(vlTOPp->reset)
				    ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					    ? vlTOPp->v__DOT__M_RestartPC
					    : vlTOPp->v__DOT__EX_RestartPC));
    __Vdly__v__DOT__M_IsBDS = (1 & ((IData)(vlTOPp->reset)
				     ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					     ? (IData)(vlTOPp->v__DOT__M_IsBDS)
					     : (IData)(vlTOPp->v__DOT__EX_IsBDS))));
    __Vdly__v__DOT__M_Trap = (1 & ((IData)(vlTOPp->reset)
				    ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					    ? (IData)(vlTOPp->v__DOT__M_Trap)
					    : (((IData)(vlTOPp->v__DOT__EX_Stall) 
						| (IData)(vlTOPp->v__DOT__EX_Exception_Flush))
					        ? 0
					        : (IData)(vlTOPp->v__DOT__EX_Trap)))));
    __Vdly__v__DOT__M_TrapCond = (1 & ((IData)(vlTOPp->reset)
				        ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					        ? (IData)(vlTOPp->v__DOT__M_TrapCond)
					        : (IData)(vlTOPp->v__DOT__EX_TrapCond))));
    __Vdly__v__DOT__M_M_CanErr = (1 & ((IData)(vlTOPp->reset)
				        ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					        ? (IData)(vlTOPp->v__DOT__M_M_CanErr)
					        : (
						   ((IData)(vlTOPp->v__DOT__EX_Stall) 
						    | (IData)(vlTOPp->v__DOT__EX_Exception_Flush))
						    ? 0
						    : (IData)(vlTOPp->v__DOT__EX_M_CanErr)))));
    __Vdly__v__DOT__M_ALUResult = ((IData)(vlTOPp->reset)
				    ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					    ? vlTOPp->v__DOT__M_ALUResult
					    : ((0x10 
						& (IData)(vlTOPp->v__DOT__EX_ALUOp))
					        ? (
						   (8 
						    & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						    ? 
						   ((4 
						     & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						     ? 
						    ((2 
						      & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						      ? 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? 
						      (vlTOPp->v__DOT__EX_ReadData1_Fwd 
						       ^ vlTOPp->v__DOT__EX_ReadData2_Imm)
						       : vlTOPp->v__DOT__ALU__DOT__AddSub_Result)
						      : 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? vlTOPp->v__DOT__ALU__DOT__AddSub_Result
						       : 
						      (vlTOPp->v__DOT__EX_ReadData2_Imm 
						       >> 
						       (0x1f 
							& vlTOPp->v__DOT__EX_ReadData1_Fwd))))
						     : 
						    ((2 
						      & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						      ? 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? 
						      (vlTOPp->v__DOT__EX_ReadData2_Imm 
						       >> 
						       (0x1f 
							& (vlTOPp->v__DOT__EX_SignExtImm 
							   >> 6)))
						       : 
						      (vlTOPp->v__DOT__EX_ReadData2_Imm 
						       >> 
						       (0x1f 
							& vlTOPp->v__DOT__EX_ReadData1_Fwd)))
						      : 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? 
						      (vlTOPp->v__DOT__EX_ReadData2_Imm 
						       >> 
						       (0x1f 
							& (vlTOPp->v__DOT__EX_SignExtImm 
							   >> 6)))
						       : 
						      ((vlTOPp->v__DOT__EX_ReadData1_Fwd 
							< vlTOPp->v__DOT__EX_ReadData2_Imm)
						        ? 1
						        : 0))))
						    : 
						   ((4 
						     & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						     ? 
						    ((2 
						      & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						      ? 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? 
						      (VL_LTS_III(1,32,32, vlTOPp->v__DOT__EX_ReadData1_Fwd, vlTOPp->v__DOT__EX_ReadData2_Imm)
						        ? 1
						        : 0)
						       : 
						      (vlTOPp->v__DOT__EX_ReadData2_Imm 
						       << 
						       (0x1f 
							& vlTOPp->v__DOT__EX_ReadData1_Fwd)))
						      : 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? 
						      (0xffff0000 
						       & (vlTOPp->v__DOT__EX_ReadData2_Imm 
							  << 0x10))
						       : 
						      (vlTOPp->v__DOT__EX_ReadData2_Imm 
						       << 
						       (0x1f 
							& (vlTOPp->v__DOT__EX_SignExtImm 
							   >> 6)))))
						     : 
						    ((2 
						      & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						      ? 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? 
						      (vlTOPp->v__DOT__EX_ReadData1_Fwd 
						       | vlTOPp->v__DOT__EX_ReadData2_Imm)
						       : 
						      (~ 
						       (vlTOPp->v__DOT__EX_ReadData1_Fwd 
							| vlTOPp->v__DOT__EX_ReadData2_Imm)))
						      : 0)))
					        : (
						   (8 
						    & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						    ? 
						   ((4 
						     & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						     ? 
						    ((2 
						      & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						      ? 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? (IData)(vlTOPp->v__DOT__ALU__DOT__Multu_Result)
						       : 0)
						      : 0)
						     : 
						    ((2 
						      & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						      ? 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? 0
						       : (IData)(vlTOPp->v__DOT__ALU__DOT__HILO))
						      : 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? (IData)(
								 (vlTOPp->v__DOT__ALU__DOT__HILO 
								  >> 0x20))
						       : 0)))
						    : 
						   ((4 
						     & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						     ? 
						    ((2 
						      & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						      ? 0
						      : 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? 0
						       : 
						      (((((((((0x80000000 
							       == 
							       (0x80000000 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd)) 
							      | (0x40000000 
								 == 
								 (0xc0000000 
								  & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							     | (0x20000000 
								== 
								(0xe0000000 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							    | (0x10000000 
							       == 
							       (0xf0000000 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							   | (0x8000000 
							      == 
							      (0xf8000000 
							       & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							  | (0x4000000 
							     == 
							     (0xfc000000 
							      & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							 | (0x2000000 
							    == 
							    (0xfe000000 
							     & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							| (0x1000000 
							   == 
							   (0xff000000 
							    & vlTOPp->v__DOT__EX_ReadData1_Fwd)))
						        ? 
						       ((0x80000000 
							 == 
							 (0x80000000 
							  & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							 ? 0
							 : 
							((0x40000000 
							  == 
							  (0xc0000000 
							   & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							  ? 1
							  : 
							 ((0x20000000 
							   == 
							   (0xe0000000 
							    & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							   ? 2
							   : 
							  ((0x10000000 
							    == 
							    (0xf0000000 
							     & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							    ? 3
							    : 
							   ((0x8000000 
							     == 
							     (0xf8000000 
							      & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							     ? 4
							     : 
							    ((0x4000000 
							      == 
							      (0xfc000000 
							       & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							      ? 5
							      : 
							     ((0x2000000 
							       == 
							       (0xfe000000 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))
							       ? 6
							       : 7)))))))
						        : 
						       (((((((((0x800000 
								== 
								(0xff800000 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd)) 
							       | (0x400000 
								  == 
								  (0xffc00000 
								   & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							      | (0x200000 
								 == 
								 (0xffe00000 
								  & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							     | (0x100000 
								== 
								(0xfff00000 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							    | (0x80000 
							       == 
							       (0xfff80000 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							   | (0x40000 
							      == 
							      (0xfffc0000 
							       & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							  | (0x20000 
							     == 
							     (0xfffe0000 
							      & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							 | (0x10000 
							    == 
							    (0xffff0000 
							     & vlTOPp->v__DOT__EX_ReadData1_Fwd)))
							 ? 
							((0x800000 
							  == 
							  (0xff800000 
							   & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							  ? 8
							  : 
							 ((0x400000 
							   == 
							   (0xffc00000 
							    & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							   ? 9
							   : 
							  ((0x200000 
							    == 
							    (0xffe00000 
							     & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							    ? 0xa
							    : 
							   ((0x100000 
							     == 
							     (0xfff00000 
							      & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							     ? 0xb
							     : 
							    ((0x80000 
							      == 
							      (0xfff80000 
							       & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							      ? 0xc
							      : 
							     ((0x40000 
							       == 
							       (0xfffc0000 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))
							       ? 0xd
							       : 
							      ((0x20000 
								== 
								(0xfffe0000 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							        ? 0xe
							        : 0xf)))))))
							 : 
							(((((((((0x8000 
								 == 
								 (0xffff8000 
								  & vlTOPp->v__DOT__EX_ReadData1_Fwd)) 
								| (0x4000 
								   == 
								   (0xffffc000 
								    & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							       | (0x2000 
								  == 
								  (0xffffe000 
								   & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							      | (0x1000 
								 == 
								 (0xfffff000 
								  & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							     | (0x800 
								== 
								(0xfffff800 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							    | (0x400 
							       == 
							       (0xfffffc00 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							   | (0x200 
							      == 
							      (0xfffffe00 
							       & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							  | (0x100 
							     == 
							     (0xffffff00 
							      & vlTOPp->v__DOT__EX_ReadData1_Fwd)))
							  ? 
							 ((0x8000 
							   == 
							   (0xffff8000 
							    & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							   ? 0x10
							   : 
							  ((0x4000 
							    == 
							    (0xffffc000 
							     & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							    ? 0x11
							    : 
							   ((0x2000 
							     == 
							     (0xffffe000 
							      & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							     ? 0x12
							     : 
							    ((0x1000 
							      == 
							      (0xfffff000 
							       & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							      ? 0x13
							      : 
							     ((0x800 
							       == 
							       (0xfffff800 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))
							       ? 0x14
							       : 
							      ((0x400 
								== 
								(0xfffffc00 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							        ? 0x15
							        : 
							       ((0x200 
								 == 
								 (0xfffffe00 
								  & vlTOPp->v__DOT__EX_ReadData1_Fwd))
								 ? 0x16
								 : 0x17)))))))
							  : 
							 (((((((((0x80 
								  == 
								  (0xffffff80 
								   & vlTOPp->v__DOT__EX_ReadData1_Fwd)) 
								 | (0x40 
								    == 
								    (0xffffffc0 
								     & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
								| (0x20 
								   == 
								   (0xffffffe0 
								    & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							       | (0x10 
								  == 
								  (0xfffffff0 
								   & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							      | (8 
								 == 
								 (0xfffffff8 
								  & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							     | (4 
								== 
								(0xfffffffc 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							    | (2 
							       == 
							       (0xfffffffe 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							   | (1 
							      == vlTOPp->v__DOT__EX_ReadData1_Fwd))
							   ? 
							  ((0x80 
							    == 
							    (0xffffff80 
							     & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							    ? 0x18
							    : 
							   ((0x40 
							     == 
							     (0xffffffc0 
							      & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							     ? 0x19
							     : 
							    ((0x20 
							      == 
							      (0xffffffe0 
							       & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							      ? 0x1a
							      : 
							     ((0x10 
							       == 
							       (0xfffffff0 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))
							       ? 0x1b
							       : 
							      ((8 
								== 
								(0xfffffff8 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							        ? 0x1c
							        : 
							       ((4 
								 == 
								 (0xfffffffc 
								  & vlTOPp->v__DOT__EX_ReadData1_Fwd))
								 ? 0x1d
								 : 
								((2 
								  == 
								  (0xfffffffe 
								   & vlTOPp->v__DOT__EX_ReadData1_Fwd))
								  ? 0x1e
								  : 0x1f)))))))
							   : 
							  ((0 
							    == vlTOPp->v__DOT__EX_ReadData1_Fwd)
							    ? 0x20
							    : 0)))))))
						     : 
						    ((2 
						      & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						      ? 
						     ((1 
						       & (IData)(vlTOPp->v__DOT__EX_ALUOp))
						       ? 
						      (((((((((0 
							       == 
							       (0x80000000 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd)) 
							      | (0x80000000 
								 == 
								 (0xc0000000 
								  & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							     | (0xc0000000 
								== 
								(0xe0000000 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							    | (0xe0000000 
							       == 
							       (0xf0000000 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							   | (0xf0000000 
							      == 
							      (0xf8000000 
							       & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							  | (0xf8000000 
							     == 
							     (0xfc000000 
							      & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							 | (0xfc000000 
							    == 
							    (0xfe000000 
							     & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							| (0xfe000000 
							   == 
							   (0xff000000 
							    & vlTOPp->v__DOT__EX_ReadData1_Fwd)))
						        ? 
						       ((0 
							 == 
							 (0x80000000 
							  & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							 ? 0
							 : 
							((0x80000000 
							  == 
							  (0xc0000000 
							   & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							  ? 1
							  : 
							 ((0xc0000000 
							   == 
							   (0xe0000000 
							    & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							   ? 2
							   : 
							  ((0xe0000000 
							    == 
							    (0xf0000000 
							     & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							    ? 3
							    : 
							   ((0xf0000000 
							     == 
							     (0xf8000000 
							      & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							     ? 4
							     : 
							    ((0xf8000000 
							      == 
							      (0xfc000000 
							       & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							      ? 5
							      : 
							     ((0xfc000000 
							       == 
							       (0xfe000000 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))
							       ? 6
							       : 7)))))))
						        : 
						       (((((((((0xff000000 
								== 
								(0xff800000 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd)) 
							       | (0xff800000 
								  == 
								  (0xffc00000 
								   & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							      | (0xffc00000 
								 == 
								 (0xffe00000 
								  & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							     | (0xffe00000 
								== 
								(0xfff00000 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							    | (0xfff00000 
							       == 
							       (0xfff80000 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							   | (0xfff80000 
							      == 
							      (0xfffc0000 
							       & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							  | (0xfffc0000 
							     == 
							     (0xfffe0000 
							      & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							 | (0xfffe0000 
							    == 
							    (0xffff0000 
							     & vlTOPp->v__DOT__EX_ReadData1_Fwd)))
							 ? 
							((0xff000000 
							  == 
							  (0xff800000 
							   & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							  ? 8
							  : 
							 ((0xff800000 
							   == 
							   (0xffc00000 
							    & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							   ? 9
							   : 
							  ((0xffc00000 
							    == 
							    (0xffe00000 
							     & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							    ? 0xa
							    : 
							   ((0xffe00000 
							     == 
							     (0xfff00000 
							      & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							     ? 0xb
							     : 
							    ((0xfff00000 
							      == 
							      (0xfff80000 
							       & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							      ? 0xc
							      : 
							     ((0xfff80000 
							       == 
							       (0xfffc0000 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))
							       ? 0xd
							       : 
							      ((0xfffc0000 
								== 
								(0xfffe0000 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							        ? 0xe
							        : 0xf)))))))
							 : 
							(((((((((0xffff0000 
								 == 
								 (0xffff8000 
								  & vlTOPp->v__DOT__EX_ReadData1_Fwd)) 
								| (0xffff8000 
								   == 
								   (0xffffc000 
								    & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							       | (0xffffc000 
								  == 
								  (0xffffe000 
								   & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							      | (0xffffe000 
								 == 
								 (0xfffff000 
								  & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							     | (0xfffff000 
								== 
								(0xfffff800 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							    | (0xfffff800 
							       == 
							       (0xfffffc00 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							   | (0xfffffc00 
							      == 
							      (0xfffffe00 
							       & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							  | (0xfffffe00 
							     == 
							     (0xffffff00 
							      & vlTOPp->v__DOT__EX_ReadData1_Fwd)))
							  ? 
							 ((0xffff0000 
							   == 
							   (0xffff8000 
							    & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							   ? 0x10
							   : 
							  ((0xffff8000 
							    == 
							    (0xffffc000 
							     & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							    ? 0x11
							    : 
							   ((0xffffc000 
							     == 
							     (0xffffe000 
							      & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							     ? 0x12
							     : 
							    ((0xffffe000 
							      == 
							      (0xfffff000 
							       & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							      ? 0x13
							      : 
							     ((0xfffff000 
							       == 
							       (0xfffff800 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))
							       ? 0x14
							       : 
							      ((0xfffff800 
								== 
								(0xfffffc00 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							        ? 0x15
							        : 
							       ((0xfffffc00 
								 == 
								 (0xfffffe00 
								  & vlTOPp->v__DOT__EX_ReadData1_Fwd))
								 ? 0x16
								 : 0x17)))))))
							  : 
							 (((((((((0xffffff00 
								  == 
								  (0xffffff80 
								   & vlTOPp->v__DOT__EX_ReadData1_Fwd)) 
								 | (0xffffff80 
								    == 
								    (0xffffffc0 
								     & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
								| (0xffffffc0 
								   == 
								   (0xffffffe0 
								    & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							       | (0xffffffe0 
								  == 
								  (0xfffffff0 
								   & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							      | (0xfffffff0 
								 == 
								 (0xfffffff8 
								  & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							     | (0xfffffff8 
								== 
								(0xfffffffc 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							    | (0xfffffffc 
							       == 
							       (0xfffffffe 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))) 
							   | (0xfffffffe 
							      == vlTOPp->v__DOT__EX_ReadData1_Fwd))
							   ? 
							  ((0xffffff00 
							    == 
							    (0xffffff80 
							     & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							    ? 0x18
							    : 
							   ((0xffffff80 
							     == 
							     (0xffffffc0 
							      & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							     ? 0x19
							     : 
							    ((0xffffffc0 
							      == 
							      (0xffffffe0 
							       & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							      ? 0x1a
							      : 
							     ((0xffffffe0 
							       == 
							       (0xfffffff0 
								& vlTOPp->v__DOT__EX_ReadData1_Fwd))
							       ? 0x1b
							       : 
							      ((0xfffffff0 
								== 
								(0xfffffff8 
								 & vlTOPp->v__DOT__EX_ReadData1_Fwd))
							        ? 0x1c
							        : 
							       ((0xfffffff8 
								 == 
								 (0xfffffffc 
								  & vlTOPp->v__DOT__EX_ReadData1_Fwd))
								 ? 0x1d
								 : 
								((0xfffffffc 
								  == 
								  (0xfffffffe 
								   & vlTOPp->v__DOT__EX_ReadData1_Fwd))
								  ? 0x1e
								  : 0x1f)))))))
							   : 
							  ((0xffffffff 
							    == vlTOPp->v__DOT__EX_ReadData1_Fwd)
							    ? 0x20
							    : 0)))))
						       : 
						      (vlTOPp->v__DOT__EX_ReadData1_Fwd 
						       & vlTOPp->v__DOT__EX_ReadData2_Imm))
						      : vlTOPp->v__DOT__ALU__DOT__AddSub_Result))))));
    __Vdly__v__DOT__M_ReadData2_PR = ((IData)(vlTOPp->reset)
				       ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
					       ? vlTOPp->v__DOT__M_ReadData2_PR
					       : vlTOPp->v__DOT__EX_ReadData2_Fwd));
    __Vdly__v__DOT__M_RtRd = ((IData)(vlTOPp->reset)
			       ? 0 : ((IData)(vlTOPp->v__DOT__M_Stall)
				       ? (IData)(vlTOPp->v__DOT__M_RtRd)
				       : (IData)(vlTOPp->v__DOT__EX_RtRd)));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/IFID_Stage.v:65
    __Vdly__v__DOT__Instruction = ((IData)(vlTOPp->reset)
				    ? 0 : ((IData)(vlTOPp->v__DOT__ID_Stall)
					    ? vlTOPp->v__DOT__Instruction
					    : (((IData)(vlTOPp->v__DOT__IF_Stall) 
						| ((((((IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect) 
						       | (IData)(vlTOPp->v__DOT__EX_EXC_Ov)) 
						      | (IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Detect)) 
						     | (IData)(vlTOPp->v__DOT__IF_EXC_AdIF)) 
						    | ((IData)(vlTOPp->v__DOT__ID_Eret) 
						       & (~ (IData)(vlTOPp->v__DOT__ID_Stall)))) 
						   | (IData)(vlTOPp->v__DOT__CP0__DOT__reset_r)))
					        ? 0
					        : ((IData)(vlTOPp->v__DOT__IF_Stall)
						    ? 0
						    : vlTOPp->InstMem_In))));
    __Vdly__v__DOT__ID_PCAdd4 = ((IData)(vlTOPp->reset)
				  ? 0 : ((IData)(vlTOPp->v__DOT__ID_Stall)
					  ? vlTOPp->v__DOT__ID_PCAdd4
					  : ((IData)(4) 
					     + vlTOPp->v__DOT__IF_PCOut)));
    __Vdly__v__DOT__ID_IsBDS = (1 & ((IData)(vlTOPp->reset)
				      ? 0 : ((IData)(vlTOPp->v__DOT__ID_Stall)
					      ? (IData)(vlTOPp->v__DOT__ID_IsBDS)
					      : (IData)(vlTOPp->v__DOT__ID_NextIsDelay))));
    __Vdly__v__DOT__ID_RestartPC = ((IData)(vlTOPp->reset)
				     ? 0 : (((IData)(vlTOPp->v__DOT__ID_Stall) 
					     | (IData)(vlTOPp->v__DOT__ID_NextIsDelay))
					     ? vlTOPp->v__DOT__ID_RestartPC
					     : vlTOPp->v__DOT__IF_PCOut));
    __Vdly__v__DOT__ID_IsFlushed = (1 & ((IData)(vlTOPp->reset)
					  ? 0 : ((IData)(vlTOPp->v__DOT__ID_Stall)
						  ? (IData)(vlTOPp->v__DOT__ID_IsFlushed)
						  : 
						 ((((((IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect) 
						      | (IData)(vlTOPp->v__DOT__EX_EXC_Ov)) 
						     | (IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Detect)) 
						    | (IData)(vlTOPp->v__DOT__IF_EXC_AdIF)) 
						   | ((IData)(vlTOPp->v__DOT__ID_Eret) 
						      & (~ (IData)(vlTOPp->v__DOT__ID_Stall)))) 
						  | (IData)(vlTOPp->v__DOT__CP0__DOT__reset_r)))));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/IDEX_Stage.v:122
    __Vdly__v__DOT__EX_Link = (1 & ((IData)(vlTOPp->reset)
				     ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					     ? (IData)(vlTOPp->v__DOT__EX_Link)
					     : (1 & 
						((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						 >> 0xd)))));
    __Vdly__v__DOT__IDEX__DOT__EX_RegDst = (1 & ((IData)(vlTOPp->reset)
						  ? 0
						  : 
						 ((IData)(vlTOPp->v__DOT__EX_Stall)
						   ? (IData)(vlTOPp->v__DOT__IDEX__DOT__EX_RegDst)
						   : 
						  (1 
						   & ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						      >> 8)))));
    __Vdly__v__DOT__EX_ALUSrcImm = (1 & ((IData)(vlTOPp->reset)
					  ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
						  ? (IData)(vlTOPp->v__DOT__EX_ALUSrcImm)
						  : 
						 (1 
						  & ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						     >> 0xc)))));
    __Vdly__v__DOT__EX_ALUOp = ((IData)(vlTOPp->reset)
				 ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					 ? (IData)(vlTOPp->v__DOT__EX_ALUOp)
					 : (((IData)(vlTOPp->v__DOT__ID_Stall) 
					     | (IData)(vlTOPp->v__DOT__ID_Exception_Flush))
					     ? 0 : 
					    ((IData)(vlTOPp->v__DOT__ID_Stall)
					      ? 0 : 
					     ((0x80000000 
					       & vlTOPp->v__DOT__Instruction)
					       ? 0 : 
					      ((0x40000000 
						& vlTOPp->v__DOT__Instruction)
					        ? (
						   (0x20000000 
						    & vlTOPp->v__DOT__Instruction)
						    ? 
						   ((0x10000000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 
						    ((0x8000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 0
						      : 
						     ((0x4000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0
						       : 
						      ((0x20 
							& vlTOPp->v__DOT__Instruction)
						        ? 
						       ((0x10 
							 & vlTOPp->v__DOT__Instruction)
							 ? 0
							 : 
							((8 
							  & vlTOPp->v__DOT__Instruction)
							  ? 0
							  : 
							 ((4 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((2 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 3
							     : 4)))))
						        : 
						       ((0x10 
							 & vlTOPp->v__DOT__Instruction)
							 ? 0
							 : 
							((8 
							  & vlTOPp->v__DOT__Instruction)
							  ? 0
							  : 
							 ((4 
							   & vlTOPp->v__DOT__Instruction)
							   ? 
							  ((2 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0xe
							     : 0xd))
							   : 
							  ((2 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0
							     : 0xf)
							    : 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 8
							     : 7))))))))
						     : 0)
						    : 0)
					        : (
						   (0x20000000 
						    & vlTOPp->v__DOT__Instruction)
						    ? 
						   ((0x10000000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 
						    ((0x8000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 
						     ((0x4000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0x15
						       : 0x1f)
						      : 
						     ((0x4000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0x13
						       : 2))
						     : 
						    ((0x8000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 
						     ((0x4000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0x18
						       : 0x17)
						      : 
						     ((0x4000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0
						       : 1)))
						    : 
						   ((0x10000000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 0
						     : 
						    ((0x8000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 0
						      : 
						     ((0x4000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 
						      ((0x100000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0
						        : 
						       ((0x80000 
							 & vlTOPp->v__DOT__Instruction)
							 ? 
							((0x40000 
							  & vlTOPp->v__DOT__Instruction)
							  ? 
							 ((0x20000 
							   & vlTOPp->v__DOT__Instruction)
							   ? 
							  ((0x10000 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 0x1e)
							   : 
							  ((0x10000 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 0x1e))
							  : 
							 ((0x20000 
							   & vlTOPp->v__DOT__Instruction)
							   ? 
							  ((0x10000 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0x18
							    : 0x17)
							   : 
							  ((0x10000 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0x18
							    : 0x17)))
							 : 0))
						       : 
						      ((0x20 
							& vlTOPp->v__DOT__Instruction)
						        ? 
						       ((0x10 
							 & vlTOPp->v__DOT__Instruction)
							 ? 
							((8 
							  & vlTOPp->v__DOT__Instruction)
							  ? 0
							  : 
							 ((4 
							   & vlTOPp->v__DOT__Instruction)
							   ? 
							  ((2 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0
							     : 0x1e)
							    : 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0
							     : 0x1e))
							   : 
							  ((2 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x18
							     : 0x17)
							    : 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x18
							     : 0x17))))
							 : 
							((8 
							  & vlTOPp->v__DOT__Instruction)
							  ? 
							 ((4 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((2 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x18
							     : 0x17)
							    : 0))
							  : 
							 ((4 
							   & vlTOPp->v__DOT__Instruction)
							   ? 
							  ((2 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x12
							     : 0x1f)
							    : 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x13
							     : 2))
							   : 
							  ((2 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x1e
							     : 0x1d)
							    : 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0
							     : 1)))))
						        : 
						       ((0x10 
							 & vlTOPp->v__DOT__Instruction)
							 ? 
							((8 
							  & vlTOPp->v__DOT__Instruction)
							  ? 
							 ((4 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((2 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 6
							     : 5)
							    : 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x11
							     : 0x10)))
							  : 
							 ((4 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((2 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0xc
							     : 0xa)
							    : 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0xb
							     : 9))))
							 : 
							((8 
							  & vlTOPp->v__DOT__Instruction)
							  ? 0
							  : 
							 ((4 
							   & vlTOPp->v__DOT__Instruction)
							   ? 
							  ((2 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x1a
							     : 0x1c)
							    : 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0
							     : 0x16))
							   : 
							  ((2 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x19
							     : 0x1b)
							    : 
							   ((1 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0
							     : 0x14))))))))))))))));
    __Vdly__v__DOT__EX_Movn = (1 & ((IData)(vlTOPp->reset)
				     ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					     ? (IData)(vlTOPp->v__DOT__EX_Movn)
					     : (1 & 
						(((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						  >> 0xb) 
						 & vlTOPp->v__DOT__Instruction)))));
    __Vdly__v__DOT__EX_Movz = (1 & ((IData)(vlTOPp->reset)
				     ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					     ? (IData)(vlTOPp->v__DOT__EX_Movz)
					     : (1 & 
						(((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						  >> 0xb) 
						 & (~ vlTOPp->v__DOT__Instruction))))));
    __Vdly__v__DOT__EX_LLSC = (1 & ((IData)(vlTOPp->reset)
				     ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					     ? (IData)(vlTOPp->v__DOT__EX_LLSC)
					     : (1 & 
						((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						 >> 7)))));
    __Vdly__v__DOT__EX_MemRead = (1 & ((IData)(vlTOPp->reset)
				        ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					        ? (IData)(vlTOPp->v__DOT__EX_MemRead)
					        : (
						   ((IData)(vlTOPp->v__DOT__ID_Stall) 
						    | (IData)(vlTOPp->v__DOT__ID_Exception_Flush))
						    ? 0
						    : 
						   (1 
						    & ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						       >> 6))))));
    __Vdly__v__DOT__EX_MemWrite = (1 & ((IData)(vlTOPp->reset)
					 ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
						 ? (IData)(vlTOPp->v__DOT__EX_MemWrite)
						 : 
						(((IData)(vlTOPp->v__DOT__ID_Stall) 
						  | (IData)(vlTOPp->v__DOT__ID_Exception_Flush))
						  ? 0
						  : 
						 (1 
						  & ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						     >> 5))))));
    __Vdly__v__DOT__EX_MemByte = (1 & ((IData)(vlTOPp->reset)
				        ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					        ? (IData)(vlTOPp->v__DOT__EX_MemByte)
					        : (1 
						   & ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						      >> 3)))));
    __Vdly__v__DOT__EX_MemHalf = (1 & ((IData)(vlTOPp->reset)
				        ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					        ? (IData)(vlTOPp->v__DOT__EX_MemHalf)
					        : (1 
						   & ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						      >> 4)))));
    __Vdly__v__DOT__EX_MemSignExtend = (1 & ((IData)(vlTOPp->reset)
					      ? 0 : 
					     ((IData)(vlTOPp->v__DOT__EX_Stall)
					       ? (IData)(vlTOPp->v__DOT__EX_MemSignExtend)
					       : (1 
						  & ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						     >> 2)))));
    __Vdly__v__DOT__EX_Left = (1 & ((IData)(vlTOPp->reset)
				     ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					     ? (IData)(vlTOPp->v__DOT__EX_Left)
					     : ((IData)(vlTOPp->v__DOT__Controller__DOT__Unaligned_Mem) 
						& (~ 
						   (vlTOPp->v__DOT__Instruction 
						    >> 0x1c))))));
    __Vdly__v__DOT__EX_Right = (1 & ((IData)(vlTOPp->reset)
				      ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					      ? (IData)(vlTOPp->v__DOT__EX_Right)
					      : ((IData)(vlTOPp->v__DOT__Controller__DOT__Unaligned_Mem) 
						 & (vlTOPp->v__DOT__Instruction 
						    >> 0x1c)))));
    __Vdly__v__DOT__EX_RegWrite = (1 & ((IData)(vlTOPp->reset)
					 ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
						 ? (IData)(vlTOPp->v__DOT__EX_RegWrite)
						 : 
						(((IData)(vlTOPp->v__DOT__ID_Stall) 
						  | (IData)(vlTOPp->v__DOT__ID_Exception_Flush))
						  ? 0
						  : 
						 (1 
						  & ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						     >> 1))))));
    __Vdly__v__DOT__EX_MemtoReg = (1 & ((IData)(vlTOPp->reset)
					 ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
						 ? (IData)(vlTOPp->v__DOT__EX_MemtoReg)
						 : 
						(1 
						 & (IData)(vlTOPp->v__DOT__Controller__DOT__Datapath)))));
    __Vdly__v__DOT__EX_ReverseEndian = (1 & ((IData)(vlTOPp->reset)
					      ? 0 : 
					     ((IData)(vlTOPp->v__DOT__EX_Stall)
					       ? (IData)(vlTOPp->v__DOT__EX_ReverseEndian)
					       : (IData)(vlTOPp->v__DOT__CP0__DOT__Status_RE))));
    __Vdly__v__DOT__EX_RestartPC = ((IData)(vlTOPp->reset)
				     ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					     ? vlTOPp->v__DOT__EX_RestartPC
					     : vlTOPp->v__DOT__ID_RestartPC));
    __Vdly__v__DOT__EX_IsBDS = (1 & ((IData)(vlTOPp->reset)
				      ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					      ? (IData)(vlTOPp->v__DOT__EX_IsBDS)
					      : (IData)(vlTOPp->v__DOT__ID_IsBDS))));
    __Vdly__v__DOT__EX_Trap = (1 & ((IData)(vlTOPp->reset)
				     ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					     ? (IData)(vlTOPp->v__DOT__EX_Trap)
					     : (((IData)(vlTOPp->v__DOT__ID_Stall) 
						 | (IData)(vlTOPp->v__DOT__ID_Exception_Flush))
						 ? 0
						 : 
						(1 
						 & ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						    >> 0xa))))));
    __Vdly__v__DOT__EX_TrapCond = (1 & ((IData)(vlTOPp->reset)
					 ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
						 ? (IData)(vlTOPp->v__DOT__EX_TrapCond)
						 : 
						(1 
						 & ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						    >> 9)))));
    __Vdly__v__DOT__EX_EX_CanErr = (1 & ((IData)(vlTOPp->reset)
					  ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
						  ? (IData)(vlTOPp->v__DOT__EX_EX_CanErr)
						  : 
						 (((IData)(vlTOPp->v__DOT__ID_Stall) 
						   | (IData)(vlTOPp->v__DOT__ID_Exception_Flush))
						   ? 0
						   : 
						  (1 
						   & ((IData)(vlTOPp->v__DOT__Controller__DOT__DP_Exceptions) 
						      >> 1))))));
    __Vdly__v__DOT__EX_M_CanErr = (1 & ((IData)(vlTOPp->reset)
					 ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
						 ? (IData)(vlTOPp->v__DOT__EX_M_CanErr)
						 : 
						(((IData)(vlTOPp->v__DOT__ID_Stall) 
						  | (IData)(vlTOPp->v__DOT__ID_Exception_Flush))
						  ? 0
						  : 
						 (1 
						  & (IData)(vlTOPp->v__DOT__Controller__DOT__DP_Exceptions))))));
    __Vdly__v__DOT__EX_ReadData1_PR = ((IData)(vlTOPp->reset)
				        ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					        ? vlTOPp->v__DOT__EX_ReadData1_PR
					        : vlTOPp->v__DOT__ID_ReadData1_End));
    __Vdly__v__DOT__EX_ReadData2_PR = ((IData)(vlTOPp->reset)
				        ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					        ? vlTOPp->v__DOT__EX_ReadData2_PR
					        : vlTOPp->v__DOT__ID_ReadData2_End));
    __Vdly__v__DOT__IDEX__DOT__EX_SignExtImm_pre = 
	(0x1ffff & ((IData)(vlTOPp->reset) ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
						   ? vlTOPp->v__DOT__IDEX__DOT__EX_SignExtImm_pre
						   : vlTOPp->v__DOT__ID_SignExtImm)));
    __Vdly__v__DOT__EX_Rs = (0x1f & ((IData)(vlTOPp->reset)
				      ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					      ? (IData)(vlTOPp->v__DOT__EX_Rs)
					      : (vlTOPp->v__DOT__Instruction 
						 >> 0x15))));
    __Vdly__v__DOT__EX_Rt = (0x1f & ((IData)(vlTOPp->reset)
				      ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
					      ? (IData)(vlTOPp->v__DOT__EX_Rt)
					      : (vlTOPp->v__DOT__Instruction 
						 >> 0x10))));
    __Vdly__v__DOT__EX_WantRsByEX = (1 & ((IData)(vlTOPp->reset)
					   ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
						   ? (IData)(vlTOPp->v__DOT__EX_WantRsByEX)
						   : 
						  (((IData)(vlTOPp->v__DOT__ID_Stall) 
						    | (IData)(vlTOPp->v__DOT__ID_Exception_Flush))
						    ? 0
						    : 
						   (1 
						    & ((IData)(vlTOPp->v__DOT__ID_DP_Hazards) 
						       >> 3))))));
    __Vdly__v__DOT__EX_NeedRsByEX = (1 & ((IData)(vlTOPp->reset)
					   ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
						   ? (IData)(vlTOPp->v__DOT__EX_NeedRsByEX)
						   : 
						  (((IData)(vlTOPp->v__DOT__ID_Stall) 
						    | (IData)(vlTOPp->v__DOT__ID_Exception_Flush))
						    ? 0
						    : 
						   (1 
						    & ((IData)(vlTOPp->v__DOT__ID_DP_Hazards) 
						       >> 2))))));
    __Vdly__v__DOT__EX_WantRtByEX = (1 & ((IData)(vlTOPp->reset)
					   ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
						   ? (IData)(vlTOPp->v__DOT__EX_WantRtByEX)
						   : 
						  (((IData)(vlTOPp->v__DOT__ID_Stall) 
						    | (IData)(vlTOPp->v__DOT__ID_Exception_Flush))
						    ? 0
						    : 
						   (1 
						    & ((IData)(vlTOPp->v__DOT__ID_DP_Hazards) 
						       >> 1))))));
    __Vdly__v__DOT__EX_NeedRtByEX = (1 & ((IData)(vlTOPp->reset)
					   ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
						   ? (IData)(vlTOPp->v__DOT__EX_NeedRtByEX)
						   : 
						  (((IData)(vlTOPp->v__DOT__ID_Stall) 
						    | (IData)(vlTOPp->v__DOT__ID_Exception_Flush))
						    ? 0
						    : 
						   (1 
						    & (IData)(vlTOPp->v__DOT__ID_DP_Hazards))))));
    __Vdly__v__DOT__EX_KernelMode = (1 & ((IData)(vlTOPp->reset)
					   ? 0 : ((IData)(vlTOPp->v__DOT__EX_Stall)
						   ? (IData)(vlTOPp->v__DOT__EX_KernelMode)
						   : (IData)(vlTOPp->v__DOT__ID_KernelMode))));
    vlTOPp->v__DOT__DataMem_Controller__DOT__RW_Mask 
	= __Vdly__v__DOT__DataMem_Controller__DOT__RW_Mask;
    vlTOPp->v__DOT__RegisterFile__DOT__registers29 
	= __Vdly__v__DOT__RegisterFile__DOT__registers29;
    vlTOPp->v__DOT__RegisterFile__DOT__registers11 
	= __Vdly__v__DOT__RegisterFile__DOT__registers11;
    vlTOPp->v__DOT__RegisterFile__DOT__registers31 
	= __Vdly__v__DOT__RegisterFile__DOT__registers31;
    vlTOPp->v__DOT__RegisterFile__DOT__registers28 
	= __Vdly__v__DOT__RegisterFile__DOT__registers28;
    vlTOPp->v__DOT__RegisterFile__DOT__registers30 
	= __Vdly__v__DOT__RegisterFile__DOT__registers30;
    vlTOPp->v__DOT__RegisterFile__DOT__registers10 
	= __Vdly__v__DOT__RegisterFile__DOT__registers10;
    vlTOPp->v__DOT__RegisterFile__DOT__registers12 
	= __Vdly__v__DOT__RegisterFile__DOT__registers12;
    vlTOPp->v__DOT__RegisterFile__DOT__registers27 
	= __Vdly__v__DOT__RegisterFile__DOT__registers27;
    vlTOPp->v__DOT__RegisterFile__DOT__registers21 
	= __Vdly__v__DOT__RegisterFile__DOT__registers21;
    vlTOPp->v__DOT__RegisterFile__DOT__registers26 
	= __Vdly__v__DOT__RegisterFile__DOT__registers26;
    vlTOPp->v__DOT__RegisterFile__DOT__registers13 
	= __Vdly__v__DOT__RegisterFile__DOT__registers13;
    vlTOPp->v__DOT__RegisterFile__DOT__registers25 
	= __Vdly__v__DOT__RegisterFile__DOT__registers25;
    vlTOPp->v__DOT__RegisterFile__DOT__registers14 
	= __Vdly__v__DOT__RegisterFile__DOT__registers14;
    vlTOPp->v__DOT__RegisterFile__DOT__registers24 
	= __Vdly__v__DOT__RegisterFile__DOT__registers24;
    vlTOPp->v__DOT__RegisterFile__DOT__registers15 
	= __Vdly__v__DOT__RegisterFile__DOT__registers15;
    vlTOPp->v__DOT__RegisterFile__DOT__registers23 
	= __Vdly__v__DOT__RegisterFile__DOT__registers23;
    vlTOPp->v__DOT__RegisterFile__DOT__registers16 
	= __Vdly__v__DOT__RegisterFile__DOT__registers16;
    vlTOPp->v__DOT__RegisterFile__DOT__registers17 
	= __Vdly__v__DOT__RegisterFile__DOT__registers17;
    vlTOPp->v__DOT__RegisterFile__DOT__registers6 = __Vdly__v__DOT__RegisterFile__DOT__registers6;
    vlTOPp->v__DOT__RegisterFile__DOT__registers18 
	= __Vdly__v__DOT__RegisterFile__DOT__registers18;
    vlTOPp->v__DOT__RegisterFile__DOT__registers22 
	= __Vdly__v__DOT__RegisterFile__DOT__registers22;
    vlTOPp->v__DOT__RegisterFile__DOT__registers19 
	= __Vdly__v__DOT__RegisterFile__DOT__registers19;
    vlTOPp->v__DOT__RegisterFile__DOT__registers20 
	= __Vdly__v__DOT__RegisterFile__DOT__registers20;
    vlTOPp->v__DOT__RegisterFile__DOT__registers8 = __Vdly__v__DOT__RegisterFile__DOT__registers8;
    vlTOPp->v__DOT__RegisterFile__DOT__registers7 = __Vdly__v__DOT__RegisterFile__DOT__registers7;
    vlTOPp->v__DOT__RegisterFile__DOT__registers5 = __Vdly__v__DOT__RegisterFile__DOT__registers5;
    vlTOPp->v__DOT__RegisterFile__DOT__registers4 = __Vdly__v__DOT__RegisterFile__DOT__registers4;
    vlTOPp->v__DOT__RegisterFile__DOT__registers3 = __Vdly__v__DOT__RegisterFile__DOT__registers3;
    vlTOPp->v__DOT__RegisterFile__DOT__registers2 = __Vdly__v__DOT__RegisterFile__DOT__registers2;
    vlTOPp->v__DOT__RegisterFile__DOT__registers1 = __Vdly__v__DOT__RegisterFile__DOT__registers1;
    vlTOPp->v__DOT__RegisterFile__DOT__registers9 = __Vdly__v__DOT__RegisterFile__DOT__registers9;
    vlTOPp->v__DOT__CP0__DOT__Status_BEV = __Vdly__v__DOT__CP0__DOT__Status_BEV;
    vlTOPp->v__DOT__CP0__DOT__Status_NMI = __Vdly__v__DOT__CP0__DOT__Status_NMI;
    vlTOPp->v__DOT__CP0__DOT__ErrorEPC = __Vdly__v__DOT__CP0__DOT__ErrorEPC;
    vlTOPp->v__DOT__CP0__DOT__Status_ERL = __Vdly__v__DOT__CP0__DOT__Status_ERL;
    vlTOPp->v__DOT__CP0__DOT__Compare = __Vdly__v__DOT__CP0__DOT__Compare;
    vlTOPp->v__DOT__CP0__DOT__Count = __Vdly__v__DOT__CP0__DOT__Count;
    vlTOPp->v__DOT__CP0__DOT__Cause_IV = __Vdly__v__DOT__CP0__DOT__Cause_IV;
    vlTOPp->v__DOT__CP0__DOT__Status_CU_0 = __Vdly__v__DOT__CP0__DOT__Status_CU_0;
    vlTOPp->v__DOT__CP0__DOT__Status_IM = __Vdly__v__DOT__CP0__DOT__Status_IM;
    vlTOPp->v__DOT__CP0__DOT__Status_IE = __Vdly__v__DOT__CP0__DOT__Status_IE;
    vlTOPp->v__DOT__CP0__DOT__Cause_IP = __Vdly__v__DOT__CP0__DOT__Cause_IP;
    vlTOPp->v__DOT__CP0__DOT__Status_UM = __Vdly__v__DOT__CP0__DOT__Status_UM;
    vlTOPp->v__DOT__CP0__DOT__Cause_BD = __Vdly__v__DOT__CP0__DOT__Cause_BD;
    vlTOPp->v__DOT__CP0__DOT__BadVAddr = __Vdly__v__DOT__CP0__DOT__BadVAddr;
    vlTOPp->v__DOT__CP0__DOT__Cause_ExcCode30 = __Vdly__v__DOT__CP0__DOT__Cause_ExcCode30;
    vlTOPp->v__DOT__CP0__DOT__Cause_CE = __Vdly__v__DOT__CP0__DOT__Cause_CE;
    vlTOPp->v__DOT__CP0__DOT__EPC = __Vdly__v__DOT__CP0__DOT__EPC;
    vlTOPp->v__DOT__CP0__DOT__Status_EXL = __Vdly__v__DOT__CP0__DOT__Status_EXL;
    vlTOPp->v__DOT__DataMem_Controller__DOT__LLSC_Atomic 
	= __Vdly__v__DOT__DataMem_Controller__DOT__LLSC_Atomic;
    vlTOPp->v__DOT__DataMem_Controller__DOT__LLSC_Address 
	= __Vdly__v__DOT__DataMem_Controller__DOT__LLSC_Address;
    vlTOPp->v__DOT__WB_RtRd = __Vdly__v__DOT__WB_RtRd;
    vlTOPp->v__DOT__WB_RegWrite = __Vdly__v__DOT__WB_RegWrite;
    vlTOPp->v__DOT__WB_ReadData = __Vdly__v__DOT__WB_ReadData;
    vlTOPp->v__DOT__WB_MemtoReg = __Vdly__v__DOT__WB_MemtoReg;
    vlTOPp->v__DOT__WB_ALUResult = __Vdly__v__DOT__WB_ALUResult;
    vlTOPp->v__DOT__M_IsBDS = __Vdly__v__DOT__M_IsBDS;
    vlTOPp->v__DOT__M_RestartPC = __Vdly__v__DOT__M_RestartPC;
    vlTOPp->v__DOT__M_MemSignExtend = __Vdly__v__DOT__M_MemSignExtend;
    vlTOPp->v__DOT__ALU__DOT__HILO = __Vdly__v__DOT__ALU__DOT__HILO;
    vlTOPp->v__DOT__M_MemtoReg = __Vdly__v__DOT__M_MemtoReg;
    vlTOPp->v__DOT__M_ReadData2_PR = __Vdly__v__DOT__M_ReadData2_PR;
    vlTOPp->v__DOT__M_LLSC = __Vdly__v__DOT__M_LLSC;
    vlTOPp->v__DOT__M_M_CanErr = __Vdly__v__DOT__M_M_CanErr;
    vlTOPp->v__DOT__M_Trap = __Vdly__v__DOT__M_Trap;
    vlTOPp->v__DOT__M_TrapCond = __Vdly__v__DOT__M_TrapCond;
    vlTOPp->v__DOT__M_ReverseEndian = __Vdly__v__DOT__M_ReverseEndian;
    vlTOPp->v__DOT__M_Left = __Vdly__v__DOT__M_Left;
    vlTOPp->v__DOT__M_Right = __Vdly__v__DOT__M_Right;
    vlTOPp->v__DOT__M_MemByte = __Vdly__v__DOT__M_MemByte;
    vlTOPp->v__DOT__M_KernelMode = __Vdly__v__DOT__M_KernelMode;
    vlTOPp->v__DOT__M_MemHalf = __Vdly__v__DOT__M_MemHalf;
    vlTOPp->v__DOT__M_MemRead = __Vdly__v__DOT__M_MemRead;
    vlTOPp->v__DOT__M_MemWrite = __Vdly__v__DOT__M_MemWrite;
    vlTOPp->v__DOT__M_ALUResult = __Vdly__v__DOT__M_ALUResult;
    vlTOPp->v__DOT__M_RegWrite = __Vdly__v__DOT__M_RegWrite;
    vlTOPp->v__DOT__M_RtRd = __Vdly__v__DOT__M_RtRd;
    vlTOPp->v__DOT__ID_PCAdd4 = __Vdly__v__DOT__ID_PCAdd4;
    vlTOPp->v__DOT__IF_PCOut = __Vdly__v__DOT__IF_PCOut;
    vlTOPp->v__DOT__ID_IsFlushed = __Vdly__v__DOT__ID_IsFlushed;
    vlTOPp->v__DOT__EX_Trap = __Vdly__v__DOT__EX_Trap;
    vlTOPp->v__DOT__EX_MemtoReg = __Vdly__v__DOT__EX_MemtoReg;
    vlTOPp->v__DOT__EX_ReverseEndian = __Vdly__v__DOT__EX_ReverseEndian;
    vlTOPp->v__DOT__EX_IsBDS = __Vdly__v__DOT__EX_IsBDS;
    vlTOPp->v__DOT__EX_MemHalf = __Vdly__v__DOT__EX_MemHalf;
    vlTOPp->v__DOT__EX_TrapCond = __Vdly__v__DOT__EX_TrapCond;
    vlTOPp->v__DOT__EX_MemRead = __Vdly__v__DOT__EX_MemRead;
    vlTOPp->v__DOT__EX_KernelMode = __Vdly__v__DOT__EX_KernelMode;
    vlTOPp->v__DOT__EX_Movn = __Vdly__v__DOT__EX_Movn;
    vlTOPp->v__DOT__EX_Movz = __Vdly__v__DOT__EX_Movz;
    vlTOPp->v__DOT__EX_LLSC = __Vdly__v__DOT__EX_LLSC;
    vlTOPp->v__DOT__EX_Right = __Vdly__v__DOT__EX_Right;
    vlTOPp->v__DOT__EX_MemWrite = __Vdly__v__DOT__EX_MemWrite;
    vlTOPp->v__DOT__EX_MemSignExtend = __Vdly__v__DOT__EX_MemSignExtend;
    vlTOPp->v__DOT__EX_Left = __Vdly__v__DOT__EX_Left;
    vlTOPp->v__DOT__EX_MemByte = __Vdly__v__DOT__EX_MemByte;
    vlTOPp->v__DOT__ID_IsBDS = __Vdly__v__DOT__ID_IsBDS;
    vlTOPp->v__DOT__ID_RestartPC = __Vdly__v__DOT__ID_RestartPC;
    vlTOPp->v__DOT__CP0__DOT__Status_RE = __Vdly__v__DOT__CP0__DOT__Status_RE;
    vlTOPp->v__DOT__EX_RegWrite = __Vdly__v__DOT__EX_RegWrite;
    vlTOPp->v__DOT__IDEX__DOT__EX_RegDst = __Vdly__v__DOT__IDEX__DOT__EX_RegDst;
    vlTOPp->v__DOT__EX_EX_CanErr = __Vdly__v__DOT__EX_EX_CanErr;
    vlTOPp->v__DOT__EX_M_CanErr = __Vdly__v__DOT__EX_M_CanErr;
    vlTOPp->v__DOT__EX_ALUSrcImm = __Vdly__v__DOT__EX_ALUSrcImm;
    vlTOPp->v__DOT__EX_ReadData1_PR = __Vdly__v__DOT__EX_ReadData1_PR;
    vlTOPp->v__DOT__EX_RestartPC = __Vdly__v__DOT__EX_RestartPC;
    vlTOPp->v__DOT__EX_ReadData2_PR = __Vdly__v__DOT__EX_ReadData2_PR;
    vlTOPp->v__DOT__IDEX__DOT__EX_SignExtImm_pre = __Vdly__v__DOT__IDEX__DOT__EX_SignExtImm_pre;
    vlTOPp->v__DOT__EX_Link = __Vdly__v__DOT__EX_Link;
    vlTOPp->v__DOT__EX_Rs = __Vdly__v__DOT__EX_Rs;
    vlTOPp->v__DOT__EX_Rt = __Vdly__v__DOT__EX_Rt;
    vlTOPp->v__DOT__EX_WantRsByEX = __Vdly__v__DOT__EX_WantRsByEX;
    vlTOPp->v__DOT__EX_NeedRsByEX = __Vdly__v__DOT__EX_NeedRsByEX;
    vlTOPp->v__DOT__EX_WantRtByEX = __Vdly__v__DOT__EX_WantRtByEX;
    vlTOPp->v__DOT__EX_NeedRtByEX = __Vdly__v__DOT__EX_NeedRtByEX;
    vlTOPp->v__DOT__Instruction = __Vdly__v__DOT__Instruction;
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Divide.v:51
    if (vlTOPp->reset) {
	__Vdly__v__DOT__ALU__DOT__Divider__DOT__active = 0;
	vlTOPp->v__DOT__ALU__DOT__Divider__DOT__neg = 0;
	__Vdly__v__DOT__ALU__DOT__Divider__DOT__cycle = 0;
	__Vdly__v__DOT__ALU__DOT__Divider__DOT__result = 0;
	vlTOPp->v__DOT__ALU__DOT__Divider__DOT__denom = 0;
	__Vdly__v__DOT__ALU__DOT__Divider__DOT__work = 0;
    } else {
	if ((((~ (IData)(vlTOPp->v__DOT__ALU__DOT__div_fsm)) 
	      & (5 == (IData)(vlTOPp->v__DOT__EX_ALUOp))) 
	     & (IData)(vlTOPp->v__DOT__ALU__DOT__HILO_Commit))) {
	    __Vdly__v__DOT__ALU__DOT__Divider__DOT__cycle = 0x1f;
	    __Vdly__v__DOT__ALU__DOT__Divider__DOT__result 
		= ((0 == (1 & (vlTOPp->v__DOT__EX_ReadData1_Fwd 
			       >> 0x1f))) ? vlTOPp->v__DOT__EX_ReadData1_Fwd
		    : VL_NEGATE_I(vlTOPp->v__DOT__EX_ReadData1_Fwd));
	    vlTOPp->v__DOT__ALU__DOT__Divider__DOT__denom 
		= ((0 == (1 & (vlTOPp->v__DOT__EX_ReadData2_Imm 
			       >> 0x1f))) ? vlTOPp->v__DOT__EX_ReadData2_Imm
		    : VL_NEGATE_I(vlTOPp->v__DOT__EX_ReadData2_Imm));
	    __Vdly__v__DOT__ALU__DOT__Divider__DOT__work = 0;
	    vlTOPp->v__DOT__ALU__DOT__Divider__DOT__neg 
		= (1 & ((vlTOPp->v__DOT__EX_ReadData1_Fwd 
			 ^ vlTOPp->v__DOT__EX_ReadData2_Imm) 
			>> 0x1f));
	    __Vdly__v__DOT__ALU__DOT__Divider__DOT__active = 1;
	} else {
	    if ((((~ (IData)(vlTOPp->v__DOT__ALU__DOT__div_fsm)) 
		  & (6 == (IData)(vlTOPp->v__DOT__EX_ALUOp))) 
		 & (IData)(vlTOPp->v__DOT__ALU__DOT__HILO_Commit))) {
		__Vdly__v__DOT__ALU__DOT__Divider__DOT__cycle = 0x1f;
		__Vdly__v__DOT__ALU__DOT__Divider__DOT__result 
		    = vlTOPp->v__DOT__EX_ReadData1_Fwd;
		vlTOPp->v__DOT__ALU__DOT__Divider__DOT__denom 
		    = vlTOPp->v__DOT__EX_ReadData2_Imm;
		__Vdly__v__DOT__ALU__DOT__Divider__DOT__work = 0;
		vlTOPp->v__DOT__ALU__DOT__Divider__DOT__neg = 0;
		__Vdly__v__DOT__ALU__DOT__Divider__DOT__active = 1;
	    } else {
		if (vlTOPp->v__DOT__ALU__DOT__Divider__DOT__active) {
		    if ((0 == (1 & (IData)((vlTOPp->v__DOT__ALU__DOT__Divider__DOT__sub 
					    >> 0x20))))) {
			__Vdly__v__DOT__ALU__DOT__Divider__DOT__result 
			    = (1 | (0xfffffffe & (vlTOPp->v__DOT__ALU__DOT__Divider__DOT__result 
						  << 1)));
			__Vdly__v__DOT__ALU__DOT__Divider__DOT__work 
			    = (IData)(vlTOPp->v__DOT__ALU__DOT__Divider__DOT__sub);
		    } else {
			__Vdly__v__DOT__ALU__DOT__Divider__DOT__work 
			    = ((0xfffffffe & (vlTOPp->v__DOT__ALU__DOT__Divider__DOT__work 
					      << 1)) 
			       | (1 & (vlTOPp->v__DOT__ALU__DOT__Divider__DOT__result 
				       >> 0x1f)));
			__Vdly__v__DOT__ALU__DOT__Divider__DOT__result 
			    = (0xfffffffe & (vlTOPp->v__DOT__ALU__DOT__Divider__DOT__result 
					     << 1));
		    }
		    if ((0 == (IData)(vlTOPp->v__DOT__ALU__DOT__Divider__DOT__cycle))) {
			__Vdly__v__DOT__ALU__DOT__Divider__DOT__active = 0;
		    }
		    __Vdly__v__DOT__ALU__DOT__Divider__DOT__cycle 
			= (0x1f & ((IData)(vlTOPp->v__DOT__ALU__DOT__Divider__DOT__cycle) 
				   - (IData)(1)));
		}
	    }
	}
    }
    vlTOPp->IP = vlTOPp->v__DOT__CP0__DOT__Cause_IP;
    vlTOPp->v__DOT__ID_KernelMode = (1 & (((~ (IData)(vlTOPp->v__DOT__CP0__DOT__Status_UM)) 
					   | (IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL)) 
					  | (IData)(vlTOPp->v__DOT__CP0__DOT__Status_ERL)));
    vlTOPp->v__DOT__WB_WriteData = ((IData)(vlTOPp->v__DOT__WB_MemtoReg)
				     ? vlTOPp->v__DOT__WB_ReadData
				     : vlTOPp->v__DOT__WB_ALUResult);
    vlTOPp->v__DOT__DataMem_Controller__DOT__BE = (1 
						   & ((IData)(vlTOPp->v__DOT__M_KernelMode) 
						      | (~ (IData)(vlTOPp->v__DOT__M_ReverseEndian))));
    vlTOPp->DataMem_Address = (0x3fffffff & (vlTOPp->v__DOT__M_ALUResult 
					     >> 2));
    vlTOPp->v__DOT__M_EXC_Tr = ((IData)(vlTOPp->v__DOT__M_Trap) 
				& ((IData)(vlTOPp->v__DOT__M_TrapCond) 
				   ^ (0 == vlTOPp->v__DOT__M_ALUResult)));
    vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_KernelMem 
	= ((~ (IData)(vlTOPp->v__DOT__M_KernelMode)) 
	   & (vlTOPp->v__DOT__M_ALUResult < 0x8000000));
    vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Half 
	= ((IData)(vlTOPp->v__DOT__M_MemHalf) & vlTOPp->v__DOT__M_ALUResult);
    vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Word 
	= (1 & ((~ ((((IData)(vlTOPp->v__DOT__M_MemHalf) 
		      | (IData)(vlTOPp->v__DOT__M_MemByte)) 
		     | (IData)(vlTOPp->v__DOT__M_Left)) 
		    | (IData)(vlTOPp->v__DOT__M_Right))) 
		& ((vlTOPp->v__DOT__M_ALUResult >> 1) 
		   | vlTOPp->v__DOT__M_ALUResult)));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/CPZero.v:647
    vlTOPp->v__DOT__CP0__DOT__reset_r = vlTOPp->reset;
    vlTOPp->InstMem_Address = (0x3fffffff & (vlTOPp->v__DOT__IF_PCOut 
					     >> 2));
    vlTOPp->v__DOT__IF_EXC_AdIF = (1 & ((vlTOPp->v__DOT__IF_PCOut 
					 >> 1) | vlTOPp->v__DOT__IF_PCOut));
    vlTOPp->v__DOT__EX_CanErr = ((IData)(vlTOPp->v__DOT__EX_EX_CanErr) 
				 | (IData)(vlTOPp->v__DOT__EX_M_CanErr));
    vlTOPp->v__DOT__EX_SignExtImm = ((0x10000 & vlTOPp->v__DOT__IDEX__DOT__EX_SignExtImm_pre)
				      ? (0xfffe0000 
					 | vlTOPp->v__DOT__IDEX__DOT__EX_SignExtImm_pre)
				      : vlTOPp->v__DOT__IDEX__DOT__EX_SignExtImm_pre);
    vlTOPp->v__DOT__EX_LinkRegDst = ((IData)(vlTOPp->v__DOT__EX_Link)
				      ? 2 : ((IData)(vlTOPp->v__DOT__IDEX__DOT__EX_RegDst)
					      ? 1 : 0));
    vlTOPp->v__DOT__ID_SignExtImm = (((3 != (0xf & 
					     (vlTOPp->v__DOT__Instruction 
					      >> 0x1c))) 
				      & (vlTOPp->v__DOT__Instruction 
					 >> 0xf)) ? 
				     (0x3fff0000 | 
				      (0xffff & vlTOPp->v__DOT__Instruction))
				      : (0xffff & vlTOPp->v__DOT__Instruction));
    vlTOPp->v__DOT__Controller__DOT__Unaligned_Mem 
	= (1 & ((((vlTOPp->v__DOT__Instruction >> 0x1f) 
		  & (~ (vlTOPp->v__DOT__Instruction 
			>> 0x1e))) & (vlTOPp->v__DOT__Instruction 
				      >> 0x1b)) & (~ 
						   (vlTOPp->v__DOT__Instruction 
						    >> 0x1a))));
    vlTOPp->v__DOT__ID_EXC_Sys = ((0 == (0x3f & (vlTOPp->v__DOT__Instruction 
						 >> 0x1a))) 
				  & (0xc == (0x3f & vlTOPp->v__DOT__Instruction)));
    vlTOPp->v__DOT__ID_EXC_Bp = ((0 == (0x3f & (vlTOPp->v__DOT__Instruction 
						>> 0x1a))) 
				 & (0xd == (0x3f & vlTOPp->v__DOT__Instruction)));
    vlTOPp->v__DOT__ID_Eret = (((0x10 == (0x3f & (vlTOPp->v__DOT__Instruction 
						  >> 0x1a))) 
				& (0x10 == (0x1f & 
					    (vlTOPp->v__DOT__Instruction 
					     >> 0x15)))) 
			       & (0x18 == (0x3f & vlTOPp->v__DOT__Instruction)));
    vlTOPp->v__DOT__ID_Mtc0 = ((0x10 == (0x3f & (vlTOPp->v__DOT__Instruction 
						 >> 0x1a))) 
			       & (4 == (0x1f & (vlTOPp->v__DOT__Instruction 
						>> 0x15))));
    vlTOPp->v__DOT__ID_Mfc0 = ((0x10 == (0x3f & (vlTOPp->v__DOT__Instruction 
						 >> 0x1a))) 
			       & (0 == (0x1f & (vlTOPp->v__DOT__Instruction 
						>> 0x15))));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Control.v:724
    if ((0x80000000 & vlTOPp->v__DOT__Instruction)) {
	if ((0x40000000 & vlTOPp->v__DOT__Instruction)) {
	    if ((0x20000000 & vlTOPp->v__DOT__Instruction)) {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    vlTOPp->v__DOT__ID_DP_Hazards = 0;
		    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		} else {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			vlTOPp->v__DOT__ID_DP_Hazards = 0;
			vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xae;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    }
		}
	    } else {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    vlTOPp->v__DOT__ID_DP_Hazards = 0;
		    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		} else {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    }
		}
	    }
	} else {
	    if ((0x20000000 & vlTOPp->v__DOT__Instruction)) {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xae;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    } else {
			vlTOPp->v__DOT__ID_DP_Hazards = 0;
			vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		    }
		} else {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xae;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xae;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xae;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xae;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    }
		}
	    } else {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    }
		} else {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    }
		}
	    }
	}
    } else {
	if ((0x40000000 & vlTOPp->v__DOT__Instruction)) {
	    if ((0x20000000 & vlTOPp->v__DOT__Instruction)) {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			vlTOPp->v__DOT__ID_DP_Hazards = 0;
			vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    if ((0x20 & vlTOPp->v__DOT__Instruction)) {
				if ((0x10 & vlTOPp->v__DOT__Instruction)) {
				    vlTOPp->v__DOT__ID_DP_Hazards = 0;
				    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				} else {
				    if ((8 & vlTOPp->v__DOT__Instruction)) {
					vlTOPp->v__DOT__ID_DP_Hazards = 0;
					vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				    } else {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    vlTOPp->v__DOT__ID_DP_Hazards = 0;
					    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					}
				    }
				}
			    } else {
				if ((0x10 & vlTOPp->v__DOT__Instruction)) {
				    vlTOPp->v__DOT__ID_DP_Hazards = 0;
				    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				} else {
				    if ((8 & vlTOPp->v__DOT__Instruction)) {
					vlTOPp->v__DOT__ID_DP_Hazards = 0;
					vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				    } else {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					}
				    }
				}
			    }
			}
		    }
		} else {
		    vlTOPp->v__DOT__ID_DP_Hazards = 0;
		    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		}
	    } else {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    vlTOPp->v__DOT__ID_DP_Hazards = 0;
		    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		} else {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			vlTOPp->v__DOT__ID_DP_Hazards = 0;
			vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    if ((0 == (0x1f & (vlTOPp->v__DOT__Instruction 
					       >> 0x15)))) {
				vlTOPp->v__DOT__ID_DP_Hazards = 0;
				vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 4;
			    } else {
				if ((4 == (0x1f & (vlTOPp->v__DOT__Instruction 
						   >> 0x15)))) {
				    vlTOPp->v__DOT__ID_DP_Hazards = 0x30;
				    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 4;
				} else {
				    if ((0x10 == (0x1f 
						  & (vlTOPp->v__DOT__Instruction 
						     >> 0x15)))) {
					vlTOPp->v__DOT__ID_DP_Hazards = 0;
					vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 4;
				    } else {
					vlTOPp->v__DOT__ID_DP_Hazards = 0;
					vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				    }
				}
			    }
			}
		    }
		}
	    }
	} else {
	    if ((0x20000000 & vlTOPp->v__DOT__Instruction)) {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			}
		    }
		} else {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 2;
			}
		    }
		}
	    } else {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xf0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xf0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			}
		    }
		} else {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    if ((0x100000 & vlTOPp->v__DOT__Instruction)) {
				if ((0x80000 & vlTOPp->v__DOT__Instruction)) {
				    vlTOPp->v__DOT__ID_DP_Hazards = 0;
				    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				} else {
				    if ((0x40000 & vlTOPp->v__DOT__Instruction)) {
					vlTOPp->v__DOT__ID_DP_Hazards = 0;
					vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				    } else {
					if ((0x20000 
					     & vlTOPp->v__DOT__Instruction)) {
					    vlTOPp->v__DOT__ID_DP_Hazards = 0;
					    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					} else {
					    if ((0x10000 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    } else {
						vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    }
					}
				    }
				}
			    } else {
				if ((0x80000 & vlTOPp->v__DOT__Instruction)) {
				    if ((0x40000 & vlTOPp->v__DOT__Instruction)) {
					if ((0x20000 
					     & vlTOPp->v__DOT__Instruction)) {
					    if ((0x10000 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    } else {
						vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
					    }
					} else {
					    if ((0x10000 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    } else {
						vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
					    }
					}
				    } else {
					if ((0x20000 
					     & vlTOPp->v__DOT__Instruction)) {
					    if ((0x10000 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
					    } else {
						vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
					    }
					} else {
					    if ((0x10000 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
					    } else {
						vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
					    }
					}
				    }
				} else {
				    if ((0x40000 & vlTOPp->v__DOT__Instruction)) {
					vlTOPp->v__DOT__ID_DP_Hazards = 0;
					vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				    } else {
					if ((0x20000 
					     & vlTOPp->v__DOT__Instruction)) {
					    vlTOPp->v__DOT__ID_DP_Hazards = 0;
					    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					} else {
					    if ((0x10000 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    } else {
						vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    }
					}
				    }
				}
			    }
			} else {
			    if ((0x20 & vlTOPp->v__DOT__Instruction)) {
				if ((0x10 & vlTOPp->v__DOT__Instruction)) {
				    if ((8 & vlTOPp->v__DOT__Instruction)) {
					vlTOPp->v__DOT__ID_DP_Hazards = 0;
					vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				    } else {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
						}
					    }
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
						}
					    }
					}
				    }
				} else {
				    if ((8 & vlTOPp->v__DOT__Instruction)) {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    vlTOPp->v__DOT__ID_DP_Hazards = 0;
					    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						vlTOPp->v__DOT__ID_DP_Hazards = 0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    }
					}
				    } else {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 2;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 2;
						}
					    }
					}
				    }
				}
			    } else {
				if ((0x10 & vlTOPp->v__DOT__Instruction)) {
				    if ((8 & vlTOPp->v__DOT__Instruction)) {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    vlTOPp->v__DOT__ID_DP_Hazards = 0;
					    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					}
				    } else {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    vlTOPp->v__DOT__ID_DP_Hazards = 0;
					    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					}
				    }
				} else {
				    if ((8 & vlTOPp->v__DOT__Instruction)) {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 4;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 4;
						}
					    }
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xbc;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xbc;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					}
				    } else {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0x23;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0x23;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0x23;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}
    }
    vlTOPp->v__DOT__ALU__DOT__Divider__DOT__cycle = __Vdly__v__DOT__ALU__DOT__Divider__DOT__cycle;
    vlTOPp->v__DOT__ALU__DOT__Divider__DOT__active 
	= __Vdly__v__DOT__ALU__DOT__Divider__DOT__active;
    vlTOPp->v__DOT__ALU__DOT__Divider__DOT__result 
	= __Vdly__v__DOT__ALU__DOT__Divider__DOT__result;
    vlTOPp->v__DOT__ALU__DOT__Divider__DOT__work = __Vdly__v__DOT__ALU__DOT__Divider__DOT__work;
    vlTOPp->v__DOT__ALU__DOT__div_fsm = __Vdly__v__DOT__ALU__DOT__div_fsm;
    vlTOPp->v__DOT__EX_ALUOp = __Vdly__v__DOT__EX_ALUOp;
    vlTOPp->v__DOT__ALU__DOT__Divider__DOT__sub = (VL_ULL(0x1ffffffff) 
						   & ((QData)((IData)(
								      ((0xfffffffe 
									& (vlTOPp->v__DOT__ALU__DOT__Divider__DOT__work 
									   << 1)) 
								       | (1 
									  & (vlTOPp->v__DOT__ALU__DOT__Divider__DOT__result 
									     >> 0x1f))))) 
						      - (QData)((IData)(vlTOPp->v__DOT__ALU__DOT__Divider__DOT__denom))));
}

void VAdd::_settle__TOP__2(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_settle__TOP__2\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->IP = vlTOPp->v__DOT__CP0__DOT__Cause_IP;
    vlTOPp->v__DOT__ID_KernelMode = (1 & (((~ (IData)(vlTOPp->v__DOT__CP0__DOT__Status_UM)) 
					   | (IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL)) 
					  | (IData)(vlTOPp->v__DOT__CP0__DOT__Status_ERL)));
    vlTOPp->v__DOT__WB_WriteData = ((IData)(vlTOPp->v__DOT__WB_MemtoReg)
				     ? vlTOPp->v__DOT__WB_ReadData
				     : vlTOPp->v__DOT__WB_ALUResult);
    vlTOPp->v__DOT__DataMem_Controller__DOT__BE = (1 
						   & ((IData)(vlTOPp->v__DOT__M_KernelMode) 
						      | (~ (IData)(vlTOPp->v__DOT__M_ReverseEndian))));
    vlTOPp->DataMem_Address = (0x3fffffff & (vlTOPp->v__DOT__M_ALUResult 
					     >> 2));
    vlTOPp->v__DOT__M_EXC_Tr = ((IData)(vlTOPp->v__DOT__M_Trap) 
				& ((IData)(vlTOPp->v__DOT__M_TrapCond) 
				   ^ (0 == vlTOPp->v__DOT__M_ALUResult)));
    vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_KernelMem 
	= ((~ (IData)(vlTOPp->v__DOT__M_KernelMode)) 
	   & (vlTOPp->v__DOT__M_ALUResult < 0x8000000));
    vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Half 
	= ((IData)(vlTOPp->v__DOT__M_MemHalf) & vlTOPp->v__DOT__M_ALUResult);
    vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Word 
	= (1 & ((~ ((((IData)(vlTOPp->v__DOT__M_MemHalf) 
		      | (IData)(vlTOPp->v__DOT__M_MemByte)) 
		     | (IData)(vlTOPp->v__DOT__M_Left)) 
		    | (IData)(vlTOPp->v__DOT__M_Right))) 
		& ((vlTOPp->v__DOT__M_ALUResult >> 1) 
		   | vlTOPp->v__DOT__M_ALUResult)));
    vlTOPp->InstMem_Address = (0x3fffffff & (vlTOPp->v__DOT__IF_PCOut 
					     >> 2));
    vlTOPp->v__DOT__IF_EXC_AdIF = (1 & ((vlTOPp->v__DOT__IF_PCOut 
					 >> 1) | vlTOPp->v__DOT__IF_PCOut));
    vlTOPp->v__DOT__CP0__DOT__EXC_Int = (((((IData)(vlTOPp->NMI) 
					    | ((IData)(vlTOPp->v__DOT__CP0__DOT__Status_IE) 
					       & (0 
						  != 
						  ((IData)(vlTOPp->v__DOT__CP0__DOT__Cause_IP) 
						   & (IData)(vlTOPp->v__DOT__CP0__DOT__Status_IM))))) 
					   & (~ (IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL))) 
					  & (~ (IData)(vlTOPp->v__DOT__CP0__DOT__Status_ERL))) 
					 & (~ (IData)(vlTOPp->v__DOT__ID_IsFlushed)));
    vlTOPp->v__DOT__EX_CanErr = ((IData)(vlTOPp->v__DOT__EX_EX_CanErr) 
				 | (IData)(vlTOPp->v__DOT__EX_M_CanErr));
    vlTOPp->v__DOT__EX_SignExtImm = ((0x10000 & vlTOPp->v__DOT__IDEX__DOT__EX_SignExtImm_pre)
				      ? (0xfffe0000 
					 | vlTOPp->v__DOT__IDEX__DOT__EX_SignExtImm_pre)
				      : vlTOPp->v__DOT__IDEX__DOT__EX_SignExtImm_pre);
    vlTOPp->v__DOT__EX_LinkRegDst = ((IData)(vlTOPp->v__DOT__EX_Link)
				      ? 2 : ((IData)(vlTOPp->v__DOT__IDEX__DOT__EX_RegDst)
					      ? 1 : 0));
    vlTOPp->v__DOT__Controller__DOT__Unaligned_Mem 
	= (1 & ((((vlTOPp->v__DOT__Instruction >> 0x1f) 
		  & (~ (vlTOPp->v__DOT__Instruction 
			>> 0x1e))) & (vlTOPp->v__DOT__Instruction 
				      >> 0x1b)) & (~ 
						   (vlTOPp->v__DOT__Instruction 
						    >> 0x1a))));
    vlTOPp->v__DOT__ID_SignExtImm = (((3 != (0xf & 
					     (vlTOPp->v__DOT__Instruction 
					      >> 0x1c))) 
				      & (vlTOPp->v__DOT__Instruction 
					 >> 0xf)) ? 
				     (0x3fff0000 | 
				      (0xffff & vlTOPp->v__DOT__Instruction))
				      : (0xffff & vlTOPp->v__DOT__Instruction));
    vlTOPp->v__DOT__ID_EXC_Bp = ((0 == (0x3f & (vlTOPp->v__DOT__Instruction 
						>> 0x1a))) 
				 & (0xd == (0x3f & vlTOPp->v__DOT__Instruction)));
    vlTOPp->v__DOT__ID_EXC_Sys = ((0 == (0x3f & (vlTOPp->v__DOT__Instruction 
						 >> 0x1a))) 
				  & (0xc == (0x3f & vlTOPp->v__DOT__Instruction)));
    vlTOPp->v__DOT__ID_Eret = (((0x10 == (0x3f & (vlTOPp->v__DOT__Instruction 
						  >> 0x1a))) 
				& (0x10 == (0x1f & 
					    (vlTOPp->v__DOT__Instruction 
					     >> 0x15)))) 
			       & (0x18 == (0x3f & vlTOPp->v__DOT__Instruction)));
    vlTOPp->v__DOT__ID_Mtc0 = ((0x10 == (0x3f & (vlTOPp->v__DOT__Instruction 
						 >> 0x1a))) 
			       & (4 == (0x1f & (vlTOPp->v__DOT__Instruction 
						>> 0x15))));
    vlTOPp->v__DOT__ID_Mfc0 = ((0x10 == (0x3f & (vlTOPp->v__DOT__Instruction 
						 >> 0x1a))) 
			       & (0 == (0x1f & (vlTOPp->v__DOT__Instruction 
						>> 0x15))));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Control.v:724
    if ((0x80000000 & vlTOPp->v__DOT__Instruction)) {
	if ((0x40000000 & vlTOPp->v__DOT__Instruction)) {
	    if ((0x20000000 & vlTOPp->v__DOT__Instruction)) {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    vlTOPp->v__DOT__ID_DP_Hazards = 0;
		    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		} else {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			vlTOPp->v__DOT__ID_DP_Hazards = 0;
			vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xae;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    }
		}
	    } else {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    vlTOPp->v__DOT__ID_DP_Hazards = 0;
		    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		} else {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    }
		}
	    }
	} else {
	    if ((0x20000000 & vlTOPp->v__DOT__Instruction)) {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xae;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    } else {
			vlTOPp->v__DOT__ID_DP_Hazards = 0;
			vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		    }
		} else {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xae;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xae;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xae;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xae;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    }
		}
	    } else {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    }
		} else {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
			}
		    }
		}
	    }
	}
    } else {
	if ((0x40000000 & vlTOPp->v__DOT__Instruction)) {
	    if ((0x20000000 & vlTOPp->v__DOT__Instruction)) {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			vlTOPp->v__DOT__ID_DP_Hazards = 0;
			vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    if ((0x20 & vlTOPp->v__DOT__Instruction)) {
				if ((0x10 & vlTOPp->v__DOT__Instruction)) {
				    vlTOPp->v__DOT__ID_DP_Hazards = 0;
				    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				} else {
				    if ((8 & vlTOPp->v__DOT__Instruction)) {
					vlTOPp->v__DOT__ID_DP_Hazards = 0;
					vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				    } else {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    vlTOPp->v__DOT__ID_DP_Hazards = 0;
					    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					}
				    }
				}
			    } else {
				if ((0x10 & vlTOPp->v__DOT__Instruction)) {
				    vlTOPp->v__DOT__ID_DP_Hazards = 0;
				    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				} else {
				    if ((8 & vlTOPp->v__DOT__Instruction)) {
					vlTOPp->v__DOT__ID_DP_Hazards = 0;
					vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				    } else {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					}
				    }
				}
			    }
			}
		    }
		} else {
		    vlTOPp->v__DOT__ID_DP_Hazards = 0;
		    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		}
	    } else {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    vlTOPp->v__DOT__ID_DP_Hazards = 0;
		    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		} else {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			vlTOPp->v__DOT__ID_DP_Hazards = 0;
			vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    if ((0 == (0x1f & (vlTOPp->v__DOT__Instruction 
					       >> 0x15)))) {
				vlTOPp->v__DOT__ID_DP_Hazards = 0;
				vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 4;
			    } else {
				if ((4 == (0x1f & (vlTOPp->v__DOT__Instruction 
						   >> 0x15)))) {
				    vlTOPp->v__DOT__ID_DP_Hazards = 0x30;
				    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 4;
				} else {
				    if ((0x10 == (0x1f 
						  & (vlTOPp->v__DOT__Instruction 
						     >> 0x15)))) {
					vlTOPp->v__DOT__ID_DP_Hazards = 0;
					vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 4;
				    } else {
					vlTOPp->v__DOT__ID_DP_Hazards = 0;
					vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				    }
				}
			    }
			}
		    }
		}
	    }
	} else {
	    if ((0x20000000 & vlTOPp->v__DOT__Instruction)) {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			}
		    }
		} else {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 2;
			}
		    }
		}
	    } else {
		if ((0x10000000 & vlTOPp->v__DOT__Instruction)) {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xf0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0xf0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			}
		    }
		} else {
		    if ((0x8000000 & vlTOPp->v__DOT__Instruction)) {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			} else {
			    vlTOPp->v__DOT__ID_DP_Hazards = 0;
			    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
			}
		    } else {
			if ((0x4000000 & vlTOPp->v__DOT__Instruction)) {
			    if ((0x100000 & vlTOPp->v__DOT__Instruction)) {
				if ((0x80000 & vlTOPp->v__DOT__Instruction)) {
				    vlTOPp->v__DOT__ID_DP_Hazards = 0;
				    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				} else {
				    if ((0x40000 & vlTOPp->v__DOT__Instruction)) {
					vlTOPp->v__DOT__ID_DP_Hazards = 0;
					vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				    } else {
					if ((0x20000 
					     & vlTOPp->v__DOT__Instruction)) {
					    vlTOPp->v__DOT__ID_DP_Hazards = 0;
					    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					} else {
					    if ((0x10000 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    } else {
						vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    }
					}
				    }
				}
			    } else {
				if ((0x80000 & vlTOPp->v__DOT__Instruction)) {
				    if ((0x40000 & vlTOPp->v__DOT__Instruction)) {
					if ((0x20000 
					     & vlTOPp->v__DOT__Instruction)) {
					    if ((0x10000 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    } else {
						vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
					    }
					} else {
					    if ((0x10000 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    } else {
						vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
					    }
					}
				    } else {
					if ((0x20000 
					     & vlTOPp->v__DOT__Instruction)) {
					    if ((0x10000 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
					    } else {
						vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
					    }
					} else {
					    if ((0x10000 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
					    } else {
						vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
					    }
					}
				    }
				} else {
				    if ((0x40000 & vlTOPp->v__DOT__Instruction)) {
					vlTOPp->v__DOT__ID_DP_Hazards = 0;
					vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				    } else {
					if ((0x20000 
					     & vlTOPp->v__DOT__Instruction)) {
					    vlTOPp->v__DOT__ID_DP_Hazards = 0;
					    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					} else {
					    if ((0x10000 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    } else {
						vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    }
					}
				    }
				}
			    }
			} else {
			    if ((0x20 & vlTOPp->v__DOT__Instruction)) {
				if ((0x10 & vlTOPp->v__DOT__Instruction)) {
				    if ((8 & vlTOPp->v__DOT__Instruction)) {
					vlTOPp->v__DOT__ID_DP_Hazards = 0;
					vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
				    } else {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
						}
					    }
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 1;
						}
					    }
					}
				    }
				} else {
				    if ((8 & vlTOPp->v__DOT__Instruction)) {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    vlTOPp->v__DOT__ID_DP_Hazards = 0;
					    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						vlTOPp->v__DOT__ID_DP_Hazards = 0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    }
					}
				    } else {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 2;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 2;
						}
					    }
					}
				    }
				}
			    } else {
				if ((0x10 & vlTOPp->v__DOT__Instruction)) {
				    if ((8 & vlTOPp->v__DOT__Instruction)) {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    vlTOPp->v__DOT__ID_DP_Hazards = 0;
					    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					}
				    } else {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    vlTOPp->v__DOT__ID_DP_Hazards = 0;
					    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0x8c;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					}
				    }
				} else {
				    if ((8 & vlTOPp->v__DOT__Instruction)) {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						vlTOPp->v__DOT__ID_DP_Hazards = 0;
						vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 4;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 4;
						}
					    }
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xbc;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xbc;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xc0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					}
				    } else {
					if ((4 & vlTOPp->v__DOT__Instruction)) {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0xaf;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					} else {
					    if ((2 
						 & vlTOPp->v__DOT__Instruction)) {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0x23;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0x23;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    } else {
						if (
						    (1 
						     & vlTOPp->v__DOT__Instruction)) {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						} else {
						    vlTOPp->v__DOT__ID_DP_Hazards = 0x23;
						    vlTOPp->v__DOT__Controller__DOT__DP_Exceptions = 0;
						}
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}
    }
    vlTOPp->v__DOT__ALU__DOT__Divider__DOT__sub = (VL_ULL(0x1ffffffff) 
						   & ((QData)((IData)(
								      ((0xfffffffe 
									& (vlTOPp->v__DOT__ALU__DOT__Divider__DOT__work 
									   << 1)) 
								       | (1 
									  & (vlTOPp->v__DOT__ALU__DOT__Divider__DOT__result 
									     >> 0x1f))))) 
						      - (QData)((IData)(vlTOPp->v__DOT__ALU__DOT__Divider__DOT__denom))));
    vlTOPp->v__DOT__M_WriteData_Pre = (((((IData)(vlTOPp->v__DOT__M_RtRd) 
					  == (IData)(vlTOPp->v__DOT__WB_RtRd)) 
					 & (0 != (IData)(vlTOPp->v__DOT__WB_RtRd))) 
					& (IData)(vlTOPp->v__DOT__WB_RegWrite))
				        ? vlTOPp->v__DOT__WB_WriteData
				        : vlTOPp->v__DOT__M_ReadData2_PR);
    vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_R 
	= (1 & ((vlTOPp->v__DOT__M_ALUResult >> 1) 
		^ ~ (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)));
    vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_L 
	= (1 & ((vlTOPp->v__DOT__M_ALUResult >> 1) 
		^ (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)));
    vlTOPp->DataMem_Read = (((IData)(vlTOPp->v__DOT__M_MemRead) 
			     & (~ (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_KernelMem) 
				    | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Word)) 
				   | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Half)))) 
			    & (~ (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__RW_Mask)));
    vlTOPp->v__DOT__M_EXC_AdEL = ((IData)(vlTOPp->v__DOT__M_MemRead) 
				  & (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_KernelMem) 
				      | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Word)) 
				     | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Half)));
    vlTOPp->v__DOT__M_EXC_AdES = ((IData)(vlTOPp->v__DOT__M_MemWrite) 
				  & (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_KernelMem) 
				      | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Word)) 
				     | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Half)));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Mux4.v:23
    vlTOPp->v__DOT__EX_RtRd = (0x1f & ((2 & (IData)(vlTOPp->v__DOT__EX_LinkRegDst))
				        ? ((1 & (IData)(vlTOPp->v__DOT__EX_LinkRegDst))
					    ? 0 : 0x1f)
				        : ((1 & (IData)(vlTOPp->v__DOT__EX_LinkRegDst))
					    ? (vlTOPp->v__DOT__EX_SignExtImm 
					       >> 0xb)
					    : (IData)(vlTOPp->v__DOT__EX_Rt))));
    vlTOPp->v__DOT__CP0__DOT__EXC_CpU = ((((0x11 == 
					    (0x3f & 
					     (vlTOPp->v__DOT__Instruction 
					      >> 0x1a))) 
					   | (0x12 
					      == (0x3f 
						  & (vlTOPp->v__DOT__Instruction 
						     >> 0x1a)))) 
					  | (0x13 == 
					     (0x3f 
					      & (vlTOPp->v__DOT__Instruction 
						 >> 0x1a)))) 
					 | ((((IData)(vlTOPp->v__DOT__ID_Mtc0) 
					      | (IData)(vlTOPp->v__DOT__ID_Mfc0)) 
					     | (IData)(vlTOPp->v__DOT__ID_Eret)) 
					    & (~ ((IData)(vlTOPp->v__DOT__CP0__DOT__Status_CU_0) 
						  | (IData)(vlTOPp->v__DOT__ID_KernelMode)))));
    vlTOPp->v__DOT__HAZ_DP_Hazards = ((0xf0 & (IData)(vlTOPp->v__DOT__ID_DP_Hazards)) 
				      | (((IData)(vlTOPp->v__DOT__EX_WantRsByEX) 
					  << 3) | (
						   ((IData)(vlTOPp->v__DOT__EX_NeedRsByEX) 
						    << 2) 
						   | (((IData)(vlTOPp->v__DOT__EX_WantRtByEX) 
						       << 1) 
						      | (IData)(vlTOPp->v__DOT__EX_NeedRtByEX)))));
}

void VAdd::_combo__TOP__3(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_combo__TOP__3\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__CP0__DOT__EXC_Int = (((((IData)(vlTOPp->NMI) 
					    | ((IData)(vlTOPp->v__DOT__CP0__DOT__Status_IE) 
					       & (0 
						  != 
						  ((IData)(vlTOPp->v__DOT__CP0__DOT__Cause_IP) 
						   & (IData)(vlTOPp->v__DOT__CP0__DOT__Status_IM))))) 
					   & (~ (IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL))) 
					  & (~ (IData)(vlTOPp->v__DOT__CP0__DOT__Status_ERL))) 
					 & (~ (IData)(vlTOPp->v__DOT__ID_IsFlushed)));
    vlTOPp->v__DOT__CP0__DOT__IF_Exception_Mask = (1 
						   & ((((IData)(vlTOPp->v__DOT__M_M_CanErr) 
							| (IData)(vlTOPp->v__DOT__EX_CanErr)) 
						       | ((((IData)(vlTOPp->v__DOT__Controller__DOT__DP_Exceptions) 
							    >> 2) 
							   | ((IData)(vlTOPp->v__DOT__Controller__DOT__DP_Exceptions) 
							      >> 1)) 
							  | (IData)(vlTOPp->v__DOT__Controller__DOT__DP_Exceptions))) 
						      | (IData)(vlTOPp->v__DOT__CP0__DOT__EXC_Int)));
}

void VAdd::_sequent__TOP__4(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_sequent__TOP__4\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__M_WriteData_Pre = (((((IData)(vlTOPp->v__DOT__M_RtRd) 
					  == (IData)(vlTOPp->v__DOT__WB_RtRd)) 
					 & (0 != (IData)(vlTOPp->v__DOT__WB_RtRd))) 
					& (IData)(vlTOPp->v__DOT__WB_RegWrite))
				        ? vlTOPp->v__DOT__WB_WriteData
				        : vlTOPp->v__DOT__M_ReadData2_PR);
    vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_R 
	= (1 & ((vlTOPp->v__DOT__M_ALUResult >> 1) 
		^ ~ (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)));
    vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_L 
	= (1 & ((vlTOPp->v__DOT__M_ALUResult >> 1) 
		^ (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)));
    vlTOPp->DataMem_Read = (((IData)(vlTOPp->v__DOT__M_MemRead) 
			     & (~ (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_KernelMem) 
				    | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Word)) 
				   | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Half)))) 
			    & (~ (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__RW_Mask)));
    vlTOPp->v__DOT__M_EXC_AdEL = ((IData)(vlTOPp->v__DOT__M_MemRead) 
				  & (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_KernelMem) 
				      | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Word)) 
				     | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Half)));
    vlTOPp->v__DOT__M_EXC_AdES = ((IData)(vlTOPp->v__DOT__M_MemWrite) 
				  & (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_KernelMem) 
				      | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Word)) 
				     | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Half)));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Mux4.v:23
    vlTOPp->v__DOT__EX_RtRd = (0x1f & ((2 & (IData)(vlTOPp->v__DOT__EX_LinkRegDst))
				        ? ((1 & (IData)(vlTOPp->v__DOT__EX_LinkRegDst))
					    ? 0 : 0x1f)
				        : ((1 & (IData)(vlTOPp->v__DOT__EX_LinkRegDst))
					    ? (vlTOPp->v__DOT__EX_SignExtImm 
					       >> 0xb)
					    : (IData)(vlTOPp->v__DOT__EX_Rt))));
    vlTOPp->v__DOT__CP0__DOT__EXC_CpU = ((((0x11 == 
					    (0x3f & 
					     (vlTOPp->v__DOT__Instruction 
					      >> 0x1a))) 
					   | (0x12 
					      == (0x3f 
						  & (vlTOPp->v__DOT__Instruction 
						     >> 0x1a)))) 
					  | (0x13 == 
					     (0x3f 
					      & (vlTOPp->v__DOT__Instruction 
						 >> 0x1a)))) 
					 | ((((IData)(vlTOPp->v__DOT__ID_Mtc0) 
					      | (IData)(vlTOPp->v__DOT__ID_Mfc0)) 
					     | (IData)(vlTOPp->v__DOT__ID_Eret)) 
					    & (~ ((IData)(vlTOPp->v__DOT__CP0__DOT__Status_CU_0) 
						  | (IData)(vlTOPp->v__DOT__ID_KernelMode)))));
    vlTOPp->v__DOT__HAZ_DP_Hazards = ((0xf0 & (IData)(vlTOPp->v__DOT__ID_DP_Hazards)) 
				      | (((IData)(vlTOPp->v__DOT__EX_WantRsByEX) 
					  << 3) | (
						   ((IData)(vlTOPp->v__DOT__EX_NeedRsByEX) 
						    << 2) 
						   | (((IData)(vlTOPp->v__DOT__EX_WantRtByEX) 
						       << 1) 
						      | (IData)(vlTOPp->v__DOT__EX_NeedRtByEX)))));
    vlTOPp->DataMem_Out = ((0xffffff00 & vlTOPp->DataMem_Out) 
			   | (0xff & vlTOPp->v__DOT__M_WriteData_Pre));
    vlTOPp->DataMem_Out = ((0xffffff & vlTOPp->DataMem_Out) 
			   | (0xff000000 & (((IData)(vlTOPp->v__DOT__M_MemByte)
					      ? vlTOPp->v__DOT__M_WriteData_Pre
					      : ((IData)(vlTOPp->v__DOT__M_MemHalf)
						  ? 
						 (vlTOPp->v__DOT__M_WriteData_Pre 
						  >> 8)
						  : 
						 (vlTOPp->v__DOT__M_WriteData_Pre 
						  >> 0x18))) 
					    << 0x18)));
    vlTOPp->DataMem_Out = ((0xff0000ff & vlTOPp->DataMem_Out) 
			   | ((0xff0000 & ((((IData)(vlTOPp->v__DOT__M_MemByte) 
					     | (IData)(vlTOPp->v__DOT__M_MemHalf))
					     ? vlTOPp->v__DOT__M_WriteData_Pre
					     : (vlTOPp->v__DOT__M_WriteData_Pre 
						>> 0x10)) 
					   << 0x10)) 
			      | (0xff00 & (((IData)(vlTOPp->v__DOT__M_MemByte)
					     ? vlTOPp->v__DOT__M_WriteData_Pre
					     : (vlTOPp->v__DOT__M_WriteData_Pre 
						>> 8)) 
					   << 8))));
    vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_RM 
	= ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_R) 
	   & (vlTOPp->v__DOT__M_ALUResult ^ (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)));
    vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_LL 
	= ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_L) 
	   & ((vlTOPp->v__DOT__M_ALUResult >> 1) ^ ~ vlTOPp->v__DOT__M_ALUResult));
    vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_LM 
	= ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_L) 
	   & (vlTOPp->v__DOT__M_ALUResult ^ ~ (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)));
    vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect = 
	(((IData)(vlTOPp->v__DOT__M_EXC_AdEL) | (IData)(vlTOPp->v__DOT__M_EXC_AdES)) 
	 | (IData)(vlTOPp->v__DOT__M_EXC_Tr));
    vlTOPp->v__DOT__HazardControl__DOT__Rs_IDMEM_Match 
	= (((((0x1f & (vlTOPp->v__DOT__Instruction 
		       >> 0x15)) == (IData)(vlTOPp->v__DOT__M_RtRd)) 
	     & (0 != (IData)(vlTOPp->v__DOT__M_RtRd))) 
	    & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
		>> 7) | ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
			 >> 6))) & (IData)(vlTOPp->v__DOT__M_RegWrite));
    vlTOPp->v__DOT__HazardControl__DOT__Rt_IDMEM_Match 
	= (((((0x1f & (vlTOPp->v__DOT__Instruction 
		       >> 0x10)) == (IData)(vlTOPp->v__DOT__M_RtRd)) 
	     & (0 != (IData)(vlTOPp->v__DOT__M_RtRd))) 
	    & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
		>> 5) | ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
			 >> 4))) & (IData)(vlTOPp->v__DOT__M_RegWrite));
    vlTOPp->v__DOT__HazardControl__DOT__Rs_EXMEM_Match 
	= (((((IData)(vlTOPp->v__DOT__EX_Rs) == (IData)(vlTOPp->v__DOT__M_RtRd)) 
	     & (0 != (IData)(vlTOPp->v__DOT__M_RtRd))) 
	    & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
		>> 3) | ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
			 >> 2))) & (IData)(vlTOPp->v__DOT__M_RegWrite));
    vlTOPp->v__DOT__HazardControl__DOT__Rt_EXMEM_Match 
	= (((((IData)(vlTOPp->v__DOT__EX_Rt) == (IData)(vlTOPp->v__DOT__M_RtRd)) 
	     & (0 != (IData)(vlTOPp->v__DOT__M_RtRd))) 
	    & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
		>> 1) | (IData)(vlTOPp->v__DOT__HAZ_DP_Hazards))) 
	   & (IData)(vlTOPp->v__DOT__M_RegWrite));
}

void VAdd::_settle__TOP__5(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_settle__TOP__5\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__CP0__DOT__IF_Exception_Mask = (1 
						   & ((((IData)(vlTOPp->v__DOT__M_M_CanErr) 
							| (IData)(vlTOPp->v__DOT__EX_CanErr)) 
						       | ((((IData)(vlTOPp->v__DOT__Controller__DOT__DP_Exceptions) 
							    >> 2) 
							   | ((IData)(vlTOPp->v__DOT__Controller__DOT__DP_Exceptions) 
							      >> 1)) 
							  | (IData)(vlTOPp->v__DOT__Controller__DOT__DP_Exceptions))) 
						      | (IData)(vlTOPp->v__DOT__CP0__DOT__EXC_Int)));
    vlTOPp->DataMem_Out = ((0xffffff00 & vlTOPp->DataMem_Out) 
			   | (0xff & vlTOPp->v__DOT__M_WriteData_Pre));
    vlTOPp->DataMem_Out = ((0xffffff & vlTOPp->DataMem_Out) 
			   | (0xff000000 & (((IData)(vlTOPp->v__DOT__M_MemByte)
					      ? vlTOPp->v__DOT__M_WriteData_Pre
					      : ((IData)(vlTOPp->v__DOT__M_MemHalf)
						  ? 
						 (vlTOPp->v__DOT__M_WriteData_Pre 
						  >> 8)
						  : 
						 (vlTOPp->v__DOT__M_WriteData_Pre 
						  >> 0x18))) 
					    << 0x18)));
    vlTOPp->DataMem_Out = ((0xff0000ff & vlTOPp->DataMem_Out) 
			   | ((0xff0000 & ((((IData)(vlTOPp->v__DOT__M_MemByte) 
					     | (IData)(vlTOPp->v__DOT__M_MemHalf))
					     ? vlTOPp->v__DOT__M_WriteData_Pre
					     : (vlTOPp->v__DOT__M_WriteData_Pre 
						>> 0x10)) 
					   << 0x10)) 
			      | (0xff00 & (((IData)(vlTOPp->v__DOT__M_MemByte)
					     ? vlTOPp->v__DOT__M_WriteData_Pre
					     : (vlTOPp->v__DOT__M_WriteData_Pre 
						>> 8)) 
					   << 8))));
    vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_RM 
	= ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_R) 
	   & (vlTOPp->v__DOT__M_ALUResult ^ (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)));
    vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_LL 
	= ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_L) 
	   & ((vlTOPp->v__DOT__M_ALUResult >> 1) ^ ~ vlTOPp->v__DOT__M_ALUResult));
    vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_LM 
	= ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_L) 
	   & (vlTOPp->v__DOT__M_ALUResult ^ ~ (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)));
    vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect = 
	(((IData)(vlTOPp->v__DOT__M_EXC_AdEL) | (IData)(vlTOPp->v__DOT__M_EXC_AdES)) 
	 | (IData)(vlTOPp->v__DOT__M_EXC_Tr));
    vlTOPp->v__DOT__CP0__DOT__ID_Exception_Detect = 
	((((IData)(vlTOPp->v__DOT__ID_EXC_Sys) | (IData)(vlTOPp->v__DOT__ID_EXC_Bp)) 
	  | (IData)(vlTOPp->v__DOT__CP0__DOT__EXC_CpU)) 
	 | (IData)(vlTOPp->v__DOT__CP0__DOT__EXC_Int));
    vlTOPp->v__DOT__HazardControl__DOT__Rs_IDMEM_Match 
	= (((((0x1f & (vlTOPp->v__DOT__Instruction 
		       >> 0x15)) == (IData)(vlTOPp->v__DOT__M_RtRd)) 
	     & (0 != (IData)(vlTOPp->v__DOT__M_RtRd))) 
	    & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
		>> 7) | ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
			 >> 6))) & (IData)(vlTOPp->v__DOT__M_RegWrite));
    vlTOPp->v__DOT__HazardControl__DOT__Rt_IDMEM_Match 
	= (((((0x1f & (vlTOPp->v__DOT__Instruction 
		       >> 0x10)) == (IData)(vlTOPp->v__DOT__M_RtRd)) 
	     & (0 != (IData)(vlTOPp->v__DOT__M_RtRd))) 
	    & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
		>> 5) | ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
			 >> 4))) & (IData)(vlTOPp->v__DOT__M_RegWrite));
    vlTOPp->v__DOT__HazardControl__DOT__Rs_EXMEM_Match 
	= (((((IData)(vlTOPp->v__DOT__EX_Rs) == (IData)(vlTOPp->v__DOT__M_RtRd)) 
	     & (0 != (IData)(vlTOPp->v__DOT__M_RtRd))) 
	    & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
		>> 3) | ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
			 >> 2))) & (IData)(vlTOPp->v__DOT__M_RegWrite));
    vlTOPp->v__DOT__HazardControl__DOT__Rt_EXMEM_Match 
	= (((((IData)(vlTOPp->v__DOT__EX_Rt) == (IData)(vlTOPp->v__DOT__M_RtRd)) 
	     & (0 != (IData)(vlTOPp->v__DOT__M_RtRd))) 
	    & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
		>> 1) | (IData)(vlTOPp->v__DOT__HAZ_DP_Hazards))) 
	   & (IData)(vlTOPp->v__DOT__M_RegWrite));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/MemControl.v:635
    vlTOPp->DataMem_Write = (((((IData)(vlTOPp->v__DOT__M_MemWrite) 
				& (~ (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_KernelMem) 
				       | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Word)) 
				      | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Half)))) 
			       & (~ (((IData)(vlTOPp->v__DOT__M_LLSC) 
				      & (IData)(vlTOPp->v__DOT__M_MemWrite)) 
				     & ((~ (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__LLSC_Atomic)) 
					| ((0x3fffffff 
					    & (vlTOPp->v__DOT__M_ALUResult 
					       >> 2)) 
					   != vlTOPp->v__DOT__DataMem_Controller__DOT__LLSC_Address))))) 
			      & (~ (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__RW_Mask)))
			      ? ((IData)(vlTOPp->v__DOT__M_MemByte)
				  ? (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_LL) 
				      << 3) | (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_LM) 
						<< 2) 
					       | (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_RM) 
						   << 1) 
						  | ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_R) 
						     & ((vlTOPp->v__DOT__M_ALUResult 
							 >> 1) 
							^ ~ vlTOPp->v__DOT__M_ALUResult)))))
				  : ((IData)(vlTOPp->v__DOT__M_MemHalf)
				      ? (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_L) 
					  << 3) | (
						   ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_L) 
						    << 2) 
						   | (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_R) 
						       << 1) 
						      | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_R))))
				      : ((IData)(vlTOPp->v__DOT__M_Left)
					  ? ((2 & vlTOPp->v__DOT__M_ALUResult)
					      ? ((1 
						  & vlTOPp->v__DOT__M_ALUResult)
						  ? 
						 ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						   ? 1
						   : 0xf)
						  : 
						 ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						   ? 3
						   : 7))
					      : ((1 
						  & vlTOPp->v__DOT__M_ALUResult)
						  ? 
						 ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						   ? 7
						   : 3)
						  : 
						 ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						   ? 0xf
						   : 1)))
					  : ((IData)(vlTOPp->v__DOT__M_Right)
					      ? ((2 
						  & vlTOPp->v__DOT__M_ALUResult)
						  ? 
						 ((1 
						   & vlTOPp->v__DOT__M_ALUResult)
						   ? 
						  ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						    ? 0xf
						    : 8)
						   : 
						  ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						    ? 0xe
						    : 0xc))
						  : 
						 ((1 
						   & vlTOPp->v__DOT__M_ALUResult)
						   ? 
						  ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						    ? 0xc
						    : 0xe)
						   : 
						  ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						    ? 8
						    : 0xf)))
					      : 0xf))))
			      : 0);
    vlTOPp->v__DOT__ID_RsFwdSel = (((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rs_IDMEM_Match) 
				    & (~ ((IData)(vlTOPp->v__DOT__M_MemRead) 
					  | (IData)(vlTOPp->v__DOT__M_MemWrite))))
				    ? 1 : ((((((0x1f 
						& (vlTOPp->v__DOT__Instruction 
						   >> 0x15)) 
					       == (IData)(vlTOPp->v__DOT__WB_RtRd)) 
					      & (0 
						 != (IData)(vlTOPp->v__DOT__WB_RtRd))) 
					     & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
						 >> 7) 
						| ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
						   >> 6))) 
					    & (IData)(vlTOPp->v__DOT__WB_RegWrite))
					    ? 2 : 0));
    vlTOPp->v__DOT__ID_RtFwdSel = ((IData)(vlTOPp->v__DOT__ID_Mfc0)
				    ? 3 : (((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rt_IDMEM_Match) 
					    & (~ ((IData)(vlTOPp->v__DOT__M_MemRead) 
						  | (IData)(vlTOPp->v__DOT__M_MemWrite))))
					    ? 1 : (
						   (((((0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10)) 
						       == (IData)(vlTOPp->v__DOT__WB_RtRd)) 
						      & (0 
							 != (IData)(vlTOPp->v__DOT__WB_RtRd))) 
						     & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
							 >> 5) 
							| ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
							   >> 4))) 
						    & (IData)(vlTOPp->v__DOT__WB_RegWrite))
						    ? 2
						    : 0)));
    vlTOPp->v__DOT__EX_RsFwdSel = ((IData)(vlTOPp->v__DOT__EX_Link)
				    ? 3 : (((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rs_EXMEM_Match) 
					    & (~ ((IData)(vlTOPp->v__DOT__M_MemRead) 
						  | (IData)(vlTOPp->v__DOT__M_MemWrite))))
					    ? 1 : (
						   (((((IData)(vlTOPp->v__DOT__EX_Rs) 
						       == (IData)(vlTOPp->v__DOT__WB_RtRd)) 
						      & (0 
							 != (IData)(vlTOPp->v__DOT__WB_RtRd))) 
						     & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
							 >> 3) 
							| ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
							   >> 2))) 
						    & (IData)(vlTOPp->v__DOT__WB_RegWrite))
						    ? 2
						    : 0)));
    vlTOPp->v__DOT__EX_RtFwdSel = ((IData)(vlTOPp->v__DOT__EX_Link)
				    ? 3 : (((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rt_EXMEM_Match) 
					    & (~ ((IData)(vlTOPp->v__DOT__M_MemRead) 
						  | (IData)(vlTOPp->v__DOT__M_MemWrite))))
					    ? 1 : (
						   (((((IData)(vlTOPp->v__DOT__EX_Rt) 
						       == (IData)(vlTOPp->v__DOT__WB_RtRd)) 
						      & (0 
							 != (IData)(vlTOPp->v__DOT__WB_RtRd))) 
						     & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
							 >> 1) 
							| (IData)(vlTOPp->v__DOT__HAZ_DP_Hazards))) 
						    & (IData)(vlTOPp->v__DOT__WB_RegWrite))
						    ? 2
						    : 0)));
}

void VAdd::_combo__TOP__6(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_combo__TOP__6\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__CP0__DOT__ID_Exception_Detect = 
	((((IData)(vlTOPp->v__DOT__ID_EXC_Sys) | (IData)(vlTOPp->v__DOT__ID_EXC_Bp)) 
	  | (IData)(vlTOPp->v__DOT__CP0__DOT__EXC_CpU)) 
	 | (IData)(vlTOPp->v__DOT__CP0__DOT__EXC_Int));
}

void VAdd::_sequent__TOP__7(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_sequent__TOP__7\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/MemControl.v:635
    vlTOPp->DataMem_Write = (((((IData)(vlTOPp->v__DOT__M_MemWrite) 
				& (~ (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_KernelMem) 
				       | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Word)) 
				      | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__EXC_Half)))) 
			       & (~ (((IData)(vlTOPp->v__DOT__M_LLSC) 
				      & (IData)(vlTOPp->v__DOT__M_MemWrite)) 
				     & ((~ (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__LLSC_Atomic)) 
					| ((0x3fffffff 
					    & (vlTOPp->v__DOT__M_ALUResult 
					       >> 2)) 
					   != vlTOPp->v__DOT__DataMem_Controller__DOT__LLSC_Address))))) 
			      & (~ (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__RW_Mask)))
			      ? ((IData)(vlTOPp->v__DOT__M_MemByte)
				  ? (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_LL) 
				      << 3) | (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_LM) 
						<< 2) 
					       | (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Byte_Access_RM) 
						   << 1) 
						  | ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_R) 
						     & ((vlTOPp->v__DOT__M_ALUResult 
							 >> 1) 
							^ ~ vlTOPp->v__DOT__M_ALUResult)))))
				  : ((IData)(vlTOPp->v__DOT__M_MemHalf)
				      ? (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_L) 
					  << 3) | (
						   ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_L) 
						    << 2) 
						   | (((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_R) 
						       << 1) 
						      | (IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__Half_Access_R))))
				      : ((IData)(vlTOPp->v__DOT__M_Left)
					  ? ((2 & vlTOPp->v__DOT__M_ALUResult)
					      ? ((1 
						  & vlTOPp->v__DOT__M_ALUResult)
						  ? 
						 ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						   ? 1
						   : 0xf)
						  : 
						 ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						   ? 3
						   : 7))
					      : ((1 
						  & vlTOPp->v__DOT__M_ALUResult)
						  ? 
						 ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						   ? 7
						   : 3)
						  : 
						 ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						   ? 0xf
						   : 1)))
					  : ((IData)(vlTOPp->v__DOT__M_Right)
					      ? ((2 
						  & vlTOPp->v__DOT__M_ALUResult)
						  ? 
						 ((1 
						   & vlTOPp->v__DOT__M_ALUResult)
						   ? 
						  ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						    ? 0xf
						    : 8)
						   : 
						  ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						    ? 0xe
						    : 0xc))
						  : 
						 ((1 
						   & vlTOPp->v__DOT__M_ALUResult)
						   ? 
						  ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						    ? 0xc
						    : 0xe)
						   : 
						  ((IData)(vlTOPp->v__DOT__DataMem_Controller__DOT__BE)
						    ? 8
						    : 0xf)))
					      : 0xf))))
			      : 0);
    vlTOPp->v__DOT__ID_RsFwdSel = (((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rs_IDMEM_Match) 
				    & (~ ((IData)(vlTOPp->v__DOT__M_MemRead) 
					  | (IData)(vlTOPp->v__DOT__M_MemWrite))))
				    ? 1 : ((((((0x1f 
						& (vlTOPp->v__DOT__Instruction 
						   >> 0x15)) 
					       == (IData)(vlTOPp->v__DOT__WB_RtRd)) 
					      & (0 
						 != (IData)(vlTOPp->v__DOT__WB_RtRd))) 
					     & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
						 >> 7) 
						| ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
						   >> 6))) 
					    & (IData)(vlTOPp->v__DOT__WB_RegWrite))
					    ? 2 : 0));
    vlTOPp->v__DOT__ID_RtFwdSel = ((IData)(vlTOPp->v__DOT__ID_Mfc0)
				    ? 3 : (((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rt_IDMEM_Match) 
					    & (~ ((IData)(vlTOPp->v__DOT__M_MemRead) 
						  | (IData)(vlTOPp->v__DOT__M_MemWrite))))
					    ? 1 : (
						   (((((0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10)) 
						       == (IData)(vlTOPp->v__DOT__WB_RtRd)) 
						      & (0 
							 != (IData)(vlTOPp->v__DOT__WB_RtRd))) 
						     & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
							 >> 5) 
							| ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
							   >> 4))) 
						    & (IData)(vlTOPp->v__DOT__WB_RegWrite))
						    ? 2
						    : 0)));
    vlTOPp->v__DOT__EX_RsFwdSel = ((IData)(vlTOPp->v__DOT__EX_Link)
				    ? 3 : (((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rs_EXMEM_Match) 
					    & (~ ((IData)(vlTOPp->v__DOT__M_MemRead) 
						  | (IData)(vlTOPp->v__DOT__M_MemWrite))))
					    ? 1 : (
						   (((((IData)(vlTOPp->v__DOT__EX_Rs) 
						       == (IData)(vlTOPp->v__DOT__WB_RtRd)) 
						      & (0 
							 != (IData)(vlTOPp->v__DOT__WB_RtRd))) 
						     & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
							 >> 3) 
							| ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
							   >> 2))) 
						    & (IData)(vlTOPp->v__DOT__WB_RegWrite))
						    ? 2
						    : 0)));
    vlTOPp->v__DOT__EX_RtFwdSel = ((IData)(vlTOPp->v__DOT__EX_Link)
				    ? 3 : (((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rt_EXMEM_Match) 
					    & (~ ((IData)(vlTOPp->v__DOT__M_MemRead) 
						  | (IData)(vlTOPp->v__DOT__M_MemWrite))))
					    ? 1 : (
						   (((((IData)(vlTOPp->v__DOT__EX_Rt) 
						       == (IData)(vlTOPp->v__DOT__WB_RtRd)) 
						      & (0 
							 != (IData)(vlTOPp->v__DOT__WB_RtRd))) 
						     & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
							 >> 1) 
							| (IData)(vlTOPp->v__DOT__HAZ_DP_Hazards))) 
						    & (IData)(vlTOPp->v__DOT__WB_RegWrite))
						    ? 2
						    : 0)));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Mux4.v:23
    vlTOPp->v__DOT__ID_ReadData1_End = ((2 & (IData)(vlTOPp->v__DOT__ID_RsFwdSel))
					 ? ((1 & (IData)(vlTOPp->v__DOT__ID_RsFwdSel))
					     ? 0 : vlTOPp->v__DOT__WB_WriteData)
					 : ((1 & (IData)(vlTOPp->v__DOT__ID_RsFwdSel))
					     ? vlTOPp->v__DOT__M_ALUResult
					     : ((0 
						 == 
						 (0x1f 
						  & (vlTOPp->v__DOT__Instruction 
						     >> 0x15)))
						 ? 0
						 : 
						(((((((((1 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15))) 
							| (2 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x15)))) 
						       | (3 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x15)))) 
						      | (4 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))) 
						     | (5 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x15)))) 
						    | (6 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x15)))) 
						   | (7 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x15)))) 
						  | (8 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x15))))
						  ? 
						 ((1 
						   == 
						   (0x1f 
						    & (vlTOPp->v__DOT__Instruction 
						       >> 0x15)))
						   ? vlTOPp->v__DOT__RegisterFile__DOT__registers1
						   : 
						  ((2 
						    == 
						    (0x1f 
						     & (vlTOPp->v__DOT__Instruction 
							>> 0x15)))
						    ? vlTOPp->v__DOT__RegisterFile__DOT__registers2
						    : 
						   ((3 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x15)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers3
						     : 
						    ((4 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x15)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers4
						      : 
						     ((5 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x15)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers5
						       : 
						      ((6 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x15)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers6
						        : 
						       ((7 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers7
							 : vlTOPp->v__DOT__RegisterFile__DOT__registers8)))))))
						  : 
						 (((((((((9 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x15))) 
							 | (0xa 
							    == 
							    (0x1f 
							     & (vlTOPp->v__DOT__Instruction 
								>> 0x15)))) 
							| (0xb 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x15)))) 
						       | (0xc 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x15)))) 
						      | (0xd 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))) 
						     | (0xe 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x15)))) 
						    | (0xf 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x15)))) 
						   | (0x10 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x15))))
						   ? 
						  ((9 
						    == 
						    (0x1f 
						     & (vlTOPp->v__DOT__Instruction 
							>> 0x15)))
						    ? vlTOPp->v__DOT__RegisterFile__DOT__registers9
						    : 
						   ((0xa 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x15)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers10
						     : 
						    ((0xb 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x15)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers11
						      : 
						     ((0xc 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x15)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers12
						       : 
						      ((0xd 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x15)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers13
						        : 
						       ((0xe 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers14
							 : 
							((0xf 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x15)))
							  ? vlTOPp->v__DOT__RegisterFile__DOT__registers15
							  : vlTOPp->v__DOT__RegisterFile__DOT__registers16)))))))
						   : 
						  (((((((((0x11 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x15))) 
							  | (0x12 
							     == 
							     (0x1f 
							      & (vlTOPp->v__DOT__Instruction 
								 >> 0x15)))) 
							 | (0x13 
							    == 
							    (0x1f 
							     & (vlTOPp->v__DOT__Instruction 
								>> 0x15)))) 
							| (0x14 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x15)))) 
						       | (0x15 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x15)))) 
						      | (0x16 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))) 
						     | (0x17 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x15)))) 
						    | (0x18 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x15))))
						    ? 
						   ((0x11 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x15)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers17
						     : 
						    ((0x12 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x15)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers18
						      : 
						     ((0x13 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x15)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers19
						       : 
						      ((0x14 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x15)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers20
						        : 
						       ((0x15 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers21
							 : 
							((0x16 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x15)))
							  ? vlTOPp->v__DOT__RegisterFile__DOT__registers22
							  : 
							 ((0x17 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x15)))
							   ? vlTOPp->v__DOT__RegisterFile__DOT__registers23
							   : vlTOPp->v__DOT__RegisterFile__DOT__registers24)))))))
						    : 
						   ((0x19 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x15)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers25
						     : 
						    ((0x1a 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x15)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers26
						      : 
						     ((0x1b 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x15)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers27
						       : 
						      ((0x1c 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x15)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers28
						        : 
						       ((0x1d 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers29
							 : 
							((0x1e 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x15)))
							  ? vlTOPp->v__DOT__RegisterFile__DOT__registers30
							  : 
							 ((0x1f 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x15)))
							   ? vlTOPp->v__DOT__RegisterFile__DOT__registers31
							   : 0)))))))))))));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Mux4.v:23
    vlTOPp->v__DOT__ID_ReadData2_End = ((2 & (IData)(vlTOPp->v__DOT__ID_RtFwdSel))
					 ? ((1 & (IData)(vlTOPp->v__DOT__ID_RtFwdSel))
					     ? (((IData)(vlTOPp->v__DOT__ID_Mfc0) 
						 & ((IData)(vlTOPp->v__DOT__CP0__DOT__Status_CU_0) 
						    | (IData)(vlTOPp->v__DOT__ID_KernelMode)))
						 ? 
						((0x8000 
						  & vlTOPp->v__DOT__Instruction)
						  ? 
						 ((0x4000 
						   & vlTOPp->v__DOT__Instruction)
						   ? 
						  ((0x2000 
						    & vlTOPp->v__DOT__Instruction)
						    ? 
						   ((0x1000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 
						    ((0x800 
						      & vlTOPp->v__DOT__Instruction)
						      ? 0
						      : vlTOPp->v__DOT__CP0__DOT__ErrorEPC)
						     : 0)
						    : 0)
						   : 
						  ((0x2000 
						    & vlTOPp->v__DOT__Instruction)
						    ? 0
						    : 
						   ((0x1000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 0
						     : 
						    ((0x800 
						      & vlTOPp->v__DOT__Instruction)
						      ? 0
						      : 
						     ((0 
						       == 
						       (7 
							& vlTOPp->v__DOT__Instruction))
						       ? 0x80008000
						       : 0)))))
						  : 
						 ((0x4000 
						   & vlTOPp->v__DOT__Instruction)
						   ? 
						  ((0x2000 
						    & vlTOPp->v__DOT__Instruction)
						    ? 
						   ((0x1000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 
						    ((0x800 
						      & vlTOPp->v__DOT__Instruction)
						      ? 1
						      : vlTOPp->v__DOT__CP0__DOT__EPC)
						     : 
						    ((0x800 
						      & vlTOPp->v__DOT__Instruction)
						      ? 
						     (((IData)(vlTOPp->v__DOT__CP0__DOT__Cause_BD) 
						       << 0x1f) 
						      | (((IData)(vlTOPp->v__DOT__CP0__DOT__Cause_CE) 
							  << 0x1c) 
							 | (((IData)(vlTOPp->v__DOT__CP0__DOT__Cause_IV) 
							     << 0x17) 
							    | (((IData)(vlTOPp->v__DOT__CP0__DOT__Cause_IP) 
								<< 8) 
							       | ((IData)(vlTOPp->v__DOT__CP0__DOT__Cause_ExcCode30) 
								  << 2)))))
						      : 
						     (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_CU_0) 
						       << 0x1c) 
						      | (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_RE) 
							  << 0x19) 
							 | (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_BEV) 
							     << 0x16) 
							    | (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_NMI) 
								<< 0x13) 
							       | (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_IM) 
								   << 8) 
								  | (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_UM) 
								      << 4) 
								     | (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_ERL) 
									 << 2) 
									| (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL) 
									    << 1) 
									   | (IData)(vlTOPp->v__DOT__CP0__DOT__Status_IE)))))))))))
						    : 
						   ((0x1000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 
						    ((0x800 
						      & vlTOPp->v__DOT__Instruction)
						      ? vlTOPp->v__DOT__CP0__DOT__Compare
						      : 0)
						     : 
						    ((0x800 
						      & vlTOPp->v__DOT__Instruction)
						      ? vlTOPp->v__DOT__CP0__DOT__Count
						      : vlTOPp->v__DOT__CP0__DOT__BadVAddr)))
						   : 0))
						 : 0)
					     : vlTOPp->v__DOT__WB_WriteData)
					 : ((1 & (IData)(vlTOPp->v__DOT__ID_RtFwdSel))
					     ? vlTOPp->v__DOT__M_ALUResult
					     : ((0 
						 == 
						 (0x1f 
						  & (vlTOPp->v__DOT__Instruction 
						     >> 0x10)))
						 ? 0
						 : 
						(((((((((1 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10))) 
							| (2 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x10)))) 
						       | (3 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x10)))) 
						      | (4 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10)))) 
						     | (5 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x10)))) 
						    | (6 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10)))) 
						   | (7 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x10)))) 
						  | (8 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x10))))
						  ? 
						 ((1 
						   == 
						   (0x1f 
						    & (vlTOPp->v__DOT__Instruction 
						       >> 0x10)))
						   ? vlTOPp->v__DOT__RegisterFile__DOT__registers1
						   : 
						  ((2 
						    == 
						    (0x1f 
						     & (vlTOPp->v__DOT__Instruction 
							>> 0x10)))
						    ? vlTOPp->v__DOT__RegisterFile__DOT__registers2
						    : 
						   ((3 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x10)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers3
						     : 
						    ((4 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x10)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers4
						      : 
						     ((5 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers5
						       : 
						      ((6 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x10)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers6
						        : 
						       ((7 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers7
							 : vlTOPp->v__DOT__RegisterFile__DOT__registers8)))))))
						  : 
						 (((((((((9 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x10))) 
							 | (0xa 
							    == 
							    (0x1f 
							     & (vlTOPp->v__DOT__Instruction 
								>> 0x10)))) 
							| (0xb 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x10)))) 
						       | (0xc 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x10)))) 
						      | (0xd 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10)))) 
						     | (0xe 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x10)))) 
						    | (0xf 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10)))) 
						   | (0x10 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x10))))
						   ? 
						  ((9 
						    == 
						    (0x1f 
						     & (vlTOPp->v__DOT__Instruction 
							>> 0x10)))
						    ? vlTOPp->v__DOT__RegisterFile__DOT__registers9
						    : 
						   ((0xa 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x10)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers10
						     : 
						    ((0xb 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x10)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers11
						      : 
						     ((0xc 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers12
						       : 
						      ((0xd 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x10)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers13
						        : 
						       ((0xe 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers14
							 : 
							((0xf 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x10)))
							  ? vlTOPp->v__DOT__RegisterFile__DOT__registers15
							  : vlTOPp->v__DOT__RegisterFile__DOT__registers16)))))))
						   : 
						  (((((((((0x11 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x10))) 
							  | (0x12 
							     == 
							     (0x1f 
							      & (vlTOPp->v__DOT__Instruction 
								 >> 0x10)))) 
							 | (0x13 
							    == 
							    (0x1f 
							     & (vlTOPp->v__DOT__Instruction 
								>> 0x10)))) 
							| (0x14 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x10)))) 
						       | (0x15 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x10)))) 
						      | (0x16 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10)))) 
						     | (0x17 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x10)))) 
						    | (0x18 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10))))
						    ? 
						   ((0x11 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x10)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers17
						     : 
						    ((0x12 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x10)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers18
						      : 
						     ((0x13 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers19
						       : 
						      ((0x14 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x10)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers20
						        : 
						       ((0x15 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers21
							 : 
							((0x16 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x10)))
							  ? vlTOPp->v__DOT__RegisterFile__DOT__registers22
							  : 
							 ((0x17 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x10)))
							   ? vlTOPp->v__DOT__RegisterFile__DOT__registers23
							   : vlTOPp->v__DOT__RegisterFile__DOT__registers24)))))))
						    : 
						   ((0x19 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x10)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers25
						     : 
						    ((0x1a 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x10)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers26
						      : 
						     ((0x1b 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers27
						       : 
						      ((0x1c 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x10)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers28
						        : 
						       ((0x1d 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers29
							 : 
							((0x1e 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x10)))
							  ? vlTOPp->v__DOT__RegisterFile__DOT__registers30
							  : 
							 ((0x1f 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x10)))
							   ? vlTOPp->v__DOT__RegisterFile__DOT__registers31
							   : 0)))))))))))));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Mux4.v:23
    vlTOPp->v__DOT__EX_ReadData1_Fwd = ((2 & (IData)(vlTOPp->v__DOT__EX_RsFwdSel))
					 ? ((1 & (IData)(vlTOPp->v__DOT__EX_RsFwdSel))
					     ? vlTOPp->v__DOT__EX_RestartPC
					     : vlTOPp->v__DOT__WB_WriteData)
					 : ((1 & (IData)(vlTOPp->v__DOT__EX_RsFwdSel))
					     ? vlTOPp->v__DOT__M_ALUResult
					     : vlTOPp->v__DOT__EX_ReadData1_PR));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Mux4.v:23
    vlTOPp->v__DOT__EX_ReadData2_Fwd = ((2 & (IData)(vlTOPp->v__DOT__EX_RtFwdSel))
					 ? ((1 & (IData)(vlTOPp->v__DOT__EX_RtFwdSel))
					     ? 8 : vlTOPp->v__DOT__WB_WriteData)
					 : ((1 & (IData)(vlTOPp->v__DOT__EX_RtFwdSel))
					     ? vlTOPp->v__DOT__M_ALUResult
					     : vlTOPp->v__DOT__EX_ReadData2_PR));
}

void VAdd::_settle__TOP__8(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_settle__TOP__8\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Mux4.v:23
    vlTOPp->v__DOT__ID_ReadData1_End = ((2 & (IData)(vlTOPp->v__DOT__ID_RsFwdSel))
					 ? ((1 & (IData)(vlTOPp->v__DOT__ID_RsFwdSel))
					     ? 0 : vlTOPp->v__DOT__WB_WriteData)
					 : ((1 & (IData)(vlTOPp->v__DOT__ID_RsFwdSel))
					     ? vlTOPp->v__DOT__M_ALUResult
					     : ((0 
						 == 
						 (0x1f 
						  & (vlTOPp->v__DOT__Instruction 
						     >> 0x15)))
						 ? 0
						 : 
						(((((((((1 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15))) 
							| (2 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x15)))) 
						       | (3 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x15)))) 
						      | (4 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))) 
						     | (5 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x15)))) 
						    | (6 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x15)))) 
						   | (7 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x15)))) 
						  | (8 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x15))))
						  ? 
						 ((1 
						   == 
						   (0x1f 
						    & (vlTOPp->v__DOT__Instruction 
						       >> 0x15)))
						   ? vlTOPp->v__DOT__RegisterFile__DOT__registers1
						   : 
						  ((2 
						    == 
						    (0x1f 
						     & (vlTOPp->v__DOT__Instruction 
							>> 0x15)))
						    ? vlTOPp->v__DOT__RegisterFile__DOT__registers2
						    : 
						   ((3 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x15)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers3
						     : 
						    ((4 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x15)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers4
						      : 
						     ((5 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x15)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers5
						       : 
						      ((6 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x15)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers6
						        : 
						       ((7 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers7
							 : vlTOPp->v__DOT__RegisterFile__DOT__registers8)))))))
						  : 
						 (((((((((9 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x15))) 
							 | (0xa 
							    == 
							    (0x1f 
							     & (vlTOPp->v__DOT__Instruction 
								>> 0x15)))) 
							| (0xb 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x15)))) 
						       | (0xc 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x15)))) 
						      | (0xd 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))) 
						     | (0xe 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x15)))) 
						    | (0xf 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x15)))) 
						   | (0x10 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x15))))
						   ? 
						  ((9 
						    == 
						    (0x1f 
						     & (vlTOPp->v__DOT__Instruction 
							>> 0x15)))
						    ? vlTOPp->v__DOT__RegisterFile__DOT__registers9
						    : 
						   ((0xa 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x15)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers10
						     : 
						    ((0xb 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x15)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers11
						      : 
						     ((0xc 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x15)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers12
						       : 
						      ((0xd 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x15)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers13
						        : 
						       ((0xe 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers14
							 : 
							((0xf 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x15)))
							  ? vlTOPp->v__DOT__RegisterFile__DOT__registers15
							  : vlTOPp->v__DOT__RegisterFile__DOT__registers16)))))))
						   : 
						  (((((((((0x11 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x15))) 
							  | (0x12 
							     == 
							     (0x1f 
							      & (vlTOPp->v__DOT__Instruction 
								 >> 0x15)))) 
							 | (0x13 
							    == 
							    (0x1f 
							     & (vlTOPp->v__DOT__Instruction 
								>> 0x15)))) 
							| (0x14 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x15)))) 
						       | (0x15 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x15)))) 
						      | (0x16 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))) 
						     | (0x17 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x15)))) 
						    | (0x18 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x15))))
						    ? 
						   ((0x11 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x15)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers17
						     : 
						    ((0x12 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x15)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers18
						      : 
						     ((0x13 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x15)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers19
						       : 
						      ((0x14 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x15)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers20
						        : 
						       ((0x15 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers21
							 : 
							((0x16 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x15)))
							  ? vlTOPp->v__DOT__RegisterFile__DOT__registers22
							  : 
							 ((0x17 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x15)))
							   ? vlTOPp->v__DOT__RegisterFile__DOT__registers23
							   : vlTOPp->v__DOT__RegisterFile__DOT__registers24)))))))
						    : 
						   ((0x19 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x15)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers25
						     : 
						    ((0x1a 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x15)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers26
						      : 
						     ((0x1b 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x15)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers27
						       : 
						      ((0x1c 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x15)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers28
						        : 
						       ((0x1d 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers29
							 : 
							((0x1e 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x15)))
							  ? vlTOPp->v__DOT__RegisterFile__DOT__registers30
							  : 
							 ((0x1f 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x15)))
							   ? vlTOPp->v__DOT__RegisterFile__DOT__registers31
							   : 0)))))))))))));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Mux4.v:23
    vlTOPp->v__DOT__ID_ReadData2_End = ((2 & (IData)(vlTOPp->v__DOT__ID_RtFwdSel))
					 ? ((1 & (IData)(vlTOPp->v__DOT__ID_RtFwdSel))
					     ? (((IData)(vlTOPp->v__DOT__ID_Mfc0) 
						 & ((IData)(vlTOPp->v__DOT__CP0__DOT__Status_CU_0) 
						    | (IData)(vlTOPp->v__DOT__ID_KernelMode)))
						 ? 
						((0x8000 
						  & vlTOPp->v__DOT__Instruction)
						  ? 
						 ((0x4000 
						   & vlTOPp->v__DOT__Instruction)
						   ? 
						  ((0x2000 
						    & vlTOPp->v__DOT__Instruction)
						    ? 
						   ((0x1000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 
						    ((0x800 
						      & vlTOPp->v__DOT__Instruction)
						      ? 0
						      : vlTOPp->v__DOT__CP0__DOT__ErrorEPC)
						     : 0)
						    : 0)
						   : 
						  ((0x2000 
						    & vlTOPp->v__DOT__Instruction)
						    ? 0
						    : 
						   ((0x1000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 0
						     : 
						    ((0x800 
						      & vlTOPp->v__DOT__Instruction)
						      ? 0
						      : 
						     ((0 
						       == 
						       (7 
							& vlTOPp->v__DOT__Instruction))
						       ? 0x80008000
						       : 0)))))
						  : 
						 ((0x4000 
						   & vlTOPp->v__DOT__Instruction)
						   ? 
						  ((0x2000 
						    & vlTOPp->v__DOT__Instruction)
						    ? 
						   ((0x1000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 
						    ((0x800 
						      & vlTOPp->v__DOT__Instruction)
						      ? 1
						      : vlTOPp->v__DOT__CP0__DOT__EPC)
						     : 
						    ((0x800 
						      & vlTOPp->v__DOT__Instruction)
						      ? 
						     (((IData)(vlTOPp->v__DOT__CP0__DOT__Cause_BD) 
						       << 0x1f) 
						      | (((IData)(vlTOPp->v__DOT__CP0__DOT__Cause_CE) 
							  << 0x1c) 
							 | (((IData)(vlTOPp->v__DOT__CP0__DOT__Cause_IV) 
							     << 0x17) 
							    | (((IData)(vlTOPp->v__DOT__CP0__DOT__Cause_IP) 
								<< 8) 
							       | ((IData)(vlTOPp->v__DOT__CP0__DOT__Cause_ExcCode30) 
								  << 2)))))
						      : 
						     (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_CU_0) 
						       << 0x1c) 
						      | (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_RE) 
							  << 0x19) 
							 | (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_BEV) 
							     << 0x16) 
							    | (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_NMI) 
								<< 0x13) 
							       | (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_IM) 
								   << 8) 
								  | (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_UM) 
								      << 4) 
								     | (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_ERL) 
									 << 2) 
									| (((IData)(vlTOPp->v__DOT__CP0__DOT__Status_EXL) 
									    << 1) 
									   | (IData)(vlTOPp->v__DOT__CP0__DOT__Status_IE)))))))))))
						    : 
						   ((0x1000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 
						    ((0x800 
						      & vlTOPp->v__DOT__Instruction)
						      ? vlTOPp->v__DOT__CP0__DOT__Compare
						      : 0)
						     : 
						    ((0x800 
						      & vlTOPp->v__DOT__Instruction)
						      ? vlTOPp->v__DOT__CP0__DOT__Count
						      : vlTOPp->v__DOT__CP0__DOT__BadVAddr)))
						   : 0))
						 : 0)
					     : vlTOPp->v__DOT__WB_WriteData)
					 : ((1 & (IData)(vlTOPp->v__DOT__ID_RtFwdSel))
					     ? vlTOPp->v__DOT__M_ALUResult
					     : ((0 
						 == 
						 (0x1f 
						  & (vlTOPp->v__DOT__Instruction 
						     >> 0x10)))
						 ? 0
						 : 
						(((((((((1 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10))) 
							| (2 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x10)))) 
						       | (3 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x10)))) 
						      | (4 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10)))) 
						     | (5 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x10)))) 
						    | (6 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10)))) 
						   | (7 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x10)))) 
						  | (8 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x10))))
						  ? 
						 ((1 
						   == 
						   (0x1f 
						    & (vlTOPp->v__DOT__Instruction 
						       >> 0x10)))
						   ? vlTOPp->v__DOT__RegisterFile__DOT__registers1
						   : 
						  ((2 
						    == 
						    (0x1f 
						     & (vlTOPp->v__DOT__Instruction 
							>> 0x10)))
						    ? vlTOPp->v__DOT__RegisterFile__DOT__registers2
						    : 
						   ((3 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x10)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers3
						     : 
						    ((4 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x10)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers4
						      : 
						     ((5 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers5
						       : 
						      ((6 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x10)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers6
						        : 
						       ((7 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers7
							 : vlTOPp->v__DOT__RegisterFile__DOT__registers8)))))))
						  : 
						 (((((((((9 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x10))) 
							 | (0xa 
							    == 
							    (0x1f 
							     & (vlTOPp->v__DOT__Instruction 
								>> 0x10)))) 
							| (0xb 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x10)))) 
						       | (0xc 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x10)))) 
						      | (0xd 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10)))) 
						     | (0xe 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x10)))) 
						    | (0xf 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10)))) 
						   | (0x10 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x10))))
						   ? 
						  ((9 
						    == 
						    (0x1f 
						     & (vlTOPp->v__DOT__Instruction 
							>> 0x10)))
						    ? vlTOPp->v__DOT__RegisterFile__DOT__registers9
						    : 
						   ((0xa 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x10)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers10
						     : 
						    ((0xb 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x10)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers11
						      : 
						     ((0xc 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers12
						       : 
						      ((0xd 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x10)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers13
						        : 
						       ((0xe 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers14
							 : 
							((0xf 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x10)))
							  ? vlTOPp->v__DOT__RegisterFile__DOT__registers15
							  : vlTOPp->v__DOT__RegisterFile__DOT__registers16)))))))
						   : 
						  (((((((((0x11 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x10))) 
							  | (0x12 
							     == 
							     (0x1f 
							      & (vlTOPp->v__DOT__Instruction 
								 >> 0x10)))) 
							 | (0x13 
							    == 
							    (0x1f 
							     & (vlTOPp->v__DOT__Instruction 
								>> 0x10)))) 
							| (0x14 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x10)))) 
						       | (0x15 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x10)))) 
						      | (0x16 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10)))) 
						     | (0x17 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x10)))) 
						    | (0x18 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10))))
						    ? 
						   ((0x11 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x10)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers17
						     : 
						    ((0x12 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x10)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers18
						      : 
						     ((0x13 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers19
						       : 
						      ((0x14 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x10)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers20
						        : 
						       ((0x15 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers21
							 : 
							((0x16 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x10)))
							  ? vlTOPp->v__DOT__RegisterFile__DOT__registers22
							  : 
							 ((0x17 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x10)))
							   ? vlTOPp->v__DOT__RegisterFile__DOT__registers23
							   : vlTOPp->v__DOT__RegisterFile__DOT__registers24)))))))
						    : 
						   ((0x19 
						     == 
						     (0x1f 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x10)))
						     ? vlTOPp->v__DOT__RegisterFile__DOT__registers25
						     : 
						    ((0x1a 
						      == 
						      (0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x10)))
						      ? vlTOPp->v__DOT__RegisterFile__DOT__registers26
						      : 
						     ((0x1b 
						       == 
						       (0x1f 
							& (vlTOPp->v__DOT__Instruction 
							   >> 0x10)))
						       ? vlTOPp->v__DOT__RegisterFile__DOT__registers27
						       : 
						      ((0x1c 
							== 
							(0x1f 
							 & (vlTOPp->v__DOT__Instruction 
							    >> 0x10)))
						        ? vlTOPp->v__DOT__RegisterFile__DOT__registers28
						        : 
						       ((0x1d 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x10)))
							 ? vlTOPp->v__DOT__RegisterFile__DOT__registers29
							 : 
							((0x1e 
							  == 
							  (0x1f 
							   & (vlTOPp->v__DOT__Instruction 
							      >> 0x10)))
							  ? vlTOPp->v__DOT__RegisterFile__DOT__registers30
							  : 
							 ((0x1f 
							   == 
							   (0x1f 
							    & (vlTOPp->v__DOT__Instruction 
							       >> 0x10)))
							   ? vlTOPp->v__DOT__RegisterFile__DOT__registers31
							   : 0)))))))))))));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Mux4.v:23
    vlTOPp->v__DOT__EX_ReadData1_Fwd = ((2 & (IData)(vlTOPp->v__DOT__EX_RsFwdSel))
					 ? ((1 & (IData)(vlTOPp->v__DOT__EX_RsFwdSel))
					     ? vlTOPp->v__DOT__EX_RestartPC
					     : vlTOPp->v__DOT__WB_WriteData)
					 : ((1 & (IData)(vlTOPp->v__DOT__EX_RsFwdSel))
					     ? vlTOPp->v__DOT__M_ALUResult
					     : vlTOPp->v__DOT__EX_ReadData1_PR));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Mux4.v:23
    vlTOPp->v__DOT__EX_ReadData2_Fwd = ((2 & (IData)(vlTOPp->v__DOT__EX_RtFwdSel))
					 ? ((1 & (IData)(vlTOPp->v__DOT__EX_RtFwdSel))
					     ? 8 : vlTOPp->v__DOT__WB_WriteData)
					 : ((1 & (IData)(vlTOPp->v__DOT__EX_RtFwdSel))
					     ? vlTOPp->v__DOT__M_ALUResult
					     : vlTOPp->v__DOT__EX_ReadData2_PR));
    vlTOPp->v__DOT__ID_CmpEQ = (vlTOPp->v__DOT__ID_ReadData1_End 
				== vlTOPp->v__DOT__ID_ReadData2_End);
    vlTOPp->v__DOT__EX_ReadData2_Imm = ((IData)(vlTOPp->v__DOT__EX_ALUSrcImm)
					 ? vlTOPp->v__DOT__EX_SignExtImm
					 : vlTOPp->v__DOT__EX_ReadData2_Fwd);
}

void VAdd::_sequent__TOP__9(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_sequent__TOP__9\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__ID_CmpEQ = (vlTOPp->v__DOT__ID_ReadData1_End 
				== vlTOPp->v__DOT__ID_ReadData2_End);
    vlTOPp->v__DOT__EX_ReadData2_Imm = ((IData)(vlTOPp->v__DOT__EX_ALUSrcImm)
					 ? vlTOPp->v__DOT__EX_SignExtImm
					 : vlTOPp->v__DOT__EX_ReadData2_Fwd);
    vlTOPp->v__DOT__ALU__DOT__Mult_Result = VL_MULS_QQQ(64,64,64, 
							VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__EX_ReadData1_Fwd), 
							VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__EX_ReadData2_Imm));
    vlTOPp->v__DOT__ALU__DOT__Multu_Result = ((QData)((IData)(vlTOPp->v__DOT__EX_ReadData1_Fwd)) 
					      * (QData)((IData)(vlTOPp->v__DOT__EX_ReadData2_Imm)));
    vlTOPp->v__DOT__ALU__DOT__AddSub_Result = (((1 
						 == (IData)(vlTOPp->v__DOT__EX_ALUOp)) 
						| (0 
						   == (IData)(vlTOPp->v__DOT__EX_ALUOp)))
					        ? (vlTOPp->v__DOT__EX_ReadData1_Fwd 
						   + vlTOPp->v__DOT__EX_ReadData2_Imm)
					        : (vlTOPp->v__DOT__EX_ReadData1_Fwd 
						   - vlTOPp->v__DOT__EX_ReadData2_Imm));
}

void VAdd::_settle__TOP__10(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_settle__TOP__10\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__ALU__DOT__Mult_Result = VL_MULS_QQQ(64,64,64, 
							VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__EX_ReadData1_Fwd), 
							VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__EX_ReadData2_Imm));
    vlTOPp->v__DOT__ALU__DOT__Multu_Result = ((QData)((IData)(vlTOPp->v__DOT__EX_ReadData1_Fwd)) 
					      * (QData)((IData)(vlTOPp->v__DOT__EX_ReadData2_Imm)));
    vlTOPp->v__DOT__ALU__DOT__AddSub_Result = (((1 
						 == (IData)(vlTOPp->v__DOT__EX_ALUOp)) 
						| (0 
						   == (IData)(vlTOPp->v__DOT__EX_ALUOp)))
					        ? (vlTOPp->v__DOT__EX_ReadData1_Fwd 
						   + vlTOPp->v__DOT__EX_ReadData2_Imm)
					        : (vlTOPp->v__DOT__EX_ReadData1_Fwd 
						   - vlTOPp->v__DOT__EX_ReadData2_Imm));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/ALU.v:687
    vlTOPp->v__DOT__EX_EXC_Ov = (1 & ((1 == (IData)(vlTOPp->v__DOT__EX_ALUOp))
				       ? (((vlTOPp->v__DOT__EX_ReadData1_Fwd 
					    ^ ~ vlTOPp->v__DOT__EX_ReadData2_Imm) 
					   & (vlTOPp->v__DOT__EX_ReadData1_Fwd 
					      ^ vlTOPp->v__DOT__ALU__DOT__AddSub_Result)) 
					  >> 0x1f) : 
				      ((0x1d == (IData)(vlTOPp->v__DOT__EX_ALUOp)) 
				       & (((vlTOPp->v__DOT__EX_ReadData1_Fwd 
					    ^ vlTOPp->v__DOT__EX_ReadData2_Imm) 
					   & (vlTOPp->v__DOT__EX_ReadData1_Fwd 
					      ^ vlTOPp->v__DOT__ALU__DOT__AddSub_Result)) 
					  >> 0x1f))));
}

void VAdd::_sequent__TOP__11(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_sequent__TOP__11\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/ALU.v:687
    vlTOPp->v__DOT__EX_EXC_Ov = (1 & ((1 == (IData)(vlTOPp->v__DOT__EX_ALUOp))
				       ? (((vlTOPp->v__DOT__EX_ReadData1_Fwd 
					    ^ ~ vlTOPp->v__DOT__EX_ReadData2_Imm) 
					   & (vlTOPp->v__DOT__EX_ReadData1_Fwd 
					      ^ vlTOPp->v__DOT__ALU__DOT__AddSub_Result)) 
					  >> 0x1f) : 
				      ((0x1d == (IData)(vlTOPp->v__DOT__EX_ALUOp)) 
				       & (((vlTOPp->v__DOT__EX_ReadData1_Fwd 
					    ^ vlTOPp->v__DOT__EX_ReadData2_Imm) 
					   & (vlTOPp->v__DOT__EX_ReadData1_Fwd 
					      ^ vlTOPp->v__DOT__ALU__DOT__AddSub_Result)) 
					  >> 0x1f))));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/CPZero.v:1010
    vlTOPp->v__DOT__CP0__DOT__Cause_ExcCode_bits = 
	((IData)(vlTOPp->v__DOT__M_EXC_AdEL) ? 4 : 
	 ((IData)(vlTOPp->v__DOT__M_EXC_AdES) ? 5 : 
	  ((IData)(vlTOPp->v__DOT__M_EXC_Tr) ? 0xd : 
	   ((IData)(vlTOPp->v__DOT__EX_EXC_Ov) ? 0xc
	     : ((IData)(vlTOPp->v__DOT__ID_EXC_Sys)
		 ? 8 : ((IData)(vlTOPp->v__DOT__ID_EXC_Bp)
			 ? 9 : ((IData)(vlTOPp->v__DOT__CP0__DOT__EXC_CpU)
				 ? 0xb : ((IData)(vlTOPp->v__DOT__IF_EXC_AdIF)
					   ? 4 : 0))))))));
    vlTOPp->v__DOT__EX_Exception_Flush = ((IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect) 
					  | (IData)(vlTOPp->v__DOT__EX_EXC_Ov));
}

void VAdd::_combo__TOP__12(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_combo__TOP__12\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__ID_Exception_Flush = (((IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect) 
					   | (IData)(vlTOPp->v__DOT__EX_EXC_Ov)) 
					  | (IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Detect));
    vlTOPp->v__DOT__IF_Stall = ((IData)(vlTOPp->InstMem_Ready) 
				| (((IData)(vlTOPp->v__DOT__IF_EXC_AdIF) 
				    & (IData)(vlTOPp->v__DOT__CP0__DOT__IF_Exception_Mask)) 
				   & (~ (((IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Detect) 
					  | (IData)(vlTOPp->v__DOT__EX_EXC_Ov)) 
					 | (IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect)))));
}

void VAdd::_settle__TOP__13(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_settle__TOP__13\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__ID_Exception_Flush = (((IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect) 
					   | (IData)(vlTOPp->v__DOT__EX_EXC_Ov)) 
					  | (IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Detect));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/CPZero.v:1010
    vlTOPp->v__DOT__CP0__DOT__Cause_ExcCode_bits = 
	((IData)(vlTOPp->v__DOT__M_EXC_AdEL) ? 4 : 
	 ((IData)(vlTOPp->v__DOT__M_EXC_AdES) ? 5 : 
	  ((IData)(vlTOPp->v__DOT__M_EXC_Tr) ? 0xd : 
	   ((IData)(vlTOPp->v__DOT__EX_EXC_Ov) ? 0xc
	     : ((IData)(vlTOPp->v__DOT__ID_EXC_Sys)
		 ? 8 : ((IData)(vlTOPp->v__DOT__ID_EXC_Bp)
			 ? 9 : ((IData)(vlTOPp->v__DOT__CP0__DOT__EXC_CpU)
				 ? 0xb : ((IData)(vlTOPp->v__DOT__IF_EXC_AdIF)
					   ? 4 : 0))))))));
    vlTOPp->v__DOT__EX_Exception_Flush = ((IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect) 
					  | (IData)(vlTOPp->v__DOT__EX_EXC_Ov));
    vlTOPp->v__DOT__IF_Stall = ((IData)(vlTOPp->InstMem_Ready) 
				| (((IData)(vlTOPp->v__DOT__IF_EXC_AdIF) 
				    & (IData)(vlTOPp->v__DOT__CP0__DOT__IF_Exception_Mask)) 
				   & (~ (((IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Detect) 
					  | (IData)(vlTOPp->v__DOT__EX_EXC_Ov)) 
					 | (IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect)))));
    vlTOPp->v__DOT__CP0__DOT__ID_Exception_Mask = (
						   ((IData)(vlTOPp->v__DOT__IF_Stall) 
						    | (IData)(vlTOPp->v__DOT__M_M_CanErr)) 
						   | (IData)(vlTOPp->v__DOT__EX_CanErr));
    vlTOPp->v__DOT__CP0__DOT__EX_Exception_Mask = ((IData)(vlTOPp->v__DOT__IF_Stall) 
						   | (IData)(vlTOPp->v__DOT__M_M_CanErr));
    vlTOPp->v__DOT__M_Stall_Controller = ((((IData)(vlTOPp->DataMem_Read) 
					    | (0 != (IData)(vlTOPp->DataMem_Write))) 
					   | (IData)(vlTOPp->DataMem_Ready)) 
					  | ((IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect) 
					     & (IData)(vlTOPp->v__DOT__IF_Stall)));
}

void VAdd::_combo__TOP__14(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_combo__TOP__14\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__CP0__DOT__ID_Exception_Mask = (
						   ((IData)(vlTOPp->v__DOT__IF_Stall) 
						    | (IData)(vlTOPp->v__DOT__M_M_CanErr)) 
						   | (IData)(vlTOPp->v__DOT__EX_CanErr));
    vlTOPp->v__DOT__CP0__DOT__EX_Exception_Mask = ((IData)(vlTOPp->v__DOT__IF_Stall) 
						   | (IData)(vlTOPp->v__DOT__M_M_CanErr));
    vlTOPp->v__DOT__M_Stall_Controller = ((((IData)(vlTOPp->DataMem_Read) 
					    | (0 != (IData)(vlTOPp->DataMem_Write))) 
					   | (IData)(vlTOPp->DataMem_Ready)) 
					  | ((IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect) 
					     & (IData)(vlTOPp->v__DOT__IF_Stall)));
    vlTOPp->v__DOT__M_Stall = ((IData)(vlTOPp->v__DOT__IF_Stall) 
			       | (IData)(vlTOPp->v__DOT__M_Stall_Controller));
}

void VAdd::_settle__TOP__15(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_settle__TOP__15\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__M_Stall = ((IData)(vlTOPp->v__DOT__IF_Stall) 
			       | (IData)(vlTOPp->v__DOT__M_Stall_Controller));
    vlTOPp->v__DOT__EX_Stall = (((((((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rs_EXMEM_Match) 
				     & ((IData)(vlTOPp->v__DOT__M_MemRead) 
					| (IData)(vlTOPp->v__DOT__M_MemWrite))) 
				    & ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
				       >> 2)) | (((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rt_EXMEM_Match) 
						  & ((IData)(vlTOPp->v__DOT__M_MemRead) 
						     | (IData)(vlTOPp->v__DOT__M_MemWrite))) 
						 & (IData)(vlTOPp->v__DOT__HAZ_DP_Hazards))) 
				  | (((IData)(vlTOPp->v__DOT__EX_EXC_Ov) 
				      & (IData)(vlTOPp->v__DOT__CP0__DOT__EX_Exception_Mask)) 
				     & (~ (IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect)))) 
				 | ((IData)(vlTOPp->v__DOT__ALU__DOT__div_fsm) 
				    & ((0x10 & (IData)(vlTOPp->v__DOT__EX_ALUOp))
				        ? ((~ ((IData)(vlTOPp->v__DOT__EX_ALUOp) 
					       >> 3)) 
					   & ((~ ((IData)(vlTOPp->v__DOT__EX_ALUOp) 
						  >> 2)) 
					      & (~ 
						 ((IData)(vlTOPp->v__DOT__EX_ALUOp) 
						  >> 1))))
				        : ((8 & (IData)(vlTOPp->v__DOT__EX_ALUOp))
					    ? ((~ ((IData)(vlTOPp->v__DOT__EX_ALUOp) 
						   >> 2)) 
					       | ((~ 
						   ((IData)(vlTOPp->v__DOT__EX_ALUOp) 
						    >> 1)) 
						  | (~ (IData)(vlTOPp->v__DOT__EX_ALUOp))))
					    : (((IData)(vlTOPp->v__DOT__EX_ALUOp) 
						>> 2) 
					       & (((IData)(vlTOPp->v__DOT__EX_ALUOp) 
						   >> 1) 
						  | (IData)(vlTOPp->v__DOT__EX_ALUOp))))))) 
				| (IData)(vlTOPp->v__DOT__M_Stall));
}

void VAdd::_combo__TOP__16(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_combo__TOP__16\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__EX_Stall = (((((((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rs_EXMEM_Match) 
				     & ((IData)(vlTOPp->v__DOT__M_MemRead) 
					| (IData)(vlTOPp->v__DOT__M_MemWrite))) 
				    & ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
				       >> 2)) | (((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rt_EXMEM_Match) 
						  & ((IData)(vlTOPp->v__DOT__M_MemRead) 
						     | (IData)(vlTOPp->v__DOT__M_MemWrite))) 
						 & (IData)(vlTOPp->v__DOT__HAZ_DP_Hazards))) 
				  | (((IData)(vlTOPp->v__DOT__EX_EXC_Ov) 
				      & (IData)(vlTOPp->v__DOT__CP0__DOT__EX_Exception_Mask)) 
				     & (~ (IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect)))) 
				 | ((IData)(vlTOPp->v__DOT__ALU__DOT__div_fsm) 
				    & ((0x10 & (IData)(vlTOPp->v__DOT__EX_ALUOp))
				        ? ((~ ((IData)(vlTOPp->v__DOT__EX_ALUOp) 
					       >> 3)) 
					   & ((~ ((IData)(vlTOPp->v__DOT__EX_ALUOp) 
						  >> 2)) 
					      & (~ 
						 ((IData)(vlTOPp->v__DOT__EX_ALUOp) 
						  >> 1))))
				        : ((8 & (IData)(vlTOPp->v__DOT__EX_ALUOp))
					    ? ((~ ((IData)(vlTOPp->v__DOT__EX_ALUOp) 
						   >> 2)) 
					       | ((~ 
						   ((IData)(vlTOPp->v__DOT__EX_ALUOp) 
						    >> 1)) 
						  | (~ (IData)(vlTOPp->v__DOT__EX_ALUOp))))
					    : (((IData)(vlTOPp->v__DOT__EX_ALUOp) 
						>> 2) 
					       & (((IData)(vlTOPp->v__DOT__EX_ALUOp) 
						   >> 1) 
						  | (IData)(vlTOPp->v__DOT__EX_ALUOp))))))) 
				| (IData)(vlTOPp->v__DOT__M_Stall));
    vlTOPp->v__DOT__ALU__DOT__HILO_Commit = (1 & (~ 
						  ((IData)(vlTOPp->v__DOT__EX_Stall) 
						   | (IData)(vlTOPp->v__DOT__EX_Exception_Flush))));
    vlTOPp->v__DOT__ID_Stall = (((((((((((0x1f & (vlTOPp->v__DOT__Instruction 
						  >> 0x15)) 
					 == (IData)(vlTOPp->v__DOT__EX_RtRd)) 
					& (0 != (IData)(vlTOPp->v__DOT__EX_RtRd))) 
				       & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
					   >> 7) | 
					  ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
					   >> 6))) 
				      & (IData)(vlTOPp->v__DOT__EX_RegWrite)) 
				     & ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
					>> 6)) | ((
						   ((((0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x10)) 
						      == (IData)(vlTOPp->v__DOT__EX_RtRd)) 
						     & (0 
							!= (IData)(vlTOPp->v__DOT__EX_RtRd))) 
						    & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
							>> 5) 
						       | ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
							  >> 4))) 
						   & (IData)(vlTOPp->v__DOT__EX_RegWrite)) 
						  & ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
						     >> 4))) 
				   | (((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rs_IDMEM_Match) 
				       & ((IData)(vlTOPp->v__DOT__M_MemRead) 
					  | (IData)(vlTOPp->v__DOT__M_MemWrite))) 
				      & ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
					 >> 6))) | 
				  (((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rt_IDMEM_Match) 
				    & ((IData)(vlTOPp->v__DOT__M_MemRead) 
				       | (IData)(vlTOPp->v__DOT__M_MemWrite))) 
				   & ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
				      >> 4))) | (((
						   ((IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Detect) 
						    | (IData)(vlTOPp->v__DOT__ID_Eret)) 
						   | (IData)(vlTOPp->v__DOT__ID_Mtc0)) 
						  & (IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Mask)) 
						 & (~ 
						    ((IData)(vlTOPp->v__DOT__EX_EXC_Ov) 
						     | (IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect))))) 
				| (IData)(vlTOPp->v__DOT__EX_Stall));
}

void VAdd::_settle__TOP__17(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_settle__TOP__17\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__ALU__DOT__HILO_Commit = (1 & (~ 
						  ((IData)(vlTOPp->v__DOT__EX_Stall) 
						   | (IData)(vlTOPp->v__DOT__EX_Exception_Flush))));
    vlTOPp->v__DOT__ID_Stall = (((((((((((0x1f & (vlTOPp->v__DOT__Instruction 
						  >> 0x15)) 
					 == (IData)(vlTOPp->v__DOT__EX_RtRd)) 
					& (0 != (IData)(vlTOPp->v__DOT__EX_RtRd))) 
				       & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
					   >> 7) | 
					  ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
					   >> 6))) 
				      & (IData)(vlTOPp->v__DOT__EX_RegWrite)) 
				     & ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
					>> 6)) | ((
						   ((((0x1f 
						       & (vlTOPp->v__DOT__Instruction 
							  >> 0x10)) 
						      == (IData)(vlTOPp->v__DOT__EX_RtRd)) 
						     & (0 
							!= (IData)(vlTOPp->v__DOT__EX_RtRd))) 
						    & (((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
							>> 5) 
						       | ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
							  >> 4))) 
						   & (IData)(vlTOPp->v__DOT__EX_RegWrite)) 
						  & ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
						     >> 4))) 
				   | (((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rs_IDMEM_Match) 
				       & ((IData)(vlTOPp->v__DOT__M_MemRead) 
					  | (IData)(vlTOPp->v__DOT__M_MemWrite))) 
				      & ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
					 >> 6))) | 
				  (((IData)(vlTOPp->v__DOT__HazardControl__DOT__Rt_IDMEM_Match) 
				    & ((IData)(vlTOPp->v__DOT__M_MemRead) 
				       | (IData)(vlTOPp->v__DOT__M_MemWrite))) 
				   & ((IData)(vlTOPp->v__DOT__HAZ_DP_Hazards) 
				      >> 4))) | (((
						   ((IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Detect) 
						    | (IData)(vlTOPp->v__DOT__ID_Eret)) 
						   | (IData)(vlTOPp->v__DOT__ID_Mtc0)) 
						  & (IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Mask)) 
						 & (~ 
						    ((IData)(vlTOPp->v__DOT__EX_EXC_Ov) 
						     | (IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect))))) 
				| (IData)(vlTOPp->v__DOT__EX_Stall));
    vlTOPp->v__DOT__CP0__DOT__M_Exception_Ready = (
						   ((~ (IData)(vlTOPp->v__DOT__ID_Stall)) 
						    & (IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect)) 
						   & (~ (IData)(vlTOPp->v__DOT__IF_Stall)));
    vlTOPp->v__DOT__CP0__DOT__EX_Exception_Ready = 
	(((~ (IData)(vlTOPp->v__DOT__ID_Stall)) & (IData)(vlTOPp->v__DOT__EX_EXC_Ov)) 
	 & (~ (IData)(vlTOPp->v__DOT__CP0__DOT__EX_Exception_Mask)));
    vlTOPp->v__DOT__CP0__DOT__ID_Exception_Ready = 
	(((~ (IData)(vlTOPp->v__DOT__ID_Stall)) & (IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Detect)) 
	 & (~ (IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Mask)));
    vlTOPp->v__DOT__CP0__DOT__IF_Exception_Ready = 
	(((~ (IData)(vlTOPp->v__DOT__ID_Stall)) & (IData)(vlTOPp->v__DOT__IF_EXC_AdIF)) 
	 & (~ (IData)(vlTOPp->v__DOT__CP0__DOT__IF_Exception_Mask)));
    vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond = ((((IData)(vlTOPp->v__DOT__CP0__DOT__Status_CU_0) 
						 | (IData)(vlTOPp->v__DOT__ID_KernelMode)) 
						& (IData)(vlTOPp->v__DOT__ID_Mtc0)) 
					       & (~ (IData)(vlTOPp->v__DOT__ID_Stall)));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Control.v:597
    vlTOPp->v__DOT__Controller__DOT__Datapath = ((IData)(vlTOPp->v__DOT__ID_Stall)
						  ? 0
						  : 
						 ((0x80000000 
						   & vlTOPp->v__DOT__Instruction)
						   ? 
						  ((0x40000000 
						    & vlTOPp->v__DOT__Instruction)
						    ? 
						   ((0x20000000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 
						    ((0x10000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 0
						      : 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0
						        : 0x10a3)))
						     : 
						    ((0x10000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 0
						      : 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0
						        : 0x10c3))))
						    : 
						   ((0x20000000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 
						    ((0x10000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0
						        : 0x1020)
						       : 0)
						      : 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0x1020
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0x1030
						        : 0x1028)))
						     : 
						    ((0x10000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0
						        : 0x1043)
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0x1053
						        : 0x104b))
						      : 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0x1043
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0x1057
						        : 0x104f)))))
						   : 
						  ((0x40000000 
						    & vlTOPp->v__DOT__Instruction)
						    ? 
						   ((0x20000000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 
						    ((0x10000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0
						        : 
						       ((0x20 
							 & vlTOPp->v__DOT__Instruction)
							 ? 
							((0x10 
							  & vlTOPp->v__DOT__Instruction)
							  ? 0
							  : 
							 ((8 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((4 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0
							     : 0x102))))
							 : 
							((0x10 
							  & vlTOPp->v__DOT__Instruction)
							  ? 0
							  : 
							 ((8 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((4 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0
							      : 0x102)
							     : 0)))))))
						      : 0)
						     : 
						    ((0x10000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 0
						      : 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0
						        : 
						       ((0 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))
							 ? 2
							 : 0)))))
						    : 
						   ((0x20000000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 0x1002
						     : 
						    ((0x10000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 0x8000
						      : 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0x6002
						        : 0x4000)
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 
						       ((0x100000 
							 & vlTOPp->v__DOT__Instruction)
							 ? 
							((0x80000 
							  & vlTOPp->v__DOT__Instruction)
							  ? 0
							  : 
							 ((0x40000 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((0x20000 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 0xa002)))
							 : 
							((0x80000 
							  & vlTOPp->v__DOT__Instruction)
							  ? 
							 ((0x40000 
							   & vlTOPp->v__DOT__Instruction)
							   ? 
							  ((0x20000 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((0x10000 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0
							     : 0x1600)
							    : 
							   ((0x10000 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0
							     : 0x1400))
							   : 
							  ((0x20000 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0x1600
							    : 0x1400))
							  : 
							 ((0x40000 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((0x20000 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 0x8000))))
						        : 
						       ((0x20 
							 & vlTOPp->v__DOT__Instruction)
							 ? 
							((0x10 
							  & vlTOPp->v__DOT__Instruction)
							  ? 
							 ((8 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((4 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0
							      : 0x600)
							     : 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0
							      : 0x400))
							    : 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x600
							     : 0x400)))
							  : 
							 ((8 
							   & vlTOPp->v__DOT__Instruction)
							   ? 
							  ((4 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x102
							     : 0))
							   : 0x102))
							 : 
							((0x10 
							  & vlTOPp->v__DOT__Instruction)
							  ? 
							 ((8 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((4 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0
							      : 0x102)
							     : 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0
							      : 0x102))))
							  : 
							 ((8 
							   & vlTOPp->v__DOT__Instruction)
							   ? 
							  ((4 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x902
							     : 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0xe002
							      : 0xc000)))
							   : 
							  ((4 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x102
							     : 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0
							      : 0x102))
							    : 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x102
							     : 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0
							      : 0x102)))))))))))));
}

void VAdd::_combo__TOP__18(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_combo__TOP__18\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__CP0__DOT__M_Exception_Ready = (
						   ((~ (IData)(vlTOPp->v__DOT__ID_Stall)) 
						    & (IData)(vlTOPp->v__DOT__CP0__DOT__M_Exception_Detect)) 
						   & (~ (IData)(vlTOPp->v__DOT__IF_Stall)));
    vlTOPp->v__DOT__CP0__DOT__EX_Exception_Ready = 
	(((~ (IData)(vlTOPp->v__DOT__ID_Stall)) & (IData)(vlTOPp->v__DOT__EX_EXC_Ov)) 
	 & (~ (IData)(vlTOPp->v__DOT__CP0__DOT__EX_Exception_Mask)));
    vlTOPp->v__DOT__CP0__DOT__ID_Exception_Ready = 
	(((~ (IData)(vlTOPp->v__DOT__ID_Stall)) & (IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Detect)) 
	 & (~ (IData)(vlTOPp->v__DOT__CP0__DOT__ID_Exception_Mask)));
    vlTOPp->v__DOT__CP0__DOT__IF_Exception_Ready = 
	(((~ (IData)(vlTOPp->v__DOT__ID_Stall)) & (IData)(vlTOPp->v__DOT__IF_EXC_AdIF)) 
	 & (~ (IData)(vlTOPp->v__DOT__CP0__DOT__IF_Exception_Mask)));
    vlTOPp->v__DOT__CP0__DOT__CP0_WriteCond = ((((IData)(vlTOPp->v__DOT__CP0__DOT__Status_CU_0) 
						 | (IData)(vlTOPp->v__DOT__ID_KernelMode)) 
						& (IData)(vlTOPp->v__DOT__ID_Mtc0)) 
					       & (~ (IData)(vlTOPp->v__DOT__ID_Stall)));
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/MIP/Control.v:597
    vlTOPp->v__DOT__Controller__DOT__Datapath = ((IData)(vlTOPp->v__DOT__ID_Stall)
						  ? 0
						  : 
						 ((0x80000000 
						   & vlTOPp->v__DOT__Instruction)
						   ? 
						  ((0x40000000 
						    & vlTOPp->v__DOT__Instruction)
						    ? 
						   ((0x20000000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 
						    ((0x10000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 0
						      : 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0
						        : 0x10a3)))
						     : 
						    ((0x10000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 0
						      : 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0
						        : 0x10c3))))
						    : 
						   ((0x20000000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 
						    ((0x10000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0
						        : 0x1020)
						       : 0)
						      : 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0x1020
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0x1030
						        : 0x1028)))
						     : 
						    ((0x10000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0
						        : 0x1043)
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0x1053
						        : 0x104b))
						      : 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0x1043
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0x1057
						        : 0x104f)))))
						   : 
						  ((0x40000000 
						    & vlTOPp->v__DOT__Instruction)
						    ? 
						   ((0x20000000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 
						    ((0x10000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0
						        : 
						       ((0x20 
							 & vlTOPp->v__DOT__Instruction)
							 ? 
							((0x10 
							  & vlTOPp->v__DOT__Instruction)
							  ? 0
							  : 
							 ((8 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((4 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0
							     : 0x102))))
							 : 
							((0x10 
							  & vlTOPp->v__DOT__Instruction)
							  ? 0
							  : 
							 ((8 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((4 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0
							      : 0x102)
							     : 0)))))))
						      : 0)
						     : 
						    ((0x10000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 0
						      : 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 0
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0
						        : 
						       ((0 
							 == 
							 (0x1f 
							  & (vlTOPp->v__DOT__Instruction 
							     >> 0x15)))
							 ? 2
							 : 0)))))
						    : 
						   ((0x20000000 
						     & vlTOPp->v__DOT__Instruction)
						     ? 0x1002
						     : 
						    ((0x10000000 
						      & vlTOPp->v__DOT__Instruction)
						      ? 0x8000
						      : 
						     ((0x8000000 
						       & vlTOPp->v__DOT__Instruction)
						       ? 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 0x6002
						        : 0x4000)
						       : 
						      ((0x4000000 
							& vlTOPp->v__DOT__Instruction)
						        ? 
						       ((0x100000 
							 & vlTOPp->v__DOT__Instruction)
							 ? 
							((0x80000 
							  & vlTOPp->v__DOT__Instruction)
							  ? 0
							  : 
							 ((0x40000 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((0x20000 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 0xa002)))
							 : 
							((0x80000 
							  & vlTOPp->v__DOT__Instruction)
							  ? 
							 ((0x40000 
							   & vlTOPp->v__DOT__Instruction)
							   ? 
							  ((0x20000 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((0x10000 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0
							     : 0x1600)
							    : 
							   ((0x10000 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0
							     : 0x1400))
							   : 
							  ((0x20000 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0x1600
							    : 0x1400))
							  : 
							 ((0x40000 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((0x20000 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 0x8000))))
						        : 
						       ((0x20 
							 & vlTOPp->v__DOT__Instruction)
							 ? 
							((0x10 
							  & vlTOPp->v__DOT__Instruction)
							  ? 
							 ((8 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((4 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0
							      : 0x600)
							     : 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0
							      : 0x400))
							    : 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x600
							     : 0x400)))
							  : 
							 ((8 
							   & vlTOPp->v__DOT__Instruction)
							   ? 
							  ((4 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x102
							     : 0))
							   : 0x102))
							 : 
							((0x10 
							  & vlTOPp->v__DOT__Instruction)
							  ? 
							 ((8 
							   & vlTOPp->v__DOT__Instruction)
							   ? 0
							   : 
							  ((4 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0
							      : 0x102)
							     : 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0
							      : 0x102))))
							  : 
							 ((8 
							   & vlTOPp->v__DOT__Instruction)
							   ? 
							  ((4 
							    & vlTOPp->v__DOT__Instruction)
							    ? 0
							    : 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x902
							     : 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0xe002
							      : 0xc000)))
							   : 
							  ((4 
							    & vlTOPp->v__DOT__Instruction)
							    ? 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x102
							     : 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0
							      : 0x102))
							    : 
							   ((2 
							     & vlTOPp->v__DOT__Instruction)
							     ? 0x102
							     : 
							    ((1 
							      & vlTOPp->v__DOT__Instruction)
							      ? 0
							      : 0x102)))))))))))));
    vlTOPp->v__DOT__ID_NextIsDelay = (1 & (((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
					    >> 0xf) 
					   | ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
					      >> 0xe)));
    vlTOPp->v__DOT__ID_PCSrc = ((2 & (IData)(vlTOPp->v__DOT__ID_PCSrc)) 
				| (1 & ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
					>> 0xe)));
    vlTOPp->v__DOT__ID_PCSrc = ((1 & (IData)(vlTOPp->v__DOT__ID_PCSrc)) 
				| (2 & (((1 & (((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						>> 0xf) 
					       & (~ 
						  ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						   >> 0xe))))
					  ? (((((((
						   ((vlTOPp->v__DOT__Instruction 
						     >> 0x1c) 
						    & (~ 
						       (vlTOPp->v__DOT__Instruction 
							>> 0x1b))) 
						   & (~ 
						      (vlTOPp->v__DOT__Instruction 
						       >> 0x1a))) 
						  & (IData)(vlTOPp->v__DOT__ID_CmpEQ)) 
						 | ((((vlTOPp->v__DOT__Instruction 
						       >> 0x1c) 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x1b)) 
						     & (vlTOPp->v__DOT__Instruction 
							>> 0x1a)) 
						    & ((~ 
							(vlTOPp->v__DOT__ID_ReadData1_End 
							 >> 0x1f)) 
						       & (0 
							  != vlTOPp->v__DOT__ID_ReadData1_End)))) 
						| ((((vlTOPp->v__DOT__Instruction 
						      >> 0x1c) 
						     & (vlTOPp->v__DOT__Instruction 
							>> 0x1b)) 
						    & (~ 
						       (vlTOPp->v__DOT__Instruction 
							>> 0x1a))) 
						   & ((vlTOPp->v__DOT__ID_ReadData1_End 
						       >> 0x1f) 
						      | (0 
							 == vlTOPp->v__DOT__ID_ReadData1_End)))) 
					       | ((((vlTOPp->v__DOT__Instruction 
						     >> 0x1c) 
						    & (~ 
						       (vlTOPp->v__DOT__Instruction 
							>> 0x1b))) 
						   & (vlTOPp->v__DOT__Instruction 
						      >> 0x1a)) 
						  & (~ (IData)(vlTOPp->v__DOT__ID_CmpEQ)))) 
					      | (((~ 
						   (vlTOPp->v__DOT__Instruction 
						    >> 0x1c)) 
						  & (vlTOPp->v__DOT__Instruction 
						     >> 0x10)) 
						 & (~ 
						    (vlTOPp->v__DOT__ID_ReadData1_End 
						     >> 0x1f)))) 
					     | (((~ 
						  (vlTOPp->v__DOT__Instruction 
						   >> 0x1c)) 
						 & (~ 
						    (vlTOPp->v__DOT__Instruction 
						     >> 0x10))) 
						& (vlTOPp->v__DOT__ID_ReadData1_End 
						   >> 0x1f)))
					  : ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
					     >> 0xf)) 
					<< 1)));
}

void VAdd::_settle__TOP__19(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_settle__TOP__19\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__ID_NextIsDelay = (1 & (((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
					    >> 0xf) 
					   | ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
					      >> 0xe)));
    vlTOPp->v__DOT__ID_PCSrc = ((2 & (IData)(vlTOPp->v__DOT__ID_PCSrc)) 
				| (1 & ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
					>> 0xe)));
    vlTOPp->v__DOT__ID_PCSrc = ((1 & (IData)(vlTOPp->v__DOT__ID_PCSrc)) 
				| (2 & (((1 & (((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						>> 0xf) 
					       & (~ 
						  ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
						   >> 0xe))))
					  ? (((((((
						   ((vlTOPp->v__DOT__Instruction 
						     >> 0x1c) 
						    & (~ 
						       (vlTOPp->v__DOT__Instruction 
							>> 0x1b))) 
						   & (~ 
						      (vlTOPp->v__DOT__Instruction 
						       >> 0x1a))) 
						  & (IData)(vlTOPp->v__DOT__ID_CmpEQ)) 
						 | ((((vlTOPp->v__DOT__Instruction 
						       >> 0x1c) 
						      & (vlTOPp->v__DOT__Instruction 
							 >> 0x1b)) 
						     & (vlTOPp->v__DOT__Instruction 
							>> 0x1a)) 
						    & ((~ 
							(vlTOPp->v__DOT__ID_ReadData1_End 
							 >> 0x1f)) 
						       & (0 
							  != vlTOPp->v__DOT__ID_ReadData1_End)))) 
						| ((((vlTOPp->v__DOT__Instruction 
						      >> 0x1c) 
						     & (vlTOPp->v__DOT__Instruction 
							>> 0x1b)) 
						    & (~ 
						       (vlTOPp->v__DOT__Instruction 
							>> 0x1a))) 
						   & ((vlTOPp->v__DOT__ID_ReadData1_End 
						       >> 0x1f) 
						      | (0 
							 == vlTOPp->v__DOT__ID_ReadData1_End)))) 
					       | ((((vlTOPp->v__DOT__Instruction 
						     >> 0x1c) 
						    & (~ 
						       (vlTOPp->v__DOT__Instruction 
							>> 0x1b))) 
						   & (vlTOPp->v__DOT__Instruction 
						      >> 0x1a)) 
						  & (~ (IData)(vlTOPp->v__DOT__ID_CmpEQ)))) 
					      | (((~ 
						   (vlTOPp->v__DOT__Instruction 
						    >> 0x1c)) 
						  & (vlTOPp->v__DOT__Instruction 
						     >> 0x10)) 
						 & (~ 
						    (vlTOPp->v__DOT__ID_ReadData1_End 
						     >> 0x1f)))) 
					     | (((~ 
						  (vlTOPp->v__DOT__Instruction 
						   >> 0x1c)) 
						 & (~ 
						    (vlTOPp->v__DOT__Instruction 
						     >> 0x10))) 
						& (vlTOPp->v__DOT__ID_ReadData1_End 
						   >> 0x1f)))
					  : ((IData)(vlTOPp->v__DOT__Controller__DOT__Datapath) 
					     >> 0xf)) 
					<< 1)));
}

void VAdd::_eval(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_eval\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (((IData)(vlTOPp->clock) & (~ (IData)(vlTOPp->__Vclklast__TOP__clock)))) {
	vlTOPp->_sequent__TOP__1(vlSymsp);
    }
    vlTOPp->_combo__TOP__3(vlSymsp);
    if (((IData)(vlTOPp->clock) & (~ (IData)(vlTOPp->__Vclklast__TOP__clock)))) {
	vlTOPp->_sequent__TOP__4(vlSymsp);
    }
    vlTOPp->_combo__TOP__6(vlSymsp);
    if (((IData)(vlTOPp->clock) & (~ (IData)(vlTOPp->__Vclklast__TOP__clock)))) {
	vlTOPp->_sequent__TOP__7(vlSymsp);
	vlTOPp->_sequent__TOP__9(vlSymsp);
	vlTOPp->_sequent__TOP__11(vlSymsp);
    }
    vlTOPp->_combo__TOP__12(vlSymsp);
    vlTOPp->_combo__TOP__14(vlSymsp);
    vlTOPp->_combo__TOP__16(vlSymsp);
    vlTOPp->_combo__TOP__18(vlSymsp);
    // Final
    vlTOPp->__Vclklast__TOP__clock = vlTOPp->clock;
}

void VAdd::_eval_initial(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_eval_initial\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_initial__TOP(vlSymsp);
}

void VAdd::final() {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::final\n"); );
    // Variables
    VAdd__Syms* __restrict vlSymsp = this->__VlSymsp;
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VAdd::_eval_settle(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_eval_settle\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_settle__TOP__2(vlSymsp);
    vlTOPp->_settle__TOP__5(vlSymsp);
    vlTOPp->_settle__TOP__8(vlSymsp);
    vlTOPp->_settle__TOP__10(vlSymsp);
    vlTOPp->_settle__TOP__13(vlSymsp);
    vlTOPp->_settle__TOP__15(vlSymsp);
    vlTOPp->_settle__TOP__17(vlSymsp);
    vlTOPp->_settle__TOP__19(vlSymsp);
}

IData VAdd::_change_request(VAdd__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAdd::_change_request\n"); );
    VAdd* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    IData __req = false;  // Logically a bool
    return __req;
}
