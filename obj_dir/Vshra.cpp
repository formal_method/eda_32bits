// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vshra.h for the primary calling header

#include "Vshra.h"             // For This
#include "Vshra__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(Vshra) {
    Vshra__Syms* __restrict vlSymsp = __VlSymsp = new Vshra__Syms(this, name());
    Vshra* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    in = VL_RAND_RESET_I(8);
    shamt = VL_RAND_RESET_I(3);
    out = VL_RAND_RESET_I(8);
}

void Vshra::__Vconfigure(Vshra__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

Vshra::~Vshra() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void Vshra::eval() {
    Vshra__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    Vshra* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate Vshra::eval\n"); );
    int __VclockLoop = 0;
    IData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void Vshra::_eval_initial_loop(Vshra__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    IData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

void Vshra::_combo__TOP__1(Vshra__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vshra::_combo__TOP__1\n"); );
    Vshra* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->out = (0xff & VL_SHIFTRS_III(8,8,3, (IData)(vlTOPp->in), (IData)(vlTOPp->shamt)));
}

void Vshra::_eval(Vshra__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vshra::_eval\n"); );
    Vshra* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
}

void Vshra::_eval_initial(Vshra__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vshra::_eval_initial\n"); );
    Vshra* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vshra::final() {
    VL_DEBUG_IF(VL_PRINTF("    Vshra::final\n"); );
    // Variables
    Vshra__Syms* __restrict vlSymsp = this->__VlSymsp;
    Vshra* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vshra::_eval_settle(Vshra__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vshra::_eval_settle\n"); );
    Vshra* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
}

IData Vshra::_change_request(Vshra__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vshra::_change_request\n"); );
    Vshra* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    IData __req = false;  // Logically a bool
    return __req;
}
