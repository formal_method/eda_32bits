// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _Vshra_H_
#define _Vshra_H_

#include "verilated.h"
class Vshra__Syms;

//----------

VL_MODULE(Vshra) {
  public:
    // CELLS
    // Public to allow access to /*verilator_public*/ items;
    // otherwise the application code can consider these internals.
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_IN8(in,7,0);
    VL_IN8(shamt,2,0);
    VL_OUT8(out,7,0);
    //char	__VpadToAlign3[1];
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    Vshra__Syms*	__VlSymsp;		// Symbol table
    
    // PARAMETERS
    // Parameters marked /*verilator public*/ for use by application code
    
    // CONSTRUCTORS
  private:
    Vshra& operator= (const Vshra&);	///< Copying not allowed
    Vshra(const Vshra&);	///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible WRT DPI scope names.
    Vshra(const char* name="TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~Vshra();
    
    // USER METHODS
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(Vshra__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(Vshra__Syms* symsp, bool first);
  private:
    static IData	_change_request(Vshra__Syms* __restrict vlSymsp);
  public:
    static void	_combo__TOP__1(Vshra__Syms* __restrict vlSymsp);
    static void	_eval(Vshra__Syms* __restrict vlSymsp);
    static void	_eval_initial(Vshra__Syms* __restrict vlSymsp);
    static void	_eval_settle(Vshra__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
