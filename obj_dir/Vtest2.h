// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _Vtest2_H_
#define _Vtest2_H_

#include "verilated.h"
class Vtest2__Syms;

//----------

VL_MODULE(Vtest2) {
  public:
    // CELLS
    // Public to allow access to /*verilator_public*/ items;
    // otherwise the application code can consider these internals.
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_IN8(clk,0,0);
    VL_IN8(rst,0,0);
    VL_OUT8(f,4,0);
    //char	__VpadToAlign3[1];
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    VL_SIG8(__Vclklast__TOP__clk,0,0);
    VL_SIG8(__Vclklast__TOP__rst,0,0);
    //char	__VpadToAlign14[2];
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    //char	__VpadToAlign20[4];
    Vtest2__Syms*	__VlSymsp;		// Symbol table
    
    // PARAMETERS
    // Parameters marked /*verilator public*/ for use by application code
    
    // CONSTRUCTORS
  private:
    Vtest2& operator= (const Vtest2&);	///< Copying not allowed
    Vtest2(const Vtest2&);	///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible WRT DPI scope names.
    Vtest2(const char* name="TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~Vtest2();
    
    // USER METHODS
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(Vtest2__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(Vtest2__Syms* symsp, bool first);
  private:
    static IData	_change_request(Vtest2__Syms* __restrict vlSymsp);
  public:
    static void	_eval(Vtest2__Syms* __restrict vlSymsp);
    static void	_eval_initial(Vtest2__Syms* __restrict vlSymsp);
    static void	_eval_settle(Vtest2__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__1(Vtest2__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
