// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtest2.h for the primary calling header

#include "Vtest2.h"            // For This
#include "Vtest2__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(Vtest2) {
    Vtest2__Syms* __restrict vlSymsp = __VlSymsp = new Vtest2__Syms(this, name());
    Vtest2* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    clk = 0;
    rst = 0;
    f = VL_RAND_RESET_I(5);
    __Vclklast__TOP__clk = 0;
    __Vclklast__TOP__rst = 0;
}

void Vtest2::__Vconfigure(Vtest2__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

Vtest2::~Vtest2() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void Vtest2::eval() {
    Vtest2__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    Vtest2* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate Vtest2::eval\n"); );
    int __VclockLoop = 0;
    IData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void Vtest2::_eval_initial_loop(Vtest2__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    IData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

void Vtest2::_sequent__TOP__1(Vtest2__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vtest2::_sequent__TOP__1\n"); );
    Vtest2* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    VL_SIG8(__Vdly__f,4,0);
    //char	__VpadToAlign5[3];
    // Body
    __Vdly__f = vlTOPp->f;
    // ALWAYS at /home/sonlam/workspace/EDA_MIP_normal/Test/Design/test2.v:5
    __Vdly__f = (0x1f & ((IData)(vlTOPp->rst) ? ((IData)(1) 
						 + (IData)(vlTOPp->f))
			  : 0));
    vlTOPp->f = __Vdly__f;
}

void Vtest2::_eval(Vtest2__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vtest2::_eval\n"); );
    Vtest2* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if ((((IData)(vlTOPp->clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__clk))) 
	 | ((~ (IData)(vlTOPp->rst)) & (IData)(vlTOPp->__Vclklast__TOP__rst)))) {
	vlTOPp->_sequent__TOP__1(vlSymsp);
    }
    // Final
    vlTOPp->__Vclklast__TOP__clk = vlTOPp->clk;
    vlTOPp->__Vclklast__TOP__rst = vlTOPp->rst;
}

void Vtest2::_eval_initial(Vtest2__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vtest2::_eval_initial\n"); );
    Vtest2* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vtest2::final() {
    VL_DEBUG_IF(VL_PRINTF("    Vtest2::final\n"); );
    // Variables
    Vtest2__Syms* __restrict vlSymsp = this->__VlSymsp;
    Vtest2* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vtest2::_eval_settle(Vtest2__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vtest2::_eval_settle\n"); );
    Vtest2* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

IData Vtest2::_change_request(Vtest2__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vtest2::_change_request\n"); );
    Vtest2* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    IData __req = false;  // Logically a bool
    return __req;
}
