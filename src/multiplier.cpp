/*! ************************************************************************
    \file multiplier.cpp
    \brief Implementation of the Class Multiplier

    The class contained in this file implements the methods of the Function
 * Multiplier
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Tue Feb 14 2006

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout
**************************************************************************/

#include "multiplier.h"

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * A Multiplier has two fanin Components so initialize mRequiredFanin with 2. 
 * The order of the fanin components doesn't matter for the Multiplier
 * Function. Add the fanin components to the fanin of this Multiplier.
 * @param fanin A vector containing the two Fanin Components of a Multiplier
 * @param fanout The string naming the output
 * @param id The Id of this Multiplier
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 * @exception InputSizeException is thrown if the fanins do not have the same 
 * size -> extend to achieve this
 */
Multiplier::Multiplier(std::vector<Component*>& fanin, std::string fanout, 
                        int id, Module* module)
{
    mModule=module;
    SetMRequiredFanin(2);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMType(MULT);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+=" not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }

	// C.Villarraga 18.8.2011 correction of the formula
    mFanoutWidth = fanin[0]->GetMFanoutWidth()+fanin[1]->GetMFanoutWidth();

    //we have two fanin components so lets add these to our new Multiplier
    //Function. First of all we allocate space for the Vector mFanin with 
    //undefined fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //Therefore we use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to position i. 
    //In this implementation we require all fanins and the result to have the
    //same width.
    unsigned width = fanin[0]->GetMFanoutWidth();
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that the fanin has the correct size
        if (width!=fanin[i]->GetMFanoutWidth())
        {
            std::string message="EXCEPTION: The width of the fanins is not";
            message +=" equivalent."; 
            throw InputSizeException(message, this, fanin);
        }   
        //check that all fanin components are added correctly
        AddFanin(fanin[i],i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mcRequiredFanin;i++)
} //End Multiplier(vector<Component> fanin)

/**
 * Methods
 */

/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin.
 * @exception DefaultException is thrown if the result can not be determined. 
 * It should be impossible that this exception occurs
 */
void Multiplier::SimulateForward () 
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
} // End SimulateForward ()

/**
 * Perform a simulation of this Multiplier for a given vector of inputs
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void Multiplier::Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
{
    //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for
    //We reset mOutput
    outVals.clear();
    //We need a variable for the carry
    satlib::SatI::lValue carry = satlib::SatI::FALSE;
    //We want both inputs to have the same size /todo this mustn't be the case
    try
    {
        if (inpVals[0].size()!=inpVals[1].size())
        {
            //The two inputs do not have the same width so we use a sign 
            //extension in order to be able to multiply the two inputs. Throw 
            //an Exception to inform the user. 
            std::string message = "EXCEPTION: The inputs of this multiplier "; 
            message+="function do not have the same width. I will use a sign ";
            message+="extension.";
            throw InputSizeException(message, this, mFanin);
        } // End if (mInputs[0].size()!=mInputs[1].size())
    } // End try
    //We handle the input size exception directly by sign extending the 
    //smaller input.
    catch(InputSizeException e)
    {
        //print the message to let the user know that we use a sign extended
        //input
        e.GetMessage();
        //Determine the smaller input; store in the variables smaller and 
        //bigger
        int smaller, bigger;
        if (inpVals[0].size()<inpVals[1].size())
        {
            smaller=0;
            bigger=1;
        }
        else
        {
            smaller=1;
            bigger=0;
        }
        //Determine the highest bit of smaller for the sign extension
        satlib::SatI::lValue sign=inpVals[smaller][inpVals[smaller].size()-1];
        //As long as smaller is smaller than bigger push sign to smaller
        while (inpVals[smaller].size()!=inpVals[bigger].size())
            inpVals[smaller].push_back(sign);
    } // End catch(InputSizeException e)
    
    // C.Villarraga 03.08.2011: replaced the method of calculation. In the
    // old implementation the output size was the same as the input size.

    //Muliply each bit of inpVals[1] with inpVals[2] and add the results after
    //shifting the result of the last add is stored in outVals
    //outVals is initialized with the width of mInputs and a false value for
    //each bit
    for(unsigned int i=0; i<inpVals[0].size(); i++)
    	outVals.push_back(satlib::SatI::FALSE);
    std::vector<satlib::SatI::lValue> partProd;
    for (unsigned int i=0; i<inpVals[1].size(); i++)
    {   
    	partProd.clear();
    	if (inpVals[1][i]==satlib::SatI::FALSE)
    	{
    		for (unsigned int j=0; j<inpVals[0].size()+i; j++)
    		{
    			partProd.push_back(satlib::SatI::FALSE);
    		}
    	}
    	else if (inpVals[1][i]==satlib::SatI::TRUE)
    	{
     		for (unsigned int j=0; j<i; j++)
			{
    			partProd.push_back(satlib::SatI::FALSE);
			}
    		for (unsigned int j=0; j<inpVals[0].size(); j++)
    		{
    			partProd.push_back(inpVals[0][j]);
    		}
    	}
    	else
    	{
    		for (unsigned int j=0; j<i; j++)
			{
    			partProd.push_back(satlib::SatI::FALSE);
			}
    		for (unsigned int j=0; j<inpVals[0].size(); j++)
    		{
    			if (inpVals[0][j]==satlib::SatI::FALSE)
    				partProd.push_back(satlib::SatI::FALSE);
    			else
    				partProd.push_back(satlib::SatI::DONTCARE);
    		}
    	}
    	// ADDITION OF the partial product and the output
        outVals.push_back(satlib::SatI::FALSE);
        partProd.push_back(satlib::SatI::FALSE);
        carry=satlib::SatI::FALSE;
        for(unsigned int j = 0; j < outVals.size(); j++)
        {
			int sum=(int)outVals[j]+(int)partProd[j]+(int)carry;
			switch (sum)
        	{
            	case 1: outVals[j]=satlib::SatI::TRUE;
                    carry=satlib::SatI::FALSE;
                    break;
	            case 2: outVals[j]=satlib::SatI::FALSE;
                    carry=satlib::SatI::TRUE;
                    break;
    	        case 0: outVals[j]=satlib::SatI::FALSE;
                    carry=satlib::SatI::FALSE;
                    break;
        	    case 3: outVals[j]=satlib::SatI::TRUE;
                    carry=satlib::SatI::TRUE;
                    break;
            	case 5: outVals[j]=satlib::SatI::DONTCARE;
                    carry=satlib::SatI::FALSE;
                    break;
	            case 7: outVals[j]= satlib::SatI::DONTCARE;
                    carry=satlib::SatI::TRUE;
                    break;
	            default:
				{
					outVals[j]=satlib::SatI::DONTCARE;
					carry=satlib::SatI::DONTCARE;
				}
    	        	break;
	        }
	    }
    } // End for (unsigned int i=0; i<mInputs[1].size();i++)
    
    /*
    // old version
    outVals.resize(inpVals[1].size(), satlib::SatI::FALSE);
    for (unsigned int i=0; i<inpVals[1].size();i++)
    {
        printf("intermediate result\n");
        for(unsigned int j=0; j<outVals.size();j++)
            printf("%d\n",outVals[j]);
        //we need a variable to store the partial products
        std::vector<satlib::SatI::lValue> partProd;
        //The result of the partial product has to be shifted therefore push
        //the needed amount bits with the value l_False
        partProd.resize(i, satlib::SatI::FALSE);
        for (unsigned int j=0;j<inpVals[0].size()-i;j++)
        {
            if(inpVals[0][j]==satlib::SatI::FALSE 
               || inpVals[1][i]==satlib::SatI::FALSE)
            {
                partProd.push_back(satlib::SatI::FALSE);
            }
            else if(inpVals[0][j]==satlib::SatI::DONTCARE 
                    || inpVals[1][i]==satlib::SatI::DONTCARE)
            {
                partProd.push_back(satlib::SatI::DONTCARE);
            }
            else partProd.push_back(satlib::SatI::TRUE);
        } // End for (unsigned int j=0;j<inpVals[0].size()-i;j++)
        printf("partProd\n");
        for(unsigned int j=0; j<partProd.size();j++)
            printf("%d\n",partProd[j]);
        //add the partial product to outVals
        //We need a variable for the carry
        carry=satlib::SatI::FALSE;
        for (unsigned int j=0;j<outVals.size();j++)
        {
            //First we add up the two integer values of input bits and the carry.
            int sum=outVals[j]+partProd[j]+carry;
            //There are 7 possibilities for the sum. Setting up a truth table we 
            //can see, that this sum will evaluate to
            switch (sum)
            {
                //1 in 3 out of 27 cases. Resulting in the output TRUE and the 
                //carry FALSE 
                case 1: outVals[j]=satlib::SatI::TRUE;
                        carry=satlib::SatI::FALSE;
                        break;
                //2 in 3 out of 27 cases resulting in the output FALSE and
                //the carry TRUE. 
                case 2: outVals[j]=satlib::SatI::FALSE;
                        carry=satlib::SatI::TRUE;
                        break;
                //0 in 1 out of 27 cases resulting in the output FALSE and the 
                //carry FALSE
                case 0: outVals[j]=satlib::SatI::FALSE;
                        carry=satlib::SatI::FALSE;
                        break;
                //3 in 1 out of 27 cases. Resulting in the output TRUE and the 
                //carry TRUE.
                case 3: outVals[j]=satlib::SatI::TRUE;
                        carry=satlib::SatI::TRUE;
                        break;
                //5 in 3 out of 27 cases. Resulting in the output DONTCARE and the 
                //carry FALSE.
                case 5: outVals[j]=satlib::SatI::DONTCARE;
                        carry=satlib::SatI::FALSE;
                        break;
                //7 in 3 out of 27 cases. Resulting in the output DONTCARE and the 
                //carry TRUE.
                case 7: outVals[j]=satlib::SatI::DONTCARE;
                        carry=satlib::SatI::TRUE;
                        break;
                //In the remaining 13 cases output and carry are DONTCARE
                default: 
                {
                    outVals[j]=satlib::SatI::DONTCARE;
                    carry=satlib::SatI::DONTCARE;
                }
            } // End of switch(sum)
        } //End for (int j=0;i<mOutputs.size();j++)
    }// End for (unsigned int i=0; i<mInputs[1].size();i++)
    */
}//End Simulate

/**
 * Create the CNF for this Multiplier.
 * @param solver the interface to the solver
 */
void Multiplier::CreateCnf (satlib::SatI& satSolver)
{
    //vector for the result literals
    std::vector<satlib::RtpLit> r;
    //vector for the literals of input a
    std::vector<satlib::RtpLit> a;
    //vector for the literasl of vector b
    std::vector<satlib::RtpLit> b;
    assert(GetFanin().size()==2);
    //Get the literals of the result
    GetFanoutRtpLits(r);
    std::vector< std::vector<satlib::RtpLit> >addends(r.size());
    //Get the literals of the inputs
    GetFanin()[0]->GetFanoutRtpLits(a);
    GetFanin()[1]->GetFanoutRtpLits(b);
    assert(a.size()>0&&b.size()>0&&r.size()>0);
    satSolver.makeAnd(a[0],b[0],r[0]);
    int extraBit=GetMFanoutWidth()+1;
  //create Partial Products
    for(unsigned i=0;i<a.size();i++){
        for(unsigned k=0;k<b.size();k++){
            if((i+k)>0&&(i+k<addends.size())){
                satlib::RtpLit pp(mId,extraBit++);
                satSolver.makeAnd(a[i],b[k],pp);
                addends[i+k].push_back(pp);
            }
        }
    }
    //create Addition network;
    for(unsigned k=1;k<r.size();k++){
    //add up column k
        while(addends[k].size()>2){
   //instantiate Fulladder
            satlib::RtpLit sum(mId,extraBit++);
            if(addends[k].size()==3){
                sum=r[k];
                extraBit--;
            }
            satlib::RtpLit cry(mId,extraBit++);
            unsigned last=addends[k].size()-1;
            satSolver.makeFA(addends[k][last],addends[k][last-1],addends[k][last-2],sum,cry);
            addends[k].pop_back();
            addends[k].pop_back();
            addends[k][last-2]=sum;
            if(k+1<addends.size())
                addends[k+1].push_back(cry);
        }
        if(addends[k].size()==2){
      //final Halfadder if necessary
            satlib::RtpLit sum=r[k];    
            satlib::RtpLit cry(mId,extraBit++);
            unsigned last=addends[k].size()-1;
            satSolver.makeHA(addends[k][last],addends[k][last-1],sum,cry);
            if(k+1<addends.size())
                addends[k+1].push_back(cry);
        }
    }
}

void Multiplier::Create_BtorComp(Btor* btor)
{
	BtorNode *fanin1_uext, *fanin2_uext;
	fanin1_uext = boolector_uext(btor, mFanin[0]->GetBtorComp(), mFanoutWidth-mFanin[0]->GetMFanoutWidth());
	fanin2_uext = boolector_uext(btor, mFanin[1]->GetBtorComp(), mFanoutWidth-mFanin[1]->GetMFanoutWidth());
	BtorNode* comp = boolector_mul(btor, fanin1_uext, fanin2_uext);
	BtorComp = comp;
	uext1 = fanin1_uext;
	uext2 = fanin2_uext;
}
