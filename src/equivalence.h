/*! ************************************************************************
    \file equivalence.h
    \brief Header file of the Class Equivalence.

    The class contained in this file defines the attributes and methods of the
 * Function Equivalence.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Thu Feb 09 2006

Modification history:
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef EQUIVALENCE_H
#define EQUIVALENCE_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"
#include "../exceptions/defaultexception.h"

/**
 * Class to describe the behaviour of the Equivalence Function.
 * This class implements the Function Equvalence derived from the class
 * Function.
 */
class Equivalence : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The Equivalence Function needs 2 Fanins that can be handled equally.
     */
    Equivalence(){SetMRequiredFanin(2);SetMType(EQ);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * An Equivalence has two fanin Components so initialize mRequiredFanin with 
     * 2. The order of the fanin components doesn't matter for the Equivalence
     * Function. Add the fanin components to the fanin of this Equivalence.
     * @param fanin A vector containing the two Fanin Components of an Add
     * @param fanout The string naming the output
     * @param id The id of this Equivalence
     * @param module The Module the Equivalence shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     * @exception InputSizeException is thrown if the fanins do not have the same 
     * size -> extend to achieve this
     */
    Equivalence(std::vector<Component*>& fanin, std::string fanout, int id, 
                Module* module);
    
    /**
     * Destructor
     */
    virtual ~Equivalence(){}
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this Equivalence as a string (EQ)
     * @return The string "EQ"
     */
    inline std::string GetType(){return "EQ";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();

    /**
     * Perform a simulation of this Equivalence for a given vector of inputs.
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );
    
    /**
     * Create the CNf for this Equivalence.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor);
    
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */

    /**
     * Operations
     */
};
#endif //EQUIVALENCE_H

