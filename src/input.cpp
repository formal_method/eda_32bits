/*! ************************************************************************
    \file input.cpp 
    \brief Implementation of class Input
    
    Implementation of the methods of the class Input
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Tue Feb 07 2006 

Modification history:
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 Moved implementation of SetInput here, as this was changed to 
 * check whether the width of the input and the data is the same. The 
 * constructor is now accepting a vector name to get the name of the input and 
 * the width of the input. The width is set by calling SetWidth.
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout

**************************************************************************/

#include "input.h"
#include <cstring>
#include <string>

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * An Input has one fanin Component so initialize mRequiredFanin with 1.
 * Add the fanin Component to the fanin of this Input.
 * @param name A vector containing the Fanin Component of the Input
 * @param fanout The string naming the output
 * @param id The ID of the Input
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 */
Input::Input(std::vector<FckId> bitWidth, std::string name, int id, 
            Module& module)
{
    //The input doesn't have a fanin component
    SetMRequiredFanin(0);
    SetMType(INPUT);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    //the input name is stored in name[0]
    SetMInputName(name);
    mId=id;
    BtorComp = NULL;
    //The width has to extracted from the string before it can be set.
    SetWidth(bitWidth[0]);
    //Set the name of the output
    SetMOutputName(name);  //Edited by Son Lam 21/10
} //End Input(vector<Component> fanin)
/**
 * Methods
 */
/**
 * Add a Component to the fanout of this Input.
 * @param cmp a reference to the Component that shall be added.
 */
void Input::AddFanout(Component* cmp) 
{
   //In the fanout we don't care about the order of fanout components so we
   //just push the Component
   mFanout.push_back(cmp); 
} // End AddFanout(Component& cmp) 


/**
 * Check whether the Input is complete.
 * @return True if the Input has at least one fanout Component.
 */
bool Input::IsComplete () 
{
    //We check for the output
    if (mFanout.size()>0);
    //not complete
    else 
    {
        std::cout<<"WARNING: "<<GetType()<<" "<<GetMOutputName()
                <<"does not have a fanout component"<<std::endl;
    }
    return true;
}

/**
 * Set the values of the input
 * @param input a vector of lValue containing the new input bits
 */
void Input::SetInput(std::vector<satlib::SatI::lValue>& input)
{
    if(input.size()!=mFaninWidth[0])
    {
        std::string message="EXCEPTION: Wrong number of input bits";
        std::vector<std::vector<satlib::SatI::lValue>* > inputs;
        inputs.push_back(&input);
        throw InputSizeException(message, this, inputs);
    }
    mOutput=input;    
}


void Input::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_var(btor, mFanoutWidth, NULL);
	BtorComp = comp;
}
