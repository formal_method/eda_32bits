/*! ************************************************************************
    \file constinput.h 
    \brief Header file of the ConstInput class
    
    Defines all attributes and methods a ConstInput has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 15 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 10/9/2014 them ham lay gia tri o dang thap phan cua hang so
**************************************************************************/

#ifndef CONSTINPUT_H
#define CONSTINPUT_H
#include <string>
#include "component.h"
#include <vector>
#include "undef.h"
#include "input.h"


/**
 * Class ConstInput represents constant Inputs.
 * Defines the attributes and methods constant Inputs have.
 */
class ConstInput : public Input {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor
     * A ConstInput needs 0 Fanins
     */
    ConstInput(){SetMRequiredFanin(0);SetMType(C_INPUT);;SetMOutputName("");}
    
    /**
     * A constructor with the constant value.
     * A ConstInput has no fanin Component so initialize mRequiredFanin with 0.
     * Set the value of the input to the constant value passed.
     * @param constValue taking the constant value for the constant Input. The
     * input is expected to contain a string combined out of lValue values.
     * @param fanout The name of the output of the constant input
     * @param id The ID of the constant input
     * @param module The Module the ConstInput shall be added to.
     */
    ConstInput(std::string constValue, int id, 
                Module* module);
    
    /**
     * Destructor
     */
    virtual ~ConstInput(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this component as a string (C_INPUT)
     * @return The string "C_INPUT"
     */
    inline std::string GetType(){return "C_INPUT";}
    /**
     * Set the values of the input
     * @param input a vector of lValues containing the new input bits
     */
    inline void SetInput(std::vector< satlib::SatI::lValue>& input){}
    
    /**
     * Perform a simulation of this ConstantInput
     * @param inpVals The input values the simulation shall be performed for 
     * (not really needed as the input is constant)
     * @param outVals The resulting output of the simulation
     */
    inline void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals ){}
    
    /**
     * Create the CNF for this ConstantInput.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);
    int get_decvalue();

	void Create_BtorComp(Btor* btor);
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * value in decimal
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Set the values of the ConsInput
     * @param input a vector of lbool containing the input bits
     */
    inline void SetConstInput(std::vector< satlib::SatI::lValue>& input)
    {mOutput=input;}
    
    /**
     * Operations
     */
};
#endif //INPUT_H

