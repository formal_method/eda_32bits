// -*- mode: C++; c-file-style: "cc-mode" -*-
//*************************************************************************
// DESCRIPTION: Verilator: make dot file
//
//
//*************************************************************************
#ifndef _V3MAKENODE_H_
#define _V3MAKENODE_H_
#include "config_build.h"
#include "verilatedos.h"
#include "V3Error.h"
#include "V3Ast.h"
#include "node.h"


//============================================================================
using namespace std;


class V3MakeNode {
public:
static void makeNode(AstNetlist* nodep);
};

// print node information
void makeTree(AstNode*);

// make DOT graph file
void makeDotFile(Node*);

#endif // Guard

