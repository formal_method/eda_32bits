/*! ************************************************************************
    \file moduleinput.h 
    \brief Header file of the ModuleInterface class
    
    Defines all attributes and methods a ModuleInput has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed June 21 2006 

Modification history:
**************************************************************************/

#ifndef MODULEINPUT_H
#define MODULEINPUT_H
#include <string>
#include <vector>
#include "../exceptions/doubledfaninexception.h"
#include "../exceptions/inputsizeexception.h"
#include "undef.h"
#include "circuitbuilder.h"

class Module;

/**
 * Class ModuleInput for the representation of hierachy.
 * The RSM file can have a hierarchy. To get this hierarchy into our circuit 
 * we need an interface plugging in a submodule into the topmodule. Hereby we 
 * preserve the hierachy until we do a timeframeexpansion. Thus, this 
 * Component does not exist in a TimeFrameExpansion. 
 */
class ModuleInput : public Component
{
    /**
     * Public stuff
     */
    public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor
     * An ModuleInput does not have a default number of fanin components. 
     * The number of fanin and fanout components is determined by the Module 
     * plugged into this Component.
     */
    ModuleInput(){SetMRequiredFanin(0);SetMType(M_INPUT);
        SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * The number of fanin Components a ModuleInput has depends on the 
     * Module plugged into it. So get the number of fanin Components from the 
     * Module.
     * Add the fanin Components to the fanin of this ModuleInterface.
     * @param fanin A vector containing the fanin Component of 
     * the ModuleInput
     * @param moduleName The string naming the Module
     * @param output the name of the output
     * @param id The ID of the ModuleInterface
     * @param module The module the ModuleInput shall be added to.
     */
    ModuleInput(std::vector<Component*>& fanin, ModuleId moduleId, 
                std::string output, int id, Module* module);
    
    /**
     * Destructor
     */
    virtual ~ModuleInput(){}
    /**
     * Accessor Methods
     */
    /**
     * Add a component to the fanout of this Function.
     * The number of fanoout Components a ModuleInput has depends on the 
     * Module plugged in. The fanout Components are not yet known at the time 
     * the Function is constructed. So it has to be possible to add fanout 
     * Components later on.
     * @param cmp a reference to the Component that shall be added.
     */
    void AddFanout (Component* cmp){mFanout.push_back(cmp);}

    /**
     * Add a Component to the fanout of this Function.
     * @param cmp a reference to the Component that shall be added.
     */
    void ClearFanout ()
    {
        mFanout.clear();
    }


    /**
     * Returns the value of the Input bits of a specified input.
     * @param input the position in mInputs that shall be returned 
     * @return The value of the input bits at the position specified by input.
     */
    inline std::vector< satlib::SatI::lValue> GetInput(int input){return mInputs[input];}
    
    /**
     * Return the values of the bits in mOutput.
     * @return A vector with the current value of mOutput.
     */
    inline std::vector<satlib::SatI::lValue> GetOutput (){return mOutput;}
    
    /**
     * Add a Component to the fanin of this ModuleInput
     * @param cmp Reference to the Component that shall be added to the fanin.
     * @param pos The position at which this Component shall be added.
     * @exception DoubledFaninException If the given input is not needed or 
     * already there
     * 
     * Increments the value of mKnownFanin.
     */
    void AddFanin (Component* cmp,unsigned int pos);
        
    /**
     * Change the Component in the fanin of this Component
     * @param cmpold Reference to the Component that shall be removed
     * @param cmpnew Reference to the Component that shall be added
     * @exception GeneralFaninException thrown if cmpold is not found in the
     * fanin of this component
     */
    void ChangeFanin(Component* cmpold, Component* cmpnew){}
    
    /**
     * Return the Components in the Fanin.
     * We would like to know which Components are in the Fanin of this 
     * ModuleInterface.
     * @return This method here returns a vector containing the Components in 
     * the fanin..
     */
    inline std::vector<Component*> GetFanin () const {return mFanin;}
    
    /**
     * Return the Components in the fanout of this Component.
     * @return The Components in the fanout of this ModuleOutput
     */
    std::vector<Component*> GetFanout();
    
    /**
     * Return the type of this Component as a string (M_INTERFACE)
     * @return The string "M_INPUT"
     */
    inline std::string GetType(){return "M_INPUT";}
    
    /**
     * Get the number of Outputs the included Module has
     * @return The number of Outputs of the included Module
     */
    inline int GetMOutputs(){return mOutputs;}
    
    /**
     * Get the width of the output
     * @return The width of the outpus
     */
    unsigned int GetMFanoutWidth(){return mFanoutWidth;}
    
    /**
     * Operations
     */
    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * As a ModuleInput has Fanin Components it also has to be able to 
     * fetch the outputs of these fanins and update its outputs accordingly.
     */
    inline void SimulateForward (){UpdateInputs();}
    
    /**
     * perform a simulation of this ModuleInput for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals ){}
        
    /**
     * Update the value of mInput
     * Fetch the current assignment of the outputs of the fanin Components and
     * update mInput of this Component accordingly.
     */
    void UpdateInputs();
    
    /**
     * Check whether the Input is complete.
     * @return True if the Input has mRequiredFanin fanin Components and the
     * required number of fanout Components.
     */
    bool IsComplete ();
    
    /**
     * Create the CNF for this component.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver){}
    
    /**
     * Get the names of the Inputs to the Module
     * @return A vector containing the inputs
     */
    std::vector<Component*> GetModuleInputs();
    
    /**
     * Get the Module included in this ModuleInput
     * @return the included Module
     */
    Module* GetInclModule(){return mInclModule;}

	void Create_BtorComp(Btor* btor){};
    
    /**
     * Protected stuff
     */
    protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
    /**
     * Private stuff
     */
    private:
    /**
     * Fields
     */
    /**
     * Reference to the included Module
     */
        Module* mInclModule;
     /**
      * Reference to the components in the fanout.
      */
        std::vector<Component*> mFanout;
     /**
      * The current value at the output.
      */
        std::vector<satlib::SatI::lValue> mOutput;
     /**
      * The value at the input
      */
        std::vector<std::vector<satlib::SatI::lValue> > mInputs;
     /**
      * Reference to the components in the Fanin
      */
        std::vector<Component*> mFanin;
     /**
      * Number of outputs 
      */
        int mOutputs;
     /**
      * The names of the outputs
      */
        std::vector<std::string> mOutputNames;
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    
    /**
     * Operations
     */
};
#endif //MODULEINPUT_H
