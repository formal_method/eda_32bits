/*! ************************************************************************
    \file add.cpp
    \brief Implementation of the Class Add

    The class contained in this file implements the methods of the Function
 * Add
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Tue Jan 31 - Wed Feb 08 2006

Modification history:
 * 14.02.06 corrected output for sum=-3 from l_True to l_False
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 15.-16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout - added CreateCnf

**************************************************************************/

#include "add.h"

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * An Add has two fanin Components so initialize mRequiredFanin with 2. The
 * order of the fanin components doesn't matter for the Add Function.
 * Add the fanin components to the fanin of this Add.
 * This Add requires that all fanins and the fanout have the same width. 
 * @param fanin A vector containing the two Fanin Components of an Add
 * @param fanout The string naming the output
 * @param id The ID for this Add
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 * @exception InputSizeException is thrown if the fanins do not have the same 
 * size -> extend to achieve this
 */
Add::Add(std::vector<Component*>& fanin, std::string fanout, int id, 
            Module* module)
{
    mModule=module;
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMRequiredFanin(2);
    SetMType(ADD);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins.";
        throw GeneralFanException(message, this, fanin);
    }
    //we have two fanin components so lets add these to our new Add Function.
    //First of all we allocate space for the Vector mFanin with undefined
    //fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //Therefore we use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to position i. 
    //In this implementation we require all addends and the result to have the
    //same width. Therefore we set the width of the output here
    mFanoutWidth=fanin[0]->GetMFanoutWidth();
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that the fanin has the correct size
        if (mFanoutWidth!=fanin[i]->GetMFanoutWidth())
        {
            std::string message="EXCEPTION: The width of the fanins is not";
            message +=" equivalent."; 
            throw InputSizeException(message, this, fanin);
        }    
        //add the fanin component
        AddFanin(fanin[i],i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mcRequiredFanin;i++)
} //End Add(vector<Component> fanin)
/**
 * Methods
 */


/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin. The simulation itself is performed by Simulate.
 */
void Add::SimulateForward ()
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
} //End SimulateForward

/**
 * perform a simulation of this Add for a given vector of inputs
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 * @exception InputSizeException Thrown if the passed inputs do not satisfy
 * the needs of this add component (number of inputs or number of input bits)
 */
void Add::Simulate
        (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )    
{
    //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for
        
    //We reset mOutput
    outVals.clear();
    //We need a variable for the carry
    satlib::SatI::lValue carry=satlib::SatI::FALSE;
    //We want both inputs to have the same size as mFaninWidth 
    try
    {
        if (inpVals[0].size()!=inpVals[1].size()
                ||inpVals[0].size()!=mFaninWidth[0])
        {
            //The two inputs do not have the same width so we use a sign 
            //extension in order to be able to add the to inputs. Throw an 
            //Exception to inform the user. 
            std::string message = "EXCEPTION: The inputs of this add function "; 
            message+="do not have the same width. I will use a sign extension.";
            throw InputSizeException(message, this, mFanin);
        } // End if (mInputs[0].size()!=mInputs[1].size())
    } // End try
    //We handle the input size exception directly by sign extending the 
    //smaller input.
    catch(InputSizeException e)
    {
        //print the message to let the user know that we use a sign extended
        //input
        e.GetMessage();
        //Determine the smaller input; store in the variables smaller and 
        //bigger
        int smaller, bigger;
        if (inpVals[0].size()<inpVals[1].size())
        {
            smaller=0;
            bigger=1;
        }
        else
        {
            smaller=1;
            bigger=0;
        }
        //Determine the highest bit of smaller for the sign extension
        satlib::SatI::lValue sign=inpVals[smaller][inpVals[smaller].size()-1];
        //As long as smaller is smaller than bigger push sign to smaller
        while (inpVals[smaller].size()!=inpVals[bigger].size())
            inpVals[smaller].push_back(sign);
    } // End catch(InputSizeException e)
    //We consider each input bit separately from the lowest bits to the 
    //highest one.
    for (unsigned int i=0;i<inpVals[0].size();i++)
    {
        //First we add up the two integer values of input bits and the carry.
        int sum=(int)inpVals[0][i]+(int)inpVals[1][i]+(int)carry;
        //There are 7 possibilities for the sum. Setting up a truth table we 
        //can see, that this sum will evaluiate to
        switch (sum)
        {
            //1 in 3 out of 27 cases. Resulting in the output TRUE and the 
            //carry FALSE 
            case 1: outVals.push_back(satlib::SatI::TRUE);
                    carry=satlib::SatI::FALSE;
                    break;
            //2 in 3 out of 27 cases resulting in the output FALSE and
            //the carry TRUE. 
            case 2: outVals.push_back(satlib::SatI::FALSE);
                    carry=satlib::SatI::TRUE;
                    break;
            //0 in 1 out of 27 cases resulting in the output FALSE and the 
            //carry FALSE
            case 0: outVals.push_back(satlib::SatI::FALSE);
                    carry=satlib::SatI::FALSE;
                    break; // C.Villarraga 17.03.2011: Break missing
            //3 in 1 out of 27 cases. Resulting in the output TRUE and the 
            //carry TRUE.
            case 3: outVals.push_back(satlib::SatI::TRUE);
                    carry=satlib::SatI::TRUE;
                    break;
            //5 in 3 out of 27 cases. Resulting in the output DONTCARE and the 
            //carry FALSE.
            case 5: outVals.push_back(satlib::SatI::DONTCARE);
                    carry=satlib::SatI::FALSE;
                    break;
            //7 in 3 out of 27 cases. Resulting in the output DONTCARE and the 
            //carry TRUE.
            case 7: outVals.push_back(satlib::SatI::DONTCARE);
                    carry=satlib::SatI::TRUE;
                    break;
            //In the remaining 13 cases output and carry are DONTCARE
            default: 
            {
                outVals.push_back(satlib::SatI::DONTCARE);
                carry=satlib::SatI::DONTCARE;
            }
            	break;
        } // End of switch(sum)
    } //End for (int i=0;i<mInputs[0].size();i++)
} // End Add::Simulate(inpVals, outVals)

/**
 * create the CNF for this Add
 * @param solver the interface to the solver
 */
void Add::CreateCnf(satlib::SatI& satSolver)
{
    //The vector containing the result bits
    std::vector<satlib::RtpLit> r;
    //addend a
    std::vector<satlib::RtpLit> a;
    //addend b
    std::vector<satlib::RtpLit> b;
    // a vector for the carries
    std::vector<satlib::RtpLit> carries;
    // a vector for the last column
    std::vector<satlib::RtpLit> lastColumn;
    //Get the rtp literals for the result vector and a and b
    GetFanoutRtpLits(r);
    GetFanin()[0]->GetFanoutRtpLits(a);
    GetFanin()[1]->GetFanoutRtpLits(b);
    //Check this here if it doesn't work out implementation is wrong can be 
    //removed later
    assert(r.size()==a.size());
    assert(r.size()==b.size());

  
    for(unsigned i=0;i<r.size()-1;i++)
    {
        carries.push_back(satlib::RtpLit(GetMId(),GetMFanoutWidth()+1+i));
    }
    satSolver.makeHA(a[0],b[0],r[0],carries[0]);
    for(unsigned i=1;i<a.size()-1;i++){
        satSolver.makeFA(a[i],b[i],carries[i-1],r[i],carries[i]);
    }
    lastColumn.push_back(a[r.size()-1]);
    lastColumn.push_back(b[r.size()-1]);
    lastColumn.push_back(carries[r.size()-2]);
    lastColumn.push_back(r[r.size()-1]);
    satSolver.makeEvenParity(lastColumn);
}


void Add::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_add(btor, mFanin[0]->GetBtorComp(), mFanin[1]->GetBtorComp());
	BtorComp = comp;
}
