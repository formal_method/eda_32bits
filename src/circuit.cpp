/*! ************************************************************************
    \file circuit.cpp 
    \brief Implementation of the class Circuit 
    \author Sacha Loitz

   Implements the methods neeeded to setup circuit and work on it. 
   \copyright (c) University of Kaiserslautern Mon Jan 30 - Wed Feb 15 2006

Modification history:
 * 17.02.06 changed return type of AddComponent from bool to void. Whether an 
 * add is successful or not is now handled by exceptions
 * 17.02.06 added parameter fanout to the calls in add component

**************************************************************************/

#include "circuit.h"
#include "math.h"

/**
 * Constructors/Destructors
 */
/**
 * Methods
 */

/**
 * Add a new Component to a Module of the Circuit.
 * First of all it has to be checked whether the circuit contains a Module
 * with the name supplied. If this is not the case a new Module has to be 
 * created. The Component is than added to the given Module.
 * @return If the Component was successfully created true is returned. If 
 * the Component can not yet be created (missing fanout) false is 
 * returned.
 * @param type An integer representing the kind of the Component that 
 * shall be added (see CircuitBuilder for the mapping of value and type).
 * @param fanin A vector containing the names of the fanin Components that
 * support the new component. For details on the correct ordering of the 
 * fanin components please take a look at the subclass implementing the 
 * Component you want to add.
 * @param fanout A string with the name of the fanout of the new component
 * that shall be added.
 * @param module The Module the Component shall be added to
 * \todo replace map by hash_map
 */
FckId Circuit::AddComponent (int type, const std::vector<FckId>& fanin, 
                       const std::string& componentName, ModuleId  module)
{
    
    if (mModuleMap.end()==mModuleMap.find(module))
    {
        mModuleMap[module]=new Module(this,this->GetNextModuleId());
        //mModuleMap[module]->SetModuleName(module);
        if (mTopmodule==0) mTopmodule = module;
    }
//    FckId id = GetNextId();
    return mModuleMap[module]->AddComponent(type, fanin, componentName);
}

/**
 * Set the Topmodule
 * @param module The ID of the Module
 */
void Circuit::SetTopmodule(const ModuleId id)
{
    if (mModuleMap.end()!=mModuleMap.find(id))
    {
        mTopmodule = id;
    }
}

/**
 * Check whether the Circuit is complete.
 * Method to check whether a Circuit has been successfully created after 
 * all Components have been added. 
 * @return True if every Component has all required fanin Components and 
 * at least one fanout Component. To check this the method isComplete of 
 * every Component in mComponents is called.
 */
bool Circuit::IsComplete ()
{
    if (mModuleMap.size()==0)
    {
        std::cout<<"No Module in Circuit"<<std::endl;
        return false;
    }
    for( std::map<ModuleId, Module*>::iterator it = mModuleMap.begin(); 
         it != mModuleMap.end(); ++it )
        if (!it->second->IsComplete()) return false;
    //All Modules are complete so the circuit is complete
    return true;
} // End IsComplete()

/**  
 * Method to print which components the circuit contains.
 * Prints all components of the circuit with their type, fanin components
 * and fanout name.
 */
void Circuit::Print(std::string path)
{
    //print the content of each module
    for( std::map<ModuleId, Module*>::iterator it = mModuleMap.begin(); it != mModuleMap.end(); ++it )
        it->second->Print(path);
}

/**  
 * Method to reorder all modules
 */
void Circuit::Reorder()
{
    //print the content of each module
    for( std::map<ModuleId, Module*>::iterator it = mModuleMap.begin(); it != mModuleMap.end(); ++it )
        it->second->Reorder();
}

/**
 * Set whether a register updates its value on a falling or rising edge.
 * @param edge Can take the defined integer values FALLING(=1), RISING(=2)
 * or BOTH(=3) depending on the edge behaviour the register shall be set 
 * to.
 * @param cmp a string containing the name of the PseudoOutput whose egde
 * shall be set.
 */
void Circuit::AddEdge(int edge, FckId id, ModuleId module)
{
    GetComponent(id, module)->SetOnEdge(edge);
    //retrun if there is nothing to be done
    if (mContainedEdge==BOTH_ || mContainedEdge==edge) return;
    if (mContainedEdge==0) mContainedEdge=edge;
    else mContainedEdge=BOTH_;
}

/**
 * Get a Module by its index
 * @param module The name of the Module
 * @return The requested Module
 */
Module* Circuit::GetModuleByIndex(int i)
{
   std::map<ModuleId, Module*>::iterator iter;
   for( iter = mModuleMap.begin(); (iter != mModuleMap.end())&&(i>0); ++iter )
   {
      i--;
   }
   return iter->second;
}

void Circuit::deletecomp(Component* comp)
{
	if (comp->Getvarref()==0)
	{
		int t=0;  //dieu kien xoa la Comp chi co 1 fanout hoac cac fanout giong nhau
		for (int i=0; i< comp->GetFanout().size(); i++)
		{
			//if ((i>0)&&(comp->GetFanout()[0]!=comp->GetFanout()[i]))
			if (comp->GetFanout()[i]->GetMValid()==1)
			{
				t=1;
				break;
			}

		}
		if (t==0)
		{
			comp->SetMValid(0);
			for (int i=0; i< comp->GetFanin().size(); i++)
				deletecomp(comp->GetFanin()[i]);
		}
	}
	// chu y: co the cho lai gia tri varref=0 khi xet xong
}

unsigned int bin2dec(std::string bin);

// convert FCK component to Verilog code
// author: quandv
// date: 03-10
void Circuit::Circuit2Verilog(std::string path){


	// number of input
	unsigned int num_input = this->GetModule(1)->GetNumInps();

	// name of outputs
	std::vector<std::string> outputName = this->GetModule(1)->GetMOutputs();
	unsigned int numOuput = this->GetModule(1)->GetNumOuts();

	// type of component
	std::string str_type;

	// name of fanin
	std::string in1;

	// size of vector component
	int size = this->GetModule(1)->GetmComponents().size();

	// using for write FCK_<Component>.v
	int f_add=0,f_and=0,f_concat=0,f_eq=0,f_ite=0,f_lt=0,f_minus=0,f_mult=0,f_not=0,f_or=0,f_shl=0,f_shr=0,f_ps_input=0,f_ps_input_n = 0,
	    f_shra=0,f_ror=0,f_rol=0,f_sign_extend=0,f_slice=0,f_u_and=0,f_u_or=0,f_u_xor=0,f_xor=0,f_zero_extend=0,f_ram=0,f_output=0;

	// create code.v file
	std::string filename = "Results/Circuit/top.v";
	filename.insert(0, path);
	std::ofstream out_file(filename.c_str(),std::ios::out);
	if(!out_file){
		std::cout<<"File can not open!";
		return;
	}
	out_file<<"module test_im(\n";

	// write input variable
	for (unsigned int i=0;i<num_input;i++)
	{
		if(this->GetModule(1)->GetModuleInputs()[i]->GetMFanoutWidth()!=1)
			out_file<<"\t input ["<<this->GetModule(1)->GetModuleInputs()[i]->GetMFanoutWidth()-1<<":0] "
				<<this->GetModule(1)->GetModuleInputs()[i]->GetMInputName()<<",\n";
		else if(this->GetModule(1)->GetModuleInputs()[i]->GetMFanoutWidth()==1)		// width = 1
			out_file<<"\t input  "
					<<this->GetModule(1)->GetModuleInputs()[i]->GetMInputName()<<",\n";
	}

	// write output
	for (unsigned int i=0;i<outputName.size();i++)
	{
		//print its name
	  	if(i!=outputName.size()-1)
	  		if(this->GetModule(1)->GetOutputWidth(outputName[i])==1)
	  			out_file<<"\t output  "<<outputName[i]<<",\n";
	  		else
	  			out_file<<"\t output ["<<this->GetModule(1)->GetOutputWidth(outputName[i])-1<<":0] "
	  	  							<<outputName[i]<<",\n";
	  	else if(this->GetModule(1)->GetOutputWidth(outputName[i])==1)
				out_file<<"\t output  "<<outputName[i]<<"\n";
			else
			out_file<<"\t output ["<<this->GetModule(1)->GetOutputWidth(outputName[i])-1<<":0] "
	  							<<outputName[i]<<"\n";
	}

	out_file<<");\n\n";

	// using for set name
	std::ostringstream ss;

	// check whether fanout is output or not
	std::string type;


	// write wire or reg
	for(unsigned int i = 0;i<size;i++)
	{

	    switch(this->GetModule(1)->GetmComponents()[i]->GetMType())
	    {
	    	// khong tao wire voi INPUT, OUTPUT,...
	    	case 0:
	    	case 1:		break;
	    	case 2:		// PS_INPUT
				if(this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()!=1)
					out_file<<"\t reg ["<<this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()-1<<":0] "
						<<this->GetModule(1)->GetmComponents()[i]->GetType()+"_"<<this->GetModule(1)->GetmComponents()[i]->GetMId()<<"_o;\n";		// TYPE_ID_o
				else
				out_file<<"\t reg "<<this->GetModule(1)->GetmComponents()[i]->GetType()+"_"<<this->GetModule(1)->GetmComponents()[i]->GetMId()<<"_o;\n";		// TYPE_ID_o
				break;
	    	case 3:
	    	case 4: break;

	    	// voi cac function
	    	//case 3:		// C_INPUT
	    	case 5:		// ADD
	    	case 6:		// AND
	    	case 7:		// CONCAT
	    	case 8:		// EQ
	    	case 9:		// ITE
	    	case 10:	// LT
	    	case 11:	// MINUS
	    	case 12:	// MULT
	    	case 13:	// NOT
	    	case 14:	// OR
	    	case 15:  	// SHL
	    	case 16: 	// SHR
	    	case 17:	// SHRA-n
	    	case 18:	// ROR-n
	    	case 19:	// ROL-n
	    	case 20:	// SIGN_EXTEND
	    	case 21:	// SLICE
	    	case 22:	// U_AND
	    	case 23:	// U_OR
	    	case 24:	// U_XOR
	    	case 25:	// WRITE-n
	    	case 26:	// XOR
	    	case 27:	// ZERO_EXTEND
	    	case 29:	// PS_OUTPUT
				ss.str("");
				ss<<this->GetModule(1)->GetmComponents()[i]->GetMId();
				if(this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()==1)
					out_file<<"\t wire "<<this->GetModule(1)->GetmComponents()[i]->GetType()+"_"+ss.str()+"_o;\n";		// TYPE_ID_o
				else out_file<<"\t wire ["<<this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()-1<<":0] "
							 <<this->GetModule(1)->GetmComponents()[i]->GetType()+"_"+ss.str()+"_o;\n";		// TYPE_ID_o
					break;
	    	case 30:	// MUX
	    	case 31:	//DECODER
				ss.str("");
				ss<<this->GetModule(1)->GetmComponents()[i]->GetMId();
				if(this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()==1)
					out_file<<"\t reg "<<this->GetModule(1)->GetmComponents()[i]->GetType()+"_"+ss.str()+"_o;\n";		// TYPE_ID_o
				else out_file<<"\t reg ["<<this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()-1<<":0] "
							 <<this->GetModule(1)->GetmComponents()[i]->GetType()+"_"+ss.str()+"_o;\n";		// TYPE_ID_o
					break;
			default:;break;
	    }
	}

	out_file<<"\n";


	// tao cac khoi tao cho cac module
	for(unsigned int i = 0;i<size;i++)
	{
		in1="";
		str_type ="";
		//flag = 1; // danh dau fanout la output
		switch(this->GetModule(1)->GetmComponents()[i]->GetMType())
		{
			case 3:
				break;

			case 7:		// CONCAT
				f_concat = 1;
				str_type = this->GetModule(1)->GetmComponents()[i]->GetType();	// component type in string format
				ss.str("");
				ss<<this->GetModule(1)->GetmComponents()[i]->GetMId();
				out_file<<"\t FCK_"<<str_type;
				out_file<<" #(.WIDTH1("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMFanoutWidth()<<"),.WIDTH2("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[1]->GetMFanoutWidth()<<")) ";
				out_file<<str_type+"_"+ss.str();			// ten module FCK_TYPE, ten khoi tao la TYPE_ID
				out_file<<" (.out("<<str_type+"_"+ss.str()+"_o),";			// neu la wire, ten wire la TYPE_ID_o
				for(unsigned int j = 0;j<this->GetModule(1)->GetmComponents()[i]->GetFanin().size();j++)
				{
					// xet fanin
					ss.str("");
					ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMId();
					if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMType()==1)
						in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName(); // fanin 1 la input
					else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMType()==2)		// PS_INPUT
						in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetType()+ss.str()+"_o";
					else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMType()==3)		// C_INPUT
						in1 = "";
					else
					{
						//ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMId();
						in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetType()+ss.str()+"_o";	// ten wire TYPE_ID_o
					}
					if(j!=this->GetModule(1)->GetmComponents()[i]->GetFanin().size()-1)
						if(in1!="") out_file<<".in"<<j+1<<"("<<in1<<"),";
						// C_INPUT, format: m'dn
						else out_file<<".in"<<j+1<<"("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMFanoutWidth()<<"'d"<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName())<<"),";
					else if(in1!="") out_file<<".in"<<j+1<<"("<<in1<<"));\n";
						else out_file<<".in"<<j+1<<"("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMFanoutWidth()<<"'d"<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName())<<"));\n";
				}
				break;		// CONCAT break
			// voi cac function

			case 21:	// SLICE
				f_slice = 1;
				str_type = this->GetModule(1)->GetmComponents()[i]->GetType();	// component type in string format
				ss.str("");
				ss<<this->GetModule(1)->GetmComponents()[i]->GetMId();
				out_file<<"\t FCK_"<<str_type;

				out_file<<" #(.WIDTH_IN("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMFanoutWidth()<<"),";
				out_file<<".POS("<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[1]->GetMOutputName())<<"),";
				out_file<<".WIDTH_OUT("<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[2]->GetMOutputName())<<")) ";
				out_file<<str_type+"_"+ss.str()<<" ";			// ten module FCK_TYPE, ten khoi tao la TYPE_ID
				out_file<<" (.out("<<str_type+"_"+ss.str()+"_o),";//.in1("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName();

				ss.str("");
				ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMId();
				if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==1)
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName(); // fanin 1 la input
				else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==2)		// PS_INPUT
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetType()+ss.str()+"_o";
				else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==3)		// C_INPUT
					in1 = "";//bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName());
				else
				{
					//ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMId();
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetType()+ss.str()+"_o";	// ten wire TYPE_ID_o
				}
				if(in1!="") out_file<<".in("<<in1<<"));\n";
				// C_INPUT
				else out_file<<".in("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMFanoutWidth()
							 <<"'d"<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName())<<"));\n";
				break;		// SLICE break

			// EQ and LT has 1 bit output
			case 8:		// EQ
			case 10:	// LT
				if(this->GetModule(1)->GetmComponents()[i]->GetMType()==8) f_eq = 1;
				if(this->GetModule(1)->GetmComponents()[i]->GetMType()==10) f_lt = 1;
				str_type = this->GetModule(1)->GetmComponents()[i]->GetType();	// component type in string format
				ss.str("");
				ss<<this->GetModule(1)->GetmComponents()[i]->GetMId();
				out_file<<"\t FCK_"<<str_type;
				out_file<<" #(.WIDTH("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMFanoutWidth()<<")) ";	// width = width of fanin[0]
				out_file<<str_type+"_"+ss.str()<<" ";			// ten module FCK_TYPE, ten khoi tao la TYPE_ID;
				out_file<<"(.out("<<str_type+"_"+ss.str()+"_o),";			// neu la wire, ten wire la TYPE_ID_o

				for(unsigned int j = 0;j<this->GetModule(1)->GetmComponents()[i]->GetFanin().size();j++)
				{
					// xet fanin
					ss.str("");
					ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMId();
					if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMType()==1)
						in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName(); // fanin 1 la input
					else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMType()==2)		// PS_INPUT
						in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetType()+ss.str()+"_o";
					else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMType()==3)		// C_INPUT
						in1 = "";//bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName());
					else
					{
						//ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMId();
						in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetType()+ss.str()+"_o";	// ten wire TYPE_ID_o
					}
					if(j!=this->GetModule(1)->GetmComponents()[i]->GetFanin().size()-1)
						if(in1!="") out_file<<".in"<<j+1<<"("<<in1<<"),";
						else out_file<<".in"<<j+1<<"("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMFanoutWidth()
								<<"'d"<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName())<<"),";	// C_INPUT
					else if(in1!="") out_file<<".in"<<j+1<<"("<<in1<<"));\n";
						else out_file<<".in"<<j+1<<"("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMFanoutWidth()
								<<"'d"<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName())<<"));\n"; // C_INPUT
				}
				break;
			case 5:		// ADD
			case 6:		// AND
			case 11:	// MINUS
			case 12:	// MULT
			case 13:	// NOT
			case 14:	// OR
			case 22:	// U_AND
			case 23:	// U_OR
			case 24:	// U_XOR
			case 25:	// WRITE
			case 26:	// XOR
				switch(this->GetModule(1)->GetmComponents()[i]->GetMType())
				{
					case 5: f_add = 1; break;
					case 6:	f_and = 1; break;	// AND
					case 11: f_minus = 1; break; 	// MINUS
					case 12: f_mult = 1; break;	// MULT
					case 13: f_not = 1; break;	// NOT
					case 14: f_or = 1; break;	// OR
					case 22: f_u_and = 1; break;	// U_AND
					case 23: f_u_or = 1; break;	// U_OR
					case 24: f_u_xor = 1; break;	// U_XOR
					case 25: break;			// WRITE
					case 26: f_xor = 1; break;	// XOR
					default: ;
				}
//				if(this->GetModule(1)->GetmComponents()[i]->GetMtype()==9)	// ITE
//					if(this->GetModule(1)->GetmComponents()[i]->Get)
				str_type = this->GetModule(1)->GetmComponents()[i]->GetType();	// component type in string format
				ss.str("");
				ss<<this->GetModule(1)->GetmComponents()[i]->GetMId();
				out_file<<"\t FCK_"<<str_type;
				if(this->GetModule(1)->GetmComponents()[i]->GetMType()==22||this->GetModule(1)->GetmComponents()[i]->GetMType()==23||this->GetModule(1)->GetmComponents()[i]->GetMType()==24)	// Unary operation
					out_file<<" #(.WIDTH("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMFanoutWidth()<<")) ";	// width = width of fanin[0]
				else out_file<<" #(.WIDTH("<<this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()<<")) ";
				out_file<<str_type+"_"+ss.str()<<" ";			// ten module FCK_TYPE, ten khoi tao la TYPE_ID;
				out_file<<"(.out("<<str_type+"_"+ss.str()+"_o),";			// neu la wire, ten wire la TYPE_ID_o

				for(unsigned int j = 0;j<this->GetModule(1)->GetmComponents()[i]->GetFanin().size();j++)
				{
					// xet fanin
					ss.str("");
					ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMId();
					if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMType()==1)
						in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName(); // fanin 1 la input
					else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMType()==2)		// PS_INPUT
						in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetType()+ss.str()+"_o";
					else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMType()==3)		// C_INPUT
						in1 = "";//bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName());
					else
					{
						//ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMId();
						in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetType()+ss.str()+"_o";	// ten wire TYPE_ID_o
					}
					if(j!=this->GetModule(1)->GetmComponents()[i]->GetFanin().size()-1)
						if(in1!="") out_file<<".in"<<j+1<<"("<<in1<<"),";
						else out_file<<".in"<<j+1<<"("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMFanoutWidth()
									<<"'d"<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName())<<"),";	// C_INPUT
					else if(in1!="") out_file<<".in"<<j+1<<"("<<in1<<"));\n";
						else out_file<<".in"<<j+1<<"("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMFanoutWidth()
								<<"'d"<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName())<<"));\n"; // C_INPUT
				}
				break;		// switch-case break

			case 9:		// ITE
				// neu PSO la fanin thi k tao ITE
				if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[1]->GetMType()==PS_OUTPUT || this->GetModule(1)->GetmComponents()[i]->GetFanin()[2]->GetMType()==PS_OUTPUT)
					break;
				f_ite = 1;
				str_type = this->GetModule(1)->GetmComponents()[i]->GetType();	// component type in string format
				ss.str("");
				ss<<this->GetModule(1)->GetmComponents()[i]->GetMId();
				out_file<<"\t FCK_"<<str_type;
				if(this->GetModule(1)->GetmComponents()[i]->GetMType()==22||this->GetModule(1)->GetmComponents()[i]->GetMType()==23||this->GetModule(1)->GetmComponents()[i]->GetMType()==24)	// Unary operation
					out_file<<" #(.WIDTH("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMFanoutWidth()<<")) ";	// width = width of fanin[0]
				else out_file<<" #(.WIDTH("<<this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()<<")) ";
				out_file<<str_type+"_"+ss.str()<<" ";			// ten module FCK_TYPE, ten khoi tao la TYPE_ID;
				out_file<<"(.out("<<str_type+"_"+ss.str()+"_o),";			// neu la wire, ten wire la TYPE_ID_o

				for(unsigned int j = 0;j<this->GetModule(1)->GetmComponents()[i]->GetFanin().size();j++)
				{
					// xet fanin
					ss.str("");
					ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMId();
					if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMType()==1)
						in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName(); // fanin 1 la input
					else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMType()==2)		// PS_INPUT
						in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetType()+ss.str()+"_o";
					else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMType()==3)		// C_INPUT
						in1 = "";//bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName());
					else
					{
						//ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMId();
						in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetType()+ss.str()+"_o";	// ten wire TYPE_ID_o
					}
					if(j!=this->GetModule(1)->GetmComponents()[i]->GetFanin().size()-1)
						if(in1!="") out_file<<".in"<<j+1<<"("<<in1<<"),";
						else out_file<<".in"<<j+1<<"("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMFanoutWidth()
									<<"'d"<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName())<<"),";	// C_INPUT
					else if(in1!="") out_file<<".in"<<j+1<<"("<<in1<<"));\n";
						else out_file<<".in"<<j+1<<"("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMFanoutWidth()
								<<"'d"<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName())<<"));\n"; // C_INPUT
				}
				break;

			case 15:  	// SHL
			case 16: 	// SHR
			case 17:	// SHRA
				if(this->GetModule(1)->GetmComponents()[i]->GetMType()==15) f_shl = 1;
				if(this->GetModule(1)->GetmComponents()[i]->GetMType()==16) f_shr = 1;
				if(this->GetModule(1)->GetmComponents()[i]->GetMType()==17) f_shra = 1;
				str_type = this->GetModule(1)->GetmComponents()[i]->GetType();	// component type in string format
				ss.str("");
				ss<<this->GetModule(1)->GetmComponents()[i]->GetMId();
				out_file<<"\t FCK_"<<str_type;

				out_file<<" #(.WIDTH("<<this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()<<"),";
				out_file<<".NUM("<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[1]->GetMOutputName())<<")) ";
				out_file<<str_type+"_"+ss.str()<<" ";			// ten module FCK_TYPE, ten khoi tao la TYPE_ID
				out_file<<"(.out("<<str_type+"_"+ss.str()+"_o),";//.in1("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName();

				ss.str("");
				ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMId();
				if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==1)
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName(); // fanin 1 la input
				else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==2)		// PS_INPUT
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetType()+ss.str()+"_o";
				else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==3)		// C_INPUT
					in1 = "";
				else
				{
					//ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMId();
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetType()+ss.str()+"_o";	// ten wire TYPE_ID_o
				}
				if(in1!="") out_file<<".in1("<<in1<<"));\n";
				else out_file<<".in1("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMFanoutWidth()
						<<"'d"<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName())<<"));\n";
				break;		// SHL break

			case 18:	// ROR
			case 19:	// ROL
				break;
			case 20:	// SIGN_EXTEND
			case 27:	// ZERO_EXTEND
				if(this->GetModule(1)->GetmComponents()[i]->GetMType()==20) f_sign_extend = 1;
				if(this->GetModule(1)->GetmComponents()[i]->GetMType()==27) f_zero_extend = 1;
				str_type = this->GetModule(1)->GetmComponents()[i]->GetType();	// component type in string format

				ss.str("");
				ss<<this->GetModule(1)->GetmComponents()[i]->GetMId();
				out_file<<"\t FCK_"<<str_type;
				out_file<<" #(.WIDTH_IN("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMFanoutWidth()
					<<"),.WIDTH_OUT("<<this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()<<")) ";
				out_file<<str_type+"_"+ss.str()<<" ";			// ten module FCK_TYPE, ten khoi tao la TYPE_ID
				out_file<<"(.out("<<str_type+"_"+ss.str()+"_o),";			// neu la wire, ten wire la TYPE_ID_o

				ss.str("");
				ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMId();
				if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==1)			// INPUT
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName();
				else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==2)			// PS_INPUT
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetType()+ss.str()+"_o";
				else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==3)		// C_INPUT
					in1 = "";
				else
				{
					//ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMId();
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetType()+ss.str()+"_o";	// ten wire TYPE_ID_o
				}
				if(in1!="") out_file<<".in("<<in1<<"));\n";
				else out_file<<".in("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMFanoutWidth()
						<<"'d"<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName())<<"));\n";

				break;		// switch-case break

			case 32:	// OUTPUT
				f_output = 1;
				out_file<<"\t FCK_BUFF #(.WIDTH("<<this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()<<")) BUFF_"<<this->GetModule(1)->GetmComponents()[i]->GetMId();
				ss.str("");
				ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMId();
				//in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetType()+ss.str()+"_o";	// fanin TYPE_ID_o
				if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==1)
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName(); // fanin 1 la input
				else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==2)		// PS_INPUT
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetType()+ss.str()+"_o";
				else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==3)		// C_INPUT
					in1 = "";//bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName());
				else
				{
					//ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMId();
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetType()+ss.str()+"_o";	// ten wire TYPE_ID_o
				}

				if(this->GetModule(1)->GetmComponents()[i]->GetMType()==29) out_file<<" (.out("<<this->GetModule(1)->GetmComponents()[i]->GetType()+"_"<<this->GetModule(1)->GetmComponents()[i]->GetMId()
													 <<"_o"<<"),";
				else out_file<<" (.out("<<this->GetModule(1)->GetmComponents()[i]->GetMOutputName()<<"),";

				if(in1!="") out_file<<".in"<<"("<<in1<<"));\n";
				else out_file<<".in"<<"("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMFanoutWidth()
							<<"'d"<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName())<<"));\n"; // C_INPUT

				break;

			case 29:	//	PS_OUTPUT
				// tao bo dem noi tin hieu cho PS_OUTPUT
				f_output = 1;
				out_file<<"\t FCK_BUFF #(.WIDTH("<<this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()<<")) BUFF_"<<this->GetModule(1)->GetmComponents()[i]->GetMId();
				ss.str("");
				ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetMId();
				out_file<<" (.out("<<this->GetModule(1)->GetmComponents()[i]->GetType()+ss.str()+"_o),";

				ss.str("");
				ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMId();
				if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==1)
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName(); // fanin 1 la input
				else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==2)		// PS_INPUT
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetType()+ss.str()+"_o";
				else if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==3)		// C_INPUT
					in1 = "";//bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMInputName());
				else
				{
					//ss<<"_"<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j]->GetMId();
					in1 = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetType()+ss.str()+"_o";	// ten wire TYPE_ID_o
				}

				if(in1!="") out_file<<".in"<<"("<<in1<<"));\n";
				else out_file<<".in"<<"("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMFanoutWidth()
							<<"'d"<<bin2dec(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName())<<"));\n"; // C_INPUT


				// Tao DFF
				for(int j = 0;j<size;j++)	// tim PS_OUTPUT co cung ten voi PS_INPUT
					if ((this->GetModule(1)->GetmComponents()[j]->GetMOutputName()==this->GetModule(1)->GetmComponents()[i]->GetMInputName())
						&& (this->GetModule(1)->GetmComponents()[j]->GetMType() == 2)
						&& (this->GetModule(1)->GetmComponents()[j]->Getarray_index()==this->GetModule(1)->GetmComponents()[i]->Getarray_index()))
					{
						if(((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->Getreset_name()!="")
						{
							f_ps_input = 1;	// DFF co reset
							if ((std::string)((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->Getreset_edge()=="NEG")
								out_file<<"\t FCK_DFF1 #(.WIDTH(";
							else out_file<<"\t FCK_DFF0 #(.WIDTH(";
							out_file<<this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()<<")) DFF_"<<this->GetModule(1)->GetmComponents()[i]->GetMId();
							if(this->GetModule(1)->GetmComponents()[i]->Getend_index()==0 && this->GetModule(1)->GetmComponents()[i]->Getstart_index()==0)
								out_file<<" (.Q(PS_INPUT_"<<this->GetModule(1)->GetmComponents()[j]->GetMId()<<"_o),";	// fanout is 1 bit-PS_INPUT
							else
								out_file<<" (.Q(PS_INPUT_"<<this->GetModule(1)->GetmComponents()[j]->GetMId()<<"_o["<<this->GetModule(1)->GetmComponents()[i]->Getend_index()<<":"<<this->GetModule(1)->GetmComponents()[i]->Getstart_index()<<"]),";	// fanout is PS_INPUT
							out_file<<".D(PS_OUTPUT_"<<this->GetModule(1)->GetmComponents()[i]->GetMId()<<"_o),";	// fanin is PS_OUTPUT
							if(this->GetComponent(((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->GetResetValue(),1)->GetMType()==1)	// Kieu INPUT
								out_file<<".preset("<<this->GetComponent(((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->GetResetValue(),1)->GetMOutputName()<<"),";
							else if(this->GetComponent(((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->GetResetValue(),1)->GetMType()==3)
								out_file<<".preset("<<this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()<<"'d"<<bin2dec(this->GetComponent(((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->GetResetValue(),1)->GetMOutputName())<<"),";
							else //cout<<"preset cua component "<<this->GetModule(1)->GetmComponents()[i]->GetMOutputName()<<" k phu hop!\n";
							{
								out_file<<".preset("<<this->GetComponent(((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->GetResetValue(),1)->GetType()
										<<"_"<<this->GetComponent(((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->GetResetValue(),1)->GetMId()<<"_o),";
							}
							Component* clk = this->GetComponent(((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->GetClock(),1);
							if (clk->GetMType()==INPUT)
								out_file<<".clk("<<((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->Getclock_name()<<")";		// default clk is positive edge
							else out_file<<".clk("<<clk->GetType()<<"_"<<clk->GetMId()<<"_o)";

							if(this->GetComponent(((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->GetReset(),1)->GetMType()==1)
							{
								out_file<<",.rst_n("<<this->GetComponent(((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->GetReset(),1)->GetMOutputName()<<"));\n";
								break;
							}
							else
							{
								out_file<<",.rst_n("<<this->GetComponent(((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->GetReset(),1)->GetType()<<"_"<<this->GetComponent(((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->GetReset(),1)->GetMId()<<"_o));\n";
								break;
							}
						}
						else
						{
							f_ps_input_n = 1;	// DFF khong co reset
							out_file<<"\t FCK_DFF2 #(.WIDTH("<<this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()<<")) DFF_"<<this->GetModule(1)->GetmComponents()[i]->GetMId();
							if(this->GetModule(1)->GetmComponents()[i]->Getend_index()==0 && this->GetModule(1)->GetmComponents()[i]->Getstart_index()==0)
								out_file<<" (.Q(PS_INPUT_"<<this->GetModule(1)->GetmComponents()[j]->GetMId()<<"_o),";
							else out_file<<" (.Q(PS_INPUT_"<<this->GetModule(1)->GetmComponents()[j]->GetMId()<<"_o["<<this->GetModule(1)->GetmComponents()[i]->Getend_index()<<":"<<this->GetModule(1)->GetmComponents()[i]->Getstart_index()<<"]),";	// fanout is PS_INPUT
							out_file<<".D(PS_OUTPUT_"<<this->GetModule(1)->GetmComponents()[i]->GetMId()<<"_o),";	// fanin is PS_OUTPUT
							Component* clk = this->GetComponent(((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->GetClock(),1);
							if (clk->GetMType()==INPUT)
								out_file<<".clk("<<((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->Getclock_name()<<"));\n";		// default clk is positive edge
							else out_file<<".clk("<<clk->GetType()<<"_"<<clk->GetMId()<<"_o));\n";
							//out_file<<".clk("<<((PseudoOutput*)this->GetModule(1)->GetmComponents()[i])->Getclock_name()<<"));\n";
						}
					}
				break;
			case 30:	// MUX
				out_file<<"\t always @(*) \n";
				str_type = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetType();	// component type in string format
				ss.str("");
				ss<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMId();
				if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==INPUT)
					out_file<<"\t\t case ("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName()<<")\n";
				else out_file<<"\t\t case ("<<str_type+"_"+ss.str()+"_o)\n";
				for (int j=0; j<this->GetModule(1)->GetmComponents()[i]->GetFanin().size()-1; j++)
				{
					str_type = this->GetModule(1)->GetmComponents()[i]->GetFanin()[j+1]->GetType();	// component type in string format
					ss.str("");
					ss<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[j+1]->GetMId();
					out_file<<"\t\t\t"<<j<<" : "<<"MUX_"<<this->GetModule(1)->GetmComponents()[i]->GetMId()<<"_o"<<" = "<<str_type+"_"+ss.str()+"_o"<<";\n";
				}
				out_file<<"\t\t endcase\n";
				break;
			case 31: //DECODER
				out_file<<"\t always @(*) \n";
				str_type = this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetType();	// component type in string format
				ss.str("");
				ss<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMId();
				if(this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMType()==INPUT)
					out_file<<"\t\t case ("<<this->GetModule(1)->GetmComponents()[i]->GetFanin()[0]->GetMInputName()<<")\n";
				else out_file<<"\t\t case ("<<str_type+"_"+ss.str()+"_o)\n";
				for (int j=0; j<this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth(); j++)
				{
					out_file<<"\t\t\t"<<j<<" : "<<"DECODER_"<<this->GetModule(1)->GetmComponents()[i]->GetMId()<<"_o"<<" = "<<this->GetModule(1)->GetmComponents()[i]->GetMFanoutWidth()<<"'d"<<pow(2,j)<<";\n";
				}
				out_file<<"\t\t endcase\n";
				break;
			default: break;
		}


	}
	out_file<<"\nendmodule\n";


	// write FCK_<function_ref>


	// PS_INPUT voi reset khong dong bo muc cao
	if(f_ps_input){
		out_file<<"\nmodule FCK_DFF0 #(parameter WIDTH = 8)(\n\
	input [WIDTH-1:0] D,\n\
	input clk,rst_n,\n\
	input [WIDTH-1:0] preset, \n\
	output [WIDTH-1:0] Q\n\
);\n\
	reg [WIDTH-1:0] Q;\n\
	always@(posedge clk or posedge rst_n)\n\
	begin\n\
		if(rst_n) Q<=preset;\n\
		else Q<=D;\n\
	end\n\
endmodule\n";
}

	// PS_INPUT voi reset khong dong bo muc thap
	if(f_ps_input){
		out_file<<"\nmodule FCK_DFF1 #(parameter WIDTH = 8)(\n\
	input [WIDTH-1:0] D,\n\
	input clk,rst_n,\n\
	input [WIDTH-1:0] preset, \n\
	output [WIDTH-1:0] Q\n\
);\n\
	reg [WIDTH-1:0] Q;\n\
	always@(posedge clk or negedge rst_n)\n\
	begin\n\
		if(!rst_n) Q<=preset;\n\
		else Q<=D;\n\
	end\n\
endmodule\n";
}

	// PS_INPUT k co reset
	if(f_ps_input_n){
		out_file<<"\nmodule FCK_DFF2 #(parameter WIDTH = 8)(\n\
	input [WIDTH-1:0] D,\n\
	input clk,\n\
	output [WIDTH-1:0] Q\n\
);\n\
	reg [WIDTH-1:0] Q;\n\
	always@(posedge clk)\n\
	begin\n\
		Q<=D;\n\
	end\n\
endmodule\n";
}
    	// voi cac function
	// ADD
	if(f_add){
		out_file<<"module FCK_ADD #(parameter WIDTH=8)\n\
(\n\
	input [WIDTH-1:0] in1,in2,\n\
	output [WIDTH-1:0] out\n\
);\n\
	assign out = in1+in2;\n\
endmodule\n";
}
	// AND
	if(f_and){
		out_file<<"module FCK_AND #(parameter WIDTH=8)\n\
(\n\
	input [WIDTH-1:0] in1,in2,\n\
	output [WIDTH-1:0] out\n\
);\n\
	assign out = in1 & in2;\n\
endmodule\n";
}
	// CONCAT
	if(f_concat){
		out_file<<"module FCK_CONCAT #(parameter WIDTH1 = 4, WIDTH2 = 4)(\n\
	input [WIDTH1-1:0] in1,\n\
	input [WIDTH2-1:0] in2,\n\
	output [WIDTH1+WIDTH2-1:0] out);\n\
	\n\
	assign out = {in1,in2};\n\
endmodule\n";
}
	// EQ
	if(f_eq) out_file<<"module FCK_EQ #(parameter WIDTH=8)(\n\
	input [WIDTH-1:0] in1,in2,\n\
	output out);\n\
	assign out = in1 == in2;\n\
endmodule\n";

	// ITE
	if(f_ite) out_file<<"module FCK_ITE #(parameter WIDTH=8)(\n\
	input in1,\n\
	input [WIDTH-1:0] in2,in3,\n\
	output [WIDTH-1:0] out);\n\
\n\
	assign out = in1 ? in2:in3;\n\
endmodule\n";

	// LT
	if(f_lt) out_file<<"module FCK_LT #(parameter WIDTH=8)(\n\
	input [WIDTH-1:0] in1,in2,\n\
	output out\n\
);\n\
	assign out = in1 < in2;\n\
endmodule\n";

	// MINUS
	if(f_minus) out_file<<"module FCK_MINUS #(parameter WIDTH=8)(\n\
	input [WIDTH-1:0] in1,\n\
	output [WIDTH-1:0] out\n\
);\n\
	assign out = ~in1 + 1;\n\
endmodule\n";

	// MULT
	if(f_mult) out_file<<"module FCK_MULT #(parameter WIDTH=8)(\n\
	input [WIDTH-1:0] in1,in2,\n\
	output [WIDTH+WIDTH-1:0] out\n\
);\n\
	assign out = in1*in2;\n\
endmodule\n";

	// NOT
	if(f_not) out_file<<"module FCK_NOT #(parameter WIDTH=8)(\n\
	input [WIDTH-1:0] in1,\n\
	output [WIDTH-1:0] out\n\
);\n\
	assign out = ~in1;\n\
endmodule\n";

	// OR
	if(f_or) out_file<<"module FCK_OR #(parameter WIDTH=8)(\n\
	input [WIDTH-1:0] in1,in2,\n\
	output [WIDTH-1:0] out\n\
);\n\
	assign out = in1|in2;\n\
endmodule\n";

  	// SHL
	if(f_shl) out_file<<"module FCK_SHL #(parameter WIDTH=8,NUM = 0)(\n\
	input [WIDTH-1:0] in1,\n\
	output [WIDTH-1:0] out\n\
);\n\
	assign out = in1<<NUM;\n\
endmodule\n";

 	// SHR
	if(f_shr) out_file<<"module FCK_SHR #(parameter WIDTH=8,NUM = 0)(\n\
	input [WIDTH-1:0] in1,\n\
	output [WIDTH-1:0] out\n\
);\n\
	assign out = in1>>NUM;\n\
endmodule\n";

	// SIGN_EXTEND
	if(f_sign_extend) out_file<<"module FCK_SIGN_EXTEND #(parameter WIDTH_IN = 4, WIDTH_OUT = 8) (\n\
	input [WIDTH_IN-1:0] in,\n\
	output [WIDTH_OUT-1:0] out);\n\
\n\
	assign out = {{(WIDTH_OUT - WIDTH_IN){in[WIDTH_IN-1]}},in};\n\
endmodule\n";

	// SLICE
	if(f_slice) out_file<<"module FCK_SLICE #(parameter WIDTH_IN = 8, POS = 0, WIDTH_OUT = 8) (\n\
	input [WIDTH_IN-1:0] in,\n\
	output [WIDTH_OUT-1:0] out);\n\
\n\
	assign out = in[WIDTH_OUT+POS-1:POS];\n\
endmodule\n";

	// U_AND
	if(f_u_and) out_file<<"module FCK_U_AND #(parameter WIDTH=8)\n\
(\n\
	input [WIDTH-1:0] in1,\n\
	output out\n\
);\n\
	assign out = &in1;\n\
endmodule\n";

	// U_OR
	if(f_u_or) out_file<<"module FCK_U_OR #(parameter WIDTH=8)\n\
(\n\
	input [WIDTH-1:0] in1,\n\
	output out\n\
);\n\
	assign out = |in1;\n\
endmodule\n";

	// U_XOR
	if(f_u_xor) out_file<<"module FCK_U_XOR #(parameter WIDTH=8)\n\
(\n\
	input [WIDTH-1:0] in1,\n\
	output out\n\
);\n\
	assign out = ^in1;\n\
endmodule\n";

	// XOR
	if(f_xor) out_file<<"module FCK_XOR #(parameter WIDTH=8)(\n\
	input [WIDTH-1:0] in1,in2,\n\
	output [WIDTH-1:0] out\n\
);\n\
	assign out = in1 ^ in2;\n\
endmodule\n";

	// ZERO_EXTEND
	if(f_zero_extend) out_file<<"module FCK_ZERO_EXTEND #(parameter WIDTH_IN = 4, WIDTH_OUT = 8) (\n\
	input [WIDTH_IN-1:0] in,\n\
	output [WIDTH_OUT-1:0] out);\n\
\n\
	assign out = {{(WIDTH_OUT - WIDTH_IN){1'b0}},in};\n\
endmodule\n";

	// PS_OUTPUT
	// OUTPUT
	if(f_output) out_file<<"module FCK_BUFF #(parameter WIDTH=8)(\n\
	input [WIDTH-1:0] in,\n\
	output [WIDTH-1:0] out\n\
);\n\
	assign out = in;\n\
endmodule\n";

	out_file.close();

}



unsigned int bin2dec(std::string bin)
{
	unsigned int dec = 0;
	for (int i=bin.size()-1;i>=0; i--)
		if (bin[i]=='1') dec = dec + pow(2,bin.size()-i-1);
	return dec;
}

