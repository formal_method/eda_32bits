/*! ************************************************************************
    \file ifthenelse.cpp
    \brief Implementation of the Class IfThenElse

    The class contained in this file implements the methods of the Function
 * IfThenElse
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Thu Feb 09 2006

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout

**************************************************************************/

#include "ifthenelse.h"

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * An ifThenElse has three fanin Components so initialize mRequiredFanin with 
 * 3. The order of the inputs is condition at pos=0, then statement at pos=1 
 * and else statement at pos=2.
 * Add the fanin components to the fanin of this IfThenElse.
 * @param fanin A vector containing the three Fanin Components of an 
 * IfThenElse.
 * @param fanout The string naming the output
 * @param id The ID of this IfThenElse
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 * @exception InputSizeException is thrown if the fanins do not have the 
 * correct size (fanin1: 1 and the other two the same size).
 */
IfThenElse::IfThenElse(std::vector<Component*>& fanin, std::string fanout, 
                        int id, Module* module)
{
    mModule=module;
    SetMRequiredFanin(3);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMType(ITE);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin) 
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have three fanin components
    //check whether the first fanin is of width 1
    if (fanin[0]->GetMFanoutWidth()!=1)
    {
        std::string message = "EXCEPTION: The width of the first fanin is not 1."; 
    } 
    //Both data inputs shall have the same width
    mFanoutWidth=fanin[1]->GetMFanoutWidth();
    //First of all we allocate space for the Vector mFanin with undefined
    //fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //Therefore we use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to position i. 
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that the data inputs have the correct size
        try
	{
	if (i>0 && mFanoutWidth!=fanin[i]->GetMFanoutWidth())
        {
            std::string message="EXCEPTION: The width of the fanins is not";
            message +=" equivalent."; 
            throw InputSizeException(message, this, fanin);
        }
	}
	catch(InputSizeException e)
	{
		e.GetMessage();
		throw e;
	}  
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mcRequiredFanin;i++)
} //End IfThenElse(vector<Component> fanin)

/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin.
 */
void IfThenElse::SimulateForward () 
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
} //End SimulateForward

/**
 * Perform a simulation of this IfThenElse for a given vector of inputs.
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void IfThenElse::Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
{
    //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for
    //We reset mOutput
    outVals.clear();
    //The first input must have only a single bit
    try
    {
        if (inpVals[0].size()!=1)
        {
            //The two inputs do not have the same width so we use a sign 
            //extension in order to be able to add the to inputs. Throw an 
            //Exception to inform the user. 
            std::string message = "EXCEPTION: The first input of this "; 
            message+="IfThenElse function does not have the width 1. I will ";
            message+="either use a false if the input is empty or the value ";
            message+="at position 0.";
            throw InputSizeException(message, this, mFanin);
        } // End if (mInputs[0].size()!=mInputs[1].size())
    } // End try
    //We handle the input size exception directly by zero extending the 
    //smaller input.
    catch(InputSizeException e)
    {
        //print the message to let the user know that we use a zero extended
        //input
        e.GetMessage();
        //if the input for the condition is empty push a False value
        if (inpVals[0].size()==0)
            inpVals[0].push_back(satlib::SatI::FALSE);
    } // End catch(InputSizeException e)
    //We want both data inputs to have the same size
    try
    {
        if (inpVals[1].size()!=inpVals[2].size())
        {
            //The two inputs do not have the same width so we use a sign 
            //extension in order to be able to add the to inputs. Throw an 
            //Exception to inform the user. 
            std::string message = "EXCEPTION: The inputs of this IfThenElse "; 
            message+="function do not have the same width. I will use a zero ";
            message+="extension.";
            throw InputSizeException(message, this, mFanin);
        } // End if (mInputs[0].size()!=mInputs[1].size())
    } // End try
    //We handle the input size exception directly by zero extending the 
    //smaller input.
    catch(InputSizeException e)
    {
        //print the message to let the user know that we use a zero extended
        //input
        e.GetMessage();
        //Determine the smaller input; store in the variables smaller and 
        //bigger
        int smaller, bigger;
        if (inpVals[1].size()<inpVals[2].size())
        {
            smaller=1;
            bigger=2;
        }
        else
        {
            smaller=2;
            bigger=1;
        }
        //As long as smaller is smaller than bigger push sign to smaller
        while (inpVals[smaller].size()!=inpVals[bigger].size())
            inpVals[smaller].push_back(satlib::SatI::FALSE);
    } // End catch(InputSizeException e)
    //Determine which data input is to be used
    int useIn=0;
    //if the statement is true use input at pos=1
    if (inpVals[0][0]==satlib::SatI::TRUE) useIn=1;
    //else if statement is false use input at pos =2
    else if (inpVals[0][0]==satlib::SatI::FALSE) useIn=2; //C. Villarraga 01.08.2011 correction use inpVals instead of mInputs
    //else condition is undefined check for each bit, whether input 1 and 
    //input 2 are equivalent if this is the case use that value, else use
    //l_Undef 
    else
    {
        //We consider each input bit separately from the lowest bits to the 
        //highest one.
        for (unsigned int i=0;i<inpVals[1].size();i++)
        { 
            //if equivalent use the value
            if(inpVals[1][i]==inpVals[2][i]) outVals.push_back(inpVals[1][i]);
            //else use l_Undef
            else outVals.push_back(satlib::SatI::DONTCARE);
        
        } //End for (int i=0;i<mInputs[0].size();i++)
        //leave this method
        return;
    } //End else
    //push the values of the input determined by useIn
    for (unsigned int i=0;i<inpVals[useIn].size();i++) 
        outVals.push_back(inpVals[useIn][i]);
} //End Simulate       

/**
 * Create the CNF for this IfThenElse.
 * @param solver the interface to the solver
 */
void IfThenElse::CreateCnf(satlib::SatI& satSolver)
{
    //vector for the literals of the result
    std::vector<satlib::RtpLit> r;
    //vector for the literals of the condition
    std::vector<satlib::RtpLit> s;
    //vector for the literals of the then statement
    std::vector<satlib::RtpLit> a;
    //vector for the literals of the else statement
    std::vector<satlib::RtpLit> b;
    assert(GetFanin().size()==3);
    //get the literals for the vectors defined above
    GetFanoutRtpLits(r);
    GetFanin()[0]->GetFanoutRtpLits(s);
    //std::cout<<"name of GetFanin()[0]: "<<GetFanin()[0]->GetMOutputName()<<std::endl;
    //std::cout<<"bitwidth of GetFanin()[0]: "<<GetFanin()[0]->GetMFanoutWidth()<<std::endl;
    //std::cout<<"name of GetFanin()[1]: "<<GetFanin()[1]->GetMOutputName()<<std::endl;
    GetFanin()[1]->GetFanoutRtpLits(a);
    GetFanin()[2]->GetFanoutRtpLits(b);
    // std::cout<<"condition size: "<<s.size()<<std::endl;
    assert(s.size()==1);
    assert(r.size()==a.size());
    assert(r.size()==b.size());
    //make the clauses for the ite constraint
    for(unsigned i=0;i<r.size();i++)
    {
        satSolver.makeIte(s[0],a[i],b[i],r[i]);
    }
}

void IfThenElse::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_cond(btor, mFanin[0]->GetBtorComp(), mFanin[1]->GetBtorComp(), mFanin[2]->GetBtorComp());
	BtorComp = comp;
}
