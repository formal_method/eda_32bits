/*! ************************************************************************
    \file pseudooutput.cpp
    \brief Implementation of class PseudoOutput

    Implementation of the methods of the class Pseudooutput
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Tue Feb 14 2006

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout

**************************************************************************/

#include "pseudooutput.h"


/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector containing the fanin and fanout Components.
 * A PseudoOutput has one fanin Component so initialize mRequiredFanin with 1.
 * The second string given in name is the bitwidth of the PseudoOutput.
 * @param ids A vector containing the fanin and fanout Component of the
 * PseudoOutput
 * @param name The string naming the output
 * @param id The ID of the PseudoOutput
 * @exception GeneralFanException is thrown if the size of the fanin does not
 * match mRequiredFanin
 */
PseudoOutput::PseudoOutput(const std::vector<FckId>& ids,
                           const std::string& componentName, int id, Module* module)
{
    mModule=module;
    mReset=0;
    mResetVal=0;
    SetMRequiredFanin(1);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMType(PS_OUTPUT);
    //SetMInputName(name[0]);
    SetMInputName(componentName);
    SetMOutputName(componentName);
    mId=id;
    BtorComp = NULL;
    //We can not add the fanin component yet, as the corresponding fanin
    //does not exist yet. So we only allocate space for the Vector mFanin with
    //undefined fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //The width has to extracted from the string before it can be set.
    SetWidth(ids[0]);
    //Set the name of the output
    SetMOutputName(componentName);
} //End Output(vector<Component> fanin)
/**
 * Methods
 */

/**
 * Add a Component to the fanin of this PseudoOutput.
 * In differenece to an Input a PseudoOutput has a Fanin Component. This
 * method adds a Component to the fanin of this PseudoOutput at the Position
 * given.
 * @param cmp Reference to the Component that shall be added to the fanin.
 * @param pos The position at which this Component shall be added. A
 * PseudoOutput has only one input (pos=1).
 * @exception DoubledFaninException if fanin is either not needed (pos>1) or
 * already there.
 */
void PseudoOutput::AddFanin (Component* cmp,unsigned int pos)
{
    // Check if there is no Element at the position
    if (pos<mRequiredFanin && mFanin[pos]->GetMType()==0)
    {
        //There is no Element at pos and pos is needed. So we add the Element
        //and return true.
        mFanin[pos]=cmp;
    } // End if (pos<mcRequiredFanin && mFanin[pos]==NULL)
    else
    {
        //Something went wrong the Element is already there or not needed
        std::string message="The fanin that shall be added is either already ";
        message+="there or there is no fanin needed at that position.";
        if (pos >= mRequiredFanin)
            throw DoubledFaninException(message, this, cmp, new Undef(), pos);
        else throw DoubledFaninException(message, this, cmp, mFanin[pos], pos);
    } //End else
    cmp->AddFanout(this);
} //End AddFanin (Component& cmp, int pos)



/**
 * Add a component to the fanout of this PseudoOutput.
 * A PseudoOutput has a PseudoInput in its Fanout. This method adds this
 * PseudoInput.
 * @param cmp a reference to the PseudoInput that shall be added.
 * @exception GeneralFanException is thrown if the fanout of a PseudoOutput is
 * not a PseudoInput
 */
void PseudoOutput::AddFanout (Component* cmp)
{
    //We can only add PseudoInputs to the fanout of PseudoOutputs
/*    if(cmp->GetMType()!=PS_INPUT && cmp->GetMType()!=OUTPUT)
    {
        std::string msg="EXCEPTION: Fanout Component of PseudoOutput is ";
        msg+="neither a PseudoInput nor an Output";
        std::vector<Component*> fanout;
        fanout.push_back(cmp);
        throw GeneralFanException(msg, this, fanout);
    }*/
    //In the fanout we don't care about the order of fanout components so we
   //just push the Component
    mFanout.push_back(cmp);
} // End AddFanout(Component& cmp)

/**
 * Check whether the PseudoOutput is complete.
 * @return True if the PseudoOutput has at least one fanout and one fanin
 * Component.
 */
bool PseudoOutput::IsComplete ()
{
    //We check for the output
    if (mFanout.size()>0 && mFanin.size()==mRequiredFanin) return true;
    //not complete
    else
    {
        std::cout<<GetType()<<" "<<GetMOutputName()<<" fanouts: "
                <<mFanout.size()<<" fanins: "<<mFanin.size()<<" required: "
                <<mRequiredFanin<<std::endl;
        return false;
    }
}

/**
 * Get a vector of the RtpLits defined by this component.
 * For this PseudoOutput these are equal to the lits of the inputs.
 * @param r the vector where the defined RtpLits will be stored in.
 */
void PseudoOutput::GetFanoutRtpLits(std::vector<satlib::RtpLit>& r)
{
    mFanin[0]->GetFanoutRtpLits(r);
}

/**
 * Set the reset properties of this PS_OUTPUT
 * @param reset The input used to reset the PS_OUTPUT
 * @param resVal The input delivering the value this PS_OUTPUT is reset to.
 */
void PseudoOutput::SetReset(FckId reset, FckId resVal)
{
    //std::cout<<GetMId()<<" got reset signal "<<reset<<" with value "<<resVal<<std::endl;
    mReset=reset;
    GetModule()->GetComponent(reset)->AddFanout(this);
    mResetVal=resVal;
    GetModule()->GetComponent(resVal)->AddFanout(this);
}

/**
 * Set the reset properties of this PS_OUTPUT
 * @param reset The input used to reset the PS_OUTPUT
 * @param resVal The input delivering the value this PS_OUTPUT is reset to.
 */
void PseudoOutput::SetClock(FckId clk)
{
    //std::cout<<GetMId()<<" got reset signal "<<reset<<" with value "<<resVal<<std::endl;
    mClock=clk;
}
