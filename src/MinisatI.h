
#ifndef MINISATI_H
#define MINISATI_H

#include <string>
#include "Solver.h"
#include "SatI.h"
#include<map>
namespace satlib{
class MinisatI:public SatI
{
public:

  // Constructors/Destructors
  //  
  

  /**
   * Empty Constructor
   */
  MinisatI (bool isBitLevel=false );

  /**
   * Empty Destructor
   */
  virtual ~MinisatI ( );
  virtual bool addClause (const std::vector<RtpLit>& clause, unsigned deleteId = 0 );
  virtual void deleteClause (unsigned deleteId );
  virtual bool solve (std::vector<unsigned> assumptionIds );
  virtual lValue getValue (RtpLit lit );
  virtual Status getStatus ( );


protected:


private:
  typedef std::map<Var,RtpLit> VarLitMap;
  typedef std::map<RtpLit,Var> LitVarMap;
  typedef std::map<unsigned,int> DelVarMap;
  Solver mSolver;
  VarLitMap mVarToLit; 
  LitVarMap mLitToVar;
  DelVarMap mDelVar;
  Status    mStatus;
};
}
#endif // MINISATI_H
