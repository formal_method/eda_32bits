/*! ************************************************************************
    \file rotateright.h
    \brief Header file of the Class RotateRight.

    The class contained in this file defines the attributes and methods of the
 *  Function RotateRight.
    \author C.Villarraga

    \copyright (c) University of Kaiserslautern 05.07.2011

Modification history:
 *

**************************************************************************/

#ifndef ROTATERIGHT_H_
#define ROTATERIGHT_H_
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/defaultexception.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class to describe the behaviour of the RotateRight Function.
 * This class implements the Function RotateRight derived from the class
 * Function.
 */
class RotateRight : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The RotateRight Function needs 2 Fanins. The first fanin is the data
     * input that shall be shifted. The second input is the amount of bits
     * that have to be shifted.
     */
    RotateRight(){SetMRequiredFanin(2);SetMType(ROR);SetMOutputName("");}

    /**
     * A constructor with a vector of fanin Components.
     * A RotateRight has two fanin Components so initialize mRequiredFanin with
     * 2. The first input is the data input to be shifted the second input
     * specifies the number of bits that have to be shifted. Add the fanin
     * components to the fanin of this RotateRight.
     * @param fanin A vector containing the two Fanin Components of the
     * RotateRight
     * @param fanout The string naming the output
     * @param id The ID of this RotateRight
     * @param module The Module the RotateRight shall be added to
     * @exception GeneralFanException is thrown if the size of the fanin does
     * not match mRequiredFanin
     */
    RotateRight(std::vector<Component*>& fanin, std::string fanout, int id,
               Module* module);

    /**
     * Destructor
     */
    virtual ~RotateRight(){}

    /**
     * Accessor Methods
     */
    /**
     * Return the type of this RotateRight as a string (ROR)
     * @return The string "ROR"
     */
    inline std::string GetType(){return "ROR";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();

    /**
     * Perform a simulation of this RotateRight for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );

    /**
     * Create the CNF for this RotateRight.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);


	void Create_BtorComp(Btor* btor){};
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */

    /**
     * Operations
     */
    /**
     * recursive method for CreateCnf
     */
    void makeRor ( satlib::SatI& satSolver, satlib::RtpLit sel,
                   std::vector<satlib::RtpLit>& res,
                   std::vector<satlib::RtpLit>& inp, unsigned rotateBy);
};

#endif /* ROTATERIGHT_H_ */
