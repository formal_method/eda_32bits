/*! ************************************************************************
    \file SatI.h 
    \brief Interface for SAT solvers
    \author Markus Wedler

    \copyright (c) University of Kaiserslautern Mi March 3rd 2006

This file was generated on Mi Jan 25 2006 at 16:36:41
The original location of this file is abllib

Modification history:
22.03.06 Sacha Loitz changed the values of the enum constants in lValue for
 * easier simulation.
 * 26.10.06 site of declaration for RtpLit is changed, definition of
 * operator- for RtpLit is moved into SatI.cpp
 * 30.01.2011 C.Villarraga - added the deleteId to missing methods,
   so it is possible to delete the clauses of any component (AND, OR, XOR, NOT, ...)
**************************************************************************/

#ifndef SATI_H
#define SATI_H

#include <string>
#include <vector>

namespace satlib {

	typedef std::pair<unsigned,int> RtpLit;
	//RtpLit operator - (RtpLit p){ return RtpLit(p.first,p.second);}
	//RtpLit operator - (RtpLit p);

	class PolySatI{
	  public:
	  virtual bool makeIte(RtpLit s,RtpLit a,RtpLit b, RtpLit r){return false;}
	  virtual bool makeBuffer(RtpLit a,RtpLit b){return false;}
	  virtual ~PolySatI(){}

	};

	class SatI:public PolySatI
	{
		public:

		  enum Status {SAT, UNSAT, UNKNOWN};
		  enum lValue {FALSE=0,TRUE=1,DONTCARE=5};
		  //typedef std::pair<unsigned,int> RtpLit;
		  //friend RtpLit operator - (RtpLit p) { return RtpLit(p.first,-p.second);}
		  friend RtpLit operator - (RtpLit p);
		  //!\brief add Clause to satsolver if deleteId is set mark clause for deletion
		  virtual bool addClause (const std::vector<RtpLit>& clause, unsigned deleteId=0)=0;
		  //!\brief delete clauses marked with deleteId
		  virtual void deleteClause (unsigned deleteId)=0;
		  //!\brief solve including clauses marked with deleteIds in vector assumptionIds
		  virtual bool solve (std::vector<unsigned> assumptionIds)=0;
		  //!\brief getValue of a literal
		  virtual lValue getValue (RtpLit lit)=0;
		  //!\brief get Status of the solver
		  virtual Status getStatus ()=0;
		  virtual ~SatI(){};
		  //!\brief fresh id for deletion marking
		  unsigned getNextTempVar() {mtempVar++; return mtempVar;}
		  //!\brief create clauses for r= ab
		  bool makeAnd(RtpLit a,RtpLit b, RtpLit r,unsigned deleteId=0);
		  //!\brief create clauses for r= inputs[0]inputs[1]...
		  bool makeAnd(std::vector<RtpLit>& inputs, RtpLit r,unsigned deleteId=0);
		   //!\brief create clauses for r= a+b
		  bool makeOr(RtpLit a,RtpLit b, RtpLit r,unsigned deleteId=0);
		  //!\brief create clauses for r= inputs[0]+inputs[1]+...
		  bool makeOr(std::vector<RtpLit>& inputs, RtpLit r,unsigned deleteId=0);
		  //!\brief a=~b
		  bool makeNot(RtpLit a,RtpLit b,unsigned deleteId=0);
		  //!\brief a=b
		  bool makeBuffer(RtpLit a,RtpLit b,unsigned deleteId=0);
		  //!\brief makeOr(a,b,-r)
		  bool makeNOr(RtpLit a,RtpLit b, RtpLit r,unsigned deleteId=0)
		  	  	  	  {return makeOr(a,b,RtpLit(r.first,-r.second), deleteId);}
		  //!\brief makeOr(inputs,-r)
		  bool makeNOr(std::vector<RtpLit>& inputs, RtpLit r,unsigned deleteId=0)
		  	  	  	  {return makeOr(inputs,RtpLit(r.first,-r.second), deleteId);}
		  //!\brief r=a xor b
		  bool makeXor(RtpLit a,RtpLit b,RtpLit r,unsigned deleteId=0);
		  //!\brief forces even number of literals to be one
		  bool makeEvenParity(std::vector<RtpLit>& cl,unsigned deleteId=0);
		  //!\brief r= ite(s,a,b)
		  bool makeIte(RtpLit s,RtpLit a,RtpLit b, RtpLit r,unsigned deleteId=0);
		  //!\brief (co,r)=a+b
		  bool makeHA(RtpLit a,RtpLit b,RtpLit r,RtpLit co,unsigned deleteId=0);
		  //!\brief (co,r)=a+b+ci
		  bool makeFA(RtpLit a,RtpLit b,RtpLit ci,RtpLit r,RtpLit co,unsigned deleteId=0);
		  //!\brief r=1 <=> majority of inputs evaluate to 1
		  bool makeMajority(std::vector<RtpLit>& inputs,RtpLit r,unsigned deleteId=0);

		protected:


		private:

		  bool makeEvenParity(std::vector<RtpLit>& cl,int depth
				  ,bool flipLast,unsigned deleteId=0);
		  bool makeMajority(std::vector<RtpLit>& inputs,RtpLit r,std::vector<RtpLit>& cl
				  ,unsigned startWith,unsigned deleteId=0);
		  unsigned mtempVar;
		  //! Flag to denote the specialcase that the Solver is used
		  //! with RtpLits (x,1,x,-1)only.
		  bool mIsBitLevel;

		public:

		  SatI(bool isBitLevel=false) {mtempVar=0;mIsBitLevel=isBitLevel;}

	}; // end class SatI

} // end namespace satlib

#endif // SATI_H
