/*! ************************************************************************
    \file timeframeexpansion.h
    \brief Headerfile Class TimeFrameExpansion describing the behavior of this

    Contains the circuit that shall be checked assumptions and properties are
 * used to build a new circuit with assumption property and the transitive
 * fanin of these
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Mon Apr 07 2006

Modification history:
10/9/2014: them ham Solve_Btor()
**************************************************************************/

#ifndef TIMEFRAMEEXPANSION_H
#define TIMEFRAMEEXPANSION_H
#include "circuit.h"
#include "MinisatI.h"
#include <map>
#include <sstream>
#include "PslProperty.h"
#include <string.h>

/**
 * Class TimeFrameExpansion describing the methods needed to check properties
 * on a circuit. Therefore a new circuit out of the property and the
 * transitive fanin of this is constructed
 */



class TimeFrameExpansion : public Module
{
    /**
     * Public stuff
     */
    public:
        TimeFrameExpansion();
        TimeFrameExpansion(const Circuit* circuit, Module*  prop, std::map<FckId, std::pair<FckId, int> > TimeFrameMap);
        TimeFrameExpansion(const Circuit* circuit, PslProperty*  prop);
	
        /**
         * Operations
         */
        /**
         * Prepare the solver (build the CNF) and solve the problem
         * @param satSolver Is used to return the solver e.g. to analyze the
         * cause of a failing property
         * @return true if there is a solution for the constructed circuit
         * =>PROPERTY FAILS. False if there is no solution => PROPERTY HOLDS
         */
        bool Solve_MiniSat(satlib::MinisatI& satSolver);
        /**
         * solver voi Boolector
         */
        bool Solve_Btor(Btor* btor);
        /**
         * Print the solution returned from MiniSat
         */
        void PrintSolution_Btor(Btor* btor, std::string module_name, std::string vcdname);
        /**
         * Print the solution returned from MiniSat
         */
        void PrintSolution_MiniSat(satlib::SatI& satSolver, std::string module_name, std::string vcdname);
        /**
         * Create a CNF representation for the TimeFrameExpansion.
         * @param satSolver The solver the CNF shall be created for
         */
        void CreateCnf(satlib::SatI& satSolver);
        int GetmExpandedTimeFrames(){return mExpandedTimeFrames;}


    private:
        /**
         * Fields
         */

        void Init(const Circuit* circuit, Module*  prop, std::map<FckId, std::pair<FckId, int> > TimeFrameMap);

        std::map<std::pair<FckId, int>, FckId > mTimeFrameMap;

        std::pair<FckId,int> TimeFrameIdToCircuitId(FckId id);

        FckId CircuitIdToTimeFrameId(std::pair<FckId,int> id);

        FckId CircuitIdToTimeFrameId(FckId id,int tf);

        Module* mModuleToExpand;

        const Circuit* mCircuitToExpand;

        std::multimap<FckId, std::pair<FckId,int> > mPropFanin;

        int mExpandedTimeFrames;

        Component* mPropOut;

        std::map<FckId, FckId> mJumpMap;

        std::map<FckId, Component*> mModuleJumpMap;

        std::map<FckId, std::pair<FckId, FckId> > mResetMap;

        std::map<FckId, FckId> mPropMap;

        FckId AddComponentTimeFrame(Component* cmp, int timeFrame);

        std::string IntToStr(int number);
};
#endif
