/*! ************************************************************************
    \file slice.h
    \brief Header file of the Class Slice.

    The class contained in this file defines the attributes and methods of the
 *  Function Slice.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Mon Feb 13 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef SLICE_H
#define SLICE_H
#include <string>
#include "function.h"
#include "constinput.h"
#include <vector>
#include "../exceptions/generalfanexception.h"
#include "../exceptions/defaultexception.h"
#include "../exceptions/integerinputexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class to describe the behaviour of the Slice Function.
 * This class implements the Function Slice derived from the class
 * Function.
 */
class Slice : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The Slice Function needs 3 Fanins. The first fanin is the data 
     * input that shall be sliced. The second input gives the starting point 
     * of the slice operation and the third is the amount of bits the output 
     * shall have.
     */
    Slice(){SetMRequiredFanin(3);SetMType(SLICE);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * A Slice has three fanin Components so initialize mRequiredFanin with 3. 
     * The first input is the data input to be sliced the second input specifies 
     * the starting position and the third input the number of bits the output has.
     * Add the fanin components to the fanin of this Slice.
     * @param fanin A vector containing the three Fanin Components of the Slice
     * @param fanout The string naming the output
     * @param id the ID of the component
     * @param module The Module the Slice shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     */
    Slice(std::vector<Component*>& fanin, std::string fanout, int id, 
          Module* module);
    
    /**
     * Destructor
     */
    virtual ~Slice(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this Slice as a string (SLICE)
     * @return the string "SLICE"
     */
    inline std::string GetType(){return "SLICE";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();
    
    /**
     * perform a simulation of this component for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );    
    
    /**
     * create the CNF for this component.
     * Do nothing as we do not need a CNF representation for a Slice.
     * This is handled by passing the rtp lits through from the inputs to the 
     * outputs.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor);
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */


    /**
     * Operations
     */
    /**
     * recursive method for CreateCnf
     */
    void makeSlice(satlib::SatI& satSolver,
                   std::vector<satlib::RtpLit>& a,
                   std::vector<satlib::RtpLit>& b,
                   std::vector<satlib::RtpLit>& r,
                   unsigned start, int bIdx, int& extraBits);
};
#endif //SLICE_H

