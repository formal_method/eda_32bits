/*! ************************************************************************
    \file moduleoutput.cpp 
    \brief Implementation of class ModuleOutput
    
    Implementation of the methods of the class ModuleOutput
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Thu March 02 2006

Modification history:
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout

**************************************************************************/

#include "moduleoutput.h"
#include "module.h"
#include "circuit.h"

/**
 * Constructors/Destructors
 */
 /**
  * A constructor with a vector of fanin Components.
  * A ModuleOutput has one fanin Component which is always a ModuleInput.
  * Add the fanin Component to the fanin of this ModuleOutput.
  * @param fanin A vector containing the Fanin Component of the ModuleOutput
  * @param module The string naming the Module
  * @param id The ID of the ModuleOutput
  */
ModuleOutput::ModuleOutput(std::vector<Component*>& fanin, 
                                    ModuleId moduleId, 
                                    std::string output,int id, Module* module)
{
    mModule=module;
    //Determine the module that shall be included
    mInclModule = mModule->GetParentCircuit()->GetModule(moduleId);
    //Set the number of required inputs
    SetMRequiredFanin(1);
    SetMType(M_OUTPUT);
    SetMOutputName(output);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have a fanin Component so lets add this to our new ModuleOutput.
    //First of all we allocate space for the Vector mFanin with undefined
    //fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //We use a loop Iterating from i=0 to mRequiredFanin-1 and add
    //the fanin at position i to position i. 
    std::string connectedOut=output.substr(0,output.find("@"));
    mFanoutWidth = mInclModule->GetOutputWidth(connectedOut);
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //set the new fanout width
        //mFanoutWidth+=fanin[i]->GetMFanoutWidth();
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mcRequiredFanin;i++)
    delete pUndef;
} //End ModuleOutput(vector<string>&, string, int, Module*)

/**
 * Methods
 */
/**
 * Add a Component to the fanin of this ModuleOutput.
 * A ModuleInterface has one Fanin Component.
 * @param cmp Reference to the Component that shall be added to the fanin.
 * @param pos The position at which this Component shall be added. 
 * @exception DoubledFaninException if this module already has a fanin 
 * Component at the position specified 
 * 
 * Increments the value of mKnownFanin.
 */
void ModuleOutput::AddFanin(Component* cmp,unsigned int pos)
{
    // Check if there is no Element at the position
    if (pos<mRequiredFanin && mFanin[pos]->GetMType()==0)
    {
        //There is no Element at pos and pos is needed. So we add the Element
        //and return true.    
        mFanin[pos]=cmp;
        //Get the Width of the fanout and set the fanin to the same width
        //if necessary resize
        if (mFaninWidth.size()<pos+1) mFaninWidth.resize(pos+1,0);
        mFaninWidth[pos]=cmp->GetMFanoutWidth();
    } // End if (pos<mcRequiredFanin && mFanin[pos]==NULL)
    else
    {
        //Something went wrong the Element is already there or not needed
        std::string message="EXCEPTION: The fanin that shall be added is ";
        message+="either already there or there is no fanin needed at that ";
        message+="position.";
        if (pos >= mRequiredFanin) 
            throw DoubledFaninException(message, this, cmp, new Undef(), pos);
        else throw DoubledFaninException(message, this, cmp, mFanin[pos], pos);
    } //End else
}

/**
 * Update the value of mInput
 * Fetch the current assignment of the output of the fanin Component and
 * update mInput of this Output accordingly.
 */
void ModuleOutput::UpdateInputs()
{
    mOutput.clear();
    //For each fanin Component get its output values and assign them to the
    //corresponding input
    //mOutput is assigned the output value of the Fanin
    mOutput=mFanin[0]->GetOutput();
} // End UpdateInputs

/**
 * Check whether the Function is complete.
 * @return True if the Function has mRequiredFanin fanin Components and
 * at least one fanout Component.
 */
bool ModuleOutput::IsComplete () 
{
    //We check for the output first and the input second, as all inputs should
    //be there after a successful creation
    if (mFanout.size()==0) return false;
    if (mFanin.size()==mRequiredFanin) return true;
    //not complete
    else return false;
}

/**
 * Return the Components in the fanout of this Component.
 * @return The Components in the fanout of this ModuleOutput.
 */
std::vector<Component*> ModuleOutput::GetFanout() 
{
    //return the vector
    return mFanout;
    
}

/**
 * Get an Output Component from the Module included by this ModuleOutput
 * @return reference to the Output Component of the Module
 */
Component* ModuleOutput::GetModuleOutput()
{
    std::string output = GetMOutputName();
    int end = output.find("@");
    std::string outName = output.substr(0,end);
    return mInclModule->GetComponent(outName);
}
