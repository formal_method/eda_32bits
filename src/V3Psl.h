// -*- mode: C++; c-file-style: "cc-mode" -*-
//*************************************************************************
// DESCRIPTION: Verilator: traverse node
//
//
//*************************************************************************
#ifndef _V3PSL_H_
#define _V3PSL_H_
#include "config_build.h"
#include "verilatedos.h"
#include "V3Error.h"
#include "V3Ast.h"
#include "V3Global.h"
#include "../circuit/circuit.h"
#include "../circuit/module.h"
#include <string.h>



//============================================================================
using namespace std;


class V3Psl {
public:
	void psl(Circuit* cir);
};


#endif // Guard
