/*! ************************************************************************
    \file write.h 
    \brief Header file of the Class Write.

    The class contained in this file defines the attributes and methods of the
 *  Function Write.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Mon Feb 13 2006 

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef WRITE_H
#define WRITE_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/defaultexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class to describe the behaviour of the Write Function.
 * This class implements the Function Write derived from the class
 * Function.
 */
class Write : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The Write Function needs 3 Fanins. The First Fanin is the data input. 
     * The second one the position at which a bit shall be inserted. The third
     * input specifies the bit that is to be inserted.
     */
    Write(){SetMRequiredFanin(3);SetMType(WRITE);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * A Write has three fanin Components so initialize mRequiredFanin with 3. The 
     * input at pos=0 is the data input a bit shall be inserted into. The input at
     * pos=0 specifies the position at which the bit shall be inserted.
     * Add the fanin components to the fanin of this Write.
     * @param fanin A vector containing the two Fanin Components of a Write.
     * @param fanout The string naming the output
     * @param id The ID of the Write
     * @param module The Module the Write shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     */
    Write(std::vector<Component*>& fanin, std::string fanout, int id, 
          Module* module);
    
    /**
     * Destructor
     */
    virtual ~Write(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this component as a string (WRITE)
     * @return the string "WRITE"
     */
    inline std::string GetType(){return "WRITE";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();
        
    /**
     * Perform a simulation of this Write for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );
    
    /**
     * Create the CNF for this Write.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor){};
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
        
    
    /**
     * Operations
     */
    /**
     * recursive method to build a CNF representation for the write
     */
    void makeWrite ( satlib::SatI& satSolver,
                     std::vector<satlib::RtpLit>& a,
                     std::vector<satlib::RtpLit>& b,
                     std::vector<satlib::RtpLit>& c,
                     std::vector<satlib::RtpLit>& r,
                     unsigned start, unsigned stop, int bIdx, int& extraBits ); 
};
#endif //WRITE_H

