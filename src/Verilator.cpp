// -*- mode: C++; c-file-style: "cc-mode" -*-
//*************************************************************************
// DESCRIPTION: Verilator: main()
//
// Code available from: http://www.veripool.org/verilator
//
//*************************************************************************
//
// Copyright 2003-2012 by Wilson Snyder.  This program is free software; you can
// redistribute it and/or modify it under the terms of either the GNU
// Lesser General Public License Version 3 or the Perl Artistic License
// Version 2.0.
//
// Verilator is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
//*************************************************************************

#include "V3Global.h"
#include "V3Ast.h"
#include <time.h>
#include <ctime>
#include <sys/stat.h>
#include <sys/resource.h>
#include <sys/sysinfo.h>
#include <cstring>

#include "V3Active.h"
#include "V3ActiveTop.h"
#include "V3Assert.h"
#include "V3AssertPre.h"
#include "V3Begin.h"
#include "V3Branch.h"
#include "V3Case.h"
#include "V3Cast.h"
#include "V3Changed.h"
#include "V3Clean.h"
#include "V3ClkGater.h"
#include "V3Clock.h"
#include "V3Combine.h"
#include "V3Const.h"
#include "V3Coverage.h"
#include "V3CoverageJoin.h"
#include "V3Dead.h"
#include "V3Delayed.h"
#include "V3Depth.h"
#include "V3DepthBlock.h"
#include "V3Descope.h"
#include "V3EmitC.h"
#include "V3EmitMk.h"
#include "V3EmitV.h"
#include "V3EmitXml.h"
#include "V3Expand.h"
#include "V3File.h"
#include "V3Cdc.h"
#include "V3Gate.h"
#include "V3GenClk.h"
#include "V3Graph.h"
#include "V3Inline.h"
#include "V3Inst.h"
#include "V3Life.h"
#include "V3LifePost.h"
#include "V3LinkCells.h"
#include "V3LinkDot.h"
#include "V3LinkJump.h"
#include "V3LinkLValue.h"
#include "V3LinkLevel.h"
#include "V3LinkParse.h"
#include "V3LinkResolve.h"
#include "V3Localize.h"
#include "V3Name.h"
#include "V3Order.h"
#include "V3Param.h"
#include "V3Parse.h"
#include "V3ParseSym.h"
#include "V3PreShell.h"
#include "V3Premit.h"
#include "V3Scope.h"
#include "V3Slice.h"
#include "V3Split.h"
#include "V3SplitAs.h"
#include "V3Stats.h"
#include "V3Subst.h"
#include "V3Table.h"
#include "V3Task.h"
#include "V3Trace.h"
#include "V3TraceDecl.h"
#include "V3Tristate.h"
#include "V3Undriven.h"
#include "V3Unknown.h"
#include "V3Unroll.h"
#include "V3Width.h"
#include "V3NodeTraversal.h"
#include "V3MakeNode.h"
//#include "V3Psl.h"
#include "../lib/pslparser/PSL_Parser.h"
#include "../lib/pslparser/PSL_Exception.h"
#include "PslToFck.h"
#include "PslProperty.h"
#include "circuit.h"
#include "timeframeexpansion.h"
#include "../lib/pslparser/PslFactoryI.h"

#ifdef __cplusplus
extern "C"{
#include "../lib/boolector-1.6.0-with-sat-solvers/boolector/boolector.h"
}
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <fstream>

using namespace std;

#define BV1_EXAMPLE_NUM_BITS 8
#define WRONG_COMMAND 0
#define READ_LINK 1
#define READ_RTL 2
#define READ_PROP 3
#define SET_SOLVER 4
#define VERIFY_PROP 5
#define PRINT_CIR 6
#define PRINT_PROP 7
#define PRINT_IPC 8
#define EXIT 9

V3Global v3Global;

//######################################################################
// V3 Class -- top level

AstNetlist* V3Global::makeNetlist() {
    AstNetlist* newp = new AstNetlist();
    newp->addTypeTablep(new AstTypeTable(newp->fileline()));
    return newp;
}

void V3Global::checkTree() { rootp()->checkTree(); }

void V3Global::clear() {
    if (m_rootp) m_rootp->deleteTree(); m_rootp=NULL;
    if (m_circuitp) delete m_circuitp;
    //if (m_modulep) delete m_modulep;
}

void V3Global::readFiles(std::vector<int>& vlineOfCode, std::vector<std::string>& vfileName) {
    // NODE STATE
    //   AstNode::user4p()	// VSymEnt*    Package and typedef symbol names
    AstUser4InUse	inuser4;

    V3InFilter filter (v3Global.opt.pipeFilter());
    V3ParseSym parseSyms (v3Global.rootp());  // Symbol table must be common across all parsing

    V3Parse parser (v3Global.rootp(), &filter, &parseSyms);
    // Read top module


	// Quandvk54, 21-01-14
	// tao log file chua nhung thong tin sau: file name, lines of code
//	std::ofstream logFile("logFile",std::ios::out);
	//int lineOfCode;
    for (V3StringList::const_iterator it = v3Global.opt.vFiles().begin();
	 it != v3Global.opt.vFiles().end(); ++it) {
	string filename = *it;
	parser.parseFile(new FileLine("COMMAND_LINE",0), filename, false,
			 "Cannot find file containing module: ",vlineOfCode, vfileName);

		// write into log file
//		logFile<<"File "<<filename<<" co so dong la: "<<lineOfCode-2<<std::endl;
    }

//    logFile.close();

    // Read libraries
    // To be compatible with other simulators,
    // this needs to be done after the top file is read
    for (V3StringSet::const_iterator it = v3Global.opt.libraryFiles().begin();
	 it != v3Global.opt.libraryFiles().end(); ++it) {
	string filename = *it;
	parser.parseFile(new FileLine("COMMAND_LINE",0), filename, true,
			 "Cannot find file containing library module: ",vlineOfCode,vfileName);
    }
    //v3Global.rootp()->dumpTreeFile(v3Global.debugFilename("parse.tree"));
    //V3Error::abortIfErrors();
    if ( V3Error::errorCount()) {
    	std::cout<<"ERROR: the path of file is invalid and the file cannot be opened \n";
    	return;
    }


    if (!v3Global.opt.preprocOnly()) {
	// Resolve all modules cells refer to
	V3LinkCells::link(v3Global.rootp(), &filter, &parseSyms);
    }
}

void V3Global::dumpGlobalTree(const string& filename, int newNumber) {
    v3Global.rootp()->dumpTreeFile(v3Global.debugFilename(filename, newNumber));
}

//######################################################################

void process () {
    bool dumpMore = (v3Global.opt.dumpTree() >= 9);
    // Sort modules by level so later algorithms don't need to care
    V3LinkLevel::modSortByLevel();
    V3Global::dumpGlobalTree("cells.tree");
    V3Error::abortIfErrors();


    

    // Convert parseref's to varrefs, and other directly post parsing fixups
    V3LinkParse::linkParse(v3Global.rootp());
    if (dumpMore) V3Global::dumpGlobalTree("linkparse.tree");
    // Cross-link signal names
    // Cross-link dotted hierarchical references
    V3LinkDot::linkDotPrimary(v3Global.rootp());
    if (dumpMore) V3Global::dumpGlobalTree("linkdot.tree");
    v3Global.checkTree();  // Force a check, as link is most likely place for problems
    // Correct state we couldn't know at parse time, repair SEL's
    V3LinkResolve::linkResolve(v3Global.rootp());
    if (dumpMore) V3Global::dumpGlobalTree("linkresolve.tree");
    // Set Lvalue's in variable refs
    V3LinkLValue::linkLValue(v3Global.rootp());
    if (dumpMore) V3Global::dumpGlobalTree("linklvalue.tree");
    // Convert return/continue/disable to jumps
    V3LinkJump::linkJump(v3Global.rootp());
    V3Global::dumpGlobalTree("link.tree");
    V3Error::abortIfErrors();

    if (v3Global.opt.stats()) V3Stats::statsStageAll(v3Global.rootp(), "Link");

    // Remove parameters by cloning modules to de-parameterized versions
    //   This requires some width calculations and constant propagation
    V3Param::param(v3Global.rootp());
    if (dumpMore) V3Global::dumpGlobalTree("param.tree");
    V3LinkDot::linkDotParamed(v3Global.rootp());	// Cleanup as made new modules
    V3Global::dumpGlobalTree("paramlink.tree");
    V3Error::abortIfErrors();

    // Remove any modules that were parameterized and are no longer referenced.
    V3Dead::deadifyModules(v3Global.rootp());
    if (dumpMore) V3Global::dumpGlobalTree("dead.tree");
    v3Global.checkTree();

    // Calculate and check widths, edit tree to TRUNC/EXTRACT any width mismatches
    V3Width::width(v3Global.rootp());
    V3Global::dumpGlobalTree("width.tree");

    V3Error::abortIfErrors();

    // Commit to the widths we've chosen; Make widthMin==width
    V3Width::widthCommit(v3Global.rootp());
    v3Global.assertDTypesResolved(true);
    v3Global.assertWidthsMatch(true);
    if (dumpMore) V3Global::dumpGlobalTree("widthcommit.tree");

    // Coverage insertion
    //    Before we do dead code elimination and inlining, or we'll lose it.
    if (v3Global.opt.coverage()) {
	V3Coverage::coverage(v3Global.rootp());
	V3Global::dumpGlobalTree("coverage.tree");
    }

    // Push constants, but only true constants preserving liveness
    // so V3Undriven sees variables to be eliminated, ie "if (0 && foo) ..."
    V3Const::constifyAllLive(v3Global.rootp());
    V3Global::dumpGlobalTree("const.tree");

    // Signal based lint checks, no change to structures
    // Must be before first constification pass drops dead code
    V3Undriven::undrivenAll(v3Global.rootp());

    // Assertion insertion
    //    After we've added block coverage, but before other nasty transforms
    V3AssertPre::assertPreAll(v3Global.rootp());
    V3Global::dumpGlobalTree("assertpre.tree");
    //
    V3Assert::assertAll(v3Global.rootp());
    V3Global::dumpGlobalTree("assert.tree");

    if (!v3Global.opt.xmlOnly()) {
	// Add top level wrapper with instance pointing to old top
	// Move packages to under new top
	// Must do this after we know the width of any parameters
	// We also do it after coverage/assertion insertion so we don't 'cover' the top level.
	V3LinkLevel::wrapTop(v3Global.rootp());
    }

    // Propagate constants into expressions
    V3Const::constifyAllLint(v3Global.rootp());
    V3Global::dumpGlobalTree("const.tree");

    if (!v3Global.opt.xmlOnly()) {
	// Remove cell arrays (must be between V3Width and scoping)
	V3Inst::dearrayAll(v3Global.rootp());
	if (dumpMore) V3Global::dumpGlobalTree("dearray.tree");
    }

    if (!v3Global.opt.xmlOnly()) {
	// Expand inouts, stage 2
	// Also simplify pin connections to always be AssignWs in prep for V3Unknown
	V3Tristate::tristateAll(v3Global.rootp());
	V3Global::dumpGlobalTree("tristate.tree");

	// Task inlining & pushing BEGINs names to variables/cells
	// Begin processing must be after Param, before module inlining
	V3Begin::debeginAll(v3Global.rootp());	// Flatten cell names, before inliner
	V3Global::dumpGlobalTree("begin.tree");

	// Move assignments from X into MODULE temps.
	// (Before flattening, so each new X variable is shared between all scopes of that module.)
	V3Unknown::unknownAll(v3Global.rootp());
	V3Global::dumpGlobalTree("unknown.tree");

	// Module inlining
	// Cannot remove dead variables after this, as alias information for final
	// V3Scope's V3LinkDot is in the AstVar.
	if (v3Global.opt.oInline()) {
	    V3Inline::inlineAll(v3Global.rootp());
	    V3Global::dumpGlobalTree("inline.tree");
	    V3LinkDot::linkDotArrayed(v3Global.rootp());	// Cleanup as made new modules
	    if (dumpMore) V3Global::dumpGlobalTree("linkdot.tree");
	}
    }

    //--PRE-FLAT OPTIMIZATIONS------------------

    // Initial const/dead to reduce work for ordering code
    V3Const::constifyAll(v3Global.rootp());
    if (dumpMore) V3Global::dumpGlobalTree("const_predead.tree");
    v3Global.checkTree();

    V3Dead::deadifyDTypes(v3Global.rootp());
    V3Global::dumpGlobalTree("const.tree");
    v3Global.checkTree();

    V3Error::abortIfErrors();

    //--FLATTENING---------------

    if (!v3Global.opt.xmlOnly()) {
	// We're going to flatten the hierarchy, so as many optimizations that
	// can be done as possible should be before this....

	// Convert instantiations to wassigns and always blocks
	V3Inst::instAll(v3Global.rootp());
	V3Global::dumpGlobalTree("inst.tree");

	// Inst may have made lots of concats; fix them
	V3Const::constifyAll(v3Global.rootp());
	V3Global::dumpGlobalTree("const.tree");

	// Flatten hierarchy, creating a SCOPE for each module's usage as a cell
	V3Scope::scopeAll(v3Global.rootp());
	V3Global::dumpGlobalTree("scope.tree");
	V3LinkDot::linkDotScope(v3Global.rootp());
	V3Global::dumpGlobalTree("linkdot.tree");
    }

    

    //--SCOPE BASED OPTIMIZATIONS--------------

    if (!v3Global.opt.xmlOnly()) {
	// Cleanup
	V3Const::constifyAll(v3Global.rootp());
	if (dumpMore) V3Global::dumpGlobalTree("const_predead.tree");
	V3Dead::deadifyDTypes(v3Global.rootp());
	v3Global.checkTree();
	V3Global::dumpGlobalTree("const.tree");

	// Inline all tasks
	V3Task::taskAll(v3Global.rootp());
	V3Global::dumpGlobalTree("task.tree");

	// Add __PVT's
	// After V3Task so task internal variables will get renamed
	V3Name::nameAll(v3Global.rootp());
	if (dumpMore) V3Global::dumpGlobalTree("name.tree");

	// Loop unrolling & convert FORs to WHILEs
	V3Unroll::unrollAll(v3Global.rootp());
	V3Global::dumpGlobalTree("unroll.tree");

	// Expand slices of arrays
	V3Slice::sliceAll(v3Global.rootp());
	V3Global::dumpGlobalTree("slices.tree");

	// Convert case statements to if() blocks.  Must be after V3Unknown
	V3Case::caseAll(v3Global.rootp());
	V3Global::dumpGlobalTree("case.tree");

	// Push constants across variables and remove redundant assignments
	V3Const::constifyAll(v3Global.rootp());
	V3Global::dumpGlobalTree("const.tree");

	if (v3Global.opt.oLife()) {
	    V3Life::lifeAll(v3Global.rootp());
	    V3Global::dumpGlobalTree("life.tree");
	}

	// Make large low-fanin logic blocks into lookup tables
	// This should probably be done much later, once we have common logic elimination.
	/*if (!v3Global.opt.lintOnly() && v3Global.opt.oTable()) {
	    V3Table::tableAll(v3Global.rootp());
	    V3Global::dumpGlobalTree("table.tree");  //edited 16/1/2014 xoa ham nay
	}*/

	// Cleanup
	V3Const::constifyAll(v3Global.rootp());
	if (dumpMore) V3Global::dumpGlobalTree("const_predead.tree");
	V3Dead::deadifyDTypes(v3Global.rootp());
	v3Global.checkTree();
	V3Global::dumpGlobalTree("const.tree");

	// Detect clock enables and mode into sensitives, and split always based on clocks
	// (so this is a good prelude to splitAlways.)
	if (v3Global.opt.oFlopGater()) {
	    V3ClkGater::clkGaterAll(v3Global.rootp());
	    V3Global::dumpGlobalTree("clkgater.tree");
	}

	// Move assignments/sensitives into a SBLOCK for each unique sensitivity list
	// (May convert some ALWAYS to combo blocks, so should be before V3Gate step.)
	V3Active::activeAll(v3Global.rootp());
	V3Global::dumpGlobalTree("active.tree");

	// Split single ALWAYS blocks into multiple blocks for better ordering chances
	if (v3Global.opt.oSplit()) {
	    V3Split::splitAlwaysAll(v3Global.rootp());
	    if (dumpMore) V3Global::dumpGlobalTree("split.tree");
	}
	V3SplitAs::splitAsAll(v3Global.rootp());
	V3Global::dumpGlobalTree("splitas.tree");

	// Tao file .dot
//	V3MakeNode::makeNode(v3Global.rootp());

	V3NodeTraversal::traverseNode(v3Global.rootp());
    }
}


int decode_command(string command, string* path){
	int id_cmd = 0; //default
	string request = "";
	size_t found = command.find(" - ");
	if (found!=string::npos) {
	    request = command.substr(0,found);
	    //cout<<request<< "     ";
	    *path = command.substr(found+3);
	    //cout<<*path<<"\n";
	}
	if (request == "read project") id_cmd = READ_LINK;
	else if (request == "read rtl") id_cmd = READ_RTL;
	else if (request == "read property") id_cmd = READ_PROP;
	else if (request == "set solver") id_cmd = SET_SOLVER;
	else if (command == "verify property") id_cmd = VERIFY_PROP;
	else if (command == "print circuit") id_cmd = PRINT_CIR;
	else if (command == "print property") id_cmd = PRINT_PROP;
	else if (command == "print ipc") id_cmd = PRINT_IPC;
	else if (command == "exit") id_cmd = EXIT;
	else id_cmd = WRONG_COMMAND;

	return id_cmd;
}



//######################################################################

int main(int argc, char** argv, char** env) {
	// Quandvk54, 21-01-14
	// tao log file chua nhung thong tin sau: file name, lines of code
	std::vector<int> vlineOfCode;
	std::vector<std::string> vfileName;
	float synthesizing_duration, verifying_duration;
	float synthesizing_mem_usage, verifying_mem_usage, total_mem_usage;
	//struct rusage r_usage;
	clock_t t;
	// General initialization
    ios::sync_with_stdio();

    time_t randseed;
    time(&randseed);
    srand( (int) randseed);

    // Post-constructor initialization of netlists
    //v3Global.boot();

    // Preprocessor
    // Before command parsing so we can handle -Ds on command line.
    V3PreShell::boot(env);

    string command = "";
    bool exit_flag = 0;
	string Solver = "";
	string link_of_project = "";
	Circuit* cir=NULL;
	std::vector<PslProperty*> properties;
	TimeFrameExpansion* TFE=NULL;

    while (!exit_flag) {
    	cout<<">> ";
    	getline(cin, command);
    	string path;
    	int id_cmd = decode_command(command, &path);

        string argString;
    	//thuc hien cac cong viec doi voi moi lenh
    	switch (id_cmd) {
    	case READ_LINK:
    		if (path=="") std::cout<<"ERROR: Missing paths\n";
    		else link_of_project = path; break;
    	case READ_RTL: {    // Synthesic
    		if (path=="") std::cout<<"ERROR: Missing paths\n";
    		else {
    			properties.clear();
    			//chon Verilog compiler
    			v3Global.set_compiler(0);
    			V3Global *glo = new V3Global();
    			v3Global = *glo;
    		    v3Global.boot();
    			V3Options *op = new V3Options();
    			v3Global.opt = *op;
    			struct rusage r_usage;
    			t = clock();
				/* Options for Verilator's input, is defined as vector of strings */
				std::vector<std::string> vstr_opts;

				/* Open input file that contains all of Verilog and PSL files */
				path.insert(0,link_of_project);
				std::ifstream script_file(path.c_str());

				/* Contain path to Verilog and PSL files */
				std::string verilog_path = "";

				if( !script_file.is_open() ) /* File not open */
				{
					cout<<"ERROR: the path of file is invalid and the file cannot be opened "<< path <<", exit!\n"; //exit(0);
				}
				else	/* File opened */
				{
					std::cout<< path << " opened; starting to parse file."<< std::endl;
					/* Get all of line of script file, until end of file */
					vstr_opts.push_back("-cc");
					while ( !script_file.eof() )
					{
						/* Get line in ifstream */
						getline( script_file,verilog_path );

						if ("" != verilog_path) /* If path to Verilog or PSL file is not empty */
						{
							vstr_opts.push_back(verilog_path);
						}
					}
					script_file.close();
				} /* end eles: file opened */


				if (!vstr_opts.empty()) {
					/* 2 dimentional array of character which is Verilator input*/
					char** verilator_opts;

					/* Dynamic allocation for rows */
					verilator_opts = (char**) malloc(vstr_opts.size() * sizeof(char*));

					/* Dynamic allocation for each array character, plus 1 for null character('\0') */
					for(int i = 0; i < vstr_opts.size(); i++)
						verilator_opts[i] = (char*) malloc(vstr_opts[i].length() * sizeof(char) + 1);

					for(unsigned int i = 0; i < vstr_opts.size(); i ++)
					{
						for(unsigned int j = 0 ; j < vstr_opts[i].length(); j++)
						{
						   /* assign each character of vector string to char** */
							verilator_opts[i][j] = (char) vstr_opts[i][j];
						}
						/* inseart null character at end character array */
						verilator_opts[i][vstr_opts[i].length()] ='\0';
					}
					// Command option parsing
					v3Global.opt.bin(argv[0]);
					argString = V3Options::argString(vstr_opts.size(), verilator_opts);
					v3Global.opt.parseOpts(new FileLine("COMMAND_LINE",0), vstr_opts.size(), verilator_opts);
					if (!v3Global.opt.outFormatOk()
					&& !v3Global.opt.preprocOnly()
					&& !v3Global.opt.lintOnly()
					&& !v3Global.opt.xmlOnly()
					&& !v3Global.opt.cdc()) {
					v3fatal("verilator: Need --cc, --sc, --sp, --cdc, --lint-only, --xml_only or --E option");
					}
					// Check environment
					V3Options::getenvSYSTEMC();
					V3Options::getenvSYSTEMC_ARCH();
					V3Options::getenvSYSTEMC_INCLUDE();
					V3Options::getenvSYSTEMC_LIBDIR();
					V3Options::getenvSYSTEMPERL();
					V3Options::getenvSYSTEMPERL_INCLUDE();

					V3Error::abortIfErrors();

					// Can we skip doing everything if times are ok?
					V3File::addSrcDepend(v3Global.opt.bin());
					if (v3Global.opt.skipIdentical()
					&& !v3Global.opt.preprocOnly()
					&& !v3Global.opt.lintOnly()
					&& !v3Global.opt.cdc()
					&& V3File::checkTimes(v3Global.opt.makeDir()+"/"+v3Global.opt.prefix()+"__verFiles.dat", argString)) {
						//	UINFO(1,"--skip-identical: No change to any source files, exiting\n");
						//	exit(0);
					}

					// Internal tests (after option parsing as need debug() setting)
					AstBasicDTypeKwd::test();
					V3Graph::test();

					//--FRONTEND------------------

					// Cleanup
					V3Options::unlinkRegexp(v3Global.opt.makeDir(), v3Global.opt.prefix()+"_*.tree");
					V3Options::unlinkRegexp(v3Global.opt.makeDir(), v3Global.opt.prefix()+"_*.dot");
					V3Options::unlinkRegexp(v3Global.opt.makeDir(), v3Global.opt.prefix()+"_*.txt");

					//int sizeOfCircuit;
					// Read first filename
					vlineOfCode.clear();
					vfileName.clear();
					v3Global.readFiles(vlineOfCode,vfileName);
					// Link, etc, if needed

					if ( V3Error::errorCount()==0) {
						if (!v3Global.opt.preprocOnly()) {
						process();
						}

						t = clock() - t;
						synthesizing_duration = (((float)t)*1000.0)/CLOCKS_PER_SEC;
						getrusage(RUSAGE_SELF,&r_usage);
						synthesizing_mem_usage = r_usage.ru_maxrss;

						cir = v3Global.circuit();
						//cir->Print(link_of_project);
						//cir->Circuit2Verilog(link_of_project);
					}
					free (verilator_opts);
					delete op;
					// finish synthesic -------------------------------------------
				}
			}
    	}
    		break;
    	case SET_SOLVER: if (("Btor" != path)&&("MiniSat" != path)) {
    				cout<<"ERROR: the solver is invalid\n";
    				cout<<"re-enter solver\n";
    				}
    			else {
    				Solver = path;
    				cout<<"Solver = "<<Solver<<"\n";
    			}
    			break;

    	case READ_PROP: {    // Synthesic
    		if (path=="") std::cout<<"ERROR: Missing paths\n";
    		else {
				if (v3Global.circuit()!=NULL) {
					properties.clear();
					std::vector<int> vlineOfCode_sva;
					std::vector<std::string> vfileName_sva;
					//chon Verilog compiler
					v3Global.set_compiler(1);
					v3Global.init_root();
					V3Options *op = new V3Options();
					v3Global.opt = *op;
					//struct rusage r_usage;
					//t = clock();
					/* Options for Verilator's input, is defined as vector of strings */
					std::vector<std::string> vstr_opts;

					/* Open input file that contains all of Verilog and PSL files */
					path.insert(0,link_of_project);
					vstr_opts.push_back("-cc");
					vstr_opts.push_back(path);

					if (!vstr_opts.empty()) {
						/* 2 dimentional array of character which is Verilator input*/
						char** verilator_opts;

						/* Dynamic allocation for rows */
						verilator_opts = (char**) malloc(vstr_opts.size() * sizeof(char*));

						/* Dynamic allocation for each array character, plus 1 for null character('\0') */
						for(int i = 0; i < vstr_opts.size(); i++)
							verilator_opts[i] = (char*) malloc(vstr_opts[i].length() * sizeof(char) + 1);

						for(unsigned int i = 0; i < vstr_opts.size(); i ++)
						{
							for(unsigned int j = 0 ; j < vstr_opts[i].length(); j++)
							{
							   /* assign each character of vector string to char** */
								verilator_opts[i][j] = (char) vstr_opts[i][j];
							}
							/* inseart null character at end character array */
							verilator_opts[i][vstr_opts[i].length()] ='\0';
						}

						// Command option parsing
						v3Global.opt.bin(argv[0]);
						argString = V3Options::argString(vstr_opts.size(), verilator_opts);
						v3Global.opt.parseOpts(new FileLine("COMMAND_LINE",0), vstr_opts.size(), verilator_opts);
						if (!v3Global.opt.outFormatOk()
						&& !v3Global.opt.preprocOnly()
						&& !v3Global.opt.lintOnly()
						&& !v3Global.opt.xmlOnly()
						&& !v3Global.opt.cdc()) {
						v3fatal("verilator: Need --cc, --sc, --sp, --cdc, --lint-only, --xml_only or --E option");
						}

						// Check environment
						V3Options::getenvSYSTEMC();
						V3Options::getenvSYSTEMC_ARCH();
						V3Options::getenvSYSTEMC_INCLUDE();
						V3Options::getenvSYSTEMC_LIBDIR();
						V3Options::getenvSYSTEMPERL();
						V3Options::getenvSYSTEMPERL_INCLUDE();

						V3Error::abortIfErrors();

						// Can we skip doing everything if times are ok?
						V3File::addSrcDepend(v3Global.opt.bin());
						if (v3Global.opt.skipIdentical()
						&& !v3Global.opt.preprocOnly()
						&& !v3Global.opt.lintOnly()
						&& !v3Global.opt.cdc()
						&& V3File::checkTimes(v3Global.opt.makeDir()+"/"+v3Global.opt.prefix()+"__verFiles.dat", argString)) {
							//	UINFO(1,"--skip-identical: No change to any source files, exiting\n");
							//	exit(0);
						}

						// Internal tests (after option parsing as need debug() setting)
						AstBasicDTypeKwd::test();
						V3Graph::test();

						//--FRONTEND------------------

						// Cleanup
						V3Options::unlinkRegexp(v3Global.opt.makeDir(), v3Global.opt.prefix()+"_*.tree");
						V3Options::unlinkRegexp(v3Global.opt.makeDir(), v3Global.opt.prefix()+"_*.dot");
						V3Options::unlinkRegexp(v3Global.opt.makeDir(), v3Global.opt.prefix()+"_*.txt");

						//int sizeOfCircuit;
						// Read first filename
						vlineOfCode_sva.clear();
						vfileName_sva.clear();
						v3Global.readFiles(vlineOfCode_sva,vfileName_sva);

						if ( V3Error::errorCount()==0) {

							// Link, etc, if needed
							if (!v3Global.opt.preprocOnly()) {
							process();
							}

							//t = clock() - t;
							//synthesizing_duration = (((float)t)*1000.0)/CLOCKS_PER_SEC;
							//getrusage(RUSAGE_SELF,&r_usage);
							//synthesizing_mem_usage = r_usage.ru_maxrss;

							properties.push_back(v3Global.property());
						}
						free (verilator_opts);
						delete op;
						// finish synthesic -------------------------------------------
					}
				}
				else {
					cout<<"Digital Circuit has not entered yet\n";
				}
			}
    	}
    		break;
    	case VERIFY_PROP: {
			if (("Btor" != Solver)&&("MiniSAT" != Solver)) {
				cout<<" No Solver is set\n";
			}
			else {	
				//Verify properties
				struct rusage r_usage;
				t = clock();
				std::vector<bool> property_state;

				for (unsigned i = 0; i<properties.size();i++)
				{
				   if (properties[i] == 0)
					  std::cout << "\nWARNING: Empty Property " << i << "\n" << std::endl;

				   TFE = new TimeFrameExpansion(v3Global.circuit(),properties[i]);

				   Btor *btor;
				   btor = boolector_new ();
				   boolector_enable_model_gen (btor);

				   std::string vcdname = "Results/VCD/"; //ten folder vcd chua timing diagram
				   vcdname.insert(0, link_of_project);
				   satlib::MinisatI* satSolver = new satlib::MinisatI();
				   //std::cout << "\n Solving...\n" << std::endl;

				   vcdname.insert(vcdname.length(),properties[0]->GetName());
				   vcdname.insert(vcdname.length(),".vcd");

				   if("Btor" == Solver)
				   {
						if (!TFE->Solve_Btor(btor))
						{
							t = clock() - t;
							getrusage(RUSAGE_SELF,&r_usage);
							verifying_mem_usage = r_usage.ru_maxrss;
							//std::cout << "Property holds" << std::endl;
							property_state.push_back(true);
						}
						else
						{
							t = clock() - t;
							getrusage(RUSAGE_SELF,&r_usage);
							verifying_mem_usage = r_usage.ru_maxrss;
							//std::cout << "Property fails" << std::endl;
							TFE->PrintSolution_Btor(btor, v3Global.circuit()->GetTopmodule()->GetModuleName(), vcdname.c_str());
							property_state.push_back(false);
						}
					}
					else if("MiniSAT" == Solver)
					{
						if (!TFE->Solve_MiniSat(*satSolver))
						{
							t = clock() - t;
							getrusage(RUSAGE_SELF,&r_usage);
							verifying_mem_usage = r_usage.ru_maxrss;
							//std::cout << "Property holds" << std::endl;
							property_state.push_back(true);
						}
						else
						{
							t = clock() - t;
							getrusage(RUSAGE_SELF,&r_usage);
							verifying_mem_usage = r_usage.ru_maxrss;
							//std::cout << "Property fails" << std::endl;
							TFE->PrintSolution_MiniSat(*satSolver, v3Global.circuit()->GetTopmodule()->GetModuleName(),vcdname.c_str());
							property_state.push_back(false);
						}
					}

				   //std::cout << "----------------------------------" << std::endl << std::endl;
				}
				verifying_duration = (((float)t)*1000.0)/CLOCKS_PER_SEC;
				std::cout << "\nSummary" << std::endl;

				std::cout << " property | state | property name " << std::endl;
				std::cout << "-------------+--------+-----------------" << std::endl;
				for (unsigned i=0; i<property_state.size(); i++)
				{
				   printf("    %9u | ",i);
				   if (property_state[i])
					  printf("holds | ");
				   else
					  printf("fails | ");
				   std::cout << properties[i]->GetName() << std::endl;
				}
				//std::cout << std::endl << std::endl;
				// finish verifying

				// Final steps
				V3Global::dumpGlobalTree("final.tree",99);
				//V3Error::abortIfWarnings();

				if (!v3Global.opt.lintOnly() && !v3Global.opt.cdc()
				&& v3Global.opt.makeDepend()) {
				V3File::writeDepend(v3Global.opt.makeDir()+"/"+v3Global.opt.prefix()+"__ver.d");
				}
				if (!v3Global.opt.lintOnly() && !v3Global.opt.cdc()
				&& (v3Global.opt.skipIdentical() || v3Global.opt.makeDepend())) {
				V3File::writeTimes(v3Global.opt.makeDir()+"/"+v3Global.opt.prefix()+"__verFiles.dat", argString);
				}

				// quandvk54 05-05
				//getrusage(RUSAGE_SELF,&r_usage);

			#ifdef VL_LEAK_CHECKS
				// Cleanup memory for valgrind leak analysis
				v3Global.clear();
			#endif
				FileLine::deleteAllRemaining();

				// quandvk54 22-01-14
				// tinh thoi gian thuc hien chuong trinh
				//t = clock()-t;
				//duration = (((float)t)*1000.0)/CLOCKS_PER_SEC;
				std::cout.setf(ios::fixed);

				// Quandvk54, 22-01-14
				std::string logname = "Results/LOG/log.txt";
				logname.insert(0, link_of_project);
				std::ofstream logFile(logname.c_str(),std::ios::out | std::ios::app);
				// write on log file
				for(int i = 0; i < vfileName.size(); i++)
				{
					logFile<<"File "<<vfileName[i]<<" co so dong la: "<<vlineOfCode[i]-2<<std::endl;
				}

				logFile<<"Circuit size: "<<v3Global.module()->Size()<<" component."<<std::endl;
				logFile<<"Iterative circuit and property size: "<<TFE->Size()<<" component."<<std::endl;
				logFile<<"Property name : "<<properties[0]->GetName()<<std::endl;
				logFile<<"SAT-solver : ";
				logFile<<Solver<<"\n";
				logFile<<" length : "<<TFE->GetmExpandedTimeFrames()<<" cycles\n";
				logFile<<"Runing time(ms) for synthesic: "<<setprecision(5)<<synthesizing_duration<<std::endl;
				logFile<<"Runing time(ms) for verification: "<<setprecision(5)<<verifying_duration<<std::endl;
				logFile<<"Physical memory used(KB) for synthesic: "<<synthesizing_mem_usage<<std::endl;
				logFile<<"Physical memory used(KB) for verification: "<<verifying_mem_usage<<std::endl;
				logFile<<"Property Status : ";
				if (property_state[0])
					logFile<<"holds ";
				else
					logFile<<"fails ";
				logFile<<"\n\n	---------------------------------------------\n\n";
				logFile.close();
			   }
			}
    		break;
    	case PRINT_CIR:
    		if (cir==NULL) std::cout<<" the circuit does not exist\n";
    		else {
			cir->Print(link_of_project);
			cir->Circuit2Verilog(link_of_project);
		}
    		break;
    	case PRINT_PROP:
    		if (properties.empty()) std::cout<<" the property does not exist\n";
    		else properties[0]->Print(link_of_project);
    		break;
    	case PRINT_IPC:
    		if (TFE==NULL) std::cout<<" the IPC computational model does not exist\n";
    		else TFE->Print(link_of_project);
    		break;
    	case EXIT: exit_flag = 1; break;
    	default: cout<<"a wrong command was entered\n"; break;
    	}
    }

    //UINFO(1,"Done, Exiting...\n");
}
