/*! ************************************************************************
    \file rotateleft.h
    \brief Header file of the Class RotateLeft.

    The class contained in this file defines the attributes and methods of the
 *  Function RotateLeft.
    \author C.Villarraga

    \copyright (c) University of Kaiserslautern 05.07.2011

Modification history:
 *

**************************************************************************/


#ifndef ROTATELEFT_H_
#define ROTATELEFT_H_
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/defaultexception.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class to describe the behaviour of the RotateRight Function.
 * This class implements the Function RotateRight derived from the class
 * Function.
 */
class RotateLeft : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The RotateLeft Function needs 2 Fanins. The first fanin is the data
     * input that shall be shifted. The second input is the amount of bits
     * that have to be shifted.
     */
    RotateLeft(){SetMRequiredFanin(2);SetMType(ROL);SetMOutputName("");}

    /**
     * A constructor with a vector of fanin Components.
     * A RotateLeft has two fanin Components so initialize mRequiredFanin with
     * 2. The first input is the data input to be rotated the second input
     * specifies the number of bits that have to be rotated. Add the fanin
     * components to the fanin of this RotateLeft.
     * @param fanin A vector containing the two Fanin Components of the
     * RotateLeft
     * @param fanout The string naming the output
     * @param id The ID of this RotateLeft
     * @param module The Module the RotateLeft shall be added to
     * @exception GeneralFanException is thrown if the size of the fanin does
     * not match mRequiredFanin
     */
    RotateLeft(std::vector<Component*>& fanin, std::string fanout, int id,
               Module* module);

    /**
     * Destructor
     */
    virtual ~RotateLeft(){}

    /**
     * Accessor Methods
     */
    /**
     * Return the type of this RotateLeft as a string (ROL)
     * @return The string "ROL"
     */
    inline std::string GetType(){return "ROL";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();

    /**
     * Perform a simulation of this RotateLeft for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals);

    /**
     * Create the CNF for this RotateLeft.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor){};

/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */

    /**
     * Operations
     */
    /**
     * recursive method for CreateCnf
     */
    void makeRol ( satlib::SatI& satSolver, satlib::RtpLit sel,
                   std::vector<satlib::RtpLit>& res,
                   std::vector<satlib::RtpLit>& inp, unsigned rotateBy);
};


#endif /* ROTATELEFT_H_ */
