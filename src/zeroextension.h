/*! ************************************************************************
    \file zeroextension.h 
    \brief Header file of the Class ZeroExtension.

    The class contained in this file defines the attributes and methods of the
 *  Function ZeroExtension.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Mon Feb 13 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef ZEROEXTENSION_H
#define ZEROEXTENSION_H
#include <string>
#include "function.h"
#include <vector>
#include "../exceptions/generalfanexception.h"
#include "../exceptions/defaultexception.h"
#include "../exceptions/integerinputexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class to describe the behaviour of the ZeroExtension Function.
 * This class implements the Function ZeroExtension derived from the class
 * Function.
 */
class ZeroExtension : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * C.Villarraga 24.03.2011: improved comment
     * Default Constructor.
     * The ZeroExtend Function needs 2 Fanins. The first fanin (at position 0) is the data
     * input that shall be extended. The second input (at position 1) is the amount of bits
     * the output shall have.
     */
    ZeroExtension(){SetMRequiredFanin(2);SetMType(ZERO_EXTEND);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * A SignExtend has two fanin Components so initialize mRequiredFanin with 2. 
     * The first input is the data input to be shifted the second input specifies 
     * the number of bits the output shall have.
     * Add the fanin components to the fanin of this ZeroExtend.
     * @param fanin A vector containing the two Fanin Components of the ZeroExtend
     * @param fanout The string naming the output
     * @param id The ID of the ZeroExtension
     * @param module The module we want to add the ZeroExtension to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     */
    ZeroExtension(std::vector<Component*>& fanin, std::string fanout, int id, 
                  Module* module);
    
    /**
     * Destructor
     */
    virtual ~ZeroExtension(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this component as a string (ZERO_EXTEND)
     * @return The string "ZERO_EXTEND"
     */
    inline std::string GetType(){return "ZERO_EXTEND";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();
        
    /**
     * Perform a simulation of this ZeroExtension for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );
    
    /**
     * Get a vector of the RtpLits defined by this ZeroExtension. 
     * For the ZeroExtension these are equal to the lits of the input.
     * @param r the vector where the defined RtpLits will be stored in.
     */
    void GetFanoutRtpLits(std::vector<satlib::RtpLit>& r);
    
    /**
     * Create the CNF for this ZeroExtension.
     * @param solver the interface to the solver
     */
    inline void CreateCnf(satlib::SatI& solver) {}
    

    /**
    * C.Villarraga 24.03.2011. Added this re-implementation from the Component class
    * Get the assignment of the output of this Component. This implementation is required
    * because this component doesnt have own literals (i.e no CNF)
    * @param satSolver The sat solver that retrieved the solution
    * @param assignment the assignment of the output shall be returned here
    */
    virtual void GetFanoutAssignment(satlib::SatI& satSolver,std::vector<satlib::SatI::lValue>& assignment)
    {
        // Get RtpLits of the FanIn since this kind of
        // component doesnt have own RtpLits
        std::vector<satlib::RtpLit> a0;
        mFanin[0]->GetFanoutRtpLits(a0);

        // Now get assigment for the FanIn
        std::vector<satlib::SatI::lValue> fanin0_assgn;
        for(unsigned int i=0; i<a0.size();i++)
        	fanin0_assgn.push_back(satSolver.getValue(a0[i]));

        // actually the other FanIn is not required, here used only
        // to avoid the simulation method to complain (InputExceptionSize)
        std::vector<satlib::RtpLit> a1;
        mFanin[1]->GetFanoutRtpLits(a1);
        std::vector<satlib::SatI::lValue> fanin1_assgn;
        for(unsigned int i=0; i<a1.size();i++)
        	fanin1_assgn.push_back(satSolver.getValue(a1[i]));

        // Perform simulation to get the assigment values for this component
        std::vector< std::vector< satlib::SatI::lValue> > inputVals;
        inputVals.push_back(fanin0_assgn);
        inputVals.push_back(fanin1_assgn);
        Simulate(inputVals, assignment);
    }

	void Create_BtorComp(Btor* btor);


/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    
    /**
     * Operations
     */
};
#endif //ZEROEXTENSION_H

