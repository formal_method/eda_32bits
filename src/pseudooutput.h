/*! ************************************************************************
    \file pseudooutput.h
    \brief Header file of the PseudoOutput class

    Defines all attributes and methods an PseudoOutput has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 1 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef PSEUDOOUTPUT_H
#define PSEUDOOUTPUT_H
#include <string>
#include "input.h"
#include "../exceptions/generalfanexception.h"
#include "module.h"

/**
 * Class PseudoOutput for the representation of registers.
 * We want to unroll the model, therefore each state is represented by a
 * PseudoInput and a PseudoOutput. This class defines the attributes and
 * methods a PseudoOutput has to have additionally to those inherited from
 * Input. It might seem to be strange that the PseudoOutput is a subclass of
 * Input and not a subclass of Output. This is because mainly in the way a
 * PseudoOutput is initialized a PseudoOutput has more in common with an Input
 * than it has with an Output.
 */
class PseudoOutput : public Input {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * A PseudoOutput needs 1 fanin but the constructor exspects 2 for the
     * name and the bitwidth. The fanin itself is added later.
     */
    PseudoOutput(){SetMRequiredFanin(1); SetMType(PS_OUTPUT);SetMOutputName("");}

    /**
     * A constructor with a vector containing the fanin and fanout Components.
     * A PseudoOutput has one fanin Component so initialize mRequiredFanin with 1.
     * The second string given in name is the bitwidth of the PseudoOutput.
     * @param name A vector containing the fanin and fanout Component of the
     * PseudoOutput
     * @param fanout The string naming the output
     * @param id The ID of the PseudoOutput
     * @param module The Module the PseudoOutput shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not
     * match mRequiredFanin
     */
    PseudoOutput(const std::vector<FckId>& name,
                           const std::string& componentName, int id, Module* module);

    /**
     * Destructor
     */
    virtual ~PseudoOutput(){}
    /**
     * Accessor Methods
     */
    /**
     * Add a component to the fanout of this PseudoOutput.
     * A PseudoOutput has a PseudoInput in its Fanout. This method adds this
     * PseudoInput.
     * @param cmp a reference to the Component that shall be added.
     */
    void AddFanout (Component* cmp);

    /**
     * Add a Component to the fanin of this PseudoOutput.
     * In differenece to an Input a PseudoOutput has a Fanin Component. This
     * method adds a Component to the fanin of this PseudoOutput at the Position
     * given.
     * @param cmp Reference to the Component that shall be added to the fanin.
     * @param pos The position at which this Component shall be added. A
     * PseudoOutput has only one input (pos=1).
     * @exception DoubledFaninException if fanin is either not needed (pos>1) or
     * already there.
     */
    void AddFanin (Component* cmp,unsigned int pos);

    /**
     * Return the Components in the Fanout.
     * @return Return a vector with the Components in the fanout of this
     * Function.
     */
    inline std::vector<Component*> GetFanout (){return mFanout;}

    /**
     * Return the Components in the Fanin.
     * We would like to know which PseudoOutput is in the Fanin of this
     * PseudoInput.
     * @return This method here returns a vector containing the Components in
     * the fanin..
     */
    inline std::vector<Component*> GetFanin () const {return mFanin;}

    /**
     * Return the type of this PseudoOutput as a string (PS_OUTPUT)
     * @return The string "PS_OUTPUT"
     */
    inline std::string GetType(){return "PS_OUTPUT";}

    /**
     * Set the edge on which the PS_OUTPUT sets it's value
     * @param edge The edge on which the register implemented by this
     * combination of PS_OUTPUT and PS_INPUT updates its value.
     */
    inline void SetOnEdge(int edge){mSetOnEdge=edge;}

    /**
     * Get the edge on which the PS_OUTPUT sets it's value.
     * @return The edge on which the register implemented by the PseudoOutput
     * and PseudoInput updates its value.
     */
    inline int GetEdge(){return mSetOnEdge;}

    /**
     * Set the reset properties of this PS_OUTPUT
     * @param reset The input used to reset the PS_OUTPUT
     * @param resVal The input delivering the value this PS_OUTPUT is reset to.
     */
    void SetReset(FckId reset, FckId resVal);

    /**
     * Get the fanin resetting the pseudo output
     * @return The name of the resetting input
     */
    FckId GetReset(){return mReset;}

    /**
     * Get the fanin determing the reset value
     * @return The name of the signal determing the value this pseudo input is
     * reset to
     */

    /**
     * Set the reset properties of this PS_OUTPUT
     * @param reset The input used to reset the PS_OUTPUT
     * @param resVal The input delivering the value this PS_OUTPUT is reset to.
     */
    void SetClock(FckId clk);

    /**
     * Get the fanin resetting the pseudo output
     * @return The name of the resetting input
     */
    FckId GetClock(){return mClock;}

    /**
     * Get the fanin determing the reset value
     * @return The name of the signal determing the value this pseudo input is
     * reset to
     */

    FckId GetResetValue(){return mResetVal;}
    /**
     * Operations
     */

    /**
     * Perform a simulation of this PseudoOutput for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
    {outVals=inpVals[0];}

    /**
     * Check whether the PseudoOutput is complete.
     * @return Component::IsComplete()
     */
    bool IsComplete ();

    /**
     * Get a vector of the RtpLits defined by this PseudoOutput.
     * For a PseudoOutput these are equivalent to the input lits.
     * @param r the vector where the defined RtpLits will be stored in.
     */
    virtual void GetFanoutRtpLits(std::vector<satlib::RtpLit>& r);

    /**
     * Create the CNF for this PseudoOutput.
     * Do nothing as we do not need a CNF representation for a PseudoOutput.
     * This is handled by passing the rtp lits through from the inputs to the
     * outputs.
     * @param solver the interface to the solver
     */
    virtual void CreateCnf(satlib::SatI& solver){}

    //them ham set, get clk va rst
    void Setclock_name(std::string name){clock_name=name;}
    std::string Getclock_name(){return clock_name;}
    void Setreset_name(std::string name){reset_name=name;}
    std::string Getreset_name(){return reset_name;}
    void Setclock_edge(std::string edge){clock_edge=edge;}
    std::string Getclock_edge(){return clock_edge;}
    void Setreset_edge(std::string edge){reset_edge=edge;}
    std::string Getreset_edge(){return reset_edge;}


	void Create_BtorComp(Btor* btor){};
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    std::string clock_name, reset_name, clock_edge, reset_edge;
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * The PseudoOutput associated with this PseudoInput
     */
    std::vector<Component*> mFanin;

    /**
     * Is this PS_OUTPUT setting it's value on a rising or falling edge
     */
    int mSetOnEdge;

    /**
     * If this PS_INPUT has a reset store the name of the resetting input
     * here.
     */
    FckId mReset, mClock;

    /**
     * If this PS_INPUT has a reset store the name of the value it is reset to
     * here.
     */
    FckId mResetVal;


    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */


    /**
     * Operations
     */
};
#endif //PSEUDOOUTPUT_H

