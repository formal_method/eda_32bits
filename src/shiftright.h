/*! ************************************************************************
    \file shiftright.h
    \brief Header file of the Class ShiftRight.

    The class contained in this file defines the attributes and methods of the
 *  Function ShiftRight.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Fri Feb 10 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef SHIFTRIGHT_H
#define SHIFTRIGHT_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/defaultexception.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class to describe the behaviour of the ShiftRight Function.
 * This class implements the Function ShiftRight derived from the class
 * Function.
 */
class ShiftRight : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The ShiftRight Function needs 2 Fanins. The first fanin is the data 
     * input that shall be shifted. The second input is the amount of bits
     * that have to be shifted.  
     */
    ShiftRight(){SetMRequiredFanin(2);SetMType(SHR);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * A ShiftRight has two fanin Components so initialize mRequiredFanin with
     * 2. The first input is the data input to be shifted the second input 
     * specifies the number of bits that have to be shifted. Add the fanin 
     * components to the fanin of this ShiftRight.
     * @param fanin A vector containing the two Fanin Components of the 
     * ShiftRight
     * @param fanout The string naming the output
     * @param id The ID of this ShiftRight
     * @param module The Module the ShiftRight shall be added to
     * @exception GeneralFanException is thrown if the size of the fanin does 
     * not match mRequiredFanin
     */
    ShiftRight(std::vector<Component*>& fanin, std::string fanout, int id, 
               Module* module);
    
    /**
     * Destructor
     */
    virtual ~ShiftRight(){}

    /**
     * Accessor Methods
     */
    /**
     * Return the type of this ShiftRight as a string (SHR)
     * @return The string "SHR"
     */
    inline std::string GetType(){return "SHR";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();
    
    /**
     * Perform a simulation of this ShiftRight for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );
    
    /**
     * Create the CNF for this ShiftRight.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor);

/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */

    /**
     * Operations
     */
    /**
     * recursive method for CreateCnf
     */ 
    void makeShr ( satlib::SatI& satSolver, satlib::RtpLit sel,
                   std::vector<satlib::RtpLit>& res,
                   std::vector<satlib::RtpLit>& inp, unsigned shiftBy);
};
#endif //SHIFTRIGHT_H

