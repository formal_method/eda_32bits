#include "PslProperty.h"

PslProperty::PslProperty()
{
   mMaximumTimeFrame = 0;
   NetlistKind = 1;
   mAssumptions.clear();
   mCommitments.clear();
   mVariables.clear();
   mConstants.clear();
   //std::cout << "-- Preparing new Property --" << std::endl;
}

FckId PslProperty::GetVariable(std::string name, int timeFrame, Circuit* widthCircuit)
{
   //look up if variable exists
   std::pair<std::string,int> pair = std::make_pair<std::string,int>(name, timeFrame);
   if (mVariables[pair]!=0)
   {
      return mVariables[pair];
   }
   // otherwise look up bit width and add variable
   std::vector<FckId> fanins;
   Module* module = (Module*)widthCircuit->GetTopmodule();
   //std::cout<<"Dia chi cua module: "<<module<<"\n";
   // module->Print();
   if(!module) {std::cout<<"Error when get top module.\n";return -1;}
   Component* cmp = module->GetComponent(name);
   if(!cmp) {std::cout<<"Error when get component "<<name<<std::endl;return -1;}
   fanins.push_back(cmp->GetMFanoutWidth());
   mVariables[pair] = AddComponent(INPUT,fanins,name);
   mTimeFrameMap[mVariables[pair]] = std::make_pair<FckId,int>(cmp->GetMId(), timeFrame);
   return mVariables[pair];
}

void PslProperty::ShiftTimeTable(int shamt)
{
   std::map<std::pair<std::string,int>,FckId> new_map;
   std::map<std::pair<std::string,int>,FckId>::iterator iter;

   for (iter = mVariables.begin(); iter!= mVariables.end(); iter++)
   {
      std::pair<std::string,int> tmp_pair = iter->first;
      tmp_pair.second += shamt;
      new_map[tmp_pair] = iter->second;
   }
   mVariables.clear();
   mVariables = new_map;
   std::map<FckId,std::pair<FckId,int> >::iterator time_frame_map_iter;
   for (time_frame_map_iter = mTimeFrameMap.begin(); time_frame_map_iter!= mTimeFrameMap.end(); time_frame_map_iter++)
   {
      std::pair<FckId,int> pair = time_frame_map_iter->second;
      pair.second += shamt;
      time_frame_map_iter->second = pair;
   }
}

FckId PslProperty::GetConstant(std::string name)
{
   //look up if constant exists
   if (mConstants[name]!=0)
   {
      return mConstants[name];
   }
   // otherwise look up bit width and add variable
   std::vector<FckId> fanins;
   mConstants[name] = AddComponent(C_INPUT,fanins,name);
   return mConstants[name];
}

int PslProperty::GetMaxTimeFrame()
{
   return mMaximumTimeFrame;
}

int PslProperty::IncreaseMaxTimeFrame(int shamt) {
	mMaximumTimeFrame = mMaximumTimeFrame + shamt;
	return mMaximumTimeFrame;
}

int PslProperty::GetMinTimeFrame()
{
   int minTimeFrame = 0;
   std::map<std::pair<std::string,int>,FckId>::iterator iter;
   for (iter = mVariables.begin(); iter!= mVariables.end(); iter++)
   {
      std::pair<std::string,int> tmp_pair = iter->first;
      if (tmp_pair.second<minTimeFrame)
         minTimeFrame = tmp_pair.second;
   }
   return minTimeFrame;
}

void PslProperty::AddAssumption(FckId id)
{
   mAssumptions.push_back(id);
}

void PslProperty::AddCommitment(FckId id)
{
   mCommitments.push_back(id);
}

FckId PslProperty::FinishProperty()
{
   // assumption = and of all assumptions
   std::cout << " - assumptions:" << std::endl;
   for (unsigned i=0; i<this->mAssumptions.size(); i++)
   {
      std::cout << "   assumption " << i << " is Id " << this->mAssumptions[i] << std::endl;
   }

   std::cout << " - commitments:" << std::endl;
   for (unsigned i=0; i<this->mCommitments.size(); i++)
   {
      std::cout << "   commitment " << i << " is Id " << this->mCommitments[i] << std::endl;
   }

   FckId assumption = 0;
   FckId commitment = 0;
   std::vector<FckId> fanins;
   if (mAssumptions.size()!=0)
   {
      assumption = mAssumptions.back();
      mAssumptions.pop_back();
   }
   while (!mAssumptions.empty())
   {
      fanins.clear();
      fanins.push_back(assumption);
      fanins.push_back(mAssumptions.back());
      mAssumptions.pop_back();
      assumption = AddComponent(AND,fanins,"");
   }
   // commitment = and of all commitments
   if (mCommitments.size()!=0)
   {
      commitment = mCommitments.back();
      mCommitments.pop_back();
   }
   while (!mCommitments.empty())
   {
      fanins.clear();
      fanins.push_back(commitment);
      fanins.push_back(mCommitments.back());
      mCommitments.pop_back();
      commitment = AddComponent(AND,fanins,"");
   }
   if (commitment==0)
   {
	  std::cout<<" khong co phan assert trong menh de\n";
      return 0;
   }
   // property = not assumption or commitment
   FckId id = commitment;
   if (assumption!=0)
   {
      fanins.clear();
      fanins.push_back(assumption);
      id = AddComponent(NOT,fanins,"");
      fanins.clear();
      fanins.push_back(commitment);
      fanins.push_back(id);
      id = AddComponent(OR,fanins,"");
   }

   // edited by Son Lam 15/6/2015
   // them cac hanh dong doi voi SVA
   FckId localvar = 0;
   if (mLocalVarConditions.size()!=0)
   {
	  localvar = mLocalVarConditions.back();
      mLocalVarConditions.pop_back();
   }
   while (!mLocalVarConditions.empty())
   {
      fanins.clear();
      fanins.push_back(localvar);
      fanins.push_back(mLocalVarConditions.back());
      mLocalVarConditions.pop_back();
      localvar = AddComponent(AND,fanins,"");
   }
   if (localvar!=0) {
	  fanins.clear();
	  fanins.push_back(localvar);
	  localvar = AddComponent(NOT,fanins,"");
	  //tao cong OR
	  fanins.clear();
	  fanins.push_back(localvar);
	  fanins.push_back(id);
	  id = AddComponent(OR,fanins,"");
   }
   //finished editing

   //fanins.clear();
   //fanins.push_back(id);
   //AddComponent(OUTPUT,fanins,"");
   return id;
}

void PslProperty::SetName(std::string name)
{
   mName = name;
}

std::string PslProperty::GetName()
{
   return mName;
}

std::map<FckId,std::pair<FckId, int> > PslProperty::GetTimeFrameMap()
{
    return mTimeFrameMap;
}

FckId PslProperty::CloneComponentTimeFrameEarlier(FckId id)
{
    Component* cmp = GetComponent(id);
    if (cmp->GetMType() == INPUT)
    {
        int timeFrame = mTimeFrameMap[cmp->GetMId()].second - 1;
        std::string name = cmp->GetMInputName();

        //look up if variable exists

        std::pair<std::string,int> pair = std::make_pair<std::string,int>(name, timeFrame);
        if (mVariables[pair]!=0)
        {
           return mVariables[pair];
        }
       // otherwise look up bit width and add variable
       std::vector<FckId> fanins;

       fanins.push_back(cmp->GetMFanoutWidth());
       mVariables[pair] = AddComponent(INPUT,fanins,name);
       mTimeFrameMap[mVariables[pair]] = std::make_pair<FckId,int>(mTimeFrameMap[id].first, timeFrame);
       return mVariables[pair];
    }
    else if (cmp->GetMType() == C_INPUT)
    {
        return id;
    }
    else
    {
        std::vector<FckId> fanins;
        for (unsigned i=0; i<cmp->GetFanin().size(); i++)
        {
            Component* tmp_cmp = cmp->GetFanin()[i];
            fanins.push_back(CloneComponentTimeFrameEarlier(tmp_cmp->GetMId()));
        }
        return AddComponent(cmp->GetMType(),fanins,cmp->GetMOutputName());
    }
}
