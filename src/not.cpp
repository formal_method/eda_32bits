/*! ************************************************************************
    \file not.cpp
    \brief Implementation of the Class Not

    The class contained in this file implements the methods of the Function
 * Not
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Wed Thu 09 2006

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout

**************************************************************************/

#include "not.h"

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * A Minus has one fanin Component so initialize mRequiredFanin with 
 * 1.
 * Add the fanin component to the fanin of this Not.
 * @param fanin A vector containing the Fanin Component of the Not. 
 * @param fanout The string naming the output
 * @param id The ID of this Not
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 */
Not::Not(std::vector<Component*>& fanin, std::string fanout, int id, 
          Module* module)
{
    mModule=module;
    SetMRequiredFanin(1);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMType(NOT);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have two fanin components so lets add these to our new Add Function.
    //First of all we allocate space for the Vector mFanin with undefined
    //fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //Therefore we use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to position i. 
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mcRequiredFanin;i++)
    mFanoutWidth=fanin[0]->GetMFanoutWidth();
} //End Not(vector<Component> fanin)  
/**
 * Methods
 */
/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin.
 */
void Not::SimulateForward () 
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
}

/**
 * Perform a simulation of this Not for a given vector of inputs
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void Not::Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
{
    //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for
    //We reset mOutput
    outVals.clear();
    for (unsigned int i=0; i<inpVals[0].size();i++)
    {
        switch(inpVals[0][i])
        {
            case 0 : outVals.push_back(satlib::SatI::TRUE);
                     break;
            case 1 : outVals.push_back(satlib::SatI::FALSE);
                     break;
            case 5 : outVals.push_back(satlib::SatI::DONTCARE);
                     break;
        }
    }
}// End Simulate

/**
 * Get a vector of the RtpLits defined by this Not.
 * For a Not the RtpLits are equivalent to the negated input lits.
 * @param r the vector where the defined RtpLits will be stored in.
 */
void Not::GetFanoutRtpLits(std::vector<satlib::RtpLit>& r)
{
    //vector for the literals in the fanin
    std::vector<satlib::RtpLit> a;
    mFanin[0]->GetFanoutRtpLits(a);
    for(unsigned int i=0; i<a.size();i++)
    {
        r.push_back(satlib::RtpLit(a[i].first,-a[i].second));
    }
}


void Not::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_not(btor, mFanin[0]->GetBtorComp());
	BtorComp = comp;
}
