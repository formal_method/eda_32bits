/*! ************************************************************************
    \file equivalence.cpp
    \brief Implementation of the Class Equivalence

    The class contained in this file implements the methods of the Function
 * Equivalence
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Wed Thu 09 2006

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout

**************************************************************************/

#include "equivalence.h"

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * An Equivalence has two fanin Components so initialize mRequiredFanin with 
 * 2. The order of the fanin components doesn't matter for the Equivalence
 * Function. Add the fanin components to the fanin of this Equivalence.
 * @param fanin A vector containing the two Fanin Components of an Add
 * @param fanout The string naming the output
 * @param id The id of this Equivalence
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 * @exception InputSizeException is thrown if the fanins do not have the same 
 * size -> extend to achieve this
 */
Equivalence::Equivalence(std::vector<Component*>& fanin, std::string fanout, 
                         int id, Module* module)
{
    mModule=module;
    SetMRequiredFanin(2);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMType(EQ);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin) 
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have two fanin components so lets add these to our new Add Function.
    //First of all we allocate space for the Vector mFanin with undefined
    //fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //Therefore we use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to position i. 
    //We want the fanins to have the same size
    if (fanin[0]->GetMFanoutWidth()!=fanin[1]->GetMFanoutWidth())
    {
        std::string message="EXCEPTION: The width of the fanins is not";
        message +=" equivalent."; 
        throw InputSizeException(message, this, fanin);
    }       
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mcRequiredFanin;i++)
    //the output has one bit
    mFanoutWidth=1;
} //End Equivalence(vector<Component> fanin) 
/**
 * Methods
 */

/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin.
 * @exception DefaultException is thrown if the result can not be determined. 
 * It should be impossible that this exception occurs
 */
void Equivalence::SimulateForward ()
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
} //End SimulateForward

/**
 * Perform a simulation of this Equivalence for a given vector of inputs
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void Equivalence::Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
{
    //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for
    //We reset outVals
    outVals.clear();
    //We want both inputs to have the same size as mFaninWidth 
    try
    {
        if (inpVals[0].size()!=inpVals[1].size()
            ||inpVals[0].size()!=mFaninWidth[0])
        {
            //The two inputs do not have the same width so we use a sign 
            //extension in order to be able to add the to inputs. Throw an 
            //Exception to inform the user. 
            std::string message = "EXCEPTION: The inputs of this eq function "; 
            message+="do not have the same width. Return False";
            throw InputSizeException(message, this, mFanin);
        } // End if (mInputs[0].size()!=mInputs[1].size())
        //We consider each input bit separately from the lowest bits to the 
        //highest one.
        satlib::SatI::lValue result = satlib::SatI::TRUE;
        for (unsigned int i=0;result!=satlib::SatI::FALSE && 
             i<inpVals[0].size();i++)
        {
            if (inpVals[0][i]==satlib::SatI::DONTCARE 
                || inpVals[1][i]==satlib::SatI::DONTCARE)
            {
                result= satlib::SatI::DONTCARE;
            } //End if
            else if (inpVals[0][i]!=inpVals[1][i])
            {
                result=satlib::SatI::FALSE;
            } // End else if
            //else keep the current value
        }//End for (unsigned int i=0;i<inpVals[0].size();i++)
        //push the result to outVals
        outVals.push_back(result);
    } // End try
    //We handle the input size exception directly by returning false
    catch(InputSizeException e)
    {
        outVals.push_back(satlib::SatI::FALSE);
    } // End catch(InputSizeException e)
} //End Simulate

/**
 * Create the CNF for this Equivalence.
 * @param solver the interface to the solver
 */
void Equivalence::CreateCnf(satlib::SatI& satSolver)
{
    //The vector containing the result lits
    std::vector<satlib::RtpLit> r;
    // The vector containing the lits of input a
    std::vector<satlib::RtpLit> a;
    //the vector containing the lits of input b
    std::vector<satlib::RtpLit> b;
    //a vector containing lits of an intermediate result calculated by the xor
    //operation
    std::vector<satlib::RtpLit> xors;
    assert(GetFanin().size()==2);
    //Get the literals for the result of this component
    GetFanoutRtpLits(r);
    //get the literals of input a
    GetFanin()[0]->GetFanoutRtpLits(a);
    //get the literals of input b
    GetFanin()[1]->GetFanoutRtpLits(b);
    assert(r.size()==1);
    assert(b.size()==a.size());
    //Get literals for the intermediate result
    for(unsigned i=0;i<a.size();i++)
    {
        xors.push_back(satlib::RtpLit(mId,GetMFanoutWidth()+1+i));
    }
    //calculate the intermediate result
    for(unsigned i=0;i<a.size();i++)
    {
      satSolver.makeXor(a[i],b[i],xors[i],0);
    }
    //All literals of the intermeidate result shall be false if the result is 
    //true
    satSolver.makeNOr(xors,r[0]);
}

void Equivalence::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_eq(btor, mFanin[0]->GetBtorComp(), mFanin[1]->GetBtorComp());
	BtorComp = comp;
}



