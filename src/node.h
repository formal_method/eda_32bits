///////////////////////////////////////////////////////////
//  Node.h
//  Implementation of the Class Node
//  Original author: Quandvk54
///////////////////////////////////////////////////////////

#ifndef _NODE_
#define _NODE_
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;
class Node 
{

public:
	Node(){} //{mNodeName="noName"; mID=0;}
	Node(int id,string name): mID(id),mNodeName(name),isTraversed(0) {};
	Node(const Node& copiedNode){
		mID = copiedNode.mID;
		mNodeName=copiedNode.mNodeName;
		//mLinkedNodes.swap(copiedNode.mLinkedNodes);
		for(int i=0;i<copiedNode.mLinkedNodes.size();i++){
			mLinkedNodes[i]=copiedNode.mLinkedNodes[i];
		}
	}
 	~Node(){}

	/*
	 * Creat Edge from a node
	 * a->b: b is contained in Node vector of a
         */
	
	void CreatEdge(Node* newNode);
	
	/*
	 * Get a node linked to current node
	 *
	 */
	vector<Node*> GetLinkedNodes();


	void SetNodeID(int id);
	int GetNodeID();
	void SetNodeName(string name);
	string GetNodeName();
	/*
	 * Check whether node is correct or incorrect
	 */
	bool IsCorrectNode();

	void PrintNodeInfo();
	
	
	//ham duyet cay theo chieu sau
	friend void DFS(ofstream& out, Node* rootNode);
private:
	vector<Node*> mLinkedNodes;
	int mID;
	string mNodeName;
	
public:
	bool isTraversed;
};
#endif 
