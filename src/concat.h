/*! ************************************************************************
    \file concat.h 
    \brief Header file of the Class Concat
    
    The class contained in this file defines the attributes and methods of the
 * class for the Function Concat
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Tue Jan 31 - Thu Feb 09 2006

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef CONCAT_H
#define CONCAT_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class to describe the behaviour of the Concat Function.
 * This class implements the Function Concat derived from the class Function.
 */
class Concat : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The Concat Function needs 2 Fanins. The first one goes at the higher 
     * position, the second one to the lower.
     */
    Concat(){SetMRequiredFanin(2);SetMType(CONCAT);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * A Concat has two fanin Components so initialize mRequiredFanin with 2. The 
     * input at pos=1 goes into the lower bits of the result, the input at pos=0 
     * goes into the higher bits of the result.
     * Add the fanin components to the fanin of this Concat.
     * @param fanin A vector containing the two Fanin Components of a Concat
     * @param fanout The string naming the output
     * @param id The ID of this Concat
     * @param module The Module the Concat shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     */
    Concat(std::vector<Component*>& fanin, std::string fanout, int id, 
           Module* module);
    
    /**
     * Destructor
     */
    virtual ~Concat(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this Concat as a string (CONCAT)
     */
    inline std::string GetType(){return "CONCAT";}
    
    /**
     * Operations
     */
        
    
    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of 
     * mInputs have to be updated by fetching the result (mOutput) of the 
     * operations in the fanin.
     */
    void SimulateForward ();
    
   /**
     * Perform a simulation of this Concat for a given vector of inputs.
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );         
    
    /**
     * Get a vector of the RtpLits defined by this Concat (equal to input 
     * lits)
     * @param r the vector where the defined RtpLits will be stored in.
     */
    void GetFanoutRtpLits(std::vector<satlib::RtpLit>& r);
    
    /**
     * Create the CNF for this Concat.
     * Do nothing as we do not need a cnf representation for a concat. This is 
     * handled by passing the rtp lits through from the inputs to the outputs.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver){}
    
    /**
     * C.Villarraga 24.03.2011. Added this re-implementation from the Component class
     * Get the assignment of the output of this Component. This implementation is required
     * because this component doesnt have own literals (i.e no CNF)
     * @param satSolver The sat solver that retrieved the solution
     * @param assignment the assignment of the output shall be returned here
     */
    virtual void GetFanoutAssignment(satlib::SatI& satSolver,std::vector<satlib::SatI::lValue>& assignment)
    {
        // Get RtpLits of the FanIn since this kind of
        // component doesnt have own RtpLits
        std::vector<satlib::RtpLit> a0;
        mFanin[0]->GetFanoutRtpLits(a0);
    	std::vector<satlib::RtpLit> a1;
        mFanin[1]->GetFanoutRtpLits(a1);

        // Now get the assigments for the FanIns
        std::vector<satlib::SatI::lValue> fanin_assgn0;
        for(unsigned int i=0; i<a0.size();i++)
        	fanin_assgn0.push_back(satSolver.getValue(a0[i]));
        std::vector<satlib::SatI::lValue> fanin_assgn1;
        for(unsigned int i=0; i<a1.size();i++)
        	fanin_assgn1.push_back(satSolver.getValue(a1[i]));

        // Perform simulation to get the assigment values for this component
        std::vector< std::vector< satlib::SatI::lValue> > inputVals;
        inputVals.push_back(fanin_assgn0);
        inputVals.push_back(fanin_assgn1);
        Simulate(inputVals, assignment);
    }


	void Create_BtorComp(Btor* btor);

/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
};
#endif //CONCAT_H

