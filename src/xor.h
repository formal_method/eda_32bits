/*! ************************************************************************
    \file xor.h 
    \brief Header file of the Class Xor

    The class contained in this file defines the attributes and methods of the
 *  Function Xor.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Mon Feb 13 2006 

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef XOR_H
#define XOR_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"
#include "../exceptions/defaultexception.h"

/**
 * Class to describe the behaviour of the Xor Function.
 * This class implements the Function Xor derived from the class Function.
 */
class Xor : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The Xor Function needs 2 Fanins that can be handled equally.
     */
    Xor(){SetMRequiredFanin(2);SetMType(XOR);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * A Xor has two fanin Components so initialize mRequiredFanin with 2. The
     * order of the fanin components doesn't matter for the Xor Function.
     * Add the fanin components to the fanin of this Xor.
     * @param fanin A vector containing the two Fanin Components of the Xor.
     * @param fanout The string naming the output
     * @param id The ID of the Xor
     * @param module The Module the Xor shall be added to
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     */
    Xor(std::vector<Component*>& fanin, std::string fanout, int id, 
        Module* module);
    
    /**
     * Destructor
     */
    virtual ~Xor(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this component as a string (XOR)
     * @return the string "XOR"
     */
    inline std::string GetType(){return "XOR";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();
    
    /**
     * Perform a simulation of this Xor for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );
    
    /**
     * Create the CNF for this Xor.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor);
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
        
    
    /**
     * Operations
     */
};
#endif //XOR_H

