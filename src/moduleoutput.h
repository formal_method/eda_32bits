/*! ************************************************************************
    \file moduleoutput.h 
    \brief Header file of the ModuleOutput class
    
    Defines all attributes and methods a ModuleOutput has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed June 21 2006 

Modification history:
**************************************************************************/

#ifndef MODULEOUTPUT_H
#define MODULEOUTPUT_H
#include <string>
#include <vector>
#include "../exceptions/doubledfaninexception.h"
#include "../exceptions/inputsizeexception.h"
#include "undef.h"
#include "circuitbuilder.h"

class Module;

/**
 * Class ModuleOutput for the representation of hierachy.
 * The RSM file can have a hierarchy. To get this hierarchy into our circuit 
 * we need an interface plugging in a submodule into the topmodule. Hereby we 
 * preserve the hierachy until we do a timeframeexpansion. Thus, this 
 * Component does not exist in a TimeFrameExpansion. The interface for the 
 * Module consists of a ModuleInput linking the inputs of a module and a 
 * ModuleOutput for each Output of the Module.
 */
class ModuleOutput : public Component
{
    /**
     * Public stuff
     */
    public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor
     * An ModuleOutput has one fanin Component (which is a ModuleInput).
     */
        ModuleOutput(){SetMRequiredFanin(2);SetMType(M_OUTPUT);
        SetMOutputName("");}
    
    /**
     * A constructor with a vector with the fanin Component (a ModuleInput).
     * Add the fanin Component to the fanin of this ModuleOutput.
     * @param fanin A vector containing the fanin Component of 
     * the ModuleOutput
     * @param moduleName name of the Module plugged in 
     * @param output the name of the output 
     * @param id The ID of the ModuleOutput
     * @param module The module the ModuleOutput shall be added to.
     */
    ModuleOutput(std::vector<Component*>& fanin, ModuleId moduleId, 
                 std::string output, int id, Module* module);
    
    /**
     * Destructor
     */
    virtual ~ModuleOutput(){}
    /**
     * Accessor Methods
     */
    /**
     * Add a component to the fanout of this Function.
     * All bit vector Functions have in common that they have at least one 
     * fanout Component. This Component is not yet known at the time the 
     * Function is constructed. So it has to be possible to add fanout 
     * Components later on.
     * @param cmp a reference to the Component that shall be added.
     */
    void AddFanout (Component* cmp){mFanout.push_back(cmp);}
    
    /**
     * Add a Component to the fanout of this Function.
     * @param cmp a reference to the Component that shall be added.
     */
    void ClearFanout ()
    {
        mFanout.clear();
    }


    /**
     * Returns the value of the Input bits of a specified input.
     * @param input the position in mInputs that shall be returned 
     * @return The value of the input bits at the position specified by input.
     */
    inline std::vector< satlib::SatI::lValue> GetInput(int input)
                                                {return mInputs[input];}
    
    /**
     * Return the values of the bits in mOutput.
     * @return A vector with the current value of mOutput.
     */
    inline std::vector<satlib::SatI::lValue> GetOutput (){return mOutput;}
    
    /**
     * Add a Component to the fanin of this ModuleOutput.
     * @param cmp Reference to the Component that shall be added to the fanin.
     * @param pos The position at which this Component shall be added.
     * @exception DoubledFaninException If the given input is not needed or 
     * already there
     * 
     * Increments the value of mKnownFanin.
     */
    void AddFanin (Component* cmp,unsigned int pos);
        
    /**
     * Change the Component in the fanin of this Component
     * @param cmpold Reference to the Component that shall be removed
     * @param cmpnew Reference to the Component that shall be added
     * @exception GeneralFaninException thrown if cmpold is not found in the
     * fanin of this component
     */
    void ChangeFanin(Component* cmpold, Component* cmpnew){}
    
    /**
     * Return the Components in the Fanin.
     * We would like to know which Components are in the Fanin of this 
     * ModuleOutput.
     * @return This method here returns a vector containing the Components in 
     * the fanin.
     */
    inline std::vector<Component*> GetFanin () const {return mFanin;}
    
    /**
     * Return the Components in the fanout of this Component.
     * @return A vector containing the Components in the fanout of this 
     * Component
     */
    std::vector<Component*> GetFanout();
    
    /**
     * Return the type of this Component as a string (M_OUTPUT)
     * @return The string "M_OUTPUT"
     */
    inline std::string GetType(){return "M_OUTPUT";}
    
    /**
     * Get the width of the output
     * @return The width of the output
     */
    unsigned int GetMFanoutWidth(){return mFanoutWidth;}
    
    
    /**
     * Operations
     */
    /**
     * Perform a forward simulation from the inputs to the outputs.
     * A ModuleOutput has to be able to fetch the Outputs of the included 
     * Module fanins and update its outputs accordingly.
     */
    inline void SimulateForward (){UpdateInputs();}
    
    /**
     * perform a simulation of this ModuleOutput for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals ){}
        
    /**
     * Update the value of mInput
     * Fetch the current assignment of the Outputs of the included Module and
     * update mInput of this Component accordingly.
     */
    void UpdateInputs();
    
    /**
     * Check whether the ModuleOutput is complete.
     * @return True if the ModuleOutput has mRequiredFanin fanin Components and
     * at least one fanout Components.
     */
    bool IsComplete ();
    
    /**
     * create the cnf for this ModuleInterface.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver){}
    
    /**
     * Get the Output Component from the Module included by this ModuleOutput
     * @return reference to the Output Component
     */
    Component* GetModuleOutput();
    
    /**
     * Get the Module included in this ModuleOutput
     * @return the included Module
     */
    Module* GetInclModule(){return mInclModule;}

	void Create_BtorComp(Btor* btor){};
    
    /**
     * Protected stuff
     */
    protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
    /**
     * Private stuff
     */
    private:
    /**
     * Fields
     */
    /**
     * Reference to the included Module
     */
        Module* mInclModule;
     /**
      * Reference to the components in the fanout.
      */
        std::vector<Component*> mFanout;
     /**
      * The current value at the output.
      */
        std::vector<satlib::SatI::lValue> mOutput;
     /**
      * The value at the input
      */
        std::vector<std::vector<satlib::SatI::lValue> > mInputs;
     /**
      * Reference to the components in the Fanin
      */
        std::vector<Component*> mFanin;
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    
    /**
     * Operations
     */
};
#endif //MODULEOUTPUT_H
