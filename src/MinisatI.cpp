#include "MinisatI.h"
#include "SolverTypes.h"
#include <iostream>
// Constructors/Destructors
//  

satlib::MinisatI::MinisatI (bool isBitLevel ):SatI(isBitLevel),mSolver(),mVarToLit(),mLitToVar(),mDelVar(),mStatus(UNKNOWN){
  
}

satlib::MinisatI::~MinisatI ( ) {
}


bool satlib::MinisatI::addClause (const std::vector<RtpLit>& clause,unsigned deleteId){
  vec<Lit>cl;
  for(unsigned i=0;i<clause.size();i++){
    RtpLit rtpVar(clause[i].first,abs(clause[i].second));
    int var = mLitToVar[rtpVar];
    if(var==0){ //no variable for Literal exists
      var=mSolver.newVar()+1; // to keep var>0
      mLitToVar[rtpVar]=var;
      mVarToLit[var]= rtpVar;
    }
    assert(clause[i].second !=0);
    if(clause[i].second < 0)
      cl.push(~Lit(var-1));
    else
      cl.push(Lit(var-1));
    
  }
  if (deleteId!=0){
      int deleteVar=mDelVar[deleteId];
      if (deleteVar==0){
        deleteVar=mSolver.newVar()+1;
        mDelVar[deleteId]= deleteVar;
      }
      cl.push(Lit(deleteVar-1));
  }
  mSolver.addClause(cl);
//    for(int i=0;i<cl.size();i++){
//      if(sign(cl[i]))
//         std::cout<<"~";
//      std::cout<<var(cl[i])<<"+";
//    }
//    std::cout<<std::endl;
  // assert(mSolver.okay());
  return mSolver.okay();
}

void satlib::MinisatI::deleteClause (unsigned deleteId ){
  vec<Lit> cl;
  cl.push(Lit(mDelVar[deleteId]-1));
  mDelVar.erase(deleteId);
  mSolver.addClause(cl);
}

bool satlib::MinisatI::solve (std::vector<unsigned> assumptionIds ){
  vec<Lit> assumps;
  for(unsigned i=0;i<assumptionIds.size();i++){
    if(mDelVar[assumptionIds[i]]>0)
      assumps.push(~Lit(mDelVar[assumptionIds[i]]-1));
  }
  if(assumps.size()>0)
    if (mSolver.solve(assumps)){
      mStatus=SAT;
      return true;
    }
    else{
      mStatus=UNSAT;
      return false;
    }
  else if (mSolver.solve()){
      mStatus=SAT;
      return true;
    }
    else{
      mStatus=UNSAT;
      return false;
    }
}

satlib::SatI::lValue satlib::MinisatI::getValue (RtpLit lit ){
  if (mStatus!=SAT)
    return DONTCARE;
  else{
    int var=mLitToVar[RtpLit(lit.first,abs(lit.second))];
    if (var==0)
      return DONTCARE;
    lbool value= mSolver.model[var-1];
    if(lit.second<0)
      value=~value;
    if(value==l_True)
      return TRUE;
    else if (value==l_False)
      return FALSE;
    else    
      return DONTCARE;
  }
}

satlib::SatI::Status satlib::MinisatI::getStatus ( ){
  
  return mStatus;
}
