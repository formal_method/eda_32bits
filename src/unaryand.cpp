/*! ************************************************************************
    \file unaryand.cpp 
    \brief Implementation of the Class UnaryAnd

    The class contained in this file implements the methods of the Function
 * UnaryAnd
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Mon Feb 13  2006 

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout
 * 26.10.06 SatI::RtpLit is replaced with RtpLit

**************************************************************************/

#include "unaryand.h"

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * A UnaryAnd has one fanin Component so initialize mRequiredFanin with 1.
 * Add the fanin component to the fanin of this UnaryAnd.
 * @param fanin A vector containing the Fanin Component of the UnaryAnd. 
 * @param fanout The string naming the output
 * @param id the ID of the UnaryAnd
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 */
UnaryAnd::UnaryAnd(std::vector<Component*>& fanin, std::string fanout, int id, 
          Module* module)
{
    mModule=module;
    SetMRequiredFanin(1);
    SetMType(U_AND);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have two fanin components so lets add these to our new UnaryAnd
    //Function. First of all we allocate space for the Vector mFanin with
    //undefined fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //We use a loop Iterating from i=0 to mcRequiredFanin-1 and add the fanin
    //at position i to position i. 
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mcRequiredFanin;i++)
    mFanoutWidth=1;
} //End UnaryAnd(st::vector<Component> fanin)  

/**
 * Methods
 */
/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin.
 */
void UnaryAnd::SimulateForward () 
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
} // End SimulateForward

/**
 * perform a simulation of this UnaryAnd for a given vector of inputs
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void UnaryAnd::Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
{
    //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for
    //We reset outVals
    outVals.clear();
    //We have to iterate over the input bits. If all input bits are l_True.
    //The result is l_True, if one or more input bits are l_Undef the result
    //is either l_Undef or l_False. Is at least on einput bit l_False, so is 
    //the result l_False. We start with a result that is l_True. If an input 
    //that is undefined is found we change the the result to l_Undef. If an 
    //input that is l_False is found the result is set to l_False and we stop 
    //iterating.
    // C.Villarraga 05.08.2011 replaced mInputs by inpVals
    satlib::SatI::lValue result=satlib::SatI::TRUE;
    for (unsigned int i=0; i<inpVals[0].size() && result!=satlib::SatI::FALSE;i++)
    {
        //check the next input bit
        if(inpVals[0][i]==satlib::SatI::DONTCARE) result=satlib::SatI::DONTCARE;
        else if (inpVals[0][i]==satlib::SatI::FALSE) result=satlib::SatI::FALSE;
    } // End for (unsigned int i=1; i<mInputs[0].size() && result!=l_False;i++)
    outVals.push_back(result);
}//End Simulate

/**
 * create the CNF for this UnaryAnd.
 * @param solver the interface to the solver
 */
void UnaryAnd::CreateCnf(satlib::SatI& satSolver)
{
    //vector for the literals of the result
    std::vector<satlib::RtpLit> r;
    //vector for the literals of the input
    std::vector<satlib::RtpLit> a;
    assert(GetFanin().size()==1);
    //Get the literals
    GetFanoutRtpLits(r);
    GetFanin()[0]->GetFanoutRtpLits(a);
    assert(r.size()==1);
    satSolver.makeAnd(a,r[0]); 
}


void UnaryAnd::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_redand(btor, mFanin[0]->GetBtorComp());
	BtorComp = comp;
}
