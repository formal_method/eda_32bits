/*! ************************************************************************
    \file circuitbuilder.h 
    \brief headerfile of the Interface CircuitBuilder

    Interface to build a circuit
    
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Mon Jan 30 - Tue Feb 07 2006

Modification history:
 * 17.02.06 changed return type of AddComponent from bool to void. Whether an 
 * add is successful or not is now handled by exceptions
 * 05.07.2011 C.Villarraga Added Constants for SHRA, ROR and ROL

**************************************************************************/

#ifndef CIRCUITBUILDER_H
#define CIRCUITBUILDER_H
#include <string>
#include <vector>
#include "component.h"

/**
 * To make it easy to access the individual Components we set them to a 
 * constant integer number.
 * @var UNDEF=0
 * @var INPUT=1
 * @var PS_INPUT=2
 * @var C_INPUT=3
 * @var M_INPUT=4
 * @var ADD=5
 * @var AND=6
 * @var CONCAT=7
 * @var EQ=8
 * @var ITE=9
 * @var LT=10
 * @var MINUS=11
 * @var MULT=12
 * @var NOT=13
 * @var OR=14
 * @var SHL=15
 * @var SHR=16
 * @var SRA=17
 * @var ROR=18
 * @var ROL=19
 * @var SIGN_EXTEND=20
 * @var SLICE=21
 * @var U_AND=22
 * @var U_OR=23
 * @var U_XOR=24
 * @var WRITE=25
 * @var XOR=26
 * @var ZERO_EXTEND=27
 * @var M_OUTPUT = 28
 * @var PS_OUTPUT=29
 * @var RAM=30
 * @var OUTPUT=31
 */

const int UNDEF=0, INPUT=1, PS_INPUT=2, C_INPUT=3, M_INPUT=4, ADD=5, AND=6, 
          CONCAT=7, EQ=8, ITE=9, LT=10, MINUS=11, MULT=12, NOT=13, OR=14, 
          SHL=15, SHR=16, SHRA=17, ROR=18, ROL=19, SIGN_EXTEND=20, SLICE=21,
          U_AND=22, U_OR=23, U_XOR=24, WRITE=25, XOR=26, ZERO_EXTEND=27,
          M_OUTPUT= 28, PS_OUTPUT=29, MUX=30, DECODER=31, OUTPUT=32;

typedef unsigned FckId;
typedef unsigned ModuleId;

/**
 * Interface CircuitBuilder.
 * Interface to build the Circuit
 */
class CircuitBuilder {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
   
    /**
     * 
     */
    /**
     * Destructor
     */
    virtual ~CircuitBuilder(){}
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
    
    /**
     * Method to add a Component to a module of the constructed circuit.
     * @param type An integer representing the kind of the Component that 
     * shall be added. The types are:
     * UNDEF=0, INPUT=1, PS_INPUT=2, C_INPUT=3, M_INPUT=4, ADD=5, AND=6,
     * CONCAT=7, EQ=8, ITE=9, LT=10, MINUS=11, MULT=12, NOT=13, OR=14,
     *  SHL=15, SHR=16, SHRA=17, ROR=18, ROL=19, SIGN_EXTEND=20, SLICE=21,
     *  U_AND=22, U_OR=23, U_XOR=24, WRITE=25, XOR=26, ZERO_EXTEND=27,
     *  M_OUTPUT= 28, PS_OUTPUT=29, OUTPUT=30;
     * @param fanin A vector containing the names of the component in the 
     * fanin of the component that shall be added. For details on the correct 
     * order in which these fanins have to be specified please take a look at 
     * the subclass implementing the Component you want to add.
     * @param fanout A string with the name that the fanout of this component
     * shall get.
     * @param module The module the Component shall be added to.
     */
    virtual FckId AddComponent(int type, const std::vector<FckId>& fanin, 
                              const std::string& componentName, ModuleId module)=0;
        
    
    /**
     * Method to check whether the constructed circuit is complete
     * @return True if every Component has all required fanin Components and 
     * at least one fanout Component. 
     */
    virtual bool IsComplete() = 0;
    
    /**
     * Method to print the constructed circuit
     */
    virtual void Print(std::string path) = 0;
    
    /**
     * Set whether a register updates its value on a falling or rising edge.
     * @param edge Can take the defined integer values FALLING(=1), RISING(=2)
     * or BOTH(=3) depending on the edge behaviour the register shall be set 
     * to.
     * @param cmp a string containing the name of the PseudoOutput whose egde
     * shall be set.
     * @param module The Module the edge shall be set on.
     */
    virtual void AddEdge(int edge, FckId id, ModuleId module)=0;
    
     /**
     * Fetch a Component by its name from a given Module.
     * @param name The name of the component that shall be returned.
     * @return the component with the given name.
      */
    //virtual Component* GetComponent(std::string name, std::string module)=0;
        
    /**
     * Set the values of the inputs of the circuit.
     * @param in the values the inputs shall take. The value at in[0] will be
     * applied to the Input at mComponent[0] and so on  
     * @param module The Module the inputs shall be set for.
     */
    virtual void SetInputs(std::vector< std::vector< satlib::SatI::lValue> >& in, 
                   ModuleId module)=0;
    
    /**
     * Get the clock edges contained in the circuit.
     * @return an integer indicating whether the registers of the circuit 
     * update their value on a falling clock (FALLING=1), a clock rise 
     * (RISING=2) or whether there are some registers updating with a falling 
     * and others updating on a rising edge (BOTH=3) 
     */
    virtual int GetEdges()=0;
    
    /**
     * Get the ID for the next component in the circuit.
     * Each component in the circuit gets a unique ID. This method determines 
     * the ID the next component shall get.
     * @return an integer value for the ID the next Component will get.
     */
    virtual FckId GetNextId()=0;
    
    
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
};
#endif //CIRCUITBUILDER_H

