/*! ************************************************************************
    \file add.h
    \brief Header file of the Class Add

    The class contained in this file defines the attributes and methods of the
 * class for the Function Add
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Tue Jan 31 - Wed Feb 08 2006

Modification history:
 * 15.-16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef MUX_H
#define MUX_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
//#include "../satlib/satI/SatI.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"
#include "../exceptions/defaultexception.h"

/**
 * Class to describe the behaviour of the Add Function.
 * This class implements the Function Add derived from the class Function.
 */
class Mux : public Function {

/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The Add Function needs 2 Fanins that can be handled equally.
     */
	Mux(){SetMRequiredFanin(2);SetMType(MUX);SetMOutputName("");}

    /**
     * A constructor with a vector of fanin Components and a string naming the
     * Output
     * An Add has two fanin Components so initialize mRequiredFanin with 2.
     * The order of the fanin components doesn't matter for the Add Function.
     * Add the fanin components to the fanin of this Add.
     * This Add requires that all fanins and the fanout have the same width.
     * @param fanin A vector containing the two Fanin Components of an Add
     * @param fanout The string naming the output
     * @param id The ID for this Add
     * @param module The module the Add shall be setted for.
     * @exception GeneralFanException is thrown if the size of the fanin does not
     * match mRequiredFanin
     * @exception InputSizeException is thrown if the fanins do not have the same
     * size -> extend to achieve this
     */
	Mux(std::vector<Component*>& fanin, std::string fanout, int id,
        Module* module);

    /**
     * Destructor
     */
    virtual ~Mux(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this component as a string (ADD)
     * @return The string "ADD"
     */
    inline std::string GetType(){return "MUX";}
    /**
     * Operations
     */
   /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward (){}

    /**
     * perform a simulation of this Add for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals ){};

    /**
     * create the CNF for this Add
     * @param solver the interface to the solver
     */
    //void CreateCnf(satlib::SatI& solver){}

	void Create_BtorComp(Btor* btor){};
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
};
#endif //MUX_H

