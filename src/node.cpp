///////////////////////////////////////////////////////////
//  Node.cpp
//  Implementation of the Class Node
//  Original author: Quandvk54
///////////////////////////////////////////////////////////

#include "node.h"


vector<Node*> Node::GetLinkedNodes()
{
	return mLinkedNodes;
}

void Node::CreatEdge(Node* newNode)
{
  //GetLinkedNodes().push_back(newNode);
  mLinkedNodes.push_back(newNode);
}

void Node::SetNodeID(int id)
{
	this->mID=id;
}

int Node::GetNodeID()
{
	return mID;
}

void Node::SetNodeName(string name)
{
	this->mNodeName = name;
}

string Node::GetNodeName()
{
	return mNodeName;
}
	
bool Node::IsCorrectNode()
{
	return false;
}
	
void Node::PrintNodeInfo()
{
	std::cout<<"Information of node:\n";
	std::cout<<"ID: "<<this->GetNodeID()<<"\n";
	std::cout<<"Node name: "<<this->GetNodeName()<<"\n";
	std::cout<<"Number of linked node: "<<this->GetLinkedNodes().size()<<"\n";
}

void DFS(ofstream &out,Node* rootNode){
	if(rootNode!=NULL)
	{
		for(int i = 0;i<rootNode->GetLinkedNodes().size();i++)
		{
			if(rootNode->GetLinkedNodes()[i]!=NULL){
				// ID[lable="NodeName"];
				if(!rootNode->isTraversed)
				out<<"\t"<<rootNode->GetNodeID()<<"[label=\""<<rootNode->GetNodeName()<<"\"]"<<endl;
				out<<"\t"<<rootNode->GetLinkedNodes()[i]->GetNodeID()<<"[label=\""<<rootNode->GetLinkedNodes()[i]->GetNodeName()<<"\"]"<<endl;
				
				// id1->id2
				out<<"\t"<<rootNode->GetNodeID()<<"->"<<rootNode->GetLinkedNodes()[i]->GetNodeID()<<endl;
				
				rootNode->GetLinkedNodes()[i]->isTraversed = 1;
				// thuc hien tuong tu voi nhung node con
				DFS(out,rootNode->GetLinkedNodes()[i]);
			}
		}
	}
}
