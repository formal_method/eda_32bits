/*! ************************************************************************
    \file pseudoinput.h
    \brief Header file of the PseudoInput class

    Defines all attributes and methods an PseudoInput has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Tue Feb 14 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 17.02.06 changed return type of AddFanin from bool to void. Whether an
 * add is successful or not is now handled by exceptions
 * 11.04.06 changed constructors to demands of the parser
 * 26.10.06 ::SatI::RtpLit is replaced by ::RtpLit
**************************************************************************/

#ifndef PSEUDOINPUT_H
#define PSEUDOINPUT_H
#include <string>
#include "input.h"
#include <vector>
#include "undef.h"
#include "../exceptions/doubledfaninexception.h"

/**
 * Class PseudoInput for the representation of registers.
 * We want to unroll the model, therefore each state is represented by a
 * PseudoInput and a PseudoOutput. This class defines the attributes and
 * methods a PseudoInput has to have additionally to those inherited from
 * Input.
 */
class PseudoInput : public Input {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor
     * An PseudoInput needs 0 Fanins up on initialization
     */
    PseudoInput(){SetMRequiredFanin(1);SetMType(PS_INPUT);SetMOutputName("");}

    /**
     * A constructor with a vector of fanin Components.
     * An PseudoInput has one fanin Component so initialize mRequiredFanin with 1.
     * @param name A vector containing the name of the PseudoInput
     * @param fanout The string naming the PseudoInput
     * @param id The ID of the component
     * @param module The Module the PseudoInput shall be added to
     */
    PseudoInput(const std::vector<FckId>& name,
                const std::string& componentName, int id, Module* module);

    /**
     * Destructor
     */
    virtual ~PseudoInput(){}
    /**
     * Accessor Methods
     */
    /**
     * Add a Component to the fanin of this PseudoInput.
     * In differenece to an Input a PseudoInput has a Fanin Component. This
     * Fanin Component has to be a PseudoOutput. This method adds a
     * PseudoOutput to the fanin of this PseudoInput at the Position given.
     * @param cmp Reference to the Component that shall be added to the fanin.
     * @param pos The position at which this Component shall be added. A
     * PseudoInput has only one input.
     * @return True if cmp can be successfully added. A Component can be
     * successfully added if it is a PseudoOutput and if no other Component
     * has already been added at this position and if the position is required
     * (here pos=0).
     *
     * Increments the value of mKnownFanin.
     */
    void AddFanin (Component* cmp,unsigned int pos);


    /**
     * Return the Components in the Fanin.
     * We would like to know which PseudoOutput is in the Fanin of this
     * PseudoInput.
     * @return This method here returns a vector containing the Components in
     * the fanin..
     */
    inline std::vector<Component*> GetFanin () const {return mFanin;}

    /**
     * Return the type of this component as a string (PS_INPUT)
     * @return The string "PS_INPUT"
     */
    inline std::string GetType(){return "PS_INPUT";}


    /**
     * Operations
     */
    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * As a PseudoInput has a Fanin Component it also has to be able to fetch
     * the output of the fanin and update its output accordingly.
     */
    inline void SimulateForward (){UpdateInputs();}

    /**
     * perform a simulation of this component for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
    {outVals=inpVals[0];}

    /**
     * Update the value of mInput
     * Fetch the current assignment of the outputs of the fanin Components and
     * update mInput of this Component accordingly.
     */
    void UpdateInputs();

    /**
     * Check whether the ConstantInput is complete.
     * @return True if the ConstantInput has mRequiredFanin fanin Components
     * (=0) and at least one fanout Components.
     */
    bool IsComplete ();

    /**
     * Get a vector of the RtpLits defined by this component
     * For a Constant Input these are equivalent to the input lits
     * @param r the vector where the defined RtpLits will be stored in.
     */
    virtual void GetFanoutRtpLits(std::vector<satlib::RtpLit>& r);

    /**
     * Create the CNF for this component.
     * Do nothing as we do not need a CNF representation for a ConstantInput.
     * This is handled by passing the rtp lits through from the inputs to the
     * outputs.
     * @param solver the interface to the solver
     */
    virtual void CreateCnf(satlib::SatI& solver){}
    void Setpsi_before(FckId id){psi_before=id;}
    FckId Getpsi_before(){return psi_before;}
    void Setpsi_after(FckId id){psi_after=id;}
    FckId Getpsi_after(){return psi_after;}


	void Create_BtorComp(Btor* btor){};
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * The PseudoOutput associated with this PseudoInput
     */
    std::vector<Component*> mFanin;
    /**
     * thuoc tinh chi dung cho PSI cua RAM
     * lan luot la o nho truoc va sau c o nho hien tai
     */
    FckId psi_before, psi_after;



    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */

    /**
     * Operations
     */
};
#endif //PSEUDOINPUT_H

