/*! ************************************************************************
    \file function.cpp 
    \brief Implementation of class Function
    
    Implementation of the non virtual methods of the class Function
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Tue Jan 31 - Wed Feb 08 2006

Modification history:
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 changed return type of AddFanin from bool to void. Whether an 
 * add is successful or not is now handled by exceptions
 * 16.03.06 changed AddFanout so that it sets the width of the output and 
 * AddFanin to set the width of the fanins.

**************************************************************************/

#include "function.h"

/**
 * Constructors/Destructors
 */
/**
 * Methods
 */
/**
 * Add a Component to the fanout of this Function.
 * @param cmp a reference to the Component that shall be added.
 */
void Function::AddFanout (Component* cmp) 
{
    //In the fanout we don't care about the order of fanout components so we
    //just push the Component
    mFanout.push_back(cmp);
} // End AddFanout (Component& cmp) 


/**
 * Check whether the Function is complete.
 * @return True if the Function has mRequiredFanin fanin Components and
 * at least one fanout Component.
 */
bool Function::IsComplete () 
{
    //Do we have an output
    if (mFanout.size()==0)
    {
        //Sometimes we don't have an Output due to Optimization so just print 
        //a warning
        std::cout<<"WARNING: "<<GetType()<<" "<<GetMOutputName()
                <<"has no fanout"<<std::endl;
    }
    //all inputs should
    //be there after a successful creation
    if (mFanin.size()==mRequiredFanin) return true;
    //not complete
    else 
    {
        std::cout<<GetType()<<" "<<GetMOutputName()<<" fanins: "
                <<mFanin.size()<<" required: "<<mRequiredFanin<<std::endl;
        return false;
    }
}

/**
 * Add a Component to the fanin of this Function at the position
 * given.
 * @param cmp Reference to the Component that shall be added to the fanin.
 * @param pos The position at which this Component shall be added.
 * @return True if cmp can be successfully added. A Component can be
 * successfully added if no other Component has already been added at this
 * position and if the position is required by the Function.
 * @exception DoubledFaninException is thrown if the component can not be 
 * added, because it is either already there or not needed.
 */
void Function::AddFanin (Component* cmp, unsigned int pos)
{
    // Check if there is no Element at the position
    if (pos<mRequiredFanin && mFanin[pos]->GetMType()==0)
    {
        //There is no Element at pos and pos is needed. So we add the Element
        //and return true.    
        mFanin[pos]=cmp;
        //Get the Width of the fanout and set the fanin to the same width
        //if necessary resize
        if (mFaninWidth.size()<pos+1) mFaninWidth.resize(pos+1,0);
        mFaninWidth[pos]=cmp->GetMFanoutWidth();
    } // End if (pos<mcRequiredFanin && mFanin[pos]==NULL)
    else
    {
        //Something went wrong the Element is already there or not needed
        std::string message="EXCEPTION: The fanin that shall be added is ";
        message+="either already there or there is no fanin needed at that position.";
        if (pos >= mRequiredFanin) 
            throw DoubledFaninException(message, this, cmp, new Undef(), pos);
        else throw DoubledFaninException(message, this, cmp, mFanin[pos], pos);
    } //End else
} //End AddFanin (Component& cmp, int pos)

/**
 * Change the Component in the fanin of this Component
 * @param cmpold Reference to the Component that shall be removed
 * @param cmpnew Reference to the Component that shall be added
 * @exception GeneralFaninException thrown if cmpold is not found in the
 * fanin of this component
 */
void Function::ChangeFanin(Component* cmpold, Component* cmpnew)
{
    // a variable used to store whether we found cmpold
    bool found=false;
    // walk through the fanins to find the position of cmpold
    for (unsigned int i=0;!found && i<mRequiredFanin;i++)
    {
        // set found to true if mFanin[i] and cmpold are equivalent
        found = (mFanin[i]==cmpold);
        //were we successful?
        if (found)
        {
            mFanin[i]=cmpnew;
        } // End if (found)
    } // End for (unsigned int i=0;!found && i<mRequiredFanin;i++)
    //Not successful? - Throw GeneralFanException
    if(!found)
    {
        std::string msg = "EXCEPTION: The fanin that shall be changed was not";
        msg += " found";
        std::vector<Component*> cmps;
        cmps.push_back(cmpold);
        throw GeneralFanException(msg, cmpnew, cmps);
    } // End if (!found) 
} // End ChangeFanin(Component* cmpold, Component* cmpnew)

/**
 * Update the value of mInputs
 * Fetch the current assignment of the outputs of the fanin Components and
 * update mInput of this Component accordingly.
 */
void Function::UpdateInputs() // C.Villarraga 23.03.2011: couts commented ... they impact the performance!
{
    mInputs.clear();
    //For each fanin Component get its output values and assign them to the
    //corresponding input
    for (unsigned int i=0;i<mRequiredFanin;i++)
    {
//        std::cout<<"input "<<i<<" of "<<GetMOutputName()<<std::endl;
//        std::cout<<"Get Output of "<<mFanin[i]->GetMOutputName()<<" "<<mFanin[i]->GetType()<<std::endl;
        //Get the output of the fanin component
        std::vector<satlib::SatI::lValue> out = mFanin[i]->GetOutput();
//        for(unsigned int j=0;j<out.size();j++)
//            std::cout<<out[j];
        mInputs.push_back(out);
// 		std::cout<<std::endl;
    } //End for (int i=0;i<mcRequiredFanin;i++) 
} // End UpdateInputs

