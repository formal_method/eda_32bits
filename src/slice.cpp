/*! ************************************************************************
    \file slice.cpp 
    \brief Implementation of the Class Slice

    The class contained in this file implements the methods of the Function
 * Slice
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Mon Feb 13 2006

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout - undon now given as 
 * parameter
 * 26.10.06 SatI::RtpLit is replaced by RtpLit

**************************************************************************/

#include "slice.h"
#include <math.h>

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * A Slice has three fanin Components so initialize mRequiredFanin with 3. 
 * The first input is the data input to be sliced the second input specifies 
 * the starting position and the third input the number of bits the output has.
 * Add the fanin components to the fanin of this Slice.
 * @param fanin A vector containing the three Fanin Components of the Slice
 * @param fanout The string naming the output
 * @param id the ID of the component
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 */
Slice::Slice(std::vector<Component*>& fanin, std::string fanout, int id, 
          Module* module)
{
    mModule=module;
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMRequiredFanin(3);
    SetMType(SLICE);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have two fanin components so lets add these to our new SLICE
    //Function. First of all we allocate space for the Vector mFanin with 
    //undefined fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //We use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to position i. 
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mRequiredFanin;i++)
    
    // C.Villarraga 10.08.2011 Exception added
    if(mFanin[2]->GetMType()!=C_INPUT)
    {
        std::string message = "EXCEPTION: The Type of fanin[2] is ";
        message+="not a constant input";
        throw GeneralFanException(message, this, fanin);
    }
    
    // C.Villarraga 10.08.2011 Calculates mFanoutWidth using string bit of fanin
    mFanoutWidth=0;
    std::vector<satlib::SatI::lValue> Fanin2;
    Fanin2 = mFanin[2]->GetOutput();
    for (unsigned int i=0;i<Fanin2.size();i++)
	{
		//retrieve the value of the current bit for l_False do nothing, for
		//l_True add 2^i to widthOutput. In the case of l_Undef the number of
		//output bits is unspecified to indicate this set widthOutput to -1
		//and we leave the for loop.
		int current = Fanin2[i];
		switch (current)
		{
			case 1: mFanoutWidth+=1<<i;
					break;
			case 0: break;
			case 5:
			{
				std::string msg="EXCEPTION: Output width is undefined";
				throw IntegerInputException(msg, this, mFanin, mInputs);
			}
				break;
			//default case is not possible
			default:
			{
				std::string message="EXCEPTION: Reached an ";
				message+="impossible lValue value";
				throw DefaultException(message);
			}
		}  //End switch (current)
	} // end for (unsigned int i=0;i<Fanin2.size();i++)

} //End Slice(vector<Component> fanin)

/**
 * Methods
 */

/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin.
 * @exception DefaultException for impossible lValue values (this is 
 * impossible)
 * @exception IntegerInputException if the output width is undefined or the
 * slice exceeds the input
 * \todo unspecified starting position can also lead to a specified output 
 * (e.g. all data input are equivalent)
 */
void Slice::SimulateForward ()
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
}//End SimulateForward


/**
 * perform a simulation of this component for a given vector of inputs
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void Slice::Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
{
    //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for
    //We reset mOutput
    outVals.clear();
    //First we have to calculate how many bits the output shall have and at 
    //which position the SLICE operation shall start. Store these values in 
    //the new variables startPosition and widthOutput. The values are
    //calculated by adding up the individual bits multiplied with the 
    //magnitude of the column. The value -1 is used to indicate an undefined 
    //value (doesn't make sense for widthOutput).
    int startPosition=0;
    for (unsigned int i=0;i<inpVals[1].size() && startPosition!=-1;i++)
    {
        //retrieve the value of the current bit for FALSE do nothing, for 
        //TRUE add 2^i to shiftby. In the case of DONTCARE the number of 
        //output bits is unspecified to indicate this set widthOutput to -1 
        //and leave the for loop.
        int current = inpVals[1][i];
        switch (current)
        { 
            case 1: startPosition+=1<<i;
                    break;
            case 0: break;
            case 5: 
            {
                std::string msg="EXCEPTION: Output width is undefined";
                throw IntegerInputException(msg, this, mFanin, mInputs);
            }
            //default case is not possible
            default:
            {
                std::string message="EXCEPTION: Reached an ";
                message+="impossible value";
                throw DefaultException(message);
            }
        }  //End switch (current)
    } //End for (int i=0;i<inpVals[1].size();i++)
    
    if(mFanoutWidth+startPosition>inpVals[0].size())
    {
        std::string msg="EXCEPTION: Slice exceeds input";
        throw IntegerInputException(msg, this, mFanin, inpVals);
    }
    //Is the start position specified?
    if(startPosition>=0)
    {
        //We add the inputs of the data input until beginning with the 
        //startPosition until the number of bits in the output equals the 
        //number of bits required by the output. 
        for (unsigned int i=startPosition;i<mFanoutWidth+startPosition
             && i<inpVals[0].size();i++)
        {
            outVals.push_back(inpVals[0][i]);
        } //End for (unsigned int i=0;i<mInputs[0].size();i++)
    }
    else
    {
        //The starting position is unspecified push all undef
        for (unsigned int i=0; i<mFanoutWidth;i++)
        {
            outVals.push_back(satlib::SatI::TRUE);
        }
    } 
} // end Simulate  

/**
 * create the CNF for this Slice
 * @param solver the interface to the solver
 */
void Slice::CreateCnf(satlib::SatI& satSolver)
{
    //vector for the literals of the result
    std::vector<satlib::RtpLit> r;
    //vector for the literals of input a
    std::vector<satlib::RtpLit> a;
    //vector for the literals of input b
    std::vector<satlib::RtpLit> b;
    assert(GetFanin().size()==3);
    GetFanoutRtpLits(r);
    GetFanin()[0]->GetFanoutRtpLits(a);
    GetFanin()[1]->GetFanoutRtpLits(b);
    int extraBits=GetMFanoutWidth()+1;
    makeSlice(satSolver,a,b,r,0,b.size()-1,extraBits);
}

/**
 * recursive method for CreateCnf
 */
void Slice::makeSlice(satlib::SatI& satSolver,
                                std::vector<satlib::RtpLit>& a,
                                std::vector<satlib::RtpLit>& b,
                                std::vector<satlib::RtpLit>& r,
                                unsigned start, int bIdx, int& extraBits)
{
    if (bIdx<0)
    {
        /*if (start+r.size()>a.size())
        {
            std::string message = "EXCEPTION: Slice out of bounds\n";
            std::cout<<"Name of Slice "<<GetMOutputName()<<" start of slice "
                <<start<<" width of Slice "<<r.size()<<" width of input "
                <<a.size();
            throw DefaultException(message);
        }
        for (unsigned i=0;i<r.size();i++)
        {
            r[i]=a[i+start];
        }*/
 unsigned i=0;
        for(;i<r.size()&&i+start<a.size();i++){
            r[i]=a[i+start];
        }
        // MW: 27.11.2007 handle slices out of range as zero
        // C.Villarraga: 25.03.2011 changed from RtpLit(1,-1) to RtpLit(0,1)
        // which in fact is FALSE value!
        for(;i<r.size();i++){
            r[i]=satlib::RtpLit(0,1);
        }
    } 
    else 
    {
        std::vector<satlib::RtpLit> upSlice;
        std::vector<satlib::RtpLit> lowSlice;
        for (unsigned i=0;i<r.size();i++)
        {
            upSlice.push_back(satlib::RtpLit(mId,extraBits++));
            lowSlice.push_back(satlib::RtpLit(mId,extraBits++));
        }
        makeSlice(satSolver,a,b,lowSlice,start,bIdx-1,extraBits);
        makeSlice(satSolver,a,b,upSlice,start+(unsigned)pow(2,bIdx),bIdx-1,
                  extraBits);
        for(unsigned i=0;i<r.size();i++)
        {
            if(upSlice[i]!=lowSlice[i])
                satSolver.makeIte(b[bIdx],upSlice[i],lowSlice[i],r[i]);
            else r[i]=upSlice[i];
        }
    }
}


void Slice::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_slice(btor, mFanin[0]->GetBtorComp(), ((ConstInput*) mFanin[1])->get_decvalue()+ mFanoutWidth -1, ((ConstInput*) mFanin[1])->get_decvalue());
	BtorComp = comp;
}
