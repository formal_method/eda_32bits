/*! ************************************************************************
    \file ifthenelse.h
    \brief Header file of the Class IfThenElse.

    The class contained in this file defines the attributes and methods of the
 *  Function IfThenElse.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 1 - Thu Feb 09 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef IFTHENELSE_H
#define IFTHENELSE_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class to describe the behaviour of the IfThenElse Function.
 * This class implements the Function IfThenElse derived from the class
 * Function.
 */
class IfThenElse : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The IfThenElse Function needs 3 Fanins representing if then and else in
     * this order.
     */
    IfThenElse(){SetMRequiredFanin(3);SetMType(ITE);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * An ifThenElse has three fanin Components so initialize mRequiredFanin with 
     * 3. The order of the inputs is condition at pos=0, then statement at pos=1 
     * and else statement at pos=2.
     * Add the fanin components to the fanin of this IfThenElse.
     * @param fanin A vector containing the three Fanin Components of an 
     * IfThenElse.
     * @param fanout The string naming the output
     * @param id The ID of this IfThenElse
     * @param module The Module the IfThenElse shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     * @exception InputSizeException is thrown if the fanins do not have the 
     * correct size (fanin1: 1 and the other two the same size).
     */
    IfThenElse(std::vector<Component*>& fanin, std::string fanout, int id, 
               Module* module);
    
    /**
     * Destructor
     */
    virtual ~IfThenElse(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this IfThenElse as a string (ITE)
     * @return The string "ITE"
     */
    inline std::string GetType(){return "ITE";}
    
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();
    
    /**
     * Perform a simulation of this IfThenElse for a given vector of inputs.
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );
    
    /**
     * Create the CNF for this IfThenElse.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor);


/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */

    /**
     * Operations
     */
};
#endif //IFTHENELSE_H

