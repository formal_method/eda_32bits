// -*- mode: C++; c-file-style: "cc-mode" -*-
//*************************************************************************
// DESCRIPTION: Verilator:
//
//*************************************************************************
#include "V3Psl.h"
#include "../pslparser/PSL_Parser.h"
#include "../pslparser/PSL_Exception.h"
#include "../PslToFck/PslToFck.h"
#include "../PslToFck/PslProperty.h"
#include "../circuit/circuit.h"
#include <stdio.h>
//#include "../circuit/timeframeexpansion.h"
#include <fstream>



void V3Psl::psl(Circuit* cir) {
    //We want to use a pslParser to parse the file
    //At a later point it might be wanted to parse other files
    std::vector<PslProperty*>* properties = new std::vector<PslProperty*>;
    PslToFck* builder = new PslToFck(properties, cir);
    PslParser* parser = new PslParser(builder);
    /* std::ifstream is("test.psl",std::ifstream::in);
    try
    {
       if(!is.is_open())
       {
          throw PslException(PslException::PARSER_ERROR,"input cannot be read", 0, "fileopen");
       }
       else
	 {
          parser->parse(is);
       }
    }
    catch(const PslException& err)
    {
       std::cout << err.getAllInfo() << "\n";
    }
    if (properties->size()==0)
    {
      std::cout << "Can't read file "<< std::endl;
    }*/

    std::cout<<"Test file V3Psl.cpp\n";
}
