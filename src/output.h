/*! ************************************************************************
    \file output.h 
    \brief Header file of the Output class
    
    Defines all attributes and methods an Output has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Wed Feb 08 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 17.02.06 changed return type of AddFanin from bool to void. Whether an 
 * add is successful or not is now handled by exceptions

**************************************************************************/

#ifndef OUTPUT_H
#define OUTPUT_H
#include <string>
#include "component.h"
#include <vector>
#include "undef.h"
#include "../exceptions/doubledfaninexception.h"
#include "../exceptions/generalfanexception.h"

/**
 * Class Input represents primary Outputs.
 * Defines the attributes and methods primary Outputs have.
 */
class Output : public Component {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor
     * An Output needs 1 Fanins
     */
    Output(){SetMRequiredFanin(1); SetMType(OUTPUT);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * An Output has one fanin Component so initialize mRequiredFanin with 1.
     * Add the fanin component to the fanin of this Output.
     * @param fanin A vector containing the Fanin Component of the Output
     * @param fanout The string naming the output
     * @param id The ID of the Output
     * @param module The Module the Output shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     */
    Output(std::vector<Component*>& fanin, std::string fanout, int id, 
           Module* module);
    
    /**
     * Destructor
     */
    virtual ~Output(){}
    /**
     * Accessor Methods
     */
    
    /**
     * Return the current value of the output.
     * The values of the output of an Output are equal to the values at its 
     * input. Thus, return the input.
     * @return The value returned by GetInput(0).
     */
    inline std::vector<satlib::SatI::lValue> GetOutput (){return GetInput(0);}
    
    
    /**
     * Return the Components in the Fanin.
     * @return This method here returns a vector containing the Components in
     * the fanin..
     */
    inline std::vector<Component*> GetFanin () const {return mFanin;}
        
    
    /**
     * Returns the values of the input bits at the position of the parameter.
     * @param input the position in mInputs that shall be returned. As there 
     * is only one input it has to be 0.
     * @return The value of the input bits at the position specified by input.
     */
    inline std::vector<satlib::SatI::lValue> GetInput (int input)
        {return mInputs[input];}
    
    /**
     * Add a Component to the fanin of this Output at the position
     * given.
     * @param cmp Reference to the Component that shall be added to the fanin.
     * @param pos The position at which this Component shall be added.
     * @exception DoubledFaninException if the Component has already been added or
     * if it is not needed.
     */
    void AddFanin (Component* cmp, unsigned int pos);
    
    /**
     * Change the Component in the fanin of this Output
     * @param cmpold Reference to the Component that shall be removed
     * @param cmpnew Reference to the Component that shall be added
     * @exception GeneralFaninException thrown if cmpold is not found in the
     * fanin of this component
     */
    void ChangeFanin(Component* cmpold, Component* cmpnew);
    
    /**
     * Add a component to the fanout of this component.
     * Not possible as this is an Output
     * @param cmp a reference to the Component that shall be added.
     */
    virtual inline void AddFanout (Component* cmp){}
    
    /**
     * Add a Component to the fanout of this Function.
     * @param cmp a reference to the Component that shall be added.
     */
    void ClearFanout (){}


    /**
     * Return the type of this component as a string (OUTPUT)
     * @return The string "OUTPUT"
     */
    inline std::string GetType(){return "OUTPUT";}
    
    
    /**
     * Operations
     */
    /**
     * Check whether the Output is complete.
     * An Output doesn't have a fanout Component, thus, it has to override the
     * IsComplete method implemented in Component.
     * @return True if the Component has mRequiredFanin (=1) fanin Components
     */
    virtual inline bool IsComplete ();
        
    
    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mInputs by fetching the result (mOutput) of the
     * operation in the fanin. Therefore it is sufficient to call 
     * UpdateInputs().
     */
    inline void SimulateForward (){UpdateInputs();}
    
    /**
     * Perform a simulation of this Output for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
    {outVals=inpVals[0];}
        
    /**
     * Update the value of mInput
     * Fetch the current assignment of the outputs of the fanin Components and
     * update mInput of this Output accordingly.
     */
    void UpdateInputs();
    
    /**
     * Get a vector of the RtpLits defined by this Output.
     * For an Output these are equivalent to the input lits.
     * @param r the vector where the defined RtpLits will be stored in.
     */
    virtual void GetFanoutRtpLits(std::vector<satlib::RtpLit>& r);
    
    /**
     * C.Villarraga 24.03.2011: Added this re-implementation from the Component class
     * Get the assignment of the output of this Component. This implementation is required
     * because this component doesnt have own literals (i.e no CNF)
     * @param satSolver The sat solver that retrieved the solution
     * @param assignment the assignment of the output shall be returned here
     */
    virtual void GetFanoutAssignment(satlib::SatI& satSolver,std::vector<satlib::SatI::lValue>& assignment)
    {
        // Get RtpLits of the FanIn since this kind of
        // component doesnt have own RtpLits
        std::vector<satlib::RtpLit> a;
        mFanin[0]->GetFanoutRtpLits(a);

        // Now get the assigment for the FanIn
        std::vector<satlib::SatI::lValue> fanin_assgn;
        for(unsigned int i=0; i<a.size();i++)
        	fanin_assgn.push_back(satSolver.getValue(a[i]));

        // Perform simulation to get the assigment values for this component
        std::vector< std::vector< satlib::SatI::lValue> > inputVals;
        inputVals.push_back(fanin_assgn);
        Simulate(inputVals, assignment);
    }

	void Create_BtorComp(Btor* btor);

/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     * The value at the output
     */
    std::vector<std::vector<satlib::SatI::lValue> > mInputs;
    /**
     * Reference to the components in the Fanin
     */
    std::vector<Component*> mFanin;
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    
    /**
     * Operations
     */
};
#endif //OUTPUT_H

