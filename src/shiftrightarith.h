/*! ************************************************************************
    \file shiftrightarith.h
    \brief Header file of the Class ShiftRightArith.

    The class contained in this file defines the attributes and methods of the
 *  Function ShiftRightArith.
    \author C.Villarraga

    \copyright (c) University of Kaiserslautern 05.07.2011

Modification history:
 *

**************************************************************************/

#ifndef SHIFTRIGHTARITH_H_
#define SHIFTRIGHTARITH_H_
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/defaultexception.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class to describe the behaviour of the ShiftRightArith Function.
 * This class implements the Function ShiftRightArith derived from the class
 * Function.
 */
class ShiftRightArith : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The ShiftRightArith Function needs 2 Fanins. The first fanin is the data
     * input that shall be shifted. The second input is the amount of bits
     * that have to be shifted.
     */
    ShiftRightArith(){SetMRequiredFanin(2);SetMType(SHRA);SetMOutputName("");}

    /**
     * A constructor with a vector of fanin Components.
     * A ShiftRightArith has two fanin Components so initialize mRequiredFanin with
     * 2. The first input is the data input to be shifted the second input
     * specifies the number of bits that have to be shifted. Add the fanin
     * components to the fanin of this ShiftRightArith.
     * @param fanin A vector containing the two Fanin Components of the
     * ShiftRightArith
     * @param fanout The string naming the output
     * @param id The ID of this ShiftRightArith
     * @param module The Module the ShiftRightArith shall be added to
     * @exception GeneralFanException is thrown if the size of the fanin does
     * not match mRequiredFanin
     */
    ShiftRightArith(std::vector<Component*>& fanin, std::string fanout, int id,
               Module* module);

    /**
     * Destructor
     */
    virtual ~ShiftRightArith(){}

    /**
     * Accessor Methods
     */
    /**
     * Return the type of this ShiftRightArith as a string (SHRA)
     * @return The string "SHRA"
     */
    inline std::string GetType(){return "SHRA";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();

    /**
     * Perform a simulation of this ShiftRightArith for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );

    /**
     * Create the CNF for this ShiftRightArith.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor){};

/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */

    /**
     * Operations
     */
    /**
     * recursive method for CreateCnf
     */
    void makeShra ( satlib::SatI& satSolver, satlib::RtpLit sel,
                   std::vector<satlib::RtpLit>& res,
                   std::vector<satlib::RtpLit>& inp, unsigned shiftBy);
};

#endif /* SHIFTRIGHTARITH_H_ */
