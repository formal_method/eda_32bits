/*! ************************************************************************
    \file signextension.cpp
    \brief Implementation of the Class SignExtension

    The class contained in this file implements the methods of the Function
 * SignExtension
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Mon Feb 13 2006

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout
 * 26.10.06 SatI::RtpLit is replaced by RtpLit

**************************************************************************/

#include "signextension.h"

/**
 * Constructors/Destructors
 */
/**
 * C.Villarraga 24.03.2011: Improved comment
 * A constructor with a vector of fanin Components.
 * A SignExtend has two fanin Components so initialize mRequiredFanin with 2. 
 * The first input (at position 0) is the data input to be shifted the second input (at position 1) specifies
 * the number of bits the output shall have.
 * Add the fanin components to the fanin of this SignExtend.
 * @param fanin A vector containing the two Fanin Components of the SignExtend
 * @param fanout The string naming the output
 * @param id The ID of this SignExtension
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
  */
SignExtension::SignExtension(std::vector<Component*>& fanin, 
                             std::string fanout, int id, Module* module)
{
    mModule=module;
    SetMRequiredFanin(2);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMType(SIGN_EXTEND);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have two fanin components so lets add these to our new SIGN_EXTEND
    //Function. First of all we allocate space for the Vector mFanin with 
    //undefined fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);  //edited by Son Lam 11/10
    //Therefore we use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to position i. 
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mRequiredFanin;i++)
    
    // C.Villarraga 10.08.2011 Added the exception
    if(mFanin[1]->GetMType()!=C_INPUT)
    {
        std::string message = "EXCEPTION: The Type of fanin[1] is ";
        message+="not a constant input";
        throw GeneralFanException(message, this, fanin);
    }
    
    //calculate fanout width
    mFanoutWidth=0;
    // C.Villarraga 10.08.2011. Gets bit string to calculate mFanoutWidth
    std::vector<satlib::SatI::lValue> Fanin1;
    Fanin1=mFanin[1]->GetOutput();
    for (unsigned int i=0;i<Fanin1.size();i++)
    {
        //retrieve the value of the current bit for l_False do nothing, for
        //l_True add 2^i to widthOutput. In the case of l_Undef the number of
        //output bits is unspecified to indicate this set widthOutput to -1
        //and we leave the for loop.
        int current = Fanin1[i];
        switch (current)
        {
            case 1: mFanoutWidth+=1<<i;
                    break;
            case 0: break;
            case 5:
            {
                std::string msg="EXCEPTION: Output width is undefined";
                throw IntegerInputException(msg, this, mFanin, mInputs);
            }
            //default case is not possible
            default:
            {
                std::string message="EXCEPTION: Reached an ";
                message+="impossible lValue value";
                throw DefaultException(message);
            }
        }  //End switch (current)
    } // End for (unsigned int i=0;i<Fanin1.size();i++)
    
	// C.Villarraga: Size of the Fanin to be extended has to be lower or equal
    // than the extension (i.e. mFanoutWidth)
    if (mFanin[0]->GetMFanoutWidth()>mFanoutWidth) {
        std::string msg="EXCEPTION: Size of the extension is lower than current size of input";
        throw GeneralFanException(msg, this, mFanin);
    }
    
} //End SignExtend(vector<Component> fanin)

/**
 * Methods
 */

/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin.
 * @exception DefaultException for impossible lValue values (this is 
 * impossible)
 * @exception IntegerInputException if the output width is undefined
 */
void SignExtension::SimulateForward () 
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
}//End SimulateForward

/**
 * Perform a simulation of this SignExtension for a given vector of inputs
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void SignExtension::Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
{
    //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for
    
    // C.Villarraga 11.08.2011 checks that the size of the extension is the same as in the constructor
    unsigned fanoutWidth=0;
    for (unsigned int i=0;i<inpVals[1].size();i++)
    {
        int current = inpVals[1][i];
        switch (current)
        {
        	case 1: fanoutWidth+=1<<i;
        			break;
        	case 0: break;
        	case 5:
        	{
        		std::string msg="EXCEPTION: Output width is undefined";
        		throw IntegerInputException(msg, this, mFanin, inpVals);
        	}
        	break;
        	//default case is not possible
        	default:
        	{
        		std::string message="EXCEPTION: Reached an ";
        		message+="impossible lValue value";
        		throw DefaultException(message);
        	}
        }  //End switch (current)
    }
    if (fanoutWidth!=mFanoutWidth)
    {
    	std::string msg = "WARNING: The size of the extension in the simulation";
    	msg+="is different of the original size extension\n";
    	std::cout << msg;
    }
    
    //We reset outVals
    outVals.clear();

    //We add the inputs of the data input until the number of bits in the 
    //output equals the number of bits in the data input 
    for (unsigned int i=0;i<inpVals[0].size() && i<fanoutWidth;i++)
        outVals.push_back(inpVals[0][i]);

    //The remaining bits of the output are filled with the sign
    for(unsigned int i=inpVals[0].size();i<fanoutWidth;i++)
        outVals.push_back(inpVals[0][inpVals[0].size()-1]);
} //End Simulate

/**
 * Get a vector of the RtpLits defined by this component. 
 * For the SignExtension these are equal to the lits of the input.
 * @param r the vector where the defined RtpLits will be stored in.
 */
void SignExtension::GetFanoutRtpLits(std::vector<satlib::RtpLit>& r)
{
    mFanin[0]->GetFanoutRtpLits(r);
    for(unsigned int i=r.size();i<mFanoutWidth;i++) // C.Villarraga using mFanoutWidth
    {
        r.push_back(r[r.size()-1]);
    }
}

void SignExtension::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_sext(btor, mFanin[0]->GetBtorComp(),mFanoutWidth - mFanin[0]->GetMFanoutWidth());
	BtorComp = comp;
}
