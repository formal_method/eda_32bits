/*! ************************************************************************
    \file concat.cpp 
    \brief Implementation of the Class Concat
    
    The class contained in this file implements the methods of the class 
 * Concat.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Tue Jan 31 - Thu Feb 09 2006

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout

 **************************************************************************/

#include "concat.h"

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * A Concat has two fanin Components so initialize mRequiredFanin with 2. The 
 * input at pos=1 goes into the lower bits of the result, the input at pos=0 
 * goes into the higher bits of the result.
 * Add the fanin components to the fanin of this Concat.
 * @param fanin A vector containing the two Fanin Components of a Concat
 * @param fanout The string naming the output
 * @param id The ID of this Concat
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 */
Concat::Concat(std::vector<Component*>& fanin, std::string fanout, int id, 
                Module* module)
{
    mModule=module;
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMRequiredFanin(2);
    SetMType(CONCAT);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have two fanin components so lets add these to our new Add Function.
    //First of all we allocate space for the Vector mFanin with undefined
    //fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //We use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to position i. 
    mFanoutWidth=0;
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //set the new fanout width
        mFanoutWidth+=fanin[i]->GetMFanoutWidth();
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mcRequiredFanin;i++)
} //End Concat(vector<Component> fanin)

/**
 * Methods
 */
/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of 
 * mInputs have to be updated by fetching the result (mOutput) of the 
 * operations in the fanin.
 */
void Concat::SimulateForward () 
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
} //End SimulateForward


/**
 * Perform a simulation of this Concat for a given vector of inputs.
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void Concat::Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
{
    //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for
    //We reset mOutput
    outVals.clear();
    //First we add the inputs of the Input at pos=1 to the lower bits of the
    //result
    for (unsigned int i=0;i<inpVals[1].size();i++)
    {
        outVals.push_back(inpVals[1][i]);
    } //End for (int i=0;i<mInputs[0].size();i++)
    //Now we add the inputs of the Input at pos=0 to the higher bits of the
    //result
    for (unsigned int i=0;i<inpVals[0].size();i++)
    {
        outVals.push_back(inpVals[0][i]);
    } //End for (unsigned int i=0;i<mInputs[0].size();i++)
} //End Simulate

/**
 * Get a vector of the RtpLits defined by this Concat.
 * For this Concat these are equal to the lits of the inputs.
 * @param r the vector where the defined RtpLits will be stored in.
 */
void Concat::GetFanoutRtpLits(std::vector<satlib::RtpLit>& r)
{
    mFanin[1]->GetFanoutRtpLits(r);
    std::vector<satlib::RtpLit> r1;
    mFanin[0]->GetFanoutRtpLits(r1);
    for(unsigned int i=0;i<r1.size();i++)
    {
        r.push_back(r1[i]);
    }
}


void Concat::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_concat(btor, mFanin[0]->GetBtorComp(), mFanin[1]->GetBtorComp());
	BtorComp = comp;
}
