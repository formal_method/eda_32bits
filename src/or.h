/*! ************************************************************************
    \file or.h
    \brief Header file of the Class Or.

    The class contained in this file defines the attributes and methods of the
 *  Function Or.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Thu Feb 09 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef OR_H
#define OR_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"
#include "../exceptions/defaultexception.h"

/**
 * Class to describe the behaviour of the Or Function.
 * This class implements the Function Or derived from the class
 * Function.
 */
class Or : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The Or Function needs 2 Fanins that can be handled equally.
     */
    Or(){SetMRequiredFanin(2);SetMType(OR);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * An Or has two fanin Components so initialize mRequiredFanin with 2. The
     * order of the fanin components doesn't matter for the Or Function.
     * Add the fanin components to the fanin of this Or.
     * @param fanin A vector containing the two Fanin Components of the Or
     * @param fanout The string naming the output
     * @param id The id of this Or
     * @param module The Module the Or shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     */
    Or(std::vector<Component*>& fanin, std::string fanout, int id, 
       Module* module);
    
    /**
     * Destructor
     */
    virtual ~Or(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this Or as a string (OR)
     * @return The string "OR"
     */
    inline std::string GetType(){return "OR";}
    /**
     * Operations
     */

    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();
    
    /**
     * Perform a simulation of this Or for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );
    
    /**
     * Create the CNF for this Or.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor);

/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */


    /**
     * Operations
     */
};
#endif //OR_H

