/*! ************************************************************************
    \file not.h
    \brief Header file of the Class Not.

    The class contained in this file defines the attributes and methods of the
 *  Function Not.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Thu Feb 09 2006

Modification history:

**************************************************************************/

#ifndef NOT_H
#define NOT_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class to describe the behaviour of the Not Function.
 * This class implements the Function Minus derived from the class
 * Not.
 */
class Not : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The Not Function needs 1 Fanin.
     */
    Not(){SetMRequiredFanin(1);SetMType(NOT);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * A Not has one fanin Component so initialize mRequiredFanin with 1.
     * Add the fanin component to the fanin of this Not.
     * @param fanin A vector containing the Fanin Component of the Not. 
     * @param fanout The string naming the output
     * @param id The ID of this Not
     * @param module The Module the Not shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     */
    Not(std::vector<Component*>& fanin, std::string fanout, int id, 
        Module* module);
    
    /**
     * Destructor
     */
    virtual ~Not(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this component as a string (NOT)
     * @return The string "NOT"
     */
    inline std::string GetType(){return "NOT";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();

    /**
     * Perform a simulation of this Not for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );
    
    /**
     * Get a vector of the RtpLits defined by this Not.
     * For a Not the RtpLits are equivalent to the negated input lits.
     * @param r the vector where the defined RtpLits will be stored in.
     */
    virtual void GetFanoutRtpLits(std::vector<satlib::RtpLit>& r);
    
    /**
     * Create the CNF for this Not.
     * No CNF needed for a not constraint this is done by passing the negated 
     * literals of the fanin.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver){}
    
    /**
     * C.Villarraga 24.03.2011: Added this re-implementation from the Component class
     * Get the assignment of the output of this Component. This implementation is required
     * because this component doesnt have own literals (i.e no CNF)
     * @param satSolver The sat solver that retrieved the solution
     * @param assignment the assignment of the output shall be returned here
     */
    virtual void GetFanoutAssignment(satlib::SatI& satSolver,std::vector<satlib::SatI::lValue>& assignment)
    {
        // Get RtpLits of the FanIn since this kind of
        // component doesnt have own RtpLits
        std::vector<satlib::RtpLit> a;
        mFanin[0]->GetFanoutRtpLits(a);

        // Now get the assigment for the FanIn
        std::vector<satlib::SatI::lValue> fanin_assgn;
        for(unsigned int i=0; i<a.size();i++)
        	fanin_assgn.push_back(satSolver.getValue(a[i]));

        // Perform simulation to get the assigment values for this component
        std::vector< std::vector< satlib::SatI::lValue> > inputVals;
        inputVals.push_back(fanin_assgn);
        Simulate(inputVals, assignment);
    }

	void Create_BtorComp(Btor* btor);

/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */

    /**
     * Operations
     */
};
#endif //NOT_H

