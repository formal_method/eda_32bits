/*! ************************************************************************
    \file multiplier.h
    \brief Header file of the Class Multiplier.

    The class contained in this file defines the attributes and methods of the
 *  Function Multiplier.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Tue Feb 14 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component


**************************************************************************/

#ifndef MULTIPLIER_H
#define MULTIPLIER_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"
#include "../exceptions/defaultexception.h"
#include "SatI.h"

/**
 * Class to describe the behaviour of the Multiplier Function.
 * This class implements the Function Multiplier derived from the class
 * Function.
 */
class Multiplier : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The Multiplier Function needs 2 Fanins that can be handled equally.
     */
    Multiplier(){SetMRequiredFanin(2);SetMType(MULT);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * A Multiplier has two fanin Components so initialize mRequiredFanin with 2. 
     * The order of the fanin components doesn't matter for the Multiplier
     * Function. Add the fanin components to the fanin of this Multiplier.
     * @param fanin A vector containing the two Fanin Components of a Multiplier
     * @param fanout The string naming the output
     * @param id The Id of this Multiplier
     * @param module The module the Multiplier shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     * @exception InputSizeException is thrown if the fanins do not have the same 
     * size -> extend to achieve this
     */
    Multiplier(std::vector<Component*>& fanin, std::string fanout, int id, 
               Module* module);
    
    /**
     * Destructor
     */
    virtual ~Multiplier(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this Multiplier as a string (MULT)
     * @return The string "MULT"
     */
    inline std::string GetType(){return "MULT";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();

    /**
     * Perform a simulation of this Multiplier for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );
    
    /**
     * Create the CNF for this Multiplier.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor);
	BtorNode* Get_uext1(){return uext1;}
	BtorNode* Get_uext2(){return uext2;}

/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * cac BtorNode fanin cua bo nhan, chung chinh la cac bo mo rong bit 0
     */
	BtorNode* uext1, *uext2;
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */

    /**
     * Operations
     */
};
#endif //MULTIPLIER_H

