/*! ************************************************************************
    \file circuit.h 
    \brief Headerfile Class Circuit describing the behaviour of a circuit.
    
    Describes the methods neeeded to setup circuit and work on it.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Mon Jan 30 - Wed Feb 15 2006

Modification history:
 * 17.02.06 changed return type of AddComponent from bool to void. Whether an 
 * add is successful or not is now handled by exceptions

**************************************************************************/

#ifndef CIRCUIT_H
#define CIRCUIT_H
#include <string>
#include "circuitbuilder.h"
#include <vector>
#include <map>
#include "../exceptions/defaultexception.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/fanoutmapmiss.h"
#include "../exceptions/faninnumberexception.h"
#include "module.h"

/**
 * Constant values to define rising and falling clocks contained in the 
 * circuit
 * @var FALLING=1
 * @var RISING=2
 * @var BOTH=3
 */
const int FALLING=1, RISING=2, BOTH_=3;

/**
 * Class Circuit provides methods to set up a circuit and to work on it.
 * Class containing the Components of the circuit and methods to add new
 * components to circuit as well as methods to work on this circuit.
 */
class Circuit : public CircuitBuilder {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Default Constructor.
     * Set up a Vector containing a Component of each
     * operation so that the number of needed fanin Components can easily be
     * determined. 
     */
    Circuit()
    {
        this->mNextModuleId = 1;
        this->mNextMId = 1;
        this->mTopmodule = 0;
    }
    
    /**
     * Destructor
     */
    ~Circuit()
    {
     /*
        for ( std::map<std::string, Module*>::iterator it = mModuleMap.begin(); it != mModuleMap.end(); ++it )
            delete it->second;
     */ 
        mModuleMap.clear();
    }
    /**
     * Accessor Methods
     */
    
    /**
     * Add a new Component to a module of the Circuit.
     * First of all it has to be checked whether the circuit contains a Module
     * with the name supplied. If this is not the case a new Module has to be 
     * created. The Component is than added to the given Module.
     * @return If the Component was successfully created true is returned. If 
     * the Component can not yet be created (missing fanout) false is 
     * returned.
     * @param type An integer representing the kind of the Component that 
     * shall be added (see CircuitBuilder for the mapping of value and type).
     * @param fanin A vector containing the names of the fanin Components that
     * support the new component. For details on the correct ordering of the 
     * fanin components please take a look at the subclass implementing the 
     * Component you want to add.
     * @param fanout A string with the name of the fanout of the new component
     * that shall be added.
     * @param module The module the Component shall be added to
     * \todo replace map by hash_map
     */
    FckId AddComponent (int type, const std::vector<FckId>& fanin, 
                       const std::string& componentName, ModuleId module);
    
    /**
     * Fetch a Component from a given Module at the given position.
     * @param pos The position of the component that shall be returned.
     * @param module The Module the Component shall be fetched from.
     * @return the component at the specified position.
     */
    Component* GetComponent(int pos, ModuleId module)
        {return mModuleMap[module]->GetComponent(pos);}
    
    /**
     * Fetch a Component by its name from a given Module.
     * @param name The name of the component that shall be returned.
     * @param module The module the Component shall be fetched from.
     * @return the component with the given name.
     */
    Component* GetComponent(std::string componentName, ModuleId module)
        {return mModuleMap[module]->GetComponent(componentName);}
    
    /**
     * Fetch a Component by its id from a given Module.
     * @param name The name of the component that shall be returned.
     * @param module The module the Component shall be fetched from.
     * @return the component with the given name.
     */
    Component* GetComponent(FckId id, ModuleId module)
        {return mModuleMap[module]->GetComponent(id);}
    
    /**
     * Set the values of the inputs of the circuit.
     * @param in the values the inputs shall take. The value at in[0] will be
     * applied to the Input at mComponent[0] and so on  
     * @param module The Module the inputs shall be set for.
     */
    void SetInputs(std::vector< std::vector< satlib::SatI::lValue> >& in, 
                   ModuleId module){mModuleMap[module]->SetInputs(in);}
    
    /**
     * Reorder all modules
     */
    void Reorder();
    
    /**
     * Reorder a Module to get the Components in the correct order.
     * If the Components are not stored in the correct order (e.g. after a map
     * of a submodule) the circuit has to be reordered
     */
    void Reorder(ModuleId module){mModuleMap[module]->Reorder();}
    
    /**
     * Simulate a Component of a Module for a vector of inputs values.
     * @param component The number of the Component that shall be simulated
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     * @param module The Module that shall be simulated
     */
    virtual void SimulateComponent // C.Villarraga 23.03.2011: Name changed from Simultate to SimulateComponent
            (int component, ModuleId module,
             std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
    {mModuleMap[module]->SimulateComponent(component, inpVals, outVals);}


    /**
     * C.Villarraga: Created this method
     * Performs simulation of a module (module has to be already ordered)
     * using the model from the SAT solver. There is another faster alternative
     * to make this using the method Component::GetFanoutAssignment for every component
     * of the Module.
     * @param SatSolver The SAT solver that has the solution
     */
    virtual void ProcessModuleCountexmp(ModuleId module, satlib::SatI& SatSolver)
    {mModuleMap[module]->SimulateProcessedCounterEx(SatSolver);}

    
    /**
     * Get the bitwidth of a variable in a Module.
     * @param var the variable the width shall be determined for
     * @param module The Module the variable is in
     * @return the width of the variable
     */
    int GetWidth(FckId id, ModuleId module)
        {return mModuleMap[module]->GetWidth(id);}
    
    /**
     * Get the clock edges contained in the circuit.
     * @return an integer indicating whether the registers of the circuit 
     * update their value on a falling clock (FALLING=1), a clock rise 
     * (RISING=2) or whether there are some registers updating with a falling 
     * and others updating on a rising edge (BOTH=3) 
     */
    int GetEdges(){return mContainedEdge;}
    
    /**
     * Get the ID for the next component in the circuit.
     * Each component in the circuit gets a unique ID. This method determines 
     * the ID the next component shall get.
     * @return an integer value for the ID the next Component will get.
     */
    FckId GetNextId(){return mNextMId++;}
    
    /**
     * Get the Topmodule
     * @return The topmodule
     */
    Module* GetTopmodule(){return mModuleMap[mTopmodule];}
    
    /**
     * Set the Topmodule
     * @param module The ID of the Module
     */
    void SetTopmodule(const ModuleId id);
    
    /**
     * Get a Module by its index
     * @param module The name of the Module
     * @return The requested Module
     */
    Module* GetModuleByIndex(int i);

    /**
     * Get a Module by its id
     * @param module The name of the Module
     * @return The requested Module
     */
    inline Module* GetModule(const ModuleId module)
    {return mModuleMap[module];}

    /**
     * Operations
     */
    /**
     * Check whether the Circuit is complete.
     * Method to check whether a Circuit has been successfully created after 
     * all Components have been added. 
     * @return True if every Component has all required fanin Components and 
     * at least one fanout Component. To check this the method isComplete of 
     * every Component in mComponents is called.
     */
    bool IsComplete ();
    
    /**
     * Check whether a Module is complete.
     * Method to check whether a Module has been successfully created after 
     * all Components have been added. 
     * @return True if every CompCircuitonent has all required fanin Components and
     * at least one fanout Component. To check this the method isComplete of 
     * every Component in mComponents is called.
     */
    bool IsComplete (ModuleId module)
        {return mModuleMap[module]->IsComplete();}
    
    /**
     * Determine how many Modules the Circuit consists of.
     * @return the number of Modules in the Circuit
     */
    unsigned int Size(){return mModuleMap.size();}
    
    /**
     * Determine how many Components a Module consists of.
     * @return the number of Components in the Module
     */
    unsigned int Size(ModuleId module){return mModuleMap[module]->Size();}
    

    // convert FCK Circuit to Verilog code
    // author: quandv
    // date: 03-10
    void Circuit2Verilog(std::string path);

    /** 
     * Method to print which components the circuit contains.
     * Prints all components of the circuit with their type, fanin components
     * and fanout name.
     */
    void Print(std::string path);
    
    /** 
     * Method to print which components the circuit contains.
     * Prints all components of the circuit with their type, fanin components
     * and fanout name.
     * @param module The Module that shall be printed 
     */
     void Print(ModuleId module, std::string path){mModuleMap[module]->Print(path);}
    
    /**
     * Set whether a register updates its value on a falling or rising edge.
     * @param edge Can take the defined integer values FALLING(=1), RISING(=2)
     * or BOTH(=3) depending on the edge behaviour the register shall be set 
     * to.
     * @param cmp a string containing the name of the PseudoOutput whose egde
     * shall be set.
     * @param module The Module the Edge shall be set for.
     */
    void AddEdge(int edge, FckId id, ModuleId module);
    
   /**
     * Get the next Module ID
     * @return the next Module Id
     */
    ModuleId GetNextModuleId(){ return mNextModuleId++; }
    void SetModuleMap(std::map<ModuleId,Module*> pmap) {
    	mModuleMap = pmap;
    }

    // quandvk54 14-04
    std::map<ModuleId, Module*> GetModuleMap(){ return mModuleMap;}

    // ham delete cac component va fanin cua no
    void deletecomp(Component* comp );

/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * A map for the Modules contained in the Circuit. 
     * As each Module has a unique name this Name is used as the key.
     */
    std::map<ModuleId, Module*> mModuleMap;

     
    
    /**
     * store what kind of egdes are used in this circuit
     */
    int mContainedEdge; 
    /**
     * the module id counter
     */
    ModuleId mNextModuleId;
    
    /**
     * store the ID the next component added will get
     */
    FckId mNextMId;
    
    /**
     * the name of the topmodule
     */
    ModuleId mTopmodule;
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
};
#endif //CIRCUIT_H
