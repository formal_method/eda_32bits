/*! ************************************************************************
    \file shiftleft.h
    \brief Header file of the Class ShiftLeft.

    The class contained in this file defines the attributes and methods of the
 *  Function ShiftLeft.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Fri Feb 10 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef SHIFTLEFT_H
#define SHIFTLEFT_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/defaultexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class to describe the behaviour of the ShiftLeft Function.
 * This class implements the Function ShiftLeft derived from the class
 * Function.
 */
class ShiftLeft : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The ShiftLeft Function needs 2 Fanins. The first fanin is the data 
     * input that shall be shifted. The second input is the amount of bits
     * that have to be shifted.  
     */
    ShiftLeft(){SetMRequiredFanin(2);SetMType(SHL);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * A ShiftLeft has two fanin Components so initialize mRequiredFanin with 2. 
     * The first input is the data input to be shifted the second input specifies 
     * the number of bits that have to be shifted.
     * Add the fanin components to the fanin of this ShiftLeft.
     * @param fanin A vector containing the two Fanin Components of the ShiftLeft
     * @param fanout The string naming the output
     * @param id The ID of this ShiftLeft
     * @param module The Module the ShiftLeft shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     */
    ShiftLeft(std::vector<Component*>& fanin, std::string fanout, int id, 
              Module* module);
    
    /**
     * Destructor
     */
    virtual ~ShiftLeft(){}

    /**
     * Accessor Methods
     */
    /**
     * Return the type of this ShiftLeft as a string (SHL)
     * @return The string "SHL"
     */
    inline std::string GetType(){return "SHL";}


    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();
    
    /**
     * Perform a simulation of this ShiftLeft for a given vector of inputs.
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );  
    
    /**
     * Create the CNF for this ShiftLeft.
     * Do nothing as we do not need a CNF representation for a ShiftLeft. This 
     * is handled by passing the rtp lits through from the inputs to the 
     * outputs.
     * @param solver the interface to the solver
     */
    virtual void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor);

/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
    /**
     * recursive method for CreateCnf
     */
    void makeShl ( satlib::SatI& satSolver, satlib::RtpLit sel,
                   std::vector<satlib::RtpLit>& res,
                   std::vector<satlib::RtpLit>& inp, unsigned shiftBy);
};
#endif //SHIFTLEFT_H

