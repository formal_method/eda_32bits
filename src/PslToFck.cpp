#include "PslToFck.h"
#include <sstream>
#include "module.h"
#include <fstream>
#include <string>
#include <stack>
#include <vector>
#include <cstdlib>
#include "../lib/pslparser/PslFactoryI.h"

PslToFck::PslToFck(std::vector<PslProperty*>* properties, Circuit* circuit)
{
   mProperties = properties;
   mWidthCircuit = circuit;
   CurrentProperty = 0;
   mCurrentProperty = new PslProperty();
   mTimeFrame = 0;
}

FckId PslToFck::MakeAndType(FckId id1,FckId id2, std::string type, std::string name)
{
   std::vector<FckId> fanins;
   fanins.push_back(id1);
   fanins.push_back(id2);
   type = LowerCase(type);
   if (type.compare("and")==0)
   {
      return mCurrentProperty->AddComponent(AND,fanins,name);
   }
   if (type.compare("nand")==0)
   {
      FckId id = mCurrentProperty->AddComponent(AND,fanins,"");
      fanins.clear();
      fanins.push_back(id);
      return mCurrentProperty->AddComponent(NOT,fanins,name);
   }
   return 0;
}

FckId PslToFck::MakeOrType(FckId id1,FckId id2, std::string type, std::string name)
{
   std::vector<FckId> fanins;
   fanins.push_back(id1);
   fanins.push_back(id2);
   type = LowerCase(type);
   if (type.compare("or")==0)
   {
      return mCurrentProperty->AddComponent(OR,fanins,name);
   }
   if (type.compare("nor")==0)
   {
      FckId id = mCurrentProperty->AddComponent(OR,fanins,"");
      fanins.clear();
      fanins.push_back(id);
      return mCurrentProperty->AddComponent(NOT,fanins,name);
   }
   if (type.compare("xor")==0)
   {
      return mCurrentProperty->AddComponent(XOR,fanins,name);
   }
   if (type.compare("xnor")==0)
   {
      FckId id = mCurrentProperty->AddComponent(XOR,fanins,"");
      fanins.clear();
      fanins.push_back(id);
      return mCurrentProperty->AddComponent(NOT,fanins,name);
   }
   return 0;
}

FckId PslToFck::MakeArith(FckId id1,FckId id2, std::string type, std::string name)
{
   std::vector<FckId> fanins;
   fanins.push_back(id1);
   fanins.push_back(id2);
   type = LowerCase(type);
   if (type.compare("add")==0)
   {
      return mCurrentProperty->AddComponent(ADD,fanins,name);
   }
   if (type.compare("mult")==0)
   {
      return mCurrentProperty->AddComponent(MULT,fanins,name);
   }
   return 0;
}

void PslToFck::FinishProperty()
{
   // Finish propery and prepare new
   mCurrentProperty->FinishProperty();
   mProperties->push_back(mCurrentProperty);
   mCurrentProperty = new PslProperty();
   CurrentProperty++;
   mTimeFrame = 0;
}

int PslToFck::MakeTimeFrameChange(std::string type, int change)
{
   // return change/-change to restore timeframe later
   type = LowerCase(type);
   if (type.compare("next")==0)
   {
      mTimeFrame += change;
      return -change;
   }
   if (type.compare("prev")==0)
   {
      mTimeFrame -= change;
      return change;
   }
   return 0;
}

FckId PslToFck::MakeIte(FckId id1,FckId id2,FckId id3, std::string name)
{
   std::vector<FckId> fanins;
   fanins.push_back(id1);
   fanins.push_back(id2);
   fanins.push_back(id3);
   return mCurrentProperty->AddComponent(ITE,fanins,name);
}

FckId PslToFck::MakeIt(FckId id1,FckId id2, std::string name)
{
   std::vector<FckId> fanins;
   fanins.push_back(id1);
   FckId id = mCurrentProperty->AddComponent(NOT,fanins,"");
   fanins.clear();
   fanins.push_back(id);
   fanins.push_back(id2);
   return mCurrentProperty->AddComponent(OR,fanins,name);
}

FckId PslToFck::MakeTemporal(FckId id1, std::string type, std::string name)
{
   // not yet implemented
   std::vector<FckId> fanins;
   fanins.push_back(id1);
   type = LowerCase(type);
   FckId id = mCurrentProperty->CloneComponentTimeFrameEarlier(id1);

   int minTimeFrame = mCurrentProperty->GetMinTimeFrame();

   mTimeFrame -= minTimeFrame;

   if (minTimeFrame<0)
      mCurrentProperty->ShiftTimeTable(-minTimeFrame);

   fanins.clear();
   if (type.compare("fell")==0)
   {
      fanins.push_back(id1);
      id1 = mCurrentProperty->AddComponent(NOT,fanins,"");
      fanins.clear();
      fanins.push_back(id);
      fanins.push_back(id1);
      return mCurrentProperty->AddComponent(AND,fanins,name);
   }
   if (type.compare("rose")==0)
   {
      fanins.push_back(id);
      id1 = mCurrentProperty->AddComponent(NOT,fanins,"");
      fanins.clear();
      fanins.push_back(id);
      fanins.push_back(id1);
      return mCurrentProperty->AddComponent(AND,fanins,name);
   }
   if (type.compare("stable")==0)
   {
      fanins.push_back(id);
      fanins.push_back(id1);
      return mCurrentProperty->AddComponent(EQ,fanins,name);
   }
   return 0;
}

FckId PslToFck::MakeExpression(std::string id)
{
   // if timeframe<0 -> shift all known timeframes such that all known and new timeframes are >= 0 
   if (mTimeFrame<0)
   {
      mCurrentProperty->ShiftTimeTable(-mTimeFrame);
      mTimeFrame = 0;
   }
   return mCurrentProperty->GetVariable(id, mTimeFrame, mWidthCircuit);
}

FckId PslToFck::MakeConst(std::string id)
{
   // check if id is binary or integer form
   if (id[0] != '\"')
   {
      id = StrToBin(id);
   }
   else
   {
       id = id.substr(1,id.length()-2);
   }
   return mCurrentProperty->GetConstant(id);
}

FckId PslToFck::MakeShift(FckId id1,FckId id2, std::string type, std::string name)
{
   std::vector<FckId> fanins;
   fanins.push_back(id1);
   fanins.push_back(id2);
   type = LowerCase(type);
   if (type.compare("shr")==0)
   {
      return mCurrentProperty->AddComponent(SHR,fanins,name);
   }
   if (type.compare("shl")==0)
   {
      return mCurrentProperty->AddComponent(SHL,fanins,name);
   }
   return 0;
}

FckId PslToFck::MakeExtend(FckId id1,FckId id2, std::string type, std::string name)
{
   std::vector<FckId> fanins;
   fanins.push_back(id1);
   fanins.push_back(id2);
   type = LowerCase(type);
   if (type.compare("zero_extend")==0)
   {
      return mCurrentProperty->AddComponent(ZERO_EXTEND,fanins,name);
   }
   if (type.compare("sign_extend")==0)
   {
      return mCurrentProperty->AddComponent(SIGN_EXTEND,fanins,name);
   }
   return 0;
}

FckId PslToFck::MakeCompare(FckId id1,FckId id2,std::string type, std::string name)
{
   std::vector<FckId> fanins;
   fanins.push_back(id1);
   fanins.push_back(id2);
   type = LowerCase(type);
   if (type.compare("==")==0)
   {
      return mCurrentProperty->AddComponent(EQ,fanins,name);
   }
   if (type.compare("!=")==0)
   {
      FckId id = mCurrentProperty->AddComponent(EQ,fanins,"");
      fanins.clear();
      fanins.push_back(id);
      return mCurrentProperty->AddComponent(NOT,fanins,name);
   }
   if (type.compare(">=")==0)
   {
      FckId id = mCurrentProperty->AddComponent(LT,fanins,"");
      fanins.clear();
      fanins.push_back(id);
      return mCurrentProperty->AddComponent(NOT,fanins,name);
   }
   if (type.compare(">")==0)
   {
      FckId id = MakeCompare(id1, id2, "<=", "");
      fanins.clear();
      fanins.push_back(id);
      return mCurrentProperty->AddComponent(NOT,fanins,name);
   }
   if (type.compare("<=")==0)
   {
      FckId id = mCurrentProperty->AddComponent(LT,fanins,"");
      FckId id1 = mCurrentProperty->AddComponent(EQ,fanins,"");
      fanins.clear();
      fanins.push_back(id);
      fanins.push_back(id1);
      return mCurrentProperty->AddComponent(OR,fanins,name);
   }
   if (type.compare("<")==0)
   {
      return mCurrentProperty->AddComponent(LT,fanins,name);
   }
   return 0;
}

FckId PslToFck::MakeConcat(FckId id1,FckId id2, std::string name)
{
   std::vector<FckId> fanins;
   fanins.push_back(id1);
   fanins.push_back(id2);
   return mCurrentProperty->AddComponent(CONCAT,fanins,name);
}

FckId PslToFck::MakeSlice(FckId id1,int id2,int id3, std::string name)
{
   std::vector<FckId> fanins;
   fanins.push_back(id1);
   FckId id = MakeConst(IntToStr(id3)); // first bit // Son Lam id2 ->id3
   fanins.push_back(id);
   id = MakeConst(IntToStr(id2-id3+1)); // bitwidth 
   fanins.push_back(id);
   return mCurrentProperty->AddComponent(SLICE,fanins,name);
}

void  PslToFck::MakeCondition(FckId id, std::string type, int repeat)
{
   type = LowerCase(type);
   if (type.compare("assume")==0)
   {
	   mCurrentProperty->AddAssumption(id);
	   if (repeat>0){
		  for (int i=0; i<repeat; i++) {
			  FckId id1 = copy_circuitblock(id,i+1);
			  mCurrentProperty->AddAssumption(id1);
		  }
	  }
   }
   if (type.compare("assert")==0)
   {
      mCurrentProperty->AddCommitment(id);
      if (repeat>0){
      		  for (int i=0; i<repeat; i++) {
      			  FckId id1 = copy_circuitblock(id,i+1);
      			  mCurrentProperty->AddCommitment(id1);
      		  }
      	  }
   }
}

FckId PslToFck::MakeNot(FckId id1, std::string name)
{
   std::vector<FckId> fanins;
   fanins.push_back(id1);
   return mCurrentProperty->AddComponent(NOT,fanins,name);
}

FckId PslToFck::MakeMinus(FckId id1, std::string name)
{
   std::vector<FckId> fanins;
   fanins.push_back(id1);
   return mCurrentProperty->AddComponent(MINUS,fanins,name);
}

FckId PslToFck::MakeReduce(FckId id1, std::string type, std::string name)
{
   std::vector<FckId> fanins;
   fanins.push_back(id1);
   type = LowerCase(type);
   if (type.compare("reduce_or")==0)
   {
      return mCurrentProperty->AddComponent(U_OR,fanins,name);
   }
   if (type.compare("reduce_and")==0)
   {
      return mCurrentProperty->AddComponent(U_AND,fanins,name);
   }
   if (type.compare("reduce_xor")==0)
   {
      return mCurrentProperty->AddComponent(U_XOR,fanins,name);
   }
   return 0;
}

void PslToFck::SetPropertyName(std::string name)
{
   mCurrentProperty->SetName(name);
}

std::string PslToFck::IntToStr(int num)
{
   std::stringstream s;
   s << num;
   return s.str();
}

std::string PslToFck::LowerCase(std::string str)
{
   char* buf = new char[str.length()];
   str.copy(buf, str.length());
   for(unsigned i =0;i <str.length(); i++)
      buf[i] = tolower(buf[i]);
   std::string r(buf, str.length());
   delete buf;
   return r;
}

std::string PslToFck::StrToBin(std::string str)
{
   int number;
   int i = atoi(str.c_str());
   std::stringstream s;
   if (i==0) s << i;
   while (i > 0)
   {
      number = i % 2;
      i = i / 2;
      s << number;
   }
   std::stringstream t;
   for (i=(s.str().length());i>=0;i--)
   {
      t << s.str().substr(i,1);
   }
   return t.str().c_str();
}
FckId PslToFck::copy_circuitblock(FckId id, int timepoint)
{
	if (mCurrentProperty->GetComponent(id)->GetType() == "INPUT") {
		std::string name = mCurrentProperty->GetComponent(id)->GetMInputName();
		int timeframe = mCurrentProperty->GetTimeFrameMap()[id].second + timepoint;
		return mCurrentProperty->GetVariable(name, timeframe, mWidthCircuit);
	}
	else {
		Component *cmp = mCurrentProperty->GetComponent(id);
		std::vector<FckId> fanins;
		FckId id1;
		fanins.clear();
		if (cmp->GetFanin().size()==0)
			id1 = mCurrentProperty->AddComponent(cmp->GetMType(), fanins ,cmp->GetMOutputName());
		else {
			FckId id2;
			fanins.clear();
			for (int t=0; t<cmp->GetFanin().size(); t++) {
				id2 = copy_circuitblock(cmp->GetFanin()[t]->GetMId(), timepoint);
				fanins.push_back(id2);
			}
			id1 = mCurrentProperty->AddComponent(cmp->GetMType(), fanins ,cmp->GetMOutputName());
			}
		return id1;
	}
}
