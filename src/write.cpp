/*! ************************************************************************
    \file write.cpp 
    \brief Implementation of the Class Write

    The class contained in this file implements the methods of the Function
 * Write
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Mon Feb 13 2006 

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout
 * 26.10.06
 * in void Write::Simulate 
 * in for (;i<position && i<(int)inpVals[.].size();i++)
 *    - the type of local variable i was changed from unsigned int to int
 *    - (int) was appended befor inpVals[.].size()
 * 26.10.06 SatI::RtpLit is replaced with RtpLit   

**************************************************************************/

#include "write.h"
#include <math.h>

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * A Write has three fanin Components so initialize mRequiredFanin with 3. The 
 * input at pos=0 is the data input a bit shall be inserted into. The input at
 * pos=0 specifies the position at which the bit shall be inserted.
 * Add the fanin components to the fanin of this Write.
 * @param fanin A vector containing the two Fanin Components of a Write.
 * @param fanout The string naming the output
 * @param id The ID of the Write
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 */
Write::Write(std::vector<Component*>& fanin, std::string fanout, int id, 
          Module* module)
{
    mModule=module;
    SetMRequiredFanin(3);
    SetMType(CONCAT);
    SetMOutputName(fanout);
    mId=id;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have three fanin components so lets add these to our new Write 
    //Function. First of all we allocate space for the Vector mFanin with 
    //undefined fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //We use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to the output at position i. 
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mcRequiredFanin;i++)
    mFanoutWidth=fanin[0]->GetMFanoutWidth();
} //End Write(vector<Component> fanin)
 
/**
 * Methods
 */

/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin.
 * @exception DefaultException for impossible lValue values (this is 
 * impossible)
 */
void Write::SimulateForward () 
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
} //End SimulateForward

/**
 * perform a simulation of this Write for a given vector of inputs
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void Write::Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
{
    //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for//We reset outVals
    outVals.clear();
    //First we have to calculate at which position the bit shall be inserted.
    //Store this value in a new variable position. The value is calculated 
    //by adding up the individual bits multiplied with the magnitude of the 
    //column. The value position=-1 is used to indicate an undefined position.
    int position=0;
    for (unsigned int i=0;i<inpVals[1].size() && position!=-1;i++)
    {
        //retrieve the value of the current bit for FALSE do nothing, for TRUE
        //add 2^i to position. In the case of DONTCARE the number of output 
        //bits is unspecified to indicate this set position to -1 and we leave 
        //the for loop.
        int current = inpVals[1][i];
        switch (current)
        { 
            case 1: position+=1<<i;
                    break;
            case 0: break;
            case 5: position=-1;
            //default case is not possible
            default:
            {
                std::string message="EXCEPTION: Reached an ";
                message+="impossible lValue value";
                throw DefaultException(message);
            }
        }  //End switch (current)
    } //End for (int i=0;i<inpVals[1].size();i++)
    position*=inpVals[2].size();
    if (position >=0)
    {
        //The bits of the data input lower than position stay the same
        int i=0;
        for (;i<position && i<(int)inpVals[0].size();i++)
        {
            outVals.push_back(inpVals[0][i]);
        } //End for (int i=0;i<position && i<inpVals[0].size();i++)
        //at position we push the bit from input 3
        for (;i-position<(int)inpVals[2].size() && i<(int)inpVals[0].size();i++)
            outVals.push_back(inpVals[2][i-position]);
        //the remaining bits are taken from the data input again
        for (;i<(int)inpVals[0].size();i++)
        {
            outVals.push_back(inpVals[0][i]);
        } //End for (int i=position+1;i<inpVals[0].size();i++)
        //Now we add the inputs of the Input at pos=0 to the higher bits of 
        //the result
    } // End if (position >=0)
    else
    {
        //The position is unspecified at unspecified inputs
        for (unsigned int i=0;i<inpVals[0].size();i++)
        {
            outVals.push_back(satlib::SatI::DONTCARE);
        } //End for (int i=0;i<inpVals[0].size();i++)
    }
} //End Write

/**
 * Create the CNF for this Write.
 * @param solver the interface to the solver
 */
void Write::CreateCnf(satlib::SatI& satSolver)
{
    //input a the data that shall be written in
    std::vector<satlib::RtpLit> a;
    //input b specifies the number of the chunk that shall be written
    std::vector<satlib::RtpLit> b;
    //input c the data that shall be written
    std::vector<satlib::RtpLit> c;
    //the result
    std::vector<satlib::RtpLit> r;
    assert(GetFanin().size()==3);
    //get the literals
    GetFanoutRtpLits(r);
    GetFanin()[0]->GetFanoutRtpLits(a);
    GetFanin()[1]->GetFanoutRtpLits(b);
    GetFanin()[2]->GetFanoutRtpLits(c);
    assert(r.size()==a.size());
    assert(b.size()>0);
    int extraBits=GetMFanoutWidth()+1;
    //stop has to be the highest bit that a combination of b and c can create
    unsigned stop = (unsigned)pow(2,b.size())*c.size();
    makeWrite(satSolver,a,b,c,r,0,stop,b.size()-1,extraBits);
}

/**
 * recursive method to build a CNF representation for the Write
 */
void Write::makeWrite ( satlib::SatI& satSolver,
                                  std::vector<satlib::RtpLit>& a,
                                  std::vector<satlib::RtpLit>& b,
                                  std::vector<satlib::RtpLit>& c,
                                  std::vector<satlib::RtpLit>& r,
                                  unsigned start, unsigned stop, int bIdx,
                                  int& extraBits ) 
{
    if(bIdx<=-1)
    {
        assert(stop-start==c.size());
        for(unsigned i=0;i<r.size();i++)
        {
            if(i<start||i>=stop)
            {
                r[i]=a[i];
            }
            else
            {
                r[i]=c[i-start];
            }
        }
    }
    else
    {
        std::vector<satlib::RtpLit> upWrite;
        std::vector<satlib::RtpLit> lowWrite;
        for(unsigned i=0;i<r.size();i++)
        {
            upWrite.push_back(satlib::RtpLit(mId,extraBits++));
            lowWrite.push_back(satlib::RtpLit(mId,extraBits++));
        }
        makeWrite(satSolver,a,b,c,lowWrite,start,start+(stop-start)/2,bIdx-1,
                  extraBits);
        makeWrite(satSolver,a,b,c,upWrite,start+(stop-start)/2,stop,bIdx-1,
                  extraBits);
        for(unsigned i=0;i<r.size();i++)
        {
            if(upWrite[i]!=lowWrite[i])
                satSolver.makeIte(b[bIdx],upWrite[i],lowWrite[i],r[i]);
            else r[i]=upWrite[i];
        }
    }
}
