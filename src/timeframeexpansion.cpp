/*! ************************************************************************
    \file timeframeexpansion.cpp
    \brief Class TimeFrameExpansion implementing the methods of a property checker

    Contains the circuit that shall be checked assumptions and properties are
 * used to build a new circuit with assumption property and the transitive
 * fanin of these
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Mon Apr 07 2006

Modification history:
 * 26.10.06 in void TimeFrameExpansion::AddProperty(Module* prop)
 * the type of local variable i was changed from int to unsigned int
 * 26.10.06 SatI::RtpLit is replaced by RtpLit
 * 17.9.2014 Thay Minisat bang Boolector


**************************************************************************/
#include <sstream>
#include "timeframeexpansion.h"
#include <fstream>
#include "SatI.h"
#include <time.h>
#include <string>
#include <stack>
#include <vector>
#include "PslProperty.h"

#define COMP_NUM_MAX 100000

TimeFrameExpansion::TimeFrameExpansion()
{
}

TimeFrameExpansion::TimeFrameExpansion(const Circuit* circuit, PslProperty*  prop)
{
    std::map<FckId,std::pair<FckId, int> > TimeFrameMap;
    TimeFrameMap = prop->GetTimeFrameMap();
    NetlistKind = 2;
    Init(circuit, prop, TimeFrameMap);
}

TimeFrameExpansion::TimeFrameExpansion(const Circuit* circuit, Module*  prop, std::map<FckId,std::pair<FckId, int> > TimeFrameMap)
{
    NetlistKind = 2;
    Init(circuit, prop, TimeFrameMap);
}

void TimeFrameExpansion::Init(const Circuit* circuit, Module*  prop, std::map<FckId,std::pair<FckId, int> > TimeFrameMap)
{
   mCircuitToExpand = circuit;
   mModuleToExpand=const_cast<Circuit*>(circuit)->GetTopmodule();

   //sonlam 2/10/2014 xoa dong lenh in ra man hinh
/*   std::cout << "TimeFrameMap:" << std::endl;
   for (std::map<FckId,std::pair<FckId, int> >::iterator iter=TimeFrameMap.begin(); iter!=TimeFrameMap.end(); iter++)
   {
       //std::cout << "   Property Id: " << iter->first << " Circuit Id " << iter->second.first << " Time Frame: " << iter->second.second << std::endl;
       printf("   Property Id: %3u Circuit Id: %3u Time Frame: %2u\n", iter->first, iter->second.first, iter->second.second);
   }
   std::cout << std::endl;
   */

   for (unsigned i = 0; i < mModuleToExpand->Size(); i++)
   {
      Component* cmp = mModuleToExpand->GetComponentByIndex(i);
      if (cmp->GetMType()==PS_INPUT)
      {
         std::string name = cmp->GetMInputName();
         //name.erase(0,10); Edited by Son Lam 23/4/2014
         for (unsigned j = mModuleToExpand->Size()-1; j > 0; j--)
         {
            Component* cmp2 = mModuleToExpand->GetComponentByIndex(j);
            if (cmp2->GetMType()==PS_OUTPUT)
            {
               std::string name2 = cmp2->GetMOutputName();
               //name2.erase(0,7);  Edited by Son Lam 23/4/2014
               if (name.compare(name2)==0)
               {
                  mResetMap[cmp->GetMId()] = std::make_pair<FckId, FckId>(cmp2->GetReset(),cmp2->GetResetValue());
                  mJumpMap[cmp->GetMId()]=cmp2->GetMId();
               }
            }
         }
      }
   }

   for (unsigned k = 0; k < const_cast<Circuit*>(circuit)->Size(); k++)
   {
      Module* cur_module = const_cast<Circuit*>(circuit)->GetModuleByIndex(k);
      for (unsigned i = 0; i < cur_module->Size(); i++)
      {
         Component* cmp = cur_module->GetComponentByIndex(i);
         if (cmp->GetMType()==M_INPUT)
         {
            std::vector<Component*> fanin;
            std::vector<Component*> module_fanin;
            fanin = cmp->GetFanin();
            module_fanin = cmp->GetModuleInputs();
            for (unsigned j=0; j<fanin.size(); j++)
            {
               mModuleJumpMap[module_fanin[i]->GetMId()]=fanin[i];
            }
         }
         if (cmp->GetMType()==M_OUTPUT)
         {
            Component* tmp_cmp = cmp->GetModuleOutput();
            mModuleJumpMap[cmp->GetMId()]=tmp_cmp;
         }
      }
   }

   mExpandedTimeFrames = 0;

   for (unsigned i = 0; i < prop->Size(); i++)
   {
      Component* cmp = prop->GetComponentByIndex(i); 
      if (cmp->GetMType() == INPUT)
      {
         std::pair<FckId, int> pair = TimeFrameMap[cmp->GetMId()];
         if (pair.second>mExpandedTimeFrames)
         {
            mExpandedTimeFrames = pair.second;
         }
      }
   }

   for (unsigned i = 0; i < prop->Size(); i++)
   {
      Component* cmp = prop->GetComponentByIndex(i);
      if ((cmp->GetMType() == C_INPUT) || (cmp->GetMType() == INPUT))
      {
         if (cmp->GetMType() == INPUT)
         {
        	 std::pair<FckId, int> pair = TimeFrameMap[cmp->GetMId()];
            Component* cmp2 = mModuleToExpand->GetComponent(pair.first);
            FckId id = AddComponentTimeFrame(cmp2, pair.second);
            mPropMap[cmp->GetMId()] = id;
         }
         else
         {
        	 std::vector<FckId> fanins;
        	 FckId id = AddComponent(C_INPUT, fanins,cmp->GetMInputName());
        	 /*std::pair<FckId,int> pair;
        	 pair.first = id;
        	 for (int i = 0; i < mExpandedTimeFrames; i++)
        	 {
        		 pair.second = i;
        		 mTimeFrameMap[pair] = id;
        	 }*/ //edited by Son Lam 25/4/2014
        	 mPropMap[cmp->GetMId()] = id;
         }
      }
      else
      {
         std::vector<Component*> fanin;
         std::vector<FckId> fanins;
         fanin = cmp->GetFanin();
         for (unsigned j = 0; j < fanin.size(); j++)
         {
            fanins.push_back(mPropMap[fanin[j]->GetMId()]);
         }
         FckId id = AddComponent(cmp->GetMType(), fanins, cmp->GetMOutputName());
         this->GetComponent(id)->Setvalidname(0);
         mPropMap[cmp->GetMId()] = id;
      }
   }

   mPropOut = mComponents[mComponents.size()-1];
   Reorder();
   //sonlam xoa 2/10/2014
   //Print();
}

bool TimeFrameExpansion::Solve_MiniSat(satlib::MinisatI& satSolver)
{
    std::vector<FckId> fanins;
    std::string name;
    if (mPropOut == 0)
       std::cout << "WARNING! mPropOut is NULL" << std::endl << std::flush;
    fanins.push_back(mPropOut->GetFanin()[0]->GetMId());
    name = "NOTPROP";
    FckId id = AddComponent(NOT,fanins,name);
    fanins.clear();
    fanins.push_back(id);
    name = "SAT?";
    id = AddComponent(OUTPUT,fanins,name);
    //Print();
    CreateCnf(satSolver);
    std::vector<satlib::RtpLit> rtp_minus;
    rtp_minus.push_back(satlib::RtpLit(0,-1));
    satSolver.addClause(rtp_minus);
    std::vector<satlib::RtpLit> prop;
    GetComponent(id)->GetFanoutRtpLits(prop);
    satSolver.addClause(prop);
    std::vector<unsigned> assumpts;
    return satSolver.solve(assumpts);
}


bool TimeFrameExpansion::Solve_Btor(Btor* btor)
{
    std::vector<FckId> fanins;
    std::string name;
    BtorNode* output;
    int result;
    if (mPropOut == 0)
       std::cout << "WARNING! mPropOut is NULL" << std::endl << std::flush;
    fanins.push_back(mPropOut->GetFanin()[0]->GetMId());
    name = "NOTPROP";
    FckId id = AddComponent(NOT,fanins,name);
    GetComponent(id)->Setvalidname(0); //sonlam edited 23/3/15
    fanins.clear();
    fanins.push_back(id);
    name = "SAT?";
    id = AddComponent(OUTPUT,fanins,name);
    GetComponent(id)->Setvalidname(0); //sonlam edited 23/3/15
    //sonlam xoa 2/10/2014
    //Print();
    output = FCKtoBtor(btor);

    /* We assert the formula and call Boolector */
    boolector_assert (btor, output);
    result = boolector_sat (btor);
    return(result == BOOLECTOR_SAT);
}


void TimeFrameExpansion::PrintSolution_MiniSat(satlib::SatI& satSolver, std::string module_name, std::string vcdname){
	using namespace std;
	unsigned int input_cnt=0,funct_cnt=0;
	for(unsigned int i=0;i<mComponents.size();i++)
	{
		if(mComponents[i]->GetMType()==INPUT)
			input_cnt++;
	}
	//to set up the value for the inputs using the result of satsolver
   //first to push empty row
	std::vector< std::vector<satlib::SatI::lValue> > inputMatrix;
	std::vector<satlib::SatI::lValue> row;
	for(unsigned int i=0;i<input_cnt;i++)
	 {
		inputMatrix.push_back(row);
	 }
	//then to push back the values and set to the corresponding input
	int q=0;int id=0;int width=0;
	vector<string> input_string;
	for(unsigned int i=0;i<mComponents.size();i++)
	{
		if(mComponents[i]->GetMType()==INPUT)
		{
			id=mComponents[i]->GetMId();
			width=mComponents[i]->GetMFanoutWidth();
			for(unsigned int j=1;j<=width;j++)
			 {
				inputMatrix[q].push_back(satSolver.getValue(satlib::RtpLit(id,j)));
			 }
			mComponents[i]->SetInput(inputMatrix[q]);
			input_string.push_back(mComponents[i]->GetMInputName());

			// quandvk54 A: 24-04
			//sonlam xoa dong lenh 2/10/2014
/*			for(unsigned int k=0;k<inputMatrix[q].size();k++)
			{
				std::cout<<"value of inputMatrix["<<q<<"]"<<"["<<k<<"]: "<<inputMatrix[q][k]<<std::endl;
			}*/
			// end A:
			q++;
		}
	}

    //to do the simulation from start to the end
    for(unsigned int i=0;i<mComponents.size();i++)
        mComponents[i]->SimulateForward();

	//string *inOutName;
	//vector<unsigned int> timeFrameArray;
	vector<string> vectorName;
	unsigned int totalTimeFrame= mExpandedTimeFrames;//,counter_name=0,counter_timeframe=0;

	for(unsigned int i=0;i<input_string.size();i++){
		string::size_type pos;
		//unsigned int timeframe=0;
		pos=input_string[i].rfind('_');						// tim vi tri co ky tu '_'
		string NameofSignal=input_string[i].substr(0,pos); 	// lay string truoc ky tu '_'
		//string TimeFrameofSignal=input_string[i].substr(pos+1,10);	// lay string sau ky tu '_'
		vectorName.push_back(NameofSignal);							// luu ten cua cac tin hieu, se dung khi viet file vcd
	}

	//the following part are the part deal with the inputs and outputs

	    //to count the total number of the input and output signals
	    //unsigned int counter=0;
	    //counter=input_cnt+funct_cnt;
	   //to allocate all the exsiting ascii symbols(the total number of ascii symbol is 94,and start from 33
	   //and then in order to use it in the later part
	    int firstAscii=33; // character '!'
	    char Ascii[94]={0};
	    for (unsigned int i=0;i<94;i++)
	     {
	        Ascii[i]=firstAscii;
	        firstAscii++;
	     }

	    //to get to system date/time when running the program
	    time_t ltime;
	    time(&ltime);
	    //to write the needed information to the vcd file
	    ofstream outFile;
	    outFile.open(vcdname.c_str());
	    outFile<<"$date"<<endl<<"\t"<<ctime(&ltime)<<endl<<"$end"<<endl;
	    outFile<<"$version"<<endl<<"\t"<<"FCK TU Kaiserslauten"<<endl<<"$end"<<endl;
	    outFile<<"$timescale"<<"\t"<<"1ns "<<"$end"<<endl;
	    int counter_ascii=0;
	    map<string,char> NameSymbolMap;
	    for(unsigned int i=0;i<input_cnt;i++)
	     {
	         unsigned int number=0;
	         for(unsigned int j=0;j<i;j++)
	          {
	             //deal with the signals with the same name but different time frame
	             if(vectorName[i]==vectorName[j])
	              {
	                number++;
	              }
	          }
	         // chi insert vao NameSymbolMap ten tin hieu la duy nhat
	         if(number==0)
	         {
	        	 NameSymbolMap.insert(pair<string,char> (vectorName[i],Ascii[counter_ascii]));
	        	 outFile<<"$var wire "<<inputMatrix[i].size()<<" "<<Ascii[counter_ascii]<<" "<<vectorName[i]<<" "<<"$end"<<endl;
	             counter_ascii++;
	          }
	      }
	    outFile<<"$enddefinitions $end"<<endl<<endl;

	    for(unsigned int m=0;m<=totalTimeFrame+1;m++)
	      {
	          outFile<<"#"<<m<<endl;
	          for(unsigned int i=0;i<mComponents.size();i++)
	          {
				  string name;
				  if(mComponents[i]->GetMType()==INPUT) name = mComponents[i]->GetMInputName();
				  else name = mComponents[i]->GetMOutputName();
				  if((name != "")&&(mComponents[i]->Getvalidname())){
						// Tim cac tin hieu co cac ten giong voi input
						string::size_type pos;
						pos = name.rfind("_RESET_ITE");
						if (pos==std::string::npos) pos=name.rfind('_');
						string NameofSignal=name.substr(0,pos);
						int cnt=0;
						for(unsigned int j=0;j<vectorName.size();j++)
						{
							if((NameofSignal==vectorName[j]) && (cnt==0)){
								if(TimeFrameIdToCircuitId(mComponents[i]->GetMId()).second==m)
								{
									cnt++;
									if(mComponents[i]->GetMFanoutWidth()>1)
										 outFile<<"b";
									for(unsigned int k=mComponents[i]->GetMFanoutWidth();k>0;k--)
									{
									  if(mComponents[i]->GetOutput()[k-1]!=5) // 5 = DONTCARE
										outFile<<mComponents[i]->GetOutput()[k-1];
									  else
										outFile<<'x';
									 }
									if(mComponents[i]->GetMFanoutWidth()>1)
										outFile<<" "<<NameSymbolMap.find(NameofSignal)->second<<endl;
									else outFile<<NameSymbolMap.find(NameofSignal)->second<<endl;
								}
							}
						}
				  }
	          }
	      }
	    outFile.close();
}



void TimeFrameExpansion::PrintSolution_Btor(Btor* btor, std::string module_name, std::string vcdname){
	//phat gia tri cho cac component
	std::map<Component*, int> CompAssignIndexmap;
	int i=0;
	char *assignments[COMP_NUM_MAX];
	for (int t=0; t < mComponents.size(); t++)
	{
		if (mComponents[t]->GetMType() != MULT)
		{
			//std::cout<<" assignment Comp: "<<mComponents[t]->GetMOutputName()<<" - id: "<<mComponents[t]->GetMId()<<" - BtorComp: "<<mComponents[t]->GetBtorComp()<<"\n";
			assignments[i] = boolector_bv_assignment (btor,mComponents[t]->GetBtorComp());
			//std::cout<<"done\n";
			if (mComponents[t]->Getvalidname()) CompAssignIndexmap.insert(std::pair<Component*,int> (mComponents[t],i));
			i++;
		}
		else {
			assignments[i] = boolector_bv_assignment (btor,((Multiplier*)mComponents[t])->Get_uext1());
			i++;
			assignments[i] = boolector_bv_assignment (btor,((Multiplier*)mComponents[t])->Get_uext2());
			i++;
			assignments[i] = boolector_bv_assignment (btor,mComponents[t]->GetBtorComp());
			if (mComponents[t]->Getvalidname()) CompAssignIndexmap.insert(std::pair<Component*,int> (mComponents[t],i));
			i++;
		}
	}

	//in ket qua ra file vcd
	int assign_num = i;
	using namespace std;
	unsigned int funct_cnt=0;
	std::vector<Component*> validname_signal_list;
	for(unsigned int i=0;i<mComponents.size();i++)
	{
		if((mComponents[i]->GetMType()==INPUT)||((mComponents[i]->GetMOutputName().substr(0,6)=="v__DOT")&&(mComponents[i]->Getvalidname()==1)))
			validname_signal_list.push_back(mComponents[i]);
	}

	std::vector<std::string> vectorName;
	unsigned int totalTimeFrame= mExpandedTimeFrames;//,counter_name=0,counter_timeframe=0;
	// tim va luu lai ten cac input cua mach
	for(unsigned int i=0;i<validname_signal_list.size();i++){
		//string::size_type pos;
		//unsigned int timeframe=0;
		//pos=validname_signal_list[i]->GetMInputName().rfind('_');						// tim vi tri co ky tu '_'
		  // format the name
		std::string fanout = validname_signal_list[i]->GetMOutputName();
		if (fanout.find("v__DOT__") != std::string::npos) {
			fanout.erase(0,1);
			fanout.insert(0, module_name);
		}
		while (fanout.find("__DOT__")!=std::string::npos) {
			std::size_t pos = fanout.find("__DOT__");
			fanout.erase(pos, 7);
			fanout.insert(pos, "/");
		}
		vectorName.push_back(fanout);
		// luu ten cua cac tin hieu, se dung khi viet file vcd
	}

	//the following part are the part deal with the inputs and outputs

	    //to count the total number of the input and output signals
	    //unsigned int counter=0;
	    //counter=input_cnt+funct_cnt;
	   //to allocate all the exsiting ascii symbols(the total number of ascii symbol is 94,and start from 33
	   //and then in order to use it in the later part
	    int firstAscii=33; // character '!'
	    char Ascii[94]={0};
	    for (unsigned int i=0;i<94;i++)
	     {
	        Ascii[i]=firstAscii;
	        firstAscii++;
	     }

	    //to get to system date/time when running the program
	    time_t ltime;
	    time(&ltime);
	    //to write the needed information to the vcd file
	    ofstream outFile;
	    outFile.open(vcdname.c_str());
	    outFile<<"$date"<<endl<<"\t"<<ctime(&ltime)<<endl<<"$end"<<endl;
	    outFile<<"$version"<<endl<<"\t"<<"FCK TU Kaiserslauten"<<endl<<"$end"<<endl;
	    outFile<<"$timescale"<<"\t"<<"1ns "<<"$end"<<endl;
	    int counter_ascii=0;
	    map<string,pair<char,char> > NameSymbolMap;
	    for(unsigned int i=0;i<validname_signal_list.size();i++)
	     {
	         unsigned int number=0;
	         for(unsigned int j=0;j<i;j++)
	          {
	             //deal with the signals with the same name but different time frame
	             if(vectorName[i]==vectorName[j])
	              {
	                number++;
	                break;
	              }
	          }
	         // chi insert vao NameSymbolMap ten tin hieu la duy nhat
	         if(number==0)
	         {
	        	 NameSymbolMap.insert(pair<string,pair<char,char> > (vectorName[i],make_pair(Ascii[counter_ascii/94 + 1],Ascii[counter_ascii%94])));
	        	 outFile<<"$var wire "<<validname_signal_list[i]->GetMFanoutWidth()<<" "<<Ascii[counter_ascii/94 + 1]<<Ascii[counter_ascii%94]<<" "<<vectorName[i]<<" "<<"$end"<<endl;
	             counter_ascii++;
	          }
	      }
	    outFile<<"$enddefinitions $end"<<endl<<endl;

	    for(unsigned int m=0;m<=totalTimeFrame+1;m++)
	      {
	          outFile<<"#"<<m<<endl;

	          for(unsigned int i=0;i<mComponents.size();i++)
	          {
	        	  //std::cout<<mComponents[i]->GetMId()<<"  "<<mComponents[i]->GetMOutputName()<<endl;
				  string name;
				  if(mComponents[i]->GetMType()==INPUT) name = mComponents[i]->GetMInputName();
				  else name = mComponents[i]->GetMOutputName();
				  // format the name
					if (name.find("v__DOT__") != std::string::npos) {
						name.erase(0,1);
						name.insert(0, module_name);
					}
					while (name.find("__DOT__")!=std::string::npos) {
						std::size_t pos = name.find("__DOT__");
						name.erase(pos, 7);
						name.insert(pos, "/");
					}

				  if((name != "")&&(mComponents[i]->Getvalidname())){
						// Tim cac tin hieu co cac ten giong voi input
						/*
						string::size_type pos;
						pos = name.rfind("_RESET_ITE");
						if (pos==std::string::npos) pos=name.rfind('_');
						string NameofSignal=name.substr(0,pos);
						*/
					  	string NameofSignal = name;
						int cnt=0;
						for(unsigned int j=0;j<vectorName.size();j++)
						{
							if((NameofSignal==vectorName[j]) && (cnt==0)){
								if(TimeFrameIdToCircuitId(mComponents[i]->GetMId()).second==m)
								{
									cnt++;
									if(mComponents[i]->GetMFanoutWidth()>1)
										 outFile<<"b";
									outFile<<assignments[CompAssignIndexmap[mComponents[i]]];
									if(mComponents[i]->GetMFanoutWidth()>1)
										outFile<<" "<<NameSymbolMap.find(NameofSignal)->second.first<<NameSymbolMap.find(NameofSignal)->second.second<<endl;
									else outFile<<NameSymbolMap.find(NameofSignal)->second.first<<NameSymbolMap.find(NameofSignal)->second.second<<endl;
								}
							}
						}
				  }
	          }
	      }
		for (int k = 0; k < assign_num; k++)
    			boolector_free_bv_assignment (btor, assignments[k]);
	    outFile.close();
}


void TimeFrameExpansion::CreateCnf(satlib::SatI& satSolver)
{
   for (unsigned i = 0; i<mComponents.size(); i++)
   {
      mComponents[i]->CreateCnf(satSolver);
   }
}

std::pair<FckId,int> TimeFrameExpansion::TimeFrameIdToCircuitId(FckId id)
{
    std::map<std::pair<FckId, int>,FckId>::iterator iter;
    for (iter = mTimeFrameMap.begin(); iter!= mTimeFrameMap.end(); iter++)
    {
        if (iter->second==id)
        {
            return iter->first;
        }
    }
    return std::pair<FckId,int>(0,0);
}

FckId TimeFrameExpansion::CircuitIdToTimeFrameId(std::pair<FckId,int> id)
{
    std::map<std::pair<FckId, int>,FckId>::iterator iter;
    for (iter = mTimeFrameMap.begin(); iter!= mTimeFrameMap.end(); iter++)
    {
        if (iter->first==id)
        {
            return iter->second;
        }
    }
    return 0;
}
// mTimeFrameMap la map anh xa gia cmp trong prop o frame i den cmp trong circuit bien dich dc

FckId TimeFrameExpansion::CircuitIdToTimeFrameId(FckId id,int tf)
{
    std::pair<FckId, int> pair;
    pair = std::pair<FckId,int>(id,tf);
    return CircuitIdToTimeFrameId(pair);
}

FckId TimeFrameExpansion::AddComponentTimeFrame(Component* cmp, int timeFrame)
{
   std::pair<FckId, int> pair;
   pair.first = cmp->GetMId();
   pair.second = timeFrame;
   //if (mModuleJumpMap[pair.first] != 0)
   //{
      //cmp = mModuleJumpMap[pair.first];
   //}
   if (mTimeFrameMap[pair] != 0)
   {
      return mTimeFrameMap[pair];
   }
   if (cmp->GetMType() == INPUT)
   {
      int width = cmp->GetMFanoutWidth();
      std::vector<FckId> fanins;
      fanins.push_back(width);
      FckId id = AddComponent(INPUT, fanins,cmp->GetMInputName());// + "_" + IntToStr(timeFrame)); sonlam edited 23/3/15
      mTimeFrameMap[pair] = id;
      return id;
   }
   if (cmp->GetMType() == PS_INPUT)
   {
      FckId id;
      std::pair<FckId, FckId> reset_pair;
      reset_pair = mResetMap[cmp->GetMId()];
      if (timeFrame == 0)
      {
         int width = cmp->GetMFanoutWidth();
         std::vector<FckId> fanins;
         fanins.push_back(width);
         id = AddComponent(INPUT, fanins,cmp->GetMInputName());// + "_" + IntToStr(timeFrame)); sonlam edited 23/3/15
      }
      else
      {
    	  bool edge; // 1 is posedge, 0 is negedge
         id = mJumpMap[cmp->GetMId()];
         edge = ((PseudoOutput*) mModuleToExpand->GetComponent(id))->Getreset_edge() == "POS";
         id = AddComponentTimeFrame(mModuleToExpand->GetComponent(id),timeFrame-1);
		  if (reset_pair.first!=0)
		  {
			 this->GetComponent(id)->Setvalidname(0);
			 std::vector<FckId> fanins;
			 fanins.push_back(AddComponentTimeFrame(mModuleToExpand->GetComponent(reset_pair.first),timeFrame-1));
			 if (edge)
			 {
				 fanins.push_back(AddComponentTimeFrame(mModuleToExpand->GetComponent(reset_pair.second),timeFrame-1));
				 fanins.push_back(id);
			 }
			 else
			 {
				 fanins.push_back(id);
				 fanins.push_back(AddComponentTimeFrame(mModuleToExpand->GetComponent(reset_pair.second),timeFrame-1));
			 }
			 //fanins.push_back(reset_pair.first);
			 //fanins.push_back(reset_pair.second);
			 id = AddComponent(ITE, fanins,cmp->GetMInputName());// + "_RESET_ITE_" + IntToStr(timeFrame));
			 //this->GetComponent(id)->Setvalidname(1);
			 //id = AddComponent(ITE, fanins,cmp->GetMInputName() + IntToStr(timeFrame));
		  }
      }
      mTimeFrameMap[pair] = id;
      this->GetComponent(id)->Setvalidname(1);
      return id;
   }
   if (cmp->GetMType() == C_INPUT)
   {
      std::vector<FckId> fanins;
      FckId id = AddComponent(C_INPUT, fanins,cmp->GetMInputName());
      for (int i = 0; i < mExpandedTimeFrames; i++)
      {
         pair.second = i;
         mTimeFrameMap[pair] = id;
      }
      this->GetComponent(id)->Setvalidname(0);
      return id;
   }
//    M_INPUT

//    M_OUTPUT

//    PS_OUTPUT
   if (cmp->GetMType() == PS_OUTPUT)
   {
      std::vector<Component*> fanins;
      fanins = cmp->GetFanin();

      FckId id = AddComponentTimeFrame(fanins[0],timeFrame);
      //this->GetComponent(id)->Setvalidname(1);
      mTimeFrameMap[pair] = id;
      return id;
   }
//    OUTPUT
   if (cmp->GetMType() == OUTPUT)
   {
      std::vector<Component*> fanins;
      fanins = cmp->GetFanin();

      FckId id = AddComponentTimeFrame(fanins[0],timeFrame);
      //this->GetComponent(id)->Setvalidname(1);
      mTimeFrameMap[pair] = id;
      return id;
   }
//    ADD
//    AND
//    CONCAT
//    EQ
//    ITE
//    LT
//    MINUS
//    MULT
//    NOT
//    OR
//    SHL
//    SHR
//    SIGN_EXTEND
//    SLICE
//    U_AND
//    U_OR
//    U_XOR
//    WRITE
//    XOR
//    ZERO_EXTEND
   std::vector<Component*> fanin;
   fanin = cmp->GetFanin();
   std::vector<FckId> fanins;
   for (unsigned i = 0; i < fanin.size(); i++)
   {
      fanins.push_back(AddComponentTimeFrame(fanin[i],timeFrame));
   }

   FckId id = AddComponent(cmp->GetMType(), fanins,cmp->GetMOutputName());//+ "_" + IntToStr(timeFrame));
   this->GetComponent(id)->Setvalidname(cmp->Getvalidname());
   mTimeFrameMap[pair] = id;
   return id;
}

std::string TimeFrameExpansion::IntToStr(int number)
{
   std::stringstream Stream;
   Stream << number;
   return Stream.str();
}

