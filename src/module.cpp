/*! ************************************************************************
    \file module.cpp
    \brief Implementation of the class Module
    \author Sacha Loitz

   Implements the methods neeeded to setup a Module and work on it.
   \copyright (c) University of Kaiserslautern Tue May 30 2006

Modification history:
  * 05.07.2011 C.Villarraga added support for SHRA, ROR and ROL
  *

**************************************************************************/

#include "module.h"
#include "circuit.h"
#include <queue>

/**
 * Constructors/Destructors
 */
/**
 * Default Constructor.
 * Set up a Vector containing a Component of each
 * operation so that the number of needed fanin Components can easily be
 * determined.
 */
Module::Module()
{
    mParent = 0;
    mOperation.resize(OUTPUT+1, new Undef());
    mOperation[INPUT]= new Input();
    mOperation[PS_INPUT]= new PseudoInput();
    mOperation[C_INPUT] = new ConstInput();
    mOperation[ADD] = new Add();
    mOperation[AND] = new And();
    mOperation[CONCAT] = new Concat();
    mOperation[EQ] = new Equivalence();
    mOperation[ITE] = new IfThenElse();
    mOperation[LT] = new LessThan();
    mOperation[MINUS] = new Minus();
    mOperation[MULT] = new Multiplier();
    mOperation[NOT] = new Not();
    mOperation[OR] = new Or();
    mOperation[SHL] = new ShiftLeft();
    mOperation[SHR] = new ShiftRight();
    mOperation[SHRA] = new ShiftRightArith();
    mOperation[ROR] = new RotateRight();
    mOperation[ROL] = new RotateLeft();
    mOperation[SIGN_EXTEND] = new SignExtension();
    mOperation[SLICE] = new Slice();
    mOperation[U_AND] = new UnaryAnd();
    mOperation[U_OR] = new UnaryOr();
    mOperation[U_XOR] = new UnaryXor();
    mOperation[WRITE] = new Write();
    mOperation[XOR] = new Xor();
    mOperation[ZERO_EXTEND] = new ZeroExtension();
    mOperation[M_OUTPUT] = new ModuleOutput();
    mOperation[PS_OUTPUT] = new PseudoOutput();
    mOperation[MUX] = new Mux();
    mOperation[DECODER] = new Decoder();
    mOperation[OUTPUT] = new Output();
    mNumInps=0;
    mModuleId = 1;
    mNextMId = 1;
} // End Circuit

/**
 * Constructor saving a pointer to the circuit this Module is in.
 */
Module::Module(Circuit* circuit, ModuleId id)
{
    mParent = circuit;
    mModuleId = id;
    mOperation.resize(OUTPUT+1, new Undef());
    mOperation[INPUT]= new Input();
    mOperation[PS_INPUT]= new PseudoInput();
    mOperation[C_INPUT] = new ConstInput();
    mOperation[ADD] = new Add();
    mOperation[AND] = new And();
    mOperation[CONCAT] = new Concat();
    mOperation[EQ] = new Equivalence();
    mOperation[ITE] = new IfThenElse();
    mOperation[LT] = new LessThan();
    mOperation[MINUS] = new Minus();
    mOperation[MULT] = new Multiplier();
    mOperation[NOT] = new Not();
    mOperation[OR] = new Or();
    mOperation[SHL] = new ShiftLeft;
    mOperation[SHR] = new ShiftRight();
    mOperation[SHRA] = new ShiftRightArith();
    mOperation[ROR] = new RotateRight();
    mOperation[ROL] = new RotateLeft();
    mOperation[SIGN_EXTEND] = new SignExtension();
    mOperation[SLICE] = new Slice();
    mOperation[U_AND] = new UnaryAnd();
    mOperation[U_OR] = new UnaryOr();
    mOperation[U_XOR] = new UnaryXor();
    mOperation[WRITE] = new Write();
    mOperation[XOR] = new Xor();
    mOperation[ZERO_EXTEND] = new ZeroExtension();
    mOperation[M_OUTPUT] = new ModuleOutput();
    mOperation[PS_OUTPUT] = new PseudoOutput();
    mOperation[MUX] = new Mux();
    mOperation[DECODER] = new Decoder();
    mOperation[OUTPUT] = new Output();
    mNumInps=0;
} // End Circuit

Module::~Module()
{

    mOperation.clear();
    mComponentNameMap.clear();
    mIdMap.clear();
    for (unsigned int i=0; i<mComponents.size();i++)
        delete mComponents[i];
    mComponents.clear();

}

/**
 * Methods
 */
/**
 * Add a new Component to the Circuit.
 * First of all the passed strings containing the names of the fanin
 * components have to be mapped to the corresponding Components. After this
 * the new Component is created, linked to the found Components in the
     * fanin and added to the list mComponents in the following order:
 *
 * Inputs
 *
 * Functions#
 *
 * Outputs
 *
 *
 * #The Functions themselves are ordered in such a way that a Function is
 * placed behind every Component in its fanins and in front of every
 * component in its fanout. A reference to the created Component is stored
 * with the passed string for the fanout variable in a map (mapFanout) so
 * that we are able to map the corresponding inputs.
 * @return If the Component was successfully created true is returned. If
 * the Component can not yet be created (missing fanout) false is
 * returned.
 * @param type An integer representing the kind of the Component that
 * shall be added (see CircuitBuilder for the mapping of value and type).
 * @param fanin A vector containing the names of the fanin Components that
 * support the new component. For details on the correct ordering of the
 * fanin components please take a look at the subclass implementing the
 * Component you want to add.
 * @param fanout A string with the name of the fanout of the new component
 * that shall be added.
 * \todo replace map by hash_map
 */

FckId Module::AddComponent (int type, const std::vector<FckId>& fanin,
                            const std::string& componentName)
{
    Circuit* cir = mParent;
    FckId nextId;
    if (cir==0)
    {
        nextId = this->GetNextId();
    }
    else
    {
        nextId = cir->GetNextId();
    }
    //prevent a segfault by checking whether type is within the specified
    //range. OUTPUT has always the highest number of the components. So check
    //whether type is greater than OUTPUT. If this is the case throw an
    //exception.
   if (type>OUTPUT)
    {
        std::cout<<"Default Exception"<<std::endl;
        std::string message="EXCEPTION: Trying to add an unknown ";
        message+="Component. Type is "+type;
        throw DefaultException(message);
    }
    // Check whether fanin has the correct size if not successful return
    // false.
   if (fanin.size()!=mOperation[type]->GetMRequiredFanin()
        && type!=M_INPUT && type!=MUX)
    {
	   	std::cout<<cir->GetComponent(fanin[1],1)->GetMOutputName()<<std::endl;
    	std::cout <<"type =  "<<type<<"fanin.size() ..." << fanin.size() << "mOperation[type]->GetMRequiredFanin() ..." << mOperation[type]->GetMRequiredFanin() << std::endl;

    	std::cout<<"Wrong number of Fanin Components"<<std::endl;
        std::string message="EXCEPTION: Incorrect number of fanin components";
        throw FaninNumberException(message, nextId, type,
                                   mOperation[type]->GetMRequiredFanin(),
                                   fanin);
    }
    // A Vector pointing on the fanin Components
    std::vector<Component*> fanincmps;
    ModuleId module = 0;
    //if we have a PseudoInput we want to handle the fanin Component as a
    //fanout Component.
    if (type>C_INPUT && type!=PS_OUTPUT)
    {
        //for each fanin map its string to the fanin Component
        for (unsigned int i=0; i<fanin.size();i++)
        {
            if (i==0 && (type==M_INPUT || type == M_OUTPUT))
            {
                i++;
                module=fanin[0];
            }
            //check whether a fanout equivalent to the fanin exists. If not
            //return false
            if (mIdMap.end()==mIdMap.find(fanin[i]))
            {
                std::string message="EXCEPTION: Fanin not found in FanoutMap.";
                std::cout<<message<<nextId<<", "<<type<<", "<<fanin[i]<<"\n";
                throw FanoutMapMiss(message, nextId, type, fanin[i]);
            } // End if (mFanoutMap.end()==mFanoutMap.find(fanin[i]))
            //push the fanout to the fanin Components.
            fanincmps.push_back(mIdMap[fanin[i]]);
        } // End for(int i=0; i<fanin.size();i++)
    }
    // End if (type!=PS_INPUT)
    //if we have a PseudoOutput we also have to add the fanin component
    //All fanins are known create the Component.
    //We have to differentiate between the Operations to create the correct
    //circuit.

    switch (type)
    {
        // An undefined Operation? return false.
		case UNDEF:
				{
					// UNDEF is not exception 19-08
					//std::string message="EXCEPTION: Trying to add an undefined ";
					        //message+="component.";
					//throw DefaultException(message);
					mComponents.push_back(new Undef(fanin, componentName, nextId, this));
					//mNumInps++;
					break;
				}
        case INPUT:
        {
            mComponents.push_back(new Input(fanin, componentName, nextId,
                                  *this));
            mNumInps++;
            break;
        }
        case M_INPUT: mComponents.push_back(new ModuleInput(fanincmps, module,
                                                componentName, nextId, this));
                          break;
        case PS_INPUT:    mComponents.push_back(new PseudoInput(fanin, componentName,
                                                nextId, this));
                          break;
        case C_INPUT:     mComponents.push_back(new ConstInput(
                                                    componentName, nextId, this));
                          break;
        case ADD:
        {
            mComponents.push_back(new Add(fanincmps, componentName, nextId,
                                  this));
            break;
        }
        case AND:         mComponents.push_back(new And(fanincmps, componentName,
                                                nextId, this));
                          break;
        case CONCAT:      mComponents.push_back(new Concat(fanincmps, componentName,
                                                nextId, this));
                          break;
        case EQ:          mComponents.push_back(new Equivalence(fanincmps,
                                                componentName, nextId, this));
                          break;
        case ITE:         mComponents.push_back(new IfThenElse(fanincmps,
                                                componentName, nextId, this));
                          break;
        case LT:          mComponents.push_back(new LessThan(fanincmps,
                                                componentName, nextId, this));
                          break;
        case MINUS:       mComponents.push_back(new Minus(fanincmps, componentName,
                                                nextId, this));
                          break;
        case MULT:        mComponents.push_back(new Multiplier(fanincmps,
                                                componentName, nextId, this));
                          break;
        case NOT:         mComponents.push_back(new Not(fanincmps, componentName,
                                                nextId, this));
                          break;
        case OR:          mComponents.push_back(new Or(fanincmps, componentName,
                                                nextId, this));
                          break;
        case SHL:         mComponents.push_back(new ShiftLeft(fanincmps,
                                                componentName, nextId, this));
                          break;
        case SHR:         mComponents.push_back(new ShiftRight(fanincmps,
                                                componentName, nextId, this));
                          break;
        case SHRA:        mComponents.push_back(new ShiftRightArith(fanincmps,
                                                componentName, nextId, this));
                          break;
        case ROR:        mComponents.push_back(new RotateRight(fanincmps,
                                                componentName, nextId, this));
                          break;
        case ROL:        mComponents.push_back(new RotateLeft(fanincmps,
                                                componentName, nextId, this));
                          break;
        case SIGN_EXTEND: mComponents.push_back(new SignExtension(fanincmps,
                                                componentName, nextId, this));
                          break;
        case SLICE:       mComponents.push_back(new Slice(fanincmps, componentName,
                                                nextId, this));
                          break;
        case U_AND:       mComponents.push_back(new UnaryAnd(fanincmps,
                                                componentName, nextId, this));
                          break;
        case U_OR:        mComponents.push_back(new UnaryOr(fanincmps, componentName,
                                                nextId, this));
                          break;
        case U_XOR:       mComponents.push_back(new UnaryXor(fanincmps,
                                                componentName, nextId, this));
                          break;
        case WRITE:       mComponents.push_back(new Write(fanincmps, componentName,
                                                nextId, this));
                          break;
        case XOR:         mComponents.push_back(new Xor(fanincmps, componentName,
                                                nextId, this));
                          break;
        case ZERO_EXTEND: mComponents.push_back(new ZeroExtension(fanincmps,
                                                componentName, nextId, this));
                          break;
        case M_OUTPUT:    mComponents.push_back(new ModuleOutput(fanincmps,
                                                module, componentName, nextId,
                                                this));
                          break;
        case PS_OUTPUT:   mComponents.push_back(new PseudoOutput(fanin,
                                                componentName, nextId, this));
                          break;
        case MUX:         mComponents.push_back(new Mux(fanincmps, componentName,
                                                nextId, this));
        					break;
        case DECODER:         mComponents.push_back(new Decoder(fanincmps, componentName,
                                                nextId, this));
                          break;
        case OUTPUT:
        {
            mComponents.push_back(new Output(fanincmps, componentName, nextId,
                                  this));
            mNumOuts++;
            mOutputs.push_back(componentName);
            break;
        }
        //The default case should not be reached so return false
        default:
        {
             std::string message="EXCEPTION: Trying to add an unknown ";
            message+="Component.";
            throw DefaultException(message);
        }

    } // End switch (type)


    // The new Component is at the last position in mComponents set a
    // reference to this Component in mFanoutMap.
    mIdMap[nextId]=mComponents[mComponents.size()-1];
    if (componentName.size()>0)
    {
        mComponentNameMap[componentName]=mComponents[mComponents.size()-1];
    }
    //in the case of a PseudoInput we also have to add the fanin component
    //if (type==PS_INPUT)
    //{
    //    mIdMap[fanin[0]]=mComponents[mComponents.size()-1];
    //    Component* cmp = GetComponent(fanin[0]);
    //    mComponentNameMap[cmp->GetMOutputName()]=mComponents[mComponents.size()-1];
    //}


//    std::cout<<"Added Component. Type: " << mComponents[mComponents.size()-1]->GetType() << ", FckId: " <<nextId<< ", name: "<<componentName<< ", BitWidth: " << mComponents[mComponents.size()-1]->GetMFanoutWidth() << std::endl;

    //std::cout<<nextId<<" "<<type<<"\n";

    return nextId;
    //The Component has been successfully added.
} // End AddComponent (int Type, vector<string> fanin, string fanout)

/**
 * perform a simulation of a component for a vector of inputs values.
 * @param component The number of the Component that shall be simulated
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void Module::Reorder()
{
    //The reordered list of Components
    std::vector<Component*> newMComponents;
    newMComponents.reserve(mComponents.size());
    //The fanouts of the Components that are already known
    std::map<FckId, Component*> knownIds;
    // The Components waiting for a fanout (stored in key)
    std::map<FckId, Component*> watchedIds;
    //A vector for the pseudo outputs
    std::vector<Component*> psOuts;
    //A vector for the outputs
    std::vector<Component*> outs;
    //The PseudoInputs shall be placed directly behind the inputs use a
    //variable pointing to the postition at which the next PseudoInput shall
    //be placed. So that we know were to insert the the next PseudoInput.
    //initialize with 0 as we don't have an input yet.
    std::insert_iterator<std::vector<Component*> > nextPsInput(newMComponents,
                                                    newMComponents.begin());
    //The ConstantInputs shall be placed directly behind the PseudoInputs use
    //a variable pointing to the postition at which the next ConstantInput
    //shall be placed. So that we know were to insert the the next
    //ConstantInput. Initialize with 0 as we don't have an input yet.
    std::vector<Component* > c_inputs;
    // iterate over all Components.
    for (unsigned int i=0;i<mComponents.size();i++)
    {
        //if the Component is an Input it shall be placed at the beginning
        if (mComponents[i]->GetMType()==INPUT)
        {
            //place the Input at the beginning
            *nextPsInput = mComponents[i];
            //increment the position of the nextCInput
            knownIds[mComponents[i]->GetMId()]=mComponents[i];
            //Try to get a Component waiting for this fanout
            std::map<FckId, Component*>::iterator iter
                    =watchedIds.find(mComponents[i]->GetMId());
            while (iter!=watchedIds.end())
            {
                //try to add these components
                if (RecReorder((*iter).second, knownIds, watchedIds,
                    newMComponents))
                    //if successfully added remove from watch list
                    watchedIds.erase(iter);
                //is there another component waiting for this
                iter=watchedIds.find(mComponents[i]->GetMId());
            } //End while (cmp!=watchedFanouts.end())
        } // End if (mComponents[i].GetMType()==INPUT)
        //PseudoInputs are placed behind the last Input
        else if (mComponents[i]->GetMType()<=C_INPUT)
        {
            c_inputs.push_back(mComponents[i]);
            //nextCInput++;
            knownIds[mComponents[i]->GetMId()]=mComponents[i];
            //Try to get a Component waiting for this fanout
            std::map<FckId, Component*>::iterator iter
                    =watchedIds.find(mComponents[i]->GetMId());
            while (iter!=watchedIds.end())
            {
                //try to add these components
                if (RecReorder((*iter).second, knownIds, watchedIds,
                    newMComponents))
                    //if successfully added remove from watch list
                    watchedIds.erase(iter);
                //is there another component waiting for this
                iter=watchedIds.find(mComponents[i]->GetMId());
            } //End while (cmp!=watchedFanouts.end())
        }
        //PseudoInputs are placed behind the last Input
        /*else if (mComponents[i]->GetMType()==C_INPUT)
        {
            //place the pseudo Input behind inputs
            *nextCInput=mComponents[i];
            knownFanouts[mComponents[i]->GetMOutputName()]=mComponents[i];
            //Try to get a Component waiting for this fanout
            std::map<std::string, Component*>::iterator iter
                    =watchedFanouts.find(mComponents[i]->GetMOutputName());
            while (iter!=watchedFanouts.end())
            {
                //try to add these components
                if (RecReorder((*iter).second, knownFanouts, watchedFanouts,
                    newMComponents))
                    //if successfully added remove from watch list
                    watchedFanouts.erase(iter);
                //is there another component waiting for this
                iter=watchedFanouts.find(mComponents[i]->GetMOutputName());
            } //End while (cmp!=watchedFanouts.end())
    }*/
        // the other components are pushed to the back if all fanin components
        // are available this has to be done recursively for the Components
        // waiting for their Fanins. So it's implemented in a separate method.
        else if (mComponents[i]->GetMType()==PS_OUTPUT)
            psOuts.push_back(mComponents[i]);
        else if (mComponents[i]->GetMType()==OUTPUT)
            outs.push_back(mComponents[i]);
        else RecReorder(mComponents[i], knownIds, watchedIds,
                        newMComponents);
    } // End for (unsigned int i=0;i<mComponents.size();i++)
    //copy the newMComponent to mComponents
    //place the pseudo Inputs behind inputs
    for(unsigned int i=0;i<c_inputs.size();i++)
            *nextPsInput=c_inputs[i];
    for(unsigned int i=0;i<psOuts.size();i++)
        newMComponents.push_back(psOuts[i]);
    for(unsigned int i=0;i<outs.size();i++)
        newMComponents.push_back(outs[i]);
    mComponents=newMComponents;
}// End reorder()

/**
 * The reordering needs a method that can call itself recursively.
 * @param cmp The Component that shall be added
 * @param knownFanouts The map containing the known fanout components
 * @param watchedFanouts The components watching for Fanouts
 * @param newMComponents the reordered vector containing the components
 * @return returns true if component was added successfully
 */
bool Module::RecReorder(const Component* cmp, std::map<FckId,
                        Component*>& knownIds,
                        std::map<FckId, Component*>& watchedIds,
                        std::vector<Component*>& newMComponents)
{
    //a variable to indicate whether all fanin components are known
    bool allKnown = true;
    //Fetch the vector of inputs
    std::vector<Component*> fanin = cmp->GetFanin();
    // check for every Input whether it is known if not set allKnown to false
    for (unsigned int j=0; allKnown && j<fanin.size();j++)
    {
        //Check whether it is already known
        if (knownIds.find(fanin[j]->GetMId())==knownIds.end())
        {
            //the fanin component is not known so set allKnown to false
            allKnown=false;
            //and add the component to the list of watchedFanouts
            watchedIds[fanin[j]->GetMId()] =
                    const_cast<Component*>(cmp);
        } // End if (knownFanouts.find(fanin[j]->GetMOutputName())
          //==knownFanouts.end())
    }// End for (unsigned int j=0; allKnown && j<fanin.size();j++)
    //if all Fanin Components are known add the Component to the new vector of
    //mComponentsadd the output of the Components. Add the output to
    //knownFanouts and check whether another Component is waiting for this
    //output.
    if (allKnown)
    {
        // Add the component
        newMComponents.push_back(const_cast<Component*>(cmp));
        // Add the fanout
        knownIds[const_cast<Component*>(cmp)->GetMId()]= const_cast<Component*>(cmp);
        //Try to get a Component waiting for this fanout
        std::map<FckId, Component*>::iterator iter
                =watchedIds.find(const_cast<Component*>(cmp)->GetMId());
        //As long as there are components waiting for this fanout
        while (iter!=watchedIds.end())
        {
            //try to add these components
            if (RecReorder((*iter).second, knownIds, watchedIds,
                newMComponents))
                //if successfully added remove from watch list
                watchedIds.erase(iter);

            //is there another component waiting for this
            iter=watchedIds.find(const_cast<Component*>(cmp)->GetMId());
        } //End while (cmp!=watchedFanouts.end())
        //we added the component successfully return true
        return true;
    } //End if (allknown)
    // Component can not yet be added return false
    return false;
}// End recReorder

/**
 * Check whether the Circuit is complete.
 * Method to check whether a Circuit has been successfully created after
 * all Components have been added.
 * @return True if every Component has all required fanin Components and
 * at least one fanout Component. To check this the method isComplete of
 * every Component in mComponents is called.
 */
bool Module::IsComplete ()
{
    if (mComponents.size()==0)
    {
        std::cout<<"No Component in Module "<<mModuleName<<std::endl;
        return false;
    }
    //We have to check every single Component
    for (unsigned int i=0; i<mComponents.size(); i++)
    {
        //if one of the Components is not complete the circuit is not
        //complete.
        if (!mComponents[i]->IsComplete())
        {
            std::cout<<i<<": "<<mComponents[i]->GetType()<<std::endl;
            return false;
        }
    } // End for (int i=0; i<mComponents.size(); i++)
    //All Components are complete so the circuit is complete
    return true;
} // End IsComplete()

/**
 * Set the values of the inputs of the circuit.
 * @param in the values the inputs shall take. The value at in[0] will be
 * applied to the Input at mComponent[0] and so on
 *\todo remove simulation
 */
void Module::SetInputs(std::vector<std::vector<satlib::SatI::lValue> >& in)
{
    //We can only set the inputs values for input components and it can not be
    //guaranteed that the size of in is equal to the number of inputs. To be
    //able to check how many inputs we really added the iterator i is declared
    //outside of the loop
    unsigned int i=0;
    //We have to stop setting the inputs as soon as we either added all inputs
    //given or as soon as all inputs have been assigned ideally both
    //conditions are fulfilled at the same time
    for(; i<in.size() && mComponents[i]->GetMType()<=2;i++)
    {
        mComponents[i]->SetInput(in[i]);
    }
    //remove the following code used for simulation
    try
    {
        for(;i<mComponents.size();i++)
        {
            mComponents[i]->SimulateForward();
        }
        in.push_back(mComponents[--i]->GetOutput());
    }
    catch(DefaultException e)
    {
        e.GetMessage();
        std::cout << "Aborted Simulation" << std::endl;
    }
}

/**
 * Fetch a Component by its name.
 * @param name The name of the component that shall be returned
 *\todo enhance this function up to now it is really inefficient
 */
Component* Module::GetComponent(std::string name)
{
	std::string assignalias_name;
	assignalias_name = name;
	assignalias_name.insert(0,"v__DOT__");
	//std::cout<<"bi danh = "<<assignalias_name<<"\n";
    //for each component in mComponents
	if(!mComponents.empty())
	    for (unsigned int i=0;i<mComponents.size();i++)
	    {
	    	if (mComponents[i]->Getvalidname()==1)
	    	{
				if ((mComponents[i]->GetMOutputName()==name) || (mComponents[i]->GetMOutputName()==assignalias_name))
					return mComponents[i];
				if (mComponents[i]->GetMType()==INPUT &&
						((mComponents[i]->GetMInputName()==name)||(mComponents[i]->GetMInputName()==assignalias_name)))
					return mComponents[i];
				if (mComponents[i]->GetMType()==PS_INPUT &&
						((mComponents[i]->GetMInputName()==name)||(mComponents[i]->GetMInputName()==assignalias_name)))
					return mComponents[i];
	    	}
	    }
	// && mComponents[i]->Getvalidname()==1
	std::cout<<" not founded \n";
	return NULL;
    /*std::string expMsg = "Exception haven't found Component ";
    expMsg += name;
    expMsg += "\nHave Components: ";
    for (unsigned int i=0;i<mComponents.size();i++)
    {
        expMsg+= mComponents[i]->GetMOutputName();
        if (mComponents[i]->GetMType()==INPUT)
            expMsg+=mComponents[i]->GetMInputName();
        expMsg+="\n";
    }
    throw DefaultException(expMsg);
    return 0;
    */
}

/**
 * Fetch a Component by its ID.
 * @param id The ID of the component that shall be returned
 *\todo enhance this function up to now it is really inefficient
 */
Component* Module::GetComponent(FckId id)
{
    //for each component in mComponents
    if (mIdMap.end()!=mIdMap.find(id))
    {
        return mIdMap[id];
    }
    std::string expMsg = "Exception haven't found Component ";
    std::stringstream s;
    s << id;
    expMsg += s.str().c_str();
    expMsg += "\nHave Components: ";
    for (unsigned int i=0;i<mComponents.size();i++)
    {
        expMsg+= mComponents[i]->GetMId();
        expMsg+= " ";
        expMsg+= mComponents[i]->GetMOutputName();
        if (mComponents[i]->GetMType()==INPUT)
            expMsg+=mComponents[i]->GetMInputName();
        expMsg+="\n";
    }
    throw DefaultException(expMsg);
    return 0;
}

/**
 * Check whether a Component is in the Module
 * @param name The name of the component that shall be checked for
 * @return true if a Component with the given name is in the Module
 *\todo enhance this function up to now it is really inefficient
 */
bool Module::IsInModule(std::string name)
{
    //for each component in mComponents
    for (unsigned int i=0;i<mComponents.size();i++)
    {
        if (mComponents[i]->GetMOutputName()==name)
            return true;
        if (mComponents[i]->GetMType()==INPUT &&
            mComponents[i]->GetMInputName()==name)
            return true;
    }
    return false;
}

/**
 * Check whether a Component is in the Module
 * @param name The ID of the component that shall be checked for
 * @return true if a Component with the given ID is in the Module
 *\todo enhance this function up to now it is really inefficient
 */
bool Module::IsInModule(FckId id)
{
    return (mIdMap.end()!=mIdMap.find(id));
}

/**
 * Method to print which components the circuit contains.
 * Prints all components of the circuit with their type, fanin components
 * and fanout name.
 */
void Module::Print(std::string path)
{
    std::cout<<std::endl<<"Printing Module "<<mModuleName<<std::endl;
    //for each component in mComponents
    for (unsigned int i=0;i<mComponents.size();i++)
    {
    	if (mComponents[i]->GetMValid()==1) {  //edited by Son Lam 19/9/2013
			//print its type
			std::cout << mComponents[i]->GetType();
			std::cout<< " ID: " << mComponents[i]->GetMId()
					 << " Output: " << mComponents[i]->GetMOutputName()
					 << " Width: "<< mComponents[i]->GetMFanoutWidth()
					 <<" ["<<mComponents[i]->Getend_index()<<":"<<mComponents[i]->Getstart_index()<<"] "
					 <<" valid: "<<mComponents[i]->Getvalidname();
			std::cout<< " fanins ";
			if (mComponents[i]->GetMType()!=PS_INPUT)
			{
				for (unsigned int j=0;j<mComponents[i]->GetFanin().size();j++)
				{
					std::cout<<mComponents[i]->GetFanin()[j]->GetMId()<<" ";
				}
			}
			//for a PS_OUTPUT print whether it has a reset
			if (mComponents[i]->GetMType()==PS_OUTPUT)
				std::cout<<"Reset "<<mComponents[i]->GetReset()<<" ResetValue "
						<<mComponents[i]->GetResetValue();
			if ((mComponents[i]->GetMType()==INPUT)||(mComponents[i]->GetMType()==PS_INPUT))
				std::cout<<mComponents[i]->GetMInputName();
			std::cout<<std::endl;
    	}
    }

    //---------------------------- print dot
    std::ofstream of;
    std::string link;

    switch (NetlistKind) {
		case 1: link = "Results/Circuit/property.dot"; break;
		case 2: link = "Results/Circuit/ipc.dot"; break;
		default: link = "Results/Circuit/circuit.dot"; break;
    }

    link.insert(0, path);
    of.open(link.c_str(),std::fstream::out);
    if(!of) {
	std::cout<<"Khong tao dc dot file khi in circuit!\n";
	return;
    }
    of<< "digraph G { \n"<<std::endl;
    for (unsigned int i=0;i<mComponents.size();i++)
    {
    	if ((mComponents[i]->GetMType()==C_INPUT ) || (mComponents[i]->GetMType()==INPUT ) || (mComponents[i]->GetMType()==PS_INPUT ))
    		of<<mComponents[i]->GetMId()<<'['<<"label="<<'"'<<mComponents[i]->GetMInputName()<<'('<<mComponents[i]->GetMId()<<'-'<<mComponents[i]->GetType()<<')'<<'"'<<"];"<<std::endl;//<<'-'<<mComponents[i]->Getstart_index()<<'-'<<mComponents[i]->Getend_index()<<')'<<'"'<<"];"<<std::endl;
    	else
    		of<<mComponents[i]->GetMId()<<'['<<"label="<<'"'<<mComponents[i]->GetMOutputName()<<'('<<mComponents[i]->GetMId()<<'-'<<mComponents[i]->GetType()<<')'<<'"'<<"];"<<std::endl;//<<'-'<<mComponents[i]->Getstart_index()<<'-'<<mComponents[i]->Getend_index()<<')'<<'"'<<"];"<<std::endl;
    }

    for (unsigned int i=0;i<mComponents.size();i++)
    	if (mComponents[i]->GetMType()!=PS_INPUT)
    	{
			for (unsigned int j=0;j<mComponents[i]->GetFanin().size();j++)
				of<<mComponents[i]->GetFanin()[j]->GetMId()<<"->"<<mComponents[i]->GetMId()<<std::endl;
    	}

    of << "}";
    of.close();


}

/**
 * Chuyen doi FCK module thanh Btor module
 * tra ve BtorNode tro den output cua module
 */

BtorNode* Module::FCKtoBtor(Btor* btor)
{
	Component* output;
	for(unsigned int i=0; i < mComponents.size();i++)
	{
		if ((mComponents[i]->GetMType()==OUTPUT)&&(mComponents[i]->GetMOutputName()=="SAT?")) output = mComponents[i];
		bool n=0;
		for (unsigned int k=0; k < mComponents[i]->GetFanin().size(); k++)
			if (mComponents[i]->GetFanin()[k]->GetBtorComp()==NULL) n=1;
		if (n==0)
		{
			mComponents[i]->Create_BtorComp(btor);
		}
		else std::cout<<" da co loi xay ra trong qua trinh chuyen doi FCK to Btor "<<"\n";
	}
	return (output->GetBtorComp());
}



/**
 * Get the position of an Output of this Module.
 * @param name The name of the requested Output
 * @return the position of the requested Output
 */
int Module::GetOutput(std::string name)
{
    for (int i=0; i<mNumOuts; i++)
    {
        if (mOutputs[i]==name)
        {
            return i;
        }
    }
    return -1;
}

/**
 * Get the names of the Inputs to the Module
 * @return A vector containing the inputs
 */
std::vector<Component*> Module::GetModuleInputs()
{
    std::vector<Component*> inputs;
    for (int i=0;mComponents[i]->GetMType()==1;i++)
        inputs.push_back(mComponents[i]);
    return inputs;
}

/**
 * Get the width of the output.
 * @param output The name of the output that's width is requested
 */
unsigned int Module::GetOutputWidth(std::string output)
{
   Component* cmp = GetComponent(output);
   if (cmp == 0)
      return 0;
   return GetWidth(cmp->GetMId());
}

/**
 * Get the width of the output.
 * @param output The name of the output that's width is requested
 */
unsigned int Module::GetOutputWidth(FckId id)
{
   return GetWidth(id);
}
