/*! ************************************************************************
    \file unaryor.cpp 
    \brief Implementation of the Class UnaryOr

    The class contained in this file implements the methods of the Function
 * UnaryOr
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Mon Feb 13 2006 

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout - changed to supply ID by 
 * parameter
 * 26.10.06 SatI::RtpLit is replaced with RtpLit

**************************************************************************/

#include "unaryor.h"

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * A UnaryOr has one fanin Component so initialize mRequiredFanin with 1.
 * Add the fanin component to the fanin of this UnaryOr.
 * @param fanin A vector containing the Fanin Component of the UnaryOr. 
 * @param fanout The string naming the output
 * @param id The ID of the UnaryOr
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 */
UnaryOr::UnaryOr(std::vector<Component*>& fanin, std::string fanout, int id, 
                 Module* module)
{
    mModule=module;
    SetMRequiredFanin(1);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMType(U_OR);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;

    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have two fanin components so lets add these to our new UnaryOr
    //Function. First of all we allocate space for the Vector mFanin with
    //undefined fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //We use a loop Iterating from i=0 to mcRequiredFanin-1 and add the fanin
    //at position i to position i. 
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mcRequiredFanin;i++)
    mFanoutWidth=1;
} //End UnaryOr(st::vector<Component> fanin)  
/**
 * Methods
 */

/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin.
 */
void UnaryOr::SimulateForward () 
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
} // End SimulateForward

/**
 * perform a simulation of this UnaryOr for a given vector of inputs
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void UnaryOr::Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
{
    //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for
    //We reset outVals
    outVals.clear();
    //We have to iterate over the input bits. If all input bits are FALSE. The
    //result is FALSE, if one or more input bits are DONTCARE the result is 
    //either DONTCARE or TRUE. Is at least on input bit TRUE, so is the result
     //TRUE. We start with a result that is FALSE. If an input that is 
    //undefined is found we change the the result to DONTCARE. If an input 
    //that is TRUE is found the result is set to TRUE and we stop iterating.
    satlib::SatI::lValue result=satlib::SatI::FALSE;
    for (unsigned int i=0; i<inpVals[0].size() && result!=satlib::SatI::TRUE;i++)
    {
        //check the next input bit
        if(inpVals[0][i]==satlib::SatI::DONTCARE) 
            result=satlib::SatI::DONTCARE;
        else if (inpVals[0][i]==satlib::SatI::TRUE) result=satlib::SatI::TRUE;
    } // End for (unsigned int i=1; i<mInputs[0].size() && result!=FALSE;i++)
    outVals.push_back(result);
}//End Simulate

/**
 * Create the CNF for this UnaryOr.
 * @param solver the interface to the solver
 */
void UnaryOr::CreateCnf(satlib::SatI& satSolver)
{
    //vector for the literals of the result
    std::vector<satlib::RtpLit> r;
    //vector for the literals of the input
    std::vector<satlib::RtpLit> a;
    assert(GetFanin().size()==1);
    //Get the literals
    GetFanoutRtpLits(r);
    GetFanin()[0]->GetFanoutRtpLits(a);
    assert(r.size()==1);
    satSolver.makeOr(a,r[0]);
}

void UnaryOr::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_redor(btor, mFanin[0]->GetBtorComp());
	BtorComp = comp;
}
