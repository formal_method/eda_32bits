// -*- mode: C++; c-file-style: "cc-mode" -*-
//*************************************************************************
// DESCRIPTION: Verilator:
//
//*************************************************************************
#include "config_build.h"
#include "verilatedos.h"
#include <cstdio>
#include <cstdarg>
#include <unistd.h>
#include <algorithm>
#include <map>
#include "V3Global.h"
#include "V3NodeTraversal.h"
#include "V3Ast.h"
#include "string.h"




//Circuit* gcircuit = new Circuit();
std::vector<FckId> fanins;//create empty vector fanins of FckId
std::string fanout;
unsigned gid(0);	// using for create function component
ModuleId moduleID(1);

//int i(0);

class NodeVisitor : public AstNVisitor {
// Mark every node in the tree
private:
// NODE STATE
// Nothing!
// This may be called deep inside other routines
//
// so userp and friends may not be used
// VISITORS
virtual void visit(AstNode* nodep, AstNUser*) {
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	nodep->iterateChildren(*this); // loop with each children
}

virtual void visit(AstNetlist* nodep, AstNUser*) {
	nodep->iterateChildren(*this);
}

virtual void visit(AstModule* nodep, AstNUser*) {
	if (v3Global.get_compiler()==0) {
		// new code 30-07
		Module* pmodule = new Module(v3Global.circuit(),1);
		v3Global.SetModule(pmodule);
		string name = (string)nodep->name();
		if (name.find("TOP_")!=std::string::npos)
			name.erase(0,4);
		v3Global.module()->SetModuleName(name);
		// Khoi tao module map cho circuit
		std::map<ModuleId,Module*> modulemap;
		modulemap[1] = pmodule;
		v3Global.circuit()->SetModuleMap(modulemap);
		v3Global.circuit()->SetTopmodule(1);
		//std::cout<<"Dia chi cua circuit: "<<v3Global.circuit()<<std::endl;
		//std::cout<<"Dia chi cua module: "<<v3Global.module()<<std::endl;
	//	std::cout<<"Dia chi cua v3Global.circuit()->GetModuleMap()[1]: "<<v3Global.circuit()->GetModuleMap()[1]<<std::endl;
	//	std::cout<<"Dia chi cua v3Global.circuit()->GetModule(1): "<<v3Global.circuit()->GetModule(1)<<std::endl;
	//	std::cout<<"Dia chi cua v3Global.circuit()->GetTopmodule(): "<<v3Global.circuit()->GetTopmodule()<<std::endl;
		nodep->iterateChildren(*this); // loop with each children
	}
	else {
		PslProperty* propertyp = new PslProperty();
		v3Global.SetProperty(propertyp);
		string name = (string)nodep->name();
		if (name.find("TOP_")!=std::string::npos)
			name.erase(0,4);
		propertyp->SetName(name);
		nodep->iterateChildren(*this);
	}
}

//Tao cac Comp Input hoac Output

virtual void visit(AstVar* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<" type = "<<((AstVar*)nodep)->varType()<<"\n";
	if (v3Global.get_compiler()==0) {
		Module* pmodule = v3Global.circuit()->GetTopmodule();
		std::vector<unsigned int>* VecId;  //Vector luu cac id cua Comp tao duoc
		VecId = new vector<unsigned int>;
		FckId id(0);
		std::vector<FckId> fanin;
		string fanout=(string)nodep->name();
		if(nodep->isInput()){ //Neu la Input tao Comp ung voi kieu INPUT
			fanin.push_back(nodep->width());  //so bit cua Input
			id = pmodule->AddComponent(INPUT,fanin,fanout);
			pmodule->GetComponent(id)->Setstart_index(0);
			pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
			VecId->push_back(id);
		}
		else
		{  //Neu la Output ta se luu ten cua no vao vector mOutputs cua module de tao Comp Output sau
			if (nodep->isOutput())
			{
				FckId id1;
				fanin.push_back(nodep->width());  //so bit cua Output
				id1 = pmodule->AddComponent(UNDEF,fanin,fanout);
				pmodule->GetComponent(id1)->Setstart_index(0);
				pmodule->GetComponent(id1)->Setend_index(pmodule->GetComponent(id1)->Getstart_index() + pmodule->GetComponent(id1)->GetMFanoutWidth()-1);
				fanin.clear();
				fanin.push_back(id1);
				id = pmodule->AddComponent(OUTPUT,fanin,fanout);
				pmodule->GetComponent(id)->Setstart_index(0);
				pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
				pmodule->GetComponent(id)->Setvalidname(0);
				pmodule->GetComponent(id)->Setignore(1);

				//pmodule->SetMOutputs((string)nodep->name());
			}
		}
		nodep->user2vi(VecId);
	}
	else {
		PslProperty *pmodule = v3Global.property();
		if(((AstVar*)nodep)->varType() == AstVarType::WIRE){ //Neu la Input tao Comp ung voi kieu INPUT
			string fanout=(string)nodep->name();
			std::size_t pos;
			pos= fanout.find("v__DOT__");
			if (pos!=std::string::npos)
			  fanout.erase(0,8);
			pos= fanout.find("___024_");
			if (pos!=std::string::npos) {
				fanout.erase(0,pos);
				fanout.insert(0,"v");
			}
			pos= fanout.find("___024_");
			while (pos!=std::string::npos) {
				fanout.erase(pos,7);
				fanout.insert(pos,"__DOT__");
				pos= fanout.find("___024_");
			}
			std::cout<<" fanout = "<<fanout<<"\n";
			pmodule->GetmInputNames()->push_back(fanout);
		}
	}
}


/**
 * tao Comp ung voi AstNode VarRef
 */

virtual void visit(AstVarRef* nodep, AstNUser*) {
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	if (v3Global.get_compiler()==0) {
		Module* pmodule = v3Global.circuit()->GetTopmodule();
		std::vector<unsigned int>* VecId;  //Vector luu cac id cua Comp tao duoc
		VecId = new vector<unsigned int>;
		FckId id;
		int found=0;  //bao tim thay hay khong tim duoc cac Comp co cung ten da tao truoc do
		int same = 0;   //neu tin hieu varref giong tin hieu duoc dinh nghia trong always thi same = 1;


		//chi tim kiem hoac tao moi Comp voi nhung AstNode VarRef duoi day
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				(((string)nodep->user1p()->castNode()->typeName()!="ARRAYSEL")||(nodep->user3()!=1))&&
				(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")||(nodep->user3()!=2))&&
						(((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")||(nodep->user3()!=2))&&
						(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY")||(nodep->user3()!=2)))
		{

			//chi xet trong mach to hop, kiem tra tin hieu nay co giong tin hieu ma khoi always dang dinh nghia khong
			AstNode *tmpNode, *parentNode, * pointNode;;
			tmpNode = nodep;
			//std::cout<<"vao vong lap"<<std::endl;
			while (((string) tmpNode->typeName()!="ASSIGNALIAS")
						&& ((string) tmpNode->typeName()!="ASSIGNW")
						&& ((string) tmpNode->typeName()!="ASSIGN")
						&& ((string) tmpNode->typeName()!="ASSIGNDLY")
						&& ((string) tmpNode->typeName()!="SCOPE")
						)
			{
				if ((string) tmpNode->user1p()->castNode()->typeName()=="ASSIGN")
				{
					if ((string) tmpNode->user1p()->castNode()->op2p()->typeName()=="VARREF")
					{
						if ((string) nodep->name() == (string) tmpNode->user1p()->castNode()->op2p()->name())
							same = 1;
					}
					else if ((string) tmpNode->user1p()->castNode()->op2p()->typeName()=="SEL")
						{
							if ((string) nodep->name() == (string) tmpNode->user1p()->castNode()->op2p()->op1p()->name()
									&& (nodep!=tmpNode->user1p()->castNode()->op2p()->op1p()))
												same = 1;
						}
						else std::cout<<"da co loi xay ra doi voi ASSIGN cua AstGraph"<<std::endl;
				}
				tmpNode = tmpNode->user1p()->castNode();
			}

			//std::cout<<"ra vong lap"<<std::endl;

			//truong hop nay chua tong quat, can sua lai
			if ((nodep->user4()==0) && (same==1))
			{
				//neu same = 1 co nghia la tmpNode se luu dia chi cua AstNode ASSIGN
				//tim kiem Component gan nhat va dai dien cho tin hieu do trong khoi always
				//tim kiem tu begin...end chua node ASSIGN, neu ko thay t se tim o cac khoi begin...end rong hon
				//Note: code nay chi thuc hien tot cho truong hop tin hieu ben trai phep gan co do rong full width

				//std::cout<<"vao vong lap same"<<std::endl;
				parentNode = tmpNode;
				tmpNode = NULL; //khoi tao lai tmpNode de dung tmpNode vao viec duyet cac node trong cac vong lap tiep theo
				found = 0; //chua tim thay Component can tim
				while (((string) parentNode->typeName()!="ALWAYS") && (found==0))
				{
					if ((string) parentNode->user1p()->castNode()->typeName()=="ALWAYS")
						tmpNode=parentNode->user1p()->castNode()->op2p();
					else if ((string) parentNode->user1p()->castNode()->typeName()=="IF")
						{
							//std::cout<<"parentNode->user3()== "<<parentNode->user3()<<std::endl;
							if (parentNode->user3()==2)
								tmpNode = parentNode->user1p()->castNode()->op2p();
							else if (parentNode->user3()==3)
									tmpNode = parentNode->user1p()->castNode()->op3p();
								else std::cout<<"da co loi xay ra voi node IF va ASSIGN cua AstGraph"<<std::endl;
						}
						else std::cout<<"da co loi xay ra voi node ALWAYS, IF va ASSIGN cua AstGraph"<<std::endl;

					//duyet khoi begin...end da xac dinh duoc
					while ((tmpNode!=NULL)&&(found==0)&&(tmpNode!=parentNode))
					{
						if (tmpNode->user2p()==NULL) break;
						for (int i=0; i<tmpNode->user2vi()->size(); i++)
						{
							if ((pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMOutputName()== (string) nodep->name())
									&& (pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getvalidname()==1)
									&& (pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getarray_index()==-1)
									&& (pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMValid()==1)
									&& (pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getstart_index()==0)
									&& (pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMFanoutWidth()==nodep->width()))
							{
								id = pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMId();
								found = 1; //da tim thay component can tim
								break;
							}
						}
						tmpNode = tmpNode->nextp();
					}
					parentNode = parentNode->user1p()->castNode();
				}

				if (found==1)
				{
					//nhung node nao duoc tao tu ASTVarRef thi co bien varref =1;
					pmodule->GetComponent(id)->Setvarref(1);
					VecId->push_back(id);
					//user2vi se luu con tro den vector VecID ( Vector luu id cua cac Comp ung voi moi AstNode
					nodep->user2vi(VecId);
				}
				else std::cout<<"vi pham rang buoc hoac chua co dk khoi tao cho tin hieu nay trong khoi always"<<std::endl;
			}
			else
			{
				found = 0;
				// xu li VarRef cho truong hop cua phep gan blocking trong mach tuan tu
				tmpNode = nodep;

				if (nodep->user4()==1)
				{
					while (((string) tmpNode->typeName()!="ALWAYS") && ((string) tmpNode->typeName()!="ACTIVE") && (found==0))
					{
						switch (tmpNode->user3())  //cach xac dinh nay can thay the cho tat ca cac cach khac
						{
							case 1 : pointNode=tmpNode->user1p()->castNode()->op1p(); break;
							case 2 : pointNode=tmpNode->user1p()->castNode()->op2p(); break;
							case 3 : pointNode=tmpNode->user1p()->castNode()->op3p(); break;
							default : pointNode=tmpNode->user1p()->castNode()->op4p(); break;
						}

						//duyet khoi begin...end da xac dinh duoc
						while ((pointNode!=NULL)&&(found==0)&&(pointNode!=tmpNode))
						{
							if (pointNode->user2p()==NULL) break;
							for (int i=0; i<pointNode->user2vi()->size(); i++)
							{
								if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMOutputName()== (string) nodep->name())
										&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getvalidname()==1)
										&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getarray_index()==-1)
										&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMValid()==1)
										&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==0)
										&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMFanoutWidth()==nodep->width()))
								{
									//cout<<"da vao\n";
									id = pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMId();
									found = 1; //da tim thay component can tim
									break;
								}
							}
							pointNode = pointNode->nextp();
						}
						tmpNode = tmpNode->user1p()->castNode();
					}
				}

				// tim kiem trong vector mComponents module neu chua tim duoc
				if (found==0)
				{
					for (unsigned int j=0;j<pmodule->GetmComponents().size();j++)
					{
						if ((pmodule->GetmComponents()[j]->GetMOutputName()==(string) nodep->name())&&
								(pmodule->GetmComponents()[j]->GetMValid()==1)&&
								(pmodule->GetmComponents()[j]->Getarray_index()==-1)&&
								(pmodule->GetmComponents()[j]->Getvalidname()==1)&&
								(pmodule->GetmComponents()[j]->GetMFanoutWidth() == nodep->width())&&
								(pmodule->GetmComponents()[j]->Getstart_index() == 0))
						{
							found=1;
							id=pmodule->GetmComponents()[j]->GetMId();
							break;
						}
					}
				}

				//Neu khong tim thay thi tao mot Comp co kieu UNDEF voi ten la ten cua AstNode
				if (found==0) {
					std::vector<FckId> fanin;
					fanin.push_back(nodep->width());
					id = pmodule->AddComponent(0,fanin,(string)(nodep->name()));
					pmodule->GetComponent(id)->Setstart_index(0);
					pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
				}
				//nhung node nao duoc tao tu ASTVarRef thi co bien varref =1;
				pmodule->GetComponent(id)->Setvarref(1);
				VecId->push_back(id);
				//user2vi se luu con tro den vector VecID ( Vector luu id cua cac Comp ung voi moi AstNode
				nodep->user2vi(VecId);
			}
		}
		nodep->iterateChildren(*this);
		//cout<<nodep->typeName()<<"::"<<nodep->name()<<" (done)\n";
	}
	else {
		std::vector<unsigned int>* VecId;  //Vector luu cac id cua Comp tao duoc
		VecId = new vector<unsigned int>;
		FckId id;
		PslProperty *pmodule = v3Global.property();

		if (((string)nodep->user1p()->castNode()->typeName()!="ASSIGN") || (nodep->user3()!=2)){
			int found=0;  //bao tim thay hay khong tim duoc cac Comp co cung ten da tao truoc do
			string fanout=(string)nodep->name();
			std::size_t pos; // = str.find(str2);
			pos= fanout.find("v__DOT__");
			PslProperty *prop = v3Global.property();
			if (pos!=std::string::npos) fanout.erase(0,8);
			pos= fanout.find("___024_");
			if (pos!=std::string::npos) {
				fanout.erase(0,pos);
				fanout.insert(0,"v");
			}
			pos= fanout.find("___024_");
			while (pos!=std::string::npos) {
				fanout.erase(pos,7);
				fanout.insert(pos,"__DOT__");
				pos= fanout.find("___024_");
			}
			// trong truong hop tin hieu la dau vao cua property
			for (int i=0; i<prop->GetmInputNames()->size(); i++) {
				if (prop->GetmInputNames()->at(i) == fanout) {
					found = 1;
					break;
				}
			}
			if (found) {
				if (prop->GetmVariable()->find(make_pair(fanout,prop->GetMaxTimeFrame())) != prop->GetmVariable()->end()) id = prop->GetmVariable()->at(make_pair(fanout,prop->GetMaxTimeFrame()));
				else {
					id = prop->GetVariable(fanout,prop->GetMaxTimeFrame(), v3Global.circuit());
				}
			}
			else {
				// tim kiem trong vector mComponents module neu chua tim duoc
				found = 0;
				if (found==0) {
					for (unsigned int j=0;j<prop->GetmComponents().size();j++) {
						if ((prop->GetmComponents()[j]->GetMOutputName()==fanout)&&
								(prop->GetmComponents()[j]->GetMValid()==1)&&
								(prop->GetmComponents()[j]->Getvalidname()==1)&&
								(prop->GetmComponents()[j]->GetMFanoutWidth() == nodep->width()))
						{
							found=1;
							id=prop->GetmComponents()[j]->GetMId();
							break;
						}
					}
				}
				//Neu khong tim thay thi tao mot Comp co kieu UNDEF voi ten la ten cua AstNode
				if (found==0) std::cout<<" ERROR: bien chua duoc dinh nghia\n";
				else {
					//nhung node nao duoc tao tu ASTVarRef thi co bien varref =1;
					prop->GetComponent(id)->Setvarref(1);
				}
			}
		}
		VecId->push_back(id);
		nodep->user2vi(VecId);
		nodep->iterateChildren(*this);
	}
}

virtual void visit(AstDelay* nodep, AstNUser*) {
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	PslProperty *pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((string) nodep->op2p()->typeName()=="CONST") {
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstDelay does not include any Id of Components\n";
			return;
		} else
			pmodule->IncreaseMaxTimeFrame(nodep->op2p()->user2vi()->at(0));
	}
	else cout<<"Unsupport: Delay in Range of cycles\n";
}


virtual void visit(AstRepetition* nodep, AstNUser*) {
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	PslProperty *pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	std::vector<unsigned int>* VecId;  //Vector luu cac id cua Comp tao duoc
	VecId = new vector<unsigned int>;
	PslProperty *prop = v3Global.property();
	AstNode *parentNode = nodep->user1p()->castNode();
	int repeat;
	if (nodep->op2p()->user2p()==NULL) {
		cout<<"ERROR: child node at op2p() of AstRepetition does not include any Id of Components\n";
		return;
	} else repeat = nodep->op2p()->user2vi()->at(0);
	if (repeat == 0) cout<<"the number of repetitive cycles must be larger than 0\n";
	else if (repeat>1) {
	    VecId->push_back(parentNode->op2p()->user2vi()->at(0));
		for (int i=0; i< repeat -1; i++) {
			prop->IncreaseMaxTimeFrame(1);
		    if (parentNode->op2p()!=NULL) {
		    	parentNode->op2p()->iterateAndNext(*this);
			    VecId->push_back(parentNode->op2p()->user2vi()->at(0));
		    }

		}
	}
	parentNode->op2p()->user2vi(VecId);
}

virtual void visit(AstBackup* nodep, AstNUser*) {
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	PslProperty *pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	std::vector<unsigned int>* VecId;  //Vector luu cac id cua Comp tao duoc
	VecId = new vector<unsigned int>;
	PslProperty *prop = v3Global.property();
	// save the condition of assign local variables
	if (nodep->op1p()->user2p()==NULL) {
		cout<<"ERROR: child node at op1p() of AstBackup does not include any Id of Components\n";
		return;
	} else prop->GetmLocalVarConditions()->push_back(nodep->op1p()->user2vi()->at(0));
	AstNode *tmp = nodep->op2p();
	while (tmp!=NULL) {
		if (tmp->user2p()==NULL) {
			cout<<"ERROR: AstAssign does not include any Id of Components\n";
			return;
		} else VecId->push_back(tmp->user2vi()->at(0));
		tmp = tmp->nextp();
	}
	nodep->user2vi(VecId);
}


virtual void visit(AstSeqItem* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	PslProperty *pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if (((string)nodep->op2p()->typeName()) != "BACKUP") {
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstSeqItem does not include any Id of Components\n";
			return;
		} else
			for (int i=0; i<nodep->op2p()->user2vi()->size(); i++) {
				if (nodep->user3()==1) pmodule->AddAssumption(nodep->op2p()->user2vi()->at(i));
				else if (nodep->user3()==3) pmodule->AddCommitment(nodep->op2p()->user2vi()->at(i));
				else cout<<"ERROR: chua xach dinh duoc vi tri cua node so voi parent cua no\n";
			}
	}
}

virtual void visit(AstImplication* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	PslProperty *pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if (!((AstImplication*) nodep)->isOverlapped()) pmodule->IncreaseMaxTimeFrame(1);
}

virtual void visit(AstProposition* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	PslProperty *pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	std::vector<unsigned int>* VecId;
	VecId = new vector<unsigned int>;
	FckId id = pmodule->FinishProperty();
	std::vector<FckId> fanins;
	if ((string) nodep->user1p()->castNode()->typeName()=="MODULE") {
		fanins.clear();
		fanins.push_back(id);
		id = pmodule->AddComponent(OUTPUT,fanins,"");
	}
	VecId->push_back(id);
	nodep->user2vi(VecId);
}

virtual void visit(AstTopScope* nodep, AstNUser*){
	if (v3Global.get_compiler()==0) {
		nodep->iterateChildren(*this); // loop with each children
	}
}

virtual void visit(AstTypeTable* nodep, AstNUser*){
	if (v3Global.get_compiler()==0) {
		nodep->iterateChildren(*this); // loop with each children
	}
}

//Tao Comp ADD

virtual void visit(AstAdd* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		std::vector<FckId> fanin;  //vector fanin cua Comp can tao

		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstAdd does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstAdd does not include any Id of Components\n";
			return;
		}
		//tao fanin
		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}
*/
		// Edited by Son Lam 27/8/2013
		string fanout;  //fanout cua Comp can tao (fanout nay chi la tam thoi)
		fanout = (string) nodep->typeName();
		//tao Comp ADD voi cac thuoc tinh tren
		id = pmodule->AddComponent(ADD,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}

//Cac Comp the hien cac cong logic co ban tao gan giong nhu ADD


virtual void visit(AstNegate* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		std::vector<FckId> fanin;
		if(nodep->op1p()) {
			if (nodep->op1p()->user2p()==NULL) {
				cout<<"ERROR: child node at op1p() of AstNegate does not include any Id of Components\n";
				return;
			}
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0));
			pushIDnext(nodep->op1p(),fanin);
		}
		id = pmodule->AddComponent(MINUS,fanin,"MINUS");

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}




virtual void visit(AstSub* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id, id_minus;
		std::vector<FckId> fanin;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstSub does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstSub does not include any Id of Components\n";
			return;
		}

		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
		id_minus = pmodule->AddComponent(MINUS,fanin,"MINUS");

		fanin.clear();
		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		fanin.push_back(id_minus);
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}
*/
		// Edited by Son Lam 27/8/2013


		id = pmodule->AddComponent(ADD,fanin,"ADD");

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}


virtual void visit(AstMul* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id,id_const, idA, idB;
		std::vector<FckId> fanin;
		string width;
		std::vector<unsigned int> fanin_const;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstMul does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstMul does not include any Id of Components\n";
			return;
		}

		//Xac dinh A va B
		if(nodep->op1p()) {
			if (nodep->op1p()->width()>nodep->width()/2) {
				FckId id_zero, id_width, id_slice;
				std::vector<FckId> fan_slice;
				fan_slice.clear();
				id_zero = pmodule->AddComponent(C_INPUT,fan_slice,"00");
				id_width = pmodule->AddComponent(C_INPUT,fan_slice, convertdec2bin(nodep->width()/2));
				fan_slice.push_back(nodep->op1p()->user2vi()->at(0));
				fan_slice.push_back(id_zero);
				fan_slice.push_back(id_width);
				id_slice = pmodule->AddComponent(SLICE,fan_slice,"SLICE");
				idA = id_slice;
			}
			else {
				idA = (unsigned)nodep->op1p()->user2vi()->at(0);
			}
		}
		if(nodep->op2p()) {
			if (nodep->op2p()->width()>nodep->width()/2) {
				FckId id_zero, id_width, id_slice;
				std::vector<FckId> fan_slice;
				fan_slice.clear();
				id_zero = pmodule->AddComponent(C_INPUT,fan_slice,"00");
				id_width = pmodule->AddComponent(C_INPUT,fan_slice, convertdec2bin(nodep->width()/2));
				fan_slice.push_back(nodep->op2p()->user2vi()->at(0));
				fan_slice.push_back(id_zero);
				fan_slice.push_back(id_width);
				id_slice = pmodule->AddComponent(SLICE,fan_slice,"SLICE");
				idB = id_slice;
			} else {
				idB = (unsigned)nodep->op2p()->user2vi()->at(0);
			}
		}

		fanin.clear();
		fanin.push_back(idA);
		fanin.push_back(idB);

/*
		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
*/
		// Edited by Son Lam 27/8/2013
		string fanout;
		fanout = (string) nodep->typeName();

			id = pmodule->AddComponent(MULT,fanin,fanout);

		//lay du so bit can thiet
		if (nodep->width() < pmodule->GetComponent(id)->GetMFanoutWidth()) {
			fanin.clear();
			fanin_const.clear();
			fanin.push_back(id);
			id_const = pmodule->AddComponent(C_INPUT,fanin_const,"00");
			fanin.push_back(id_const);
			id_const = pmodule->AddComponent(C_INPUT,fanin_const,convertdec2bin(nodep->width()));
			fanin.push_back(id_const);
			id = pmodule->AddComponent(SLICE,fanin,fanout);
		}

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}




virtual void visit(AstMulS* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		std::vector<FckId> fanin, faninA,faninB;
		FckId id, idA, idB, idSel_As, idSel_Bs, idNeg_A, idNeg_B,  idEq_A, idEq_B, idIte_A, idIte_B;
		FckId id_const, idMul, id_xor, idNeg_Mul, idIte_Mul;
		int width;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstMulS does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstMulS does not include any Id of Components\n";
			return;
		}
		// xac dinh A va B

		if(nodep->op1p()) {
			if (nodep->op1p()->width()>nodep->width()/2) {
				FckId id_zero, id_width, id_slice;
				std::vector<FckId> fan_slice;
				fan_slice.clear();
				id_zero = pmodule->AddComponent(C_INPUT,fan_slice,"00");
				id_width = pmodule->AddComponent(C_INPUT,fan_slice, convertdec2bin(nodep->width()/2));
				fan_slice.push_back(nodep->op1p()->user2vi()->at(0));
				fan_slice.push_back(id_zero);
				fan_slice.push_back(id_width);
				id_slice = pmodule->AddComponent(SLICE,fan_slice,"SLICE");
				idA = id_slice;
			}
			else {
				idA = (unsigned)nodep->op1p()->user2vi()->at(0);
			}
		}
		if(nodep->op2p()) {
			if (nodep->op2p()->width()>nodep->width()/2) {
				FckId id_zero, id_width, id_slice;
				std::vector<FckId> fan_slice;
				fan_slice.clear();
				id_zero = pmodule->AddComponent(C_INPUT,fan_slice,"00");
				id_width = pmodule->AddComponent(C_INPUT,fan_slice, convertdec2bin(nodep->width()/2));
				fan_slice.push_back(nodep->op2p()->user2vi()->at(0));
				fan_slice.push_back(id_zero);
				fan_slice.push_back(id_width);
				id_slice = pmodule->AddComponent(SLICE,fan_slice,"SLICE");
				idB = id_slice;
			} else {
				idB = (unsigned)nodep->op2p()->user2vi()->at(0);
			}
		}

		width =  pmodule->GetComponent(idA)->GetMFanoutWidth() ;

		//tach bit dau
		faninA.clear();
		faninB.clear();
		fanin.clear();
		faninA.push_back(idA);
		faninB.push_back(idB);

		id_const = pmodule->AddComponent(C_INPUT,fanin,convertdec2bin(width-1));
		faninA.push_back(id_const);
		faninB.push_back(id_const);

		id_const = pmodule->AddComponent(C_INPUT,fanin,"1");
		faninA.push_back(id_const);
		faninB.push_back(id_const);

		idSel_As = pmodule->AddComponent(SLICE,faninA,"SLICE");
		idSel_Bs = pmodule->AddComponent(SLICE,faninB,"SLICE");


		//xac dinh dau
		faninA.clear();
		faninB.clear();
		fanin.clear();

		id_const = pmodule->AddComponent(C_INPUT,fanin,"1");
		faninA.push_back(idSel_As);
		faninA.push_back(id_const);
		faninB.push_back(idSel_Bs);
		faninB.push_back(id_const);

		idEq_A = pmodule->AddComponent(EQ,faninA,"EQ");
		idEq_B = pmodule->AddComponent(EQ,faninB,"EQ");

		//lay so doi
		faninA.clear();
		faninB.clear();
		fanin.clear();

		faninA.push_back(idA);
		faninB.push_back(idB);

		idNeg_A = pmodule->AddComponent(MINUS,faninA,"MINUS");
		idNeg_B = pmodule->AddComponent(MINUS,faninB,"MINUS");

		//tao bo ITE de lay Abs(A)
		faninA.clear();
		faninB.clear();
		fanin.clear();

		faninA.push_back(idEq_A);
		faninA.push_back(idNeg_A);
		faninA.push_back(idA);
		idIte_A = pmodule->AddComponent(ITE,faninA,"ITE");

		faninB.push_back(idEq_B);
		faninB.push_back(idNeg_B);
		faninB.push_back(idB);
		idIte_B = pmodule->AddComponent(ITE,faninB,"ITE");

		//tao bo nhan ko dau
		fanin.clear();
		fanin.push_back(idIte_A);
		fanin.push_back(idIte_B);

		idMul = pmodule->AddComponent(MULT,fanin,"MUL");

		// tao bo xor de xac dinh su khac dau
		fanin.clear();
		fanin.push_back(idEq_A);
		fanin.push_back(idEq_B);

		id_xor = pmodule->AddComponent(XOR,fanin,"XOR");

		//lay so doi cua ket qua bo nhan ko dau
		fanin.clear();
		fanin.push_back(idMul);

		idNeg_Mul = pmodule->AddComponent(MINUS,fanin,"MINUS");

		//tao ket qua nhan co dau bang ITE
		fanin.clear();
		fanin.push_back(id_xor);
		fanin.push_back(idNeg_Mul);
		fanin.push_back(idMul);

		idIte_Mul = pmodule->AddComponent(ITE,fanin,"ITE");

		//lay du so bit can thiet
		if (nodep->width() < pmodule->GetComponent(idIte_Mul)->GetMFanoutWidth()) {
			faninA.clear();
			fanin.clear();
			faninA.push_back(idIte_Mul);
			id_const = pmodule->AddComponent(C_INPUT,fanin,"00");
			faninA.push_back(id_const);
			id_const = pmodule->AddComponent(C_INPUT,fanin, convertdec2bin(nodep->width()));
			faninA.push_back(id_const);

			id = pmodule->AddComponent(SLICE,faninA,"SLICE");
		}
		else id = idIte_Mul;
		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);

		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}

virtual void visit(AstAnd* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	if ((string)nodep->op2p()->typeName() == "PROPOSITION") {
	    //ASTNODE_PREFETCH(nodep->op2p());
	    //ASTNODE_PREFETCH(nodep->op1p());
	    // if () not needed since iterateAndNext accepts null this, but faster with it.
	    //duyet op2p truoc
	    if (nodep->op2p()) nodep->op2p()->iterateAndNext(*this);
	    //op1p duyet sau
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
	    int MaxTimeFrame = ((PslProperty*)pmodule)->GetMaxTimeFrame();
	    vector<FckId> ResetDisable;
	    vector<FckId> fanins;
	    FckId id;
		((PslProperty*)pmodule)->SetMaxTimeFrame(((PslProperty*)pmodule)->GetMinTimeFrame());
		while (((PslProperty*)pmodule)->GetMaxTimeFrame() <= MaxTimeFrame) {
			if (nodep->op1p()) nodep->op1p()->iterateAndNext(*this);
			if (nodep->op1p()->user2p()==NULL) {
				cout<<"ERROR: child node at op1p() of AstAnd does not include any Id of Components\n";
				return;
			}
			ResetDisable.push_back(nodep->op1p()->user2vi()->at(0));
			((PslProperty*)pmodule)->IncreaseMaxTimeFrame(1);
		}

		FckId reset = 0;
		if (ResetDisable.size()!=0)
		   {
		      reset = ResetDisable.back();
		      ResetDisable.pop_back();
		   }
		   while (!ResetDisable.empty())
		   {
		      fanins.clear();
		      fanins.push_back(reset);
		      fanins.push_back(ResetDisable.back());
		      ResetDisable.pop_back();
		      reset = ((PslProperty*)pmodule)->AddComponent(AND,fanins,"");
		   }
		if (reset!=0) {
			fanins.clear();
			fanins.push_back(reset);
			reset = ((PslProperty*)pmodule)->AddComponent(NOT,fanins,"");
			//tao cong Or
			fanins.clear();
			fanins.push_back(reset);
			fanins.push_back(nodep->op2p()->user2vi()->at(0));
			id = ((PslProperty*)pmodule)->AddComponent(OR,fanins,"");
			//tao OUTPUT
			fanins.clear();
			fanins.push_back(id);
			id = ((PslProperty*)pmodule)->AddComponent(OUTPUT,fanins,"");
		}
		((PslProperty*)pmodule)->SetMaxTimeFrame(MaxTimeFrame);
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
	else {
		nodep->iterateChildren(*this);
		if ((nodep->user4()<3) || (v3Global.get_compiler()))
		{
			// thuc hien lap tren cac con

			//nodep->iterateChildren(*this);

			// tao component
			std::vector<unsigned int>* VecId;
			VecId = new vector<unsigned int>;
			FckId id;
			std::vector<FckId> fanin;
			std::vector<Component*> fanincmps;

			if (nodep->op1p()->user2p()==NULL) {
				cout<<"ERROR: child node at op1p() of AstAnd does not include any Id of Components\n";
				return;
			}
			if (nodep->op2p()->user2p()==NULL) {
				cout<<"ERROR: child node at op2p() of AstAnd does not include any Id of Components\n";
				return;
			}

			if(nodep->op1p()) {
				fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
				pushIDnext(nodep->op1p(),fanin);
			}
			if(nodep->op2p()) {
					fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
					pushIDnext(nodep->op2p(),fanin);
				}
	/*		if(nodep->op3p()) {
					fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
					pushIDnext(nodep->op3p(),fanin);
				}
			if(nodep->op4p()) {
					fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
					pushIDnext(nodep->op4p(),fanin);
				}
	*/
			// Edited by Son Lam 27/8/2013
			string fanout;
			fanout = (string) nodep->typeName();

			if ((string) nodep->op2p()->typeName() == "CCAST") {
				id = pmodule->AddComponent(EQ,fanin,"EQ");
			}
			else
				id = pmodule->AddComponent(AND,fanin,fanout);

			// xac dinh do hop le cua ten
			if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
					((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
					((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
					((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
				pmodule->GetComponent(id)->Setvalidname(0);
			// store id
			VecId->push_back(id);
			nodep->user2vi(VecId);
		}
	}
}


virtual void visit(AstOr* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		std::vector<FckId> fanin;
		std::vector<Component*> fanincmps;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstOr does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstOr does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}
*/
		// Edited by Son Lam 27/8/2013
		string fanout;
		fanout = (string) nodep->typeName();

			id = pmodule->AddComponent(OR,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}


virtual void visit(AstXor* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		std::vector<FckId> fanin;
		std::vector<Component*> fanincmps;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstXor does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstXor does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}
*/
		// Edited by Son Lam 27/8/2013
		string fanout;
		fanout = (string) nodep->typeName();

			id = pmodule->AddComponent(XOR,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}



virtual void visit(AstXnor* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id, id_xor;
		std::vector<FckId> fanin;
		std::vector<Component*> fanincmps;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstXnor does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstXnor does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}


		//tao cong XOR
		id_xor = pmodule->AddComponent(XOR,fanin,"XOR");
		// Edited by Son Lam 27/8/2013
		string fanout;
		fanout = (string) nodep->typeName();
		//tao cong XNOR
		fanin.clear();
		fanin.push_back(id_xor);
		id = pmodule->AddComponent(NOT,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}



virtual void visit(AstShiftR* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		std::vector<FckId> fanin;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstShiftR does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstShiftR does not include any Id of Components\n";
			return;
		}

		//trong truong hop dich theo hang so da dinh truoc
		if ((string)nodep->op2p()->typeName()=="CONST") {
			if(nodep->op1p()) {
				fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
				pushIDnext(nodep->op1p(),fanin);
			}
			if(nodep->op2p()) {
					fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
					pushIDnext(nodep->op2p(),fanin);
				}
			// Edited by Son Lam 27/8/2013
			string fanout;
			fanout = (string) nodep->typeName();

			id = pmodule->AddComponent(SHR,fanin,fanout);
		}
		else {
			//trong truong hop so lan dich xac dinh theo gia tri bien{
			//kiem tra lai cac dieu kien ve do rong bit
			//doi voi bo dich nay, do rong bit cua dau ra bang 2 mu so bit cua bien luu so lan dich
			//chu y kiem tra cac dieu kien bit-width
			FckId id_one, id_const, id_const_sel, id_sh, id_sel;

			//tao hang so 1
			fanin.clear();
			id_one = pmodule->AddComponent(C_INPUT,fanin,"1");
			id = (unsigned int) nodep->op1p()->user2vi()->at(0);
			for (int i = nodep->op2p()->width()-1; i>=0; i--) {
				//tao hang so ung voi chi so cua so bit dang xet trong bien dich (shamt)
				fanin.clear();
				id_const_sel = pmodule->AddComponent(C_INPUT,fanin,convertdec2bin(i));
				//tao bo slice cho bien dich
				fanin.clear();
				fanin.push_back((unsigned int) nodep->op2p()->user2vi()->at(0));
				fanin.push_back(id_const_sel);
				fanin.push_back(id_one);
				id_sel = pmodule->AddComponent(SLICE,fanin,"SLICE");

				//tao hang so ung voi so lan dich tuong ung
				fanin.clear();
				id_const = pmodule->AddComponent(C_INPUT,fanin,convertdec2bin_width(powerof2(i),nodep->op2p()->width()));

				//tao bo dich
				fanin.clear();
				fanin.push_back(id);
				fanin.push_back(id_const);
				id_sh = pmodule->AddComponent(SHR,fanin,"SHR");

				//tao bo ITE
				fanin.clear();
				fanin.push_back(id_sel);
				fanin.push_back(id_sh);
				fanin.push_back(id);
				id = pmodule->AddComponent(ITE,fanin,"ITE");
				//ket thuc
			}
		}

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}




virtual void visit(AstShiftRS* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{

		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstShiftRS does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstShiftRS does not include any Id of Components\n";
			return;
		}

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id,id_in, id_shamt, id_width_extend;
		std::vector<FckId> fanin;
		fanin.clear();

		//chu y kiem tra dieu kien ve bit-width

		// mo rong dau cua bien can dich len bang 2^(bit-width_of_shamt + 1) voi shamt la bien luu so lan dich
		int width_of_shamt = nodep->op2p()->width();
		id_width_extend = pmodule->AddComponent(C_INPUT,fanin,convertdec2bin(powerof2(width_of_shamt+1)));
		//tao sign_extend de mo rong dau cho input
		fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0));
		fanin.push_back(id_width_extend);
		id_in = pmodule->AddComponent(SIGN_EXTEND,fanin,"SIGN_EXTEND");
		fanin.clear();
		//mo rong bit cho shamt
		id_width_extend = pmodule->AddComponent(C_INPUT,fanin,convertdec2bin(width_of_shamt+1));
		fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
		fanin.push_back(id_width_extend);
		id_shamt = pmodule->AddComponent(ZERO_EXTEND,fanin,"ZERO_EXTEND");

		//thuc hien phep dich
		FckId id_one, id_const, id_const_sel, id_sh, id_sel;

		//tao hang so 1
		fanin.clear();
		id_one = pmodule->AddComponent(C_INPUT,fanin,"1");
		id = id_in;
		for (int i = width_of_shamt; i>=0; i--) {
			//tao hang so ung voi chi so cua so bit dang xet trong bien dich (shamt)
			fanin.clear();
			id_const_sel = pmodule->AddComponent(C_INPUT,fanin,convertdec2bin(i));
			//tao bo slice cho bien dich
			fanin.clear();
			fanin.push_back(id_shamt);
			fanin.push_back(id_const_sel);
			fanin.push_back(id_one);
			id_sel = pmodule->AddComponent(SLICE,fanin,"SLICE");

			//tao hang so ung voi so lan dich tuong ung
			fanin.clear();
			id_const = pmodule->AddComponent(C_INPUT,fanin,convertdec2bin_width(powerof2(i),width_of_shamt + 1));

			//tao bo dich
			fanin.clear();
			fanin.push_back(id);
			fanin.push_back(id_const);
			id_sh = pmodule->AddComponent(SHR,fanin,"SHR");

			//tao bo ITE
			fanin.clear();
			fanin.push_back(id_sel);
			fanin.push_back(id_sh);
			fanin.push_back(id);
			id = pmodule->AddComponent(ITE,fanin,"ITE");
			//ket thuc
		}

		//trich chon lay du so bit can thiet = width of op1p()
		fanin.clear();
		id_const_sel = pmodule->AddComponent(C_INPUT,fanin,"0");
		id_const = pmodule->AddComponent(C_INPUT,fanin,convertdec2bin(nodep->op1p()->width()));
		fanin.push_back(id);
		fanin.push_back(id_const_sel);
		fanin.push_back(id_const);
		id = pmodule->AddComponent(SLICE,fanin,"SLICE");

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}





virtual void visit(AstShiftL* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		std::vector<FckId> fanin;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstShiftL does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstShiftL does not include any Id of Components\n";
			return;
		}

		//trong truong hop dich theo hang so da dinh truoc
		if ((string)nodep->op2p()->typeName()=="CONST") {
			if(nodep->op1p()) {
				fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
				pushIDnext(nodep->op1p(),fanin);
			}
			if(nodep->op2p()) {
					fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
					pushIDnext(nodep->op2p(),fanin);
				}
			// Edited by Son Lam 27/8/2013
			string fanout;
			fanout = (string) nodep->typeName();

			id = pmodule->AddComponent(SHL,fanin,fanout);
		}
		else {
			//trong truong hop so lan dich xac dinh theo gia tri bien{
			//kiem tra lai cac dieu kien ve do rong bit
			//doi voi bo dich nay, do rong bit cua dau ra bang 2 mu so bit cua bien luu so lan dich
			//chu y nen kiem tra dieu kien ve bit-width
			FckId id_one, id_const, id_const_sel, id_sh, id_sel;

			//tao hang so 1
			fanin.clear();
			id_one = pmodule->AddComponent(C_INPUT,fanin,"1");
			id = (unsigned int) nodep->op1p()->user2vi()->at(0);
			for (int i = nodep->op2p()->width()-1; i>=0; i--) {
				//tao hang so ung voi chi so cua so bit dang xet trong bien dich (shamt)
				fanin.clear();
				id_const_sel = pmodule->AddComponent(C_INPUT,fanin,convertdec2bin(i));
				//tao bo slice cho bien dich
				fanin.clear();
				fanin.push_back((unsigned int) nodep->op2p()->user2vi()->at(0));
				fanin.push_back(id_const_sel);
				fanin.push_back(id_one);
				id_sel = pmodule->AddComponent(SLICE,fanin,"SLICE");

				//tao hang so ung voi so lan dich tuong ung
				fanin.clear();
				id_const = pmodule->AddComponent(C_INPUT,fanin,convertdec2bin_width(powerof2(i),nodep->op2p()->width()));

				//tao bo dich
				fanin.clear();
				fanin.push_back(id);
				fanin.push_back(id_const);
				id_sh = pmodule->AddComponent(SHL,fanin,"SHR");

				//tao bo ITE
				fanin.clear();
				fanin.push_back(id_sel);
				fanin.push_back(id_sh);
				fanin.push_back(id);
				id = pmodule->AddComponent(ITE,fanin,"ITE");
				//ket thuc
			}
		}

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}



virtual void visit(AstEq* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		std::vector<FckId> fanin;
		std::vector<Component*> fanincmps;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstEq does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstEq does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}
*/
		// Edited by Son Lam 27/8/2013
		string fanout;
		fanout = (string) nodep->typeName();

			id = pmodule->AddComponent(EQ,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}


virtual void visit(AstNeq* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id, id1;
		std::vector<FckId> fanin;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstNeq does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstNeq does not include any Id of Components\n";
			return;
		}

		// tao Component EQ
		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}
*/
		// Edited by Son Lam 27/8/2013
		string fanout;
		fanout = "EQ";

		id = pmodule->AddComponent(EQ,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);

		// tao them Comp Not de tao thanh Neq

		fanin.clear();
		fanin.push_back(id);
		fanout = "NOT";
		id1 = pmodule->AddComponent(NOT,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id1)->Setvalidname(0);

		// store id
		VecId->push_back(id1);
		nodep->user2vi(VecId);
	}
}


virtual void visit(AstLt* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		std::vector<FckId> fanin;
		std::vector<Component*> fanincmps;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstLt does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstLt does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}
*/
		// Edited by Son Lam 27/8/2013
		string fanout;
		fanout = (string) nodep->typeName();

			id = pmodule->AddComponent(LT,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}


virtual void visit(AstLtS* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id, idA, idB, id_const, id_zero, id_one, idEq, idEx_A, idEx_B, idNeg_Ex_A, idAdd, idSel, idOr;
		std::vector<FckId> fanin;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstLtS does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstLtS does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) idA = (unsigned)nodep->op1p()->user2vi()->at(0);
		if(nodep->op2p()) idB = (unsigned)nodep->op2p()->user2vi()->at(0);

		//tao cac hang so can thiet
		//hang so mo rong dau (width + 1)
		fanin.clear();
		id_const = pmodule->AddComponent(C_INPUT,fanin,convertdec2bin(nodep->op1p()->width() + 1));
		//tao hang so 0
		id_zero = pmodule->AddComponent(C_INPUT,fanin,"0");
		//tap hang so 1
		id_one = pmodule->AddComponent(C_INPUT,fanin,"1");

		//kiem tra dk bang nhau cua A va B
		fanin.clear();
		fanin.push_back(idA);
		fanin.push_back(idB);
		idEq = pmodule->AddComponent(EQ,fanin,"EQ");

		//mo rong dau cho A va B
		fanin.clear();
		fanin.push_back(idA);
		fanin.push_back(id_const);
		idEx_A = pmodule->AddComponent(SIGN_EXTEND,fanin,"SIGN_EXTEND");

		fanin.clear();
		fanin.push_back(idB);
		fanin.push_back(id_const);
		idEx_B = pmodule->AddComponent(SIGN_EXTEND,fanin,"SIGN_EXTEND");

		//tao so doi cua A
		fanin.clear();
		fanin.push_back(idEx_A);
		idNeg_Ex_A = pmodule->AddComponent(MINUS,fanin,"MINUS");

		//thuc hien phep tru B - A
		fanin.clear();
		fanin.push_back(idEx_B);
		fanin.push_back(idNeg_Ex_A);
		idAdd = pmodule->AddComponent(ADD,fanin,"ADD");

		//tach lay bit dau cua phep tru
		//tao hang so ung voi vi tri bit dau
		fanin.clear();
		id_const = pmodule->AddComponent(C_INPUT,fanin,convertdec2bin(nodep->op1p()->width()));
		fanin.push_back(idAdd);
		fanin.push_back(id_const);
		fanin.push_back(id_one);
		idSel = pmodule->AddComponent(SLICE,fanin,"SLICE");

		//Xac dinh gia tri A<B
		fanin.clear();
		fanin.push_back(idEq);
		fanin.push_back(idSel);
		idOr = pmodule->AddComponent(OR,fanin,"OR");

		fanin.clear();
		fanin.push_back(idOr);
		id = pmodule->AddComponent(NOT,fanin,"NOT");

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}



virtual void visit(AstGtS* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id, idA, idB, id_const, id_zero, id_one, idEq, idEx_A, idEx_B, idNeg_Ex_B, idAdd, idSel, idOr;
		std::vector<FckId> fanin;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstGtS does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstGtS does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) idA = (unsigned)nodep->op1p()->user2vi()->at(0);
		if(nodep->op2p()) idB = (unsigned)nodep->op2p()->user2vi()->at(0);

		//tao cac hang so can thiet
		//hang so mo rong dau (width + 1)
		fanin.clear();
		id_const = pmodule->AddComponent(C_INPUT,fanin,convertdec2bin(nodep->op1p()->width() + 1));
		//tao hang so 0
		id_zero = pmodule->AddComponent(C_INPUT,fanin,"0");
		//tap hang so 1
		id_one = pmodule->AddComponent(C_INPUT,fanin,"1");

		//kiem tra dk bang nhau cua A va B
		fanin.clear();
		fanin.push_back(idA);
		fanin.push_back(idB);
		idEq = pmodule->AddComponent(EQ,fanin,"EQ");

		//mo rong dau cho A va B
		fanin.clear();
		fanin.push_back(idA);
		fanin.push_back(id_const);
		idEx_A = pmodule->AddComponent(SIGN_EXTEND,fanin,"SIGN_EXTEND");

		fanin.clear();
		fanin.push_back(idB);
		fanin.push_back(id_const);
		idEx_B = pmodule->AddComponent(SIGN_EXTEND,fanin,"SIGN_EXTEND");

		//tao so doi cua B
		fanin.clear();
		fanin.push_back(idEx_B);
		idNeg_Ex_B = pmodule->AddComponent(MINUS,fanin,"MINUS");

		//thuc hien phep tru A - B
		fanin.clear();
		fanin.push_back(idEx_A);
		fanin.push_back(idNeg_Ex_B);
		idAdd = pmodule->AddComponent(ADD,fanin,"ADD");

		//tach lay bit dau cua phep tru
		//tao hang so ung voi vi tri bit dau
		fanin.clear();
		id_const = pmodule->AddComponent(C_INPUT,fanin,convertdec2bin(nodep->op1p()->width()));
		fanin.push_back(idAdd);
		fanin.push_back(id_const);
		fanin.push_back(id_one);
		idSel = pmodule->AddComponent(SLICE,fanin,"SLICE");

		//Xac dinh gia tri A<B
		fanin.clear();
		fanin.push_back(idEq);
		fanin.push_back(idSel);
		idOr = pmodule->AddComponent(OR,fanin,"OR");

		fanin.clear();
		fanin.push_back(idOr);
		id = pmodule->AddComponent(NOT,fanin,"NOT");

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}




virtual void visit(AstGt* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id,id1,id2,id3;
		std::vector<FckId> fanin;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstGt does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstGt does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}
*/
		// Edited by Son Lam 27/8/2013
		string fanout;
		fanout = "LT";

		id1 = pmodule->AddComponent(LT,fanin,fanout);
		pmodule->GetComponent(id1)->Setvalidname(0);

		fanout = "EQ";
		id2 = pmodule->AddComponent(EQ,fanin,fanout);
		pmodule->GetComponent(id2)->Setvalidname(0);

		fanout = "OR";
		fanin.clear();
		fanin.push_back(id1);
		fanin.push_back(id2);
		id3= pmodule->AddComponent(OR,fanin,fanout);
		pmodule->GetComponent(id3)->Setvalidname(0);

		fanout = "NOT";
		fanin.clear();
		fanin.push_back(id3);
		id = pmodule->AddComponent(NOT,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);

		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}


virtual void visit(AstLte* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id,id1,id2;
		std::vector<FckId> fanin;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstLte does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstLte does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}
*/
		// Edited by Son Lam 27/8/2013
		string fanout;

		fanout = "LT";
		id1 = pmodule->AddComponent(LT,fanin,fanout);
		pmodule->GetComponent(id1)->Setvalidname(0);

		fanout = "EQ";
		id2 = pmodule->AddComponent(EQ,fanin,fanout);
		pmodule->GetComponent(id2)->Setvalidname(0);

		fanout = "OR";
		fanin.clear();
		fanin.push_back(id1);
		fanin.push_back(id2);
		id= pmodule->AddComponent(OR,fanin,fanout);
		pmodule->GetComponent(id)->Setvalidname(0);


		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);

		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}


virtual void visit(AstGte* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id,id1;
		std::vector<FckId> fanin;
		std::vector<Component*> fanincmps;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstGte does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstGte does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}
*/
		// Edited by Son Lam 27/8/2013
		string fanout;

		fanout = "LT";
		id1 = pmodule->AddComponent(LT,fanin,fanout);
		pmodule->GetComponent(id1)->Setvalidname(0);

		fanout = "NOT";
		fanin.clear();
		fanin.push_back(id1);
		id = pmodule->AddComponent(NOT,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}


virtual void visit(AstConcat* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		std::vector<FckId> fanin;
		std::vector<Component*> fanincmps;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstConcat does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstConcat does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}
*/
		// Edited by Son Lam 27/8/2013
		string fanout;
		fanout = (string) nodep->typeName();

			id = pmodule->AddComponent(CONCAT,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}

//Tao Comp SLICE
virtual void visit(AstSel* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id(0);
		std::vector<FckId> fanin;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstSel does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstSel does not include any Id of Components\n";
			return;
		}
		if (nodep->op3p()->user2p()==NULL) {
			cout<<"ERROR: child node at op3p() of AstSel does not include any Id of Components\n";
			return;
		}

		if ((string) nodep->op2p()->typeName()=="VARREF")
		{ // day la truong hop co dang f = in[x]; voi x la 1 bien
			std::vector<FckId> fanin_sel; // fanin cua cac Component SLICE se tao ra lam dau vao cho bo MUX
			std::vector<FckId> fanin_const;
			FckId id_sel, id_index, id_one;

			// xac dinh hang so co gia tri bang 1
			//fanin_const.clear();
			id_one = nodep->op3p()->user2vi()->at(0);
			if (convertbin2dec(pmodule->GetComponent(id_one)->GetMOutputName())!= 1)
				std::cout<<" error: so bit duoc chon lon hon 1\n";
			// tao cac fanin cho bo MUX va them chung vao vector fanin
			fanin.clear();
			fanin.push_back(nodep->op2p()->user2vi()->at(0));
			for (int i=0; i<nodep->op1p()->width(); i++)
			{
				string fanout_const;
				fanout_const = convertdec2bin(i);
				fanin_const.clear();
				id_index = pmodule->AddComponent(C_INPUT,fanin_const,fanout_const);
				//tao SLICE
				fanin_sel.clear();
				fanin_sel.push_back(nodep->op1p()->user2vi()->at(0));
				fanin_sel.push_back(id_index);
				fanin_sel.push_back(id_one);
				id_sel = pmodule->AddComponent(SLICE,fanin_sel,"SLICE");
				fanin.push_back(id_sel);
			}
			id = pmodule->AddComponent(MUX,fanin,"MUX");
			// xac dinh do hop le cua ten
			if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
					((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
					((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
					((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
				pmodule->GetComponent(id)->Setvalidname(0);
			VecId->push_back(id);
		}
		else
		{
			if(nodep->op1p()) {
				fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
				pushIDnext(nodep->op1p(),fanin);
			}
			if(nodep->op2p()) {
					fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
					pushIDnext(nodep->op2p(),fanin);
				}
			if(nodep->op3p()) {
					fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
					pushIDnext(nodep->op3p(),fanin);
				}
/*			if(nodep->op4p()) {
					fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
					pushIDnext(nodep->op4p(),fanin);
				}*/
			// Edited by Son Lam 27/8/2013
			string fanout;
			fanout = (string) nodep->typeName();
			id = pmodule->AddComponent(SLICE,fanin,fanout);

			// xac dinh do hop le cua ten
			if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
					((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
					((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
					((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
				pmodule->GetComponent(id)->Setvalidname(0);
			// store id
			VecId->push_back(id);
		}
		nodep->user2vi(VecId);
	}
}


//Tao Comp NOT
virtual void visit(AstNot* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		std::vector<FckId> fanin;
		std::vector<Component*> fanincmps;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstNot does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}

		// Edited by Son Lam 27/8/2013
		string fanout;
		fanout = (string) nodep->typeName();

			id = pmodule->AddComponent(NOT,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}


//Tao Comp unaryand
virtual void visit(AstRedAnd* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con
		//nodep->iterateChildren(*this);
		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		std::vector<FckId> fanin;
		std::vector<Component*> fanincmps;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstRedAnd does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}*/
		// Edited by Son Lam 27/8/2013
		string fanout;
		fanout = (string) nodep->typeName();

			id = pmodule->AddComponent(U_AND,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}


//Tao Comp unaryor
virtual void visit(AstRedOr* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con
		//nodep->iterateChildren(*this);
		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		std::vector<FckId> fanin;
		std::vector<Component*> fanincmps;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstRedOr does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}*/
		// Edited by Son Lam 27/8/2013
		string fanout;
		fanout = (string) nodep->typeName();

			id = pmodule->AddComponent(U_OR,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}


//Tao Comp unaryxor
virtual void visit(AstRedXor* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con
		//nodep->iterateChildren(*this);
		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		std::vector<FckId> fanin;
		std::vector<Component*> fanincmps;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstXor does not include any Id of Components\n";
			return;
		}

		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}*/
		// Edited by Son Lam 27/8/2013
		string fanout;
		fanout = (string) nodep->typeName();

			id = pmodule->AddComponent(U_XOR,fanin,fanout);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id)->Setvalidname(0);
		// store id
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}

virtual void visit(AstReplicate* nodep, AstNUser*) {
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);  //tham cac node con truoc
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		int num_rep;  // so lan nhan ban
		FckId id_rep;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstReplicate does not include any Id of Components\n";
			return;
		}

		id_rep = nodep->op1p()->user2vi()->at(0);
		std::vector<FckId> fanin;
		string fanout = "CONCAT";

		num_rep = convertbin2dec(converthex2bin(nodep->op2p()->name()));
		for (int i=0; i<num_rep-1; i++)
		{
			FckId id;
			fanin.clear();
			fanin.push_back(id_rep);
			fanin.push_back(nodep->op1p()->user2vi()->at(0));

			id = pmodule->AddComponent(CONCAT,fanin,fanout);
			id_rep = id;


		}

		if (pmodule->GetComponent(id_rep)->GetMFanoutWidth()!=nodep->width())
			std::cout<<"da co loi xay ra trong tao Repliceta "<<std::endl;
		else
		{
			VecId->push_back(id_rep);

			// xac dinh do hop le cua ten
			if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
					((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
					((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
					((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
				pmodule->GetComponent(id_rep)->Setvalidname(0);

			// store id
			nodep->user2vi(VecId);
		}
	}
}


//AstCCast
virtual void visit(AstCCast* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// thuc hien lap tren cac con

		//nodep->iterateChildren(*this);

		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id1, id2;
		std::vector<FckId> fanin;
		string fanout_Const;
		//tao Comp CONST co do rong 32 bit va co gia tri bang width
		fanout_Const = convertdec2bin (nodep->size());
		id1 = pmodule->AddComponent(C_INPUT,fanin,fanout_Const);

		// tao Comp Extend
		fanin.clear();
		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}
*/
		fanin.push_back(id1);


		// Edited by Son Lam 27/8/2013
		string fanout_Extend;
		fanout_Extend = (string) nodep->typeName();

		id2 = pmodule->AddComponent(ZERO_EXTEND,fanin,fanout_Extend);

		//xu ly chinh cho AstCCast
		if ((string)nodep->user1p()->castNode()->typeName()=="AND") {
			if ((string)nodep->user1p()->castNode()->op1p()->typeName() == "CONST") {
				fanin.clear();
				fanin.push_back(id2);
				fanin.push_back(nodep->user1p()->castNode()->op1p()->user2vi()->at(0));
				id1 = pmodule->AddComponent(AND,fanin,"");
			}
			else std::cout<<"loi o AstCCast \n";
		}
		else std::cout<<"loi o AstCCast \n";

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id1)->Setvalidname(0);
		// store id
		VecId->push_back(id1);
		nodep->user2vi(VecId);
	}
}


//Mo rong bit bang cach them cac bit 0
virtual void visit(AstExtend* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id1, id2;
		std::vector<FckId> fanin;
		string fanout_Const;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstExtend does not include any Id of Components\n";
			return;
		}

		//tao Comp CONST co do rong 32 bit va co gia tri bang width
		fanout_Const = convertdec2bin (nodep->width());
		id1 = pmodule->AddComponent(C_INPUT,fanin,fanout_Const);

		// tao Comp Extend
		fanin.clear();
		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
/*		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}
*/
		fanin.push_back(id1);


		// Edited by Son Lam 27/8/2013
		string fanout_Extend;
		fanout_Extend = (string) nodep->typeName();

		id2 = pmodule->AddComponent(ZERO_EXTEND,fanin,fanout_Extend);
		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id2)->Setvalidname(0);
		// store id
		VecId->push_back(id2);
		nodep->user2vi(VecId);
	}
}

//Mo rong bit bang cach them cac bit dau
virtual void visit(AstExtendS* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		// tao component
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id1, id2;
		std::vector<FckId> fanin;
		string fanout_Const;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstExtendS does not include any Id of Components\n";
			return;
		}

		//tao Comp CONST co do rong 32 bit va co gia tri bang width
		fanout_Const = convertdec2bin (nodep->width());
		id1 = pmodule->AddComponent(C_INPUT,fanin,fanout_Const);

		// tao Comp SIGN_EXTEND
		fanin.clear();
		if(nodep->op1p()) {
			fanin.push_back((unsigned)nodep->op1p()->user2vi()->at(0) );
			pushIDnext(nodep->op1p(),fanin);
		}
		if(nodep->op2p()) {
				fanin.push_back((unsigned)nodep->op2p()->user2vi()->at(0));
				pushIDnext(nodep->op2p(),fanin);
			}
		if(nodep->op3p()) {
				fanin.push_back((unsigned)nodep->op3p()->user2vi()->at(0));
				pushIDnext(nodep->op3p(),fanin);
			}
/*		if(nodep->op4p()) {
				fanin.push_back((unsigned)nodep->op4p()->user2vi()->at(0));
				pushIDnext(nodep->op4p(),fanin);
			}
*/
		fanin.push_back(id1);


		// Edited by Son Lam 27/8/2013
		string fanout_Extend;
		fanout_Extend = (string) nodep->typeName();

		id2 = pmodule->AddComponent(SIGN_EXTEND,fanin,fanout_Extend);

		// xac dinh do hop le cua ten
		if(((string)nodep->user1p()->castNode()->typeName()!="ASSIGNW")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNALIAS")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGN")&&
				((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY"))
			pmodule->GetComponent(id2)->Setvalidname(0);
		// store id
		VecId->push_back(id2);
		nodep->user2vi(VecId);
	}
}

//Tao khoi RAM
virtual void visit(AstArraySel* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	if (v3Global.get_compiler()==0) {
		Module* pmodule = v3Global.circuit()->GetTopmodule();
		if ((nodep->user4()<3) || (v3Global.get_compiler()))
		{
			std::vector<unsigned int>* VecId;
			VecId = new vector<unsigned int>;

			unsigned int size;  //so o nho cua Ram
			unsigned int addr_width; //so bit dia chi
			FckId id_psi0,id;
			std::vector<FckId> fanin;
			string size_in_binary;
			string fanout;
			int found = 0;

			if (nodep->op1p()!=NULL)
			{
				fanout = nodep->op1p()->name();
				AstArrayDType* Array = nodep->op1p()->dtypep()->castArrayDType();
				size = Array->msb() - Array->lsb() +1 ;
				size_in_binary = convertdec2bin(size-1);
				addr_width = size_in_binary.length();

				// taoj Ram neu no chua duoc tao
				//tim xem no da duoc tao chua
				for (unsigned int j=0;j<pmodule->GetmComponents().size();j++)
					if ((pmodule->GetmComponents()[j]->GetMType()==PS_INPUT) &&
							(pmodule->GetmComponents()[j]->GetMValid()==1)&&
							(pmodule->GetmComponents()[j]->Getarray_index()==0)&&
							(pmodule->GetmComponents()[j]->GetMOutputName() == fanout)&&
							(pmodule->GetmComponents()[j]->Getstart_index()==0)&&
							(pmodule->GetmComponents()[j]->GetMFanoutWidth() ==nodep->width()))
					{
						found = 1;
						id_psi0 = pmodule->GetmComponents()[j]->GetMId();
						break;
					}
				// tao ram neu truoc do no chua duoc tao
				if (found==0)
				{
					FckId id_psi,id_psi_before,id_pso;
					fanin.push_back(nodep->width());
					for (int i=0; i<size; i++)
					{
						id_psi = pmodule->AddComponent(PS_INPUT,fanin,fanout);
						pmodule->GetComponent(id_psi)->Setarray_index(i);
						pmodule->GetComponent(id_psi)->Setstart_index(0);
						pmodule->GetComponent(id_psi)->Setend_index(nodep->width()-1);

						if (i==0) id_psi0 = id_psi;
						else
						{
							((PseudoInput*)pmodule->GetComponent(id_psi))->Setpsi_before(id_psi_before);
							((PseudoInput*)pmodule->GetComponent(id_psi_before))->Setpsi_after(id_psi);
						}
						id_psi_before = id_psi;
					}
				}
			}

			nodep->iterateChildren(*this);

			if (((string)nodep->user1p()->castNode()->typeName()!="ASSIGNDLY")||(nodep->user3()!=2))
			{
				if (test_ram_addr(pmodule->GetComponent(nodep->op2p()->user2vi()->at(0)))==0)
				{
					unsigned int array_index = convertbin2dec(converthex2bin(nodep->op2p()->name()));
					if (array_index==0) id = id_psi0;
					else {
						id = id_psi0;
						for (int i=0; i<array_index; i++)
						{
							id = ((PseudoInput*)pmodule->GetComponent(id))->Getpsi_after();
						}
					}
					// tao bo dem cho PSI
					fanin.clear();
					fanin.push_back(id);
					fanin.push_back(id);
					id = pmodule->AddComponent(AND,fanin,"AND");
					VecId->push_back(id);
				}
				else
				{
					FckId id_psi;
					fanin.clear();
					fanin.push_back(nodep->op2p()->user2vi()->at(0));
					id_psi = id_psi0;
					while (id_psi != 0)
					{
						fanin.push_back(id_psi);
						id_psi = ((PseudoInput*)pmodule->GetComponent(id_psi))->Getpsi_after();
					}
					if (size < powerof2(addr_width))
					{
						FckId id_zero;
						std::vector<FckId> fanin_zero;
						fanin_zero.clear();
						id_zero = pmodule->AddComponent(C_INPUT,fanin_zero,convertdec2bin_width(0,nodep->width()));
						for (int i=0; i<(powerof2(addr_width)-size); i++)
							fanin.push_back(id_zero);

					}
					id = pmodule->AddComponent(MUX,fanin,fanout);
					VecId->push_back(id);
				}
			}
			else
			{
				VecId->push_back(id_psi0);
			}
			if ((string)nodep->op2p()->typeName()=="CONST")
				v3Global.circuit()->deletecomp(pmodule->GetComponent(nodep->op2p()->user2vi()->at(0)));
			nodep->user2vi(VecId);
		}
	}
}


virtual void visit(AstCondBound* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		/*FckId id;
		//nodep->iterateChildren(*this);
		std::vector<FckId> fanin;
		string fanout;
		fanin.push_back(nodep->op1p()->user2vi()->at(0));
		fanin.push_back(nodep->op2p()->user2vi()->at(0));
		fanin.push_back(nodep->op3p()->user2vi()->at(0));
		fanout = "ITE";
		id = pmodule->AddComponent(ITE,fanin,fanout);
		*/

		for (int i=0; i<nodep->op2p()->user2vi()->size(); i++)
			VecId->push_back(nodep->op2p()->user2vi()->at(i));
		v3Global.circuit()->deletecomp(pmodule->GetComponent(nodep->op1p()->user2vi()->at(0)));
		v3Global.circuit()->deletecomp(pmodule->GetComponent(nodep->op3p()->user2vi()->at(0)));
		//VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}


//Tao hang so

virtual void visit(AstConst* nodep, AstNUser*){
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	if (v3Global.get_compiler()==0) {
		Module* pmodule = v3Global.circuit()->GetTopmodule();
		if ((nodep->user4()<3) || (v3Global.get_compiler()))
		{
			std::vector<unsigned int>* VecId;
			VecId = new vector<unsigned int>;
			FckId id;
			std::vector<unsigned int> fanin;
			string hex_num = (string) nodep->name();  //hang so o dang hexa doc tu AstNode
			string binary_num;  //hang so o dang binary

			binary_num = converthex2bin(hex_num);

			//dam bao kich thuoc binary_num trung voi so bit can tao cua hang so
			if (binary_num.length()>nodep->width())
				binary_num.erase(0, binary_num.length()- nodep->width());
			else
				binary_num.insert(0,nodep->width()-binary_num.length(),'0' );
			id = pmodule->AddComponent(C_INPUT,fanin,binary_num);
			VecId->push_back(id);
			nodep->user2vi(VecId);
		}
	}
	else {
		PslProperty *pmodule = v3Global.property();
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		if (((string)nodep->user1p()->castNode()->typeName()!="DELAY") && ((string)nodep->user1p()->castNode()->typeName()!="REPETITION")) {
			string hex_num = (string) nodep->name();  //hang so o dang hexa doc tu AstNode
			string binary_num;  //hang so o dang binary

			binary_num = converthex2bin(hex_num);

			//dam bao kich thuoc binary_num trung voi so bit can tao cua hang so
			if (binary_num.length()>nodep->width())
				binary_num.erase(0, binary_num.length()- nodep->width());
			else
				binary_num.insert(0,nodep->width()-binary_num.length(),'0' );
			id = pmodule->GetConstant(binary_num);
		}
		else {
			id = convertbin2dec(converthex2bin((string) nodep->name()));
		}
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}


// COND tuong duong voi IF..ELSE cho 1 tin hieu
// Tao Comp If then else tuong ung cho no
virtual void visit(AstCond* nodep, AstNUser*) {
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule;
	if (v3Global.get_compiler()==0)
		pmodule = v3Global.circuit()->GetTopmodule();
	else pmodule = v3Global.property();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		//nodep->iterateChildren(*this);
		std::vector<FckId> fanin;
		string fanout;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstCond does not include any Id of Components\n";
			return;
		}
		if (nodep->op2p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstCond does not include any Id of Components\n";
			return;
		}

		if (nodep->op3p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstCond does not include any Id of Components\n";
			return;
		}

		fanin.push_back(nodep->op1p()->user2vi()->at(0));
		fanin.push_back(nodep->op2p()->user2vi()->at(0));
		fanin.push_back(nodep->op3p()->user2vi()->at(0));

		fanout = "ITE";
		id = pmodule->AddComponent(ITE,fanin,fanout);

		// tren COND co the la COND hoac ASSIGN
		if((string)nodep->user1p()->castNode()->typeName()=="COND")
			pmodule->GetComponent(id)->Setvalidname(0);

		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}


//bien dich cu phap "assign", phep gan khong nam trong khoi always

virtual void visit(AstAssignW* nodep, AstNUser*) {
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule = v3Global.circuit()->GetTopmodule();
	nodep->iterateChildren(*this);  //tham cac node con truoc
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id(0);
		std::vector<unsigned int> fanin;


		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op1p() of AstAssignW does not include any Id of Components\n";
			return;
		}

		string fanout = (string) nodep->op2p()->name();  //ten cua left hand side
		string str = (string)(nodep->op1p()->typeName()); //Kieu AstNode ung voi o op1p()


		// trong truong hop gan lhs = rhs ta se tao bo BUFF (hoac AND voi 2 dau vao cung 1 tin hieu)
		if((str=="VARREF")||(str=="CONST")){
			fanin.push_back(nodep->op1p()->user2vi()->at(0));
			fanin.push_back(nodep->op1p()->user2vi()->at(0));
			id = pmodule->AddComponent(AND,fanin,fanout);
		}
		else
		{ //neu khong phai tao BUFF ta se gan ten cho Comp ung voi op1p la name cua op2p
			// tim component co id luu trong op1p
			Component *pComp = pmodule->GetComponent(nodep->op1p()->user2vi()->at(0));
			if(pComp!=NULL) pComp->SetMOutputName(fanout);
			else cout<<"Component with id "<<nodep->op1p()->user2vi()->at(0)<< " not found!\n";
			id = nodep->op1p()->user2vi()->at(0);
		}

		if ((string)nodep->op2p()->typeName()=="VARREF")
		{
			pmodule->GetComponent(id)->Setstart_index(0);
			pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);


			//tim Component UNDEF co ten trung voi ten cua nodep o phai nodep assignw de thay the
			for (unsigned int j=0;j<pmodule->GetmComponents().size();j++)
				if ((pmodule->GetmComponents()[j]->GetMType()==UNDEF) &&
						(pmodule->GetmComponents()[j]->GetMValid()==1)&&
						(pmodule->GetmComponents()[j]->Getarray_index()==-1)&&
						(pmodule->GetmComponents()[j]->GetMOutputName() == pmodule->GetComponent(id)->GetMOutputName())&&
						(pmodule->GetmComponents()[j]->Getstart_index()==pmodule->GetComponent(id)->Getstart_index())&&
						(pmodule->GetmComponents()[j]->GetMFanoutWidth() ==pmodule->GetComponent(id)->GetMFanoutWidth())
						)
				{
					pmodule->GetmComponents()[j]->SetMValid(0);
					// thay UNDEF trong cac ket noi bang Component ADD vua tao
					for (unsigned int i= 0; i< pmodule->GetmComponents()[j]->GetFanout().size(); i++)
					{
						//gan cac vector mFanout cua UNDEF cho component ADD vua tao
						pmodule->GetComponent(id)->AddFanout(pmodule->GetmComponents()[j]->GetFanout().at(i));
						//voi moi fanout do, tim trong fanin cua no vi tri chua UNDEF vao thay bang ADD
						pmodule->GetmComponents()[j]->GetFanout().at(i)->ChangeFanin(pmodule->GetmComponents()[j],pmodule->GetComponent(id));
					}
					break;
				}
		}
		// xu li ten va chi so
		if ((string)nodep->op2p()->typeName()=="SEL")
		{
			pmodule->GetComponent(id)->SetMOutputName((string) nodep->op2p()->op1p()->name());
			pmodule->GetComponent(id)->Setstart_index(convertbin2dec(converthex2bin((string) nodep->op2p()->op2p()->name())));
			pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
			//std::cout<<"start_index= "<<pmodule->GetComponent(id)->Getstart_index()<<std::endl;
			if (pmodule->GetComponent(nodep->op2p()->user2vi()->at(0))->GetFanin()[0]->GetMType()==UNDEF)
				{
					Component* Undef_Comp = pmodule->GetComponent(nodep->op2p()->user2vi()->at(0))->GetFanin()[0];
					Undef_Comp->Addelements(pmodule->GetComponent(id));
				}
			else std::cout<<"da co loi xay ra"<<std::endl;
			v3Global.circuit()->deletecomp(pmodule->GetComponent(nodep->op2p()->user2vi()->at(0)));

		}

		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}

//bien dich cu phap "assign", phep gan nam trong khoi always ko co clock
virtual void visit(AstAssign* nodep, AstNUser*) {
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	if (v3Global.get_compiler()==0) {
		Module* pmodule = v3Global.circuit()->GetTopmodule();
		if ((nodep->user4()<3) || (v3Global.get_compiler()))
		{
			std::vector<unsigned int>* VecId;
			VecId = new vector<unsigned int>;
			//nodep->iterateChildren(*this);

			FckId id(0), id1(0);
			AstNode *pointNode, *parentNode;
			std::vector<unsigned int> fanin;
			string fanout;

			string str = (string)(nodep->op1p()->typeName());
			int found=0;
			int found_concat=0;

			// neu day la phep gan blocking trong mach tuan tu thi phai dam bao tin hieu ben trai phep gan da co PS_INPUT tuong ung
			if ((nodep->user4()==1)&&((string) nodep->op2p()->typeName()!="ARRAYSEL"))
			{
				unsigned int start_index;
				unsigned int width;
				if ((string) nodep->op2p()->typeName()=="VARREF")
				{
					fanout = (string)nodep->op2p()->name();
					start_index = 0;
					width = nodep->op2p()->width();
				}
				else if ((string) nodep->op2p()->typeName()=="SEL")
				{
					fanout = (string) nodep->op2p()->op1p()->name();
					start_index = convertbin2dec(converthex2bin((string) nodep->op2p()->op2p()->name()));
					width = nodep->op2p()->op1p()->width();
				}

				for (unsigned int i=0;i<pmodule->GetmComponents().size();i++)
				{
					if ((pmodule->GetmComponents()[i]->GetMOutputName()==fanout)
							&& (pmodule->GetmComponents()[i]->GetMValid()==1)
							&& (pmodule->GetmComponents()[i]->Getarray_index()==-1)
							&& (pmodule->GetmComponents()[i]->Getvalidname()==1)
							&& (pmodule->GetmComponents()[i]->Getstart_index()==0)
							&& (pmodule->GetmComponents()[i]->GetMFanoutWidth()==width)
							)
					{
						if (pmodule->GetmComponents()[i]->GetMType()==UNDEF)
						{
							found=1; // da tim thay
							fanin.clear();
							fanin.push_back(width);
							id = pmodule->AddComponent(PS_INPUT,fanin,fanout);
							pmodule->GetComponent(id)->Setstart_index(0);
							pmodule->GetComponent(id)->Setend_index(width-1);
							// thay UNDEF trong cac ket noi bang Component PS_INPUT vua tao
							for (unsigned int j= 0; j< pmodule->GetmComponents()[i]->GetFanout().size(); j++)
							{
								//gan cac vector mFanout cua UNDEF cho component PS_INPUT vua tao
								pmodule->GetComponent(id)->AddFanout(pmodule->GetmComponents()[i]->GetFanout().at(j));
								//voi moi fanout do, tim trong fanin cua no vi tri chua UNDEF vao thay bang ADD
								pmodule->GetmComponents()[i]->GetFanout().at(j)->ChangeFanin(pmodule->GetmComponents()[i],pmodule->GetComponent(id));
							}
							//Xoa Comp UNDEF
							pmodule->GetmComponents()[i]->SetMValid(0);
						}
						else
						{
							if (pmodule->GetmComponents()[i]->GetMType()==PS_INPUT)
							{
								found=1; // da tim thay
								id = pmodule->GetmComponents()[i]->GetMId();
							}
						}
						if (found==1) break;
					}
				}
				if (found==0)
				{
					fanin.clear();
					fanin.push_back(width);
					id = pmodule->AddComponent(PS_INPUT,fanin,fanout);
					pmodule->GetComponent(id)->Setstart_index(0);
					pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->GetMFanoutWidth()-1);
				}
			}

			nodep->iterateChildren(*this);

			if (nodep->op1p()->user2p()==NULL) {
				cout<<"ERROR: child node at op2p() of AstAssign does not include any Id of Components\n";
				return;
			}

			if (((string)nodep->op2p()->typeName()=="SEL") && ((string)nodep->op2p()->op2p()->typeName()=="VARREF"))
			{ //truong hop nay ung voi cu phap f[x] = in; voi x la mot bien
				// tim gia tri cua f truoc do, mac dinh cu phap nay f phai co gia tri mac dinh
				FckId id_default;
				bool default_found=0;
				parentNode = nodep;
				while (((string) parentNode->typeName()!="ALWAYS") && (default_found==0))
				{
					if ((string)parentNode->user1p()->castNode()->typeName()=="ALWAYS")
						pointNode = parentNode->user1p()->castNode()->op2p();
					else if ((string)parentNode->user1p()->castNode()->typeName()=="IF")
						{
							if (parentNode->user3()==2)
								pointNode = parentNode->user1p()->castNode()->op2p();
							else if (parentNode->user3()==3)
								pointNode = parentNode->user1p()->castNode()->op3p();
								else std::cout<<"da co loi xay ra voi IF va cac op() cua no"<<std::endl;
						}
							else std::cout<<"da co loi xay ra voi node IF va ALWAYS cua AstGraph "<<std::endl;
					// Duyet khoi begin...end da xac dinh
					while ((pointNode!=NULL)&&(default_found==0))
					{
						if (pointNode->user2p()==NULL) break;
						else
						{
							for (int i=0; i<pointNode->user2vi()->size(); i++)
							{
								if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMOutputName()==pmodule->GetComponent(nodep->op2p()->op1p()->user2vi()->at(0))->GetMOutputName())
										&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(nodep->op2p()->op1p()->user2vi()->at(0))->Getstart_index())
										&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMFanoutWidth()==pmodule->GetComponent(nodep->op2p()->op1p()->user2vi()->at(0))->GetMFanoutWidth())
										&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMValid()==1)
										&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getarray_index()==pmodule->GetComponent(nodep->op2p()->op1p()->user2vi()->at(0))->Getarray_index())
										//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getvalidname()==0)
										&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getignore()==0))
								{
									id_default = pointNode->user2vi()->at(i);
									default_found=1;
									break;
								}
							}
							pointNode = pointNode->nextp();
						}
					}
					parentNode = parentNode->user1p()->castNode();
				}

				if (default_found==0) std::cout<<" da co loi xay ra trong tim gia tri mac dinh cua tin hieu \n";
				else
				{// khi da tim thay gia tri mac dinh tien hanh tao bo decoder de xac dinh vi tri bit can duoc thay doi gia tri
					FckId id_decoder;
					FckId id_one, id_index, id_sel_decoder, id_sel_default , id_ite, id_concat_before, id_concat;
					id_concat_before = 0;
					fanin.clear();
					fanin.push_back(nodep->op2p()->op2p()->user2vi()->at(0));
					id_decoder = pmodule->AddComponent(DECODER,fanin,"DECODER");
					//fanin.clear();
					id_one = nodep->op2p()->op3p()->user2vi()->at(0);
					if (convertbin2dec(pmodule->GetComponent(id_one)->GetMOutputName())!=1)
						std::cout<<" da co loi xay ra: so bit can thay doi gia tri lon hon 1\n";
					for (int j=0; j<nodep->op2p()->op1p()->width(); j++)
					{
						fanin.clear();
						string index;
						index = convertdec2bin(j);
						id_index = pmodule->AddComponent(C_INPUT,fanin,index);
						fanin.push_back(id_decoder);
						fanin.push_back(id_index);
						fanin.push_back(id_one);
						id_sel_decoder = pmodule->AddComponent(SLICE,fanin,"SLICE");
						fanin.clear();
						fanin.push_back(id_default);
						fanin.push_back(id_index);
						fanin.push_back(id_one);
						id_sel_default = pmodule->AddComponent(SLICE,fanin,"SLICE");
						//tao bo ITE
						fanin.clear();
						fanin.push_back(id_sel_decoder);
						fanin.push_back(nodep->op1p()->user2vi()->at(0));
						fanin.push_back(id_sel_default);
						id_ite = pmodule->AddComponent(ITE,fanin,"ITE");
						if (id_concat_before==0)
						{
							id_concat_before = id_ite;
						}
						else
						{
							fanin.clear();
							fanin.push_back(id_ite);
							fanin.push_back(id_concat_before);
							id_concat = pmodule->AddComponent(CONCAT,fanin,"CONCAT");
							id_concat_before = id_concat;
						}
					}
					id = id_concat;
					// gan gia tri cho cac thuoc tinh khac
					pmodule->GetComponent(id)->SetMOutputName((string) nodep->op2p()->op1p()->name());
					pmodule->GetComponent(id)->Setstart_index(pmodule->GetComponent(nodep->op2p()->op1p()->user2vi()->at(0))->Getstart_index());
					pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id_concat)->GetMFanoutWidth()-1);
					v3Global.circuit()->deletecomp(pmodule->GetComponent(nodep->op2p()->user2vi()->at(0)));
				}
			}
			else
			{
				fanout = (string) nodep->op2p()->name();

				// trong truong hop gan lhs = rhs ta se tao bo BUFF (hoac AND voi 2 dau vao cung 1 tin hieu)
				if((str=="VARREF")||(str=="CONST")){
					fanin.clear();
					fanin.push_back(nodep->op1p()->user2vi()->at(0));
					fanin.push_back(nodep->op1p()->user2vi()->at(0));
					id = pmodule->AddComponent(AND,fanin,fanout);
				}
				else
				{
					// tim component co id luu trong op1p
					Component *pComp = pmodule->GetComponent(nodep->op1p()->user2vi()->at(0));
					if(pComp!=NULL) pComp->SetMOutputName(fanout);
					else cout<<"Component with id "<<nodep->op1p()->user2vi()->at(0)<< " not found!\n";
					id = nodep->op1p()->user2vi()->at(0);
				}

				if ((string)nodep->op2p()->typeName()=="VARREF")
				{
					pmodule->GetComponent(id)->Setstart_index(0);
					pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
				}
				else if ((string)nodep->op2p()->typeName()=="SEL")
				{
					pmodule->GetComponent(id)->SetMOutputName((string) nodep->op2p()->op1p()->name());
					pmodule->GetComponent(id)->Setstart_index(convertbin2dec(converthex2bin((string) nodep->op2p()->op2p()->name())));
					pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
					//std::cout<<"start_index= "<<pmodule->GetComponent(id)->Getstart_index()<<std::endl;
					v3Global.circuit()->deletecomp(pmodule->GetComponent(nodep->op2p()->user2vi()->at(0)));
				}
			}

			// xoa cac component ko dung hoac xoa ten cua no de dam bao co duy nhat 1 ten hop le tai 1 khoi begin...end
			if ((string) nodep->user1p()->castNode()->typeName()=="ALWAYS")
				pointNode = nodep->user1p()->castNode()->op2p();
			else if ((string) nodep->user1p()->castNode()->typeName()=="IF")
				{
					if (nodep->user3()==2)
						pointNode = nodep->user1p()->castNode()->op2p();
					else pointNode = nodep->user1p()->castNode()->op3p();
				}


			found = 0;
			while ((pointNode!=NULL)&&(found==0))
			{
				if (pointNode->user2p()==NULL) break;
				else
				{
					for (int i=0; i<pointNode->user2vi()->size(); i++)
					{
						if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMOutputName()==pmodule->GetComponent(id)->GetMOutputName())
								&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(id)->Getstart_index())
								&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMFanoutWidth()==pmodule->GetComponent(id)->GetMFanoutWidth())
								&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMValid()==1)
								&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getarray_index()==pmodule->GetComponent(id)->Getarray_index())
								&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getvalidname()==1)
								&& (pointNode->user2vi()->at(i)!=id))
						{
							v3Global.circuit()->deletecomp(pmodule->GetComponent(pointNode->user2vi()->at(i)));
							pmodule->GetComponent(pointNode->user2vi()->at(i))->Setvalidname(0);
							found = 1;
						}
					}
					pointNode = pointNode->nextp();
				}
			}

			// gop tin hieu neu can thiet
			if ((string) nodep->user1p()->castNode()->typeName()=="IF")
			{
				AstNode* tmpNode;
				found_concat = 0;
				if (nodep->user3()==2)
					tmpNode = nodep->user1p()->castNode()->op2p();
				else if (nodep->user3()==3)
					tmpNode = nodep->user1p()->castNode()->op3p();
				else std::cout<<" AstGraph co loi xay ra"<<std::endl;

				while ((tmpNode!=nodep)&& (found_concat==0))
				{
					for (int i=0; i<tmpNode->user2vi()->size(); i++)
					{
						if ((pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMOutputName()== pmodule->GetComponent(id)->GetMOutputName())
								&& (pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getvalidname()==1)
								&& (pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMValid()==1)
								&& (pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getarray_index()==pmodule->GetComponent(id)->Getarray_index())
								)
						{
							if (pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getstart_index() == pmodule->GetComponent(id)->Getend_index()+1)
							{
								found_concat = 1; //da tim thay Component can gop
								fanout = pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMOutputName();
								fanin.clear();
								fanin.push_back(tmpNode->user2vi()->at(i));
								fanin.push_back(id);
								id1 = pmodule->AddComponent(CONCAT,fanin,fanout);
								pmodule->GetComponent(id1)->Setstart_index(pmodule->GetComponent(id)->Getstart_index());
								pmodule->GetComponent(id1)->Setend_index(pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getend_index());
							}
							else if (pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getend_index() == pmodule->GetComponent(id)->Getstart_index()-1)
							{
								found_concat = 1; //da tim thay Component can gop
								fanout = pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMOutputName();
								fanin.clear();
								fanin.push_back(id);
								fanin.push_back(tmpNode->user2vi()->at(i));
								id1 = pmodule->AddComponent(CONCAT,fanin,fanout);
								pmodule->GetComponent(id1)->Setstart_index(pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getstart_index());
								pmodule->GetComponent(id1)->Setend_index(pmodule->GetComponent(id)->Getend_index());
							}
							else std::cout<<"ton tai tin hieu cung ten nhung roi rac so voi tin hieu dang xet -> ko gop dc"<<" name = "<<pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMOutputName()<<std::endl;
						}
					}
					tmpNode=tmpNode->nextp();
				}

				//xoa validname cua cac Component duoc dung lam fanin cua CONCAT
				if (found_concat==1)
				{
					pmodule->GetComponent(fanin[0])->Setvalidname(0);
					pmodule->GetComponent(fanin[1])->Setvalidname(0);
				}
			}

			// luu id vao user2p()
			if (found_concat==1) VecId->push_back(id1);
			else VecId->push_back(id);

			//if (nodep->user4()==1) VecId->clear();  // can chu y thao tac nay, co the se xay ra loi
			nodep->user2vi(VecId);
		}
	}
	else {
		nodep->iterateChildren(*this);
		if (nodep->op1p()->user2p()==NULL) {
			cout<<"ERROR: child node at op2p() of AstAssign does not include any Id of Components\n";
			return;
		}

		PslProperty *pmodule = v3Global.property();
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		FckId id;
		std::vector<unsigned int> fanin;
		string fanout = (string)nodep->op2p()->name();
		string str = (string)(nodep->op1p()->typeName());
		unsigned int width = nodep->op2p()->width();
		std::size_t pos;

		if (fanout.find("v__DOT__") != std::string::npos)
		  fanout.erase(0,8);
		pos= fanout.find("___024_");
		if (pos!=std::string::npos) {
			fanout.erase(0,pos);
			fanout.insert(0,"v");
		}
		pos= fanout.find("___024_");
		while (pos!=std::string::npos) {
			fanout.erase(pos,7);
			fanout.insert(pos,"__DOT__");
			pos= fanout.find("___024_");
		}

		// trong truong hop gan lhs = rhs ta se tao bo BUFF (hoac AND voi 2 dau vao cung 1 tin hieu)
		if((str=="VARREF")||(str=="CONST")){
			fanin.clear();
			fanin.push_back(nodep->op1p()->user2vi()->at(0));
			fanin.push_back(nodep->op1p()->user2vi()->at(0));
			id = pmodule->AddComponent(AND,fanin,fanout);
		}
		else
		{
			// tim component co id luu trong op1p
			Component *pComp = pmodule->GetComponent(nodep->op1p()->user2vi()->at(0));
			if(pComp!=NULL) pComp->SetMOutputName(fanout);
			else cout<<"Component with id "<<nodep->op1p()->user2vi()->at(0)<< " not found!\n";
			id = nodep->op1p()->user2vi()->at(0);
		}
		pmodule->GetComponent(id)->Setstart_index(0);
		pmodule->GetComponent(id)->Setend_index(width-1);
		VecId->push_back(id);
		nodep->user2vi(VecId);
	}
}

//bien dich cu phap "assign", phep gan nam trong khoi always co clock
virtual void visit(AstAssignDly* nodep, AstNUser*) {
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule = v3Global.circuit()->GetTopmodule();
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		std::vector<unsigned int>* VecId;
		VecId = new vector<unsigned int>;
		int found=0;
		FckId id(0);

		// Tao PS_INPUT tuong ung voi thanh ghi
		// doi voi cac Comp duoc tao ra tu khoi ALWAYS co clock,
		//chi co Comp PS_INPUT co validname=1, cac Comp khac co validname=0

		//xac dinh ten va cac chi so cua PS_INPUT can tao
		string fanout;
		unsigned int start_index;
		unsigned int width;
		AstNode *pointNode;

		if ((string) nodep->op2p()->typeName()=="ARRAYSEL")
		{
			nodep->iterateChildren(*this);
			FckId id1(0),id_dec;
			std::vector<unsigned int> fanin;
			string str = (string)(nodep->op1p()->typeName());

			if (nodep->op1p()->user2p()==NULL) {
				cout<<"ERROR: child node at op1p() of AstAssignDly does not include any Id of Components\n";
				return;
			}

			// trong truong hop gan lhs = rhs ta se tao bo BUFF (hoac AND voi 2 dau vao cung 1 tin hieu)
			if((str=="VARREF")||(str=="CONST")){
				fanin.clear();
				fanin.push_back(nodep->op1p()->user2vi()->at(0));
				fanin.push_back(nodep->op1p()->user2vi()->at(0));
				id1 = pmodule->AddComponent(AND,fanin,"AND");
			}
			else
			{
				id1 = nodep->op1p()->user2vi()->at(0);
			}
			// gan gia tri thich hop cac o nho cua RAM
			if (test_ram_addr(pmodule->GetComponent(nodep->op2p()->op2p()->user2vi()->at(0)))==0)
			{
				unsigned int array_index = convertbin2dec(converthex2bin(nodep->op2p()->op2p()->name()));
				pmodule->GetComponent(id1)->SetMOutputName(nodep->op2p()->op1p()->name());
				pmodule->GetComponent(id1)->Setarray_index(array_index);
				pmodule->GetComponent(id1)->Setstart_index(0);
				pmodule->GetComponent(id1)->Setend_index(nodep->op2p()->op1p()->width()-1);
				pmodule->GetComponent(id1)->Setvalidname(0);
				VecId->push_back(id1);
			}
			else
			{
				// tao bo decoder de giai ma dia chi va xac dinh o nho can ghi
				FckId id_one, id_const,id_slice,id_ite, id_psi;
				string binary_const;  // dang xau nhi phan cua cac hang so
				fanin.clear();
				fanin.push_back(nodep->op2p()->op2p()->user2vi()->at(0));
				id_dec = pmodule->AddComponent(DECODER,fanin,"DECODER");
				pmodule->GetComponent(id_dec)->Setvalidname(0);

				//xay dung bo giai ma dia chi va ghi ram
				// tao hang so 1
				binary_const = "1";
				fanin.clear();
				id_one = pmodule->AddComponent(C_INPUT,fanin,binary_const);

				//xac dinh PSI ung voi o nho so 0 cua RAm
				id_psi = nodep->op2p()->user2vi()->at(0);

				while (id_psi != 0)
				{
					//tao hang so ung voi so thu tu o nho
					binary_const = convertdec2bin(pmodule->GetComponent(id_psi)->Getarray_index());
					fanin.clear();
					id_const = pmodule->AddComponent(C_INPUT,fanin,binary_const);
					//tao slice de lay tin hieu giai ma dia chi cho o nho do
					fanin.clear();
					fanin.push_back(id_dec);
					fanin.push_back(id_const);
					fanin.push_back(id_one);

					id_slice = pmodule->AddComponent(SLICE,fanin,"SLICE");
					pmodule->GetComponent(id_slice)->Setvalidname(0);
					//tao ITE de xac dinh thay doi trang thai hoac khong
					fanin.clear();
					fanin.push_back(id_slice);
					fanin.push_back(id1);
					fanin.push_back(id_psi);

					id_ite = pmodule->AddComponent(ITE,fanin,nodep->op2p()->op1p()->name());
					pmodule->GetComponent(id_ite)->Setvalidname(0);
					pmodule->GetComponent(id_ite)->Setarray_index(pmodule->GetComponent(id_psi)->Getarray_index());
					pmodule->GetComponent(id_ite)->Setstart_index(pmodule->GetComponent(id_psi)->Getstart_index());
					pmodule->GetComponent(id_ite)->Setend_index(pmodule->GetComponent(id_psi)->Getend_index());

					VecId->push_back(id_ite);
					id_psi = ((PseudoInput*)pmodule->GetComponent(id_psi))->Getpsi_after();
				}
			}
			nodep->user2vi(VecId);
		}
		else
		{
			if ((string) nodep->op2p()->typeName()=="VARREF")
			{
				fanout = (string)nodep->op2p()->name();
				start_index = 0;
				width = nodep->op2p()->width();
			}
			else if ((string) nodep->op2p()->typeName()=="SEL")
			{
				fanout = (string) nodep->op2p()->op1p()->name();
				start_index = convertbin2dec(converthex2bin((string) nodep->op2p()->op2p()->name()));
				width = nodep->op2p()->op1p()->width();
			}

			for (unsigned int i=0;i<pmodule->GetmComponents().size();i++)
			{
				if ((pmodule->GetmComponents()[i]->GetMOutputName()==fanout)
						&& (pmodule->GetmComponents()[i]->GetMValid()==1)
						&& (pmodule->GetmComponents()[i]->Getarray_index()==-1)
						&& (pmodule->GetmComponents()[i]->Getvalidname()==1)
						&& (pmodule->GetmComponents()[i]->Getstart_index()==0)
						&& (pmodule->GetmComponents()[i]->GetMFanoutWidth()==width)
						)
				{
					found=1; // da tim thay
					if (pmodule->GetmComponents()[i]->GetMType()==UNDEF)
					{
						std::vector<FckId> fanin;
						fanin.push_back(width);
						id = pmodule->AddComponent(PS_INPUT,fanin,fanout);
						pmodule->GetComponent(id)->Setstart_index(0);
						pmodule->GetComponent(id)->Setend_index(width-1);
						// thay UNDEF trong cac ket noi bang Component PS_INPUT vua tao
						for (unsigned int j= 0; j< pmodule->GetmComponents()[i]->GetFanout().size(); j++)
						{
							//gan cac vector mFanout cua UNDEF cho component PS_INPUT vua tao
							pmodule->GetComponent(id)->AddFanout(pmodule->GetmComponents()[i]->GetFanout().at(j));
							//voi moi fanout do, tim trong fanin cua no vi tri chua UNDEF vao thay bang ADD
							pmodule->GetmComponents()[i]->GetFanout().at(j)->ChangeFanin(pmodule->GetmComponents()[i],pmodule->GetComponent(id));
						}
						//Xoa Comp UNDEF
						pmodule->GetmComponents()[i]->SetMValid(0);
					}
					else
					{
						if (pmodule->GetmComponents()[i]->GetMType()==PS_INPUT)
							id = pmodule->GetmComponents()[i]->GetMId();
						else
						{
							std::cout<<"da co loi xay ra trong tao mach"<<std::endl;
							std::cout<<" TYPE = "<<pmodule->GetmComponents()[i]->GetMType()<<std::endl;
						}
					}
					break;
				}
			}
			if (found==0)
			{
				std::vector<FckId> fanin;
				fanin.push_back(width);
				id = pmodule->AddComponent(PS_INPUT,fanin,fanout);
				pmodule->GetComponent(id)->Setstart_index(0);
				pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->GetMFanoutWidth()-1);
			}

			// Sau khi tao PS_INPUT tuong ung, cac thao tac voi ham visit nay giong voi cac loai Assign khac

			//std::vector<unsigned int>* VecId;
			//VecId = new vector<unsigned int>;
			nodep->iterateChildren(*this);
			if (nodep->op1p()->user2p()==NULL) {
				cout<<"ERROR: child node at op1p() of AstAssignDly does not include any Id of Components\n";
				return;
			}

			FckId id1(0);
			std::vector<unsigned int> fanin;
			//string fanout = (string) nodep->op2p()->name();
			string str = (string)(nodep->op1p()->typeName());

			// trong truong hop gan lhs = rhs ta se tao bo BUFF (hoac AND voi 2 dau vao cung 1 tin hieu)
			if((str=="VARREF")||(str=="CONST")){
				fanin.push_back(nodep->op1p()->user2vi()->at(0));
				fanin.push_back(nodep->op1p()->user2vi()->at(0));
				id1 = pmodule->AddComponent(AND,fanin,fanout);
			}
			else
			{
				// tim component co id luu trong op1p
				Component *pComp = pmodule->GetComponent(nodep->op1p()->user2vi()->at(0));
				if(pComp!=NULL) pComp->SetMOutputName(fanout);
				else cout<<"Component with id "<<nodep->op1p()->user2vi()->at(0)<< " not found!\n";
				id1 = nodep->op1p()->user2vi()->at(0);
			}

			pmodule->GetComponent(id1)->Setstart_index(start_index);
			pmodule->GetComponent(id1)->Setend_index(pmodule->GetComponent(id1)->Getstart_index() + pmodule->GetComponent(id1)->GetMFanoutWidth()-1);

			//chi co Comp UNDEF hoac PS_INPUT moi co validname=1
			pmodule->GetComponent(id1)->Setvalidname(0);


			if ((string) nodep->op2p()->typeName()=="SEL")
			{
				//xoa SLICE khong dung den
				v3Global.circuit()->deletecomp(pmodule->GetComponent(nodep->op2p()->user2vi()->at(0)));
			}

			// xoa cac component ko dung hoac chuyen len ignore=1 cho no de dam bao co duy nhat 1 ignore=0 tai 1 khoi begin...end
			if ((string) nodep->user1p()->castNode()->typeName()=="ALWAYS")
				pointNode = nodep->user1p()->castNode()->op2p();
			else if ((string) nodep->user1p()->castNode()->typeName()=="IF")
				{
					if (nodep->user3()==2)
						pointNode = nodep->user1p()->castNode()->op2p();
					else pointNode = nodep->user1p()->castNode()->op3p();
				}


			found = 0;
			while ((pointNode!=NULL)&&(found==0))
			{
				if (pointNode->user2p()==NULL) break;
				else
				{
					for (int i=0; i<pointNode->user2vi()->size(); i++)
					{
						if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMOutputName()==pmodule->GetComponent(id1)->GetMOutputName())
								&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(id1)->Getstart_index())
								&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMFanoutWidth()==pmodule->GetComponent(id1)->GetMFanoutWidth())
								&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMValid()==1)
								&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getarray_index()==pmodule->GetComponent(id1)->Getarray_index())
								&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getignore()==0)
								&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getvalidname()==0)
								&& (pointNode->user2vi()->at(i)!=id1))
						{
							v3Global.circuit()->deletecomp(pmodule->GetComponent(pointNode->user2vi()->at(i)));
							pmodule->GetComponent(pointNode->user2vi()->at(i))->Setignore(1);
							found = 1;
						}
					}
					pointNode = pointNode->nextp();
				}
			}


			// gop tin hieu neu can thiet
			FckId id2;
			int found_concat = 0;
			//std::vector<FckId> fanin;

			if (((string)nodep->user1p()->castNode()->typeName()=="IF")||((string)nodep->user1p()->castNode()->typeName()=="ALWAYS"))
			{
				AstNode* tmpNode;
				found_concat = 0;
				if (nodep->user3()==2)
					tmpNode = nodep->user1p()->castNode()->op2p();
				else if (nodep->user3()==3)
					tmpNode = nodep->user1p()->castNode()->op3p();
				else std::cout<<" AstGraph co loi xay ra"<<std::endl;

				while ((tmpNode!=nodep)&& (found_concat==0))
				{
					for (int i=0; i<tmpNode->user2vi()->size(); i++)
					{
						if ((pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMOutputName()== pmodule->GetComponent(id1)->GetMOutputName())
								&& (pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMValid()==1)
								&& (pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getarray_index()==pmodule->GetComponent(id1)->Getarray_index())
								&& (pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getignore()==0)
								)
						{
							if (pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getstart_index() == pmodule->GetComponent(id1)->Getend_index()+1)
							{
								found_concat = 1; //da tim thay Component can gop
								fanout = pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMOutputName();
								fanin.clear();
								fanin.push_back(tmpNode->user2vi()->at(i));
								fanin.push_back(id1);
								id2 = pmodule->AddComponent(CONCAT,fanin,fanout);
								pmodule->GetComponent(id2)->Setstart_index(pmodule->GetComponent(id1)->Getstart_index());
								pmodule->GetComponent(id2)->Setend_index(pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getend_index());
								pmodule->GetComponent(id2)->Setvalidname(0);
								pmodule->GetComponent(tmpNode->user2vi()->at(i))->Setignore(1);
								pmodule->GetComponent(id1)->Setignore(1);
							}
							else if (pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getend_index() == pmodule->GetComponent(id1)->Getstart_index()-1)
							{
								found_concat = 1; //da tim thay Component can gop
								fanout = pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMOutputName();
								fanin.clear();
								fanin.push_back(id1);
								fanin.push_back(tmpNode->user2vi()->at(i));
								id2 = pmodule->AddComponent(CONCAT,fanin,fanout);
								pmodule->GetComponent(id2)->Setstart_index(pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getstart_index());
								pmodule->GetComponent(id2)->Setend_index(pmodule->GetComponent(id1)->Getend_index());
								pmodule->GetComponent(id2)->Setvalidname(0);
								pmodule->GetComponent(tmpNode->user2vi()->at(i))->Setignore(1);
								pmodule->GetComponent(id1)->Setignore(1);
							}
							else
							{
								std::cout<<"ton tai tin hieu cung ten nhung roi rac so voi tin hieu dang xet -> ko gop dc"<<" name = "<<pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMOutputName()<<std::endl;
								std::cout<<pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getend_index()<<"  "<<pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getstart_index()<<std::endl;
								std::cout<<pmodule->GetComponent(id1)->Getend_index()<<"  "<<pmodule->GetComponent(id1)->Getstart_index()<<std::endl;
							}
						}
					}
					tmpNode=tmpNode->nextp();
				}
			}

			if (found_concat==1)	VecId->push_back(id2);
			else 	VecId->push_back(id1);
			nodep->user2vi(VecId);
		}
	}
}


//Son Lam dang xu ly AstIf 22/6

virtual void visit(AstIf* nodep, AstNUser*) {
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	if (v3Global.get_compiler()==0) {
		Module* pmodule = v3Global.circuit()->GetTopmodule();
		nodep->iterateChildren(*this);
		if ((nodep->user4()<3) || (v3Global.get_compiler()))
		{
			std::vector<unsigned int>* VecId;
			VecId = new vector<unsigned int>;
			std::vector<FckId> fanin;
			string fanout;
			int found=0; // bao da tim thay

			if (nodep->op1p()->user2p()==NULL) {
				cout<<"ERROR: child node at op1p() of AstAssignDly does not include any Id of Components\n";
				return;
			}
			AstNode *tmp = nodep->op2p();
			while (tmp!=NULL) {
				if (tmp->user2p()==NULL) {
					cout<<"ERROR: child node at op2p() of AstIf does not include any Id of Components\n";
					return;
				}
				tmp = tmp->nextp();
			}
			tmp = nodep->op3p();
			while (tmp!=NULL) {
				if (tmp->user2p()==NULL) {
					cout<<"ERROR: child node at op3p() of AstIf does not include any Id of Components\n";
					return;
				}
				tmp = tmp->nextp();
			}

			if (nodep->user4()==1)
			{
				// Duyet op2p de tao cac Comp ITE ung voi cac tin hieu o nhanh IF
				if (nodep->op2p())
				{
					AstNode* tmpNode;
					tmpNode=nodep->op2p();  //bat dau xet tu node con cua If cho den het danh sach
					while (tmpNode!= NULL)
					{	//tim component lam fanin cho bo mux
						if (tmpNode->user2p()!=NULL)
						{
							for (int x=0; x<tmpNode->user2vi()->size();x++)
							{
								//nhung comp co ten hop le se la fanin can tim
								if ((pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMValid()==1)
										&& (pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getignore()==0))
								{	//neu tim thay co bo mux
									FckId id;
									AstNode *pointNode, *parentNode;  //Node dung de duyet cac node trong 1 khoi begin...end
									int counter=0; //bo den so vong lap

									parentNode = tmpNode;
									found=0;
									fanout = pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName();
									fanin.clear(); //tao vector fanin va push cond va fanin 1 vao
									fanin.push_back(nodep->op1p()->user2vi()->at(0));
									fanin.push_back(tmpNode->user2vi()->at(x));

									while (((string) parentNode->typeName()!="ALWAYS") && (found==0))
									{	//tim component lam fanin thu 3 cho bo mux dua theo so do thuat toan trong bao cao
										if (counter==0) pointNode = parentNode->user1p()->castNode()->op3p();
										else if ((string)parentNode->user1p()->castNode()->typeName()=="ALWAYS")
												pointNode = parentNode->user1p()->castNode()->op2p();
											else if ((string)parentNode->user1p()->castNode()->typeName()=="IF")
												{
													if (parentNode->user3()==2)
														pointNode = parentNode->user1p()->castNode()->op2p();
													else if (parentNode->user3()==3)
														pointNode = parentNode->user1p()->castNode()->op3p();
														else std::cout<<"da co loi xay ra voi IF va cac op() cua no"<<std::endl;
												}
												else std::cout<<"da co loi xay ra voi node IF va ALWAYS cua AstGraph "<<std::endl;
										// Duyet khoi begin...end da xac dinh
										while ((pointNode!=NULL)&&(found==0))
										{
											if (pointNode->user2p()==NULL) break;
											else
											{
												for (int i=0; i<pointNode->user2vi()->size(); i++)
												{
													if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMOutputName()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName())
															//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index())
															//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMFanoutWidth()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth())
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMValid()==1)
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getarray_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index())
															//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getvalidname()==0)
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getignore()==0))
													{
														if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index()) &&
																(pmodule->GetComponent(pointNode->user2vi()->at(i))->Getend_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getend_index()))
														{
															fanin.push_back(pointNode->user2vi()->at(i));
															id = pmodule->AddComponent(ITE,fanin,fanout);
															pmodule->GetComponent(id)->Setvalidname(0);
															pmodule->GetComponent(id)->Setarray_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index());
															pmodule->GetComponent(id)->Setstart_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index());
															pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
															VecId->push_back(id);
															found=1;
															break;
														}
														else if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()<=pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index()) &&
																(pmodule->GetComponent(pointNode->user2vi()->at(i))->Getend_index()>=pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getend_index()))
															{
																//neu pham vi chi so rong hon can thiet ta dung Slice de cat lay phan can thiet cua tin hieu
																FckId id_sel, id_width, id_start;
																string width_binary, start_binary;
																width_binary = convertdec2bin(pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth());
																start_binary = convertdec2bin(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index() - pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index());
																std::vector<FckId> fanin_sel;
																fanin_sel.clear();
																id_start = pmodule->AddComponent(C_INPUT,fanin_sel,start_binary);
																id_width = pmodule->AddComponent(C_INPUT,fanin_sel,width_binary);
																fanin_sel.clear();
																fanin_sel.push_back(pointNode->user2vi()->at(i));
																fanin_sel.push_back(id_start);
																fanin_sel.push_back(id_width);
																id_sel = pmodule->AddComponent(SLICE,fanin_sel,"SLICE");
																// tao ITE
																fanin.push_back(id_sel);
																id = pmodule->AddComponent(ITE,fanin,fanout);
																pmodule->GetComponent(id)->Setvalidname(0);
																pmodule->GetComponent(id)->Setarray_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index());
																pmodule->GetComponent(id)->Setstart_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index());
																pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
																VecId->push_back(id);
																found=1;
																break;
															}
													}
												}
												pointNode = pointNode->nextp();
											}
										}
										parentNode = parentNode->user1p()->castNode();
										counter++;
									}

									if (found==0)
									{
										if (pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMType()==PS_OUTPUT)
										{
											VecId->push_back(tmpNode->user2vi()->at(x));
										}
										else
										{
											for (int j=pmodule->GetmComponents().size()-1; j>=0 ; j--)
											{
												if ((pmodule->GetmComponents().at(j)->GetMOutputName()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName())
														//&& (pmodule->GetmComponents().at(j)->GetMFanoutWidth()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth())
														//&& (pmodule->GetmComponents().at(j)->Getstart_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index())
														&& (pmodule->GetmComponents().at(j)->GetMValid())
														&& (pmodule->GetmComponents().at(j)->Getarray_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index())
														&& (pmodule->GetmComponents().at(j)->GetMType()==PS_INPUT)
														)
												{
													found = 1;
													if ((pmodule->GetmComponents().at(j)->GetMFanoutWidth()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth())
														&& (pmodule->GetmComponents().at(j)->Getstart_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index()))
													{
														fanin.push_back(pmodule->GetmComponents().at(j)->GetMId());
														id = pmodule->AddComponent(ITE,fanin,fanout);
														pmodule->GetComponent(id)->Setvalidname(0);
														pmodule->GetComponent(id)->Setarray_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index());
														pmodule->GetComponent(id)->Setstart_index(pmodule->GetmComponents().at(j)->Getstart_index());
														pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
														VecId->push_back(id);
													}
													else if ((pmodule->GetmComponents().at(j)->GetMFanoutWidth() > pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth()))
														{
															FckId id1,id2,id3;
															std::vector<FckId> fan_in;
															fan_in.clear();
															int start = pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index();
															int width = pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth();
															id1 = pmodule->AddComponent(C_INPUT,fan_in,convertdec2bin(start));
															id2 = pmodule->AddComponent(C_INPUT,fan_in,convertdec2bin(width));
															fan_in.push_back(pmodule->GetmComponents().at(j)->GetMId());
															fan_in.push_back(id1);
															fan_in.push_back(id2);
															id3 = pmodule->AddComponent(SLICE,fan_in,pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName());
															pmodule->GetComponent(id3)->Setvalidname(0);

															fanin.push_back(id3);
															id = pmodule->AddComponent(ITE,fanin,fanout);
															pmodule->GetComponent(id)->Setvalidname(0);
															pmodule->GetComponent(id)->Setarray_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index());
															pmodule->GetComponent(id)->Setstart_index(start);
															pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
															VecId->push_back(id);
														}
													break;

												}
											}
										}
									}

									if (found==1)
									{
										// xoa cac component ko dung hoac xoa ten cua no de dam bao co duy nhat 1 ten hop le tai 1 khoi begin...end
										if ((string) nodep->user1p()->castNode()->typeName()=="ALWAYS")
											pointNode = nodep->user1p()->castNode()->op2p();
										else if ((string) nodep->user1p()->castNode()->typeName()=="IF")
											{
												if (nodep->user3()==2)
													pointNode = nodep->user1p()->castNode()->op2p();
												else pointNode = nodep->user1p()->castNode()->op3p();
											}

										found = 0;

										while ((pointNode!=NULL)&&(found==0))
										{
											if (pointNode->user2p()==NULL) break;
											else
											{
												for (int i=0; i<pointNode->user2vi()->size(); i++)
												{
													if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMOutputName()==pmodule->GetComponent(id)->GetMOutputName())
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(id)->Getstart_index())
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMFanoutWidth()==pmodule->GetComponent(id)->GetMFanoutWidth())
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMValid()==1)
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getarray_index()==pmodule->GetComponent(id)->Getarray_index())
															//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getvalidname()==0)
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getignore()==0)
															&& (pointNode->user2vi()->at(i)!=id))
													{
														if (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetFanout().size()==0)
															v3Global.circuit()->deletecomp(pmodule->GetComponent(pointNode->user2vi()->at(i)));
														pmodule->GetComponent(pointNode->user2vi()->at(i))->Setignore(1);
														found = 1;
													}
												}
												pointNode = pointNode->nextp();
											}
										}
									}
									//else std::cout<<"Khong tim thay Component phu hop de tao bo MUX 1"<<std::endl;

								}
							}
						}
						tmpNode=tmpNode->nextp();
					}
				}

				// Duyet op3p de tao cac Comp ITE ung voi cac tin hieu o nhanh ELSE
				if (nodep->op3p())
				{
					AstNode* tmpNode;
					tmpNode=nodep->op3p();  //bat dau xet tu node con cua If cho den het danh sach
					while (tmpNode!= NULL)
					{	//tim component lam fanin cho bo mux
						for (int x=0; x<tmpNode->user2vi()->size();x++)
							//nhung comp co ten hop le se la fanin can tim
							if ((pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMValid()==1)
									&& (pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getignore()==0))
							{	//neu tim thay co bo mux
								FckId id;
								AstNode *pointNode;  //Node dung de duyet cac node trong 1 khoi begin...end
								AstNode *parentNode;  //Node dung de duyet cac node trong 1 khoi begin...end
								int counter=0; //bo den so vong lap

								parentNode = tmpNode;

								found=0;
								fanout = pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName();
								fanin.clear(); //tao vector fanin va push cond va fanin 1 vao
								fanin.push_back(nodep->op1p()->user2vi()->at(0));
								//fanin.push_back(tmpNode->user2vi()->at(x));

								while (((string) parentNode->typeName()!="ALWAYS") && (found==0))
								{	//tim component lam fanin thu 3 cho bo mux dua theo so do thuat toan trong bao cao
									if (counter==0) pointNode = parentNode->user1p()->castNode()->op2p();
									else if ((string)parentNode->user1p()->castNode()->typeName()=="ALWAYS")
											pointNode = parentNode->user1p()->castNode()->op2p();
										else if ((string)parentNode->user1p()->castNode()->typeName()=="IF")
											{
												if (parentNode->user3()==2)
													pointNode = parentNode->user1p()->castNode()->op2p();
												else if (parentNode->user3()==3)
													pointNode = parentNode->user1p()->castNode()->op3p();
													else std::cout<<"da co loi xay ra voi IF va cac op() cua no"<<std::endl;
											}
											else std::cout<<"da co loi xay ra voi node IF va ALWAYS cua AstGraph "<<std::endl;
									// Duyet khoi begin...end da xac dinh
									while ((pointNode!=NULL)&&(found==0))
									{
										if (pointNode->user2p()==NULL) break;
										else
										{
											for (int i=0; i<pointNode->user2vi()->size(); i++)
											{
												if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMOutputName()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName())
														//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index())
														//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMFanoutWidth()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth())
														&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMValid()==1)
														&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getarray_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index())
														//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getvalidname()==0)
														&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getignore()==0))
												{
													if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index()) &&
															(pmodule->GetComponent(pointNode->user2vi()->at(i))->Getend_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getend_index()))
													{
														found=1;
														if (counter!= 0)
														{
															fanin.push_back(pointNode->user2vi()->at(i));
															fanin.push_back(tmpNode->user2vi()->at(x));
															id = pmodule->AddComponent(ITE,fanin,fanout);
															pmodule->GetComponent(id)->Setvalidname(0);
															//std::cout<<pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index()<<endl;
															pmodule->GetComponent(id)->Setarray_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index());
															pmodule->GetComponent(id)->Setstart_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index());
															pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
															VecId->push_back(id);
															break;
														}
														break;
													}
													else if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()<=pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index()) &&
															(pmodule->GetComponent(pointNode->user2vi()->at(i))->Getend_index()>=pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getend_index()))
														{
															found=1;
															//neu pham vi chi so rong hon can thiet ta dung Slice de cat lay phan can thiet cua tin hieu
															FckId id_sel, id_width, id_start;
															string width_binary, start_binary;
															width_binary = convertdec2bin(pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth());
															start_binary = convertdec2bin(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index() - pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index());
															std::vector<FckId> fanin_sel;
															fanin_sel.clear();
															id_start = pmodule->AddComponent(C_INPUT,fanin_sel,start_binary);
															id_width = pmodule->AddComponent(C_INPUT,fanin_sel,width_binary);
															fanin_sel.clear();
															fanin_sel.push_back(pointNode->user2vi()->at(i));
															fanin_sel.push_back(id_start);
															fanin_sel.push_back(id_width);
															id_sel = pmodule->AddComponent(SLICE,fanin_sel,"SLICE");
															//Tao ITE
															if (counter != 0)
															{
																fanin.push_back(id_sel);
																fanin.push_back(tmpNode->user2vi()->at(x));
																id = pmodule->AddComponent(ITE,fanin,fanout);
																pmodule->GetComponent(id)->Setvalidname(0);
																//std::cout<<pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index()<<endl;
																pmodule->GetComponent(id)->Setarray_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index());
																pmodule->GetComponent(id)->Setstart_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index());
																pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
																VecId->push_back(id);
																break;
															}
															else
															{
																std::cout<<"khong ho tro truong hop nay\n";
																break;
															}
														}
												}
											}
											pointNode = pointNode->nextp();
										}
									}
									parentNode = parentNode->user1p()->castNode();
									counter++;
								}

								if (found==0)
								{
									for (int j=pmodule->GetmComponents().size()-1; j>=0 ; j--)
									{
										if ((pmodule->GetmComponents().at(j)->GetMOutputName()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName())
												//&& (pmodule->GetmComponents().at(j)->GetMFanoutWidth()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth())
												//&& (pmodule->GetmComponents().at(j)->Getstart_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index())
												&& (pmodule->GetmComponents().at(j)->GetMValid())
												&& (pmodule->GetmComponents().at(j)->Getarray_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index())
												&& (pmodule->GetmComponents().at(j)->GetMType()==PS_INPUT)
												)
										{
											found = 1;
											if ((pmodule->GetmComponents().at(j)->GetMFanoutWidth()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth())
												&& (pmodule->GetmComponents().at(j)->Getstart_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index()))
											{
												fanin.push_back(pmodule->GetmComponents().at(j)->GetMId());
												fanin.push_back(tmpNode->user2vi()->at(x));
												id = pmodule->AddComponent(ITE,fanin,fanout);
												pmodule->GetComponent(id)->Setvalidname(0);
												pmodule->GetComponent(id)->Setarray_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index());
												pmodule->GetComponent(id)->Setstart_index(pmodule->GetmComponents().at(j)->Getstart_index());
												pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
												VecId->push_back(id);
											}
											else if ((pmodule->GetmComponents().at(j)->GetMFanoutWidth() > pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth()))
												{
													FckId id1,id2,id3;
													std::vector<FckId> fan_in;
													fan_in.clear();
													int start = pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index();
													int width = pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth();
													id1 = pmodule->AddComponent(C_INPUT,fan_in,convertdec2bin(start));
													id2 = pmodule->AddComponent(C_INPUT,fan_in,convertdec2bin(width));
													fan_in.push_back(pmodule->GetmComponents().at(j)->GetMId());
													fan_in.push_back(id1);
													fan_in.push_back(id2);
													id3 = pmodule->AddComponent(SLICE,fan_in,pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName());
													pmodule->GetComponent(id3)->Setvalidname(0);

													fanin.push_back(id3);
													fanin.push_back(tmpNode->user2vi()->at(x));
													id = pmodule->AddComponent(ITE,fanin,fanout);
													pmodule->GetComponent(id)->Setvalidname(0);
													pmodule->GetComponent(id)->Setarray_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index());
													pmodule->GetComponent(id)->Setstart_index(start);
													pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
													VecId->push_back(id);
												}
											break;

										}
									}
								}

								if (found==1)
								{
									if (counter>1)
									{
										// xoa cac component ko dung hoac xoa ten cua no de dam bao co duy nhat 1 ten hop le tai 1 khoi begin...end
										if ((string) nodep->user1p()->castNode()->typeName()=="ALWAYS")
											pointNode = nodep->user1p()->castNode()->op2p();
										else if ((string) nodep->user1p()->castNode()->typeName()=="IF")
											{
												if (nodep->user3()==2)
													pointNode = nodep->user1p()->castNode()->op2p();
												else pointNode = nodep->user1p()->castNode()->op3p();
											}

										found = 0;

										while ((pointNode!=NULL)&&(found==0))
										{
											if (pointNode->user2p()==NULL) break;
											else
											{
												for (int i=0; i<pointNode->user2vi()->size(); i++)
												{
													if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMOutputName()==pmodule->GetComponent(id)->GetMOutputName())
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(id)->Getstart_index())
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMFanoutWidth()==pmodule->GetComponent(id)->GetMFanoutWidth())
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMValid()==1)
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getarray_index()==pmodule->GetComponent(id)->Getarray_index())
															//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getvalidname()==0)
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getignore()==0)
															&& (pointNode->user2vi()->at(i)!=id))
													{
														if (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetFanout().size()==0)
															v3Global.circuit()->deletecomp(pmodule->GetComponent(pointNode->user2vi()->at(i)));
														pmodule->GetComponent(pointNode->user2vi()->at(i))->Setignore(1);
														found = 1;
													}
												}
												pointNode = pointNode->nextp();
											}
										}
									}
								}
								else std::cout<<"Khong tim thay Component phu hop de tao bo MUX 1"<<std::endl;
							}
						tmpNode=tmpNode->nextp();
					}
				}
			}
			else
			{
				// Duyet op2p de tao cac Comp ITE ung voi cac tin hieu o nhanh IF
				if (nodep->op2p())
				{
					AstNode* tmpNode;
					tmpNode=nodep->op2p();  //bat dau xet tu node con cua If cho den het danh sach
					while (tmpNode!= NULL)
					{	//tim component lam fanin cho bo mux
						for (int x=0; x<tmpNode->user2vi()->size();x++)
							//nhung comp co ten hop le se la fanin can tim
							if ((pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getvalidname() ==1) &&
									(pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMValid()==1))
							{

								//neu tim thay co bo mux
								FckId id;
								AstNode *parentNode;  //Con tro toi node cha
								AstNode *pointNode;  //Node dung de duyet cac node trong 1 khoi begin...end
								int counter=0; //bo den so vong lap

								found=0;
								parentNode = tmpNode;
								fanout = pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName();
								fanin.clear(); //tao vector fanin va push cond va fanin 1 vao
								fanin.push_back(nodep->op1p()->user2vi()->at(0));
								fanin.push_back(tmpNode->user2vi()->at(x));

								while (((string) parentNode->typeName()!="ALWAYS") && (found==0))
								{	//tim component lam fanin thu 3 cho bo mux dua theo so do thuat toan trong bao cao
									if (counter==0) pointNode = parentNode->user1p()->castNode()->op3p();
									else if ((string)parentNode->user1p()->castNode()->typeName()=="ALWAYS")
											pointNode = parentNode->user1p()->castNode()->op2p();
										else if ((string)parentNode->user1p()->castNode()->typeName()=="IF")
											{
												if (parentNode->user3()==2)
													pointNode = parentNode->user1p()->castNode()->op2p();
												else if (parentNode->user3()==3)
													pointNode = parentNode->user1p()->castNode()->op3p();
													else std::cout<<"da co loi xay ra voi IF va cac op() cua no"<<std::endl;
											}
											else std::cout<<"da co loi xay ra voi node IF va ALWAYS cua AstGraph "<<std::endl;
									// Duyet khoi begin...end da xac dinh
									while ((pointNode!=NULL)&&(found==0))
									{
										if (pointNode->user2p()==NULL) break;
										else
										{
											for (int i=0; i<pointNode->user2vi()->size(); i++)
											{
												if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMOutputName()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName())
														//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index())
														//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMFanoutWidth()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth())
														&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMValid()==1)
														&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getarray_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index())
														&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getvalidname()==1))
												{
													if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index()) &&
															(pmodule->GetComponent(pointNode->user2vi()->at(i))->Getend_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getend_index()))
													{
														fanin.push_back(pointNode->user2vi()->at(i));
														id = pmodule->AddComponent(ITE,fanin,fanout);
														pmodule->GetComponent(id)->Setarray_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index());
														pmodule->GetComponent(id)->Setstart_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index());
														pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
														VecId->push_back(id);
														found=1;
														break;
													}
													else if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()<=pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index()) &&
															(pmodule->GetComponent(pointNode->user2vi()->at(i))->Getend_index()>=pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getend_index()))
													{
														//neu pham vi chi so rong hon can thiet ta dung Slice de cat lay phan can thiet cua tin hieu
														FckId id_sel, id_width, id_start;
														string width_binary, start_binary;
														width_binary = convertdec2bin(pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth());
														start_binary = convertdec2bin(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index() - pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index());
														std::vector<FckId> fanin_sel;
														fanin_sel.clear();
														id_start = pmodule->AddComponent(C_INPUT,fanin_sel,start_binary);
														id_width = pmodule->AddComponent(C_INPUT,fanin_sel,width_binary);
														fanin_sel.clear();
														fanin_sel.push_back(pointNode->user2vi()->at(i));
														fanin_sel.push_back(id_start);
														fanin_sel.push_back(id_width);
														id_sel = pmodule->AddComponent(SLICE,fanin_sel,"SLICE");
														//tao ITE
														fanin.push_back(id_sel);
														id = pmodule->AddComponent(ITE,fanin,fanout);
														pmodule->GetComponent(id)->Setarray_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index());
														pmodule->GetComponent(id)->Setstart_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index());
														pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
														VecId->push_back(id);
														found=1;
														break;
													}
												}
											}
											pointNode = pointNode->nextp();
										}
									}
									parentNode = parentNode->user1p()->castNode();
									counter++;
								}

								if (found==1)
								{
									// xoa cac component ko dung hoac xoa ten cua no de dam bao co duy nhat 1 ten hop le tai 1 khoi begin...end
									if ((string) nodep->user1p()->castNode()->typeName()=="ALWAYS")
										pointNode = nodep->user1p()->castNode()->op2p();
									else if ((string) nodep->user1p()->castNode()->typeName()=="IF")
										{
											if (nodep->user3()==2)
												pointNode = nodep->user1p()->castNode()->op2p();
											else pointNode = nodep->user1p()->castNode()->op3p();
										}

									found = 0;

									while ((pointNode!=NULL)&&(found==0))
									{
										if (pointNode->user2p()==NULL) break;
										else
										{
											for (int i=0; i<pointNode->user2vi()->size(); i++)
											{
												if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMOutputName()==pmodule->GetComponent(id)->GetMOutputName())
														&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(id)->Getstart_index())
														&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMFanoutWidth()==pmodule->GetComponent(id)->GetMFanoutWidth())
														&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMValid()==1)
														&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getarray_index()==pmodule->GetComponent(id)->Getarray_index())
														&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getvalidname()==1)
														&& (pointNode->user2vi()->at(i)!=id))
												{
													if (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetFanout().size()==0)
														v3Global.circuit()->deletecomp(pmodule->GetComponent(pointNode->user2vi()->at(i)));
													pmodule->GetComponent(pointNode->user2vi()->at(i))->Setvalidname(0);
													found = 1;
												}
											}
											pointNode = pointNode->nextp();
										}
									}
								}
								else std::cout<<"Khong tim thay Component phu hop de tao bo MUX : "<<pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName()<<std::endl;
							}
						tmpNode=tmpNode->nextp();
					}
				}


				// duyet op3p de tao cac Comp ITE ung voi nhanh ELSE, neu Comp da tao o nhanh IF se khong duoc tao lai
				if (nodep->op3p())
				{
					AstNode* tmpNode;
					tmpNode=nodep->op3p();  //bat dau xet tu node con cua If cho den het danh sach
					while (tmpNode!= NULL)
					{	//tim component lam fanin cho bo mux
						for (int x=0; x<tmpNode->user2vi()->size();x++)
							//nhung comp co ten hop le se la fanin can tim
							if ((pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getvalidname() ==1) &&
									(pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMValid()==1))
							{

								//neu tim thay co bo mux
								FckId id;
								AstNode *parentNode;  //Con tro toi node cha
								AstNode *pointNode;  //Node dung de duyet cac node trong 1 khoi begin...end
								int counter=0; //bo den so vong lap

								found=0;
								parentNode = tmpNode;
								fanout = pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName();
								fanin.clear(); //tao vector fanin va push cond va fanin 1 vao
								fanin.push_back(nodep->op1p()->user2vi()->at(0));
								// ta se tim fanin 2 la fanin con thieu

								while (((string) parentNode->typeName()!="ALWAYS") && (found==0))
								{	//tim component lam fanin thu 2 cho bo mux dua theo so do thuat toan trong bao cao
									if (counter==0) pointNode = parentNode->user1p()->castNode()->op2p();
									else if ((string)parentNode->user1p()->castNode()->typeName()=="ALWAYS")
											pointNode = parentNode->user1p()->castNode()->op2p();
										else if ((string)parentNode->user1p()->castNode()->typeName()=="IF")
											{
												if (parentNode->user3()==2)
													pointNode = parentNode->user1p()->castNode()->op2p();
												else if (parentNode->user3()==3)
													pointNode = parentNode->user1p()->castNode()->op3p();
													else std::cout<<"da co loi xay ra voi IF va cac op() cua no"<<std::endl;
											}
											else std::cout<<"da co loi xay ra voi node IF va ALWAYS cua AstGraph "<<std::endl;
									// Duyet khoi begin...end da xac dinh
									while ((pointNode!=NULL)&&(found==0))
									{
										if (pointNode->user2p()==NULL) break;
										else
										{
											for (int i=0; i<pointNode->user2vi()->size(); i++)
											{
												if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMOutputName()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName())
														//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index())
														//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMFanoutWidth()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth())
														&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMValid()==1)
														&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getarray_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index())
														&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getvalidname()==1))
												{
													if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index()) &&
															(pmodule->GetComponent(pointNode->user2vi()->at(i))->Getend_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getend_index()))
													{
														if (counter!=0) // neu da xet va tao ITE o op2p thi ko tao lai nua
														{
															fanin.push_back(pointNode->user2vi()->at(i));
															fanin.push_back(tmpNode->user2vi()->at(x));
															id = pmodule->AddComponent(ITE,fanin,fanout);
															pmodule->GetComponent(id)->Setarray_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index());
															pmodule->GetComponent(id)->Setstart_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index());
															pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
															VecId->push_back(id);
														}
														found=1;
														break;
													}
													else if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()<=pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index()) &&
															(pmodule->GetComponent(pointNode->user2vi()->at(i))->Getend_index()>=pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getend_index()))
														{
															//neu pham vi chi so rong hon can thiet ta dung Slice de cat lay phan can thiet cua tin hieu
															FckId id_sel, id_width, id_start;
															string width_binary, start_binary;
															width_binary = convertdec2bin(pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth());
															start_binary = convertdec2bin(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index() - pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index());
															std::vector<FckId> fanin_sel;
															fanin_sel.clear();
															id_start = pmodule->AddComponent(C_INPUT,fanin_sel,start_binary);
															id_width = pmodule->AddComponent(C_INPUT,fanin_sel,width_binary);
															fanin_sel.clear();
															fanin_sel.push_back(pointNode->user2vi()->at(i));
															fanin_sel.push_back(id_start);
															fanin_sel.push_back(id_width);
															id_sel = pmodule->AddComponent(SLICE,fanin_sel,"SLICE");
															//tao ITE
															if (counter!=0) // neu da xet va tao ITE o op2p thi ko tao lai nua
															{
																fanin.push_back(id_sel);
																fanin.push_back(tmpNode->user2vi()->at(x));
																id = pmodule->AddComponent(ITE,fanin,fanout);
																pmodule->GetComponent(id)->Setarray_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index());
																pmodule->GetComponent(id)->Setstart_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index());
																pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
																VecId->push_back(id);
															}
															else std::cout<<"Error: khong ho tro truong hop nay \n";
															found=1;
															break;
														}
												}
											}
											pointNode = pointNode->nextp();
										}
									}
									parentNode = parentNode->user1p()->castNode();
									counter++;
								}

								if (found==1)
								{
									if (counter>1)
									{
										// xoa cac component ko dung hoac xoa ten cua no de dam bao co duy nhat 1 ten hop le tai 1 khoi begin...end
										if ((string) nodep->user1p()->castNode()->typeName()=="ALWAYS")
											pointNode = nodep->user1p()->castNode()->op2p();
										else if ((string) nodep->user1p()->castNode()->typeName()=="IF")
											{
												if (nodep->user3()==2)
													pointNode = nodep->user1p()->castNode()->op2p();
												else pointNode = nodep->user1p()->castNode()->op3p();
											}

										found = 0;

										while ((pointNode!=NULL)&&(found==0))
										{
											if (pointNode->user2p()==NULL) break;
											else
											{
												for (int i=0; i<pointNode->user2vi()->size(); i++)
												{
													if ((pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMOutputName()==pmodule->GetComponent(id)->GetMOutputName())
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(id)->Getstart_index())
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMFanoutWidth()==pmodule->GetComponent(id)->GetMFanoutWidth())
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMValid()==1)
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getarray_index()==pmodule->GetComponent(id)->Getarray_index())
															&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getvalidname()==1)
															&& (pointNode->user2vi()->at(i)!=id))
													{
														if (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetFanout().size()==0)
															v3Global.circuit()->deletecomp(pmodule->GetComponent(pointNode->user2vi()->at(i)));
														pmodule->GetComponent(pointNode->user2vi()->at(i))->Setvalidname(0);
														found = 1;
													}
												}
												pointNode = pointNode->nextp();
											}
										}
									}
								}
								else std::cout<<"Khong tim thay Component phu hop de tao bo MUX : "<<pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName()<<std::endl;
							}
						tmpNode=tmpNode->nextp();
					}
				}
			}
			nodep->user2vi(VecId);

			//gop tin hieu trong cung mot khoi if neu khoi if dinh nghia cho cac phan lien tiep cua 1 tin hieu
			std::vector <FckId> myqueue;
			for (int j=0; j<nodep->user2vi()->size(); j++)
				myqueue.push_back(nodep->user2vi()->at(j));
			while (!myqueue.empty())
			{
				if (pmodule->GetComponent(myqueue[0])->Getvalidname()==0)
						myqueue.erase(myqueue.begin());
				else
				{
					bool exist; //su ton tai cua thanh phan lien tiep
					exist = 0;
					for (int j=1; j<myqueue.size(); j++)
					{
						if ((pmodule->GetComponent(myqueue[j])->GetMOutputName()==pmodule->GetComponent(myqueue[0])->GetMOutputName())
								//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->Getstart_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index())
								//&& (pmodule->GetComponent(pointNode->user2vi()->at(i))->GetMFanoutWidth()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth())
								&& (pmodule->GetComponent(myqueue[j])->GetMValid()==1)
								&& (pmodule->GetComponent(myqueue[j])->Getarray_index()==pmodule->GetComponent(myqueue[0])->Getarray_index())
								&& (pmodule->GetComponent(myqueue[j])->Getvalidname()==1))
						{
							if (pmodule->GetComponent(myqueue[j])->Getstart_index()==pmodule->GetComponent(myqueue[0])->Getend_index()+1)
							{
								FckId id_concat;
								fanin.clear();
								fanin.push_back(myqueue[j]);
								fanin.push_back(myqueue[0]);
								id_concat = pmodule->AddComponent(CONCAT,fanin,pmodule->GetComponent(myqueue[0])->GetMOutputName());
								pmodule->GetComponent(id_concat)->Setstart_index(pmodule->GetComponent(myqueue[0])->Getstart_index());
								pmodule->GetComponent(id_concat)->Setend_index(pmodule->GetComponent(myqueue[j])->Getend_index());
								pmodule->GetComponent(id_concat)->Setarray_index(pmodule->GetComponent(myqueue[0])->Getarray_index());
								myqueue.push_back(id_concat);
								//myqueue.erase(myqueue.begin());
								pmodule->GetComponent(myqueue[0])->Setvalidname(0);
								pmodule->GetComponent(myqueue[j])->Setvalidname(0);
								exist = 1;
								break;
							}
							else {
								if (pmodule->GetComponent(myqueue[j])->Getend_index()==pmodule->GetComponent(myqueue[0])->Getstart_index()-1)
								{
									FckId id_concat;
									fanin.clear();
									fanin.push_back(myqueue[0]);
									fanin.push_back(myqueue[j]);
									id_concat = pmodule->AddComponent(CONCAT,fanin,pmodule->GetComponent(myqueue[0])->GetMOutputName());
									pmodule->GetComponent(id_concat)->Setstart_index(pmodule->GetComponent(myqueue[j])->Getstart_index());
									pmodule->GetComponent(id_concat)->Setend_index(pmodule->GetComponent(myqueue[0])->Getend_index());
									pmodule->GetComponent(id_concat)->Setarray_index(pmodule->GetComponent(myqueue[0])->Getarray_index());
									myqueue.push_back(id_concat);
									//myqueue.erase(myqueue.begin());
									pmodule->GetComponent(myqueue[0])->Setvalidname(0);
									pmodule->GetComponent(myqueue[j])->Setvalidname(0);
									exist = 1;
									break;
								}
							}
						}
					}
					if ((exist==0)&& (pmodule->GetComponent(myqueue[0])->GetMType()==CONCAT)) VecId->push_back(myqueue[0]);
					myqueue.erase(myqueue.begin());
				}
			}

			nodep->user2vi(VecId);

			for (int n=0; n<nodep->user2vi()->size(); n++)
			{
				if ((pmodule->GetComponent(nodep->user2vi()->at(n))->GetMValid()==1)
						&& (pmodule->GetComponent(nodep->user2vi()->at(n))->Getvalidname()==1))
				{
					// xoa cac component ko dung hoac xoa ten cua no de dam bao co duy nhat 1 ten hop le tai 1 khoi begin...end
					AstNode *pointNode;  //Node dung de duyet cac node trong 1 khoi begin...end
					if ((string) nodep->user1p()->castNode()->typeName()=="ALWAYS")
						pointNode = nodep->user1p()->castNode()->op2p();
					else if ((string) nodep->user1p()->castNode()->typeName()=="IF")
						{
							if (nodep->user3()==2)
								pointNode = nodep->user1p()->castNode()->op2p();
							else pointNode = nodep->user1p()->castNode()->op3p();
						}

					found = 0;

					while ((pointNode!=NULL)&&(found==0))
					{
						if (pointNode->user2p()==NULL) break;
						else
						{
							for (int k=0; k<pointNode->user2vi()->size(); k++)
							{
								if ((pmodule->GetComponent(pointNode->user2vi()->at(k))->GetMOutputName()==pmodule->GetComponent(nodep->user2vi()->at(n))->GetMOutputName())
										&& (pmodule->GetComponent(pointNode->user2vi()->at(k))->Getstart_index()==pmodule->GetComponent(nodep->user2vi()->at(n))->Getstart_index())
										&& (pmodule->GetComponent(pointNode->user2vi()->at(k))->GetMFanoutWidth()==pmodule->GetComponent(nodep->user2vi()->at(n))->GetMFanoutWidth())
										&& (pmodule->GetComponent(pointNode->user2vi()->at(k))->GetMValid()==1)
										&& (pmodule->GetComponent(pointNode->user2vi()->at(k))->Getarray_index()==pmodule->GetComponent(nodep->user2vi()->at(n))->Getarray_index())
										&& (pmodule->GetComponent(pointNode->user2vi()->at(k))->Getvalidname()==1)
										&& (pointNode->user2vi()->at(k)!=nodep->user2vi()->at(n)))
								{
									if (pmodule->GetComponent(pointNode->user2vi()->at(k))->GetFanout().size()==0)
										v3Global.circuit()->deletecomp(pmodule->GetComponent(pointNode->user2vi()->at(k)));
									pmodule->GetComponent(pointNode->user2vi()->at(k))->Setvalidname(0);
									found = 1;
								}
							}
							pointNode = pointNode->nextp();
						}
					}
				}
			}

			//gop tin hieu voi cac lenh truoc trong cung 1 begin..end neu can thiet
			if (((string)nodep->user1p()->castNode()->typeName()=="IF")||((string)nodep->user1p()->castNode()->typeName()=="ALWAYS"))
			{
				int found_concat;
				for (int l=0; l<nodep->user2vi()->size(); l++ )
				{
					if (pmodule->GetComponent(nodep->user2vi()->at(l))->Getvalidname()==1)
					{
						FckId id1(0);
						AstNode* tmpNode;
						found_concat = 0;
						if (nodep->user3()==2)
							tmpNode = nodep->user1p()->castNode()->op2p();
						else if (nodep->user3()==3)
							tmpNode = nodep->user1p()->castNode()->op3p();
						else std::cout<<" AstGraph co loi xay ra"<<std::endl;

						while ((tmpNode!=nodep)&& (found_concat==0))
						{
							for (int i=0; i<tmpNode->user2vi()->size(); i++)
							{
								if ((pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMOutputName()== pmodule->GetComponent(nodep->user2vi()->at(l))->GetMOutputName())
										&& (((nodep->user4()==0)&&(pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getvalidname()==1))
												||((nodep->user4()==1)&&(pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getignore()==0)))
										&& (pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMValid()==1)
										&& (pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getarray_index()==pmodule->GetComponent(nodep->user2vi()->at(l))->Getarray_index())
										)
								{
									if (pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getstart_index() == pmodule->GetComponent(nodep->user2vi()->at(l))->Getend_index()+1)
									{
										found_concat = 1; //da tim thay Component can gop
										fanout = pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMOutputName();
										fanin.clear();
										fanin.push_back(tmpNode->user2vi()->at(i));
										fanin.push_back(nodep->user2vi()->at(l));
										id1 = pmodule->AddComponent(CONCAT,fanin,fanout);
										VecId->push_back(id1);
										pmodule->GetComponent(id1)->Setstart_index(pmodule->GetComponent(nodep->user2vi()->at(l))->Getstart_index());
										pmodule->GetComponent(id1)->Setend_index(pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getend_index());
									}
									else if (pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getend_index() == pmodule->GetComponent(nodep->user2vi()->at(l))->Getstart_index()-1)
									{
										found_concat = 1; //da tim thay Component can gop
										fanout = pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMOutputName();
										fanin.clear();
										fanin.push_back(nodep->user2vi()->at(l));
										fanin.push_back(tmpNode->user2vi()->at(i));
										id1 = pmodule->AddComponent(CONCAT,fanin,fanout);
										VecId->push_back(id1);
										pmodule->GetComponent(id1)->Setstart_index(pmodule->GetComponent(tmpNode->user2vi()->at(i))->Getstart_index());
										pmodule->GetComponent(id1)->Setend_index(pmodule->GetComponent(nodep->user2vi()->at(l))->Getend_index());
									}
									else std::cout<<"ton tai tin hieu cung ten nhung roi rac so voi tin hieu dang xet -> ko gop dc"<<" name = "<<pmodule->GetComponent(tmpNode->user2vi()->at(i))->GetMOutputName()<<std::endl;
								}
								if (found_concat==1)
								{
									if (nodep->user4()==0)
									{
										pmodule->GetComponent(tmpNode->user2vi()->at(i))->Setvalidname(0);
										pmodule->GetComponent(nodep->user2vi()->at(l))->Setvalidname(0);
									}
									else if (nodep->user4()==1)
									{
										pmodule->GetComponent(tmpNode->user2vi()->at(i))->Setignore(1);
										pmodule->GetComponent(nodep->user2vi()->at(l))->Setignore(1);
										pmodule->GetComponent(id1)->Setvalidname(0);
									}
									break;
								}
							}
							tmpNode=tmpNode->nextp();
						}
					}
				}

			}
			nodep->user2vi(VecId);
		}
		if (pmodule->GetComponent(nodep->op1p()->user2vi()->at(0))->GetFanout().size()==0)
			v3Global.circuit()->deletecomp(pmodule->GetComponent(nodep->op1p()->user2vi()->at(0)));
	}
}



virtual void visit(AstAlways* nodep, AstNUser*) {
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	if (v3Global.get_compiler()==0) {
		Module* pmodule = v3Global.circuit()->GetTopmodule();
		nodep->iterateChildren(*this);
		if ((nodep->user4()<3) || (v3Global.get_compiler()))
		{
			std::vector<unsigned int>* VecId;
			VecId = new vector<unsigned int>;
			//nodep->iterateChildren(*this);
			int found =0;

			AstNode *tmp = nodep->op2p();
			while (tmp!=NULL) {
				if (tmp->user2p()==NULL) {
					cout<<"ERROR: child node at op2p() of AstAlways does not include any Id of Components\n";
					return;
				}
				tmp = tmp->nextp();
			}

			//ALWAYS cung co 2 loai nhu IF

			if (nodep->user4()==1)
			{
				//Xac dinh rst dong bo hay khong dong bo
				//std::string reset="";
				//std::string reset_degree=""; // muc reset cao hay thap

				AstNode *sens_list;
				AstNode *clock_nodep, *reset_nodep;
				clock_nodep = NULL;
				reset_nodep = NULL;



				// duyet cac AstNode o op2p
				if (nodep->op2p())
				{
					AstNode* tmpNode;
					tmpNode=nodep->op2p();
					while (tmpNode!= NULL)
					{
						for (int x=0; x<tmpNode->user2vi()->size();x++)
							if ((pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMValid()==1)
									//&& (pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index()==-1)
									&& (pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getignore()==0))
							{
								found =0;
								//tim tat ca Comp co cung fanout va dua validname ve 0
								for (unsigned int i=0;i<pmodule->GetmComponents().size();i++)
									if ((pmodule->GetmComponents()[i]->GetMOutputName() == pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName())
											&& (pmodule->GetmComponents()[i]->GetMFanoutWidth() == pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth())
											&& (pmodule->GetmComponents()[i]->Getstart_index() == pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index())
											&& (pmodule->GetmComponents()[i]->GetMType()!=PS_INPUT)
											&& (pmodule->GetmComponents()[i]->Getarray_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index())
											)
										pmodule->GetmComponents()[i]->Setvalidname(0);


								FckId id;
								Component *Comp;
								Comp = pmodule->GetComponent(tmpNode->user2vi()->at(x));
								int done=0; //done=1 bao da tao duoc PS_OUTPUT can tao
								std::vector<FckId> fanin;
								std::string fanout;
								int PSO_pos; //vi tri cua PSO tring bo ITE
								fanout = pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName();
								fanin.push_back(pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth());
								sens_list = nodep->user1p()->castNode()->op1p()->op1p();


								// tao PS_OUTPUT

								if ((Comp->GetMType()==ITE)||(Comp->GetMType()==AND)||(Comp->GetMType()==OR))
								{
									Component *Reset_Signal;
									bool not_reset=0; //co cong NOT truoc reset
									if (Comp->GetMType()==ITE)
										Reset_Signal = Comp->GetFanin()[0];
									else
									{
										if ((Comp->GetFanin()[0]->GetMType()==NOT)&&(Comp->GetFanin()[0]->Getvalidname()==0))
										{
											Reset_Signal = Comp->GetFanin()[0]->GetFanin()[0];
											not_reset = 1;
										}
										else Reset_Signal = Comp->GetFanin()[0];
									}

									while (sens_list!=NULL)
									{
										if (Reset_Signal->Getvalidname()==1)
										{
											if (Reset_Signal->GetMOutputName()== (string) sens_list->op1p()->name())
											{
												FckId id_resetvalue;
												if (reset_nodep==NULL) reset_nodep = sens_list;
												else if (reset_nodep!=sens_list) std::cout<<" da co loi xay ra voi viec xac dinh reset "<<std::endl;

												if (Comp->GetMType()==ITE)
												{
													if ((string) ((AstSenItem*)sens_list)->edgeType().ascii()=="POS")
														id_resetvalue = Comp->GetFanin()[1]->GetMId();
													else if ((string) ((AstSenItem*)sens_list)->edgeType().ascii()=="NEG")
															id_resetvalue = Comp->GetFanin()[2]->GetMId();
														else std::cout<<"ERROR: ko xac dinh dung muc reset"<<std::endl;

													done = 1;
													id = pmodule->AddComponent(PS_OUTPUT,fanin,fanout);
													pmodule->GetComponent(id)->Setvalidname(0);
													pmodule->GetComponent(id)->AddFanin(Comp,0);
	/*
													// Xu li PSO
													done = 1;
													id = pmodule->AddComponent(PS_OUTPUT,fanin,fanout);
													pmodule->GetComponent(id)->Setvalidname(0);
													Comp->GetFanin()[PSO_pos]->ClearFanout();
													pmodule->GetComponent(id)->AddFanin(Comp->GetFanin()[PSO_pos],0);
													Comp->ChangeFanin(pmodule->GetComponent(id)->GetFanin()[0],pmodule->GetComponent(id));
													pmodule->GetComponent(id)->AddFanout(Comp);
													Comp->SetMValid(0) ;  //Edit by SonLam 23/4/2014
													*/
												}
												else //type = AND
												{
													done = 1;
													// xac dinh gia tri reset cua DFF
													FckId id_const;
													std::vector<FckId> vec_input;
													vec_input.clear();
													if ((((string) ((AstSenItem*)sens_list)->edgeType().ascii()=="POS")&&(not_reset==1))||
															(((string) ((AstSenItem*)sens_list)->edgeType().ascii()=="NEG")&&(not_reset==0)))
														id_const = pmodule->AddComponent(C_INPUT,vec_input,"0");
													else id_const = pmodule->AddComponent(C_INPUT,vec_input,"1");
													//tao PSO
													id = pmodule->AddComponent(PS_OUTPUT,fanin,fanout);
													pmodule->GetComponent(id)->Setvalidname(0);
													pmodule->GetComponent(id)->AddFanin(Comp,0);

													id_resetvalue = id_const;

													/* vec_input.clear();
													vec_input.push_back(Reset_Signal->GetMId());
													if ((string) ((AstSenItem*)sens_list)->edgeType().ascii()=="POS")
													{
														vec_input.push_back(id_const);
														vec_input.push_back(id);
													}
													else
													{
														vec_input.push_back(id);
														vec_input.push_back(id_const);
													}


													id_ite = pmodule->AddComponent(ITE,vec_input,fanout);
													pmodule->GetComponent(id_ite)->Setvalidname(0);
													pmodule->GetComponent(id_ite)->Setstart_index(0);
													pmodule->GetComponent(id_ite)->Setend_index(0);
													pmodule->GetComponent(id_ite)->SetMValid(0);  //Edit by SonLam 23/4/2014
													*/
												}

												/* FckId id_resetvalue;
												int pos;
												if (pmodule->GetComponent(id)->GetFanout()[0]->GetMType()==ITE)
												{
													if (pmodule->GetComponent(id)->GetFanout()[0]->GetFanin()[1]->GetMType()==PS_OUTPUT)
														pos=2;
													else pos =1;
													id_resetvalue = pmodule->GetComponent(id)->GetFanout()[0]->GetFanin()[pos]->GetMId();
													pmodule->GetComponent(id)->ClearFanout();
												}
												else std::cout<<" ITE sau PSO sai "<<std::endl;
												*/
												((PseudoOutput*)pmodule->GetComponent(id))->SetReset(Reset_Signal->GetMId(),id_resetvalue);
												//std::cout<<"id reset = "<<((PseudoOutput*)pmodule->GetComponent(id))->GetReset()<<std::endl;
											}
											else
											{
												if (clock_nodep==NULL) clock_nodep = sens_list;
												else if (clock_nodep!=sens_list) std::cout<<" da co loi xay ra voi viec xac dinh clock "<<std::endl;
											}
										}
										sens_list = sens_list->nextp();
									}
								}

	/*
								if ((Comp->GetMType()==ITE)||(Comp->GetMType()==AND))
								{
									Component *Reset_Signal;
									Reset_Signal = Comp->GetFanin()[0];

									while (sens_list!=NULL)
									{
										if (Reset_Signal->Getvalidname()==1)
										{
											if (Reset_Signal->GetMOutputName()== (string) sens_list->op1p()->name())
											{
												if (reset_nodep==NULL) reset_nodep = sens_list;
												else if (reset_nodep!=sens_list) std::cout<<" da co loi xay ra voi viec xac dinh reset "<<std::endl;

												if (Comp->GetMType()==ITE)
												{
													if ((string) ((AstSenItem*)sens_list)->edgeType().ascii()=="POS")
														PSO_pos = 2;
													else if ((string) ((AstSenItem*)sens_list)->edgeType().ascii()=="NEG")
															PSO_pos = 1;
														else std::cout<<"ERROR: ko xac dinh dung muc reset"<<std::endl;

													// Xu li PSO
													done = 1;
													id = pmodule->AddComponent(PS_OUTPUT,fanin,fanout);
													pmodule->GetComponent(id)->Setvalidname(0);
													Comp->GetFanin()[PSO_pos]->ClearFanout();
													pmodule->GetComponent(id)->AddFanin(Comp->GetFanin()[PSO_pos],0);
													Comp->ChangeFanin(pmodule->GetComponent(id)->GetFanin()[0],pmodule->GetComponent(id));
													pmodule->GetComponent(id)->AddFanout(Comp);
												}
												else //type = AND
												{
													done = 1;

													FckId id_const, id_ite;
													std::vector<FckId> vec_input;

													vec_input.clear();
													id_const = pmodule->AddComponent(C_INPUT,vec_input,"0");

													id = pmodule->AddComponent(PS_OUTPUT,fanin,fanout);
													pmodule->GetComponent(id)->Setvalidname(0);
													pmodule->GetComponent(id)->AddFanin(Comp,0);

													vec_input.push_back(Reset_Signal->GetMId());
													vec_input.push_back(id);
													vec_input.push_back(id_const);

													id_ite = pmodule->AddComponent(ITE,vec_input,fanout);
													pmodule->GetComponent(id_ite)->Setvalidname(0);
													pmodule->GetComponent(id_ite)->Setstart_index(0);
													pmodule->GetComponent(id_ite)->Setend_index(0);
												}

												FckId id_resetvalue;
												int pos;
												if (pmodule->GetComponent(id)->GetFanout()[0]->GetMType()==ITE)
												{
													if (pmodule->GetComponent(id)->GetFanout()[0]->GetFanin()[1]->GetMType()==PS_OUTPUT)
														pos=2;
													else pos =1;
													id_resetvalue = pmodule->GetComponent(id)->GetFanout()[0]->GetFanin()[pos]->GetMId();
												}
												else std::cout<<" ITE sau PSO sai "<<std::endl;
												((PseudoOutput*)pmodule->GetComponent(id))->SetReset(Reset_Signal->GetMId(),id_resetvalue);
												std::cout<<"id reset = "<<((PseudoOutput*)pmodule->GetComponent(id))->GetReset()<<std::endl;
											}
											else
											{
												if (clock_nodep==NULL) clock_nodep = sens_list;
												else if (clock_nodep!=sens_list) std::cout<<" da co loi xay ra voi viec xac dinh clock "<<std::endl;
											}
										}
										sens_list = sens_list->nextp();
									}
								}
	*/
								if (done==0)
								{
									if (clock_nodep==NULL) clock_nodep = nodep->user1p()->castNode()->op1p()->op1p();
									else if (clock_nodep!=nodep->user1p()->castNode()->op1p()->op1p()) std::cout<<" da co loi xay ra voi viec xac dinh clock voi done = 0"<<std::endl;
									//std::cout<<clock_nodep->op1p()->name()<<std::endl;
									done = 1;
									id = pmodule->AddComponent(PS_OUTPUT,fanin,fanout);
									pmodule->GetComponent(id)->AddFanin(pmodule->GetComponent(tmpNode->user2vi()->at(x)),0);
									pmodule->GetComponent(id)->Setvalidname(0);
								}

								if (done == 1)
								{
									// gan cac gia tri clock, reset thich hop cho PSO
									if (clock_nodep!=NULL)
									{
										((PseudoOutput*)pmodule->GetComponent(id))->Setclock_name(((string)clock_nodep->op1p()->name()));
										((PseudoOutput*)pmodule->GetComponent(id))->Setclock_edge(((string)((AstSenItem*)clock_nodep)->edgeType().ascii()));
										((PseudoOutput*)pmodule->GetComponent(id))->SetClock(clock_nodep->op1p()->user2vi()->at(0));
									}
									else std::cout<<" khong tim thay clock cho khoi Always cho mach tuan tu"<<std::endl;

									if (reset_nodep!=NULL)
									{
										((PseudoOutput*)pmodule->GetComponent(id))->Setreset_name(((string)reset_nodep->op1p()->name()));
										((PseudoOutput*)pmodule->GetComponent(id))->Setreset_edge(((string)((AstSenItem*)reset_nodep)->edgeType().ascii()));
									}

									pmodule->GetComponent(id)->Setarray_index(pmodule->GetComponent(id)->GetFanin()[0]->Getarray_index());
									pmodule->GetComponent(id)->Setstart_index(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index());
									pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
								}
								else std::cout<<" da co loi xay ra voi tao PSO "<<std::endl;

								VecId->push_back(id);
							}
						tmpNode=tmpNode->nextp();
					}
				}
			}
			else
			{
				// duyet cac AstNode o op2p
				if (nodep->op2p())
				{
					AstNode* tmpNode;
					tmpNode=nodep->op2p();
					while (tmpNode!= NULL)
					{
						for (int x=0; x<tmpNode->user2vi()->size();x++)
							if ((pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getvalidname() ==1) &&
									//(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index()==-1) &&
									(pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMValid()==1))
							{
								found = 0;
								//tim tat ca Comp co cung fanout va dua validname ve 0
								for (unsigned int i=0;i<pmodule->GetmComponents().size();i++)
									if ((pmodule->GetmComponents()[i]->GetMOutputName() == pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName())
											&& (pmodule->GetmComponents()[i]->GetMFanoutWidth() == pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth())
											&& (pmodule->GetmComponents()[i]->Getarray_index()==pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index())
											&& (pmodule->GetmComponents()[i]->Getstart_index() == pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index())
											&& (pmodule->GetmComponents()[i]!=pmodule->GetComponent(tmpNode->user2vi()->at(x)))
											)
										pmodule->GetmComponents()[i]->Setvalidname(0);

								// Thay the va loai bo cac Comp UNDEF
								for (unsigned int j=0;j<pmodule->GetmComponents().size();j++)
									if ((pmodule->GetmComponents()[j]->GetMType()==UNDEF) &&
											(pmodule->GetmComponents()[j]->GetMValid()==1)&&
											(pmodule->GetmComponents()[j]->Getarray_index()==-1)&&
											(pmodule->GetmComponents()[j]->GetMOutputName() == pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName())
											)
									{
										if ((pmodule->GetmComponents()[j]->GetMFanoutWidth() == pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth())&&
												(pmodule->GetmComponents()[j]->Getstart_index() == pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index()))
										{
											pmodule->GetmComponents()[j]->SetMValid(0);
											// thay UNDEF trong cac ket noi bang Component ADD vua tao
											for (unsigned int k= 0; k< pmodule->GetmComponents()[j]->GetFanout().size(); k++)
											{
												//gan cac vector mFanout cua UNDEF cho component ADD vua tao
												pmodule->GetComponent(tmpNode->user2vi()->at(x))->AddFanout(pmodule->GetmComponents()[j]->GetFanout().at(k));
												//voi moi fanout do, tim trong fanin cua no vi tri chua UNDEF vao thay bang ADD
												pmodule->GetmComponents()[j]->GetFanout().at(k)->ChangeFanin(pmodule->GetmComponents()[j],pmodule->GetComponent(tmpNode->user2vi()->at(x)));
											}
										}
										else pmodule->GetmComponents()[j]->Addelements(pmodule->GetComponent(tmpNode->user2vi()->at(x)));
										break;
									}
								VecId->push_back(tmpNode->user2vi()->at(x));
							}
						tmpNode=tmpNode->nextp();
					}
				}
			}
			nodep->user2vi(VecId);
		}
	}
}

virtual void visit(AstInitial* nodep, AstNUser*) {
	//cout<<nodep->typeName()<<"::"<<nodep->name()<<"\n";
	Module* pmodule = v3Global.circuit()->GetTopmodule();
	nodep->iterateChildren(*this);
	if ((nodep->user4()<3) || (v3Global.get_compiler()))
	{
		//std::vector<unsigned int>* VecId;
		//VecId = new vector<unsigned int>;
		//nodep->iterateChildren(*this);
		int found =0;

		if (nodep->op1p())
		{
			AstNode* tmpNode;
			tmpNode=nodep->op1p();
			while (tmpNode!= NULL)
			{
				for (int x=0; x<tmpNode->user2vi()->size();x++)
					if ((pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getvalidname() ==1) &&
								(pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMValid()==1) &&
								(pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getarray_index()==-1))
					{
						found = 0;
						// Thay the va loai bo cac Comp UNDEF
						for (unsigned int j=0;j<pmodule->GetmComponents().size();j++)
							if ((pmodule->GetmComponents()[j]->GetMType()==UNDEF) &&
										(pmodule->GetmComponents()[j]->GetMValid()==1)&&
										(pmodule->GetmComponents()[j]->Getarray_index()==-1)&&
										(pmodule->GetmComponents()[j]->GetMOutputName() == pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMOutputName())
									)
							{
								if ((pmodule->GetmComponents()[j]->GetMFanoutWidth() == pmodule->GetComponent(tmpNode->user2vi()->at(x))->GetMFanoutWidth())&&
											(pmodule->GetmComponents()[j]->Getstart_index() == pmodule->GetComponent(tmpNode->user2vi()->at(x))->Getstart_index()))
								{
									pmodule->GetmComponents()[j]->SetMValid(0);
									// thay UNDEF trong cac ket noi bang Component ADD vua tao
									for (unsigned int k= 0; k< pmodule->GetmComponents()[j]->GetFanout().size(); k++)
									{
										//gan cac vector mFanout cua UNDEF cho component ADD vua tao
										pmodule->GetComponent(tmpNode->user2vi()->at(x))->AddFanout(pmodule->GetmComponents()[j]->GetFanout().at(k));
										//voi moi fanout do, tim trong fanin cua no vi tri chua UNDEF vao thay bang ADD
										pmodule->GetmComponents()[j]->GetFanout().at(k)->ChangeFanin(pmodule->GetmComponents()[j],pmodule->GetComponent(tmpNode->user2vi()->at(x)));
									}
								}
								else pmodule->GetmComponents()[j]->Addelements(pmodule->GetComponent(tmpNode->user2vi()->at(x)));
								break;
							}
						//VecId->push_back(tmpNode->user2vi()->at(x));
					}
				tmpNode=tmpNode->nextp();
			}
		}
		//nodep->user2vi(VecId);
	}
}

/*
virtual void visit(AstActive* nodep, AstNUser*) {
	std::cout<<nodep->name()<<std::endl;
	std::cout<<nodep->user4()<<std::endl;
	nodep->iterateChildren(*this);
}
*/





int CreateCompConnection(AstNode* pnode){
	return 0;//edited by Son Lam 18/9
}




/*
 * Tao 1 FCK module tuong ung node AstModule
 * tro con tro toi AstNUser1, mark is 1
 * Tao FCK Input tuong ung voi node AstInput
 *
 */


public:
	// CONSTRUCTORS
	NodeVisitor(AstNetlist* nodep) {
		nodep->accept(*this);
	}
	// Destructor
	virtual ~NodeVisitor() {}
};

// implement traverseNode() method
void V3NodeTraversal::traverseNode(AstNetlist* nodep) {
	//UINFO(9,__FUNCTION__<<": "<<endl);

	// connect a node to parent
	Connect2Parent(nodep);

	// visit all node
	// create FCK component for each AstNode
	NodeVisitor mvisitor (nodep);

	//edited by Son Lam 24/9/2013

	Module* pmodule;
	if (v3Global.get_compiler()==0) {
		pmodule = v3Global.circuit()->GetTopmodule();
		//gop cac thanh phan cua tin hieu, dong thoi loai bo UNDEF
		for (int j=0; j<pmodule->GetmComponents().size(); j++)
		{
			if ((pmodule->GetmComponents()[j]->GetMType()==UNDEF)&&
					//(pmodule->GetmComponents()[j]->GetFanout()[0]->GetMType()!=RAM)&&
					(pmodule->GetmComponents()[j]->GetMValid()==1)&&
					(pmodule->GetmComponents()[j]->Getarray_index()==-1))
			{
				Component* Comp_Undef;
				Component* Comp_before, *Comp_after;
				Comp_Undef = pmodule->GetmComponents()[j];
				//std::cout<<"so element "<<Comp_Undef->Getelements().size()<<std::endl;
				if (Comp_Undef->Getelements().size()!=0)
				{
					int found=0;
					int done=0;
					while (done==0)
					{
						found=0;
						for (int i=0; i<Comp_Undef->Getelements().size(); i++)
							if ((Comp_Undef->Getelements()[i]->Getstart_index()==0)&&(Comp_Undef->Getelements()[i]->Getvalidname()==1))
							{
								Comp_before = Comp_Undef->Getelements()[i];
								found=1;
								break;
							}
						if (found==0)
						{
							std::cout<<"da co loi xay ra trong gop bit"<<std::endl;
							break;
						}
						else
						{
							if (Comp_before->GetMFanoutWidth()==Comp_Undef->GetMFanoutWidth())
							{
								Comp_Undef->SetMValid(0);
								// thay UNDEF trong cac ket noi bang Component vua tao
								for (unsigned int k= 0; k< Comp_Undef->GetFanout().size(); k++)
								{
									//gan cac vector mFanout cua UNDEF cho component ADD vua tao
									Comp_before->AddFanout(Comp_Undef->GetFanout().at(k));
									//voi moi fanout do, tim trong fanin cua no vi tri chua UNDEF vao thay bang ADD
									Comp_Undef->GetFanout().at(k)->ChangeFanin(Comp_Undef,Comp_before);
								}
								done=1;
							}
							else
							{//tim Comp_after de gop bit
								found=0;
								for (int l=0; l<Comp_Undef->Getelements().size(); l++)
									if ((Comp_Undef->Getelements()[l]->Getstart_index()==Comp_before->GetMFanoutWidth())&&(Comp_Undef->Getelements()[l]->Getvalidname()==1))
									{
										found=1;
										Comp_after = Comp_Undef->Getelements()[l];
										FckId id; //id cua Comp Concat dung de gop bit
										std::vector<FckId> fanin;
										fanin.clear();
										fanin.push_back(Comp_after->GetMId());
										fanin.push_back(Comp_before->GetMId());
										id = pmodule->AddComponent(CONCAT,fanin,Comp_Undef->GetMOutputName());
										pmodule->GetComponent(id)->Setstart_index(0);
										pmodule->GetComponent(id)->Setend_index(pmodule->GetComponent(id)->Getstart_index() + pmodule->GetComponent(id)->GetMFanoutWidth()-1);
										Comp_Undef->Addelements(pmodule->GetComponent(id));
										Comp_before->Setvalidname(0);
										Comp_after->Setvalidname(0);
										break;
									}
								if (found==0)
								{
									std::cout<<"da co loi xay ra (thieu thanh phan de gop)"<<std::endl;
									done=1;
								}
							}
						}
					}
				}
				else std::cout<<"da co loi xay ra (khong the gop bit)"<<std::endl;
			}
		}

		// loai bo cac component co valid = 0 ra khoi vector mComponents
		std::vector<Component*> mComp;
		for (unsigned int i=0;i<pmodule->GetmComponents().size();i++)
			if (pmodule->GetmComponents()[i]->GetMValid()==1)
				mComp.push_back(pmodule->GetmComponents()[i]);

		pmodule->SetmComponents(mComp);
		v3Global.module()->SetmComponents(mComp);
		//pmodule->Print();
		//FCK2Verilog(pmodule);
	}
}


/*
 *	The function connect a node to its parent, using AstNUser to point to a AstNode.
 *  firstly, this funct traverses the first child node of current node and node next to child node.
 *	with a node visited, assign AstNUser of child node to current node.
 *  this action iterate on all child and next to children of current node.
 *  author: Quandvk54
 */
void Connect2Parent(AstNode* pnode) {
	// new code 29-07
		if(!pnode) return;
		// edited by Son Lam 20/10 , su dung user4 de bieu thi node nam o mach seq(1) hay comb(0)
		if ((string)pnode->typeName()=="NETLIST") pnode->user4(3);
		else pnode->user4(pnode->user1p()->castNode()->user4());
		if ((string)pnode->typeName()=="ACTIVE")
		{
			if ((string) pnode->name()=="initial") pnode->user4(2);
			if ((string) pnode->name()=="sequent") pnode->user4(1);
			if ((string) pnode->name()=="combo") pnode->user4(0);

		}
		//else pnode->user4(0);

		// assign AstNUser of a child node to parent AstNode
		if(pnode->op1p()){

			pnode->op1p()->user1p(pnode);
			pnode->op1p()->user3(1);  //user3p cua AstNode con luu so thu tu op cua AstNode cha chua no
			//if (pnode->user4()==1) pnode->op1p()->user4(1);  //edited by Son Lam 20/10
			//else pnode->op1p()->user4(0);

			// assign AstNUser of a node being next to child node to parent AstNode
		    if(pnode->op1p()->nextp()){
				AstNode* tmpNode = pnode->op1p()->nextp();
				while(tmpNode){		// loop with all node being next to node
					tmpNode->user1p(pnode);
					tmpNode->user3(1);
					//if (pnode->user4()==1) tmpNode->user4(1);  //edited by Son Lam 20/10
					//else tmpNode->user4(0);
					tmpNode = tmpNode->nextp();
				}
				delete tmpNode;
			}
		    Connect2Parent(pnode->op1p());	// loop with m_op1p child node
		}


		// assign AstNUser of child node to parent AstNode
		if(pnode->op2p()){

			pnode->op2p()->user1p(pnode);
			pnode->op2p()->user3(2);
			//if (pnode->user4()==1) pnode->op2p()->user4(1);  //edited by Son Lam 20/10
			//else pnode->op2p()->user4(0);

		    if(pnode->op2p()->nextp()){
				AstNode* tmpNode = pnode->op2p()->nextp();
				while(tmpNode){		// loop with all next to node
					tmpNode->user1p(pnode);
					tmpNode->user3(2);
					//if (pnode->user4()==1) tmpNode->user4(1);  //edited by Son Lam 20/10
					//else tmpNode->user4(0);
					tmpNode = tmpNode->nextp();
				}
				delete tmpNode;
			}
			Connect2Parent(pnode->op2p());	// loop with m_op1p child node
		}


		// assign AstNUser of child node to parent AstNode
		if(pnode->op3p()){

			pnode->op3p()->user1p(pnode);
			pnode->op3p()->user3(3);
			//if (pnode->user4()==1) pnode->op3p()->user4(1);  //edited by Son Lam 20/10
			//else pnode->op3p()->user4(0);

		    if(pnode->op3p()->nextp()){
				AstNode* tmpNode = pnode->op3p()->nextp();
				while(tmpNode){		// loop with all next to node
					tmpNode->user1p(pnode);
					tmpNode->user3(3);
					//if (pnode->user4()==1) tmpNode->user4(1);  //edited by Son Lam 20/10
					//else tmpNode->user4(0);
					tmpNode = tmpNode->nextp();
				}
				delete tmpNode;
			}
		    Connect2Parent(pnode->op3p());	// loop with m_op1p child node
		}

		// assign AstNUser of child node to parent AstNode
		if(pnode->op4p()){

			pnode->op4p()->user1p(pnode);
			pnode->op4p()->user3(4);
			//if (pnode->user4()==1) pnode->op4p()->user4(1);  //edited by Son Lam 20/10
			//else pnode->op4p()->user4(0);

		    if(pnode->op4p()->nextp()){
				AstNode* tmpNode = pnode->op4p()->nextp();
				while(tmpNode){		// loop with all next to node
					tmpNode->user1p(pnode);
					tmpNode->user3(4);
					//if (pnode->user4()==1) tmpNode->user4(1);  //edited by Son Lam 20/10
					//else tmpNode->user4(0);
					tmpNode = tmpNode->nextp();
				}
				delete tmpNode;
			}
		    Connect2Parent(pnode->op4p());	// loop with m_op1p child node
		}

		//	loop with node next to current node
		Connect2Parent(pnode->nextp());
}



void pushIDnext(AstNode* nodep,std::vector<FckId> fanin){
	AstNode* tmpNode=nodep;

	// loop every node which next to pAstNode
	while(tmpNode->nextp()){
		fanin.push_back((unsigned)tmpNode->user2vi()->at(0) );
		tmpNode=tmpNode->nextp();
	}
}

std::string convertdec2bin(unsigned int n)  //width thuong co gia tri bang 32
{
	std::vector<int> vector_bit;  // vector luu cac so du
	std::string binary;  //chuoi so nhi phan
	int surplus;  //so du
	int quotient=n; //thuong so
	do
	{
		surplus = quotient % 2;
		quotient = quotient/2;
		vector_bit.push_back(surplus);
	}
	while (quotient!=0);
	for (int i=vector_bit.size()-1; i>=0; i--)
	{
		if (vector_bit[i]==1)
			binary.insert(binary.length(),"1");
		else binary.insert(binary.length(),"0");
	}
	//if (binary.length() < width)
		//binary.insert(0,width-binary.length(),'0');
	return binary;
}

std::string convertdec2bin_width(unsigned int n, int width)
{
	std::vector<int> vector_bit;  // vector luu cac so du
	std::string binary;  //chuoi so nhi phan
	int surplus;  //so du
	int quotient=n; //thuong so
	do
	{
		surplus = quotient % 2;
		quotient = quotient/2;
		vector_bit.push_back(surplus);
	}
	while (quotient!=0);
	for (int i=vector_bit.size()-1; i>=0; i--)
	{
		if (vector_bit[i]==1)
			binary.insert(binary.length(),"1");
		else binary.insert(binary.length(),"0");
	}
	if (binary.length() < width)
		binary.insert(0,width-binary.length(),'0');
	return binary;
}


std::string converthex2bin(std::string hex)
{
	string binary;  //hang so o dang binary

	//cac thao tac chuyen tu HEXA sang BINARY
	int pos = hex.find("h");
	for (int i=pos+1; i<hex.length(); i++)
		switch (hex.at(i))
		{
		case '0': binary.insert(binary.length(),"0000" );
					break;
		case '1': binary.insert(binary.length(),"0001" );
					break;
		case '2': binary.insert(binary.length(),"0010" );
					break;
		case '3': binary.insert(binary.length(),"0011" );
					break;
		case '4': binary.insert(binary.length(),"0100" );
					break;
		case '5': binary.insert(binary.length(),"0101" );
					break;
		case '6': binary.insert(binary.length(),"0110" );
					break;
		case '7': binary.insert(binary.length(),"0111" );
					break;
		case '8': binary.insert(binary.length(),"1000" );
					break;
		case '9': binary.insert(binary.length(),"1001" );
					break;
		case 'a': binary.insert(binary.length(),"1010" );
					break;
		case 'b': binary.insert(binary.length(),"1011" );
					break;
		case 'c': binary.insert(binary.length(),"1100" );
					break;
		case 'd': binary.insert(binary.length(),"1101" );
					break;
		case 'e': binary.insert(binary.length(), "1110" );
					break;
		case 'f': binary.insert(binary.length(), "1111" );
					break;
		default : break;
		}
	return binary;
}

unsigned int convertbin2dec(std::string bin)
{
	unsigned int dec = 0;
	for (int i=bin.size()-1;i>=0; i--)
		if (bin[i]=='1') dec = dec + powerof2(bin.size()-i-1);
	return dec;
}


unsigned int powerof2(unsigned int n)
{
	unsigned int s=1;
	for (int i=0; i<n; i++)
		s=s*2;
	return s;
}

//kiem tra xem addr cua ram la bien so hay hang so
//neu la bien so tra ve 1
bool test_ram_addr (Component* comp){
	std::queue<Component*> myqueue ;
	bool found = 0;
	myqueue.push(comp);

	while ((!myqueue.empty())&& (found==0))
	{
		if (myqueue.front()->Getvarref()==1) found=1;
		else if (myqueue.front()->GetFanin().size()>0)
			{
				for (int i=0; i<myqueue.front()->GetFanin().size();i++)
				{
					myqueue.push(myqueue.front()->GetFanin()[i]);
				}
			}
		myqueue.pop();
	}
	return found;

}
