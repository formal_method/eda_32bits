/*! ************************************************************************
    \file unaryxor.h 
    \brief Header file of the Class UnaryXor.

    The class contained in this file defines the attributes and methods of the
 *  Function UnaryXor.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Mon Feb 13 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef UNARYXOR_H
#define UNARYXOR_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class to describe the behaviour of the UnaryXor Function.
 * This class implements the Function UnaryOr derived from the class
 * Function.
 */
class UnaryXor : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The UnaryXor Function needs 1 Fanin.
     */
    UnaryXor(){SetMRequiredFanin(1);SetMType(U_OR);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * A UnaryOr has one fanin Component so initialize mRequiredFanin with 1.
     * Add the fanin component to the fanin of this UnaryOr.
     * @param fanin A vector containing the Fanin Component of the UnaryOr.
     * @param fanout The string naming the output
     * @param id The ID of the UnaryXor
     * @param module The Module the UnaryXor shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     */
    UnaryXor(std::vector<Component*>& fanin, std::string fanout, int id, 
             Module* module);
    
    /**
     * Destructor
     */
    virtual ~UnaryXor(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this component as a string (U_XOR)
     * @return the string "U_XOR"
     */
    inline std::string GetType(){return "U_XOR";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();
        
    /**
     * Perform a simulation of this UnaryXor for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );
    
    /**
     * Create the CNF for this UnaryXor.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor);
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
        
    
    /**
     * Operations
     */
};
#endif //UNARYXOR_H

