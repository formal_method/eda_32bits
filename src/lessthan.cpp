/*! ************************************************************************
    \file lessthan.cpp
    \brief Implementation of the Class LessThan

    The class contained in this file implements the methods of the Function
 * LessThan
    \author Sacha Lotiz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Wed Thu 09 2006

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout

**************************************************************************/

#include "lessthan.h"

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * A LessThan has two fanin Components so initialize mRequiredFanin with 
 * 2. The result of a LessThan is whether the fanin at pos=0 is less than the
 * fanin at pos=1.
 * Add the fanin components to the fanin of this LessThan.
 * @param fanin A vector containing the two Fanin Components of the LessThan.
 * @param fanout The string naming the output
 * @param id The ID of this LessThan
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 * @exception InputSizeException is thrown if the fanins do not have the same 
 * size -> extend to achieve this
 */
LessThan::LessThan(std::vector<Component*>& fanin, std::string fanout, int id, 
                    Module* module)
{
    mModule=module;
    SetMRequiredFanin(2);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMType(LT);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have two fanin components so lets add these to our new Add Function.
    //First of all we allocate space for the Vector mFanin with undefined
    //fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //Therefore we use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to position i. 
    //In this implementation we require the fanins to have the same size.
    unsigned int help=fanin[0]->GetMFanoutWidth();
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that the fanin has the correct size
        if (help!=fanin[i]->GetMFanoutWidth())
        {
            std::string message="EXCEPTION: The width of the fanins is not";
            message +=" equivalent."; 
            throw InputSizeException(message, this, fanin);
        }  
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mcRequiredFanin;i++)
    mFanoutWidth=1;
} //End LessThan(vector<Component> fanin)  
/**
 * Methods
 */
/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin.
 */
void LessThan::SimulateForward ()
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
} //End SimulateForward

/**
 * Perform a simulation of this LessThan for a given vector of inputs
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void LessThan::Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
{
    //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for
    //We reset mOutput
    mOutput.clear();
    //We want both inputs to have the same size /todo this mustn't be the case
    try
    {
        if (inpVals[0].size()!=inpVals[1].size())
        {
            //The two inputs do not have the same width so we use a sign 
            //extension in order to be able to add the to inputs. Throw an 
            //Exception to inform the user. 
            std::string message = "EXCEPTION: The inputs of this LessThan "; 
            message+="function do not have the same width. I will use a zero ";
            message+="extension.";
            throw InputSizeException(message, this, mFanin);
        } // End if (mInputs[0].size()!=mInputs[1].size())
    } // End try
    //We handle the input size exception directly by zero extending the 
    //smaller input.
    catch(InputSizeException e)
    {
        //print the message to let the user know that we use a sign extended
        //input
        e.GetMessage();
        //Determine the smaller input; store in the variables smaller and 
        //bigger
        int smaller, bigger;
        if (inpVals.size()<inpVals[1].size())
        {
            smaller=0;
            bigger=1;
        }
        else
        {
            smaller=1;
            bigger=0;
        }
        //As long as smaller is smaller than bigger push sign to smaller
        while (inpVals[smaller].size()!=inpVals[bigger].size())
            inpVals[smaller].push_back(satlib::SatI::FALSE);
    } // End catch(InputSizeException e)
    outVals.push_back(Simulate(inpVals[0].size()-1, inpVals));
}

/**
 * Calculate simulation result from a specified bit on.
 * If the specified bit can not determine the result call the next lower
 * bit recursively.
 * @param pos The position of the bit that shall be simulated.
 * @param inpVals The input values the simulation shall be performed for
 * @return The result of the simulation.
 */
satlib::SatI::lValue LessThan::Simulate(int pos,
                std::vector< std::vector< satlib::SatI::lValue> >& inpVals)
{
   //if pos=-1 than the two operands are equivalent return false
    if (pos==-1) return satlib::SatI::FALSE;
    //switch for the result of the input at pos=0
    switch (inpVals[0][pos])
    {
        //FALSE if the other input bit is TRUE return TRUE. Otherwise 
        //consider the result of the next bit
        case 0: if (inpVals[1][pos]==satlib::SatI::TRUE)  // C. Villarraga 18.08.2011: Correction now using inpVals instead of mInputs
        		 	 return satlib::SatI::TRUE;
                 //otherwise we have to fetch the result from the next 
                 //lower bit
                 else 
                 {
                     satlib::SatI::lValue next=Simulate(pos-1, inpVals);
                     //if next is true the output will be true no matter
                     //whether mInputs[1][pos] is l_Undef or l_False
                     if (next==satlib::SatI::TRUE) return satlib::SatI::TRUE;
                     //if both inputs are FALSE next is returned 
                     else if (inpVals[1][pos]==satlib::SatI::FALSE) return next;
                     //if the second bit is l_Undef and not followed by a
                     //result l_True than the result is l_Undef
                     else return satlib::SatI::DONTCARE;
                 } //End else
        //DONTCARE if the other input is DONTCARE as well DONTCARE 
        //is returned
        case 5:  if (inpVals[1][pos]==satlib::SatI::DONTCARE)
                         return satlib::SatI::DONTCARE;
                 //otherwise we have to fetch the result from the next 
                 //lower bit
                 else
                 {
                     satlib::SatI::lValue next=Simulate(pos-1,inpVals);
                     //if the second input and the returned result are 
                     //equal the result is equal to next
                     if(next==inpVals[1][pos]) return next;
                     //otherwise we can not tell anythong about the
                     //output return false
                     else return satlib::SatI::FALSE;
                 } //End else
        //TRUE if the other input is FALSE the result is FALSE. 
        //Otherwise the result of the next bit has to be considered
        case 1:  if(inpVals[1][pos]==satlib::SatI::FALSE)
                    return satlib::SatI::FALSE;
                 //We have to fetch the result from the next lower bit
                 else
                 {
                     satlib::SatI::lValue next=Simulate(pos-1,inpVals); 
                     //if next is false the output will be false no
                     //matter whether mInputs[1][pos] is l_Undef or 
                     //l_False
                     if (next==satlib::SatI::FALSE) return satlib::SatI::FALSE;
                     //if both inputs are l_True next is returned 
                     else if (inpVals[1][pos]==satlib::SatI::TRUE) return next;
                     //if the second bit is l_Undef and not followed by a
                     //result l_True than the result is l_Undef
                     else return satlib::SatI::DONTCARE;
                 } // End else
        //We should not be able to reach another case
        default: 
        {
            std::string message="EXCEPTION: Reached an ";
            message+="impossible sum in add";
            throw DefaultException(message);
        }
    } // End of switch(mInputs[0][pos].toInt()) 
}//End Simulate

/**
 * Create the CNF for this LessThan.
 * @param solver the interface to the solver
 */
void LessThan::CreateCnf(satlib::SatI& satSolver)
{
    //vector for the result literals
    std::vector<satlib::RtpLit> r;
    //vector for the literals of input a
    std::vector<satlib::RtpLit> a;
    //vector for the literals of input b
    std::vector<satlib::RtpLit> b;
    //vector storing the result of an intermediate lessthan operation
    std::vector<satlib::RtpLit> lt;
    assert(GetFanin().size()==2);
    //Get the literals for the result and the inputs
    GetFanoutRtpLits(r);
    GetFanin()[0]->GetFanoutRtpLits(a);
    GetFanin()[1]->GetFanoutRtpLits(b);
    assert(r.size()==1);
    assert(b.size()==a.size());
    assert(a.size()>0);
    //Get Literals for the intermediate result
    for(unsigned i=0;i<a.size()-1;i++)
        lt.push_back(satlib::RtpLit(mId,GetMFanoutWidth()+1+i));
    // changed on 07-29-10 from -r[0].second to r[0].second in order to get
    // anticipated result for less than operation
    lt.push_back(satlib::RtpLit(r[0].first,r[0].second));
    satSolver.makeAnd(satlib::RtpLit(a[0].first,-a[0].second),b[0],lt[0]);
    for(unsigned i=1;i<lt.size();i++)
    {
    //one step in the lt path
        std::vector<satlib::RtpLit> cl(3);
        cl[0]=satlib::RtpLit(a[i].first,-a[i].second);cl[1]=b[i];
        cl[2]=satlib::RtpLit(lt[i].first,-lt[i].second);
        satSolver.addClause(cl);
        cl[0]=a[i];cl[1]=satlib::RtpLit(b[i].first,-b[i].second);
        cl[2]=lt[i];
        satSolver.addClause(cl);
        cl[0]=a[i];cl[1]=satlib::RtpLit(lt[i-1].first,-lt[i-1].second);
        cl[2]=lt[i];
        satSolver.addClause(cl);
        cl[0]=satlib::RtpLit(a[i].first,-a[i].second);cl[1]=lt[i-1];
        cl[2]=satlib::RtpLit(lt[i].first,-lt[i].second);
        satSolver.addClause(cl);
        cl[0]=b[i];cl[1]=lt[i-1];
        cl[2]=satlib::RtpLit(lt[i].first,-lt[i].second);
        satSolver.addClause(cl);
        cl[0]=satlib::RtpLit(b[i].first,-b[i].second);
        cl[1]=satlib::RtpLit(lt[i-1].first,-lt[i-1].second);cl[2]=lt[i];
        satSolver.addClause(cl);
    }
}

void LessThan::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_ult(btor, mFanin[0]->GetBtorComp(), mFanin[1]->GetBtorComp());
	BtorComp = comp;
}
