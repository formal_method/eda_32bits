/*! ************************************************************************
    \file component.h 
    \brief Header File of Class Component.
    
    Defines the attributes and methods all components either have in common or
 * have to implement
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Tue Jan 31 - Wed Feb 08 2006

Modification history:
 * 17.02.06 changed return type of AddFanin from bool to void. Whether an 
 * add is successful or not is now handled by exceptions
 * added attribute mOutputName to name the component and the accessor methods
 * 16.03.06 added attributes to store the width of fanin (mFaninSize) and 
 * fanout (mFanoutSize). To get the width of the fanout a method 
 * GetMFanoutSize has been added.
 *
 * 26.10.06
 *
 * 10/9/2014
 * them con tro den Btor Component tuong ung voi FCK component va cac ham set, get()
 *
 * #include <cassert> is added
**************************************************************************/

#ifndef COMPONENT_H
#define COMPONENT_H
#include <string>
#include <vector>
#include <iostream>
#include <cassert>
#include <cstdio>
#include "SatI.h"
#include "circuitbuilder.h"

#include "../lib/boolector-1.6.0-with-sat-solvers/boolector/boolector.h"
#include <assert.h>

typedef unsigned FckId;

class Module;

/**
 * Parent class for the Components the Circuit consists of.
 * 
 * Defines the attributes and methods all components either have in common or
 * have to implement.
 */
class Component {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     * not needed as this class has pure virtual members
     */
    /**
     * Destructor
     */
    virtual ~Component(){}
    /**
     * Accessor Methods
     */
    /**
     * Returns the value of the Input bits of a specified input.
     * @param input the position in mInputs that shall be returned 
     * @return The value of the input bits at the position specified by input.
     * 
     * Abstract method has to be implemented by subclass as this class does 
     * not know mInputs.
     */
    virtual std::vector< satlib::SatI::lValue> GetInput(int input)=0;
        
    
    /**
     * Returns the Output bits of the Component.
     * @return The value of the output bits.
     * 
     * Abstract has to be implemented by subclass as this class does not know
     * mOutput.
     */
    virtual std::vector< satlib::SatI::lValue> GetOutput()=0;
        
    
    /**
     * Return the Components in the fanout of this Component.
     * If a Component has Components in the Fanout it must override this 
     * method. 
     * @return This method here returns an empty vector for those Components 
     * that do not have Components in their fanout (Override when necessary).
     */
    virtual std::vector<Component*> GetFanout();
    
    /**
     * Return the Components in the fanout of this Component.
     * If a Component has Components in the Fanout it must override this 
     * method. 
     * @return This method here returns GetFanout() for those Components 
     * that have only one output (Override when necessary).
     */
    virtual std::vector<Component*> GetFanout(std::string name)
        {return GetFanout();}
        
    
    /**
     * Return the Components in the Fanin.
     * If a Component has Components in the Fanin it has to override this 
     * method. 
     * @return This method here returns an empty vector (Override when 
     * necessary).
     */
    virtual std::vector<Component*> GetFanin() const;
    
    /**
     * Return the number of fanin Components required by this component.
     * @return The Number of fanin Components needed.
     */
    unsigned int GetMRequiredFanin(){return mRequiredFanin;}
    
    /**
     * Return the type of this Components.
     * @return The Integer value of the type.
     */
    int GetMType() {return mType;}
    
    /**
     * Get the type of this Component in string representation
     * @return the type of this component as a string
     */
    virtual std::string GetType()=0;
    
    /**
     * For an input set the values of the input.
     * Do nothing here input will override this method, however, no other 
     * class will.
     * @param input a vector of lbool containing the new input bits
     */
    virtual void SetInput(std::vector< satlib::SatI::lValue>& input)
    { std::cout << "warning: SetInput inherited from Component." << std::endl;}
    
    /**
     * Add a Component to the fanin of this Component at the position
     * given.
     * @param cmp Reference to the Component that shall be added to the fanin.
     * @param pos The position at which this Component shall be added. 
     */
    virtual void AddFanin (Component* cmp, unsigned int pos)=0;
    
    /**
     * Change the Component in the fanin of this Component
     * @param cmpold Reference to the Component that shall be removed
     * @param cmpnew Reference to the Component that shall be added
     * @exception GeneralFaninException thrown if cmpold is not found in the
     * fanin of this component
     */
    virtual void ChangeFanin(Component* cmpold, Component* cmpnew)=0;
    
    /**
     * Add a component to the fanout of this component.
     * @param cmp a reference to the Component that shall be added.
     */
    virtual void AddFanout (Component* cmp){};
    
    /**
     * Add a component to the fanout of this component.
     * @param cmp a reference to the Component that shall be added.
     */
    virtual void ClearFanout ()=0;

    /**
     * Add a Component a named fanout of this Component.
     * @param cmp a reference to the Component that shall be added.
     * @param name not used at this level
     */
    virtual void AddFanout (Component* cmp, std::string name){AddFanout(cmp);}
    
    /**
     * Get the output name of the component
     * @return the name of the output of the component
     */
    std::string GetMOutputName() const {return mOutputName;}
    
    /**
     * Get the width of the output
     * @return the width of the output
     */
    unsigned int GetMFanoutWidth(){return mFanoutWidth;}
    
    /**
     * Get the ID of the component
     */
    int GetMId(){return mId;}
    
    /**
     * Get a vector with the RtpLits defined by this component
     * @param r the vector where the defiend rtpLits will be stored in.
     */
    virtual void GetFanoutRtpLits(std::vector<satlib::RtpLit>& r);
    
    /**
     * Get the input name of the component
     * @return "" Inputs have to override this
     */
    virtual std::string GetMInputName(){return "";}

    /**
     * Set the Edge on which the PS_OUTPUT sets it's value
     */
    virtual void SetOnEdge(int edge){}
    
    /**
     * Get the Edge on which the PS_OUTPUT sets it's value
     */
    virtual int GetEdge(){return 0;}    
    
    /**
     * Get the number of Outputs the Component has.
     * For all Components except of ModuleInterface this is 1.
     * @return The number of Outputs of the Component
     */
    virtual inline int GetMOutputs(){return 1;}
    /**
     * Operations
     */
    /**
     * Check whether the Component is complete.
     * pure virtual method as the fanin components are not konwn on this level
     * @return True if the Component has mRequiredFanin fanin Components and
     * at least one fanout Components.
     */
    virtual bool IsComplete()=0;
    
    /**
     * Update the value of mInput.
     * Fetch the current assignment of the outputs of the fanin Components and
     * update mInput of this Component accordingly.
     */
    virtual void UpdateInputs()=0;
    
    /**
     * Perform a forward simulation from the Inputs to the Outputs. Therefore
     * get the output values of the fanin components and update the values of 
     * mOutput according to the current values of mInputs.
     * 
     * Abstract method has to be implemented by subclasses. As the simulation
     * highly depends on the kind of the Component.
     */
    virtual void SimulateForward(){}
    
    /**
     * Perform a simulation on this component for a given vector of inputs
     * 
     * Abstract method has to be implemented by subclasses. As the simulation
     * highly depends on the kind of the Component.
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    virtual void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )=0;
    
    /**
     * Create the CNF for this Component
     * @param solver the interface to the solver
     */
    virtual void CreateCnf(satlib::SatI& solver){}
    
    /**
     * Print the assignment of the output of this Component
     * @param satSolver The sat solver that retrieved the solution
     */
    virtual void PrintFanoutAssignment(satlib::SatI& satSolver);
    
    /**
     * Get the assignment of the output of this Component
     * @param satSolver The sat solver that retrieved the solution
     * @param assignment the assignment of the output shall be returned here
     */
    virtual void GetFanoutAssignment(satlib::SatI& satSolver, 
                               std::vector<satlib::SatI::lValue>& assignment);
    /**
     * Get the position of an Output of a Module.
     * Here only used as Interface to moduleInterface overriden there
     * @param name The name of the requested Output
     * @return the position of the requested Output
     */
    virtual int GetOutput(std::string name){return 0;}
    
    /**
     * Get the names of the Inputs of a Module
     * Here only used as Interface to moduleInterface overriden there
     * @return A vector containing the inputs
     */
    virtual std::vector<Component*> GetModuleInputs();
    
    /**
     * Get an Output Component from the Module included by this 
     * ModuleInterface
     * @return reference to the Output Component
     */
    virtual Component* GetModuleOutput();
    
    /**
     * Get the Module included in a ModuleInterface - Is junk here.
     * @return if the Component is a ModuleInterface the included Module.
     */
    virtual Module* GetInclModule(){return mModule;}
    
    /**
     * Get the Module containing this Component
     * @return the Module containing this Component
     */
    virtual Module* GetModule(){return mModule;}
    
    /**
     * Set the reset properties of this PS_INPUT
     * @param reset The input used to reset the PS_INPUT
     * @param resVal The input delivering the value this PS_INPUT is reset to.
     */
    virtual inline void SetReset(FckId reset, FckId resVal){}
    
    /**
     * Get the fanin resetting the pseudo input
     * @return The name of the resetting input
     */
    virtual FckId GetReset(){return 0;}
     
    /**
     * Get the fanin determing the reset value
     * @return The name of the signal determing the value this pseudo input is
     * reset to
     */
    virtual FckId GetResetValue(){return 0;}
    /**
         * Set the name of the Output of this Component
         * @param output The name of the output
         */
    void SetMOutputName(std::string output){mOutputName=output;}
    /**
     * Set the valid or invalid of Components.
     * @param valid = 1 mean this is a valid Component, valid = 0 mean this is a invalid Component
    */
    void SetMValid(int valid){mValid=valid;}
    /**
     * Get the mValid of the component
    */
    int GetMValid(){return mValid;}
    // ham set va get cho cac thuoc tinh khac
    void Setvalidname(int valid_name){validname=valid_name;}
    int Getvalidname(){return validname;}
    // chu y comment
    void Setvarref(int var_ref){varref=var_ref;}
    int Getvarref(){return varref;}
    // chu y comment
    void Setstart_index(int start){start_index=start;}
    int Getstart_index(){return start_index;}
    // chu y comment
    void Setend_index(int end){end_index=end;}
    int Getend_index(){return end_index;}
    // chu y comment
    void Setignore(int ig){ignore=ig;}
    int Getignore(){return ignore;}
    // chu y comment
    void Setarray_index(int array){array_index=array;}
    int Getarray_index(){return array_index;}
    /**
     *
     */
    std::vector<Component*> Getelements(){return elements;};
    void Addelements(Component* comp) {elements.push_back(comp);};

    /**
     * tao Boolector Component tuong ung voi FCK component
     */
	virtual void Create_BtorComp(Btor* btor){}
    BtorNode* GetBtorComp(){return BtorComp;}

/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     * Constant value defining how many fanin Components this Component needs.
     */
    unsigned int mRequiredFanin;
    /**
     * value represents for valid of component
     */
    int mValid;
    /**
     * chi so bat dau va ket thuc
     */
    int ignore; //duoc dung trong tao ITE va tao PSO, khi ignore=1 thi ko can xet den
    int array_index; //chi so cua tin hieu trong mang tin hieu, neu tin hieu ko thuoc mang nao thi array_index=-1

    int start_index;
    int end_index;
    /**
     * parts of signal
     */
    std::vector<Component*> elements;
    /**
     * cac thuoc tinh duoc dung trong tao machj to hop dung alway
     */
    int validname; // the hien cho tinh hop le cua fanout
    int varref; // varref=1 hien thi rang component nay duoc tao tu AstVarRef
    // ket thuc cac thuoc tinh
    /**
     * Constant value defining the type of the operation
     */
    int mType;
    /**
     * A string naming the output of this Component
     */
    std::string mOutputName;
    /**
     * A vector to store the size of the fanin Components
     */
    std::vector<unsigned int> mFaninWidth;
    /**
     * The size of the fanout components
     */
    unsigned int mFanoutWidth;
    /**
     * The ID of the component
     */
    int mId;
    
    /**
     * The Module this Component is in
     */
    Module* mModule;
    /**
     * 
     */
	BtorNode* BtorComp;
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Set the number of fanin Components required by this component.
     * @param fanin The Number of fanin Components needed.
     */
    void SetMRequiredFanin(unsigned int fanin){mRequiredFanin=fanin;}
    
    /**
     * Set the type of this Components.
     * @param type The Integer value of the type.
     */


    // change to public 22-08
public:
    void SetMType(int type) {mType=type;}

    /**
     * Operations
     */
    
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
};
#endif //COMPONENT_H

