/*! ************************************************************************
    \file constinput.cpp 
    \brief Implementation of class Input
    
    Implementation of the methods of the class ConstInput
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 15 2006 

Modification history:
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 15.03.06 added the for loop analysing the constValue and setting the 
 * ConstantInput accordingly
 * 26.10.06 SatI::RtpLit is replaced by RtpLit

**************************************************************************/

#include "constinput.h"
#include <cstring>
#include <string>

/**
 * Constructors/Destructors
 */
 /**
  * A constructor with the constant value.
  * A ConstInput has no fanin Component so initialize mRequiredFanin with 0.
  * Set the value of the input to the constant value passed.
  * @param constValue taking the constant value for the constant Input. The
  * input is expected to contain a string combined out of lValue values.
  * @param fanout The name of the output of the constant input
  * @param id The ID of the constant input
  */
ConstInput::ConstInput(std::string constValue, int id, 
                        Module* module)
{
    mModule=module;
    SetMRequiredFanin(0);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMType(C_INPUT);
    //SetMOutputName(fanout);
    SetMOutputName(constValue); //edited by Son Lam 9/10/2013
    SetMInputName(constValue);
    mId=id;
    BtorComp = NULL;
    //analyze the constant value and store the equivalent lValue vector in cin.
    std::vector<satlib::SatI::lValue> cin;
    // iterate over the string constValue for each 1 push a l_True, for each
    // -1 push a l_False and for each 0 push a l_Undef
    for (int i=constValue.size()-1; i>=0; i--)
    {
        //Do we have a false value? Therefore we have to check constValue at
        //i-1 as this is invalid for i=0 we have to check whether i>0 first
        if (constValue[i]=='0')
        {
            //add the false value 
            // std::cout<<'0'; // C.Villarraga 18.03.2011 Comented this line
            cin.push_back(satlib::SatI::FALSE);
        }
        else if (constValue[i]=='1') 
        {
            // std::cout<<'1'; // C.Villarraga 18.03.2011 Comented this line
            cin.push_back(satlib::SatI::TRUE);
        }
        else 
        {
            // std::cout<<'5'; // C.Villarraga 18.03.2011 Comented this line
            cin.push_back(satlib::SatI::DONTCARE);
        }
    }
    // std::cout<<std::endl; // C.Villarraga 18.03.2011 Comented this line
    SetConstInput(cin);
    mFanoutWidth=constValue.size();
    mFaninWidth.push_back(constValue.size());
} //End Input(vector<Component> fanin)
    
    /**
     * Create the CNF for this ConstantInput.
     * @param solver the interface to the solver
     */
void ConstInput::CreateCnf(satlib::SatI& solver)
{
    //The vector containing the result bits
    std::vector<satlib::RtpLit> r;
    //Get the rtp literals for the result vector
    GetFanoutRtpLits(r);
    
    for (unsigned int i=0;i<mOutput.size();i++)
    {
        if (mOutput[i]!=satlib::SatI::DONTCARE)
        {
            std::vector<satlib::RtpLit> cl;
            if (mOutput[i]==satlib::SatI::TRUE)
            {
                cl.push_back(r[i]);
            }
            else
            {
                cl.push_back(satlib::RtpLit(r[i].first,-r[i].second));
            }
            solver.addClause(cl);
            cl.clear();
        }
    }
}

int ConstInput::get_decvalue()
{
	int decvalue = 0;
	for (int i=0; i<this->GetMOutputName().length(); i++)
	{
		int powerof2 = 1;
		if (this->GetMOutputName()[i]=='1')
		{
			for (int j=0; j<this->GetMOutputName().length()-i-1; j++)
			{
				powerof2 = powerof2*2;
			}
			decvalue = decvalue + powerof2;
		}
	}
	return decvalue;
}

void ConstInput::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_const(btor,this->GetMInputName().c_str());
	BtorComp = comp;
}

