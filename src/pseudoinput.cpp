/*! ************************************************************************
    \file pseudoinput.cpp
    \brief Implementation of class PseudoInput

    Implementation of the methods of the class PseudoInput
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Tue Feb 14 2006

Modification history:
 * 17.02.06 changed return type of AddFanin from bool to void. Whether an
 * add is successful or not is now handled by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout

**************************************************************************/

#include "pseudoinput.h"

/**
 * Constructors/Destructors
 */
 /**
  * A constructor with a vector of fanin Components.
  * An PseudoInput has one fanin Component so initialize mRequiredFanin with 1.
  * @param name A vector containing the name of the PseudoInput
  * @param fanout The string naming the PseudoInput
  * @param id The ID of the component
  */
PseudoInput::PseudoInput(const std::vector<FckId>& ids,
                         const std::string& name, int id, Module* module)
{
    mModule=module;
    SetMRequiredFanin(1);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    Setpsi_before(0);
    Setpsi_after(0);
    SetMType(PS_INPUT);
    SetMInputName(name);
    SetMOutputName(name); //edited by Son Lam 21/10
    mId=id;
    BtorComp = NULL;
    //We can not add the fanin component yet, as the corresponding fanout
    //does not exist yet. So we only allocate space for the Vector mFanin with
    //undefined fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //The width has to extracted from the string before it can be set.
    SetWidth(ids[0]);
    //Set the name of the output
    //SetMOutputName(name);
} //End Input(vector<Component> fanin)

/**
 * Methods
 */
/**
 * Add a Component to the fanin of this PseudoInput.
 * In differenece to an Input a PseudoInput has a Fanin Component. This
 * Fanin Component has to be a PseudoOutput. This method adds a
 * PseudoOutput to the fanin of this PseudoInput at the Position given.
 * @param cmp Reference to the Component that shall be added to the fanin.
 * @param pos The position at which this Component shall be added. A
 * PseudoInput has only one input.
 * @exception DoubledFaninException if fanin is either not needed or already
 * there.
 */
void PseudoInput::AddFanin (Component* cmp,unsigned int pos)
{
    // Check if there is no Element at the position
    if (pos<mRequiredFanin && mFanin[pos]->GetMType()==0)
    {
        //There is no Element at pos and pos is needed. So we add the Element
        //and return true.
        mFanin[pos]=cmp;
        SetWidth(cmp->GetMFanoutWidth());
    } // End if (pos<mcRequiredFanin && mFanin[pos]==NULL)
    else
    {
        //Something went wrong the Element is already there or not needed
        std::string message="The fanin that shall be added is either already ";
        message+="there or there is no fanin needed at that position.";
        if (pos >= mRequiredFanin)
            throw DoubledFaninException(message, this, cmp, new Undef(), pos);
        else throw DoubledFaninException(message, this, cmp, mFanin[pos], pos);
    } //End else
    cmp->AddFanout(this);
} //End AddFanin (Component& cmp, int pos)


/**
 * Update the value of mInput
 * Fetch the current assignment of the output of the fanin Component and
 * update mInput of this Output accordingly.
 */
void PseudoInput::UpdateInputs()
{
    mOutput.clear();
    //For each fanin Component get its output values and assign them to the
    //corresponding input
    //mOutput is assigned the output value of the Fanin
    mOutput=mFanin[0]->GetOutput();
} // End UpdateInputs

/**
 * Check whether the ConstantInput is complete.
 * @return True if the ConstantInput has mRequiredFanin fanin Components (=0)
 * and at least one fanout Component.
 */
bool PseudoInput::IsComplete ()
{
    //Do we have an output
    if (mFanout.size()==0)
    {
        //Sometimes we don't have an Output due to Optimization so just print
        //a warning
        std::cout<<"WARNING: "<<GetType()<<" "<<GetMOutputName()
                <<"has no fanout"<<std::endl;
    }
    //all inputs should
    //be there after a successful creation
    if (mFanin.size()==mRequiredFanin) return true;
    //not complete
    else
    {
        std::cout<<GetType()<<" "<<GetMOutputName()<<" fanins: "
                <<mFanin.size()<<" required: "<<mRequiredFanin<<std::endl;
        return false;
    }
}

/**
 * Get a vector of the RtpLits defined by this component
 * For a Constant Input these are equivalent to the input lits
 * @param r the vector where the defined RtpLits will be stored in.
 */
void PseudoInput::GetFanoutRtpLits(std::vector<satlib::RtpLit>& r)
{
    mFanin[0]->GetFanoutRtpLits(r);
}
