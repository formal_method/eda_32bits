// -*- mode: C++; c-file-style: "cc-mode" -*-
//*************************************************************************
// DESCRIPTION: Verilator: traverse node
//
//
//*************************************************************************
#ifndef _V3NODETRAVERSAL_H_
#define _V3NODETRAVERSAL_H_
#include "config_build.h"
#include "verilatedos.h"
#include "V3Error.h"
#include "V3Ast.h"
#include "circuit.h"
#include "module.h"
#include <queue>


//============================================================================
using namespace std;


class V3NodeTraversal {
public:
static void traverseNode(AstNetlist* nodep);
};
// print node information
void makeTree(AstNode*);

//void FCK2Verilog(Circuit* pcircuit);

// connect a node to parent, using AstNUser
void Connect2Parent(AstNode* pnode);

int CreateCompConnection(AstNode* pnode);

void pushIDnext(AstNode* nodep,std::vector<FckId> fanin);

std::string convertdec2bin(unsigned int n);
std::string convertdec2bin_width(unsigned int n, int width);
std::string converthex2bin(std::string hex);
unsigned int convertbin2dec(std::string bin);
unsigned int powerof2(unsigned n);
bool test_ram_addr(Component* comp);

#endif // Guard
