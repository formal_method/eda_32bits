/**************************************************************************
Modification history:
22.03.06 Sacha Loitz changed the values of the enum constants in lValue for
 * easier simulation.
 * 26.10.06 site of declaration for RtpLit is changed, definition of
 * operator- for RtpLit is moved into SatI.cpp
 * 30.01.2011 C.Villarraga - added the deleteId to missing methods,
   so it is possible to delete the clauses of any component (AND, OR, XOR, NOT, ...)
**************************************************************************/

#include "SatI.h"
#include <vector>
#include <iostream>

// this definition was moved here from SatI.h  
satlib::RtpLit operator - (satlib::RtpLit p){ return satlib::RtpLit(p.first,-p.second);}

bool satlib::SatI::makeBuffer(RtpLit a,RtpLit r,unsigned deleteId){
  return makeNot(a,-r,deleteId);
}
bool satlib::SatI::makeNot(RtpLit a,RtpLit r,unsigned deleteId){
  std::vector<RtpLit> cl;
  cl.push_back(a);
  cl.push_back(r);
  addClause(cl,deleteId);
  cl.clear();
  cl.push_back(-a);
  cl.push_back(-r);
  addClause(cl,deleteId);
  return getStatus()!=UNSAT;
}
bool satlib::SatI::makeAnd(RtpLit a,RtpLit b, RtpLit r,unsigned deleteId){
  std::vector<RtpLit> cl;
  cl.push_back(a);
  cl.push_back(b);
  return makeAnd (cl,r,deleteId);
}
bool satlib::SatI::makeAnd(std::vector<RtpLit>& inputs, RtpLit r,
                           unsigned deleteId){
  std::vector<RtpLit> clLong;
  for(unsigned i=0;i<inputs.size();i++){
    std::vector<RtpLit> cl;
    cl.push_back(inputs[i]);
    cl.push_back(RtpLit(r.first,-r.second));
    addClause(cl,deleteId);
    clLong.push_back(RtpLit(inputs[i].first,-inputs[i].second));
  }
  clLong.push_back(r);
  addClause(clLong,deleteId);
   
  return getStatus()!=UNSAT;
}
//! r=ite(s,a,b)
bool satlib::SatI::makeIte(RtpLit s,RtpLit a,RtpLit b, RtpLit r,unsigned deleteId){
//   std::cout<<"@"<<r.first<<"["<<r.second<<"] = ite(";
//   std::cout<<"@"<<s.first<<"["<<s.second<<"],";
//   std::cout<<"@"<<a.first<<"["<<a.second<<"],";
//   std::cout<<"@"<<b.first<<"["<<b.second<<"])"<<std::endl;
   std::vector<RtpLit> cl(3);
   cl[0]=s;cl[1]=r;cl[2]=-b; //-s=>(-r=>-b)
   addClause(cl,deleteId);
   cl[0]=s;cl[1]=-r;cl[2]=b; //-s=>(-b=>-r)
   addClause(cl,deleteId);
   cl[0]=-s;cl[1]=r;cl[2]=-a; //s=>(-r=>-a)
   addClause(cl,deleteId);
   cl[0]=-s;cl[1]=-r;cl[2]=a; //s=>(-a=>-r)
   addClause(cl,deleteId);
   //redundant but stronger unit propagation
   cl[0]=-a;cl[1]=-b;cl[2]=r; //ab=>r
   addClause(cl,deleteId);
   cl[0]=a;cl[1]=b;cl[2]=-r; //(-a)(-b)=>-r
   addClause(cl,deleteId);
   return getStatus()!=UNSAT;
}
bool satlib::SatI::makeOr(RtpLit a,RtpLit b, RtpLit r,unsigned deleteId){
  std::vector<RtpLit> cl;
  cl.push_back(RtpLit(a.first,-a.second));
  cl.push_back(RtpLit(b.first,-b.second));
  return makeAnd (cl,RtpLit(r.first,-r.second),deleteId);//de Morgan
}
bool satlib::SatI::makeOr(std::vector<RtpLit>& inputs, RtpLit r,unsigned deleteId){
  std::vector<RtpLit> cl;
  for(unsigned i=0;i<inputs.size();i++){
    cl.push_back(-inputs[i]);
  }
   return makeAnd (cl,-r,deleteId);//de Morgan

}
bool satlib::SatI::makeXor(RtpLit a,RtpLit b, RtpLit r,unsigned deleteId){
  std::vector<RtpLit> cl;
  cl.push_back(a);
  cl.push_back(b);
  cl.push_back(r);
  return makeEvenParity (cl,deleteId);
}
bool satlib::SatI::makeEvenParity(std::vector<RtpLit>& cl,
                                  unsigned deleteId){
 
  return makeEvenParity(cl,cl.size()-1,true,deleteId);
}
bool satlib::SatI::makeEvenParity(std::vector<RtpLit>& cl,int level
                                  ,bool fliplast,unsigned deleteId){
  
  if(level>0){ 
    makeEvenParity(cl,level-1,fliplast,deleteId);
    cl[level]=RtpLit(cl[level].first,-cl[level].second);
    makeEvenParity(cl,level-1,!fliplast,deleteId);
    cl[level]=RtpLit(cl[level].first,-cl[level].second);
  } else{
    if(fliplast)
      cl[level]=RtpLit(cl[level].first,-cl[level].second);
    addClause(cl,deleteId);
   if(fliplast)
      cl[level]=RtpLit(cl[level].first,-cl[level].second);
  }    
  return getStatus()!=UNSAT;

}
bool satlib::SatI::makeHA(RtpLit a,RtpLit b, RtpLit r, RtpLit c,unsigned deleteId){
  std::vector<RtpLit> cl;
  makeXor(a,b,r,deleteId);
  makeAnd(a,b,c,deleteId);
  cl.push_back(-r);
  cl.push_back(-c);
  addClause(cl,deleteId); //c=>-r to improve unit propagation
  return getStatus()!=UNSAT;
}
bool satlib::SatI::makeFA(RtpLit a,RtpLit b, RtpLit ci, RtpLit r, RtpLit co,unsigned deleteId){
  std::vector<RtpLit> inputs;
  inputs.push_back(a);
  inputs.push_back(b);
  inputs.push_back(ci);
  makeMajority(inputs,co,deleteId);
  inputs.push_back(r);
  makeEvenParity(inputs,deleteId);
  return getStatus()!=UNSAT;
}
bool satlib::SatI::makeMajority(std::vector<RtpLit>& inputs,
                                RtpLit r,unsigned deleteId){
  std::vector<RtpLit> cl;
  makeMajority(inputs,r,cl,0,deleteId);
  return getStatus()!=UNSAT;
}
bool satlib::SatI::makeMajority(std::vector<RtpLit>& inputs,
                                RtpLit r,
                                std::vector<RtpLit>& cl,
                                unsigned startWith,unsigned deleteId){
  if(cl.size()+inputs.size()-startWith<=inputs.size()/2)
    return true;
  if(cl.size()>inputs.size()/2){
    cl.push_back(-r);
    addClause(cl,deleteId);
    for(unsigned i=0;i<cl.size();i++)
      cl[i]=-cl[i];
    addClause(cl,deleteId) ;
    for(unsigned i=0;i<cl.size();i++)
      cl[i]=-cl[i];
    cl.pop_back();
    return getStatus()!=UNSAT;
  } else {
    for(unsigned i=startWith;i<inputs.size();i++){
      cl.push_back(inputs[i]);
      makeMajority(inputs,r,cl,i+1,deleteId);
      cl.pop_back();
    } 
    return getStatus()!=UNSAT;

  }
               
  
}

