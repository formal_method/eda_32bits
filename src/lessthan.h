/*! ************************************************************************
    \file lessthan.h
    \brief Header file of the Class LessThan.

    The class contained in this file defines the attributes and methods of the
 *  Function LessThan.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Thu Feb 09 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef LESSTHAN_H
#define LESSTHAN_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"
#include "../exceptions/defaultexception.h"

/**
 * Class to describe the behaviour of the LessThan Function.
 * This class implements the Function LessThan derived from the class
 * Function.
 */
class LessThan : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The LessThan Function needs 2 Fanins. The result is whether the
     * fanin at pos=0 is less than the fanin at pos=1.
     */
    LessThan(){SetMRequiredFanin(2);SetMType(LT);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * A LessThan has two fanin Components so initialize mRequiredFanin with 
     * 2. The result of a LessThan is whether the fanin at pos=0 is less than the
     * fanin at pos=1.
     * Add the fanin components to the fanin of this LessThan.
     * @param fanin A vector containing the two Fanin Components of the LessThan.
     * @param fanout The string naming the output
     * @param id The ID of this LessThan
     * @param module the Module the LessThan shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     * @exception InputSizeException is thrown if the fanins do not have the same 
     * size -> extend to achieve this
     */
    LessThan(std::vector<Component*>& fanin, std::string fanout, int id, 
             Module* module);
    
    /**
     * Destructor
     */
    virtual ~LessThan(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this LessThan as a string (LT)
     * @return The string "LT"
     */
    inline std::string GetType(){return "LT";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();

    /**
     * Perform a simulation of this LessThan for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );
    
    /**
     * Create the CNF for this LessThan.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor);
    
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */

    /**
     * Operations
     */
    
    /**
     * Calculate simulation result from a specified bit on.
     * If the specified bit can not determine the result call the next lower 
     * bit recursively.
     * @param pos The position of the bit that shall be simulated.
     * @param inpVals The input values the simulation shall be performed for
     * @return The result of the simulation.
     */
    satlib::SatI::lValue Simulate(int pos,
                  std::vector< std::vector< satlib::SatI::lValue> >& inpVals);
};
#endif //LESSTHAN_H

