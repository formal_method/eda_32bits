/*! ************************************************************************
    \file unaryand.h 
    \brief Header file of the Class UnaryAnd.

    The class contained in this file defines the attributes and methods of the
 *  Function UnaryAnd.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Mon Feb 13 2006 

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef UNARYAND_H
#define UNARYAND_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class to describe the behaviour of the UnaryAnd Function.
 * This class implements the Function UnaryAnd derived from the class
 * Function.
 */
class UnaryAnd : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The UnaryAnd Function needs 1 Fanin.
     */
    UnaryAnd(){SetMRequiredFanin(1);SetMType(U_AND);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * A UnaryAnd has one fanin Component so initialize mRequiredFanin with 1.
     * Add the fanin component to the fanin of this UnaryAnd.
     * @param fanin A vector containing the Fanin Component of the UnaryAnd. 
     * @param fanout The string naming the output
     * @param id the ID of the UnaryAnd
     * @param module the Module the UnaryAnd shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     */
    UnaryAnd(std::vector<Component*>& fanin, std::string fanout, int id, 
             Module* module);
    
    /**
     * Destructor
     */
    virtual ~UnaryAnd(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this UnaryAnd as a string (U_AND)
     * @return the string "U_AND"
     */
    inline std::string GetType(){return "U_AND";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();
        
    /**
     * perform a simulation of this UnaryAnd for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );
    /**
     * create the CNF for this UnaryAnd.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor);
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
        
    
    /**
     * Operations
     */
};
#endif //UNARYAND_H

