/*! ************************************************************************
    \file component.cpp
    \brief Implementation of class Component

    Implementation of the non virtual methods of the class Component.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Tue Jan 31 - Wed Feb 07 2006

Modification history:

**************************************************************************/

#include "component.h"
#include "undef.h"

/**
 * Constructors/Destructors
 */
/**
 * Methods
 */

/**
 * Return the Components in the fanout of this Component.
 *
 * If a Component has Components in the Fanout it must override this
 * method.
 * @return This method here returns an empty vector for those Components
 * that do not have Components in their fanout (Override when necessary).
 */
std::vector<Component*> Component::GetFanout ()
{
    //create the empty vector
    std::vector<Component*> emptyVector;
    //return the vector
    return emptyVector;

}
/**
 * Return the Components in the Fanin.
 *
 * If a Component has Components in the Fanin it has to override this method.
 * @return This method here returns an empty vector (Override when necessary).
 */
std::vector<Component*> Component::GetFanin () const
{
   //create the empty vector
    std::vector<Component*> emptyVector;
    //return the vector
    return emptyVector;
}

/**
 * Get a vector with the RtpLits defined by this component
 * @param r the vector that shall contain the rtpLits
 */
void Component::GetFanoutRtpLits(std::vector<satlib::RtpLit>& r)
{
    r.clear();
    for(unsigned int i=0; i<mFanoutWidth; i++)
    {
        r.push_back(satlib::RtpLit(mId, i+1));
    }
}

/**
 * Print the assignment of the output of this Component
 * @param satSolver The sat solver that retrieved the solution
 */
void Component::PrintFanoutAssignment(satlib::SatI& satSolver)
{
    std::cout<<GetMOutputName()<<": ";
    for(unsigned int i=mFanoutWidth; i>0; i--)
    {
        std::cout<<satSolver.getValue(satlib::RtpLit(mId, i));
    }
    std::cout<<std::endl;
}

/**
 * Print the assignment of the output of this Component
 * @param satSolver The sat solver that retrieved the solution
 */
void Component::GetFanoutAssignment(satlib::SatI& satSolver,
                                std::vector<satlib::SatI::lValue>& assignment)
{
    for(unsigned int i=0; i<mFanoutWidth; i++)
    {
        assignment.push_back(satSolver.getValue(
                satlib::RtpLit(mId, i+1)));
    }
}

/**
 * Get the names of the Inputs of a Module
 * Here only used as Interface to moduleInterface overriden there
 * @return A vector containing the inputs
 */
std::vector<Component*> Component::GetModuleInputs()
{
    std::vector<Component*> undef;
    return undef;
}

/**
 * Get an Output Component from the Module included by this ModuleInterface
 * @param name The name of the Output
 * @return reference to the Output Component
 */
Component* Component::GetModuleOutput()
{
    std::cout<<"WARNING GetModuleOutput called for non ModuleOutput"<<std::endl;
    return new Undef();
}


