/*! ************************************************************************
    \file module.h
    \brief Headerfile Class Module describing the behaviour of a module.

    Describes the methods neeeded to setup a module of a circuit and work on
 * this module.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Tue May 30 2006

Modification history:
  * 05.07.2011 C.Villarraga added support for SHRA, ROR and ROL
  * 10.9.2014 them ham FCKtoBtor()

**************************************************************************/

#ifndef MODULE_H
#define MODULE_H
#include <string>
#include <sstream>
#include <fstream>
#include "circuitbuilder.h"
#include "component.h"
#include <vector>
#include <map>
#include "input.h"
#include "pseudoinput.h"
#include "constinput.h"
#include "add.h"
#include "and.h"
#include "concat.h"
#include "equivalence.h"
#include "ifthenelse.h"
#include "lessthan.h"
#include "minus.h"
#include "moduleinput.h"
#include "multiplier.h"
#include "not.h"
#include "or.h"
#include "shiftleft.h"
#include "shiftright.h"
#include "signextension.h"
#include "slice.h"
#include "unaryand.h"
#include "unaryor.h"
#include "unaryxor.h"
#include "write.h"
#include "xor.h"
#include "zeroextension.h"
#include "moduleoutput.h"
#include "pseudooutput.h"
#include "output.h"
#include "shiftrightarith.h"
#include "rotateright.h"
#include "rotateleft.h"
#include "undef.h"
#include "mux.h"
#include "decoder.h"
#include "../exceptions/defaultexception.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/fanoutmapmiss.h"
#include "../exceptions/faninnumberexception.h"
#include "SatI.h"

class Circuit;

/**
 * Class Module provides methods to set up a module and to work on it.
 * A circuit consists of several Modules. A Modules itself contains its
 * Components and methods to add new Components to itself as well as methods
 * to work on it.
 */
class Module
{
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Default Constructor.
     * Set up a Vector containing a Component of each
     * operation so that the number of needed fanin Components can easily be
     * determined.
     */
    Module();

    /**
     * Constructor saving a pointer to the circuit this Module is in.
     */
    Module(Circuit* circuit, ModuleId id);

    /**
     * Destructor
     */
    virtual ~Module();

    /**
     * Accessor Methods
     */
    /**
     * Add a new Component to the Circuit.
     * First of all the passed strings containing the names of the fanin
     * components have to mapped to the corresponding Components. After this
     * the new Component is created, linked to the found Components in the
     * fanin and added to the list mComponents in the following order:
     *
     * Inputs
     *
     * Functions*
     *
     * Outputs
     *
     *
     * *The Functions themselves are ordered in such a way that a Function is
     * placed behind every Component in its fanins and in front of every
     * component in its fanout. A reference to the created Component is stored
     * with the passed string for the fanout variable in a map (mapFanout) so
     * that we are able to map the corresponding inputs.
     * @return If the Component was successfully created true is returned. If
     * the Component can not yet be created (missing fanout) false is
     * returned.
     * @param type An integer representing the kind of the Component that
     * shall be added (see CircuitBuilder for the mapping of value and type).
     * @param fanin A vector containing the names of the fanin Components that
     * support the new component. For details on the correct ordering of the
     * fanin components please take a look at the subclass implementing the
     * Component you want to add.
     * @param fanout A string with the name of the fanout of the new component
     * that shall be added.
     * \todo replace map by hash_map
     */
    FckId AddComponent (int type, const std::vector<FckId>& fanin,
                       const std::string& fanout);

    /**
     * Fetch the Component at the given position.
     * @param pos The position of the component that shall be returned.
     * @return the component at the specified position.
     */
    Component* GetComponentByIndex(int pos)const{return mComponents[pos];}

    /**
     * Fetch a Component by its name.
     * @param name The name of the component that shall be returned.
     * @return the component with the given name.
     */
    Component* GetComponent(std::string name);

    /**
     * Fetch a Component by its ID.
     * @param name The ID of the component that shall be returned.
     * @return the component with the given name.
     */
    Component* GetComponent(FckId id);
	
	    /**
     * Fetch a Component by its id and time frame.
     * @param name The name of the component that shall be returned.
     * @return the component with the given id and time frame.
     */
    Component* GetComponentTimeFrame(FckId id, int timeFrame)
        {
// ------------------------------------------		return mModuleMap[module]->GetComponent(id);}
		return 0;
		}

    /**
     * Fetch a id by its id and time frame.
     * @param name The name of the component that shall be returned.
     * @return the id with the given id and time frame.
     */
    FckId GetIdTimeFrame(FckId id, int timeFrame)
        {
// ------------------------------------------		return mModuleMap[module]->GetComponent(id);}
		return 0;
		}
		
    /**
     * Check whether a Component is in the Module
     * @param name The name of the component that shall be checked for
     * @return true if a Component with the given name is in the Module
     */
    bool IsInModule(std::string name);

    /**
     * Check whether a Component is in the Module
     * @param name The ID of the component that shall be checked for
     * @return true if a Component with the given ID is in the Module
     */
    bool IsInModule(FckId id);

    /**
     * Set the values of the inputs of the circuit.
     * @param in the values the inputs shall take. The value at in[0] will be
     * applied to the Input at mComponent[0] and so on
     */
    void SetInputs(std::vector< std::vector< satlib::SatI::lValue> >& in);

    /**
     * Reorder the Module to get the Components in the correct order.
     * If the Components are not stored in the correct order the Module has to
     * be reordered
     */
    void Reorder();

    /**
     * perform a simulation of a component for a vector of inputs values.
     * @param component The number of the Component that shall be simulated
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    virtual void SimulateComponent // C.Villarraga 23.03.2011: Name changed from Simultate to SimulateComponent
            (int component,
             std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
    {mComponents[component]->Simulate(inpVals,outVals);}


    /**
     * C.Villarraga 23.03.2011: Created this method
     * Performs simulation of a module (module has to be already ordered)
     * using the model from the SAT solver. There is another faster alternative
     * to make this using the method Component::GetFanoutAssignment for every component
     * of the Module.
     * @param SatSolver The SAT solver that has the solution
     */
    virtual void SimulateProcessedCounterEx(satlib::SatI& SatSolver)
    {
    	// Gets Values for the Inputs from Counterexample
    	// and put them inside every input component
    	for(unsigned int i=0; i<mComponents.size(); i++){
    		if(mComponents[i]->GetMType()==INPUT){
    			std::vector<satlib::SatI::lValue> assign;
    			mComponents[i]->GetFanoutAssignment(SatSolver, assign); // gets values
    			mComponents[i]->SetInput(assign); // puts inside component
    		}
    	}

    	// Simulates the module (important! module has to be already ordered)
    	for(unsigned int i=0;i<mComponents.size();i++)
    		mComponents[i]->SimulateForward();
    }


    /**
     * Get the bitwidth of a variable.
     * @param id id of the variable the width shall be determined for
     * @return the width of the variable
     */
    int GetWidth(FckId id){return mIdMap[id]->GetMFanoutWidth();}

    /**
     * Get the number of Outputs of this Module
     * @return the number of Outputs of this Module
     */
    inline int GetNumOuts(){return mNumOuts;}

    /**
     * Get the number of Inputs of this Module
     * @return the number of Inputs of this Module
     */
    inline int GetNumInps(){return mNumInps;}

    /**
     * Get the Name of the Module
     * @return mModuleName
     */
    std::string GetModuleName(){return mModuleName;}

    /**
     * Get the Id of the Module
     * @return mModuleId
     */
    ModuleId GetModuleId(){return mModuleId;}

    /**
     * Set the Name of the Module
     * @param name The name of the Module
     */
    void SetModuleName(std::string name){mModuleName=name;}

    /**
     * Get the name of the outputs.
     * @return the name of the outputs
     */
    virtual inline void GetMOutputs(std::vector<std::string>& outs)
        {outs=mOutputs;}
    /**
         * Get the name of the outputs.
         * @return the name of the outputs
         */
    std::vector<std::string> GetMOutputs()
            {return mOutputs;}
    /**
     * Set the name vector of the outputs
     * @param name of output
     */
    virtual inline void SetMOutputs(std::string nameout)  //edited by Son Lam 10/10/2013
            {mOutputs.push_back(nameout);}
    /**
     * Operations
     */
    /**
     * Check whether the Module is complete.
     * Method to check whether a Module has been successfully created after
     * all Components have been added.
     * @return True if every Component has all required fanin Components and
     * at least one fanout Component. To check this the method isComplete of
     * every Component in mComponents is called.
     */
    bool IsComplete ();

    /**
     * Determine how many components the Module consists of.
     * @return the number of components in the Module
     */
    unsigned int Size(){return mComponents.size();}

    /**
     * Method to print the Components contained in the Module.
     * Prints all components of the Module with their type, fanin Components
     * and fanout name.
     */
    void Print(std::string path);
    /**
     * Ham chuyen doi FCK module -> Boolector module
     * tra ve BtorNode tuong ung voi output cua module (chi co duy nhat 1 output the hien cho tinh
     * dung dan cua khang dinh- assertion)
     */
    BtorNode * FCKtoBtor(Btor *btor);

    /**
     * Set whether a register updates its value on a falling or rising edge.
     * @param edge Can take the defined integer values FALLING(=1), RISING(=2)
     * or BOTH(=3) depending on the edge behaviour the register shall be set
     * to.
     * @param cmp a string containing the name of the PseudoOutput whose egde
     * shall be set.
     */
    inline void AddEdge(int edge, std::string cmp)
                                        {GetComponent(cmp)->SetOnEdge(edge);}

    /**
     * Set whether a register updates its value on a falling or rising edge.
     * @param edge Can take the defined integer values FALLING(=1), RISING(=2)
     * or BOTH(=3) depending on the edge behaviour the register shall be set
     * to.
     * @param cmp a FckId containing the id of the PseudoOutput whose egde
     * shall be set.
     */
    inline void AddEdge(int edge, FckId id)
                                        {GetComponent(id)->SetOnEdge(edge);}

    /**
     * Return the Circuit containing this module
     * @return The parent Circuit
     */
    inline Circuit* GetParentCircuit()
        {return const_cast<Circuit*>(mParent);}

    /**
     * Get the position of an Output of this Module.
     * @param name The name of the requested Output
     * @return the position of the requested Output
     */
    int GetOutput(std::string name);

    /**
     * Get the names of the Inputs to the Module
     * @return A vector containing the inputs
     */
    std::vector<Component*> GetModuleInputs();

    /**
     * Get the width of the output.
     * @param output The name of the output that's width is requested
     */
    unsigned int GetOutputWidth(std::string output);

    /**
     * Get the width of the output.
     * @param id The id of the output that's width is requested
     */
    unsigned int GetOutputWidth(FckId id);
    /**
     * Get the ID for the next component in the circuit.
     * Each component in the circuit gets a unique ID. This method determines 
     * the ID the next component shall get.
     * @return an integer value for the ID the next Component will get.
     */
    FckId GetNextId(){return mNextMId++;}
    /**
     * Get vector contains Components of module
     * @return mComponents
     */
    std::vector<Component*> GetmComponents() {return mComponents;}
    /**
     * Get hash map for the relation between signal names and fanout Components.
     * @return mComponentNameMap
     */
    std::map<std::string, Component*> GetmComponentNameMap (){return mComponentNameMap;}
    /**
     * Get hash map for the relation between signal names and fanout Components.
     * @return mIdMap
     */
    std::map<FckId, Component*> GetmIdMap(){return mIdMap;}

    /**
     * Set vector of Components
     * @param vector Components of Module
     */
    void SetmComponents (std::vector<Component*> mComp)
    {
    	mComponents=mComp;
    }
    void SetNetlistKind(int id){NetlistKind = id;}

/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     * A vector containing the Components of the Module.
     *
     * The Components are added to this vector in the following order:
     *
     * Inputs
     *
     * Functions*
     *
     * Outputs
     *
     * *The Functions itself are ordered in such a way that a Function is
     * placed behind every Component in its fanins and in front of every
     * Component in its fanout.
     */
    std::vector<Component*> mComponents;

    /**
     * The Name of this Module
     */
    std::string mModuleName;
    /**
     * The Name of this Module
     */
    ModuleId mModuleId;
    /**
     *
     */
    /**
     * identify the kind of Component net-lists: 0-design module , 1-property, 2-IPC computational model
     */
    int NetlistKind;
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */

     /**
      * Hash map for the relation between signal names and fanout Components.
      * We need a hash map to store this relation in order to map the fanins
      * of a new Component correctly to the Components in the fanin.
      */
    std::map<std::string, Component*> mComponentNameMap;

     /**
      * Hash map for the relation between signal names and fanout Components.
      * We need a hash map to store this relation in order to map the fanins
      * of a new Component correctly to the Components in the fanin.
      */
    std::map<FckId, Component*> mIdMap;

    /**
     * A Vector containing a Component of each Operation.
     * This Vector enables us to easily determine the number of needed
     * Fanin Components.
     */
    std::vector<Component*> mOperation;

    /**
     * Flag indicating whether this is the topmodule
     */
    bool mIsTopmodule;


    /**
     * The number of Outputs
     */
    int mNumOuts;
    /**
     * The names of the Outputs are stored in this vector to easiliy determine
     * how the Outputs of this Module shall be mapped.
     */
    std::vector<std::string> mOutputs;
    /**
     * The number of Inputs
     */
    int mNumInps;
    /**
     * Store the circuit containing this module
     */
    Circuit* mParent;
    /**
     * store the ID the next component added will get
     */
    FckId mNextMId;

    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
    /**
     * The reordering needs a method that can call itself recursively
     * @param cmp The Component that shall be added
     * @param knownFanouts The map containing the known fanout components
     * @param watchedFanouts The components watching for Fanouts
     * @param newMComponents the reordered vector containing the components
     * @return returns true if component was added successfully
     */
    bool RecReorder(const Component* cmp, std::map<FckId,
                    Component*>& knownIds,
                    std::map<FckId, Component*>& watchedIds,
                    std::vector<Component*>& newMComponents);
};
#endif //MODULE_H
