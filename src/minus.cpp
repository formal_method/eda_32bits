/*! ************************************************************************
    \file minus.cpp
    \brief Implementation of the Class Minus

    The class contained in this file implements the methods of the Function
 * Minus
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Thu Feb 09 2006

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout

**************************************************************************/

#include "minus.h"

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * A Minus has one fanin Component so initialize mRequiredFanin with 
 * 1.
 * Add the fanin component to the fanin of this Minus.
 * @param fanin A vector containing the Fanin Component of the Minus. 
 * @param fanout The string naming the output
 * @param id The ID of this Minus
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 */
Minus::Minus(std::vector<Component*>& fanin, std::string fanout, int id, 
                Module* module)
{
    mModule=module;
    SetMValid(1); //edited by Son Lam 17/9/2013
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMRequiredFanin(1);
    SetMType(MINUS);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have two fanin components so lets add these to our new Add Function.
    //First of all we allocate space for the Vector mFanin with undefined
    //fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //Therefore we use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to position i. 
    //The width of the output is equivalent to the width of the input
    mFanoutWidth=fanin[0]->GetMFanoutWidth();
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mcRequiredFanin;i++)
} //End Minus(vector<Component> fanin)  

/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin.
 */
void Minus::SimulateForward () 
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
} // End SimulateForward

/**
 * Perform a simulation of this Minus for a given vector of inputs
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void Minus::Simulate
        (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
         std::vector< satlib::SatI::lValue>&  outVals )    
{
    //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for
    //We reset mOutput
    outVals.clear();
    //We need a variable for a carry
    satlib::SatI::lValue carry=satlib::SatI::TRUE;
    for (unsigned int i=0; i<inpVals[0].size(); i++)
    {
        if (carry==satlib::SatI::TRUE)
        {
            outVals.push_back(inpVals[0][i]);
            if (inpVals[0][i]==satlib::SatI::TRUE)
                carry=satlib::SatI::FALSE;
        }
        else switch(inpVals[0][i])
        {
            case 0 : outVals.push_back(satlib::SatI::TRUE);
                     break;
            case 1 : outVals.push_back(satlib::SatI::FALSE);
                     break;
            case 5 : outVals.push_back(inpVals[0][i]);
                     break;
        } //end else switch
    } // End for (unsigned int i=0; i<inpVals.size(); i++)
} // End Simulate

/**
 * Create the CNF for this Minus.
 * @param solver the interface to the solver
 */
void Minus::CreateCnf(satlib::SatI& satSolver)
{
    //vector for the result literals
    std::vector<satlib::RtpLit> r;
    //vector for the literals of the input
    std::vector<satlib::RtpLit> a;
    //vector for the literals of the carries
    std::vector<satlib::RtpLit> carries;
    ////vector for the last column
    std::vector<satlib::RtpLit> lastColumn;
    assert(GetFanin().size()==1);
    GetFanoutRtpLits(r);
    GetFanin()[0]->GetFanoutRtpLits(a);
    assert(r.size()==a.size());

  
    for(unsigned i=0;i<r.size()-1;i++)
    {
        carries.push_back(satlib::RtpLit(mId,GetMFanoutWidth()+1+i));
    }
    // C.Villarraga 27.03.2011: comment added RtpLit(0,-1) is equal to TRUE value
    satSolver.makeHA(satlib::RtpLit(a[0].first,-a[0].second),
                     satlib::RtpLit(0,-1),r[0],carries[0]);
    for(unsigned i=1;i<a.size()-1;i++)
    {
        satSolver.makeHA(satlib::RtpLit(a[i].first,-a[i].second),
                         carries[i-1],r[i],carries[i]);
    }
    satSolver.makeXor(satlib::RtpLit(a[a.size()-1].first,
                      -a[a.size()-1].second),carries[a.size()-2],
                      r[a.size()-1]);
}

void Minus::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_neg(btor, mFanin[0]->GetBtorComp());
	BtorComp = comp;
}
