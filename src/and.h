/*! ************************************************************************
    \file and.h
    \brief Header file of the Class And.

    The class contained in this file defines the attributes and methods of the
 *  Function And.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Tue Jan 31 - Thu Feb 09 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef AND_H
#define AND_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "SatI.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"
#include "../exceptions/defaultexception.h"

/**
 * Class to describe the behaviour of the And Function.
 * This class implements the Function And derived from the class Function.
 */
class And : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The And Function needs 2 Fanins that can be handled equally.
     */
    And(){SetMRequiredFanin(2);SetMType(AND);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * An And has two fanin Components so initialize mRequiredFanin with 2. The
     * order of the fanin components doesn't matter for the And Function.
     * Add the fanin components to the fanin of this And.
     * @param fanin A vector containing the two Fanin Components of an And
     * @param fanout The string naming the output
     * @param id The ID of this And
     * @param module The Module the And shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     * @exception InputSizeException is thrown if the fanins do not have the same 
     * size -> extend to achieve this
     */
    And(std::vector<Component*>& fanin, std::string fanout, int id, Module* module);
    
    /**
     * Destructor
     */
    virtual ~And(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this And as a string (AND)
     * @return The string "AND"
     */
    inline std::string GetType(){return "AND";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();

    /**
     * Perform a simulation of this And for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );
    
    /**
     * create the CNF for this And Function
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);
    
//    void Setread_addr(FckId id){id_read_addr = id;}
//    FckId Getread_addr(){return id_read_addr;}


	void Create_BtorComp(Btor* btor);
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
 //   FckId id_read_addr;
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
};
#endif //AND_H

