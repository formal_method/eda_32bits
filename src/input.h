/*! ************************************************************************
    \file input.h 
    \brief Header file of the Input class
    
    Defines all attributes and methods an Input has
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Wed Feb 08 2006

Modification history:
 * 17.02.06 changed return type of AddFanin from bool to void. Whether an 
 * add is successful or not is now handled by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed the parameter fanin of the constructor to std::vector so 
 * that it can hold the name as well as the width of the input. 

**************************************************************************/

#ifndef INPUT_H
#define INPUT_H
#include <string>
#include "component.h"
#include <vector>
#include "undef.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class Input represents primary Inputs.
 * Defines the attributes and methods primary Inputs have.
 */
class Input : public Component {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor
     * An Input needs 0 Fanins but the constructor exspects 2 for the input 
     * name and the bitwidth of an input
     */
    Input(){SetMRequiredFanin(1);SetMType(INPUT);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * An Input has one fanin Component so initialize mRequiredFanin with 1.
     * Add the fanin Component to the fanin of this Input.
     * @param name A vector containing the Fanin Component of the Input
     * @param fanout The string naming the output
     * @param id The ID of the Input
     * @param module The Module the Input shall be added to
     * @exception GeneralFanException is thrown if the size of the fanin does
     * not match mRequiredFanin
     */
    Input(std::vector<FckId> bitwidth, std::string componentName, int id, 
          Module& module);
    
    /**
     * Destructor
     */
    virtual ~Input(){}
    /**
     * Accessor Methods
     */
    /**
     * Add a Component to the fanout of this Input.
     * @param cmp a pointer to the Component that shall be added.
     */
    void AddFanout (Component* cmp);

    /**
     * Add a Component to the fanout of this Function.
     * @param cmp a reference to the Component that shall be added.
     */
    void ClearFanout ()
    {
    	std::cout<<"da xoa fanout\n";
        mFanout.clear();
    }
        
    
    /**
     * Return the values of the bits in mOutput.
     * @return A vector with the current value of mOutput.
     */
    inline std::vector<satlib::SatI::lValue> GetOutput (){return mOutput;}
        
    
    /**
     * Return the Components in the Fanout.
     * @return Return a vector with the Components in the fanout of this 
     * Function.
     */
    inline std::vector<Component*> GetFanout (){return mFanout;}
        
    
    /**
     * Return the current value of the input.
     * The values of the input of an Input are equal to the values at its 
     * Output. Thus, return the Output.
     * @param input there is only one input thus input has to be 1.
     * @return GetOutput().
     */
    inline std::vector<satlib::SatI::lValue> GetInput (int input)
    {return GetOutput();}
    
    /**
     * Set the value of the input
     * @param input a vector of lValue containing the new input bits
     */
    virtual void SetInput(std::vector<satlib::SatI::lValue>& input);
    
    /**
     * Add a Component to the fanin of this Input at the position given.
     * Just needed because Component requires it. Of course an Input can not 
     * have a fanin Component.
     * @param cmp Reference to the Component that shall be added to the fanin.
     * @param pos The position at which this Component shall be added. 
     * @exception DoubledFaninException As an Input doesn't need a fanin 
     * Component this method always throws that exception.
     */
    virtual inline void AddFanin (Component* cmp, unsigned int pos)
    {
        std::string message="EXCEPTION: An Input doesn't need a fanin component.";
        throw DoubledFaninException(message, this, cmp, 0, pos);
    }
    
    /**
     * Change the Component in the fanin of this Component
     * @param cmpold Reference to the Component that shall be removed
     * @param cmpnew Reference to the Component that shall be added
     * @exception GeneralFaninException thrown if cmpold is not found in the
     * fanin of this component
     */
    virtual void ChangeFanin(Component* cmpold, Component* cmpnew)
    {
        std::string message="EXCEPTION: An Input doesn't have a fanin component.";
        throw DoubledFaninException(message, this, cmpold, new Undef(), 0);
    }
    
    /**
     * Set the name of this Input
     * @param name the name of the Input
     */
    virtual void SetMInputName(std::string name){mInputName=name;}
    
    /**
     * Get the name of this Input
     * @return the name of the Input
     */
    virtual std::string GetMInputName(){return mInputName;} 
    
    /**
     * Set the width of the Input
     * @param width The width of the Input
     */
    virtual inline void SetWidth(unsigned int width)
    {mFanoutWidth=width; mFaninWidth.push_back(width);}
    
    /**
     * Return the type of this component as a string (INPUT)
     * @return The string "INPUT"
     */
    inline std::string GetType(){return "INPUT";}
    
    /**
     * Operations
     */
     /**
     * Check whether the Input is complete.
     * @return True if the Input has mRequiredFanin(=0) fanin Components and
     * at least one fanout Component.
      */
    virtual bool IsComplete ();   
    
    /**
     * Update the value of mInput
     * Fetch the current assignment of the outputs of the fanin Components and
     * update mInput of this Component accordingly.
     */
    inline void UpdateInputs(){};
    
    /**
     * Perform a simulation of this Input for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals) {outVals=inpVals[0];}  
    
    /**
     * Create the CNF for this component.
     * Nothing to be done for an input
     * @param solver the interface to the solver
     */
    virtual void CreateCnf(satlib::SatI& solver){}

	void Create_BtorComp(Btor* btor);
    
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     * The value at the output
     */
    std::vector<satlib::SatI::lValue> mOutput;
    /**
     * Reference to the component in the fanout
     */
    std::vector<Component*> mFanout;
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * String zum Speichern des Namens des Einganges
     */
    std::string mInputName;
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    
    /**
     * Operations
     */
};
#endif //INPUT_H

