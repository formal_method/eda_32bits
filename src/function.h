/*! ************************************************************************
    \file function.h 
    \brief Header file of the Function class
    
    Defines all attributes and methods all Functions either have in common or 
 * have to implement
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Tue Jan 31 - Wed Feb 08 2006

Modification history:
 * 17.02.06 changed return type of AddFanin from bool to void. Whether an 
 * add is successful or not is now handled by exceptions

**************************************************************************/

#ifndef FUNCTION_H
#define FUNCTION_H
#include <string>
#include "component.h"
#include <vector>
#include "undef.h"
#include "../exceptions/doubledfaninexception.h"
#include "../exceptions/generalfanexception.h"


/**
 * Class Function represents bit vector functions.
 * Defines the attributes and methods all Functions either have in common or
 * have to implement.
 */
class Function : public Component {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     * not needed as Function is pure virtual
     */
    /**
     * Destructor
     */
    virtual ~Function(){}
    /**
     * Accessor Methods
     */
    /**
     * Add a component to the fanout of this Function.
     * All bit vector Functions have in common that they have at least one 
     * fanout Component. This Component is not yet known at the time the 
     * Function is constructed. So it has to be possible to add fanout 
     * Components later on.
     * @param cmp a reference to the Component that shall be added.
     */
    void AddFanout (Component* cmp);
    /**
     * Add a Component to the fanout of this Function.
     * @param cmp a reference to the Component that shall be added.
     */
    void ClearFanout ()
    {
        mFanout.clear();
    }
        
    
    /**
     * Return the values of the bits in mOutput.
     * @return A vector with the current value of mOutput.
     */
    inline std::vector<satlib::SatI::lValue> GetOutput (){return mOutput;}
        
    
    /**
     * Return the Components in the Fanout.
     * @return Return a vector with the Components in the fanout of this 
     * Function.
     */
    inline std::vector<Component*> GetFanout (){return mFanout;}
    
    /**
     * Returns the value of the Input bits at the position specified.
     * @param input the position in mInputs that shall be returned 
     * @return The value of the input bits at the position specified by input. 
     */
    inline std::vector<satlib::SatI::lValue> GetInput (int input){return mInputs[input];}
        
    
    /**
     * Return the Components in the Fanin.
     * @return This method here returns a vector containing the Components in 
     * the fanin.
     */
    inline std::vector<Component*> GetFanin () const {return mFanin;}
    
    /**
     * Add a Component to the fanin of this Component at the position
     * given.
     * @param cmp Reference to the Component that shall be added to the fanin.
     * @param pos The position at which this Component shall be added. 
     * @exception DoubledFaninException if the fanin that shall be added is 
     * already there or not needed
     */
    void AddFanin (Component* cmp, unsigned int pos);
    
    /**
     * Change the Component in the fanin of this Component
     * @param cmpold Reference to the Component that shall be removed
     * @param cmpnew Reference to the Component that shall be added
     * @exception GeneralFaninException thrown if cmpold is not found in the
     * fanin of this component
     */
    void ChangeFanin(Component* cmpold, Component* cmpnew);
    
    /**
     * Operations
     */
        
    
    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs.
     * 
     * Abstract method has to be implemented by subclasses. As the simulation
     * highly depends on the kind of Function.
     */
    virtual void SimulateForward ()=0;
    
    /**
     * Check whether the Function is complete.
     * @return True if the Function has mRequiredFanin fanin Components and
     * at least one fanout Components.
     */
    bool IsComplete ();
    
    /**
     * Update the value of mInput.
     * Fetch the current assignment of the outputs of the fanin Components and
     * update mInput of this Component accordingly.
     */
    void UpdateInputs();
        
    
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
 * Reference to the components in the fanout.
     */
    std::vector<Component*> mFanout;
    /**
     * The current value at the output.
     */
    std::vector<satlib::SatI::lValue> mOutput;
    /**
     * The value at the input
     */
    std::vector<std::vector<satlib::SatI::lValue> > mInputs;
    /**
     * Reference to the components in the Fanin
     */
    std::vector<Component*> mFanin;/**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    
    /**
     * Operations
     */
};
#endif //FUNCTION_H

