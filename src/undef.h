/*! ************************************************************************
    \file undef.h 
    \brief Header file of the Class Undef
    
    Sometimes we need an instance of a component without knowing its type as 
 * we don't want to instance component we use a type Undef
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Tue Feb 07 - Wed Feb 08 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 changed return type of AddFanin from bool to void. Whether an 
 * add is successful or not is now handled by exceptions

**************************************************************************/

#ifndef UNDEF_H
#define UNDEF_H
#include <string>
#include "component.h"
#include "circuitbuilder.h"
#include <vector>
#include "../exceptions/doubledfaninexception.h"

/**
 * Class to describe the behaviour of an Undef Component.
 * This class implements a Component that is Undefined.
 */
class Undef : public Component {

/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     */
	Undef(){SetMRequiredFanin(1);SetMType(UNDEF);SetMOutputName("");}


	/**
	 *	Constructor funct uses for connecting Verilator to FCK
	 */
	/**
	 * A constructor with a vector of fanin Components.
	 * A UNDEF is constructed as a INPUT
	 * An Undef has one fanin Component so initialize mRequiredFanin with 1.
	 * Add the fanin Component to the fanin of this Undef.
	 * @param name A vector containing the Fanin Component of the Undef
	 * @param fanout The string naming the output
	 * @param id The ID of the Undef
	 * @param module The Module the Input shall be added to
	 */
	Undef(std::vector<FckId> fanin, std::string fanout, int id, Module* module){
	    //The Undef doesn't have a fanin component
	    SetMRequiredFanin(0);
	    SetMType(UNDEF);
	    SetMValid(1); //edited by Son Lam 22/9
	    Setvalidname(1);
	    Setvarref(0);
	    Setignore(0);
	    Setarray_index(-1);
	    //the Undef name is stored in fanout
	    SetMOutputName(fanout);
	    mId=id;
	    mModule=module;
	    //The width has to extracted from the string before it can be set.
	    SetWidth(fanin[0]);
	}


    /**
     * Destructor
     */
    virtual ~Undef(){}
    /**
     * Accessor Methods
     */
     /**
      * Return the type of this component as a string (UNDEF)
      * @return The string "UNDEF" 
      */
    inline std::string GetType(){return "UNDEF";}   
    
    /**
      * Set the width of the Input
      * @param width The width of the Input
      */
    virtual inline void SetWidth(unsigned int width)
          {mFanoutWidth=width; mFaninWidth.push_back(width);}

    /**
     * Return the values of the bits in mOutput.
     * @return A vector with the current value of mOutput (undefined).
     */
    inline std::vector<satlib::SatI::lValue> GetOutput (){return undefined;}
        
    
    /**
     * Return the current value of the input.
     * The values of the input of an Input are equal to the values at its 
     * Output. Thus, return the Output.
     * @param input doesn't really matter as this returns undefined in any way
     * @return An undefined vector
     */
    inline std::vector<satlib::SatI::lValue> GetInput (int input)
        {return undefined;}
    
    /**
     * Operations
     */
    
    /**
     * Check whether the Undef is complete.
     * @return false as this Component can't be complete.
     */
    bool IsComplete (){return false;}   
        
    /**
     * Update the value of mInput.
     * Do nothing.
     */
    inline void UpdateInputs(){}
    
    /**
     * Add a Component to the fanin of this Undef at the position given.
     * @param cmp Reference to the Component that shall be added to the fanin.
     * @param pos The position at which this Component shall be added. 
     * @exception DoubledFaninException As no Fanin is needed always throw 
     * this exception
     */
    inline void AddFanin (Component* cmp, unsigned int pos)
    {
        std::string message="EXCEPTION: Fanin for Undef";
        throw DoubledFaninException(message, this, cmp, new Undef(), pos);
    }
    
    /**
     * Change the Component in the fanin of this Undef.
     * @param cmpold Reference to the Component that shall be removed
     * @param cmpnew Reference to the Component that shall be added
     * @exception GeneralFaninException always thrown as there is no fanin to 
     * change.
     */
    virtual void ChangeFanin(Component* cmpold, Component* cmpnew)
    {
        std::string message=
                "EXCEPTION: An Undef doesn't have a fanin component.";
        throw DoubledFaninException(message, this, cmpold, new Undef(), 0);
    }
    
    /**
     * Add a component to the fanout of this Undef.
     * Don't do anything.
     * @param cmp a reference to the Component that shall be added.
     */
    inline void AddFanout (Component* cmp){
    	mFanout.push_back(cmp);
    }
    /**
     * Add a Component to the fanout of this Function.
     * @param cmp a reference to the Component that shall be added.
     */
    void ClearFanout (){mFanout.clear();}

    
    /**
     * Perform a simulation of this Undef for a given vector of inputs.
     * Don't do anything.
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    inline void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals ){}
    
    /**
     * Create the CNF for this Undef.
     * Don't do anything.
     * @param solver the interface to the solver
     */
    inline void CreateCnf(satlib::SatI& solver){}
    
    /**
     * Return the Components in the Fanout.
     * @return Return a vector with the Components in the fanout of this
     * Function.
     */
    inline std::vector<Component*> GetFanout (){return mFanout;}


/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     * Reference to the component in the fanout
     */
    std::vector<Component*> mFanout;
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    std::vector<satlib::SatI::lValue> undefined;
    /**
     * 
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
};
#endif //ADD_H

