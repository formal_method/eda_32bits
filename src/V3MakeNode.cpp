// -*- mode: C++; c-file-style: "cc-mode" -*-
//*************************************************************************
// DESCRIPTION: Verilator:
//
//*************************************************************************
#include "config_build.h"
#include "verilatedos.h"
#include <cstdio>
#include <cstdarg>
#include <unistd.h>
#include <algorithm>
#include <map>
#include "V3Global.h"
#include "V3MakeNode.h"
#include "V3Ast.h"

class Node;
int id(0);
Node* gRootNode = new Node();

class NodeVisitor : public AstNVisitor {
// Mark every node in the tree
private:
// NODE STATE
// Nothing!
// This may be called deep inside other routines
//
// so userp and friends may not be used
// VISITORS
virtual void visit(AstNode* nodep, AstNUser*) {

	// convert V3AstNode to Node structure
	// each AstNode visited create a Node
	Node* newNode =  new Node(++id,(string)(nodep->typeName())+"::"+nodep->name());
	nodep->pNode = newNode;
	if((string)(nodep->typeName())=="NETLIST") gRootNode = nodep->pNode; 	// NETLIST node is the top

	nodep->iterateChildren(*this); // loop with each children
}
virtual void visit(AstIf* nodep, AstNUser*)
{
    //std::cout<<nodep->typeName()<<" ------ \n";
    Node* newNode =  new Node(++id,(string)(nodep->typeName())+"::"+nodep->name());
	nodep->pNode = newNode;
    nodep->iterateChildren(*this);
}


public:
	// CONSTRUCTORS
	NodeVisitor(AstNetlist* nodep) {
		nodep->accept(*this);
	}
	// Destructor
	virtual ~NodeVisitor() {}
};

// implement traverseNode() method
void V3MakeNode::makeNode(AstNetlist* nodep) {
	//UINFO(9,__FUNCTION__<<": "<<endl);


	// visit all node
	NodeVisitor mvisitor (nodep);


	// make tree from AstNode
	makeTree(nodep);

	// make dot file
	makeDotFile(gRootNode);
}

// implement printNode() funct
void 	makeTree(AstNode* pnode){
	if(!pnode) return;

	// creating edge
	if(pnode->op1p()){
	    pnode->pNode->CreatEdge(pnode->op1p()->pNode); // creating edge between current node and m_op1p child node

	    // creating edge between current node and all of node next to m_op1p child node
	    if(pnode->op1p()->nextp()){
			AstNode* tmpNode = pnode->op1p()->nextp();
			while(tmpNode){		// loop with all next to node
				pnode->pNode->CreatEdge(tmpNode->pNode);
				tmpNode = tmpNode->nextp();
			}
		}
		makeTree(pnode->op1p());	// loop with m_op1p child node
	}
	if(pnode->op2p()){
	    pnode->pNode->CreatEdge(pnode->op2p()->pNode);
		if(pnode->op2p()->nextp()){
			AstNode* tmpNode = pnode->op2p()->nextp();
			while(tmpNode){
				pnode->pNode->CreatEdge(tmpNode->pNode);
				tmpNode = tmpNode->nextp();
			}
		}
		makeTree(pnode->op2p());
	}
	if(pnode->op3p()){
	    pnode->pNode->CreatEdge(pnode->op3p()->pNode);
		if(pnode->op3p()->nextp()){
			AstNode* tmpNode = pnode->op3p()->nextp();
			while(tmpNode){
				pnode->pNode->CreatEdge(tmpNode->pNode);
				tmpNode = tmpNode->nextp();
			}
		}
		makeTree(pnode->op3p());
	}
	if(pnode->op4p()){
	    pnode->pNode->CreatEdge(pnode->op4p()->pNode);
		if(pnode->op4p()->nextp()){
			AstNode* tmpNode = pnode->op4p()->nextp();
			while(tmpNode){
				pnode->pNode->CreatEdge(tmpNode->pNode);
				tmpNode = tmpNode->nextp();
			}
		}
		makeTree(pnode->op4p());
	}

	//	loop with node next to current node
	makeTree(pnode->nextp());
}


void makeDotFile(Node* prNode){
	ofstream output("..Test/Results/Circuit/ast.dot",ios::out);
  	if(!output){
  		cout<<"File can not open!";
  		//exit(1);
		return;
  	}
  	output<<"digraph G{\n";

  	// call DFS funct
  	DFS(output,prNode);

  	output<<"\n}";
  	output.close();
}

