
; tex-stuff
; (require 'tex-site)
; (require 'bib-cite)
;;  (setq TeX-auto-save t)
;;  (setq TeX-parse-self t)
;;  (setq-default TeX-master nil)
;;  (setq TeX-view-style '(("^a5$" "xdvi %d -paper a5")
;;  			    ("^landscape$" "xdvi %d -paper a4r -s 6")
;;  			    ;; The latest xdvi can show embedded postscript.
;;  			    ;; If you don't have that, uncomment next line.
;;  			    ;; ("^epsf$" "ghostview %f")
;;  			    ("." "xdvi %d")))

(global-set-key [f11] 'next-buffer)
(global-set-key [f9] 'bury-buffer)
(global-set-key [f13] 'what-cursor-position)
(global-set-key [f14] 'undo)
(global-set-key [f16] 'clipboard-kill-ring-save)
(global-set-key [f18] 'clipboard-yank)
(global-set-key [f20] 'clipboard-kill-region)

(global-set-key [f3] 'previous-error)
(global-set-key [f4] 'next-error)
(global-set-key [f5] 'compile)
(global-set-key [f6] 'remote-compile)
(global-set-key [f7] 'delete-other-frame)
(global-set-key [f10] 'undo)
(global-set-key [?\C-x ?\C-k] 'kill-this-buffer)


