#ifndef PSLPROERTY_H2
#define PSLPROERTY_H2

#include "module.h"
#include "circuit.h"

class PslProperty : public Module
{
    public:
        /**
                      * Initialize the property, default constructor
                     */
        PslProperty();
	~PslProperty(){}
        
        /**
                      * return the id of the signal name in timeframe timeFrame
                      * @return id of the signal
                      * @param name the name of the signal 
                      * @param timeFrame the timeframe of the signal 
                      * @param widthCircuit circuit to exract the bitwidth of the component
                      */
        FckId GetVariable(std::string name, int timeFrame, Circuit* widthCircuit);
        /**
                      * return the map mVariables
                      */
        std::map<std::pair<std::string,int>,FckId>* GetmVariable() {return &mVariables;};
        /**
                      * return the id of the constant name
                      * @return id of the constant
                      * @param name the value of the constant in binary or integer form 
                      */
        FckId GetConstant(std::string name);
        
        /**
                      * Shift all components of the property by shamt timeframes (needed, if timeframe<0)
                      * @param shamt shift amount
                      */
        void ShiftTimeTable(int shamt);
        
        /**
                      * Get the maximum timeframe in the property
                      * @return the maximum timeframe
                      */
        int GetMaxTimeFrame();
        void SetMaxTimeFrame(int timepoint) {mMaximumTimeFrame = timepoint;}
        /**
                      * increase and then return the maximum timeframe in the property
                      */
        int IncreaseMaxTimeFrame(int shamt);

	/**
                      * Get the minimum timeframe in the property
                      * @return the minimum timeframe
                      */
        int GetMinTimeFrame();
	
        /**
                      * Add a assumption
                      * @param id the id of the assumption 
                      */
        void AddAssumption(FckId id);
        
        /**
                      * Add a commitment
                      * @param id the id of the commitment 
                      */
        void AddCommitment(FckId id);
        
        /**
                      * All assumptions and commitments are added. Combine these to the property and maybe begin a new property
                      */
        FckId FinishProperty();
        
        /**
                      * Name the property
                       * @param name the name of the property
                      */
        void SetName(std::string name);
        
        /**
                      * Read the name of the property
                      * @return name of the property
                      */
        std::string GetName();
	
	/**
                      * Return the mapping of the timeframes
                      * @return map of timeframes
                      */
	
	std::map<FckId,std::pair<FckId, int> > GetTimeFrameMap();

		/**
                      * Clone a component at t-1
                      * @return the new component
                      */
	
	FckId CloneComponentTimeFrameEarlier(FckId id);
	std::vector<std::string>* GetmInputNames (){return &mInputNames;}
	std::vector<FckId>* GetmLocalVarConditions () { return &mLocalVarConditions; }

    protected:

    private:
		/**
					  * map to store the name of inputs of the property
					  */
		std::vector<std::string> mInputNames;
        /**
                      * map to store the known inputs of the property
                      */
        std::map<std::pair<std::string,int>,FckId> mVariables;
        
        /**
                      * map to store the known constants of the property
                      */
        std::map<std::string,FckId> mConstants;
        
        /**
                      * the maximum timeframe of the property
                      */
        int mMaximumTimeFrame;
        
        /**
                      * vector to store the known assumptions of the property
                      */
        std::vector<FckId> mAssumptions;
        
        /**
                      * vector to store the known commitments of the property
                      */
        std::vector<FckId> mCommitments;

        /**
                      * vector to store the condition to assign local variable of the property
                      */
        std::vector<FckId> mLocalVarConditions;
        /**
                      * the name of the property
                      */
        std::string mName;
	/**
                      * the mapping of the timepoints
                      */
	std::map<FckId,std::pair<FckId, int> > mTimeFrameMap;
};
#endif
