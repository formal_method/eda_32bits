/*! ************************************************************************
    \file xor.cpp 
    \brief Implementation of the Class Xor

    The class contained in this file implements the methods of the Function
 * Xor
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Thu Feb 09 2006 

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout
 * 26.10.06 SatI::RtpLit is replaced with RtpLit

**************************************************************************/

#include "xor.h"

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * A Xor has two fanin Components so initialize mRequiredFanin with 2. The
 * order of the fanin components doesn't matter for the Xor Function.
 * Add the fanin components to the fanin of this Xor.
 * @param fanin A vector containing the two Fanin Components of the Xor.
 * @param fanout The string naming the output
 * @param id The ID of the Xor
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 */
Xor::Xor(std::vector<Component*>& fanin, std::string fanout, int id, 
          Module* module)
{
    mModule=module;
    SetMRequiredFanin(2);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMType(XOR);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have two fanin components so lets add these to our new And Function.
    //First of all we allocate space for the Vector mFanin with undefined
    //fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //Therefore we use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to position i. 
    //In this implementation we require all fanins and the result to have the
    //same width. Therefore we set the width of the output here
    mFanoutWidth=fanin[0]->GetMFanoutWidth();
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that the fanin has the correct size
        if (mFanoutWidth!=fanin[i]->GetMFanoutWidth())
        {
            std::string message="EXCEPTION: The width of the fanins is not";
            message +=" equivalent."; 
            throw InputSizeException(message, this, fanin);
        }   
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mcRequiredFanin;i++)
} //End Xor(vector<Component> fanin)

/**
 * Methods
 */

/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin.
 * @exception DefaultException is thrown if the result can not be determined. 
 * It should be impossible that this exception occurs
 */
void Xor::SimulateForward () 
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
} //End SimulateForward

/**
 * Perform a simulation of this Xor for a given vector of inputs
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void Xor::Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
{
    //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for
    //We reset outVals
    outVals.clear();
    //We want both inputs to have the same size as mFaninWidth 
    try
    {
        if (inpVals[0].size()!=inpVals[1].size()
            ||inpVals[0].size()!=mFaninWidth[0])
        {
            //The two inputs do not have the same width so we use a sign 
            //extension in order to be able to add the to inputs. Throw an 
            //Exception to inform the user. 
            std::string message = "EXCEPTION: The inputs of this add function "; 
            message+="do not have the same width. I will use a sign extension.";
            throw InputSizeException(message, this, mFanin);
        } // End if (mInputs[0].size()!=mInputs[1].size())
    } // End try
    //We handle the input size exception directly by zero extending the 
    //smaller input.
    catch(InputSizeException e)
    {
        //print the message to let the user know that we use a sign extended
        //input
        e.GetMessage();
        //Determine the smaller input; store in the variables smaller and 
        //bigger
        int smaller, bigger;
        if (inpVals[0].size()<inpVals[1].size())
        {
            smaller=0;
            bigger=1;
        }
        else
        {
            smaller=1;
            bigger=0;
        }
        //As long as smaller is smaller than bigger push FALSE to smaller
        while (inpVals[smaller].size()!=inpVals[bigger].size())
            inpVals[smaller].push_back(satlib::SatI::FALSE);
    } // End catch(InputSizeException e)
    //We consider each input bit separately from the lowest bits to the 
    //highest one.
    for (unsigned int i=0;i<inpVals[0].size();i++)
    {
        if(inpVals[0][i]==satlib::SatI::DONTCARE 
           || inpVals[1][i]==satlib::SatI::DONTCARE)
        {
            outVals.push_back(satlib::SatI::DONTCARE);
        }
        else if(inpVals[0][i]== inpVals[1][i])
        {
            outVals.push_back(satlib::SatI::FALSE);
        }
        else outVals.push_back(satlib::SatI::TRUE);
    }
}//End Simulate

/**
 * create the CNF for this component.
 * @param solver the interface to the solver
 */
void Xor::CreateCnf(satlib::SatI& satSolver)
{
    //vector for the result literals
    std::vector<satlib::RtpLit> r;
    //vector for the literals of input a
    std::vector<satlib::RtpLit> a;
    //vector for the literals of input b
    std::vector<satlib::RtpLit> b;
    assert(GetFanin().size()==2);
    //get the literals
    GetFanoutRtpLits(r);
    GetFanin()[0]->GetFanoutRtpLits(a);
    GetFanin()[1]->GetFanoutRtpLits(b);
    assert(r.size()==a.size());
    assert(r.size()==b.size());
    //build the Xor bit wise
    for(unsigned i=0;i<r.size();i++)
    {
        satSolver.makeXor(a[i],b[i],r[i]);
    }
}


void Xor::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_xor(btor, mFanin[0]->GetBtorComp(), mFanin[1]->GetBtorComp());
	BtorComp = comp;
}
