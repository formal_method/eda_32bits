/*! ************************************************************************
    \file rotateleft.cpp
    \brief Implementation of the Class RotateRight

    The class contained in this file implements the methods of the Function
    RotateLeft
    \author C.Villarraga

    \copyright (c) University of Kaiserslautern 05.07.2011

Modification history:


**************************************************************************/

#include "rotateleft.h"

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * A RotateLeft has two fanin Components so initialize mRequiredFanin with 2.
 * The first input is the data input to be rotated the second input specifies
 * the number of bits that have to be rotated.
 * Add the fanin components to the fanin of this RotateLeft.
 * @param fanin A vector containing the two Fanin Components of the RotateLeft
 * @param fanout The string naming the output
 * @param id The ID of this RotateLeft
 * @exception GeneralFanException is thrown if the size of the fanin does not
 * match mRequiredFanin
 */
RotateLeft::RotateLeft(std::vector<Component*>& fanin, std::string fanout,
                        int id, Module* module)
{
    mModule=module;
    SetMRequiredFanin(2);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMType(ROL);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is";
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have two fanin components so lets add these to our new RotateLeft
    //Function. First of all we allocate space for the Vector mFanin with
    //undefined fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //Therefore we use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to position i.
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mRequiredFanin;i++)
    //set the width of the fanout to the width of the data input
    mFanoutWidth=mFanin[0]->GetMFanoutWidth();
} //End ShiftLeft(vector<Component> fanin)

/**
 * Methods
 */

/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin.
 * @exception Throws Default Exception in the case of an impossible lValue
 * value (can never occur)
 * \todo better handlign of unspecified input (fewer undefined output bits)
 */
void RotateLeft::SimulateForward ()
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
}//End SimulateForward

/**
 * C.Villarraga 01.07.2011. ToDo: this simulation is wrong ... it has to
 * agree with the CreateCNF method (below) so far it performs logical shift
 * Perform a simulation of this RotateLeft for a given vector of inputs
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void RotateLeft::Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
{
   //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!";
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!";
            throw InputSizeException(msg, this, mFanin);
        } //End if
    } //End for
    //We reset mOutput
    outVals.clear();
    //First we have to calculate how many bits have to be shifted.
    //store this value in a new variable shiftby. The value is calculated by
    //adding up the individual bits multilpied with the magnitude of the
    //column. The value shiftby=-1 is used to indicate an undefined amount of
    //bits to be shifted.
    int shiftby=0;
    for (unsigned int i=0;i<inpVals[1].size() && shiftby!=-1;i++)
    {
        //retrieve the value of the current bit for FALSE do nothing, for
        //TRUE add 2^i (implemented by the shiftleft 1<<i) to shiftby. In
        //the case of DONTCARE the shift is unspecified to indicate this set
        //shiftby to -1 and we leave the for loop.
        int current = inpVals[1][i];
        switch (current)
        {
            case 1: shiftby+=1<<i;
                    break;
            case 0: break;
            case 5: shiftby=-1;
            		break;
            //default case is not possible
            default:
            {
                std::string message="EXCEPTION: Reached an ";
                message+="impossible lValue value";
                throw DefaultException(message);
            }
        }  //End switch (current)
    } //End for (int i=0;i<inpVals[1].size();i++)

    if (shiftby>=0) // is the amount of bits to be shifted specified?
    {
    	// normalize rotation amount
    	unsigned int rotateby = shiftby % (inpVals[0].size());
    	for (unsigned int i=0;i<rotateby;i++)
            outVals.push_back(inpVals[0][inpVals[0].size()-rotateby+i]);

    	for(unsigned int i=rotateby; i<inpVals[0].size() ;i++)
    		outVals.push_back(inpVals[0][i-rotateby]);
    } // End if (shiftby>0)
    else
    {	// ToDo: is the code in this else correct???
        //We have a unspecified value by which the data input shall be shifted
        //This results in as many FALSE values at the beginning as the data
        //input has. From the first TRUE or DONTCARE on the output is set to
        //DONTCARE.
        //determine the highest input that is TRUE, by counting down
        //highTrue until a bit that is TRUE has been discovered or the last
        //bit is reached
        int highTrue=inpVals[0].size()-1;
        for (;inpVals[0][highTrue]!=satlib::SatI::TRUE && highTrue>0;highTrue--)
        //All bits lower or equal to highTrue get the value l_Undef
            for (int i=0;i<=highTrue;i++)
        {
            outVals.push_back(satlib::SatI::DONTCARE);
        } // End for(int i=0;i<=highTrue;i++)
        for (unsigned int i=highTrue+1;i<inpVals[0].size();i++)
        {
            outVals.push_back(satlib::SatI::FALSE);
        } //End for (unsigned int i=highTrue;i<inpVals[0].size();i++)
    } // End else
} // end Simulate


// Creates the CNF for RotateLeft.
// @param solver the interface to the solver
void RotateLeft::CreateCnf(satlib::SatI& satSolver)
{
    //vector for the literals of input a
    std::vector<satlib::RtpLit> a;
    //vector for the literals of input b
    std::vector<satlib::RtpLit> b;
    //vector for the literals of the result
    std::vector<satlib::RtpLit> r;
    assert(GetFanin().size()==2);
    GetFanoutRtpLits(r);
    GetFanin()[0]->GetFanoutRtpLits(a);
    GetFanin()[1]->GetFanoutRtpLits(b);
    assert(r.size()==a.size());
    assert(b.size()>0);
    std::vector<satlib::RtpLit> ri(r.size());
    for (unsigned i=0,shiftBy=1; i<b.size(); shiftBy*=2,i++) {
        if (i<b.size()-1) { // create new variables (ri) for intermidate stages
            for (unsigned k=0; k<ri.size(); k++)
                ri[k]=satlib::RtpLit(mId,GetMFanoutWidth()+1+k+i*GetMFanoutWidth());
        }
        else { // for last stage use result variables directly (r)
            for(unsigned k=0; k<ri.size(); k++)
                ri[k]=r[k];
        }
        // create multiplexing stage
        makeRol(satSolver,b[i],ri,a,shiftBy);
        for (unsigned k=0; k<ri.size(); k++)
           a[k]=ri[k];
    }
}


// Creates Cnf for a multiplexing stage. "inp" is shifted to the right
// "rotateBy" positions (after normalizing) when "sel" is active.
// Shifted out bits are inserted in the LSBs.
// @param solver: the interface to the solver
// @param res: RtpLit for the result
// @param inp: RtpLit for the input
// @param sel: determines whether shift is done or not.
void RotateLeft::makeRol (satlib::SatI& satSolver, satlib::RtpLit sel,
                              std::vector<satlib::RtpLit>& res,
                              std::vector<satlib::RtpLit>& inp,
                              unsigned rotateBy)
{
	// normalize rotation amount
	unsigned rotateNorm = rotateBy % res.size();

	// LSBs: shifted out bits are multiplexed
	for (unsigned k=0; k<rotateNorm; k++) {
		unsigned index = res.size() - rotateNorm + k;
		satSolver.makeIte(sel,inp[index],inp[k],res[k]);
	}

	// MSBs: shift is performed
	for (unsigned k=rotateNorm; k<res.size(); k++) {
		// res[k]=ite(sel,inp[k-shiftBy],inp[k])
		satSolver.makeIte(sel,inp[k-rotateNorm],inp[k],res[k]);
	}

}
