/*! ************************************************************************
    \file shiftright.cpp
    \brief Implementation of the Class ShiftRight

    The class contained in this file implements the methods of the Function
 * ShiftRight
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Fri Feb 10 2006

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 16.03.06 changed constructor to aquire and set the width of fanin and fanout
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout

**************************************************************************/

#include "shiftright.h"

/**
 * Constructors/Destructors
 */
/**
 * A constructor with a vector of fanin Components.
 * A ShiftRight has two fanin Components so initialize mRequiredFanin with 2. 
 * The first input is the data input to be shifted the second input specifies 
 * the number of bits that have to be shifted.
 * Add the fanin components to the fanin of this ShiftRight.
 * @param fanin A vector containing the two Fanin Components of the ShiftRight
 * @param fanout The string naming the output
 * @param id The ID of this ShiftRight
 * @exception GeneralFanException is thrown if the size of the fanin does not 
 * match mRequiredFanin
 */
ShiftRight::ShiftRight(std::vector<Component*>& fanin, std::string fanout, 
                        int id, Module* module)
{
    mModule=module;
    SetMRequiredFanin(2);
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMType(SHR);
    SetMOutputName(fanout);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have two fanin components so lets add these to our new ShiftRight
    //Function. First of all we allocate space for the Vector mFanin with 
    //undefined fanin Components
    Undef* pUndef = new Undef();
    mFanin.resize(mRequiredFanin, pUndef);
    //Therefore we use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to position i. 
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mRequiredFanin;i++)
    //set the width of the fanout to the width of the data input
    mFanoutWidth=mFanin[0]->GetMFanoutWidth();
} //End ShiftLeft(vector<Component> fanin)

/**
 * Methods
 */

/**
 * Perform a forward simulation from the Inputs to the Outputs.
 * Update the values of mOutput according to the current values of
 * mInputs. For correct simulation results first of all the values of
 * mInputs have to be updated by fetching the result (mOutput) of the
 * operations in the fanin.
 * @exception Throws Default Exception in the case of an impossible lValue  
 * value (can never occur)
 * \todo better handlign of unspecified input (fewer undefined output bits)
 */
void ShiftRight::SimulateForward () 
{
    //Update the values of mInputs
    UpdateInputs();
    //call Simulate with the mInputs as input and mOutput as Output
    Simulate(mInputs, mOutput);
}//End SimulateForward

/**
 * C.Villarraga 01.07.2011. ToDo: check this simulation ... it has to
 * agree with the CreateCNF method (below) ... now it seems to be buggy
 * Perform a simulation of this ShiftRight for a given vector of inputs
 * @param inpVals The input values the simulation shall be performed for
 * @param outVals The resulting output of the simulation
 */
void ShiftRight::Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals )
{
   //Check whether the number of inputs is correct
    if (inpVals.size()!=mRequiredFanin)
    {
        std::string msg = "EXCEPTION: The number of inputs passed for the ";
        msg+="Simulation is not equal to the number of needed fanin components!"; 
        throw InputSizeException(msg, this, mFanin);
    } // End if
    //Check whether each input has the correct number of bits 
    for (unsigned int i=0;i<inpVals.size();i++)
    {
        if (inpVals[i].size()!=mFaninWidth[i])
        {
            std::string msg = "EXCEPTION: The number of bits passed for the ";
            msg+="Simulation is not equal to the number of fanin bits this ";
            msg+="component requires!"; 
            throw InputSizeException(msg, this, mFanin);
        } //End if   
    } //End for
    //We reset mOutput
    outVals.clear();
    //First we have to calculate how many bits have to be shifted.
    //store this value in a new variable shiftby. The value is calculated by 
    //adding up the individual bits multilpied with the magnitude of the 
    //column. The value shiftby=-1 is used to indicate an undefined amount of 
    //bits to be shifted.
    int shiftby=0;
    for (unsigned int i=0;i<inpVals[1].size() && shiftby!=-1;i++)
    {
        //retrieve the value of the current bit for FALSE do nothing, for 
        //TRUE add 2^i (implemented by the shiftleft 1<<i) to shiftby. In 
        //the case of DONTCARE the shift is unspecified to indicate this set 
        //shiftby to -1 and we leave the for loop.
        int current = inpVals[1][i];
        switch (current)
        { 
            case 1: shiftby+=1<<i;
            		break;
            case 0: break;
            case 5: shiftby=-1;
            		break;
            //default case is not possible
            default:
            {
                std::string message="EXCEPTION: Reached an ";
                message+="impossible lValue value";
                throw DefaultException(message);
            }
        }  //End switch (current)
    } //End for (int i=0;i<inpVals[1].size();i++)
    
    //  Is the amount of bits that have to be shifted specified?  
    if (shiftby>=0)
    {
	    // C. Villarraga 03.08.2011 size normalization sets the size of the vector when the number of bits
	    // to shift is greater than the size vector
    	if ((unsigned)shiftby>inpVals[0].size()) {shiftby=inpVals[0].size();}

        //The bits 0 to shiftby-1 are removed from the data input. 
        // Add the bits from shiftby on
        for(unsigned int i=shiftby; i<inpVals[0].size();i++)
           outVals.push_back(inpVals[0][i]);
    
        // now shiftby bits are missing add these with the value FALSE
        for (int i=0;i<shiftby;i++)
           outVals.push_back(satlib::SatI::FALSE);

    } // End if (shiftby>0)
    else
    {
        //We have a unspecified value by which the data input shall be shifted
        //This results in as many FALSE values at the beginning as the data 
        //input has. From the first TRUE or DONTCARE on the output is set to 
        //DONTCARE.
        //determine the highest input that is TRUE, by counting down 
        //highTrue until a bit that is TRUE has been discovered or the last 
        //bit is reached
        int highTrue=inpVals[0].size()-1;
        for (;inpVals[0][highTrue]!=satlib::SatI::TRUE && highTrue>0;highTrue--)
        //All bits lower or equal to highTrue get the value l_Undef
            for (int i=0;i<=highTrue;i++)
        {
            outVals.push_back(satlib::SatI::DONTCARE);
        } // End for(int i=0;i<=highTrue;i++)
        for (unsigned int i=highTrue+1;i<inpVals[0].size();i++)
        {
            outVals.push_back(satlib::SatI::FALSE);
        } //End for (unsigned int i=highTrue;i<inpVals[0].size();i++)
    } // End else 
} // end Simulate


// C.Villarraga 01.07.2011. Method fixed, last implementation was buggy
// Creates the CNF for ShiftRight.
// @param solver the interface to the solver
void ShiftRight::CreateCnf(satlib::SatI& satSolver)
{
    //vector for the literals of input a
    std::vector<satlib::RtpLit> a;
    //vector for the literals of input b
    std::vector<satlib::RtpLit> b;
    //vector for the literals of the result
    std::vector<satlib::RtpLit> r;
    assert(GetFanin().size()==2);
    GetFanoutRtpLits(r);
    GetFanin()[0]->GetFanoutRtpLits(a);
    GetFanin()[1]->GetFanoutRtpLits(b);
    assert(r.size()==a.size());
    assert(b.size()>0);
    std::vector<satlib::RtpLit> ri(r.size());
    for (unsigned i=0,shiftBy=1; i<b.size(); shiftBy*=2,i++) {
        if (i<b.size()-1) { // create new variables (ri) for intermidate stages
            for (unsigned k=0; k<ri.size(); k++)
                ri[k]=satlib::RtpLit(mId,GetMFanoutWidth()+1+k+i*GetMFanoutWidth());
        }
        else { // for last stage use result variables directly (r)
            for(unsigned k=0; k<ri.size(); k++)
                ri[k]=r[k];
        }
        // create multiplexing stage
        makeShr(satSolver,b[i],ri,a,shiftBy);
        for (unsigned k=0; k<ri.size(); k++)
           a[k]=ri[k];
    }
}


// C.Villarraga 01.07.2011. Method fixed, last implementation was buggy
// Creates Cnf for a multiplexing stage. "inp" is shifted to the right
// "shiftBy" positions, when "sel" is active. Zeros are inserted in the
// MSBs. If shift amount "shiftBy" is higher than the size of the signal
// then all bits of the input are multiplexed with zero.
// @param solver: the interface to the solver
// @param res: RtpLit for the result
// @param inp: RtpLit for the input
// @param sel: determines whether shift is done or not.
void ShiftRight::makeShr (satlib::SatI& satSolver, satlib::RtpLit sel,
                              std::vector<satlib::RtpLit>& res,
                              std::vector<satlib::RtpLit>& inp,
                              unsigned shiftBy)
{
    if (shiftBy<res.size()) { // standard case
        for (unsigned k=res.size()-shiftBy;k<res.size();k++) {
            // res[k]=ite(sel,0,inp[k])
            // implemented by: res[k]= -sel*inp[k]
            satSolver.makeAnd(satlib::RtpLit(sel.first,-sel.second),inp[k],res[k]);
        }
        for(unsigned k=shiftBy;k<res.size();k++) {
                // res[k-shiftBy]=ite(sel,inp[k],inp[k-shiftBy]);
                satSolver.makeIte(sel,inp[k],inp[k-shiftBy],res[k-shiftBy]);
        }
    }
    else { // shift amount is higher than size of result
        for(unsigned k=0;k<res.size();k++) { // multiplexing with 0 for all bits
            // res[k]=ite(sel,0,inp[k])
            // implemented by: res[k]= -sel*inp[k]
            satSolver.makeAnd(satlib::RtpLit(sel.first,-sel.second),inp[k],res[k]);
        }
    }
}

void ShiftRight::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_srl(btor, mFanin[0]->GetBtorComp(), mFanin[1]->GetBtorComp());
	BtorComp = comp;
}
