/*! ************************************************************************
    \file output.cpp 
    \brief Implementation of class Output
    
    Implementation of the methods of the class Output
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Wed Feb 08 2006 

Modification history:
 * 15.02.06 added mFanin[i]->AddFanout(this);
 * 16.02.06 replaced asserts by exceptions
 * 17.02.06 changed return type of AddFanin from bool to void. Whether an 
 * add is successful or not is now handled by exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values

**************************************************************************/

#include "output.h"

/**
 * Constructors/Destructors
 */
 /**
  * A constructor with a vector of fanin Components.
  * An Output has one fanin Component so initialize mRequiredFanin with 1.
  * Add the fanin component to the fanin of this Output.
  * @param fanin A vector containing the Fanin Component of the Output
  * @param fanout The string naming the output
  * @param id The ID of the Output
  * @exception GeneralFanException is thrown if the size of the fanin does not 
  * match mRequiredFanin
  */
Output::Output(std::vector<Component*>& fanin, std::string fanout, int id, 
          Module* module)
{
    mModule=module;
    SetMRequiredFanin(1); 
    SetMValid(1);
    Setvalidname(1);
    Setvarref(0);
    Setignore(0);
    Setarray_index(-1);
    SetMType(OUTPUT);
    SetMOutputName(fanout);
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }
    //we have one fanin components so lets add this to our new Output.
    //First of all we allocate space for the Vector mFanin with undefined
    //fanin Components
    Undef* undef = new Undef();
    mFanin.resize(mRequiredFanin, undef);
    //We use a loop Iterating from i=0 to mcRequiredFanin-1 and add
    //the fanin at position i to position i. 
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //use an assert to check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mcRequiredFanin;i++)
    mFanoutWidth=fanin[0]->GetMFanoutWidth();
    mId=id;
} //End Output(vector<Component> fanin)
/**
 * Methods
 */

/**
 * Add a Component to the fanin of this Output at the position
 * given.
 * @param cmp Reference to the Component that shall be added to the fanin.
 * @param pos The position at which this Component shall be added.
 * @exception DoubledFaninException if the Component has already been added or
 * if it is not needed.
 */
void Output::AddFanin (Component* cmp, unsigned int pos)
{
    // Check if there is no Element at the position
    if (pos<mRequiredFanin && mFanin[pos]->GetMType()==0)
    {
        //There is no Element at pos and pos is needed. So we add the Element
        //and return true.    
        mFanin[pos]=cmp;
    } // End if (pos<mcRequiredFanin && mFanin[pos]==NULL)
    else
    {
        //Something went wrong the Element is already there or not needed
        std::string message="The fanin that shall be added is either already ";
        message+="there or there is no fanin needed at that position.";
        if (pos >= mRequiredFanin) 
            throw DoubledFaninException(message, this, cmp, new Undef(), pos);
        else throw DoubledFaninException(message, this, cmp, mFanin[pos], pos);
    } //End else
} //End AddFanin (Component& cmp, int pos)

/**
 * Change the Component in the fanin of this Output
 * @param cmpold Reference to the Component that shall be removed
 * @param cmpnew Reference to the Component that shall be added
 * @exception GeneralFaninException thrown if cmpold is not found in the
 * fanin of this component
 */
void Output::ChangeFanin(Component* cmpold, Component* cmpnew)
{
    // a variable used to store whether we found cmpold
    bool found=false;
    // walk through the fanins to find the position of cmpold
    for (unsigned int i=0;!found && i<mRequiredFanin;i++)
    {
        // set found to true if mFanin[i] and cmpold are equivalent
        found = (mFanin[i]==cmpold);
        //were we successful?
        if (found)
        {
            mFanin[i]=cmpnew;
        } // End if (found)
    } // End for (unsigned int i=0;!found && i<mRequiredFanin;i++)
    //Not successful? - Throw GeneralFanException
    if(!found)
    {
        std::string msg = "EXCEPTION: The fanin that shall be changed was not";
        msg += " found";
        std::vector<Component*> cmps;
        cmps.push_back(cmpold);
        throw GeneralFanException(msg, this, cmps);
    } // End if (!found) 
} // End ChangeFanin(Component* cmpold, Component* cmpnew)

/**
 * Update the value of mInput
 * Fetch the current assignment of the output of the fanin Component and
 * update mInput of this Output accordingly.
 */
void Output::UpdateInputs()
{
    mInputs.clear();
    //For each fanin Component get its output values and assign them to the
    //corresponding input
    for (unsigned int i=0;i<mRequiredFanin;i++)
    {
        //Input i is assigned the output value of Fanin i
        mInputs.push_back(mFanin[i]->GetOutput());
    } //End for (int i=0;i<mcRequiredFanin;i++) 
} // End UpdateInputs

/**
 * Check whether the Output is complete.
 * @return True if the Output has mRequiredFanin (=1) fanin Components
 */
bool Output::IsComplete () 
{
    //We check for the output first and the input second, as all inputs should
    //be there after a successful creation
    if (mFanin.size()==mRequiredFanin) return true;
    //not complete
    else 
    {
        std::cout<<GetType()<<" "<<GetMOutputName()<<" fanins: "
                <<mFanin.size()<<" required: "<<mRequiredFanin<<std::endl;
        return false;
    }
}

/**
 * Get a vector of the RtpLits defined by this Output.
 * For an Output the RtpLits are equivalent to the input lits.
 * @param r the vector where the defined RtpLits will be stored in.
 */
void Output::GetFanoutRtpLits(std::vector<satlib::RtpLit>& r)
{
    //vector for the literals in the fanin
    std::vector<satlib::RtpLit> a;
    mFanin[0]->GetFanoutRtpLits(a);
    for(unsigned int i=0; i<a.size();i++)
    {
        r.push_back(satlib::RtpLit(a[i].first,a[i].second));
    }
}

void Output::Create_BtorComp(Btor* btor)
{
	BtorNode* comp = boolector_and(btor, mFanin[0]->GetBtorComp(), mFanin[0]->GetBtorComp());
	BtorComp = comp;
}
