/*! ************************************************************************
    \file PslToFck.h
    \brief Headerfile Class TimeFrameExpansion describing the behavior of this

    Contains the circuit that shall be checked assumptions and properties are
 * used to build a new circuit with assumption property and the transitive
 * fanin of these
    \author Oliver Marx

    \copyright (c) University of Kaiserslautern Mon Apr 07 2006

Modification history:

**************************************************************************/

#ifndef PSLTOFCK_H2
#define PSLTOFCK_H2
#include "circuit.h"
#include "circuitbuilder.h"
#include "../lib/pslparser/PslFactoryI.h"
#include "PslProperty.h"
#include <map>
#include <sstream>
#include <string>

class PslToFck : public PslFactoryI
{
    public:
        PslToFck(std::vector<PslProperty*>* properties, Circuit* circuit);
        virtual ~PslToFck(){}
	
        //! \brief This exception may be thrown by each of the factory functions
	//! of \a PslFactoryI and is catched within \a PSL_Parser, which in
	//! turn throws \a PSL_Exception, containing the line number and token,
	//! that must be catched by the user of the parser.
	class Exception : public std::exception
	{
	public:
		Exception(const std::string what) : m_What(what) {}
		~Exception() throw() {}
		const char* what() const throw() { return m_What.c_str(); }

	private:
		const std::string	m_What;
	};

        /**
                      * Create a "and" or "nand" for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the first fanin 
                      * @param id2 the id of the second fanin 
                      * @param type "and" or "nand"
                      * @param name the name of the assignment
                      */
        virtual FckId MakeAndType(FckId id1,FckId id2, std::string type, std::string name);
        
        /**
                      * Create a "or" , "nor", "xor", or "xnor" for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the first fanin 
                      * @param id2 the id of the second fanin 
                      * @param type "or" , "nor", "xor", or "xnor"
                      * @param name the name of the assignment
                      */
        virtual FckId MakeOrType(FckId id1,FckId id2, std::string type, std::string name);
        
        /**
                      * Create a "add" or "mult" for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the first fanin 
                      * @param id2 the id of the second fanin 
                      * @param type "add" or "mult"
                      * @param name the name of the assignment
                      */
        virtual FckId MakeArith(FckId id1,FckId id2, std::string type, std::string name);
        
        /**
                      * All assumptions and commitments are added. Combine these to the property and maybe begin a new property
                      */
        virtual void FinishProperty();
        
        /**
                      * The timeframe changes.
                      * @return if type="next" then -change else change
                      * @param type "next" or "prev"
                      * @param change the amount of change 
                      */
        virtual int MakeTimeFrameChange(std::string type, int change);
        
        /**
                      * Create a if-then-else for the signals id1, id2, id3
                      * @return id of the assignment
                      * @param id1 if-id 
                      * @param id2 then-id
                      * @param id3 else-id
                      * @param name the name of the assignment
                      */
        virtual FckId MakeIte(FckId id1,FckId id2,FckId id3, std::string name);
        
        /**
                      * Create a if-then for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 if-id 
                      * @param id2 then-id
                      * @param name the name of the assignment
                      */
        virtual FckId MakeIt(FckId id1,FckId id2, std::string name);
        
        /**
                      * Create a "fell", "rose" or "stable" for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the fanin 
                      * @param type "fell", "rose" or "stable"
                      * @param name the name of the assignment
                      */
        virtual FckId MakeTemporal(FckId id1, std::string type, std::string name);
        
        /**
                      * Create a id for the signal id
                      * @return id of the variable
                      * @param id the name of the signal in the circuit 
                      */
        virtual FckId MakeExpression(std::string id);
        
        /**
                      * Create a constant for the binary or integer value in id
                      * @return id of the constant
                      * @param id constant value in binary or integer form
                      */
        virtual FckId MakeConst(std::string id);
        
        /**
                      * Create a "shl" or "shr" for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the fanin 
                      * @param id2 the id of the shift amount 
                      * @param type "shl" or "shr"
                      * @param name the name of the assignment
                      */
        virtual FckId MakeShift(FckId id1,FckId id2, std::string type, std::string name);
        
        /**
                      * Create a "zero_extend" or "sign_extend" for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the first fanin 
                      * @param id2 the id of the second fanin 
                      * @param type "zero_extend" or "sign_extend"
                      * @param name the name of the assignment
                      */
        virtual FckId MakeExtend(FckId id1,FckId id2, std::string type, std::string name);
        
        /**
                      * Create a comparison for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the first fanin 
                      * @param id2 the id of the second fanin 
                      * @param type ">",  "<",  ">=",  "<=",  "==" or "!="
                      * @param name the name of the assignment
                      */
        virtual FckId MakeCompare(FckId id1,FckId id2,std::string type, std::string name);
        
        /**
                      * Create a concatenation of the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the first fanin 
                      * @param id2 the id of the second fanin 
                      * @param name the name of the assignment
                      */
        virtual FckId MakeConcat(FckId id1,FckId id2, std::string name);
        
        /**
                      * Create a slice for the signals id1 and id2
                      * @return id of the assignment
                      * @param id1 the id of the fanin 
                      * @param id2 the upper bound
                      * @param id3 the the lower bound
                      * @param name the name of the assignment
                      */
        virtual FckId MakeSlice(FckId id1,int id2,int id3, std::string name);
        
        /**
                      * add a assumption or a commitment
                      * @return id of the assignment
                      * @param id the id of the assumption/commitment
                      * @param type "assume" or "assert"
                      */
        virtual void MakeCondition(FckId id, std::string type, int repeat);
        
        /**
                      * Create a "not" for the signal id1
                      * @return id of the assignment
                      * @param id1 the id of the fanin 
                      * @param name the name of the assignment
                      */
        virtual FckId MakeNot(FckId id1, std::string name);
        
         /**
                      * Create a negation for the signal id1
                      * @return id of the assignment
                      * @param id1 the id of the fanin 
                      * @param name the name of the assignment
                      */
        virtual FckId MakeMinus(FckId id1, std::string name);
        
        /**
                      * Create a unary operation for the signal id1
                      * @return id of the assignment
                      * @param id1 the id of the fanin 
                      * @param type "reduce_or", "reduce_and" or "reduce_xor"
                      * @param name the name of the assignment
                      */
        virtual FckId MakeReduce(FckId id1, std::string type, std::string name);
        
        /**
                      * Name the current property
                       * @param name the name of the property
                      */
        virtual void SetPropertyName(std::string name);

        //son lam 30.12.2014
        FckId copy_circuitblock(FckId id, int timepoint);
    protected:

    private:
        /**
                      * convert integer to string
                      */
        std::string IntToStr(int num);
        
        /**
                      * convert string to lower case string
                      */
        std::string LowerCase(std::string str);
        
        /**
                      * convert integer as string to binary string
                      */
        std::string StrToBin(std::string str);

        /**
                      * vector to store the properties of the file
                      */
        std::vector<PslProperty*>* mProperties;
        
        /**
                      * property counter
                      */
        int CurrentProperty;
        
        /**
                      * current property
                      */
        PslProperty* mCurrentProperty;
        
        /**
                      * current timeframe
                      */
        int mTimeFrame;
        
        /**
                      * circuit to extract bitwidth
                      */
        Circuit* mWidthCircuit;
};
#endif
