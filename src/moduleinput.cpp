/*! ************************************************************************
    \file moduleinput.cpp 
    \brief Implementation of class ModuleInput
    
    Implementation of the methods of the class ModuleInput
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Thu March 02 2006

Modification history:
 * 27.03.06 changed simulation to use lValues from satlib::SatI instead of 
 * lbool values
 * 28.03.06 retrieve ID from the name of the fanout

**************************************************************************/

#include "moduleinput.h"
#include "module.h"
#include "circuit.h"

/**
 * Constructors/Destructors
 */
 /**
  * A constructor with a vector of fanin Components.
  * The number of fanin Components a ModuleInput has depends on the Module
  * plugged into it. So get the number of fanin Components from the Module.
  * Add the fanin Components to the fanin of this ModuleInput.
  * @param fanin A vector containing the Fanin Components of the ModuleInput
  * @param module The string naming the Module
  * @param id The ID of the ModuleInput
  */
ModuleInput::ModuleInput(std::vector<Component*>& fanin, 
                         ModuleId moduleId, std::string output,int id, 
                         Module* module)
{
    mModule=module;
    //Determine the module that shall be included
    mInclModule = mModule->GetParentCircuit()->GetModule(moduleId);
    //Set the number of required inputs
    SetMRequiredFanin(mInclModule->GetNumInps());
    mOutputs=mInclModule->GetNumOuts();
    SetMType(M_INPUT);
    SetMOutputName(output);
    mId=id;
    BtorComp = NULL;
    //do we really have the correct number of fanin components?
    //The Module can contain real inputs that are not mapped. 
    //We try to work around this problem by not checking whether the number of
    //fanin components is correct. Therefore the open inputs have to follow the mapped ones.
    /*if(fanin.size()!=mRequiredFanin)
    {
        //we do not have the correct number of fanin components throw a 
        //GeneralFanException
        std::cout<<"Required "<<mRequiredFanin<<std::endl;
        std::string message = "EXCEPTION: The number of fanin components is"; 
        message+="not equivalent to the number of required fanins";
        throw GeneralFanException(message, this, fanin);
    }*/
    //we have n fanin Components so lets add these to our new ModuleInput.
    //First of all we allocate space for the Vector mFanin with undefined
    //fanin Components
    Undef* undef = new Undef();
    mFanin.resize(mRequiredFanin, undef);
    //We use a loop Iterating from i=0 to mRequiredFanin-1 and add
    //the fanin at position i to position i. 
    mFanoutWidth=0;
    mInclModule->GetMOutputs(mOutputNames);
    for (unsigned int i=0; i<mRequiredFanin;i++)
    {
        //check that all fanin components are added correctly
        AddFanin(fanin[i], i);
        //add this component to the fanout of the fanin
        mFanin[i]->AddFanout(this);
    } //End of for (int i=0; i<mRequiredFanin;i++)
    delete undef;
} //End ModuleInput(vector<string>&, string, int, Module*)

/**
 * Methods
 */
/**
 * Add a Component to the fanin of this ModuleInput.
 * This method adds the Component given to the fanin of this ModuleInput.
 * @param cmp Reference to the Component that shall be added to the fanin.
 * @param pos The position at which this Component shall be added. 
 * @exception GeneralFanException if the fanin of a fanout Component can
 * not be sucessfully changed.
 * @exception DoubledFaninException if this Module already has a fanin 
 * Component at the position specified 
 * 
 * Increments the value of mKnownFanin.
 */
void ModuleInput::AddFanin(Component* cmp,unsigned int pos)
{
    // Check if there is no Element at the position
    if (pos<mRequiredFanin && mFanin[pos]->GetMType()==0)
    {
        //There is no Element at pos and pos is needed. So we add the Element
        //and return true.    
        mFanin[pos]=cmp;
        //Get the Width of the fanout and set the fanin to the same width
        //if necessary resize
        if (mFaninWidth.size()<pos+1) mFaninWidth.resize(pos+1,0);
        mFaninWidth[pos]=cmp->GetMFanoutWidth();
    } // End if (pos<mcRequiredFanin && mFanin[pos]==NULL)
    else
    {
        //Something went wrong the Element is already there or not needed
        std::string message="EXCEPTION: The fanin that shall be added is ";
        message+="either already there or there is no fanin needed at that position.";
        if (pos >= mRequiredFanin) 
            throw DoubledFaninException(message, this, cmp, new Undef(), pos);
        else throw DoubledFaninException(message, this, cmp, mFanin[pos], pos);
    } //End else
}

/**
 * Update the value of mInput
 * Fetch the current assignment of the output of the fanin Component and
 * update mInput of this Output accordingly.
 */
void ModuleInput::UpdateInputs()
{
    mOutput.clear();
    //For each fanin Component get its output values and assign them to the
    //corresponding input
    //mOutput is assigned the output value of the Fanin
    mOutput=mFanin[0]->GetOutput();
} // End UpdateInputs

/**
 * Check whether the Function is complete.
 * @return True if the Function has mRequiredFanin fanin Components and the 
 * number of fanout Components required by the included Module.
 */
bool ModuleInput::IsComplete () 
{
    //We check for the output first and the input second, as all inputs should
    //be there after a successful creation
    if (mFanout.size()!=1)
    {
        return false;
    }
    if (mFanin.size()==mRequiredFanin) return true;
    //not complete
    else return false;
}

/**
 * Return the Components in the fanout of this Component.
 * 
 * If a Component has Components in the Fanout it must override this 
 * method. 
 * @param output the name of the Output we want to retrieve the fanout for
 * @return This method here returns an empty vector for those Components 
 * that do not have Components in their fanout (Override when necessary).
 */
std::vector<Component*> ModuleInput::GetFanout() 
{
    //return the vector
    return mFanout;
    
}

/**
 * Get the names of the Inputs to the Module
 * @return A vector containing the inputs
 */
std::vector<Component*> ModuleInput::GetModuleInputs()
{
    return mInclModule->GetModuleInputs();
}
