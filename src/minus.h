/*! ************************************************************************
    \file minus.h
    \brief Header file of the Class Minus.

    The class contained in this file defines the attributes and methods of the
 *  Function Minus.
    \author Sacha Loitz

    \copyright (c) University of Kaiserslautern Wed Feb 01 - Thu Feb 09 2006

Modification history:
 * 16.02.06 included Exceptions
 * 17.02.06 added parameter fanout to constructor, to name the component

**************************************************************************/

#ifndef MINUS_H
#define MINUS_H
#include <string>
#include "function.h"
#include <vector>
#include "undef.h"
#include "../exceptions/generalfanexception.h"
#include "../exceptions/inputsizeexception.h"

/**
 * Class to describe the behaviour of the Minus Function.
 * This class implements the Function Minus derived from the class
 * Function.
 */
class Minus : public Function {
/**
 * Public stuff
 */
public:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Default Constructor.
     * The Minus Function needs 2 Fanin.
     */
    Minus(){SetMRequiredFanin(1);SetMType(MINUS);SetMOutputName("");}
    
    /**
     * A constructor with a vector of fanin Components.
     * A Minus has one fanin Component so initialize mRequiredFanin with 
     * 1.
     * Add the fanin component to the fanin of this Minus.
     * @param fanin A vector containing the Fanin Component of the Minus. 
     * @param fanout The string naming the output
     * @param id The ID of this Minus
     * @param module The Module the Minus shall be added to.
     * @exception GeneralFanException is thrown if the size of the fanin does not 
     * match mRequiredFanin
     */
    Minus(std::vector<Component*>& fanin, std::string fanout, int id, 
          Module* module);
    
    /**
     * Destructor
     */
    virtual ~Minus(){}
    /**
     * Accessor Methods
     */
    /**
     * Return the type of this Minus as a string (MINUS)
     * @return The string "MINUS"
     */
    inline std::string GetType(){return "MINUS";}
    /**
     * Operations
     */

    /**
     * Perform a forward simulation from the Inputs to the Outputs.
     * Update the values of mOutput according to the current values of
     * mInputs. For correct simulation results first of all the values of
     * mInputs have to be updated by fetching the result (mOutput) of the
     * operations in the fanin.
     */
    void SimulateForward ();

    /**
     * Perform a simulation of this Minus for a given vector of inputs
     * @param inpVals The input values the simulation shall be performed for
     * @param outVals The resulting output of the simulation
     */
    void Simulate
            (std::vector< std::vector< satlib::SatI::lValue> >& inpVals,
             std::vector< satlib::SatI::lValue>&  outVals );
    
    /**
     * Create the cnf for this Minus.
     * @param solver the interface to the solver
     */
    void CreateCnf(satlib::SatI& solver);

	void Create_BtorComp(Btor* btor);
/**
 * Protected stuff
 */
protected:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */
    /**
     * Operations
     */
/**
 * Private stuff
 */
private:
    /**
     * Fields
     */
    /**
     *
     */
    /**
     * Constructors
     */
    /**
     * Accessor Methods
     */

    /**
     * Operations
     */
};
#endif //MINUS_H

