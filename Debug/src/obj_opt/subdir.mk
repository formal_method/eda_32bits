################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../src/obj_opt/MinisatI.o \
../src/obj_opt/PSL_Parser.o \
../src/obj_opt/PSL_ParserFunction.o \
../src/obj_opt/PSL_Scanner.o \
../src/obj_opt/PSL_ScannerFunction.o \
../src/obj_opt/PslProperty.o \
../src/obj_opt/PslToFck.o \
../src/obj_opt/SatI.o \
../src/obj_opt/Solver.o \
../src/obj_opt/V3Active.o \
../src/obj_opt/V3ActiveTop.o \
../src/obj_opt/V3Assert.o \
../src/obj_opt/V3AssertPre.o \
../src/obj_opt/V3Ast.o \
../src/obj_opt/V3AstNodes.o \
../src/obj_opt/V3Begin.o \
../src/obj_opt/V3Branch.o \
../src/obj_opt/V3Broken.o \
../src/obj_opt/V3Case.o \
../src/obj_opt/V3Cast.o \
../src/obj_opt/V3Cdc.o \
../src/obj_opt/V3Changed.o \
../src/obj_opt/V3Clean.o \
../src/obj_opt/V3ClkGater.o \
../src/obj_opt/V3Clock.o \
../src/obj_opt/V3Combine.o \
../src/obj_opt/V3Config.o \
../src/obj_opt/V3Const__gen.o \
../src/obj_opt/V3Coverage.o \
../src/obj_opt/V3CoverageJoin.o \
../src/obj_opt/V3Dead.o \
../src/obj_opt/V3Delayed.o \
../src/obj_opt/V3Depth.o \
../src/obj_opt/V3DepthBlock.o \
../src/obj_opt/V3Descope.o \
../src/obj_opt/V3EmitC.o \
../src/obj_opt/V3EmitCInlines.o \
../src/obj_opt/V3EmitCSyms.o \
../src/obj_opt/V3EmitMk.o \
../src/obj_opt/V3EmitV.o \
../src/obj_opt/V3EmitXml.o \
../src/obj_opt/V3Error.o \
../src/obj_opt/V3Expand.o \
../src/obj_opt/V3File.o \
../src/obj_opt/V3Gate.o \
../src/obj_opt/V3GenClk.o \
../src/obj_opt/V3Graph.o \
../src/obj_opt/V3GraphAcyc.o \
../src/obj_opt/V3GraphAlg.o \
../src/obj_opt/V3GraphDfa.o \
../src/obj_opt/V3GraphTest.o \
../src/obj_opt/V3Hashed.o \
../src/obj_opt/V3Inline.o \
../src/obj_opt/V3Inst.o \
../src/obj_opt/V3Life.o \
../src/obj_opt/V3LifePost.o \
../src/obj_opt/V3LinkCells.o \
../src/obj_opt/V3LinkDot.o \
../src/obj_opt/V3LinkJump.o \
../src/obj_opt/V3LinkLValue.o \
../src/obj_opt/V3LinkLevel.o \
../src/obj_opt/V3LinkParse.o \
../src/obj_opt/V3LinkResolve.o \
../src/obj_opt/V3Localize.o \
../src/obj_opt/V3MakeNode.o \
../src/obj_opt/V3Name.o \
../src/obj_opt/V3NodeTraversal.o \
../src/obj_opt/V3Number.o \
../src/obj_opt/V3Options.o \
../src/obj_opt/V3Order.o \
../src/obj_opt/V3Param.o \
../src/obj_opt/V3ParseGrammar.o \
../src/obj_opt/V3ParseImp.o \
../src/obj_opt/V3ParseLex.o \
../src/obj_opt/V3PreProc.o \
../src/obj_opt/V3PreShell.o \
../src/obj_opt/V3Premit.o \
../src/obj_opt/V3Scope.o \
../src/obj_opt/V3Slice.o \
../src/obj_opt/V3Split.o \
../src/obj_opt/V3SplitAs.o \
../src/obj_opt/V3Stats.o \
../src/obj_opt/V3StatsReport.o \
../src/obj_opt/V3String.o \
../src/obj_opt/V3Subst.o \
../src/obj_opt/V3Table.o \
../src/obj_opt/V3Task.o \
../src/obj_opt/V3Trace.o \
../src/obj_opt/V3TraceDecl.o \
../src/obj_opt/V3Tristate.o \
../src/obj_opt/V3Undriven.o \
../src/obj_opt/V3Unknown.o \
../src/obj_opt/V3Unroll.o \
../src/obj_opt/V3Width.o \
../src/obj_opt/V3WidthSel.o \
../src/obj_opt/Verilator.o \
../src/obj_opt/add.o \
../src/obj_opt/and.o \
../src/obj_opt/circuit.o \
../src/obj_opt/component.o \
../src/obj_opt/concat.o \
../src/obj_opt/constinput.o \
../src/obj_opt/decoder.o \
../src/obj_opt/equivalence.o \
../src/obj_opt/function.o \
../src/obj_opt/ifthenelse.o \
../src/obj_opt/input.o \
../src/obj_opt/lessthan.o \
../src/obj_opt/minus.o \
../src/obj_opt/module.o \
../src/obj_opt/moduleinput.o \
../src/obj_opt/moduleoutput.o \
../src/obj_opt/multiplier.o \
../src/obj_opt/mux.o \
../src/obj_opt/node.o \
../src/obj_opt/not.o \
../src/obj_opt/or.o \
../src/obj_opt/output.o \
../src/obj_opt/pseudoinput.o \
../src/obj_opt/pseudooutput.o \
../src/obj_opt/rotateleft.o \
../src/obj_opt/rotateright.o \
../src/obj_opt/shiftleft.o \
../src/obj_opt/shiftright.o \
../src/obj_opt/shiftrightarith.o \
../src/obj_opt/signextension.o \
../src/obj_opt/slice.o \
../src/obj_opt/timeframeexpansion.o \
../src/obj_opt/unaryand.o \
../src/obj_opt/unaryor.o \
../src/obj_opt/unaryxor.o \
../src/obj_opt/write.o \
../src/obj_opt/xor.o \
../src/obj_opt/zeroextension.o 

CPP_SRCS += \
../src/obj_opt/V3Const__gen.cpp \
../src/obj_opt/V3Lexer.yy.cpp \
../src/obj_opt/V3Lexer_pregen.yy.cpp \
../src/obj_opt/V3PreLex.yy.cpp \
../src/obj_opt/V3PreLex_pregen.yy.cpp 

C_SRCS += \
../src/obj_opt/V3ParseBison.c 

OBJS += \
./src/obj_opt/V3Const__gen.o \
./src/obj_opt/V3Lexer.yy.o \
./src/obj_opt/V3Lexer_pregen.yy.o \
./src/obj_opt/V3ParseBison.o \
./src/obj_opt/V3PreLex.yy.o \
./src/obj_opt/V3PreLex_pregen.yy.o 

C_DEPS += \
./src/obj_opt/V3ParseBison.d 

CPP_DEPS += \
./src/obj_opt/V3Const__gen.d \
./src/obj_opt/V3Lexer.yy.d \
./src/obj_opt/V3Lexer_pregen.yy.d \
./src/obj_opt/V3PreLex.yy.d \
./src/obj_opt/V3PreLex_pregen.yy.d 


# Each subdirectory must supply rules for building sources it contributes
src/obj_opt/%.o: ../src/obj_opt/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/obj_opt/%.o: ../src/obj_opt/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


