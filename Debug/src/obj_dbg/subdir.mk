################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../src/obj_dbg/MinisatI.o \
../src/obj_dbg/PSL_Parser.o \
../src/obj_dbg/PSL_ParserFunction.o \
../src/obj_dbg/PSL_Scanner.o \
../src/obj_dbg/PSL_ScannerFunction.o \
../src/obj_dbg/PslProperty.o \
../src/obj_dbg/PslToFck.o \
../src/obj_dbg/SatI.o \
../src/obj_dbg/Solver.o \
../src/obj_dbg/V3Active.o \
../src/obj_dbg/V3ActiveTop.o \
../src/obj_dbg/V3Assert.o \
../src/obj_dbg/V3AssertPre.o \
../src/obj_dbg/V3Ast.o \
../src/obj_dbg/V3AstNodes.o \
../src/obj_dbg/V3Begin.o \
../src/obj_dbg/V3Branch.o \
../src/obj_dbg/V3Broken.o \
../src/obj_dbg/V3Case.o \
../src/obj_dbg/V3Cast.o \
../src/obj_dbg/V3Cdc.o \
../src/obj_dbg/V3Changed.o \
../src/obj_dbg/V3Clean.o \
../src/obj_dbg/V3ClkGater.o \
../src/obj_dbg/V3Clock.o \
../src/obj_dbg/V3Combine.o \
../src/obj_dbg/V3Config.o \
../src/obj_dbg/V3Const__gen.o \
../src/obj_dbg/V3Coverage.o \
../src/obj_dbg/V3CoverageJoin.o \
../src/obj_dbg/V3Dead.o \
../src/obj_dbg/V3Delayed.o \
../src/obj_dbg/V3Depth.o \
../src/obj_dbg/V3DepthBlock.o \
../src/obj_dbg/V3Descope.o \
../src/obj_dbg/V3EmitC.o \
../src/obj_dbg/V3EmitCInlines.o \
../src/obj_dbg/V3EmitCSyms.o \
../src/obj_dbg/V3EmitMk.o \
../src/obj_dbg/V3EmitV.o \
../src/obj_dbg/V3EmitXml.o \
../src/obj_dbg/V3Error.o \
../src/obj_dbg/V3Expand.o \
../src/obj_dbg/V3File.o \
../src/obj_dbg/V3Gate.o \
../src/obj_dbg/V3GenClk.o \
../src/obj_dbg/V3Graph.o \
../src/obj_dbg/V3GraphAcyc.o \
../src/obj_dbg/V3GraphAlg.o \
../src/obj_dbg/V3GraphDfa.o \
../src/obj_dbg/V3GraphTest.o \
../src/obj_dbg/V3Hashed.o \
../src/obj_dbg/V3Inline.o \
../src/obj_dbg/V3Inst.o \
../src/obj_dbg/V3Life.o \
../src/obj_dbg/V3LifePost.o \
../src/obj_dbg/V3LinkCells.o \
../src/obj_dbg/V3LinkDot.o \
../src/obj_dbg/V3LinkJump.o \
../src/obj_dbg/V3LinkLValue.o \
../src/obj_dbg/V3LinkLevel.o \
../src/obj_dbg/V3LinkParse.o \
../src/obj_dbg/V3LinkResolve.o \
../src/obj_dbg/V3Localize.o \
../src/obj_dbg/V3MakeNode.o \
../src/obj_dbg/V3Name.o \
../src/obj_dbg/V3NodeTraversal.o \
../src/obj_dbg/V3Number.o \
../src/obj_dbg/V3Options.o \
../src/obj_dbg/V3Order.o \
../src/obj_dbg/V3Param.o \
../src/obj_dbg/V3ParseGrammar.o \
../src/obj_dbg/V3ParseImp.o \
../src/obj_dbg/V3ParseLex.o \
../src/obj_dbg/V3PreProc.o \
../src/obj_dbg/V3PreShell.o \
../src/obj_dbg/V3Premit.o \
../src/obj_dbg/V3Scope.o \
../src/obj_dbg/V3Slice.o \
../src/obj_dbg/V3Split.o \
../src/obj_dbg/V3SplitAs.o \
../src/obj_dbg/V3Stats.o \
../src/obj_dbg/V3StatsReport.o \
../src/obj_dbg/V3String.o \
../src/obj_dbg/V3Subst.o \
../src/obj_dbg/V3Table.o \
../src/obj_dbg/V3Task.o \
../src/obj_dbg/V3Trace.o \
../src/obj_dbg/V3TraceDecl.o \
../src/obj_dbg/V3Tristate.o \
../src/obj_dbg/V3Undriven.o \
../src/obj_dbg/V3Unknown.o \
../src/obj_dbg/V3Unroll.o \
../src/obj_dbg/V3Width.o \
../src/obj_dbg/V3WidthSel.o \
../src/obj_dbg/Verilator.o \
../src/obj_dbg/add.o \
../src/obj_dbg/and.o \
../src/obj_dbg/circuit.o \
../src/obj_dbg/component.o \
../src/obj_dbg/concat.o \
../src/obj_dbg/constinput.o \
../src/obj_dbg/decoder.o \
../src/obj_dbg/equivalence.o \
../src/obj_dbg/function.o \
../src/obj_dbg/ifthenelse.o \
../src/obj_dbg/input.o \
../src/obj_dbg/lessthan.o \
../src/obj_dbg/minus.o \
../src/obj_dbg/module.o \
../src/obj_dbg/moduleinput.o \
../src/obj_dbg/moduleoutput.o \
../src/obj_dbg/multiplier.o \
../src/obj_dbg/mux.o \
../src/obj_dbg/node.o \
../src/obj_dbg/not.o \
../src/obj_dbg/or.o \
../src/obj_dbg/output.o \
../src/obj_dbg/pseudoinput.o \
../src/obj_dbg/pseudooutput.o \
../src/obj_dbg/rotateleft.o \
../src/obj_dbg/rotateright.o \
../src/obj_dbg/shiftleft.o \
../src/obj_dbg/shiftright.o \
../src/obj_dbg/shiftrightarith.o \
../src/obj_dbg/signextension.o \
../src/obj_dbg/slice.o \
../src/obj_dbg/timeframeexpansion.o \
../src/obj_dbg/unaryand.o \
../src/obj_dbg/unaryor.o \
../src/obj_dbg/unaryxor.o \
../src/obj_dbg/write.o \
../src/obj_dbg/xor.o \
../src/obj_dbg/zeroextension.o 

CPP_SRCS += \
../src/obj_dbg/V3Const__gen.cpp \
../src/obj_dbg/V3Lexer.yy.cpp \
../src/obj_dbg/V3Lexer_pregen.yy.cpp \
../src/obj_dbg/V3PreLex.yy.cpp \
../src/obj_dbg/V3PreLex_pregen.yy.cpp 

C_SRCS += \
../src/obj_dbg/V3ParseBison.c 

OBJS += \
./src/obj_dbg/V3Const__gen.o \
./src/obj_dbg/V3Lexer.yy.o \
./src/obj_dbg/V3Lexer_pregen.yy.o \
./src/obj_dbg/V3ParseBison.o \
./src/obj_dbg/V3PreLex.yy.o \
./src/obj_dbg/V3PreLex_pregen.yy.o 

C_DEPS += \
./src/obj_dbg/V3ParseBison.d 

CPP_DEPS += \
./src/obj_dbg/V3Const__gen.d \
./src/obj_dbg/V3Lexer.yy.d \
./src/obj_dbg/V3Lexer_pregen.yy.d \
./src/obj_dbg/V3PreLex.yy.d \
./src/obj_dbg/V3PreLex_pregen.yy.d 


# Each subdirectory must supply rules for building sources it contributes
src/obj_dbg/%.o: ../src/obj_dbg/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/obj_dbg/%.o: ../src/obj_dbg/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


