################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../lib/boolector-1.6.0-with-sat-solvers/boolector/boolector.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/boolectormain.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btor2horn.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btoraig.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btoraigvec.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorbeta.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorbtor.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorconst.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btordump.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btordump2.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorexp.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorfmt.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorhash.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btoriter.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorlog.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btormain.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btormap.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btormbt.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btormc.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btormem.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorminisat.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btormisc.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorparamcache.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorrewrite.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorsat.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorsmt.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorsmt2.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btoruntrace.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorutil.o \
../lib/boolector-1.6.0-with-sat-solvers/boolector/synthebtor.o 

C_SRCS += \
../lib/boolector-1.6.0-with-sat-solvers/boolector/boolector.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/boolectormain.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btor2horn.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btoraig.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btoraigvec.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorbeta.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorbtor.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorconst.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btordump.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btordump2.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorexp.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorfmt.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorhash.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btoriter.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorlog.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btormain.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btormap.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btormbt.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btormc.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btormem.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btormisc.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorparamcache.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorrewrite.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorsat.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorsmt.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorsmt2.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btoruntrace.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorutil.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/deltabtor.c \
../lib/boolector-1.6.0-with-sat-solvers/boolector/synthebtor.c 

CC_SRCS += \
../lib/boolector-1.6.0-with-sat-solvers/boolector/btorminisat.cc 

OBJS += \
./lib/boolector-1.6.0-with-sat-solvers/boolector/boolector.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/boolectormain.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btor2horn.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btoraig.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btoraigvec.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorbeta.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorbtor.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorconst.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btordump.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btordump2.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorexp.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorfmt.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorhash.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btoriter.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorlog.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btormain.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btormap.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btormbt.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btormc.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btormem.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorminisat.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btormisc.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorparamcache.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorrewrite.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorsat.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorsmt.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorsmt2.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btoruntrace.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorutil.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/deltabtor.o \
./lib/boolector-1.6.0-with-sat-solvers/boolector/synthebtor.o 

C_DEPS += \
./lib/boolector-1.6.0-with-sat-solvers/boolector/boolector.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/boolectormain.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btor2horn.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btoraig.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btoraigvec.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorbeta.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorbtor.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorconst.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btordump.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btordump2.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorexp.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorfmt.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorhash.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btoriter.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorlog.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btormain.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btormap.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btormbt.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btormc.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btormem.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btormisc.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorparamcache.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorrewrite.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorsat.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorsmt.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorsmt2.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btoruntrace.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorutil.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/deltabtor.d \
./lib/boolector-1.6.0-with-sat-solvers/boolector/synthebtor.d 

CC_DEPS += \
./lib/boolector-1.6.0-with-sat-solvers/boolector/btorminisat.d 


# Each subdirectory must supply rules for building sources it contributes
lib/boolector-1.6.0-with-sat-solvers/boolector/%.o: ../lib/boolector-1.6.0-with-sat-solvers/boolector/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

lib/boolector-1.6.0-with-sat-solvers/boolector/%.o: ../lib/boolector-1.6.0-with-sat-solvers/boolector/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


