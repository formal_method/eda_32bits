################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lib/boolector-1.6.0-with-sat-solvers/picosat/app.c \
../lib/boolector-1.6.0-with-sat-solvers/picosat/main.c \
../lib/boolector-1.6.0-with-sat-solvers/picosat/picogcnf.c \
../lib/boolector-1.6.0-with-sat-solvers/picosat/picomcs.c \
../lib/boolector-1.6.0-with-sat-solvers/picosat/picomus.c \
../lib/boolector-1.6.0-with-sat-solvers/picosat/picosat.c \
../lib/boolector-1.6.0-with-sat-solvers/picosat/version.c 

OBJS += \
./lib/boolector-1.6.0-with-sat-solvers/picosat/app.o \
./lib/boolector-1.6.0-with-sat-solvers/picosat/main.o \
./lib/boolector-1.6.0-with-sat-solvers/picosat/picogcnf.o \
./lib/boolector-1.6.0-with-sat-solvers/picosat/picomcs.o \
./lib/boolector-1.6.0-with-sat-solvers/picosat/picomus.o \
./lib/boolector-1.6.0-with-sat-solvers/picosat/picosat.o \
./lib/boolector-1.6.0-with-sat-solvers/picosat/version.o 

C_DEPS += \
./lib/boolector-1.6.0-with-sat-solvers/picosat/app.d \
./lib/boolector-1.6.0-with-sat-solvers/picosat/main.d \
./lib/boolector-1.6.0-with-sat-solvers/picosat/picogcnf.d \
./lib/boolector-1.6.0-with-sat-solvers/picosat/picomcs.d \
./lib/boolector-1.6.0-with-sat-solvers/picosat/picomus.d \
./lib/boolector-1.6.0-with-sat-solvers/picosat/picosat.d \
./lib/boolector-1.6.0-with-sat-solvers/picosat/version.d 


# Each subdirectory must supply rules for building sources it contributes
lib/boolector-1.6.0-with-sat-solvers/picosat/%.o: ../lib/boolector-1.6.0-with-sat-solvers/picosat/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


