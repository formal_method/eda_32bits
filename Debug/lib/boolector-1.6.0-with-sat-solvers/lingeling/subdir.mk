################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/ilingeling.o \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/lglbnr.o \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/lglddtrace.o \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/lgldimacs.o \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/lglib.o \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/lglmain.o \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/lglmbt.o \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/lgluntrace.o \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/plingeling.o \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/treengeling.o 

C_SRCS += \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/blimc.c \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/ilingeling.c \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/lglbnr.c \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/lglddtrace.c \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/lgldimacs.c \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/lglib.c \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/lglmain.c \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/lglmbt.c \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/lgluntrace.c \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/plingeling.c \
../lib/boolector-1.6.0-with-sat-solvers/lingeling/treengeling.c 

OBJS += \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/blimc.o \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/ilingeling.o \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/lglbnr.o \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/lglddtrace.o \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/lgldimacs.o \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/lglib.o \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/lglmain.o \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/lglmbt.o \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/lgluntrace.o \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/plingeling.o \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/treengeling.o 

C_DEPS += \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/blimc.d \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/ilingeling.d \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/lglbnr.d \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/lglddtrace.d \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/lgldimacs.d \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/lglib.d \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/lglmain.d \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/lglmbt.d \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/lgluntrace.d \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/plingeling.d \
./lib/boolector-1.6.0-with-sat-solvers/lingeling/treengeling.d 


# Each subdirectory must supply rules for building sources it contributes
lib/boolector-1.6.0-with-sat-solvers/lingeling/%.o: ../lib/boolector-1.6.0-with-sat-solvers/lingeling/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


