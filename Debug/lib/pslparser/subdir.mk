################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_UPPER_SRCS += \
../lib/pslparser/PSL_Parser.C \
../lib/pslparser/PSL_ParserFunction.C \
../lib/pslparser/PSL_Scanner.C \
../lib/pslparser/PSL_ScannerFunction.C 

OBJS += \
./lib/pslparser/PSL_Parser.o \
./lib/pslparser/PSL_ParserFunction.o \
./lib/pslparser/PSL_Scanner.o \
./lib/pslparser/PSL_ScannerFunction.o 

C_UPPER_DEPS += \
./lib/pslparser/PSL_Parser.d \
./lib/pslparser/PSL_ParserFunction.d \
./lib/pslparser/PSL_Scanner.d \
./lib/pslparser/PSL_ScannerFunction.d 


# Each subdirectory must supply rules for building sources it contributes
lib/pslparser/%.o: ../lib/pslparser/%.C
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


