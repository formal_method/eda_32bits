################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../include/verilated.cpp \
../include/verilated_dpi.cpp \
../include/verilated_save.cpp \
../include/verilated_vcd_c.cpp \
../include/verilated_vcd_sc.cpp \
../include/verilated_vpi.cpp 

OBJS += \
./include/verilated.o \
./include/verilated_dpi.o \
./include/verilated_save.o \
./include/verilated_vcd_c.o \
./include/verilated_vcd_sc.o \
./include/verilated_vpi.o 

CPP_DEPS += \
./include/verilated.d \
./include/verilated_dpi.d \
./include/verilated_save.d \
./include/verilated_vcd_c.d \
./include/verilated_vcd_sc.d \
./include/verilated_vpi.d 


# Each subdirectory must supply rules for building sources it contributes
include/%.o: ../include/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


