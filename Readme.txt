NAME

	This is the FCKBK Package README file.

DISTRIBUTION

	This package is Copyright 2014-2015 by Lam S.Nguyen. (Report bugs to email
	lamnsk54@edabk.org.)

	FCKBK is free software; you can redistribute it and/or modify it.
	(See the documentation for more details.)

	This program is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

DESCRIPTION

    	FCKBK is a formal verification tool developed by BKIC laboratory, 
	Hanoi University of Science and Technology in order to formally 
	verify digital designs that are written in Verilog-HDL against 
	properties described in SVA.

SUPPORTED SYSTEMS

	FCKBK is developed and has primary testing on:

        	Ubuntu 12.04 Intel core I3 i386-linux-2.6.27, GCC 4.3.2

	FCKBK have also built on Fedora, CentOs.
	It should run with minor porting on any Linix-ish platform. FCKBK
	also works on Windows under Cygwin, and Windows under MinGW (gcc
	-mno-cygwin).

INSTALLATION
	A FCKBK-user-manual can be found in /Doc folder. Please follow instructions to install and use this tools 
	
