property test_cnt;
	wire clk, rst, en;
	wire [4:0] f;
	reg [4:0] g;
	@ (posedge clk) disable iff (!rst) ##1 (1'b1, g = f) ##0 en == 1'b1 [*4] |=> ##0 f == g + 5'b00100;
endproperty
