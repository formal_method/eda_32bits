property test_cnt;
	wire clk, rst, en;
	wire [4:0] f;
	@ (posedge clk) disable iff (!rst) ##1 f == 5'd1 ##0 en == 1'b1 [*4] |=> ##0 f == 5'd6;
endproperty
