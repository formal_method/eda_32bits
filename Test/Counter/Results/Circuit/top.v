module test_im(
	 input  clk,
	 input  rst,
	 input  en,
	 output [4:0] f
);

	 reg [4:0] PS_INPUT_6_o;
	 wire [4:0] ADD_8_o;
	 wire [4:0] ITE_9_o;
	 wire [4:0] ITE_11_o;
	 wire [4:0] PS_OUTPUT_12_o;

	 FCK_BUFF #(.WIDTH(5)) BUFF_5 (.out(f),.in(PS_INPUT_6_o));
	 FCK_ADD #(.WIDTH(5)) ADD_8 (.out(ADD_8_o),.in1(5'd1),.in2(PS_INPUT_6_o));
	 FCK_ITE #(.WIDTH(5)) ITE_9 (.out(ITE_9_o),.in1(en),.in2(ADD_8_o),.in3(PS_INPUT_6_o));
	 FCK_ITE #(.WIDTH(5)) ITE_11 (.out(ITE_11_o),.in1(rst),.in2(ITE_9_o),.in3(5'd0));
	 FCK_BUFF #(.WIDTH(5)) BUFF_12 (.out(PS_OUTPUT_12_o),.in(ITE_11_o));
	 FCK_DFF1 #(.WIDTH(5)) DFF_12 (.Q(PS_INPUT_6_o[4:0]),.D(PS_OUTPUT_12_o),.preset(5'd0),.clk(clk),.rst_n(rst));

endmodule

module FCK_DFF0 #(parameter WIDTH = 8)(
	input [WIDTH-1:0] D,
	input clk,rst_n,
	input [WIDTH-1:0] preset, 
	output [WIDTH-1:0] Q
);
	reg [WIDTH-1:0] Q;
	always@(posedge clk or posedge rst_n)
	begin
		if(rst_n) Q<=preset;
		else Q<=D;
	end
endmodule

module FCK_DFF1 #(parameter WIDTH = 8)(
	input [WIDTH-1:0] D,
	input clk,rst_n,
	input [WIDTH-1:0] preset, 
	output [WIDTH-1:0] Q
);
	reg [WIDTH-1:0] Q;
	always@(posedge clk or negedge rst_n)
	begin
		if(!rst_n) Q<=preset;
		else Q<=D;
	end
endmodule
module FCK_ADD #(parameter WIDTH=8)
(
	input [WIDTH-1:0] in1,in2,
	output [WIDTH-1:0] out
);
	assign out = in1+in2;
endmodule
module FCK_ITE #(parameter WIDTH=8)(
	input in1,
	input [WIDTH-1:0] in2,in3,
	output [WIDTH-1:0] out);

	assign out = in1 ? in2:in3;
endmodule
module FCK_BUFF #(parameter WIDTH=8)(
	input [WIDTH-1:0] in,
	output [WIDTH-1:0] out
);
	assign out = in;
endmodule
