module test(clk,en,rst,f);
	input clk, rst, en;
	output reg [4:0] f;
	
	always @(posedge clk, negedge rst)
		if (!rst) f<= 0;
		else if (en) f <= f + 5'd1;
			else f <= f;
endmodule
