property mip_lwadata;
	wire clock, reset;
	wire [31:0] InstMem_In;
	wire v_$_IF_Stall, v_$_ID_Stall, v_$_IF_Flush, v_$_ID_Exception_Flush, v_$_IF_Exception_Flush;
	wire [31:0] v_$_RegisterFile_$_registers7;
	wire [29:0] DataMem_In;
	reg [31:0] DataMem_In_3;

	@ (posedge clock) disable iff (reset) 
	##1 ((InstMem_In[20:16] == 5'd7)&&(InstMem_In[25:21] == 5'd2)&&(InstMem_In[31:26] == 6'b100011)) 		
	##0 ((!v_$_IF_Stall)&&(!v_$_ID_Stall)&&(!v_$_IF_Flush)&&(!v_$_ID_Exception_Flush)&&(!v_$_IF_Exception_Flush)) [*3] 
	##1 (1'b1, DataMem_In_3 = DataMem_In)
	|=> 
	##1 (v_$_RegisterFile_$_registers7 == DataMem_In_3);

endproperty