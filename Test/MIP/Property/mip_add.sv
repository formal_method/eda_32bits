property mip_add;
	wire clock, reset;
	wire InstMem_Ready;
	wire [31:0] InstMem_In;
	wire v_$_IF_Stall, v_$_ID_Stall, v_$_IF_Flush, v_$_ID_Exception_Flush, v_$_IF_Exception_Flush, v_$_WB_Stall;
	wire [31:0] v_$_RegisterFile_$_registers11, v_$_RegisterFile_$_registers2, v_$_RegisterFile_$_registers7;
	reg [31:0] reg2, reg7;

	@ (posedge clock) disable iff (reset || InstMem_Ready) ##1 ((InstMem_In[15:11] == 5'd11)&&(InstMem_In[20:16] == 5'd7)&&(InstMem_In[25:21] == 5'd2)&&(InstMem_In[5:0] == 6'h20)&&(InstMem_In[31:26] == 6'd0)) ##0 ((!v_$_IF_Stall)&&(!v_$_ID_Stall)&&(!v_$_IF_Flush)&&(!v_$_ID_Exception_Flush)&&(!v_$_IF_Exception_Flush)) [*3] ##1 (!v_$_WB_Stall) ##1 (1'b1, reg2 = v_$_RegisterFile_$_registers2, reg7 = v_$_RegisterFile_$_registers7) |=> ##0 v_$_RegisterFile_$_registers11 == reg2 + reg7 ;
	
endproperty
