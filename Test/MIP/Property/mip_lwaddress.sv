property mip_lwaddress;
	wire clock, reset;
	wire [31:0] InstMem_In;
	wire v_$_IF_Stall, v_$_ID_Stall, v_$_IF_Flush, v_$_ID_Exception_Flush, v_$_IF_Exception_Flush;
	wire [31:0] v_$_RegisterFile_$_registers2;
	wire [29:0] DataMem_Address;
	reg [31:0] reg2_3;
	reg [31:0] shamt;
	reg [29:0] DataMem_Address_3;
	reg [31:0] sum1, sum2;

	@ (posedge clock) disable iff (reset) 
	##1 ((InstMem_In[20:16] == 5'd7)&&(InstMem_In[25:21] == 5'd2)&&(InstMem_In[31:26] == 6'b100011)) 
	##0 (1'b1, shamt = {{16{InstMem_In[15:15]}},InstMem_In[15:0]}) 		
	##0 ((!v_$_IF_Stall)&&(!v_$_ID_Stall)&&(!v_$_IF_Flush)&&(!v_$_ID_Exception_Flush)&&(!v_$_IF_Exception_Flush)) [*3] 
	|=> 
	##0 (1'b1, DataMem_Address_3 = DataMem_Address, reg2_3 = v_$_RegisterFile_$_registers2)
	##1 (1'b1, sum1 = (v_$_RegisterFile_$_registers2 + shamt), sum2 = (reg2_3 + shamt))				
	##0 ((DataMem_Address_3 == sum1[31:2])||(DataMem_Address_3 == sum2[31:2])) ;

endproperty
