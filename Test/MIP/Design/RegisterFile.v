`timescale 1ns / 1ps
/*
 * File         : RegisterFile.v
 * Project      : University of Utah, XUM Project MIPS32 core
 * Creator(s)   : Grant Ayers (ayers@cs.utah.edu)
 *
 * Modification History:
 *   Rev   Date         Initials  Description of Change
 *   1.0   7-Jun-2011   GEA       Initial design.
 *
 * Standards/Formatting:
 *   Verilog 2001, 4 soft tab, wide column.
 *
 * Description:
 *   A Register File for a MIPS processor. Contains 32 general-purpose
 *   32-bit wide registers and two read ports. Register 0 always reads
 *   as zero.
 */
module RegisterFile(
    input  clock,
    input  reset,
    input  [4:0]  ReadReg1, ReadReg2, WriteReg,
    input  [31:0] WriteData,
    input  RegWrite,
    output [31:0] ReadData1, ReadData2
    );

    // Register file of 32 32-bit registers. Register 0 is hardwired to 0s

    reg [31:0] registers1;
    reg [31:0] registers2;
    reg [31:0] registers3;
    reg [31:0] registers4;
    reg [31:0] registers5;
    reg [31:0] registers6;
    reg [31:0] registers7;
    reg [31:0] registers8;
    reg [31:0] registers9;
    reg [31:0] registers10;
    reg [31:0] registers11;
    reg [31:0] registers12;
    reg [31:0] registers13;
    reg [31:0] registers14;
    reg [31:0] registers15;
    reg [31:0] registers16;
    reg [31:0] registers17;
    reg [31:0] registers18;
    reg [31:0] registers19;
    reg [31:0] registers20;
    reg [31:0] registers21;
    reg [31:0] registers22;
    reg [31:0] registers23;
    reg [31:0] registers24;
    reg [31:0] registers25;
    reg [31:0] registers26;
    reg [31:0] registers27;
    reg [31:0] registers28;
    reg [31:0] registers29;
    reg [31:0] registers30;
    reg [31:0] registers31;

	wire [31:0] mem_read1, mem_read2;

	
/*
    // Initialize all to zero
    integer i;
    initial begin
        for (i=1; i<32; i=i+1) begin
            registers[i] <= 0;
        end
    end
*/
    // Sequential (clocked) write.
    // 'WriteReg' is the register index to write. 'RegWrite' is the command.
    always @(posedge clock) begin
        if (reset) begin
		registers1 <=0;
		registers2 <=0;
		registers3 <=0;
		registers4 <=0;
		registers5 <=0;
		registers6 <=0;
		registers7 <=0;
		registers8 <=0;
		registers9 <=0;
		registers10 <=0;
		registers11 <=0;
		registers12 <=0;
		registers13 <=0;
		registers14 <=0;
		registers15 <=0;
		registers16 <=0;
		registers17 <=0;
		registers18 <=0;
		registers19 <=0;
		registers20 <=0;
		registers21 <=0;
		registers22 <=0;
		registers23 <=0;
		registers24 <=0;
		registers25 <=0;
		registers26 <=0;
		registers27 <=0;
		registers28 <=0;
		registers29 <=0;
		registers30 <=0;
		registers31 <=0;
            /*for (i=1; i<32; i=i+1) begin
                registers[i] <= 0; */
        end
        else begin
            if (WriteReg != 0)
		case (WriteReg)
		1 : registers1 <= (RegWrite) ? WriteData : registers1;
		2 : registers2 <= (RegWrite) ? WriteData : registers2;
		3 : registers3 <= (RegWrite) ? WriteData : registers3;
		4 : registers4 <= (RegWrite) ? WriteData : registers4;
		5 : registers5 <= (RegWrite) ? WriteData : registers5;
		6 : registers6 <= (RegWrite) ? WriteData : registers6;
		7 : registers7 <= (RegWrite) ? WriteData : registers7;
		8 : registers8 <= (RegWrite) ? WriteData : registers8;
		9 : registers9 <= (RegWrite) ? WriteData : registers9;
		10 : registers10 <= (RegWrite) ? WriteData : registers10;
		11 : registers11 <= (RegWrite) ? WriteData : registers11;
		12 : registers12 <= (RegWrite) ? WriteData : registers12;
		13 : registers13 <= (RegWrite) ? WriteData : registers13;
		14 : registers14 <= (RegWrite) ? WriteData : registers14;
		15 : registers15 <= (RegWrite) ? WriteData : registers15;
		16 : registers16 <= (RegWrite) ? WriteData : registers16;
		17 : registers17 <= (RegWrite) ? WriteData : registers17;
		18 : registers18 <= (RegWrite) ? WriteData : registers18;
		19 : registers19 <= (RegWrite) ? WriteData : registers19;
		20 : registers20 <= (RegWrite) ? WriteData : registers20;
		21 : registers21 <= (RegWrite) ? WriteData : registers21;
		22 : registers22 <= (RegWrite) ? WriteData : registers22;
		23 : registers23 <= (RegWrite) ? WriteData : registers23;
		24 : registers24 <= (RegWrite) ? WriteData : registers24;
		25 : registers25 <= (RegWrite) ? WriteData : registers25;
		26 : registers26 <= (RegWrite) ? WriteData : registers26;
		27 : registers27 <= (RegWrite) ? WriteData : registers27;
		28 : registers28 <= (RegWrite) ? WriteData : registers28;
		29 : registers29 <= (RegWrite) ? WriteData : registers29;
		30 : registers30 <= (RegWrite) ? WriteData : registers30;
		31 : registers31 <= (RegWrite) ? WriteData : registers31;
                //registers[WriteReg] <= (RegWrite) ? WriteData : registers[WriteReg];
		endcase
        end
    end
    
	Mem_read read1 (registers1,registers2,registers3,registers4,registers5,
			registers6,registers7,registers8,registers9,registers10,
			registers11,registers12,registers13,registers14,registers15,
			registers16,registers17,registers18,registers19,registers20,
			registers21,registers22,registers23,registers24,registers25,
			registers26,registers27,registers28,registers29,registers30,registers31,
			ReadReg1,mem_read1);

	Mem_read read2 (registers1,registers2,registers3,registers4,registers5,
			registers6,registers7,registers8,registers9,registers10,
			registers11,registers12,registers13,registers14,registers15,
			registers16,registers17,registers18,registers19,registers20,
			registers21,registers22,registers23,registers24,registers25,
			registers26,registers27,registers28,registers29,registers30,registers31,
			ReadReg2,mem_read2);

    // Combinatorial Read. Register 0 is all 0s.
    assign ReadData1 = (ReadReg1 == 0) ? 32'h00000000 : mem_read1;
    assign ReadData2 = (ReadReg2 == 0) ? 32'h00000000 : mem_read2;

endmodule

module Mem_read (
    input [31:0] registers1,
    input [31:0] registers2,
    input [31:0] registers3,
    input [31:0] registers4,
    input [31:0] registers5,
    input [31:0] registers6,
    input [31:0] registers7,
    input [31:0] registers8,
    input [31:0] registers9,
    input [31:0] registers10,
    input [31:0] registers11,
    input [31:0] registers12,
    input [31:0] registers13,
    input [31:0] registers14,
    input [31:0] registers15,
    input [31:0] registers16,
    input [31:0] registers17,
    input [31:0] registers18,
    input [31:0] registers19,
    input [31:0] registers20,
    input [31:0] registers21,
    input [31:0] registers22,
    input [31:0] registers23,
    input [31:0] registers24,
    input [31:0] registers25,
    input [31:0] registers26,
    input [31:0] registers27,
    input [31:0] registers28,
    input [31:0] registers29,
    input [31:0] registers30,
    input [31:0] registers31,
    input [4:0] addr,
    output reg [31:0] out);

	always @(*)
		case(addr)
			1: out = registers1;
			2: out = registers2;
			3: out = registers3;
			4: out = registers4;
			5: out = registers5;
			6: out = registers6;
			7: out = registers7;
			8: out = registers8;
			9: out = registers9;
			10: out = registers10;
			11: out = registers11;
			12: out = registers12;
			13: out = registers13;
			14: out = registers14;
			15: out = registers15;
			16: out = registers16;
			17: out = registers17;
			18: out = registers18;
			19: out = registers19;
			20: out = registers20;
			21: out = registers21;
			22: out = registers22;
			23: out = registers23;
			24: out = registers24;
			25: out = registers25;
			26: out = registers26;
			27: out = registers27;
			28: out = registers28;
			29: out = registers29;
			30: out = registers30;
			31: out = registers31;
			default : out = 32'd0;
		endcase
endmodule

