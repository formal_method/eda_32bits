
/*
 * File         : CPZero.v
 * Project      : University of Utah, XUM Project MIPS32 core
 * Creator(s)   : Grant Ayers (ayers@cs.utah.edu)
 *
 * Modification History:
 *   Rev   Date         Initials  Description of Change
 *   1.0   16-Sep-2011  GEA       Initial design.
 *   2.0   14-May-2012  GEA       Complete rework.
 *
 * Standards/Formatting:
 *   Verilog 2001, 4 soft tab, wide column.
 *
 * Description:
 *   The MIPS-32 Coprocessor 0 (CP0). This is the processor management unit that allows
 *   interrupts, traps, system calls, and other exceptions. It distinguishes
 *   user and kernel modes, provides status information, and can override program
 *   flow. This processor is designed for "bare metal" memory access and thus does
 *   not have virtual memory hardware as a part of it. However, the subset of CP0
 *   is MIPS-32-compliant.
 */
module CPZero(
    input  clock,
    //-- CP0 Functionality --//
    input  Mfc0,                    // CPU instruction is Mfc0
    input  Mtc0,                    // CPU instruction is Mtc0
    input  IF_Stall,
    input  ID_Stall,                // Commits are not made during stalls
    input  COP1,                    // Instruction for Coprocessor 1
    input  COP2,                    // Instruction for Coprocessor 2
    input  COP3,                    // Instruction for Coprocessor 3
    input  ERET,                    // Instruction is ERET (Exception Return)
    input  [4:0] Rd,                // Specifies Cp0 register
    input  [2:0] Sel,               // Specifies Cp0 'select'
    input  [31:0] Reg_In,           // Data from GP register to replace CP0 register
    //-- Hw Interrupts --//
    input  [4:0] Int,               // Five hardware interrupts external to the processor
    //-- Exceptions --//
    input  reset,                   // Cold Reset (EXC_Reset)
//  input  EXC_SReset,              // Soft Reset (not implemented)
    input  EXC_NMI,                 // Non-Maskable Interrupt
    input  EXC_AdIF,                // Address Error Exception from i-fetch (mapped to AdEL)
    input  EXC_AdEL,                // Address Error Exception from data memory load
    input  EXC_AdES,                // Address Error Exception from data memory store
    input  EXC_Ov,                  // Integer Overflow Exception
    input  EXC_Tr,                  // Trap Exception
    input  EXC_Sys,                 // System Call Exception
    input  EXC_Bp,                  // Breakpoint Exception
    input  EXC_RI,                  // Reserved Instruction Exception
    //-- Exception Data --//
    input  [31:0] ID_RestartPC,     // PC for exception, whether PC of instruction or of branch (PC-4) if BDS
    input  [31:0] EX_RestartPC,     // Same as 'ID_RestartPC' but in EX stage
    input  [31:0] M_RestartPC,      // Same as 'ID_RestartPC' but in MEM stage
    input  ID_IsFlushed,
    input  IF_IsBD,                 // Indicator of IF exception being a branch delay slot instruction
    input  ID_IsBD,                 // Indicator of ID exception being a branch delay slot instruction
    input  EX_IsBD,                 // Indicator of EX exception being a branch delay slot instruction
    input  M_IsBD,                  // Indicator of M  exception being a branch delay slot instruction
    input  [31:0] BadAddr_M,        // Bad 'Virtual' Address for exceptions AdEL, AdES in MEM stage
    input  [31:0] BadAddr_IF,       // Bad 'Virtual' Address for AdIF (i.e. AdEL) in IF stage
    input  ID_CanErr,               // Cumulative signal, i.e. (ID_ID_CanErr | ID_EX_CanErr | ID_M_CanErr)
    input  EX_CanErr,               // Cumulative signal, i.e. (EX_EX_CanErr | EX_M_CanErr)
    input  M_CanErr,                // Memory stage can error (i.e. cause exception)
    //-- Exception Control Flow --/
    output reg [31:0] Reg_Out,      // Data from CP0 register for GP register
    output KernelMode,              // Kernel mode indicator for pipeline transit
    output ReverseEndian,           // Reverse Endian memory indicator for User Mode
    output IF_Exception_Stall,
    output ID_Exception_Stall,
    output EX_Exception_Stall,
    output M_Exception_Stall,
    output IF_Exception_Flush,
    output ID_Exception_Flush,
    output EX_Exception_Flush,
    output M_Exception_Flush,
    output Exc_PC_Sel,              // Mux selector for exception PC override
    output reg [31:0] Exc_PC_Out,   // Address for PC at the beginning of an exception
    output [7:0] IP                 // Pending Interrupts from Cause register (for diagnostic purposes)
    );

    	parameter [31:0] EXC_Vector_Base_Reset          = 32'h0000_0010;    // MIPS Standard is 0xBFC0_0000
	parameter [31:0] EXC_Vector_Base_Other_NoBoot   = 32'h0000_0000;    // MIPS Standard is 0x8000_0000
	parameter [31:0] EXC_Vector_Base_Other_Boot     = 32'h0000_0000;    // MIPS Standard is 0xBFC0_0200
	parameter [31:0] EXC_Vector_Offset_General      = 32'h0000_0000;    // MIPS Standard is 0x0000_0180
	parameter [31:0] EXC_Vector_Offset_Special      = 32'h0000_0008;    // MIPS Standard is 0x0000_0200
	parameter [31:0] UMem_Lower = 32'h08000000;
	parameter Big_Endian = 1;
	parameter [5:0] Op_Type_R   = 6'b00_0000;  // Standard R-Type instructions
	parameter [5:0] Op_Type_R2  = 6'b01_1100;  // Extended R-Like instructions
	parameter [5:0] Op_Type_BI  = 6'b00_0001;  // Branch/Trap extended instructions
	parameter [5:0] Op_Type_CP0 = 6'b01_0000;  // Coprocessor 0 instructions
	parameter [5:0] Op_Type_CP1 = 6'b01_0001;  // Coprocessor 1 instructions (not implemented)
	parameter [5:0] Op_Type_CP2 = 6'b01_0010;  // Coprocessor 2 instructions (not implemented)
	parameter [5:0] Op_Type_CP3 = 6'b01_0011;  // Coprocessor 3 instructions (not implemented)
	parameter [5:0] Op_Add      = Op_Type_R;
	parameter [5:0] Op_Addi     = 6'b00_1000;
	parameter [5:0] Op_Addiu    = 6'b00_1001;
	parameter [5:0] Op_Addu     = Op_Type_R;
	parameter [5:0] Op_And      = Op_Type_R;
	parameter [5:0] Op_Andi     = 6'b00_1100;
	parameter [5:0] Op_Beq      = 6'b00_0100;
	parameter [5:0] Op_Bgez     = Op_Type_BI;
	parameter [5:0] Op_Bgezal   = Op_Type_BI;
	parameter [5:0] Op_Bgtz     = 6'b00_0111;
	parameter [5:0] Op_Blez     = 6'b00_0110;
	parameter [5:0] Op_Bltz     = Op_Type_BI;
	parameter [5:0] Op_Bltzal   = Op_Type_BI;
	parameter [5:0] Op_Bne      = 6'b00_0101;
	parameter [5:0] Op_Break    = Op_Type_R;
	parameter [5:0] Op_Clo      = Op_Type_R2;
	parameter [5:0] Op_Clz      = Op_Type_R2;
	parameter [5:0] Op_Div      = Op_Type_R;
	parameter [5:0] Op_Divu     = Op_Type_R;
	parameter [5:0] Op_Eret     = Op_Type_CP0;
	parameter [5:0] Op_J        = 6'b00_0010;
	parameter [5:0] Op_Jal      = 6'b00_0011;
	parameter [5:0] Op_Jalr     = Op_Type_R;
	parameter [5:0] Op_Jr       = Op_Type_R;
	parameter [5:0] Op_Lb       = 6'b10_0000;
	parameter [5:0] Op_Lbu      = 6'b10_0100;
	parameter [5:0] Op_Lh       = 6'b10_0001;
	parameter [5:0] Op_Lhu      = 6'b10_0101;
	parameter [5:0] Op_Ll       = 6'b11_0000;
	parameter [5:0] Op_Lui      = 6'b00_1111;
	parameter [5:0] Op_Lw       = 6'b10_0011;
	parameter [5:0] Op_Lwl      = 6'b10_0010;
	parameter [5:0] Op_Lwr      = 6'b10_0110;
	parameter [5:0] Op_Madd     = Op_Type_R2;
	parameter [5:0] Op_Maddu    = Op_Type_R2;
	parameter [5:0] Op_Mfc0     = Op_Type_CP0;
	parameter [5:0] Op_Mfhi     = Op_Type_R;
	parameter [5:0] Op_Mflo     = Op_Type_R;
	parameter [5:0] Op_Movn     = Op_Type_R;
	parameter [5:0] Op_Movz     = Op_Type_R;
	parameter [5:0] Op_Msub     = Op_Type_R2;
	parameter [5:0] Op_Msubu    = Op_Type_R2;
	parameter [5:0] Op_Mtc0     = Op_Type_CP0;
	parameter [5:0] Op_Mthi     = Op_Type_R;
	parameter [5:0] Op_Mtlo     = Op_Type_R;
	parameter [5:0] Op_Mul      = Op_Type_R2;
	parameter [5:0] Op_Mult     = Op_Type_R;
	parameter [5:0] Op_Multu    = Op_Type_R;
	parameter [5:0] Op_Nor      = Op_Type_R;
	parameter [5:0] Op_Or       = Op_Type_R;
	parameter [5:0] Op_Ori      = 6'b00_1101;
	parameter [5:0] Op_Pref     = 6'b11_0011; // Prefetch does nothing in this implementation.
	parameter [5:0] Op_Sb       = 6'b10_1000;
	parameter [5:0] Op_Sc       = 6'b11_1000;
	parameter [5:0] Op_Sh       = 6'b10_1001;
	parameter [5:0] Op_Sll      = Op_Type_R;
	parameter [5:0] Op_Sllv     = Op_Type_R;
	parameter [5:0] Op_Slt      = Op_Type_R;
	parameter [5:0] Op_Slti     = 6'b00_1010;
	parameter [5:0] Op_Sltiu    = 6'b00_1011;
	parameter [5:0] Op_Sltu     = Op_Type_R;
	parameter [5:0] Op_Sra      = Op_Type_R;
	parameter [5:0] Op_Srav     = Op_Type_R;
	parameter [5:0] Op_Srl      = Op_Type_R;
	parameter [5:0] Op_Srlv     = Op_Type_R;
	parameter [5:0] Op_Sub      = Op_Type_R;
	parameter [5:0] Op_Subu     = Op_Type_R;
	parameter [5:0] Op_Sw       = 6'b10_1011;
	parameter [5:0] Op_Swl      = 6'b10_1010;
	parameter [5:0] Op_Swr      = 6'b10_1110;
	parameter [5:0] Op_Syscall  = Op_Type_R;
	parameter [5:0] Op_Teq      = Op_Type_R;
	parameter [5:0] Op_Teqi     = Op_Type_BI;
	parameter [5:0] Op_Tge      = Op_Type_R;
	parameter [5:0] Op_Tgei     = Op_Type_BI;
	parameter [5:0] Op_Tgeiu    = Op_Type_BI;
	parameter [5:0] Op_Tgeu     = Op_Type_R;
	parameter [5:0] Op_Tlt      = Op_Type_R;
	parameter [5:0] Op_Tlti     = Op_Type_BI;
	parameter [5:0] Op_Tltiu    = Op_Type_BI;
	parameter [5:0] Op_Tltu     = Op_Type_R;
	parameter [5:0] Op_Tne      = Op_Type_R;
	parameter [5:0] Op_Tnei     = Op_Type_BI;
	parameter [5:0] Op_Xor      = Op_Type_R;
	parameter [5:0] Op_Xori     = 6'b00_1110;

	/* Op Code Rt fields for Branches & Traps */
	parameter [4:0] OpRt_Bgez   = 5'b00001;
	parameter [4:0] OpRt_Bgezal = 5'b10001;
	parameter [4:0] OpRt_Bltz   = 5'b00000;
	parameter [4:0] OpRt_Bltzal = 5'b10000;
	parameter [4:0] OpRt_Teqi   = 5'b01100;
	parameter [4:0] OpRt_Tgei   = 5'b01000;
	parameter [4:0] OpRt_Tgeiu  = 5'b01001;
	parameter [4:0] OpRt_Tlti   = 5'b01010;
	parameter [4:0] OpRt_Tltiu  = 5'b01011;
	parameter [4:0] OpRt_Tnei   = 5'b01110;

	/* Op Code Rs fields for Coprocessors */
	parameter [4:0] OpRs_MF     = 5'b00000;
	parameter [4:0] OpRs_MT     = 5'b00100;

	/* Special handling for ERET */
	parameter [4:0] OpRs_ERET   = 5'b10000;
	parameter [5:0] Funct_ERET  = 6'b011000;

	/* Function Codes for R-Type Op Codes */
	parameter [5:0] Funct_Add     = 6'b10_0000;
	parameter [5:0] Funct_Addu    = 6'b10_0001;
	parameter [5:0] Funct_And     = 6'b10_0100;
	parameter [5:0] Funct_Break   = 6'b00_1101;
	parameter [5:0] Funct_Clo     = 6'b10_0001; // same as Addu
	parameter [5:0] Funct_Clz     = 6'b10_0000; // same as Add
	parameter [5:0] Funct_Div     = 6'b01_1010;
	parameter [5:0] Funct_Divu    = 6'b01_1011;
	parameter [5:0] Funct_Jr      = 6'b00_1000;
	parameter [5:0] Funct_Jalr    = 6'b00_1001;
	parameter [5:0] Funct_Madd    = 6'b00_0000;
	parameter [5:0] Funct_Maddu   = 6'b00_0001;
	parameter [5:0] Funct_Mfhi    = 6'b01_0000;
	parameter [5:0] Funct_Mflo    = 6'b01_0010;
	parameter [5:0] Funct_Movn    = 6'b00_1011;
	parameter [5:0] Funct_Movz    = 6'b00_1010;
	parameter [5:0] Funct_Msub    = 6'b00_0100; // same as Sllv
	parameter [5:0] Funct_Msubu   = 6'b00_0101;
	parameter [5:0] Funct_Mthi    = 6'b01_0001;
	parameter [5:0] Funct_Mtlo    = 6'b01_0011;
	parameter [5:0] Funct_Mul     = 6'b00_0010; // same as Srl
	parameter [5:0] Funct_Mult    = 6'b01_1000;
	parameter [5:0] Funct_Multu   = 6'b01_1001;
	parameter [5:0] Funct_Nor     = 6'b10_0111;
	parameter [5:0] Funct_Or      = 6'b10_0101;
	parameter [5:0] Funct_Sll     = 6'b00_0000;
	parameter [5:0] Funct_Sllv    = 6'b00_0100;
	parameter [5:0] Funct_Slt     = 6'b10_1010;
	parameter [5:0] Funct_Sltu    = 6'b10_1011;
	parameter [5:0] Funct_Sra     = 6'b00_0011;
	parameter [5:0] Funct_Srav    = 6'b00_0111;
	parameter [5:0] Funct_Srl     = 6'b00_0010;
	parameter [5:0] Funct_Srlv    = 6'b00_0110;
	parameter [5:0] Funct_Sub     = 6'b10_0010;
	parameter [5:0] Funct_Subu    = 6'b10_0011;
	parameter [5:0] Funct_Syscall = 6'b00_1100;
	parameter [5:0] Funct_Teq     = 6'b11_0100;
	parameter [5:0] Funct_Tge     = 6'b11_0000;
	parameter [5:0] Funct_Tgeu    = 6'b11_0001;
	parameter [5:0] Funct_Tlt     = 6'b11_0010;
	parameter [5:0] Funct_Tltu    = 6'b11_0011;
	parameter [5:0] Funct_Tne     = 6'b11_0110;
	parameter [5:0] Funct_Xor     = 6'b10_0110;

	/* ALU Operations (Implementation) */
	parameter [4:0] AluOp_Add    = 5'd1;
	parameter [4:0] AluOp_Addu   = 5'd0;
	parameter [4:0] AluOp_And    = 5'd2;
	parameter [4:0] AluOp_Clo    = 5'd3;
	parameter [4:0] AluOp_Clz    = 5'd4;
	parameter [4:0] AluOp_Div    = 5'd5;
	parameter [4:0] AluOp_Divu   = 5'd6;
	parameter [4:0] AluOp_Madd   = 5'd7;
	parameter [4:0] AluOp_Maddu  = 5'd8;
	parameter [4:0] AluOp_Mfhi   = 5'd9;
	parameter [4:0] AluOp_Mflo   = 5'd10;
	parameter [4:0] AluOp_Msub   = 5'd13;
	parameter [4:0] AluOp_Msubu  = 5'd14;
	parameter [4:0] AluOp_Mthi   = 5'd11;
	parameter [4:0] AluOp_Mtlo   = 5'd12;
	parameter [4:0] AluOp_Mul    = 5'd15;
	parameter [4:0] AluOp_Mult   = 5'd16;
	parameter [4:0] AluOp_Multu  = 5'd17;
	parameter [4:0] AluOp_Nor    = 5'd18;
	parameter [4:0] AluOp_Or     = 5'd19;
	parameter [4:0] AluOp_Sll    = 5'd20;
	parameter [4:0] AluOp_Sllc   = 5'd21;  // Move this if another AluOp is needed
	parameter [4:0] AluOp_Sllv   = 5'd22;
	parameter [4:0] AluOp_Slt    = 5'd23;
	parameter [4:0] AluOp_Sltu   = 5'd24;
	parameter [4:0] AluOp_Sra    = 5'd25;
	parameter [4:0] AluOp_Srav   = 5'd26;
	parameter [4:0] AluOp_Srl    = 5'd27;
	parameter [4:0] AluOp_Srlv   = 5'd28;
	parameter [4:0] AluOp_Sub    = 5'd29;
	parameter [4:0] AluOp_Subu   = 5'd30;
	parameter [4:0] AluOp_Xor    = 5'd31;
	parameter [15:0] DP_None        = 16'b000_00000_000000_00;    // Instructions which require nothing of the main datapath.
	parameter [15:0] DP_RType       = 16'b000_00001_000000_10;    // Standard R-Type
	parameter [15:0] DP_IType       = 16'b000_10000_000000_10;    // Standard I-Type
	parameter [15:0] DP_Branch      = 16'b100_00000_000000_00;    // Standard Branch
	parameter [15:0] DP_BranchLink  = 16'b101_00000_000000_10;    // Branch and Link
	parameter [15:0] DP_HiLoWr      = 16'b000_00000_000000_00;    // Write to Hi/Lo ALU register (Div,Divu,Mult,Multu,Mthi,Mtlo). Currently 'DP_None'.
	parameter [15:0] DP_Jump        = 16'b010_00000_000000_00;    // Standard Jump
	parameter [15:0] DP_JumpLink    = 16'b011_00000_000000_10;    // Jump and Link
	parameter [15:0] DP_JumpLinkReg = 16'b111_00000_000000_10;    // Jump and Link Register
	parameter [15:0] DP_JumpReg     = 16'b110_00000_000000_00;    // Jump Register
	parameter [15:0] DP_LoadByteS   = 16'b000_10000_010011_11;    // Load Byte Signed
	parameter [15:0] DP_LoadByteU   = 16'b000_10000_010010_11;    // Load Byte Unsigned
	parameter [15:0] DP_LoadHalfS   = 16'b000_10000_010101_11;    // Load Half Signed
	parameter [15:0] DP_LoadHalfU   = 16'b000_10000_010100_11;    // Load Half Unsigned
	parameter [15:0] DP_LoadWord    = 16'b000_10000_010000_11;    // Load Word
	parameter [15:0] DP_ExtWrRt     = 16'b000_00000_000000_10;    // A DP-external write to Rt
	parameter [15:0] DP_ExtWrRd     = 16'b000_00001_000000_10;    // A DP-external write to Rd
	parameter [15:0] DP_Movc        = 16'b000_01001_000000_10;    // Conditional Move
	parameter [15:0] DP_LoadLinked  = 16'b000_10000_110000_11;    // Load Linked
	parameter [15:0] DP_StoreCond   = 16'b000_10000_101000_11;    // Store Conditional
	parameter [15:0] DP_StoreByte   = 16'b000_10000_001010_00;    // Store Byte
	parameter [15:0] DP_StoreHalf   = 16'b000_10000_001100_00;    // Store Half
	parameter [15:0] DP_StoreWord   = 16'b000_10000_001000_00;    // Store Word
	parameter [15:0] DP_TrapRegCNZ  = 16'b000_00110_000000_00;    // Trap using Rs and Rt,  non-zero ALU (Tlt,  Tltu,  Tne)
	parameter [15:0] DP_TrapRegCZ   = 16'b000_00100_000000_00;    // Trap using RS and Rt,  zero ALU     (Teq,  Tge,   Tgeu)
	parameter [15:0] DP_TrapImmCNZ  = 16'b000_10110_000000_00;    // Trap using Rs and Imm, non-zero ALU (Tlti, Tltiu, Tnei)
	parameter [15:0] DP_TrapImmCZ   = 16'b000_10100_000000_00;    // Trap using Rs and Imm, zero ALU     (Teqi, Tgei,  Tgeiu)
	//--------------------------------------------------------
	parameter [15:0] DP_Add     = DP_RType;
	parameter [15:0] DP_Addi    = DP_IType;
	parameter [15:0] DP_Addiu   = DP_IType;
	parameter [15:0] DP_Addu    = DP_RType;
	parameter [15:0] DP_And     = DP_RType;
	parameter [15:0] DP_Andi    = DP_IType;
	parameter [15:0] DP_Beq     = DP_Branch;
	parameter [15:0] DP_Bgez    = DP_Branch;
	parameter [15:0] DP_Bgezal  = DP_BranchLink;
	parameter [15:0] DP_Bgtz    = DP_Branch;
	parameter [15:0] DP_Blez    = DP_Branch;
	parameter [15:0] DP_Bltz    = DP_Branch;
	parameter [15:0] DP_Bltzal  = DP_BranchLink;
	parameter [15:0] DP_Bne     = DP_Branch;
	parameter [15:0] DP_Break   = DP_None;
	parameter [15:0] DP_Clo     = DP_RType;
	parameter [15:0] DP_Clz     = DP_RType;
	parameter [15:0] DP_Div     = DP_HiLoWr;
	parameter [15:0] DP_Divu    = DP_HiLoWr;
	parameter [15:0] DP_Eret    = DP_None;
	parameter [15:0] DP_J       = DP_Jump;
	parameter [15:0] DP_Jal     = DP_JumpLink;
	parameter [15:0] DP_Jalr    = DP_JumpLinkReg;
	parameter [15:0] DP_Jr      = DP_JumpReg;
	parameter [15:0] DP_Lb      = DP_LoadByteS;
	parameter [15:0] DP_Lbu     = DP_LoadByteU;
	parameter [15:0] DP_Lh      = DP_LoadHalfS;
	parameter [15:0] DP_Lhu     = DP_LoadHalfU;
	parameter [15:0] DP_Ll      = DP_LoadLinked;
	parameter [15:0] DP_Lui     = DP_IType;
	parameter [15:0] DP_Lw      = DP_LoadWord;
	parameter [15:0] DP_Lwl     = DP_LoadWord;
	parameter [15:0] DP_Lwr     = DP_LoadWord;
	parameter [15:0] DP_Madd    = DP_HiLoWr;
	parameter [15:0] DP_Maddu   = DP_HiLoWr;
	parameter [15:0] DP_Mfc0    = DP_ExtWrRt;
	parameter [15:0] DP_Mfhi    = DP_ExtWrRd;
	parameter [15:0] DP_Mflo    = DP_ExtWrRd;
	parameter [15:0] DP_Movn    = DP_Movc;
	parameter [15:0] DP_Movz    = DP_Movc;
	parameter [15:0] DP_Msub    = DP_HiLoWr;
	parameter [15:0] DP_Msubu   = DP_HiLoWr;
	parameter [15:0] DP_Mtc0    = DP_None;
	parameter [15:0] DP_Mthi    = DP_HiLoWr;
	parameter [15:0] DP_Mtlo    = DP_HiLoWr;
	parameter [15:0] DP_Mul     = DP_RType;
	parameter [15:0] DP_Mult    = DP_HiLoWr;
	parameter [15:0] DP_Multu   = DP_HiLoWr;
	parameter [15:0] DP_Nor     = DP_RType;
	parameter [15:0] DP_Or      = DP_RType;
	parameter [15:0] DP_Ori     = DP_IType;
	parameter [15:0] DP_Pref    = DP_None; // Not Implemented
	parameter [15:0] DP_Sb      = DP_StoreByte;
	parameter [15:0] DP_Sc      = DP_StoreCond;
	parameter [15:0] DP_Sh      = DP_StoreHalf;
	parameter [15:0] DP_Sll     = DP_RType;
	parameter [15:0] DP_Sllv    = DP_RType;
	parameter [15:0] DP_Slt     = DP_RType;
	parameter [15:0] DP_Slti    = DP_IType;
	parameter [15:0] DP_Sltiu   = DP_IType;
	parameter [15:0] DP_Sltu    = DP_RType;
	parameter [15:0] DP_Sra     = DP_RType;
	parameter [15:0] DP_Srav    = DP_RType;
	parameter [15:0] DP_Srl     = DP_RType;
	parameter [15:0] DP_Srlv    = DP_RType;
	parameter [15:0] DP_Sub     = DP_RType;
	parameter [15:0] DP_Subu    = DP_RType;
	parameter [15:0] DP_Sw      = DP_StoreWord;
	parameter [15:0] DP_Swl     = DP_StoreWord;
	parameter [15:0] DP_Swr     = DP_StoreWord;
	parameter [15:0] DP_Syscall = DP_None;
	parameter [15:0] DP_Teq     = DP_TrapRegCZ;
	parameter [15:0] DP_Teqi    = DP_TrapImmCZ;
	parameter [15:0] DP_Tge     = DP_TrapRegCZ;
	parameter [15:0] DP_Tgei    = DP_TrapImmCZ;
	parameter [15:0] DP_Tgeiu   = DP_TrapImmCZ;
	parameter [15:0] DP_Tgeu    = DP_TrapRegCZ;
	parameter [15:0] DP_Tlt     = DP_TrapRegCNZ;
	parameter [15:0] DP_Tlti    = DP_TrapImmCNZ;
	parameter [15:0] DP_Tltiu   = DP_TrapImmCNZ;
	parameter [15:0] DP_Tltu    = DP_TrapRegCNZ;
	parameter [15:0] DP_Tne     = DP_TrapRegCNZ;
	parameter [15:0] DP_Tnei    = DP_TrapImmCNZ;
	parameter [15:0] DP_Xor     = DP_RType;
	parameter [15:0] DP_Xori    = DP_IType;

	parameter [2:0] EXC_None = 3'b000;
	parameter [2:0] EXC_ID   = 3'b100;
	parameter [2:0] EXC_EX   = 3'b010;
	parameter [2:0] EXC_MEM  = 3'b001;
	//--------------------------------
	parameter [2:0] EXC_Add     = EXC_EX;
	parameter [2:0] EXC_Addi    = EXC_EX;
	parameter [2:0] EXC_Addiu   = EXC_None;
	parameter [2:0] EXC_Addu    = EXC_None;
	parameter [2:0] EXC_And     = EXC_None;
	parameter [2:0] EXC_Andi    = EXC_None;
	parameter [2:0] EXC_Beq     = EXC_None;
	parameter [2:0] EXC_Bgez    = EXC_None;
	parameter [2:0] EXC_Bgezal  = EXC_None;
	parameter [2:0] EXC_Bgtz    = EXC_None;
	parameter [2:0] EXC_Blez    = EXC_None;
	parameter [2:0] EXC_Bltz    = EXC_None;
	parameter [2:0] EXC_Bltzal  = EXC_None;
	parameter [2:0] EXC_Bne     = EXC_None;
	parameter [2:0] EXC_Break   = EXC_ID;
	parameter [2:0] EXC_Clo     = EXC_None;
	parameter [2:0] EXC_Clz     = EXC_None;
	parameter [2:0] EXC_Div     = EXC_None;
	parameter [2:0] EXC_Divu    = EXC_None;
	parameter [2:0] EXC_Eret    = EXC_ID;
	parameter [2:0] EXC_J       = EXC_None;
	parameter [2:0] EXC_Jal     = EXC_None;
	parameter [2:0] EXC_Jalr    = EXC_None;
	parameter [2:0] EXC_Jr      = EXC_None;
	parameter [2:0] EXC_Lb      = EXC_MEM;
	parameter [2:0] EXC_Lbu     = EXC_MEM;
	parameter [2:0] EXC_Lh      = EXC_MEM;
	parameter [2:0] EXC_Lhu     = EXC_MEM;
	parameter [2:0] EXC_Ll      = EXC_MEM;
	parameter [2:0] EXC_Lui     = EXC_None;
	parameter [2:0] EXC_Lw      = EXC_MEM;
	parameter [2:0] EXC_Lwl     = EXC_MEM;
	parameter [2:0] EXC_Lwr     = EXC_MEM;
	parameter [2:0] EXC_Madd    = EXC_None;
	parameter [2:0] EXC_Maddu   = EXC_None;
	parameter [2:0] EXC_Mfc0    = EXC_ID;
	parameter [2:0] EXC_Mfhi    = EXC_None;
	parameter [2:0] EXC_Mflo    = EXC_None;
	parameter [2:0] EXC_Movn    = EXC_None;
	parameter [2:0] EXC_Movz    = EXC_None;
	parameter [2:0] EXC_Msub    = EXC_None;
	parameter [2:0] EXC_Msubu   = EXC_None;
	parameter [2:0] EXC_Mtc0    = EXC_ID;
	parameter [2:0] EXC_Mthi    = EXC_None;
	parameter [2:0] EXC_Mtlo    = EXC_None;
	parameter [2:0] EXC_Mul     = EXC_None;
	parameter [2:0] EXC_Mult    = EXC_None;
	parameter [2:0] EXC_Multu   = EXC_None;
	parameter [2:0] EXC_Nor     = EXC_None;
	parameter [2:0] EXC_Or      = EXC_None;
	parameter [2:0] EXC_Ori     = EXC_None;
	parameter [2:0] EXC_Pref    = EXC_None; // XXX
	parameter [2:0] EXC_Sb      = EXC_MEM;
	parameter [2:0] EXC_Sc      = EXC_MEM;
	parameter [2:0] EXC_Sh      = EXC_MEM;
	parameter [2:0] EXC_Sll     = EXC_None;
	parameter [2:0] EXC_Sllv    = EXC_None;
	parameter [2:0] EXC_Slt     = EXC_None;
	parameter [2:0] EXC_Slti    = EXC_None;
	parameter [2:0] EXC_Sltiu   = EXC_None;
	parameter [2:0] EXC_Sltu    = EXC_None;
	parameter [2:0] EXC_Sra     = EXC_None;
	parameter [2:0] EXC_Srav    = EXC_None;
	parameter [2:0] EXC_Srl     = EXC_None;
	parameter [2:0] EXC_Srlv    = EXC_None;
	parameter [2:0] EXC_Sub     = EXC_EX;
	parameter [2:0] EXC_Subu    = EXC_None;
	parameter [2:0] EXC_Sw      = EXC_MEM;
	parameter [2:0] EXC_Swl     = EXC_MEM;
	parameter [2:0] EXC_Swr     = EXC_MEM;
	parameter [2:0] EXC_Syscall = EXC_ID;
	parameter [2:0] EXC_Teq     = EXC_MEM;
	parameter [2:0] EXC_Teqi    = EXC_MEM;
	parameter [2:0] EXC_Tge     = EXC_MEM;
	parameter [2:0] EXC_Tgei    = EXC_MEM;
	parameter [2:0] EXC_Tgeiu   = EXC_MEM;
	parameter [2:0] EXC_Tgeu    = EXC_MEM;
	parameter [2:0] EXC_Tlt     = EXC_MEM;
	parameter [2:0] EXC_Tlti    = EXC_MEM;
	parameter [2:0] EXC_Tltiu   = EXC_MEM;
	parameter [2:0] EXC_Tltu    = EXC_MEM;
	parameter [2:0] EXC_Tne     = EXC_MEM;
	parameter [2:0] EXC_Tnei    = EXC_MEM;
	parameter [2:0] EXC_Xor     = EXC_None;
	parameter [2:0] EXC_Xori    = EXC_None;

	parameter [7:0] HAZ_Nothing  = 8'b00000000; // Jumps, Lui, Mfhi/lo, special, etc.
	parameter [7:0] HAZ_IDRsIDRt = 8'b11110000; // Beq, Bne, Traps
	parameter [7:0] HAZ_IDRs     = 8'b11000000; // Most branches, Jumps to registers
	parameter [7:0] HAZ_IDRt     = 8'b00110000; // Mtc0
	parameter [7:0] HAZ_IDRtEXRs = 8'b10111100; // Movn, Movz
	parameter [7:0] HAZ_EXRsEXRt = 8'b10101111; // Many R-Type ops
	parameter [7:0] HAZ_EXRs     = 8'b10001100; // Immediates: Loads, Clo/z, Mthi/lo, etc.
	parameter [7:0] HAZ_EXRsWRt  = 8'b10101110; // Stores
	parameter [7:0] HAZ_EXRt     = 8'b00100011; // Shifts using Shamt field
	//-----------------------------------------
	parameter [7:0] HAZ_Add     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Addi    = HAZ_EXRs;
	parameter [7:0] HAZ_Addiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Addu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_And     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Andi    = HAZ_EXRs;
	parameter [7:0] HAZ_Beq     = HAZ_IDRsIDRt;
	parameter [7:0] HAZ_Bgez    = HAZ_IDRs;
	parameter [7:0] HAZ_Bgezal  = HAZ_IDRs;
	parameter [7:0] HAZ_Bgtz    = HAZ_IDRs;
	parameter [7:0] HAZ_Blez    = HAZ_IDRs;
	parameter [7:0] HAZ_Bltz    = HAZ_IDRs;
	parameter [7:0] HAZ_Bltzal  = HAZ_IDRs;
	parameter [7:0] HAZ_Bne     = HAZ_IDRsIDRt;
	parameter [7:0] HAZ_Break   = HAZ_Nothing;
	parameter [7:0] HAZ_Clo     = HAZ_EXRs;
	parameter [7:0] HAZ_Clz     = HAZ_EXRs;
	parameter [7:0] HAZ_Div     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Divu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Eret    = HAZ_Nothing;
	parameter [7:0] HAZ_J       = HAZ_Nothing;
	parameter [7:0] HAZ_Jal     = HAZ_Nothing;
	parameter [7:0] HAZ_Jalr    = HAZ_IDRs;
	parameter [7:0] HAZ_Jr      = HAZ_IDRs;
	parameter [7:0] HAZ_Lb      = HAZ_EXRs;
	parameter [7:0] HAZ_Lbu     = HAZ_EXRs;
	parameter [7:0] HAZ_Lh      = HAZ_EXRs;
	parameter [7:0] HAZ_Lhu     = HAZ_EXRs;
	parameter [7:0] HAZ_Ll      = HAZ_EXRs;
	parameter [7:0] HAZ_Lui     = HAZ_Nothing;
	parameter [7:0] HAZ_Lw      = HAZ_EXRs;
	parameter [7:0] HAZ_Lwl     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Lwr     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Madd    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Maddu   = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Mfc0    = HAZ_Nothing;
	parameter [7:0] HAZ_Mfhi    = HAZ_Nothing;
	parameter [7:0] HAZ_Mflo    = HAZ_Nothing;
	parameter [7:0] HAZ_Movn    = HAZ_IDRtEXRs;
	parameter [7:0] HAZ_Movz    = HAZ_IDRtEXRs;
	parameter [7:0] HAZ_Msub    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Msubu   = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Mtc0    = HAZ_IDRt;
	parameter [7:0] HAZ_Mthi    = HAZ_EXRs;
	parameter [7:0] HAZ_Mtlo    = HAZ_EXRs;
	parameter [7:0] HAZ_Mul     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Mult    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Multu   = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Nor     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Or      = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Ori     = HAZ_EXRs;
	parameter [7:0] HAZ_Pref    = HAZ_Nothing; // XXX
	parameter [7:0] HAZ_Sb      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Sc      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Sh      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Sll     = HAZ_EXRt;
	parameter [7:0] HAZ_Sllv    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Slt     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Slti    = HAZ_EXRs;
	parameter [7:0] HAZ_Sltiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Sltu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Sra     = HAZ_EXRt;
	parameter [7:0] HAZ_Srav    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Srl     = HAZ_EXRt;
	parameter [7:0] HAZ_Srlv    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Sub     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Subu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Sw      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Swl     = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Swr     = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Syscall = HAZ_Nothing;
	parameter [7:0] HAZ_Teq     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Teqi    = HAZ_EXRs;
	parameter [7:0] HAZ_Tge     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tgei    = HAZ_EXRs;
	parameter [7:0] HAZ_Tgeiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Tgeu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tlt     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tlti    = HAZ_EXRs;
	parameter [7:0] HAZ_Tltiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Tltu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tne     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tnei    = HAZ_EXRs;
	parameter [7:0] HAZ_Xor     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Xori    = HAZ_EXRs;


    /***
     Exception Control Flow Notes
     
     - Exceptions can occur in every pipeline stage. This implies that more than one exception
       can be raised in a single cycle. When this occurs, only the forward-most exception
       (i.e. MEM over EX) is handled. This and the following note guarantee program order.
       
     - An exception in any pipeline stage must stall that stage until all following stages are 
       exception-free. This is because it only makes sense for exceptions to occur in program order.
       
     - A pipeline stage which causes an exception must flush, i.e. prevent any commits it would
       have normally made and convert itself to a NOP for the next pipeline stage. Furthermore,
       it must flush all previous pipeline stages as well in order to retain program order.
       
     - Instructions reading CP0 (mtc0) read in ID without further action. Writes to CP0 (mtc0,
       eret) also write in ID, but only after forward pipeline stages have been cleared
       of possible exceptions. This prevents many insidious bugs, such as switching to User Mode
       in ID when a legitimate memory access in kernel mode is processing in MEM, or conversely
       a switch to Kernel Mode in ID when an instruction in User Mode is attempting a kernel region
       memory access (when a kernel mode signal does not propagate through the pipeline).
       
     - Commits occur in ID (CP0), EX (HILO), MEM, and WB (registers).
     
     - Hardware interrupts are detected and inserted in the ID stage, but only when there are no
       other possible exceptions in the pipeline. Because they appear 'asynchronous' to the
       processor, the remaining instructions in forward stages (EX, MEM, WB) can either be
       flushed or completed. It is simplest to have them complete to avoid restarts, but the
       interrupt latency is higher if e.g. the MEM stage stalls on a memory access (this would
       be unavoidable on single-cycle processors). This implementation allows all forward instructions
       to complete, for a greater instruction throughput but higher interrupt latency.
       
     - Software interrupts should appear synchronous in the program order, meaning that all
       instructions previous to them should complete and no instructions after them should start
       until the interrupts has been processed. 

     Exception Name             Short Name          Pipeline Stage
       Address Error Ex         (AdEL, AdES)        MEM, IF
       Integer Overflow Ex      (Ov)                EX
       Trap Ex                  (Tr)                MEM
       Syscall                  (Sys)               ID
       Breakpoint               (Bp)                ID
       Reserved Instruction     (RI)                ID
       Coprocessor Unusable     (CpU)               ID
       Interrupt                (Int)               ID
       Reset, SReset, NMI                           ID
    ***/
    
    
    // Exceptions Generated Internally
    wire EXC_CpU;

    // Hardware Interrupt #5, caused by Timer/Perf counter
    wire Int5;

    // Top-level Authoritative Interrupt Signal
    wire EXC_Int;

    // General Exception detection (all but Interrupts, Reset, Soft Reset, and NMI)
    wire EXC_General = EXC_AdIF | EXC_AdEL | EXC_AdES | EXC_Ov | EXC_Tr | EXC_Sys | EXC_Bp | EXC_RI | EXC_CpU;

    // Misc
    wire CP0_WriteCond;
    reg  [3:0] Cause_ExcCode_bits;

    reg reset_r;
    always @(posedge clock) begin
        reset_r <= reset;
    end

    /***
     MIPS-32 COPROCESSOR 0 (Cp0) REGISTERS
     
     These are defined in "MIPS32 Architecture for Programmers Volume III:
     The MIPS32 Privileged Resource Architecture" from MIPS Technologies, Inc.
     
     Optional registers are omitted. Changes to the processor (such as adding
     an MMU/TLB, etc. must be reflected in these registers.
    */
     
    // BadVAddr Register (Register 8, Select 0)
    reg [31:0] BadVAddr;

    // Count Register (Register 9, Select 0)
    reg [31:0] Count;

    // Compare Register (Register 11, Select 0)
    reg [31:0] Compare;

    // Status Register (Register 12, Select 0)
    wire [2:0] Status_CU_321 = 3'b000;
    reg  Status_CU_0;       // Access Control to CPs, [2]->Cp3, ... [0]->Cp0
    wire Status_RP = 0;
    wire Status_FR = 0;
    reg  Status_RE;         // Reverse Endian Memory for User Mode
    wire Status_MX = 0;
    wire Status_PX = 0;
    reg  Status_BEV;        // Exception vector locations (0->Norm, 1->Bootstrap)
    wire Status_TS = 0;
    wire Status_SR = 0;     // Soft reset not implemented
    reg  Status_NMI;        // Non-Maskable Interrupt
    wire Status_RES = 0;
    wire [1:0] Status_Custom = 2'b00;
    reg [7:0] Status_IM;    // Interrupt mask
    wire Status_KX = 0;
    wire Status_SX = 0;
    wire Status_UX = 0;
    reg  Status_UM;         // Base operating mode (0->Kernel, 1->User)
    wire Status_R0 = 0;
    reg  Status_ERL;        // Error Level     (0->Normal, 1->Error (reset, NMI))
    reg  Status_EXL;        // Exception level (0->Normal, 1->Exception)
    reg  Status_IE;         // Interrupt Enable
    wire [31:0] Status = {Status_CU_321, Status_CU_0, Status_RP, Status_FR, Status_RE, Status_MX, 
                                 Status_PX, Status_BEV, Status_TS, Status_SR, Status_NMI, Status_RES,
                                 Status_Custom, Status_IM, Status_KX, Status_SX, Status_UX,
                                 Status_UM, Status_R0, Status_ERL, Status_EXL, Status_IE};

    // Cause Register (Register 13, Select 0)
    reg  Cause_BD;                  // Exception occured in Branch Delay
    reg  [1:0] Cause_CE;            // CP number for CP Unusable exception
    reg  Cause_IV;                  // Indicator of general IV (0->0x180) or special IV (1->0x200)
    wire Cause_WP = 0;
    reg  [7:0] Cause_IP;            // Pending HW Interrupt indicator.
    wire Cause_ExcCode4 = 0;        // Can be made into a register when this bit is needed.
    reg  [3:0] Cause_ExcCode30;     // Description of Exception (only lower 4 bits currently used; see above)
    wire [31:0] Cause  = {Cause_BD, 1'b0, Cause_CE, 4'b0000, Cause_IV, Cause_WP,
                                 6'b000000, Cause_IP, 1'b0, Cause_ExcCode4, Cause_ExcCode30, 2'b00};
                        
    // Exception Program Counter (Register 14, Select 0)
    reg [31:0] EPC;

    // Processor Identification (Register 15, Select 0)
    wire [7:0] ID_Options = 8'b0000_0000;
    wire [7:0] ID_CID = 8'b0000_0000;
    wire [7:0] ID_PID = 8'b0000_0000;
    wire [7:0] ID_Rev = 8'b0000_0001;
    wire [31:0] PRId = {ID_Options, ID_CID, ID_PID, ID_Rev};

    // Configuration Register (Register 16, Select 0)
    wire Config_M = 1;
    wire [14:0] Config_Impl = 15'b000_0000_0000_0000;
    wire Config_BE = Big_Endian;    // From parameters file
    wire [1:0] Config_AT = 2'b00;
    wire [2:0] Config_AR = 3'b000;
    wire [2:0] Config_MT = 3'b000;
    wire [2:0] Config_K0 = 3'b000;
    wire [31:0] Config = {Config_M, Config_Impl, Config_BE, Config_AT, Config_AR, Config_MT, 
                                 4'b0000, Config_K0};

    // Configuration Register 1 (Register 16, Select 1)
    wire Config1_M = 0;
    wire [5:0] Config1_MMU = 6'b000000;
    wire [2:0] Config1_IS = 3'b000;
    wire [2:0] Config1_IL = 3'b000;
    wire [2:0] Config1_IA = 3'b000;
    wire [2:0] Config1_DS = 3'b000;
    wire [2:0] Config1_DL = 3'b000;
    wire [2:0] Config1_DA = 3'b000;
    wire Config1_C2 = 0;
    wire Config1_MD = 0;
    wire Config1_PC = 0;    // XXX Performance Counters
    wire Config1_WR = 0;    // XXX Watch Registers
    wire Config1_CA = 0;
    wire Config1_EP = 0;
    wire Config1_FP = 0;
    wire [31:0] Config1 = {Config1_M, Config1_MMU, Config1_IS, Config1_IL, Config1_IA,
                                  Config1_DS, Config1_DL, Config1_DA, Config1_C2,
                                  Config1_MD, Config1_PC, Config1_WR, Config1_CA,
                                  Config1_EP, Config1_FP};

    // Performance Counter Register (Register 25) XXX TODO

    // ErrorEPC Register (Register 30, Select 0)
    reg [31:0] ErrorEPC;

    // Exception Detection and Processing
    wire M_Exception_Detect, EX_Exception_Detect, ID_Exception_Detect, IF_Exception_Detect;
    wire M_Exception_Mask, EX_Exception_Mask, ID_Exception_Mask, IF_Exception_Mask;
    wire M_Exception_Ready, EX_Exception_Ready, ID_Exception_Ready, IF_Exception_Ready;

    assign IP = Cause_IP;

    /*** Coprocessor Unusable Exception ***/
    assign EXC_CpU = COP1 | COP2 | COP3 | ((Mtc0 | Mfc0 | ERET) & ~(Status_CU_0 | KernelMode));

    /*** Kernel Mode Signal ***/
    assign KernelMode = ~Status_UM | Status_EXL | Status_ERL;

    /*** Reverse Endian for User Mode ***/
    assign ReverseEndian = Status_RE;

    /*** Interrupts ***/
    assign Int5 = (Count == Compare);
    //assign EXC_Int = ((Cause_IP[7:0] & Status_IM[7:0]) != 8'h00) & Status_IE & ~Status_EXL & ~Status_ERL & ~ID_IsFlushed;
    wire   Enabled_Interrupt = EXC_NMI | (Status_IE & ((Cause_IP[7:0] & Status_IM[7:0]) != 8'h00));
    assign EXC_Int = Enabled_Interrupt & ~Status_EXL & ~Status_ERL & ~ID_IsFlushed;

    assign CP0_WriteCond = (Status_CU_0 | KernelMode) & Mtc0 & ~ID_Stall;


    /***
     Exception Hazard Flow Control Explanation:
        - An exception at any time in any stage causes its own and any previous stages to
          flush (clear own commits, NOPS to fwd stages).
        - An exception in a stage can also stall that stage (and inherently all previous stages) if and only if:
            1. A forward stage is capable of causing an exception AND
            2. A forward stage is not currently causing an exception.
        - An exception is ready to process when it is detected and not stalled in a stage.
        
        Flush specifics per pipeline stage:
            MEM: Mask 'MemWrite' and 'MemRead' (for performance) after EX/M and before data memory. NOPs to M/WB.
            EX : Mask writes to HI/LO. NOPs to EX/M.
            ID : Mask writes (reads?) to CP0. NOPs to ID/EX.
            IF : NOP to IF/ID.
    ***/

    /*** Exceptions grouped by pipeline stage ***/
    assign   M_Exception_Detect = EXC_AdEL | EXC_AdES | EXC_Tr;
    assign  EX_Exception_Detect = EXC_Ov;
    assign  ID_Exception_Detect = EXC_Sys | EXC_Bp | EXC_RI | EXC_CpU | EXC_Int;
    assign  IF_Exception_Detect = EXC_AdIF;

    /*** Exception mask conditions ***/
    
    // A potential bug would occur if e.g. EX stalls, MEM has data, but MEM is not stalled and finishes
    // going through the pipeline so forwarding would fail. This is not a problem however because 
    // EX would not need data since it would flush on an exception.
    assign   M_Exception_Mask = IF_Stall;
    assign  EX_Exception_Mask = IF_Stall | M_CanErr;
    assign  ID_Exception_Mask = IF_Stall | M_CanErr | EX_CanErr;
    assign  IF_Exception_Mask = M_CanErr | EX_CanErr | ID_CanErr | EXC_Int;

    /*** 
     Exceptions which must wait for forward stages. A stage will not stall if a forward stage has an exception.
     These stalls must be inserted as stall conditions in the hazard unit so that it will take care of chaining.
     All writes to CP0 must also wait for forward hazard conditions to clear.
    */
    assign  M_Exception_Stall  =  M_Exception_Detect  & M_Exception_Mask;
    assign  EX_Exception_Stall =  EX_Exception_Detect & EX_Exception_Mask & ~M_Exception_Detect;
    assign  ID_Exception_Stall = (ID_Exception_Detect | ERET | Mtc0) & ID_Exception_Mask & ~(EX_Exception_Detect | M_Exception_Detect);
    assign  IF_Exception_Stall =  IF_Exception_Detect & IF_Exception_Mask & ~(ID_Exception_Detect | EX_Exception_Detect | M_Exception_Detect);


    /*** Exceptions which are ready to process (mutually exclusive) ***/
    // XXX can remove ~ID_Stall since in mask now (?)
    assign   M_Exception_Ready =  ~ID_Stall & M_Exception_Detect  & ~M_Exception_Mask;
    assign  EX_Exception_Ready =  ~ID_Stall & EX_Exception_Detect & ~EX_Exception_Mask;
    assign  ID_Exception_Ready =  ~ID_Stall & ID_Exception_Detect & ~ID_Exception_Mask;
    assign  IF_Exception_Ready =  ~ID_Stall & IF_Exception_Detect & ~IF_Exception_Mask;

    /*** 
     Flushes. A flush clears a pipeline stage's control signals and prevents the stage from committing any changes.
     Data such as 'RestartPC' and the detected exception must remain.
    */
    assign   M_Exception_Flush = M_Exception_Detect;
    assign  EX_Exception_Flush = M_Exception_Detect | EX_Exception_Detect;
    assign  ID_Exception_Flush = M_Exception_Detect | EX_Exception_Detect | ID_Exception_Detect;
    assign  IF_Exception_Flush = M_Exception_Detect | EX_Exception_Detect | ID_Exception_Detect | IF_Exception_Detect | (ERET & ~ID_Stall) | reset_r;

      
    /*** Software reads of CP0 Registers ***/
    always @(*) begin
        if (Mfc0 & (Status_CU_0 | KernelMode)) begin
            case (Rd)
                5'd8  : Reg_Out = BadVAddr;
                5'd9  : Reg_Out = Count;
                5'd11 : Reg_Out = Compare;
                5'd12 : Reg_Out = Status;
                5'd13 : Reg_Out = Cause;
                5'd14 : Reg_Out = EPC;
                5'd15 : Reg_Out = PRId;
                5'd16 : Reg_Out = (Sel == 3'b000) ? Config : Config1;
                5'd30 : Reg_Out = ErrorEPC;
                default : Reg_Out = 32'h0000_0000;
            endcase
        end
        else begin
            Reg_Out = 32'h0000_0000;
        end
    end

    /*** Cp0 Register Assignments: Non-general exceptions (Reset, Soft Reset, NMI...) ***/
    always @(posedge clock) begin
        if (reset) begin
            Status_BEV <= 1;
            Status_NMI <= 0;
            Status_ERL <= 1;
            ErrorEPC   <= 32'b0;
        end
        else if (ID_Exception_Ready & EXC_NMI) begin
            Status_BEV <= 1;
            Status_NMI <= 1;
            Status_ERL <= 1;
            ErrorEPC   <= ID_RestartPC;
        end
        else begin
            Status_BEV    <= (CP0_WriteCond & (Rd == 5'd12) & (Sel == 3'b000)) ? Reg_In[22]   : Status_BEV;
            Status_NMI    <= (CP0_WriteCond & (Rd == 5'd12) & (Sel == 3'b000)) ? Reg_In[19]   : Status_NMI;
            Status_ERL    <= (CP0_WriteCond & (Rd == 5'd12) & (Sel == 3'b000)) ? Reg_In[2]    : ((Status_ERL & ERET & ~ID_Stall) ? 0 : Status_ERL);
            ErrorEPC      <= (CP0_WriteCond & (Rd == 5'd30) & (Sel == 3'b000)) ? Reg_In       : ErrorEPC;
        end
    end
    
    /*** Cp0 Register Assignments: All other registers ***/
    always @(posedge clock) begin
        if (reset) begin
            Count         <= 32'b0;
            Compare       <= 32'b0;
            Status_CU_0   <= 0;
            Status_RE     <= 0;
            Status_IM     <= 8'b0;
            Status_UM     <= 0;
            Status_IE     <= 0;
            Cause_IV      <= 0;
            Cause_IP      <= 8'b0;
        end
        else begin
            Count         <= (CP0_WriteCond & (Rd == 5'd9 ) & (Sel == 3'b000)) ? Reg_In       : ((Count == Compare) ? 32'b0 : Count + 1);
            Compare       <= (CP0_WriteCond & (Rd == 5'd11) & (Sel == 3'b000)) ? Reg_In       : Compare;
            Status_CU_0   <= (CP0_WriteCond & (Rd == 5'd12) & (Sel == 3'b000)) ? Reg_In[28]   : Status_CU_0;
            Status_RE     <= (CP0_WriteCond & (Rd == 5'd12) & (Sel == 3'b000)) ? Reg_In[25]   : Status_RE;
            Status_IM     <= (CP0_WriteCond & (Rd == 5'd12) & (Sel == 3'b000)) ? Reg_In[15:8] : Status_IM;
            Status_UM     <= (CP0_WriteCond & (Rd == 5'd12) & (Sel == 3'b000)) ? Reg_In[4]    : Status_UM;
            Status_IE     <= (CP0_WriteCond & (Rd == 5'd12) & (Sel == 3'b000)) ? Reg_In[0]    : Status_IE;
            Cause_IV      <= (CP0_WriteCond & (Rd == 5'd13) & (Sel == 3'b000)) ? Reg_In[23]   : Cause_IV;
            /* Cause_IP indicates 8 interrupts:
               [7]   is set by the timer comparison, and cleared by reading 'Count'.
               [6:2] are set and cleared by external hardware.
               [1:0] are set and cleared by software.
             */
            // If reading -> 0, Otherwise if 0 -> Int5.
            Cause_IP[7]   <= ((Status_CU_0 | KernelMode) & Mfc0 & (Rd == 5'd9) & (Sel == 3'b000)) ? 0 : ((Cause_IP[7] == 0) ? Int5 : Cause_IP[7]);
            Cause_IP[6:2] <= Int[4:0];
            Cause_IP[1:0] <= (CP0_WriteCond & (Rd == 5'd13) & (Sel == 3'b000)) ? Reg_In[9:8]  : Cause_IP[1:0];
        end
    end

    /*** Cp0 Register Assignments: General Exception and Interrupt Processing ***/
    always @(posedge clock) begin
        if (reset) begin
            Cause_BD <= 0;
            Cause_CE <= 2'b00;
            Cause_ExcCode30 <= 4'b0000;
            Status_EXL <= 0;
            EPC <= 32'h0;
            BadVAddr <= 32'h0;
        end
        else begin
            // MEM stage
            if (M_Exception_Ready) begin
                Cause_BD <= (Status_EXL) ? Cause_BD : M_IsBD;
                Cause_CE <= (COP3) ? 2'b11 : ((COP2) ? 2'b10 : ((COP1) ? 2'b01 : 2'b00));
                Cause_ExcCode30 <= Cause_ExcCode_bits;
                Status_EXL <= 1;
                EPC <= (Status_EXL) ? EPC : M_RestartPC;
                BadVAddr <= BadAddr_M;
            end
            // EX stage
            else if (EX_Exception_Ready) begin
                Cause_BD <= (Status_EXL) ? Cause_BD : EX_IsBD;
                Cause_CE <= (COP3) ? 2'b11 : ((COP2) ? 2'b10 : ((COP1) ? 2'b01 : 2'b00));
                Cause_ExcCode30 <= Cause_ExcCode_bits;
                Status_EXL <= 1;
                EPC <= (Status_EXL) ? EPC : EX_RestartPC;
                BadVAddr <= BadVAddr;
            end
            // ID stage
            else if (ID_Exception_Ready) begin
                Cause_BD <= (Status_EXL) ? Cause_BD : ID_IsBD;
                Cause_CE <= (COP3) ? 2'b11 : ((COP2) ? 2'b10 : ((COP1) ? 2'b01 : 2'b00));
                Cause_ExcCode30 <= Cause_ExcCode_bits;
                Status_EXL <= 1;
                EPC <= (Status_EXL) ? EPC : ID_RestartPC;
                BadVAddr <= BadVAddr;
            end
            // IF stage
            else if (IF_Exception_Ready) begin
                Cause_BD <= (Status_EXL) ? Cause_BD : IF_IsBD;
                Cause_CE <= (COP3) ? 2'b11 : ((COP2) ? 2'b10 : ((COP1) ? 2'b01 : 2'b00));
                Cause_ExcCode30 <= Cause_ExcCode_bits;
                Status_EXL <= 1;
                EPC <= (Status_EXL) ? EPC : BadAddr_IF;
                BadVAddr <= BadAddr_IF;
            end
            // No exceptions this cycle
            else begin
                Cause_BD <= 1'b0;
                Cause_CE <= Cause_CE;
                Cause_ExcCode30 <= Cause_ExcCode30;
                // Without new exceptions, 'Status_EXL' is set by software or cleared by ERET.
                Status_EXL <= (CP0_WriteCond & (Rd == 5'd12) & (Sel == 3'b000)) ? Reg_In[1] : ((Status_EXL & ERET & ~ID_Stall) ? 0 : Status_EXL);
                // The EPC is also writable by software
                EPC <= (CP0_WriteCond & (Rd == 5'd14) & (Sel == 3'b000)) ? Reg_In : EPC;
                BadVAddr <= BadVAddr;
            end
        end
    end


    /*** Program Counter for all Exceptions/Interrupts ***/
    always @(*) begin
        // Following is redundant since PC has initial value now.
        if (reset) begin
            Exc_PC_Out = EXC_Vector_Base_Reset;
        end
        else if (ERET & ~ID_Stall) begin
            Exc_PC_Out = (Status_ERL) ? ErrorEPC : EPC;
        end
        else if (EXC_General) begin
            Exc_PC_Out = (Status_BEV) ? (EXC_Vector_Base_Other_Boot   + EXC_Vector_Offset_General) :
                                         (EXC_Vector_Base_Other_NoBoot + EXC_Vector_Offset_General);
        end
        else if (EXC_NMI) begin
            Exc_PC_Out = EXC_Vector_Base_Reset;
        end
        else if (EXC_Int & Cause_IV) begin
            Exc_PC_Out = (Status_BEV) ? (EXC_Vector_Base_Other_Boot   + EXC_Vector_Offset_Special) :
                                         (EXC_Vector_Base_Other_NoBoot + EXC_Vector_Offset_Special);
        end        
        else begin
            Exc_PC_Out = (Status_BEV) ? (EXC_Vector_Base_Other_Boot   + EXC_Vector_Offset_General) :
                                         (EXC_Vector_Base_Other_NoBoot + EXC_Vector_Offset_General);
        end
    end

    //assign Exc_PC_Sel = (reset | (ERET & ~ID_Stall) | EXC_General | EXC_Int);
    assign Exc_PC_Sel = reset | (ERET & ~ID_Stall) | IF_Exception_Ready | ID_Exception_Ready | EX_Exception_Ready | M_Exception_Ready;

    /*** Cause Register ExcCode Field ***/
    always @(*) begin
        // Ordered by Pipeline Stage with Interrupts last
        if      (EXC_AdEL) Cause_ExcCode_bits = 4'h4;     // 00100
        else if (EXC_AdES) Cause_ExcCode_bits = 4'h5;     // 00101
        else if (EXC_Tr)   Cause_ExcCode_bits = 4'hd;     // 01101
        else if (EXC_Ov)   Cause_ExcCode_bits = 4'hc;     // 01100
        else if (EXC_Sys)  Cause_ExcCode_bits = 4'h8;     // 01000
        else if (EXC_Bp)   Cause_ExcCode_bits = 4'h9;     // 01001
        else if (EXC_RI)   Cause_ExcCode_bits = 4'ha;     // 01010
        else if (EXC_CpU)  Cause_ExcCode_bits = 4'hb;     // 01011
        else if (EXC_AdIF) Cause_ExcCode_bits = 4'h4;     // 00100
        else if (EXC_Int)  Cause_ExcCode_bits = 4'h0;     // 00000     // OK that NMI writes this.
        else               Cause_ExcCode_bits = 4'bxxxx;
    end
 
endmodule

