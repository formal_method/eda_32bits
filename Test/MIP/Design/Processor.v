`timescale 1ns / 1ps
/*
 * File         : Processor.v
 * Project      : University of Utah, XUM Project MIPS32 core
 * Creator(s)   : Grant Ayers (ayers@cs.utah.edu)
 *
 * Modification History:
 *   Rev   Date         Initials  Description of Change
 *   1.0   23-Jul-2011  GEA       Initial design.
 *   2.0   26-May-2012  GEA       Release version with CP0.
 *   2.01   1-Nov-2012  GEA       Fixed issue with Jal.
 *
 * Standards/Formatting:
 *   Verilog 2001, 4 soft tab, wide column.
 *
 * Description:
 *   The top-level MIPS32 Processor. This file is mostly the instantiation
 *   and wiring of the building blocks of the processor according to the 
 *   hardware design diagram. It contains very little logic itself.
 */
module Processor(
    input  clock,
    input  reset,
    input  [4:0] Interrupts,            // 5 general-purpose hardware interrupts
    input  NMI,                         // Non-maskable interrupt
    input  InstMem_Ready,
    // Data Memory Interface
    input  [31:0] DataMem_In,
    input  DataMem_Ready,
    input  [31:0] InstMem_In,
    output DataMem_Read, 
    output [3:0]  DataMem_Write,        // 4-bit Write, one for each byte in word.
    output [29:0] DataMem_Address,      // Addresses are words, not bytes.
    output [31:0] DataMem_Out,
    // Instruction Memory Interface
    output [29:0] InstMem_Address,      // Addresses are words, not bytes.
    output InstMem_Read,
    output [7:0] IP                     // Pending interrupts (diagnostic)
    );

    	parameter [31:0] EXC_Vector_Base_Reset          = 32'h0000_0010;    // MIPS Standard is 0xBFC0_0000
	parameter [31:0] EXC_Vector_Base_Other_NoBoot   = 32'h0000_0000;    // MIPS Standard is 0x8000_0000
	parameter [31:0] EXC_Vector_Base_Other_Boot     = 32'h0000_0000;    // MIPS Standard is 0xBFC0_0200
	parameter [31:0] EXC_Vector_Offset_General      = 32'h0000_0000;    // MIPS Standard is 0x0000_0180
	parameter [31:0] EXC_Vector_Offset_Special      = 32'h0000_0008;    // MIPS Standard is 0x0000_0200
	parameter [31:0] UMem_Lower = 32'h08000000;
	parameter Big_Endian = 1;
	parameter [5:0] Op_Type_R   = 6'b00_0000;  // Standard R-Type instructions
	parameter [5:0] Op_Type_R2  = 6'b01_1100;  // Extended R-Like instructions
	parameter [5:0] Op_Type_BI  = 6'b00_0001;  // Branch/Trap extended instructions
	parameter [5:0] Op_Type_CP0 = 6'b01_0000;  // Coprocessor 0 instructions
	parameter [5:0] Op_Type_CP1 = 6'b01_0001;  // Coprocessor 1 instructions (not implemented)
	parameter [5:0] Op_Type_CP2 = 6'b01_0010;  // Coprocessor 2 instructions (not implemented)
	parameter [5:0] Op_Type_CP3 = 6'b01_0011;  // Coprocessor 3 instructions (not implemented)
	parameter [5:0] Op_Add      = Op_Type_R;
	parameter [5:0] Op_Addi     = 6'b00_1000;
	parameter [5:0] Op_Addiu    = 6'b00_1001;
	parameter [5:0] Op_Addu     = Op_Type_R;
	parameter [5:0] Op_And      = Op_Type_R;
	parameter [5:0] Op_Andi     = 6'b00_1100;
	parameter [5:0] Op_Beq      = 6'b00_0100;
	parameter [5:0] Op_Bgez     = Op_Type_BI;
	parameter [5:0] Op_Bgezal   = Op_Type_BI;
	parameter [5:0] Op_Bgtz     = 6'b00_0111;
	parameter [5:0] Op_Blez     = 6'b00_0110;
	parameter [5:0] Op_Bltz     = Op_Type_BI;
	parameter [5:0] Op_Bltzal   = Op_Type_BI;
	parameter [5:0] Op_Bne      = 6'b00_0101;
	parameter [5:0] Op_Break    = Op_Type_R;
	parameter [5:0] Op_Clo      = Op_Type_R2;
	parameter [5:0] Op_Clz      = Op_Type_R2;
	parameter [5:0] Op_Div      = Op_Type_R;
	parameter [5:0] Op_Divu     = Op_Type_R;
	parameter [5:0] Op_Eret     = Op_Type_CP0;
	parameter [5:0] Op_J        = 6'b00_0010;
	parameter [5:0] Op_Jal      = 6'b00_0011;
	parameter [5:0] Op_Jalr     = Op_Type_R;
	parameter [5:0] Op_Jr       = Op_Type_R;
	parameter [5:0] Op_Lb       = 6'b10_0000;
	parameter [5:0] Op_Lbu      = 6'b10_0100;
	parameter [5:0] Op_Lh       = 6'b10_0001;
	parameter [5:0] Op_Lhu      = 6'b10_0101;
	parameter [5:0] Op_Ll       = 6'b11_0000;
	parameter [5:0] Op_Lui      = 6'b00_1111;
	parameter [5:0] Op_Lw       = 6'b10_0011;
	parameter [5:0] Op_Lwl      = 6'b10_0010;
	parameter [5:0] Op_Lwr      = 6'b10_0110;
	parameter [5:0] Op_Madd     = Op_Type_R2;
	parameter [5:0] Op_Maddu    = Op_Type_R2;
	parameter [5:0] Op_Mfc0     = Op_Type_CP0;
	parameter [5:0] Op_Mfhi     = Op_Type_R;
	parameter [5:0] Op_Mflo     = Op_Type_R;
	parameter [5:0] Op_Movn     = Op_Type_R;
	parameter [5:0] Op_Movz     = Op_Type_R;
	parameter [5:0] Op_Msub     = Op_Type_R2;
	parameter [5:0] Op_Msubu    = Op_Type_R2;
	parameter [5:0] Op_Mtc0     = Op_Type_CP0;
	parameter [5:0] Op_Mthi     = Op_Type_R;
	parameter [5:0] Op_Mtlo     = Op_Type_R;
	parameter [5:0] Op_Mul      = Op_Type_R2;
	parameter [5:0] Op_Mult     = Op_Type_R;
	parameter [5:0] Op_Multu    = Op_Type_R;
	parameter [5:0] Op_Nor      = Op_Type_R;
	parameter [5:0] Op_Or       = Op_Type_R;
	parameter [5:0] Op_Ori      = 6'b00_1101;
	parameter [5:0] Op_Pref     = 6'b11_0011; // Prefetch does nothing in this implementation.
	parameter [5:0] Op_Sb       = 6'b10_1000;
	parameter [5:0] Op_Sc       = 6'b11_1000;
	parameter [5:0] Op_Sh       = 6'b10_1001;
	parameter [5:0] Op_Sll      = Op_Type_R;
	parameter [5:0] Op_Sllv     = Op_Type_R;
	parameter [5:0] Op_Slt      = Op_Type_R;
	parameter [5:0] Op_Slti     = 6'b00_1010;
	parameter [5:0] Op_Sltiu    = 6'b00_1011;
	parameter [5:0] Op_Sltu     = Op_Type_R;
	parameter [5:0] Op_Sra      = Op_Type_R;
	parameter [5:0] Op_Srav     = Op_Type_R;
	parameter [5:0] Op_Srl      = Op_Type_R;
	parameter [5:0] Op_Srlv     = Op_Type_R;
	parameter [5:0] Op_Sub      = Op_Type_R;
	parameter [5:0] Op_Subu     = Op_Type_R;
	parameter [5:0] Op_Sw       = 6'b10_1011;
	parameter [5:0] Op_Swl      = 6'b10_1010;
	parameter [5:0] Op_Swr      = 6'b10_1110;
	parameter [5:0] Op_Syscall  = Op_Type_R;
	parameter [5:0] Op_Teq      = Op_Type_R;
	parameter [5:0] Op_Teqi     = Op_Type_BI;
	parameter [5:0] Op_Tge      = Op_Type_R;
	parameter [5:0] Op_Tgei     = Op_Type_BI;
	parameter [5:0] Op_Tgeiu    = Op_Type_BI;
	parameter [5:0] Op_Tgeu     = Op_Type_R;
	parameter [5:0] Op_Tlt      = Op_Type_R;
	parameter [5:0] Op_Tlti     = Op_Type_BI;
	parameter [5:0] Op_Tltiu    = Op_Type_BI;
	parameter [5:0] Op_Tltu     = Op_Type_R;
	parameter [5:0] Op_Tne      = Op_Type_R;
	parameter [5:0] Op_Tnei     = Op_Type_BI;
	parameter [5:0] Op_Xor      = Op_Type_R;
	parameter [5:0] Op_Xori     = 6'b00_1110;

	/* Op Code Rt fields for Branches & Traps */
	parameter [4:0] OpRt_Bgez   = 5'b00001;
	parameter [4:0] OpRt_Bgezal = 5'b10001;
	parameter [4:0] OpRt_Bltz   = 5'b00000;
	parameter [4:0] OpRt_Bltzal = 5'b10000;
	parameter [4:0] OpRt_Teqi   = 5'b01100;
	parameter [4:0] OpRt_Tgei   = 5'b01000;
	parameter [4:0] OpRt_Tgeiu  = 5'b01001;
	parameter [4:0] OpRt_Tlti   = 5'b01010;
	parameter [4:0] OpRt_Tltiu  = 5'b01011;
	parameter [4:0] OpRt_Tnei   = 5'b01110;

	/* Op Code Rs fields for Coprocessors */
	parameter [4:0] OpRs_MF     = 5'b00000;
	parameter [4:0] OpRs_MT     = 5'b00100;

	/* Special handling for ERET */
	parameter [4:0] OpRs_ERET   = 5'b10000;
	parameter [5:0] Funct_ERET  = 6'b011000;

	/* Function Codes for R-Type Op Codes */
	parameter [5:0] Funct_Add     = 6'b10_0000;
	parameter [5:0] Funct_Addu    = 6'b10_0001;
	parameter [5:0] Funct_And     = 6'b10_0100;
	parameter [5:0] Funct_Break   = 6'b00_1101;
	parameter [5:0] Funct_Clo     = 6'b10_0001; // same as Addu
	parameter [5:0] Funct_Clz     = 6'b10_0000; // same as Add
	parameter [5:0] Funct_Div     = 6'b01_1010;
	parameter [5:0] Funct_Divu    = 6'b01_1011;
	parameter [5:0] Funct_Jr      = 6'b00_1000;
	parameter [5:0] Funct_Jalr    = 6'b00_1001;
	parameter [5:0] Funct_Madd    = 6'b00_0000;
	parameter [5:0] Funct_Maddu   = 6'b00_0001;
	parameter [5:0] Funct_Mfhi    = 6'b01_0000;
	parameter [5:0] Funct_Mflo    = 6'b01_0010;
	parameter [5:0] Funct_Movn    = 6'b00_1011;
	parameter [5:0] Funct_Movz    = 6'b00_1010;
	parameter [5:0] Funct_Msub    = 6'b00_0100; // same as Sllv
	parameter [5:0] Funct_Msubu   = 6'b00_0101;
	parameter [5:0] Funct_Mthi    = 6'b01_0001;
	parameter [5:0] Funct_Mtlo    = 6'b01_0011;
	parameter [5:0] Funct_Mul     = 6'b00_0010; // same as Srl
	parameter [5:0] Funct_Mult    = 6'b01_1000;
	parameter [5:0] Funct_Multu   = 6'b01_1001;
	parameter [5:0] Funct_Nor     = 6'b10_0111;
	parameter [5:0] Funct_Or      = 6'b10_0101;
	parameter [5:0] Funct_Sll     = 6'b00_0000;
	parameter [5:0] Funct_Sllv    = 6'b00_0100;
	parameter [5:0] Funct_Slt     = 6'b10_1010;
	parameter [5:0] Funct_Sltu    = 6'b10_1011;
	parameter [5:0] Funct_Sra     = 6'b00_0011;
	parameter [5:0] Funct_Srav    = 6'b00_0111;
	parameter [5:0] Funct_Srl     = 6'b00_0010;
	parameter [5:0] Funct_Srlv    = 6'b00_0110;
	parameter [5:0] Funct_Sub     = 6'b10_0010;
	parameter [5:0] Funct_Subu    = 6'b10_0011;
	parameter [5:0] Funct_Syscall = 6'b00_1100;
	parameter [5:0] Funct_Teq     = 6'b11_0100;
	parameter [5:0] Funct_Tge     = 6'b11_0000;
	parameter [5:0] Funct_Tgeu    = 6'b11_0001;
	parameter [5:0] Funct_Tlt     = 6'b11_0010;
	parameter [5:0] Funct_Tltu    = 6'b11_0011;
	parameter [5:0] Funct_Tne     = 6'b11_0110;
	parameter [5:0] Funct_Xor     = 6'b10_0110;

	/* ALU Operations (Implementation) */
	parameter [4:0] AluOp_Add    = 5'd1;
	parameter [4:0] AluOp_Addu   = 5'd0;
	parameter [4:0] AluOp_And    = 5'd2;
	parameter [4:0] AluOp_Clo    = 5'd3;
	parameter [4:0] AluOp_Clz    = 5'd4;
	parameter [4:0] AluOp_Div    = 5'd5;
	parameter [4:0] AluOp_Divu   = 5'd6;
	parameter [4:0] AluOp_Madd   = 5'd7;
	parameter [4:0] AluOp_Maddu  = 5'd8;
	parameter [4:0] AluOp_Mfhi   = 5'd9;
	parameter [4:0] AluOp_Mflo   = 5'd10;
	parameter [4:0] AluOp_Msub   = 5'd13;
	parameter [4:0] AluOp_Msubu  = 5'd14;
	parameter [4:0] AluOp_Mthi   = 5'd11;
	parameter [4:0] AluOp_Mtlo   = 5'd12;
	parameter [4:0] AluOp_Mul    = 5'd15;
	parameter [4:0] AluOp_Mult   = 5'd16;
	parameter [4:0] AluOp_Multu  = 5'd17;
	parameter [4:0] AluOp_Nor    = 5'd18;
	parameter [4:0] AluOp_Or     = 5'd19;
	parameter [4:0] AluOp_Sll    = 5'd20;
	parameter [4:0] AluOp_Sllc   = 5'd21;  // Move this if another AluOp is needed
	parameter [4:0] AluOp_Sllv   = 5'd22;
	parameter [4:0] AluOp_Slt    = 5'd23;
	parameter [4:0] AluOp_Sltu   = 5'd24;
	parameter [4:0] AluOp_Sra    = 5'd25;
	parameter [4:0] AluOp_Srav   = 5'd26;
	parameter [4:0] AluOp_Srl    = 5'd27;
	parameter [4:0] AluOp_Srlv   = 5'd28;
	parameter [4:0] AluOp_Sub    = 5'd29;
	parameter [4:0] AluOp_Subu   = 5'd30;
	parameter [4:0] AluOp_Xor    = 5'd31;
	parameter [15:0] DP_None        = 16'b000_00000_000000_00;    // Instructions which require nothing of the main datapath.
	parameter [15:0] DP_RType       = 16'b000_00001_000000_10;    // Standard R-Type
	parameter [15:0] DP_IType       = 16'b000_10000_000000_10;    // Standard I-Type
	parameter [15:0] DP_Branch      = 16'b100_00000_000000_00;    // Standard Branch
	parameter [15:0] DP_BranchLink  = 16'b101_00000_000000_10;    // Branch and Link
	parameter [15:0] DP_HiLoWr      = 16'b000_00000_000000_00;    // Write to Hi/Lo ALU register (Div,Divu,Mult,Multu,Mthi,Mtlo). Currently 'DP_None'.
	parameter [15:0] DP_Jump        = 16'b010_00000_000000_00;    // Standard Jump
	parameter [15:0] DP_JumpLink    = 16'b011_00000_000000_10;    // Jump and Link
	parameter [15:0] DP_JumpLinkReg = 16'b111_00000_000000_10;    // Jump and Link Register
	parameter [15:0] DP_JumpReg     = 16'b110_00000_000000_00;    // Jump Register
	parameter [15:0] DP_LoadByteS   = 16'b000_10000_010011_11;    // Load Byte Signed
	parameter [15:0] DP_LoadByteU   = 16'b000_10000_010010_11;    // Load Byte Unsigned
	parameter [15:0] DP_LoadHalfS   = 16'b000_10000_010101_11;    // Load Half Signed
	parameter [15:0] DP_LoadHalfU   = 16'b000_10000_010100_11;    // Load Half Unsigned
	parameter [15:0] DP_LoadWord    = 16'b000_10000_010000_11;    // Load Word
	parameter [15:0] DP_ExtWrRt     = 16'b000_00000_000000_10;    // A DP-external write to Rt
	parameter [15:0] DP_ExtWrRd     = 16'b000_00001_000000_10;    // A DP-external write to Rd
	parameter [15:0] DP_Movc        = 16'b000_01001_000000_10;    // Conditional Move
	parameter [15:0] DP_LoadLinked  = 16'b000_10000_110000_11;    // Load Linked
	parameter [15:0] DP_StoreCond   = 16'b000_10000_101000_11;    // Store Conditional
	parameter [15:0] DP_StoreByte   = 16'b000_10000_001010_00;    // Store Byte
	parameter [15:0] DP_StoreHalf   = 16'b000_10000_001100_00;    // Store Half
	parameter [15:0] DP_StoreWord   = 16'b000_10000_001000_00;    // Store Word
	parameter [15:0] DP_TrapRegCNZ  = 16'b000_00110_000000_00;    // Trap using Rs and Rt,  non-zero ALU (Tlt,  Tltu,  Tne)
	parameter [15:0] DP_TrapRegCZ   = 16'b000_00100_000000_00;    // Trap using RS and Rt,  zero ALU     (Teq,  Tge,   Tgeu)
	parameter [15:0] DP_TrapImmCNZ  = 16'b000_10110_000000_00;    // Trap using Rs and Imm, non-zero ALU (Tlti, Tltiu, Tnei)
	parameter [15:0] DP_TrapImmCZ   = 16'b000_10100_000000_00;    // Trap using Rs and Imm, zero ALU     (Teqi, Tgei,  Tgeiu)
	//--------------------------------------------------------
	parameter [15:0] DP_Add     = DP_RType;
	parameter [15:0] DP_Addi    = DP_IType;
	parameter [15:0] DP_Addiu   = DP_IType;
	parameter [15:0] DP_Addu    = DP_RType;
	parameter [15:0] DP_And     = DP_RType;
	parameter [15:0] DP_Andi    = DP_IType;
	parameter [15:0] DP_Beq     = DP_Branch;
	parameter [15:0] DP_Bgez    = DP_Branch;
	parameter [15:0] DP_Bgezal  = DP_BranchLink;
	parameter [15:0] DP_Bgtz    = DP_Branch;
	parameter [15:0] DP_Blez    = DP_Branch;
	parameter [15:0] DP_Bltz    = DP_Branch;
	parameter [15:0] DP_Bltzal  = DP_BranchLink;
	parameter [15:0] DP_Bne     = DP_Branch;
	parameter [15:0] DP_Break   = DP_None;
	parameter [15:0] DP_Clo     = DP_RType;
	parameter [15:0] DP_Clz     = DP_RType;
	parameter [15:0] DP_Div     = DP_HiLoWr;
	parameter [15:0] DP_Divu    = DP_HiLoWr;
	parameter [15:0] DP_Eret    = DP_None;
	parameter [15:0] DP_J       = DP_Jump;
	parameter [15:0] DP_Jal     = DP_JumpLink;
	parameter [15:0] DP_Jalr    = DP_JumpLinkReg;
	parameter [15:0] DP_Jr      = DP_JumpReg;
	parameter [15:0] DP_Lb      = DP_LoadByteS;
	parameter [15:0] DP_Lbu     = DP_LoadByteU;
	parameter [15:0] DP_Lh      = DP_LoadHalfS;
	parameter [15:0] DP_Lhu     = DP_LoadHalfU;
	parameter [15:0] DP_Ll      = DP_LoadLinked;
	parameter [15:0] DP_Lui     = DP_IType;
	parameter [15:0] DP_Lw      = DP_LoadWord;
	parameter [15:0] DP_Lwl     = DP_LoadWord;
	parameter [15:0] DP_Lwr     = DP_LoadWord;
	parameter [15:0] DP_Madd    = DP_HiLoWr;
	parameter [15:0] DP_Maddu   = DP_HiLoWr;
	parameter [15:0] DP_Mfc0    = DP_ExtWrRt;
	parameter [15:0] DP_Mfhi    = DP_ExtWrRd;
	parameter [15:0] DP_Mflo    = DP_ExtWrRd;
	parameter [15:0] DP_Movn    = DP_Movc;
	parameter [15:0] DP_Movz    = DP_Movc;
	parameter [15:0] DP_Msub    = DP_HiLoWr;
	parameter [15:0] DP_Msubu   = DP_HiLoWr;
	parameter [15:0] DP_Mtc0    = DP_None;
	parameter [15:0] DP_Mthi    = DP_HiLoWr;
	parameter [15:0] DP_Mtlo    = DP_HiLoWr;
	parameter [15:0] DP_Mul     = DP_RType;
	parameter [15:0] DP_Mult    = DP_HiLoWr;
	parameter [15:0] DP_Multu   = DP_HiLoWr;
	parameter [15:0] DP_Nor     = DP_RType;
	parameter [15:0] DP_Or      = DP_RType;
	parameter [15:0] DP_Ori     = DP_IType;
	parameter [15:0] DP_Pref    = DP_None; // Not Implemented
	parameter [15:0] DP_Sb      = DP_StoreByte;
	parameter [15:0] DP_Sc      = DP_StoreCond;
	parameter [15:0] DP_Sh      = DP_StoreHalf;
	parameter [15:0] DP_Sll     = DP_RType;
	parameter [15:0] DP_Sllv    = DP_RType;
	parameter [15:0] DP_Slt     = DP_RType;
	parameter [15:0] DP_Slti    = DP_IType;
	parameter [15:0] DP_Sltiu   = DP_IType;
	parameter [15:0] DP_Sltu    = DP_RType;
	parameter [15:0] DP_Sra     = DP_RType;
	parameter [15:0] DP_Srav    = DP_RType;
	parameter [15:0] DP_Srl     = DP_RType;
	parameter [15:0] DP_Srlv    = DP_RType;
	parameter [15:0] DP_Sub     = DP_RType;
	parameter [15:0] DP_Subu    = DP_RType;
	parameter [15:0] DP_Sw      = DP_StoreWord;
	parameter [15:0] DP_Swl     = DP_StoreWord;
	parameter [15:0] DP_Swr     = DP_StoreWord;
	parameter [15:0] DP_Syscall = DP_None;
	parameter [15:0] DP_Teq     = DP_TrapRegCZ;
	parameter [15:0] DP_Teqi    = DP_TrapImmCZ;
	parameter [15:0] DP_Tge     = DP_TrapRegCZ;
	parameter [15:0] DP_Tgei    = DP_TrapImmCZ;
	parameter [15:0] DP_Tgeiu   = DP_TrapImmCZ;
	parameter [15:0] DP_Tgeu    = DP_TrapRegCZ;
	parameter [15:0] DP_Tlt     = DP_TrapRegCNZ;
	parameter [15:0] DP_Tlti    = DP_TrapImmCNZ;
	parameter [15:0] DP_Tltiu   = DP_TrapImmCNZ;
	parameter [15:0] DP_Tltu    = DP_TrapRegCNZ;
	parameter [15:0] DP_Tne     = DP_TrapRegCNZ;
	parameter [15:0] DP_Tnei    = DP_TrapImmCNZ;
	parameter [15:0] DP_Xor     = DP_RType;
	parameter [15:0] DP_Xori    = DP_IType;

	parameter [2:0] EXC_None = 3'b000;
	parameter [2:0] EXC_ID   = 3'b100;
	parameter [2:0] EXC_EX   = 3'b010;
	parameter [2:0] EXC_MEM  = 3'b001;
	//--------------------------------
	parameter [2:0] EXC_Add     = EXC_EX;
	parameter [2:0] EXC_Addi    = EXC_EX;
	parameter [2:0] EXC_Addiu   = EXC_None;
	parameter [2:0] EXC_Addu    = EXC_None;
	parameter [2:0] EXC_And     = EXC_None;
	parameter [2:0] EXC_Andi    = EXC_None;
	parameter [2:0] EXC_Beq     = EXC_None;
	parameter [2:0] EXC_Bgez    = EXC_None;
	parameter [2:0] EXC_Bgezal  = EXC_None;
	parameter [2:0] EXC_Bgtz    = EXC_None;
	parameter [2:0] EXC_Blez    = EXC_None;
	parameter [2:0] EXC_Bltz    = EXC_None;
	parameter [2:0] EXC_Bltzal  = EXC_None;
	parameter [2:0] EXC_Bne     = EXC_None;
	parameter [2:0] EXC_Break   = EXC_ID;
	parameter [2:0] EXC_Clo     = EXC_None;
	parameter [2:0] EXC_Clz     = EXC_None;
	parameter [2:0] EXC_Div     = EXC_None;
	parameter [2:0] EXC_Divu    = EXC_None;
	parameter [2:0] EXC_Eret    = EXC_ID;
	parameter [2:0] EXC_J       = EXC_None;
	parameter [2:0] EXC_Jal     = EXC_None;
	parameter [2:0] EXC_Jalr    = EXC_None;
	parameter [2:0] EXC_Jr      = EXC_None;
	parameter [2:0] EXC_Lb      = EXC_MEM;
	parameter [2:0] EXC_Lbu     = EXC_MEM;
	parameter [2:0] EXC_Lh      = EXC_MEM;
	parameter [2:0] EXC_Lhu     = EXC_MEM;
	parameter [2:0] EXC_Ll      = EXC_MEM;
	parameter [2:0] EXC_Lui     = EXC_None;
	parameter [2:0] EXC_Lw      = EXC_MEM;
	parameter [2:0] EXC_Lwl     = EXC_MEM;
	parameter [2:0] EXC_Lwr     = EXC_MEM;
	parameter [2:0] EXC_Madd    = EXC_None;
	parameter [2:0] EXC_Maddu   = EXC_None;
	parameter [2:0] EXC_Mfc0    = EXC_ID;
	parameter [2:0] EXC_Mfhi    = EXC_None;
	parameter [2:0] EXC_Mflo    = EXC_None;
	parameter [2:0] EXC_Movn    = EXC_None;
	parameter [2:0] EXC_Movz    = EXC_None;
	parameter [2:0] EXC_Msub    = EXC_None;
	parameter [2:0] EXC_Msubu   = EXC_None;
	parameter [2:0] EXC_Mtc0    = EXC_ID;
	parameter [2:0] EXC_Mthi    = EXC_None;
	parameter [2:0] EXC_Mtlo    = EXC_None;
	parameter [2:0] EXC_Mul     = EXC_None;
	parameter [2:0] EXC_Mult    = EXC_None;
	parameter [2:0] EXC_Multu   = EXC_None;
	parameter [2:0] EXC_Nor     = EXC_None;
	parameter [2:0] EXC_Or      = EXC_None;
	parameter [2:0] EXC_Ori     = EXC_None;
	parameter [2:0] EXC_Pref    = EXC_None; // XXX
	parameter [2:0] EXC_Sb      = EXC_MEM;
	parameter [2:0] EXC_Sc      = EXC_MEM;
	parameter [2:0] EXC_Sh      = EXC_MEM;
	parameter [2:0] EXC_Sll     = EXC_None;
	parameter [2:0] EXC_Sllv    = EXC_None;
	parameter [2:0] EXC_Slt     = EXC_None;
	parameter [2:0] EXC_Slti    = EXC_None;
	parameter [2:0] EXC_Sltiu   = EXC_None;
	parameter [2:0] EXC_Sltu    = EXC_None;
	parameter [2:0] EXC_Sra     = EXC_None;
	parameter [2:0] EXC_Srav    = EXC_None;
	parameter [2:0] EXC_Srl     = EXC_None;
	parameter [2:0] EXC_Srlv    = EXC_None;
	parameter [2:0] EXC_Sub     = EXC_EX;
	parameter [2:0] EXC_Subu    = EXC_None;
	parameter [2:0] EXC_Sw      = EXC_MEM;
	parameter [2:0] EXC_Swl     = EXC_MEM;
	parameter [2:0] EXC_Swr     = EXC_MEM;
	parameter [2:0] EXC_Syscall = EXC_ID;
	parameter [2:0] EXC_Teq     = EXC_MEM;
	parameter [2:0] EXC_Teqi    = EXC_MEM;
	parameter [2:0] EXC_Tge     = EXC_MEM;
	parameter [2:0] EXC_Tgei    = EXC_MEM;
	parameter [2:0] EXC_Tgeiu   = EXC_MEM;
	parameter [2:0] EXC_Tgeu    = EXC_MEM;
	parameter [2:0] EXC_Tlt     = EXC_MEM;
	parameter [2:0] EXC_Tlti    = EXC_MEM;
	parameter [2:0] EXC_Tltiu   = EXC_MEM;
	parameter [2:0] EXC_Tltu    = EXC_MEM;
	parameter [2:0] EXC_Tne     = EXC_MEM;
	parameter [2:0] EXC_Tnei    = EXC_MEM;
	parameter [2:0] EXC_Xor     = EXC_None;
	parameter [2:0] EXC_Xori    = EXC_None;

	parameter [7:0] HAZ_Nothing  = 8'b00000000; // Jumps, Lui, Mfhi/lo, special, etc.
	parameter [7:0] HAZ_IDRsIDRt = 8'b11110000; // Beq, Bne, Traps
	parameter [7:0] HAZ_IDRs     = 8'b11000000; // Most branches, Jumps to registers
	parameter [7:0] HAZ_IDRt     = 8'b00110000; // Mtc0
	parameter [7:0] HAZ_IDRtEXRs = 8'b10111100; // Movn, Movz
	parameter [7:0] HAZ_EXRsEXRt = 8'b10101111; // Many R-Type ops
	parameter [7:0] HAZ_EXRs     = 8'b10001100; // Immediates: Loads, Clo/z, Mthi/lo, etc.
	parameter [7:0] HAZ_EXRsWRt  = 8'b10101110; // Stores
	parameter [7:0] HAZ_EXRt     = 8'b00100011; // Shifts using Shamt field
	//-----------------------------------------
	parameter [7:0] HAZ_Add     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Addi    = HAZ_EXRs;
	parameter [7:0] HAZ_Addiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Addu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_And     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Andi    = HAZ_EXRs;
	parameter [7:0] HAZ_Beq     = HAZ_IDRsIDRt;
	parameter [7:0] HAZ_Bgez    = HAZ_IDRs;
	parameter [7:0] HAZ_Bgezal  = HAZ_IDRs;
	parameter [7:0] HAZ_Bgtz    = HAZ_IDRs;
	parameter [7:0] HAZ_Blez    = HAZ_IDRs;
	parameter [7:0] HAZ_Bltz    = HAZ_IDRs;
	parameter [7:0] HAZ_Bltzal  = HAZ_IDRs;
	parameter [7:0] HAZ_Bne     = HAZ_IDRsIDRt;
	parameter [7:0] HAZ_Break   = HAZ_Nothing;
	parameter [7:0] HAZ_Clo     = HAZ_EXRs;
	parameter [7:0] HAZ_Clz     = HAZ_EXRs;
	parameter [7:0] HAZ_Div     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Divu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Eret    = HAZ_Nothing;
	parameter [7:0] HAZ_J       = HAZ_Nothing;
	parameter [7:0] HAZ_Jal     = HAZ_Nothing;
	parameter [7:0] HAZ_Jalr    = HAZ_IDRs;
	parameter [7:0] HAZ_Jr      = HAZ_IDRs;
	parameter [7:0] HAZ_Lb      = HAZ_EXRs;
	parameter [7:0] HAZ_Lbu     = HAZ_EXRs;
	parameter [7:0] HAZ_Lh      = HAZ_EXRs;
	parameter [7:0] HAZ_Lhu     = HAZ_EXRs;
	parameter [7:0] HAZ_Ll      = HAZ_EXRs;
	parameter [7:0] HAZ_Lui     = HAZ_Nothing;
	parameter [7:0] HAZ_Lw      = HAZ_EXRs;
	parameter [7:0] HAZ_Lwl     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Lwr     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Madd    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Maddu   = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Mfc0    = HAZ_Nothing;
	parameter [7:0] HAZ_Mfhi    = HAZ_Nothing;
	parameter [7:0] HAZ_Mflo    = HAZ_Nothing;
	parameter [7:0] HAZ_Movn    = HAZ_IDRtEXRs;
	parameter [7:0] HAZ_Movz    = HAZ_IDRtEXRs;
	parameter [7:0] HAZ_Msub    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Msubu   = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Mtc0    = HAZ_IDRt;
	parameter [7:0] HAZ_Mthi    = HAZ_EXRs;
	parameter [7:0] HAZ_Mtlo    = HAZ_EXRs;
	parameter [7:0] HAZ_Mul     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Mult    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Multu   = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Nor     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Or      = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Ori     = HAZ_EXRs;
	parameter [7:0] HAZ_Pref    = HAZ_Nothing; // XXX
	parameter [7:0] HAZ_Sb      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Sc      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Sh      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Sll     = HAZ_EXRt;
	parameter [7:0] HAZ_Sllv    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Slt     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Slti    = HAZ_EXRs;
	parameter [7:0] HAZ_Sltiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Sltu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Sra     = HAZ_EXRt;
	parameter [7:0] HAZ_Srav    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Srl     = HAZ_EXRt;
	parameter [7:0] HAZ_Srlv    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Sub     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Subu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Sw      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Swl     = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Swr     = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Syscall = HAZ_Nothing;
	parameter [7:0] HAZ_Teq     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Teqi    = HAZ_EXRs;
	parameter [7:0] HAZ_Tge     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tgei    = HAZ_EXRs;
	parameter [7:0] HAZ_Tgeiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Tgeu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tlt     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tlti    = HAZ_EXRs;
	parameter [7:0] HAZ_Tltiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Tltu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tne     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tnei    = HAZ_EXRs;
	parameter [7:0] HAZ_Xor     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Xori    = HAZ_EXRs;


    /*** MIPS Instruction and Components (ID Stage) ***/
    wire [31:0] Instruction;
    wire [5:0]  OpCode = Instruction[31:26];
    wire [4:0]  Rs = Instruction[25:21];
    wire [4:0]  Rt = Instruction[20:16];
    wire [4:0]  Rd = Instruction[15:11];
    wire [5:0]  Funct = Instruction[5:0];
    wire [15:0] Immediate = Instruction[15:0];
    wire [25:0] JumpAddress = Instruction[25:0];
    wire [2:0]  Cp0_Sel = Instruction[2:0];

    /*** IF (Instruction Fetch) Signals ***/
    wire IF_Stall, IF_Flush;
    wire IF_EXC_AdIF;
    wire IF_Exception_Stall;
    wire IF_Exception_Flush;
    wire IF_IsBDS;
    wire [31:0] IF_PCAdd4, IF_PC_PreExc, IF_PCIn, IF_PCOut, IF_Instruction;

    /*** ID (Instruction Decode) Signals ***/
    wire ID_Stall;
    wire [1:0] ID_PCSrc;
    wire [1:0] ID_RsFwdSel, ID_RtFwdSel;
    wire ID_Link, ID_Movn, ID_Movz;
    wire ID_SignExtend;
    wire ID_LLSC;
    wire ID_RegDst, ID_ALUSrcImm, ID_MemWrite, ID_MemRead, ID_MemByte, ID_MemHalf, ID_MemSignExtend, ID_RegWrite, ID_MemtoReg;
    wire [4:0] ID_ALUOp;
    wire ID_Mfc0, ID_Mtc0, ID_Eret;
    wire ID_NextIsDelay;
    wire ID_CanErr, ID_ID_CanErr, ID_EX_CanErr, ID_M_CanErr;
    wire ID_KernelMode;
    wire ID_ReverseEndian;
    wire ID_Trap, ID_TrapCond;
    wire ID_EXC_Sys, ID_EXC_Bp, ID_EXC_RI;
    wire ID_Exception_Stall;
    wire ID_Exception_Flush;
    wire ID_PCSrc_Exc;
    wire [31:0] ID_ExceptionPC;
    wire ID_CP1, ID_CP2, ID_CP3;
    wire [31:0] ID_PCAdd4;
    wire [31:0] ID_ReadData1_RF, ID_ReadData1_End;
    wire [31:0] ID_ReadData2_RF, ID_ReadData2_End;
    wire [31:0] CP0_RegOut;
    wire ID_CmpEQ, ID_CmpGZ, ID_CmpLZ, ID_CmpGEZ, ID_CmpLEZ;
    wire [29:0] ID_SignExtImm = (ID_SignExtend & Immediate[15]) ? {14'h3FFF, Immediate} : {14'h0000, Immediate};
    wire [31:0] ID_ImmLeftShift2 = {ID_SignExtImm[29:0], 2'b00};
    wire [31:0] ID_JumpAddress = {ID_PCAdd4[31:28], JumpAddress[25:0], 2'b00};
    wire [31:0] ID_BranchAddress;
    wire [31:0] ID_RestartPC;
    wire ID_IsBDS;
    wire ID_Left, ID_Right;
    wire ID_IsFlushed;

    /*** EX (Execute) Signals ***/
    wire EX_ALU_Stall, EX_Stall;
    wire [1:0] EX_RsFwdSel, EX_RtFwdSel;
    wire EX_Link;
    wire [1:0] EX_LinkRegDst;
    wire EX_ALUSrcImm;
    wire [4:0] EX_ALUOp;
    wire EX_Movn, EX_Movz;
    wire EX_LLSC;
    wire EX_MemRead, EX_MemWrite, EX_MemByte, EX_MemHalf, EX_MemSignExtend, EX_RegWrite, EX_MemtoReg;
    wire [4:0] EX_Rs, EX_Rt;
    wire EX_WantRsByEX, EX_NeedRsByEX, EX_WantRtByEX, EX_NeedRtByEX;
    wire EX_Trap, EX_TrapCond;
    wire EX_CanErr, EX_EX_CanErr, EX_M_CanErr;
    wire EX_KernelMode;
    wire EX_ReverseEndian;
    wire EX_Exception_Stall;
    wire EX_Exception_Flush;
    wire [31:0] EX_ReadData1_PR, EX_ReadData1_Fwd, EX_ReadData2_PR, EX_ReadData2_Fwd, EX_ReadData2_Imm;
    wire [31:0] EX_SignExtImm;
    wire [4:0] EX_Rd, EX_RtRd, EX_Shamt;
    wire [31:0] EX_ALUResult;
    wire EX_BZero;
    wire EX_EXC_Ov;
    wire [31:0] EX_RestartPC;
    wire EX_IsBDS;
    wire EX_Left, EX_Right;

    /*** MEM (Memory) Signals ***/
    wire M_Stall, M_Stall_Controller;
    wire M_LLSC;
    wire M_MemRead, M_MemWrite, M_MemByte, M_MemHalf, M_MemSignExtend;
    wire M_RegWrite, M_MemtoReg;
    wire M_WriteDataFwdSel;
    wire M_EXC_AdEL, M_EXC_AdES;
    wire M_M_CanErr;
    wire M_KernelMode;
    wire M_ReverseEndian;
    wire M_Trap, M_TrapCond;
    wire M_EXC_Tr;
    wire M_Exception_Flush;
    wire [31:0] M_ALUResult, M_ReadData2_PR;
    wire [4:0] M_RtRd;
    wire [31:0] M_MemReadData;
    wire [31:0] M_RestartPC;
    wire M_IsBDS;
    wire [31:0] M_WriteData_Pre;
    wire M_Left, M_Right;
    wire M_Exception_Stall;

    /*** WB (Writeback) Signals ***/
    wire WB_Stall, WB_RegWrite;
    wire [31:0] WB_ReadData, WB_ALUResult;
    wire [4:0]  WB_RtRd;
    wire [31:0] WB_WriteData;

    /*** Other Signals ***/
    wire [7:0] ID_DP_Hazards, HAZ_DP_Hazards;

    /*** Assignments ***/
    assign IF_Instruction = (IF_Stall) ? 32'h00000000 : InstMem_In;
    assign IF_IsBDS = ID_NextIsDelay;
    assign HAZ_DP_Hazards = {ID_DP_Hazards[7:4], EX_WantRsByEX, EX_NeedRsByEX, EX_WantRtByEX, EX_NeedRtByEX};
    assign IF_EXC_AdIF = IF_PCOut[1] | IF_PCOut[0];
    assign ID_CanErr = ID_ID_CanErr | ID_EX_CanErr | ID_M_CanErr;
    assign EX_CanErr = EX_EX_CanErr | EX_M_CanErr;
    assign M_CanErr  = M_M_CanErr;

    // External Memory Interface
    reg IRead, IReadMask;
    assign InstMem_Address = IF_PCOut[31:2];
    assign DataMem_Address = M_ALUResult[31:2];
    always @(posedge clock) begin
        IRead <= (reset) ? 1 : ~InstMem_Ready;
        IReadMask <= (reset) ? 0 : ((IRead & InstMem_Ready) ? 1 : ((~IF_Stall) ? 0 : IReadMask));
    end
    //assign InstMem_Read = IRead & ~IReadMask;
    // edit by hoa
  //  assign InstMem_Read = IRead & IReadMask;	
    assign InstMem_Read= 0;
    // end edit


    /*** Datapath Controller ***/
    Control Controller (
        .ID_Stall       (ID_Stall),
        .OpCode         (OpCode),
        .Funct          (Funct),
        .Rs             (Rs),
        .Rt             (Rt),
        .Cmp_EQ         (ID_CmpEQ),
        .Cmp_GZ         (ID_CmpGZ),
        .Cmp_GEZ        (ID_CmpGEZ),
        .Cmp_LZ         (ID_CmpLZ),
        .Cmp_LEZ        (ID_CmpLEZ),
        .IF_Flush       (IF_Flush),
        .DP_Hazards     (ID_DP_Hazards),
        .PCSrc          (ID_PCSrc),
        .SignExtend     (ID_SignExtend),
        .Link           (ID_Link),
        .Movn           (ID_Movn),
        .Movz           (ID_Movz),
        .Mfc0           (ID_Mfc0),
        .Mtc0           (ID_Mtc0),
        .CP1            (ID_CP1),
        .CP2            (ID_CP2),
        .CP3            (ID_CP3),
        .Eret           (ID_Eret),
        .Trap           (ID_Trap),
        .TrapCond       (ID_TrapCond),
        .EXC_Sys        (ID_EXC_Sys),
        .EXC_Bp         (ID_EXC_Bp),
        .EXC_RI         (ID_EXC_RI),
        .ID_CanErr      (ID_ID_CanErr),
        .EX_CanErr      (ID_EX_CanErr),
        .M_CanErr       (ID_M_CanErr),
        .NextIsDelay    (ID_NextIsDelay),
        .RegDst         (ID_RegDst),
        .ALUSrcImm      (ID_ALUSrcImm),
        .ALUOp          (ID_ALUOp),
        .LLSC           (ID_LLSC),
        .MemWrite       (ID_MemWrite),
        .MemRead        (ID_MemRead),
        .MemByte        (ID_MemByte),
        .MemHalf        (ID_MemHalf),
        .MemSignExtend  (ID_MemSignExtend),
        .Left           (ID_Left),
        .Right          (ID_Right),
        .RegWrite       (ID_RegWrite),
        .MemtoReg       (ID_MemtoReg)
    );

    /*** Hazard and Forward Control Unit ***/
    Hazard_Detection HazardControl (
        .DP_Hazards          (HAZ_DP_Hazards),
        .ID_Rs               (Rs),
        .ID_Rt               (Rt),
        .EX_Rs               (EX_Rs),
        .EX_Rt               (EX_Rt),
        .EX_RtRd             (EX_RtRd),
        .MEM_RtRd            (M_RtRd),
        .WB_RtRd             (WB_RtRd),
        .EX_Link             (EX_Link),
        .EX_RegWrite         (EX_RegWrite),
        .MEM_RegWrite        (M_RegWrite),
        .WB_RegWrite         (WB_RegWrite),
        .MEM_MemRead         (M_MemRead),
        .MEM_MemWrite        (M_MemWrite),
        .InstMem_Read        (InstMem_Read),
        .InstMem_Ready       (InstMem_Ready),
        .Mfc0                (ID_Mfc0),
        .IF_Exception_Stall  (IF_Exception_Stall),
        .ID_Exception_Stall  (ID_Exception_Stall),
        .EX_Exception_Stall  (EX_Exception_Stall),
        .EX_ALU_Stall        (EX_ALU_Stall),
        .M_Stall_Controller  (M_Stall_Controller),
        .IF_Stall            (IF_Stall),
        .ID_Stall            (ID_Stall),
        .EX_Stall            (EX_Stall),
        .M_Stall             (M_Stall),
        .WB_Stall            (WB_Stall),
        .ID_RsFwdSel         (ID_RsFwdSel),
        .ID_RtFwdSel         (ID_RtFwdSel),
        .EX_RsFwdSel         (EX_RsFwdSel),
        .EX_RtFwdSel         (EX_RtFwdSel),
        .M_WriteDataFwdSel   (M_WriteDataFwdSel)
    );

    /*** Coprocessor 0: Exceptions and Interrupts ***/
    CPZero CP0 (
        .clock               (clock),
        .Mfc0                (ID_Mfc0),
        .Mtc0                (ID_Mtc0),
        .IF_Stall            (IF_Stall),
        .ID_Stall            (ID_Stall),
        .COP1                (ID_CP1),
        .COP2                (ID_CP2),
        .COP3                (ID_CP3),
        .ERET                (ID_Eret),
        .Rd                  (Rd),
        .Sel                 (Cp0_Sel),
        .Reg_In              (ID_ReadData2_End),
        .Reg_Out             (CP0_RegOut),
        .KernelMode          (ID_KernelMode),
        .ReverseEndian       (ID_ReverseEndian),
        .Int                 (Interrupts),
        .reset               (reset),
        .EXC_NMI             (NMI),
        .EXC_AdIF            (IF_EXC_AdIF),
        .EXC_AdEL            (M_EXC_AdEL),
        .EXC_AdES            (M_EXC_AdES),
        .EXC_Ov              (EX_EXC_Ov),
        .EXC_Tr              (M_EXC_Tr),
        .EXC_Sys             (ID_EXC_Sys),
        .EXC_Bp              (ID_EXC_Bp),
        .EXC_RI              (ID_EXC_RI),
        .ID_RestartPC        (ID_RestartPC),
        .EX_RestartPC        (EX_RestartPC),
        .M_RestartPC         (M_RestartPC),
        .ID_IsFlushed        (ID_IsFlushed),
        .IF_IsBD             (IF_IsBDS),
        .ID_IsBD             (ID_IsBDS),
        .EX_IsBD             (EX_IsBDS),
        .M_IsBD              (M_IsBDS),
        .BadAddr_M           (M_ALUResult),
        .BadAddr_IF          (IF_PCOut),
        .ID_CanErr           (ID_CanErr),
        .EX_CanErr           (EX_CanErr),
        .M_CanErr            (M_CanErr),
        .IF_Exception_Stall  (IF_Exception_Stall),
        .ID_Exception_Stall  (ID_Exception_Stall),
        .EX_Exception_Stall  (EX_Exception_Stall),
        .M_Exception_Stall   (M_Exception_Stall),
        .IF_Exception_Flush  (IF_Exception_Flush),
        .ID_Exception_Flush  (ID_Exception_Flush),
        .EX_Exception_Flush  (EX_Exception_Flush),
        .M_Exception_Flush   (M_Exception_Flush),
        .Exc_PC_Sel          (ID_PCSrc_Exc),
        .Exc_PC_Out          (ID_ExceptionPC),
        .IP                  (IP)
    );

    /*** PC Source Non-Exception Mux ***/
    Mux4 #(.WIDTH(32)) PCSrcStd_Mux (
        .sel  (ID_PCSrc),
        .in0  (IF_PCAdd4),
        .in1  (ID_JumpAddress),
        .in2  (ID_BranchAddress),
        .in3  (ID_ReadData1_End),
        .out  (IF_PC_PreExc)
    );

    /*** PC Source Exception Mux ***/
    Mux2 #(.WIDTH(32)) PCSrcExc_Mux (
        .sel  (ID_PCSrc_Exc),
        .in0  (IF_PC_PreExc),
        .in1  (ID_ExceptionPC),
        .out  (IF_PCIn)
    );

    /*** Program Counter (MIPS spec is 0xBFC00000 starting address) ***/
    Register #(.WIDTH(32), .INIT(EXC_Vector_Base_Reset)) PC (
        .clock   (clock),
        .reset   (reset),
        //.enable  (~IF_Stall),   // XXX verify. HERE. Was 1 but on stall latches PC+4, ad nauseum.
        .enable (~(IF_Stall | ID_Stall)),
        .D       (IF_PCIn),
        .Q       (IF_PCOut)
    );

    /*** PC +4 Adder ***/
    Add PC_Add4 (
        .A  (IF_PCOut),
        .B  (32'h00000004),
        .C  (IF_PCAdd4)
    );

    /*** Instruction Fetch -> Instruction Decode Stage Register ***/
    IFID_Stage IFID (
        .clock           (clock),
        .reset           (reset),
        .IF_Flush        (IF_Exception_Flush | IF_Flush),
        .IF_Stall        (IF_Stall),
        .ID_Stall        (ID_Stall),
        .IF_Instruction  (IF_Instruction),
        .IF_PCAdd4       (IF_PCAdd4),
        .IF_PC           (IF_PCOut),
        .IF_IsBDS        (IF_IsBDS),
        .ID_Instruction  (Instruction),
        .ID_PCAdd4       (ID_PCAdd4),
        .ID_RestartPC    (ID_RestartPC),
        .ID_IsBDS        (ID_IsBDS),
        .ID_IsFlushed    (ID_IsFlushed)
    );

    /*** Register File ***/
    RegisterFile RegisterFile (
        .clock      (clock),
        .reset      (reset),
        .ReadReg1   (Rs),
        .ReadReg2   (Rt),
        .WriteReg   (WB_RtRd),
        .WriteData  (WB_WriteData),
        .RegWrite   (WB_RegWrite),
        .ReadData1  (ID_ReadData1_RF),
        .ReadData2  (ID_ReadData2_RF)
    );

    /*** ID Rs Forwarding/Link Mux ***/
    Mux4 #(.WIDTH(32)) IDRsFwd_Mux (
        .sel  (ID_RsFwdSel),
        .in0  (ID_ReadData1_RF),
        .in1  (M_ALUResult),
        .in2  (WB_WriteData),
        .in3  (32'hxxxxxxxx),
        .out  (ID_ReadData1_End)
    );

    /*** ID Rt Forwarding/CP0 Mfc0 Mux ***/
    Mux4 #(.WIDTH(32)) IDRtFwd_Mux (
        .sel  (ID_RtFwdSel),
        .in0  (ID_ReadData2_RF),
        .in1  (M_ALUResult),
        .in2  (WB_WriteData),
        .in3  (CP0_RegOut),
        .out  (ID_ReadData2_End)
    );

    /*** Condition Compare Unit ***/
    Compare Compare (
        .A    (ID_ReadData1_End),
        .B    (ID_ReadData2_End),
        .EQ   (ID_CmpEQ),
        .GZ   (ID_CmpGZ),
        .LZ   (ID_CmpLZ),
        .GEZ  (ID_CmpGEZ),
        .LEZ  (ID_CmpLEZ)
    );

    /*** Branch Address Adder ***/
    Add BranchAddress_Add (
        .A  (ID_PCAdd4),
        .B  (ID_ImmLeftShift2),
        .C  (ID_BranchAddress)
    );

    /*** Instruction Decode -> Execute Pipeline Stage ***/
    IDEX_Stage IDEX (
        .clock             (clock),
        .reset             (reset),
        .ID_Flush          (ID_Exception_Flush),
        .ID_Stall          (ID_Stall),
        .EX_Stall          (EX_Stall),
        .ID_Link           (ID_Link),
        .ID_RegDst         (ID_RegDst),
        .ID_ALUSrcImm      (ID_ALUSrcImm),
        .ID_ALUOp          (ID_ALUOp),
        .ID_Movn           (ID_Movn),
        .ID_Movz           (ID_Movz),
        .ID_LLSC           (ID_LLSC),
        .ID_MemRead        (ID_MemRead),
        .ID_MemWrite       (ID_MemWrite),
        .ID_MemByte        (ID_MemByte),
        .ID_MemHalf        (ID_MemHalf),
        .ID_MemSignExtend  (ID_MemSignExtend),
        .ID_Left           (ID_Left),
        .ID_Right          (ID_Right),
        .ID_RegWrite       (ID_RegWrite),
        .ID_MemtoReg       (ID_MemtoReg),
        .ID_ReverseEndian  (ID_ReverseEndian),
        .ID_Rs             (Rs),
        .ID_Rt             (Rt),
        .ID_WantRsByEX     (ID_DP_Hazards[3]),
        .ID_NeedRsByEX     (ID_DP_Hazards[2]),
        .ID_WantRtByEX     (ID_DP_Hazards[1]),
        .ID_NeedRtByEX     (ID_DP_Hazards[0]),
        .ID_KernelMode     (ID_KernelMode),
        .ID_RestartPC      (ID_RestartPC),
        .ID_IsBDS          (ID_IsBDS),
        .ID_Trap           (ID_Trap),
        .ID_TrapCond       (ID_TrapCond),
        .ID_EX_CanErr      (ID_EX_CanErr),
        .ID_M_CanErr       (ID_M_CanErr),
        .ID_ReadData1      (ID_ReadData1_End),
        .ID_ReadData2      (ID_ReadData2_End),
        .ID_SignExtImm     (ID_SignExtImm[16:0]),
        .EX_Link           (EX_Link),
        .EX_LinkRegDst     (EX_LinkRegDst),
        .EX_ALUSrcImm      (EX_ALUSrcImm),
        .EX_ALUOp          (EX_ALUOp),
        .EX_Movn           (EX_Movn),
        .EX_Movz           (EX_Movz),
        .EX_LLSC           (EX_LLSC),
        .EX_MemRead        (EX_MemRead),
        .EX_MemWrite       (EX_MemWrite),
        .EX_MemByte        (EX_MemByte),
        .EX_MemHalf        (EX_MemHalf),
        .EX_MemSignExtend  (EX_MemSignExtend),
        .EX_Left           (EX_Left),
        .EX_Right          (EX_Right),
        .EX_RegWrite       (EX_RegWrite),
        .EX_MemtoReg       (EX_MemtoReg),
        .EX_ReverseEndian  (EX_ReverseEndian),
        .EX_Rs             (EX_Rs),
        .EX_Rt             (EX_Rt),
        .EX_WantRsByEX     (EX_WantRsByEX),
        .EX_NeedRsByEX     (EX_NeedRsByEX),
        .EX_WantRtByEX     (EX_WantRtByEX),
        .EX_NeedRtByEX     (EX_NeedRtByEX),
        .EX_KernelMode     (EX_KernelMode),
        .EX_RestartPC      (EX_RestartPC),
        .EX_IsBDS          (EX_IsBDS),
        .EX_Trap           (EX_Trap),
        .EX_TrapCond       (EX_TrapCond),
        .EX_EX_CanErr      (EX_EX_CanErr),
        .EX_M_CanErr       (EX_M_CanErr),
        .EX_ReadData1      (EX_ReadData1_PR),
        .EX_ReadData2      (EX_ReadData2_PR),
        .EX_SignExtImm     (EX_SignExtImm),
        .EX_Rd             (EX_Rd),
        .EX_Shamt          (EX_Shamt)
    );

    /*** EX Rs Forwarding Mux ***/
    Mux4 #(.WIDTH(32)) EXRsFwd_Mux (
        .sel  (EX_RsFwdSel),
        .in0  (EX_ReadData1_PR),
        .in1  (M_ALUResult),
        .in2  (WB_WriteData),
        .in3  (EX_RestartPC),
        .out  (EX_ReadData1_Fwd)
    );

    /*** EX Rt Forwarding / Link Mux ***/
    Mux4 #(.WIDTH(32)) EXRtFwdLnk_Mux (
        .sel  (EX_RtFwdSel),
        .in0  (EX_ReadData2_PR),
        .in1  (M_ALUResult),
        .in2  (WB_WriteData),
        .in3  (32'h00000008),
        .out  (EX_ReadData2_Fwd)
    );

    /*** EX ALU Immediate Mux ***/
    Mux2 #(.WIDTH(32)) EXALUImm_Mux (
        .sel  (EX_ALUSrcImm),
        .in0  (EX_ReadData2_Fwd),
        .in1  (EX_SignExtImm),
        .out  (EX_ReadData2_Imm)
    );

    /*** EX RtRd / Link Mux ***/
    Mux4 #(.WIDTH(5)) EXRtRdLnk_Mux (
        .sel  (EX_LinkRegDst),
        .in0  (EX_Rt),
        .in1  (EX_Rd),
        .in2  (5'b11111),
        .in3  (5'bxxxxx),
        .out  (EX_RtRd)
    );

    /*** Arithmetic Logic Unit ***/
    ALU ALU (
        .clock      (clock),
        .reset      (reset),
        .EX_Stall   (EX_Stall),
        .EX_Flush   (EX_Exception_Flush),
        .A          (EX_ReadData1_Fwd),
        .B          (EX_ReadData2_Imm),
        .Operation  (EX_ALUOp),
        .Shamt      (EX_Shamt),
        .Result     (EX_ALUResult),
        .BZero      (EX_BZero),
        .EXC_Ov     (EX_EXC_Ov),
        .ALU_Stall  (EX_ALU_Stall)
    );

    /*** Execute -> Memory Pipeline Stage ***/
    EXMEM_Stage EXMEM (
        .clock             (clock),
        .reset             (reset),
        .EX_Flush          (EX_Exception_Flush),
        .EX_Stall          (EX_Stall),
        .M_Stall           (M_Stall),
        .EX_Movn           (EX_Movn),
        .EX_Movz           (EX_Movz),
        .EX_BZero          (EX_BZero),
        .EX_RegWrite       (EX_RegWrite),
        .EX_MemtoReg       (EX_MemtoReg),
        .EX_ReverseEndian  (EX_ReverseEndian),
        .EX_LLSC           (EX_LLSC),
        .EX_MemRead        (EX_MemRead),
        .EX_MemWrite       (EX_MemWrite),
        .EX_MemByte        (EX_MemByte),
        .EX_MemHalf        (EX_MemHalf),
        .EX_MemSignExtend  (EX_MemSignExtend),
        .EX_Left           (EX_Left),
        .EX_Right          (EX_Right),
        .EX_KernelMode     (EX_KernelMode),
        .EX_RestartPC      (EX_RestartPC),
        .EX_IsBDS          (EX_IsBDS),
        .EX_Trap           (EX_Trap),
        .EX_TrapCond       (EX_TrapCond),
        .EX_M_CanErr       (EX_M_CanErr),
        .EX_ALU_Result     (EX_ALUResult),
        .EX_ReadData2      (EX_ReadData2_Fwd),
        .EX_RtRd           (EX_RtRd),
        .M_RegWrite        (M_RegWrite),
        .M_MemtoReg        (M_MemtoReg),
        .M_ReverseEndian   (M_ReverseEndian),
        .M_LLSC            (M_LLSC),
        .M_MemRead         (M_MemRead),
        .M_MemWrite        (M_MemWrite),
        .M_MemByte         (M_MemByte),
        .M_MemHalf         (M_MemHalf),
        .M_MemSignExtend   (M_MemSignExtend),
        .M_Left            (M_Left),
        .M_Right           (M_Right),
        .M_KernelMode      (M_KernelMode),
        .M_RestartPC       (M_RestartPC),
        .M_IsBDS           (M_IsBDS),
        .M_Trap            (M_Trap),
        .M_TrapCond        (M_TrapCond),
        .M_M_CanErr        (M_M_CanErr),
        .M_ALU_Result      (M_ALUResult),
        .M_ReadData2       (M_ReadData2_PR),
        .M_RtRd            (M_RtRd)
    );

    /*** Trap Detection Unit ***/
    TrapDetect TrapDetect (
        .Trap       (M_Trap),
        .TrapCond   (M_TrapCond),
        .ALUResult  (M_ALUResult),
        .EXC_Tr     (M_EXC_Tr)
    );

    /*** MEM Write Data Mux ***/
    Mux2 #(.WIDTH(32)) MWriteData_Mux (
        .sel  (M_WriteDataFwdSel),
        .in0  (M_ReadData2_PR),
        .in1  (WB_WriteData),
        .out  (M_WriteData_Pre)
    );

    /*** Data Memory Controller ***/
    MemControl DataMem_Controller (
        .clock         (clock),
        .reset         (reset),
        .DataIn        (M_WriteData_Pre),
        .Address       (M_ALUResult),
        .MReadData     (DataMem_In),
        .MemRead       (M_MemRead),
        .MemWrite      (M_MemWrite),
        .DataMem_Ready (DataMem_Ready),
        .Byte          (M_MemByte),
        .Half          (M_MemHalf),
        .SignExtend    (M_MemSignExtend),
        .KernelMode    (M_KernelMode),
        .ReverseEndian (M_ReverseEndian),
        .LLSC          (M_LLSC),
        .ERET          (ID_Eret),
        .Left          (M_Left),
        .Right         (M_Right),
        .M_Exception_Stall (M_Exception_Stall),
        
        .IF_Stall (IF_Stall),
        
        .DataOut       (M_MemReadData),
        .MWriteData    (DataMem_Out),
        .WriteEnable   (DataMem_Write),
        .ReadEnable    (DataMem_Read),
        .M_Stall       (M_Stall_Controller),
        .EXC_AdEL      (M_EXC_AdEL),
        .EXC_AdES      (M_EXC_AdES)
    );

    /*** Memory -> Writeback Pipeline Stage ***/
    MEMWB_Stage MEMWB (
        .clock          (clock),
        .reset          (reset),
        .M_Flush        (M_Exception_Flush),
        .M_Stall        (M_Stall),
        .WB_Stall       (WB_Stall),
        .M_RegWrite     (M_RegWrite),
        .M_MemtoReg     (M_MemtoReg),
        .M_ReadData     (M_MemReadData),
        .M_ALU_Result   (M_ALUResult),
        .M_RtRd         (M_RtRd),
        .WB_RegWrite    (WB_RegWrite),
        .WB_MemtoReg    (WB_MemtoReg),
        .WB_ReadData    (WB_ReadData),
        .WB_ALU_Result  (WB_ALUResult),
        .WB_RtRd        (WB_RtRd)
    );

    /*** WB MemtoReg Mux ***/
    Mux2 #(.WIDTH(32)) WBMemtoReg_Mux (
        .sel  (WB_MemtoReg),
        .in0  (WB_ALUResult),
        .in1  (WB_ReadData),
        .out  (WB_WriteData)
    );

endmodule

