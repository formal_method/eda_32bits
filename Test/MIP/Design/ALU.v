
/*
 * File         : ALU.v
 * Project      : University of Utah, XUM Project MIPS32 core
 * Creator(s)   : Grant Ayers (ayers@cs.utah.edu)
 *
 * Modification History:
 *   Rev   Date         Initials  Description of Change
 *   1.0   7-Jun-2011   GEA       Initial design.
 *   2.0   26-Jul-2012  GEA       Many changes have been made.
 *
 * Standards/Formatting:
 *   Verilog 2001, 4 soft tab, wide column.
 *
 * Description:
 *   An Arithmetic Logic Unit for a MIPS32 processor. This module computes all
 *   arithmetic operations, including the following:
 *
 *   Add, Subtract, Multiply, And, Or, Nor, Xor, Shift, Count leading 1s/0s.
 */
module ALU(
    input  clock,
    input  reset,
    input  EX_Stall,
    input  EX_Flush,
    input  [31:0] A, B,
    input  [4:0]  Operation,
    input  signed [4:0] Shamt,
    output reg signed [31:0] Result,
    output BZero,           // Used for Movc
    output reg EXC_Ov,
    output ALU_Stall        // Stalls due to long ALU operations
    );

      
	parameter [31:0] EXC_Vector_Base_Reset          = 32'h0000_0010;    // MIPS Standard is 0xBFC0_0000
	parameter [31:0] EXC_Vector_Base_Other_NoBoot   = 32'h0000_0000;    // MIPS Standard is 0x8000_0000
	parameter [31:0] EXC_Vector_Base_Other_Boot     = 32'h0000_0000;    // MIPS Standard is 0xBFC0_0200
	parameter [31:0] EXC_Vector_Offset_General      = 32'h0000_0000;    // MIPS Standard is 0x0000_0180
	parameter [31:0] EXC_Vector_Offset_Special      = 32'h0000_0008;    // MIPS Standard is 0x0000_0200
	parameter [31:0] UMem_Lower = 32'h08000000;
	parameter Big_Endian = 1;
	parameter [5:0] Op_Type_R   = 6'b00_0000;  // Standard R-Type instructions
	parameter [5:0] Op_Type_R2  = 6'b01_1100;  // Extended R-Like instructions
	parameter [5:0] Op_Type_BI  = 6'b00_0001;  // Branch/Trap extended instructions
	parameter [5:0] Op_Type_CP0 = 6'b01_0000;  // Coprocessor 0 instructions
	parameter [5:0] Op_Type_CP1 = 6'b01_0001;  // Coprocessor 1 instructions (not implemented)
	parameter [5:0] Op_Type_CP2 = 6'b01_0010;  // Coprocessor 2 instructions (not implemented)
	parameter [5:0] Op_Type_CP3 = 6'b01_0011;  // Coprocessor 3 instructions (not implemented)
	parameter [5:0] Op_Add      = Op_Type_R;
	parameter [5:0] Op_Addi     = 6'b00_1000;
	parameter [5:0] Op_Addiu    = 6'b00_1001;
	parameter [5:0] Op_Addu     = Op_Type_R;
	parameter [5:0] Op_And      = Op_Type_R;
	parameter [5:0] Op_Andi     = 6'b00_1100;
	parameter [5:0] Op_Beq      = 6'b00_0100;
	parameter [5:0] Op_Bgez     = Op_Type_BI;
	parameter [5:0] Op_Bgezal   = Op_Type_BI;
	parameter [5:0] Op_Bgtz     = 6'b00_0111;
	parameter [5:0] Op_Blez     = 6'b00_0110;
	parameter [5:0] Op_Bltz     = Op_Type_BI;
	parameter [5:0] Op_Bltzal   = Op_Type_BI;
	parameter [5:0] Op_Bne      = 6'b00_0101;
	parameter [5:0] Op_Break    = Op_Type_R;
	parameter [5:0] Op_Clo      = Op_Type_R2;
	parameter [5:0] Op_Clz      = Op_Type_R2;
	parameter [5:0] Op_Div      = Op_Type_R;
	parameter [5:0] Op_Divu     = Op_Type_R;
	parameter [5:0] Op_Eret     = Op_Type_CP0;
	parameter [5:0] Op_J        = 6'b00_0010;
	parameter [5:0] Op_Jal      = 6'b00_0011;
	parameter [5:0] Op_Jalr     = Op_Type_R;
	parameter [5:0] Op_Jr       = Op_Type_R;
	parameter [5:0] Op_Lb       = 6'b10_0000;
	parameter [5:0] Op_Lbu      = 6'b10_0100;
	parameter [5:0] Op_Lh       = 6'b10_0001;
	parameter [5:0] Op_Lhu      = 6'b10_0101;
	parameter [5:0] Op_Ll       = 6'b11_0000;
	parameter [5:0] Op_Lui      = 6'b00_1111;
	parameter [5:0] Op_Lw       = 6'b10_0011;
	parameter [5:0] Op_Lwl      = 6'b10_0010;
	parameter [5:0] Op_Lwr      = 6'b10_0110;
	parameter [5:0] Op_Madd     = Op_Type_R2;
	parameter [5:0] Op_Maddu    = Op_Type_R2;
	parameter [5:0] Op_Mfc0     = Op_Type_CP0;
	parameter [5:0] Op_Mfhi     = Op_Type_R;
	parameter [5:0] Op_Mflo     = Op_Type_R;
	parameter [5:0] Op_Movn     = Op_Type_R;
	parameter [5:0] Op_Movz     = Op_Type_R;
	parameter [5:0] Op_Msub     = Op_Type_R2;
	parameter [5:0] Op_Msubu    = Op_Type_R2;
	parameter [5:0] Op_Mtc0     = Op_Type_CP0;
	parameter [5:0] Op_Mthi     = Op_Type_R;
	parameter [5:0] Op_Mtlo     = Op_Type_R;
	parameter [5:0] Op_Mul      = Op_Type_R2;
	parameter [5:0] Op_Mult     = Op_Type_R;
	parameter [5:0] Op_Multu    = Op_Type_R;
	parameter [5:0] Op_Nor      = Op_Type_R;
	parameter [5:0] Op_Or       = Op_Type_R;
	parameter [5:0] Op_Ori      = 6'b00_1101;
	parameter [5:0] Op_Pref     = 6'b11_0011; // Prefetch does nothing in this implementation.
	parameter [5:0] Op_Sb       = 6'b10_1000;
	parameter [5:0] Op_Sc       = 6'b11_1000;
	parameter [5:0] Op_Sh       = 6'b10_1001;
	parameter [5:0] Op_Sll      = Op_Type_R;
	parameter [5:0] Op_Sllv     = Op_Type_R;
	parameter [5:0] Op_Slt      = Op_Type_R;
	parameter [5:0] Op_Slti     = 6'b00_1010;
	parameter [5:0] Op_Sltiu    = 6'b00_1011;
	parameter [5:0] Op_Sltu     = Op_Type_R;
	parameter [5:0] Op_Sra      = Op_Type_R;
	parameter [5:0] Op_Srav     = Op_Type_R;
	parameter [5:0] Op_Srl      = Op_Type_R;
	parameter [5:0] Op_Srlv     = Op_Type_R;
	parameter [5:0] Op_Sub      = Op_Type_R;
	parameter [5:0] Op_Subu     = Op_Type_R;
	parameter [5:0] Op_Sw       = 6'b10_1011;
	parameter [5:0] Op_Swl      = 6'b10_1010;
	parameter [5:0] Op_Swr      = 6'b10_1110;
	parameter [5:0] Op_Syscall  = Op_Type_R;
	parameter [5:0] Op_Teq      = Op_Type_R;
	parameter [5:0] Op_Teqi     = Op_Type_BI;
	parameter [5:0] Op_Tge      = Op_Type_R;
	parameter [5:0] Op_Tgei     = Op_Type_BI;
	parameter [5:0] Op_Tgeiu    = Op_Type_BI;
	parameter [5:0] Op_Tgeu     = Op_Type_R;
	parameter [5:0] Op_Tlt      = Op_Type_R;
	parameter [5:0] Op_Tlti     = Op_Type_BI;
	parameter [5:0] Op_Tltiu    = Op_Type_BI;
	parameter [5:0] Op_Tltu     = Op_Type_R;
	parameter [5:0] Op_Tne      = Op_Type_R;
	parameter [5:0] Op_Tnei     = Op_Type_BI;
	parameter [5:0] Op_Xor      = Op_Type_R;
	parameter [5:0] Op_Xori     = 6'b00_1110;

	/* Op Code Rt fields for Branches & Traps */
	parameter [4:0] OpRt_Bgez   = 5'b00001;
	parameter [4:0] OpRt_Bgezal = 5'b10001;
	parameter [4:0] OpRt_Bltz   = 5'b00000;
	parameter [4:0] OpRt_Bltzal = 5'b10000;
	parameter [4:0] OpRt_Teqi   = 5'b01100;
	parameter [4:0] OpRt_Tgei   = 5'b01000;
	parameter [4:0] OpRt_Tgeiu  = 5'b01001;
	parameter [4:0] OpRt_Tlti   = 5'b01010;
	parameter [4:0] OpRt_Tltiu  = 5'b01011;
	parameter [4:0] OpRt_Tnei   = 5'b01110;

	/* Op Code Rs fields for Coprocessors */
	parameter [4:0] OpRs_MF     = 5'b00000;
	parameter [4:0] OpRs_MT     = 5'b00100;

	/* Special handling for ERET */
	parameter [4:0] OpRs_ERET   = 5'b10000;
	parameter [5:0] Funct_ERET  = 6'b011000;

	/* Function Codes for R-Type Op Codes */
	parameter [5:0] Funct_Add     = 6'b10_0000;
	parameter [5:0] Funct_Addu    = 6'b10_0001;
	parameter [5:0] Funct_And     = 6'b10_0100;
	parameter [5:0] Funct_Break   = 6'b00_1101;
	parameter [5:0] Funct_Clo     = 6'b10_0001; // same as Addu
	parameter [5:0] Funct_Clz     = 6'b10_0000; // same as Add
	parameter [5:0] Funct_Div     = 6'b01_1010;
	parameter [5:0] Funct_Divu    = 6'b01_1011;
	parameter [5:0] Funct_Jr      = 6'b00_1000;
	parameter [5:0] Funct_Jalr    = 6'b00_1001;
	parameter [5:0] Funct_Madd    = 6'b00_0000;
	parameter [5:0] Funct_Maddu   = 6'b00_0001;
	parameter [5:0] Funct_Mfhi    = 6'b01_0000;
	parameter [5:0] Funct_Mflo    = 6'b01_0010;
	parameter [5:0] Funct_Movn    = 6'b00_1011;
	parameter [5:0] Funct_Movz    = 6'b00_1010;
	parameter [5:0] Funct_Msub    = 6'b00_0100; // same as Sllv
	parameter [5:0] Funct_Msubu   = 6'b00_0101;
	parameter [5:0] Funct_Mthi    = 6'b01_0001;
	parameter [5:0] Funct_Mtlo    = 6'b01_0011;
	parameter [5:0] Funct_Mul     = 6'b00_0010; // same as Srl
	parameter [5:0] Funct_Mult    = 6'b01_1000;
	parameter [5:0] Funct_Multu   = 6'b01_1001;
	parameter [5:0] Funct_Nor     = 6'b10_0111;
	parameter [5:0] Funct_Or      = 6'b10_0101;
	parameter [5:0] Funct_Sll     = 6'b00_0000;
	parameter [5:0] Funct_Sllv    = 6'b00_0100;
	parameter [5:0] Funct_Slt     = 6'b10_1010;
	parameter [5:0] Funct_Sltu    = 6'b10_1011;
	parameter [5:0] Funct_Sra     = 6'b00_0011;
	parameter [5:0] Funct_Srav    = 6'b00_0111;
	parameter [5:0] Funct_Srl     = 6'b00_0010;
	parameter [5:0] Funct_Srlv    = 6'b00_0110;
	parameter [5:0] Funct_Sub     = 6'b10_0010;
	parameter [5:0] Funct_Subu    = 6'b10_0011;
	parameter [5:0] Funct_Syscall = 6'b00_1100;
	parameter [5:0] Funct_Teq     = 6'b11_0100;
	parameter [5:0] Funct_Tge     = 6'b11_0000;
	parameter [5:0] Funct_Tgeu    = 6'b11_0001;
	parameter [5:0] Funct_Tlt     = 6'b11_0010;
	parameter [5:0] Funct_Tltu    = 6'b11_0011;
	parameter [5:0] Funct_Tne     = 6'b11_0110;
	parameter [5:0] Funct_Xor     = 6'b10_0110;

	/* ALU Operations (Implementation) */
	parameter [4:0] AluOp_Add    = 5'd1;
	parameter [4:0] AluOp_Addu   = 5'd0;
	parameter [4:0] AluOp_And    = 5'd2;
	parameter [4:0] AluOp_Clo    = 5'd3;
	parameter [4:0] AluOp_Clz    = 5'd4;
	parameter [4:0] AluOp_Div    = 5'd5;
	parameter [4:0] AluOp_Divu   = 5'd6;
	parameter [4:0] AluOp_Madd   = 5'd7;
	parameter [4:0] AluOp_Maddu  = 5'd8;
	parameter [4:0] AluOp_Mfhi   = 5'd9;
	parameter [4:0] AluOp_Mflo   = 5'd10;
	parameter [4:0] AluOp_Msub   = 5'd13;
	parameter [4:0] AluOp_Msubu  = 5'd14;
	parameter [4:0] AluOp_Mthi   = 5'd11;
	parameter [4:0] AluOp_Mtlo   = 5'd12;
	parameter [4:0] AluOp_Mul    = 5'd15;
	parameter [4:0] AluOp_Mult   = 5'd16;
	parameter [4:0] AluOp_Multu  = 5'd17;
	parameter [4:0] AluOp_Nor    = 5'd18;
	parameter [4:0] AluOp_Or     = 5'd19;
	parameter [4:0] AluOp_Sll    = 5'd20;
	parameter [4:0] AluOp_Sllc   = 5'd21;  // Move this if another AluOp is needed
	parameter [4:0] AluOp_Sllv   = 5'd22;
	parameter [4:0] AluOp_Slt    = 5'd23;
	parameter [4:0] AluOp_Sltu   = 5'd24;
	parameter [4:0] AluOp_Sra    = 5'd25;
	parameter [4:0] AluOp_Srav   = 5'd26;
	parameter [4:0] AluOp_Srl    = 5'd27;
	parameter [4:0] AluOp_Srlv   = 5'd28;
	parameter [4:0] AluOp_Sub    = 5'd29;
	parameter [4:0] AluOp_Subu   = 5'd30;
	parameter [4:0] AluOp_Xor    = 5'd31;
	parameter [15:0] DP_None        = 16'b000_00000_000000_00;    // Instructions which require nothing of the main datapath.
	parameter [15:0] DP_RType       = 16'b000_00001_000000_10;    // Standard R-Type
	parameter [15:0] DP_IType       = 16'b000_10000_000000_10;    // Standard I-Type
	parameter [15:0] DP_Branch      = 16'b100_00000_000000_00;    // Standard Branch
	parameter [15:0] DP_BranchLink  = 16'b101_00000_000000_10;    // Branch and Link
	parameter [15:0] DP_HiLoWr      = 16'b000_00000_000000_00;    // Write to Hi/Lo ALU register (Div,Divu,Mult,Multu,Mthi,Mtlo). Currently 'DP_None'.
	parameter [15:0] DP_Jump        = 16'b010_00000_000000_00;    // Standard Jump
	parameter [15:0] DP_JumpLink    = 16'b011_00000_000000_10;    // Jump and Link
	parameter [15:0] DP_JumpLinkReg = 16'b111_00000_000000_10;    // Jump and Link Register
	parameter [15:0] DP_JumpReg     = 16'b110_00000_000000_00;    // Jump Register
	parameter [15:0] DP_LoadByteS   = 16'b000_10000_010011_11;    // Load Byte Signed
	parameter [15:0] DP_LoadByteU   = 16'b000_10000_010010_11;    // Load Byte Unsigned
	parameter [15:0] DP_LoadHalfS   = 16'b000_10000_010101_11;    // Load Half Signed
	parameter [15:0] DP_LoadHalfU   = 16'b000_10000_010100_11;    // Load Half Unsigned
	parameter [15:0] DP_LoadWord    = 16'b000_10000_010000_11;    // Load Word
	parameter [15:0] DP_ExtWrRt     = 16'b000_00000_000000_10;    // A DP-external write to Rt
	parameter [15:0] DP_ExtWrRd     = 16'b000_00001_000000_10;    // A DP-external write to Rd
	parameter [15:0] DP_Movc        = 16'b000_01001_000000_10;    // Conditional Move
	parameter [15:0] DP_LoadLinked  = 16'b000_10000_110000_11;    // Load Linked
	parameter [15:0] DP_StoreCond   = 16'b000_10000_101000_11;    // Store Conditional
	parameter [15:0] DP_StoreByte   = 16'b000_10000_001010_00;    // Store Byte
	parameter [15:0] DP_StoreHalf   = 16'b000_10000_001100_00;    // Store Half
	parameter [15:0] DP_StoreWord   = 16'b000_10000_001000_00;    // Store Word
	parameter [15:0] DP_TrapRegCNZ  = 16'b000_00110_000000_00;    // Trap using Rs and Rt,  non-zero ALU (Tlt,  Tltu,  Tne)
	parameter [15:0] DP_TrapRegCZ   = 16'b000_00100_000000_00;    // Trap using RS and Rt,  zero ALU     (Teq,  Tge,   Tgeu)
	parameter [15:0] DP_TrapImmCNZ  = 16'b000_10110_000000_00;    // Trap using Rs and Imm, non-zero ALU (Tlti, Tltiu, Tnei)
	parameter [15:0] DP_TrapImmCZ   = 16'b000_10100_000000_00;    // Trap using Rs and Imm, zero ALU     (Teqi, Tgei,  Tgeiu)
	//--------------------------------------------------------
	parameter [15:0] DP_Add     = DP_RType;
	parameter [15:0] DP_Addi    = DP_IType;
	parameter [15:0] DP_Addiu   = DP_IType;
	parameter [15:0] DP_Addu    = DP_RType;
	parameter [15:0] DP_And     = DP_RType;
	parameter [15:0] DP_Andi    = DP_IType;
	parameter [15:0] DP_Beq     = DP_Branch;
	parameter [15:0] DP_Bgez    = DP_Branch;
	parameter [15:0] DP_Bgezal  = DP_BranchLink;
	parameter [15:0] DP_Bgtz    = DP_Branch;
	parameter [15:0] DP_Blez    = DP_Branch;
	parameter [15:0] DP_Bltz    = DP_Branch;
	parameter [15:0] DP_Bltzal  = DP_BranchLink;
	parameter [15:0] DP_Bne     = DP_Branch;
	parameter [15:0] DP_Break   = DP_None;
	parameter [15:0] DP_Clo     = DP_RType;
	parameter [15:0] DP_Clz     = DP_RType;
	parameter [15:0] DP_Div     = DP_HiLoWr;
	parameter [15:0] DP_Divu    = DP_HiLoWr;
	parameter [15:0] DP_Eret    = DP_None;
	parameter [15:0] DP_J       = DP_Jump;
	parameter [15:0] DP_Jal     = DP_JumpLink;
	parameter [15:0] DP_Jalr    = DP_JumpLinkReg;
	parameter [15:0] DP_Jr      = DP_JumpReg;
	parameter [15:0] DP_Lb      = DP_LoadByteS;
	parameter [15:0] DP_Lbu     = DP_LoadByteU;
	parameter [15:0] DP_Lh      = DP_LoadHalfS;
	parameter [15:0] DP_Lhu     = DP_LoadHalfU;
	parameter [15:0] DP_Ll      = DP_LoadLinked;
	parameter [15:0] DP_Lui     = DP_IType;
	parameter [15:0] DP_Lw      = DP_LoadWord;
	parameter [15:0] DP_Lwl     = DP_LoadWord;
	parameter [15:0] DP_Lwr     = DP_LoadWord;
	parameter [15:0] DP_Madd    = DP_HiLoWr;
	parameter [15:0] DP_Maddu   = DP_HiLoWr;
	parameter [15:0] DP_Mfc0    = DP_ExtWrRt;
	parameter [15:0] DP_Mfhi    = DP_ExtWrRd;
	parameter [15:0] DP_Mflo    = DP_ExtWrRd;
	parameter [15:0] DP_Movn    = DP_Movc;
	parameter [15:0] DP_Movz    = DP_Movc;
	parameter [15:0] DP_Msub    = DP_HiLoWr;
	parameter [15:0] DP_Msubu   = DP_HiLoWr;
	parameter [15:0] DP_Mtc0    = DP_None;
	parameter [15:0] DP_Mthi    = DP_HiLoWr;
	parameter [15:0] DP_Mtlo    = DP_HiLoWr;
	parameter [15:0] DP_Mul     = DP_RType;
	parameter [15:0] DP_Mult    = DP_HiLoWr;
	parameter [15:0] DP_Multu   = DP_HiLoWr;
	parameter [15:0] DP_Nor     = DP_RType;
	parameter [15:0] DP_Or      = DP_RType;
	parameter [15:0] DP_Ori     = DP_IType;
	parameter [15:0] DP_Pref    = DP_None; // Not Implemented
	parameter [15:0] DP_Sb      = DP_StoreByte;
	parameter [15:0] DP_Sc      = DP_StoreCond;
	parameter [15:0] DP_Sh      = DP_StoreHalf;
	parameter [15:0] DP_Sll     = DP_RType;
	parameter [15:0] DP_Sllv    = DP_RType;
	parameter [15:0] DP_Slt     = DP_RType;
	parameter [15:0] DP_Slti    = DP_IType;
	parameter [15:0] DP_Sltiu   = DP_IType;
	parameter [15:0] DP_Sltu    = DP_RType;
	parameter [15:0] DP_Sra     = DP_RType;
	parameter [15:0] DP_Srav    = DP_RType;
	parameter [15:0] DP_Srl     = DP_RType;
	parameter [15:0] DP_Srlv    = DP_RType;
	parameter [15:0] DP_Sub     = DP_RType;
	parameter [15:0] DP_Subu    = DP_RType;
	parameter [15:0] DP_Sw      = DP_StoreWord;
	parameter [15:0] DP_Swl     = DP_StoreWord;
	parameter [15:0] DP_Swr     = DP_StoreWord;
	parameter [15:0] DP_Syscall = DP_None;
	parameter [15:0] DP_Teq     = DP_TrapRegCZ;
	parameter [15:0] DP_Teqi    = DP_TrapImmCZ;
	parameter [15:0] DP_Tge     = DP_TrapRegCZ;
	parameter [15:0] DP_Tgei    = DP_TrapImmCZ;
	parameter [15:0] DP_Tgeiu   = DP_TrapImmCZ;
	parameter [15:0] DP_Tgeu    = DP_TrapRegCZ;
	parameter [15:0] DP_Tlt     = DP_TrapRegCNZ;
	parameter [15:0] DP_Tlti    = DP_TrapImmCNZ;
	parameter [15:0] DP_Tltiu   = DP_TrapImmCNZ;
	parameter [15:0] DP_Tltu    = DP_TrapRegCNZ;
	parameter [15:0] DP_Tne     = DP_TrapRegCNZ;
	parameter [15:0] DP_Tnei    = DP_TrapImmCNZ;
	parameter [15:0] DP_Xor     = DP_RType;
	parameter [15:0] DP_Xori    = DP_IType;

	parameter [2:0] EXC_None = 3'b000;
	parameter [2:0] EXC_ID   = 3'b100;
	parameter [2:0] EXC_EX   = 3'b010;
	parameter [2:0] EXC_MEM  = 3'b001;
	//--------------------------------
	parameter [2:0] EXC_Add     = EXC_EX;
	parameter [2:0] EXC_Addi    = EXC_EX;
	parameter [2:0] EXC_Addiu   = EXC_None;
	parameter [2:0] EXC_Addu    = EXC_None;
	parameter [2:0] EXC_And     = EXC_None;
	parameter [2:0] EXC_Andi    = EXC_None;
	parameter [2:0] EXC_Beq     = EXC_None;
	parameter [2:0] EXC_Bgez    = EXC_None;
	parameter [2:0] EXC_Bgezal  = EXC_None;
	parameter [2:0] EXC_Bgtz    = EXC_None;
	parameter [2:0] EXC_Blez    = EXC_None;
	parameter [2:0] EXC_Bltz    = EXC_None;
	parameter [2:0] EXC_Bltzal  = EXC_None;
	parameter [2:0] EXC_Bne     = EXC_None;
	parameter [2:0] EXC_Break   = EXC_ID;
	parameter [2:0] EXC_Clo     = EXC_None;
	parameter [2:0] EXC_Clz     = EXC_None;
	parameter [2:0] EXC_Div     = EXC_None;
	parameter [2:0] EXC_Divu    = EXC_None;
	parameter [2:0] EXC_Eret    = EXC_ID;
	parameter [2:0] EXC_J       = EXC_None;
	parameter [2:0] EXC_Jal     = EXC_None;
	parameter [2:0] EXC_Jalr    = EXC_None;
	parameter [2:0] EXC_Jr      = EXC_None;
	parameter [2:0] EXC_Lb      = EXC_MEM;
	parameter [2:0] EXC_Lbu     = EXC_MEM;
	parameter [2:0] EXC_Lh      = EXC_MEM;
	parameter [2:0] EXC_Lhu     = EXC_MEM;
	parameter [2:0] EXC_Ll      = EXC_MEM;
	parameter [2:0] EXC_Lui     = EXC_None;
	parameter [2:0] EXC_Lw      = EXC_MEM;
	parameter [2:0] EXC_Lwl     = EXC_MEM;
	parameter [2:0] EXC_Lwr     = EXC_MEM;
	parameter [2:0] EXC_Madd    = EXC_None;
	parameter [2:0] EXC_Maddu   = EXC_None;
	parameter [2:0] EXC_Mfc0    = EXC_ID;
	parameter [2:0] EXC_Mfhi    = EXC_None;
	parameter [2:0] EXC_Mflo    = EXC_None;
	parameter [2:0] EXC_Movn    = EXC_None;
	parameter [2:0] EXC_Movz    = EXC_None;
	parameter [2:0] EXC_Msub    = EXC_None;
	parameter [2:0] EXC_Msubu   = EXC_None;
	parameter [2:0] EXC_Mtc0    = EXC_ID;
	parameter [2:0] EXC_Mthi    = EXC_None;
	parameter [2:0] EXC_Mtlo    = EXC_None;
	parameter [2:0] EXC_Mul     = EXC_None;
	parameter [2:0] EXC_Mult    = EXC_None;
	parameter [2:0] EXC_Multu   = EXC_None;
	parameter [2:0] EXC_Nor     = EXC_None;
	parameter [2:0] EXC_Or      = EXC_None;
	parameter [2:0] EXC_Ori     = EXC_None;
	parameter [2:0] EXC_Pref    = EXC_None; // XXX
	parameter [2:0] EXC_Sb      = EXC_MEM;
	parameter [2:0] EXC_Sc      = EXC_MEM;
	parameter [2:0] EXC_Sh      = EXC_MEM;
	parameter [2:0] EXC_Sll     = EXC_None;
	parameter [2:0] EXC_Sllv    = EXC_None;
	parameter [2:0] EXC_Slt     = EXC_None;
	parameter [2:0] EXC_Slti    = EXC_None;
	parameter [2:0] EXC_Sltiu   = EXC_None;
	parameter [2:0] EXC_Sltu    = EXC_None;
	parameter [2:0] EXC_Sra     = EXC_None;
	parameter [2:0] EXC_Srav    = EXC_None;
	parameter [2:0] EXC_Srl     = EXC_None;
	parameter [2:0] EXC_Srlv    = EXC_None;
	parameter [2:0] EXC_Sub     = EXC_EX;
	parameter [2:0] EXC_Subu    = EXC_None;
	parameter [2:0] EXC_Sw      = EXC_MEM;
	parameter [2:0] EXC_Swl     = EXC_MEM;
	parameter [2:0] EXC_Swr     = EXC_MEM;
	parameter [2:0] EXC_Syscall = EXC_ID;
	parameter [2:0] EXC_Teq     = EXC_MEM;
	parameter [2:0] EXC_Teqi    = EXC_MEM;
	parameter [2:0] EXC_Tge     = EXC_MEM;
	parameter [2:0] EXC_Tgei    = EXC_MEM;
	parameter [2:0] EXC_Tgeiu   = EXC_MEM;
	parameter [2:0] EXC_Tgeu    = EXC_MEM;
	parameter [2:0] EXC_Tlt     = EXC_MEM;
	parameter [2:0] EXC_Tlti    = EXC_MEM;
	parameter [2:0] EXC_Tltiu   = EXC_MEM;
	parameter [2:0] EXC_Tltu    = EXC_MEM;
	parameter [2:0] EXC_Tne     = EXC_MEM;
	parameter [2:0] EXC_Tnei    = EXC_MEM;
	parameter [2:0] EXC_Xor     = EXC_None;
	parameter [2:0] EXC_Xori    = EXC_None;

	parameter [7:0] HAZ_Nothing  = 8'b00000000; // Jumps, Lui, Mfhi/lo, special, etc.
	parameter [7:0] HAZ_IDRsIDRt = 8'b11110000; // Beq, Bne, Traps
	parameter [7:0] HAZ_IDRs     = 8'b11000000; // Most branches, Jumps to registers
	parameter [7:0] HAZ_IDRt     = 8'b00110000; // Mtc0
	parameter [7:0] HAZ_IDRtEXRs = 8'b10111100; // Movn, Movz
	parameter [7:0] HAZ_EXRsEXRt = 8'b10101111; // Many R-Type ops
	parameter [7:0] HAZ_EXRs     = 8'b10001100; // Immediates: Loads, Clo/z, Mthi/lo, etc.
	parameter [7:0] HAZ_EXRsWRt  = 8'b10101110; // Stores
	parameter [7:0] HAZ_EXRt     = 8'b00100011; // Shifts using Shamt field
	//-----------------------------------------
	parameter [7:0] HAZ_Add     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Addi    = HAZ_EXRs;
	parameter [7:0] HAZ_Addiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Addu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_And     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Andi    = HAZ_EXRs;
	parameter [7:0] HAZ_Beq     = HAZ_IDRsIDRt;
	parameter [7:0] HAZ_Bgez    = HAZ_IDRs;
	parameter [7:0] HAZ_Bgezal  = HAZ_IDRs;
	parameter [7:0] HAZ_Bgtz    = HAZ_IDRs;
	parameter [7:0] HAZ_Blez    = HAZ_IDRs;
	parameter [7:0] HAZ_Bltz    = HAZ_IDRs;
	parameter [7:0] HAZ_Bltzal  = HAZ_IDRs;
	parameter [7:0] HAZ_Bne     = HAZ_IDRsIDRt;
	parameter [7:0] HAZ_Break   = HAZ_Nothing;
	parameter [7:0] HAZ_Clo     = HAZ_EXRs;
	parameter [7:0] HAZ_Clz     = HAZ_EXRs;
	parameter [7:0] HAZ_Div     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Divu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Eret    = HAZ_Nothing;
	parameter [7:0] HAZ_J       = HAZ_Nothing;
	parameter [7:0] HAZ_Jal     = HAZ_Nothing;
	parameter [7:0] HAZ_Jalr    = HAZ_IDRs;
	parameter [7:0] HAZ_Jr      = HAZ_IDRs;
	parameter [7:0] HAZ_Lb      = HAZ_EXRs;
	parameter [7:0] HAZ_Lbu     = HAZ_EXRs;
	parameter [7:0] HAZ_Lh      = HAZ_EXRs;
	parameter [7:0] HAZ_Lhu     = HAZ_EXRs;
	parameter [7:0] HAZ_Ll      = HAZ_EXRs;
	parameter [7:0] HAZ_Lui     = HAZ_Nothing;
	parameter [7:0] HAZ_Lw      = HAZ_EXRs;
	parameter [7:0] HAZ_Lwl     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Lwr     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Madd    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Maddu   = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Mfc0    = HAZ_Nothing;
	parameter [7:0] HAZ_Mfhi    = HAZ_Nothing;
	parameter [7:0] HAZ_Mflo    = HAZ_Nothing;
	parameter [7:0] HAZ_Movn    = HAZ_IDRtEXRs;
	parameter [7:0] HAZ_Movz    = HAZ_IDRtEXRs;
	parameter [7:0] HAZ_Msub    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Msubu   = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Mtc0    = HAZ_IDRt;
	parameter [7:0] HAZ_Mthi    = HAZ_EXRs;
	parameter [7:0] HAZ_Mtlo    = HAZ_EXRs;
	parameter [7:0] HAZ_Mul     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Mult    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Multu   = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Nor     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Or      = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Ori     = HAZ_EXRs;
	parameter [7:0] HAZ_Pref    = HAZ_Nothing; // XXX
	parameter [7:0] HAZ_Sb      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Sc      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Sh      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Sll     = HAZ_EXRt;
	parameter [7:0] HAZ_Sllv    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Slt     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Slti    = HAZ_EXRs;
	parameter [7:0] HAZ_Sltiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Sltu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Sra     = HAZ_EXRt;
	parameter [7:0] HAZ_Srav    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Srl     = HAZ_EXRt;
	parameter [7:0] HAZ_Srlv    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Sub     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Subu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Sw      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Swl     = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Swr     = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Syscall = HAZ_Nothing;
	parameter [7:0] HAZ_Teq     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Teqi    = HAZ_EXRs;
	parameter [7:0] HAZ_Tge     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tgei    = HAZ_EXRs;
	parameter [7:0] HAZ_Tgeiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Tgeu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tlt     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tlti    = HAZ_EXRs;
	parameter [7:0] HAZ_Tltiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Tltu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tne     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tnei    = HAZ_EXRs;
	parameter [7:0] HAZ_Xor     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Xori    = HAZ_EXRs;

    
    /***
     Performance Notes:
     
     The ALU is the longest delay path in the Execute stage, and one of the longest
     in the entire processor. This path varies based on the logic blocks that are
     chosen to implement various functions, but there is certainly room to improve
     the speed of arithmetic operations. The ALU could also be placed in a separate
     pipeline stage after the Execute forwarding has completed.
    ***/
    
    
    /***
     Divider Logic:
     
     The hardware divider requires 32 cycles to complete. Because it writes its
     results to HILO and not to the pipeline, the pipeline can proceed without
     stalling. When a later instruction tries to access HILO, the pipeline will
     stall if the divide operation has not yet completed.
    ***/
    
    
    // Internal state registers
    reg  [63:0] HILO;
    reg  HILO_Access;                   // Behavioral; not DFFs
    reg  [5:0] CLO_Result, CLZ_Result;  // Behavioral; not DFFs
    reg  div_fsm;
    
    // Internal signals
    wire [31:0] HI, LO;
    wire HILO_Commit;
    wire signed [31:0] As, Bs;
    wire AddSub_Add;
    wire signed [31:0] AddSub_Result;
    wire signed [63:0] Mult_Result;
    wire [63:0] Multu_Result;
    wire [31:0] Quotient;
    wire [31:0] Remainder;
    wire Div_Stall;
    wire Div_Start, Divu_Start;
    wire DivOp;
    wire Div_Commit;
    
    // Assignments
    assign HI = HILO[63:32];
    assign LO = HILO[31:0];
    assign HILO_Commit = ~(EX_Stall | EX_Flush);
    assign As = A;
    assign Bs = B;
    assign AddSub_Add = ((Operation == AluOp_Add) | (Operation == AluOp_Addu));
    assign AddSub_Result = (AddSub_Add) ? (A + B) : (A - B);
    assign Mult_Result = As * Bs;
    assign Multu_Result = A * B;
    assign BZero = (B == 32'h00000000);
    assign DivOp = (Operation == AluOp_Div) || (Operation == AluOp_Divu);
    assign Div_Commit   = (div_fsm == 1'b1) && (Div_Stall == 1'b0);
    assign Div_Start    = (div_fsm == 1'b0) && (Operation == AluOp_Div)  && (HILO_Commit == 1'b1);
    assign Divu_Start   = (div_fsm == 1'b0) && (Operation == AluOp_Divu) && (HILO_Commit == 1'b1);
    assign ALU_Stall    = (div_fsm == 1'b1) && (HILO_Access == 1'b1);
    
    always @(*) begin
        case (Operation)
            AluOp_Add   : Result = AddSub_Result;
            AluOp_Addu  : Result = AddSub_Result;
            AluOp_And   : Result = A & B;
            AluOp_Clo   : Result = {26'b0, CLO_Result};
            AluOp_Clz   : Result = {26'b0, CLZ_Result};
            AluOp_Mfhi  : Result = HI;
            AluOp_Mflo  : Result = LO;
            AluOp_Mul   : Result = Multu_Result[31:0];
            AluOp_Nor   : Result = ~(A | B);
            AluOp_Or    : Result = A | B;
            AluOp_Sll   : Result = B << Shamt;
            AluOp_Sllc  : Result = {B[15:0], 16'b0};
            AluOp_Sllv  : Result = B << A[4:0];
            AluOp_Slt   : Result = (As < Bs) ? 32'h00000001 : 32'h00000000;
            AluOp_Sltu  : Result = (A < B)   ? 32'h00000001 : 32'h00000000;
            AluOp_Sra   : Result = Bs >> Shamt;
            AluOp_Srav  : Result = Bs >> As[4:0];
            AluOp_Srl   : Result = B >> Shamt;
            AluOp_Srlv  : Result = B >> A[4:0];
            AluOp_Sub   : Result = AddSub_Result;
            AluOp_Subu  : Result = AddSub_Result;
            AluOp_Xor   : Result = A ^ B;
            default     : Result = 32'bx;
        endcase
    end
    
    
    always @(posedge clock) begin
        if (reset) begin
            HILO <= 64'h00000000_00000000;
        end
        else if (Div_Commit) begin
            HILO <= {Remainder, Quotient};
        end
        else if (HILO_Commit) begin
            case (Operation)
                AluOp_Mult  : HILO <= Mult_Result;
                AluOp_Multu : HILO <= Multu_Result;
                AluOp_Madd  : HILO <= HILO + Mult_Result;
                AluOp_Maddu : HILO <= HILO + Multu_Result;
                AluOp_Msub  : HILO <= HILO - Mult_Result;
                AluOp_Msubu : HILO <= HILO - Multu_Result;
                AluOp_Mthi  : HILO <= {A, LO};
                AluOp_Mtlo  : HILO <= {HI, B};
                default     : HILO <= HILO;
            endcase
        end
        else begin
            HILO <= HILO;
        end
    end
    
    // Detect accesses to HILO. RAW and WAW hazards are possible while a
    // divide operation is computing, so reads and writes to HILO must stall
    // while the divider is busy.
    // (This logic could be put into an earlier pipeline stage or into the
    // datapath bits to improve timing.)
    always @(Operation) begin
        case (Operation)
            AluOp_Div   : HILO_Access = 1;
            AluOp_Divu  : HILO_Access = 1;
            AluOp_Mfhi  : HILO_Access = 1;
            AluOp_Mflo  : HILO_Access = 1;
            AluOp_Mult  : HILO_Access = 1;
            AluOp_Multu : HILO_Access = 1;
            AluOp_Madd  : HILO_Access = 1;
            AluOp_Maddu : HILO_Access = 1;
            AluOp_Msub  : HILO_Access = 1;
            AluOp_Msubu : HILO_Access = 1;
            AluOp_Mthi  : HILO_Access = 1;
            AluOp_Mtlo  : HILO_Access = 1;
            default     : HILO_Access = 0;
        endcase
    end
    
    // Divider FSM: The divide unit is either available or busy.
    always @(posedge clock) begin
        if (reset) begin
            div_fsm <= 1'd0;
        end
        else begin
            case (div_fsm)
                1'd0 : div_fsm <= (DivOp & HILO_Commit) ? 1'd1 : 1'd0;
                1'd1 : div_fsm <= (~Div_Stall) ? 1'd0 : 1'd1;
            endcase
        end
    end
    
    // Detect overflow for signed operations. Note that MIPS32 has no overflow
    // detection for multiplication/division operations.
    always @(*) begin
        case (Operation)
            AluOp_Add : EXC_Ov = ((A[31] ~^ B[31]) & (A[31] ^ AddSub_Result[31]));
            AluOp_Sub : EXC_Ov = ((A[31]  ^ B[31]) & (A[31] ^ AddSub_Result[31]));
            default   : EXC_Ov = 0;
        endcase
    end
    
    // Count Leading Ones
    always @(A) begin
        casez (A)
            32'b0???_????_????_????_????_????_????_???? : CLO_Result = 6'd0;
            32'b10??_????_????_????_????_????_????_???? : CLO_Result = 6'd1;
            32'b110?_????_????_????_????_????_????_???? : CLO_Result = 6'd2;
            32'b1110_????_????_????_????_????_????_???? : CLO_Result = 6'd3;
            32'b1111_0???_????_????_????_????_????_???? : CLO_Result = 6'd4;
            32'b1111_10??_????_????_????_????_????_???? : CLO_Result = 6'd5;
            32'b1111_110?_????_????_????_????_????_???? : CLO_Result = 6'd6;
            32'b1111_1110_????_????_????_????_????_???? : CLO_Result = 6'd7;
            32'b1111_1111_0???_????_????_????_????_???? : CLO_Result = 6'd8;
            32'b1111_1111_10??_????_????_????_????_???? : CLO_Result = 6'd9;
            32'b1111_1111_110?_????_????_????_????_???? : CLO_Result = 6'd10;
            32'b1111_1111_1110_????_????_????_????_???? : CLO_Result = 6'd11;
            32'b1111_1111_1111_0???_????_????_????_???? : CLO_Result = 6'd12;
            32'b1111_1111_1111_10??_????_????_????_???? : CLO_Result = 6'd13;
            32'b1111_1111_1111_110?_????_????_????_???? : CLO_Result = 6'd14;
            32'b1111_1111_1111_1110_????_????_????_???? : CLO_Result = 6'd15;
            32'b1111_1111_1111_1111_0???_????_????_???? : CLO_Result = 6'd16;
            32'b1111_1111_1111_1111_10??_????_????_???? : CLO_Result = 6'd17;
            32'b1111_1111_1111_1111_110?_????_????_???? : CLO_Result = 6'd18;
            32'b1111_1111_1111_1111_1110_????_????_???? : CLO_Result = 6'd19;
            32'b1111_1111_1111_1111_1111_0???_????_???? : CLO_Result = 6'd20;
            32'b1111_1111_1111_1111_1111_10??_????_???? : CLO_Result = 6'd21;
            32'b1111_1111_1111_1111_1111_110?_????_???? : CLO_Result = 6'd22;
            32'b1111_1111_1111_1111_1111_1110_????_???? : CLO_Result = 6'd23;
            32'b1111_1111_1111_1111_1111_1111_0???_???? : CLO_Result = 6'd24;
            32'b1111_1111_1111_1111_1111_1111_10??_???? : CLO_Result = 6'd25;
            32'b1111_1111_1111_1111_1111_1111_110?_???? : CLO_Result = 6'd26;
            32'b1111_1111_1111_1111_1111_1111_1110_???? : CLO_Result = 6'd27;
            32'b1111_1111_1111_1111_1111_1111_1111_0??? : CLO_Result = 6'd28;
            32'b1111_1111_1111_1111_1111_1111_1111_10?? : CLO_Result = 6'd29;
            32'b1111_1111_1111_1111_1111_1111_1111_110? : CLO_Result = 6'd30;
            32'b1111_1111_1111_1111_1111_1111_1111_1110 : CLO_Result = 6'd31;
            32'b1111_1111_1111_1111_1111_1111_1111_1111 : CLO_Result = 6'd32;
            default : CLO_Result = 6'd0;
        endcase
    end

    // Count Leading Zeros
    always @(A) begin
        casez (A)
            32'b1???_????_????_????_????_????_????_???? : CLZ_Result = 6'd0;
            32'b01??_????_????_????_????_????_????_???? : CLZ_Result = 6'd1;
            32'b001?_????_????_????_????_????_????_???? : CLZ_Result = 6'd2;
            32'b0001_????_????_????_????_????_????_???? : CLZ_Result = 6'd3;
            32'b0000_1???_????_????_????_????_????_???? : CLZ_Result = 6'd4;
            32'b0000_01??_????_????_????_????_????_???? : CLZ_Result = 6'd5;
            32'b0000_001?_????_????_????_????_????_???? : CLZ_Result = 6'd6;
            32'b0000_0001_????_????_????_????_????_???? : CLZ_Result = 6'd7;
            32'b0000_0000_1???_????_????_????_????_???? : CLZ_Result = 6'd8;
            32'b0000_0000_01??_????_????_????_????_???? : CLZ_Result = 6'd9;
            32'b0000_0000_001?_????_????_????_????_???? : CLZ_Result = 6'd10;
            32'b0000_0000_0001_????_????_????_????_???? : CLZ_Result = 6'd11;
            32'b0000_0000_0000_1???_????_????_????_???? : CLZ_Result = 6'd12;
            32'b0000_0000_0000_01??_????_????_????_???? : CLZ_Result = 6'd13;
            32'b0000_0000_0000_001?_????_????_????_???? : CLZ_Result = 6'd14;
            32'b0000_0000_0000_0001_????_????_????_???? : CLZ_Result = 6'd15;
            32'b0000_0000_0000_0000_1???_????_????_???? : CLZ_Result = 6'd16;
            32'b0000_0000_0000_0000_01??_????_????_???? : CLZ_Result = 6'd17;
            32'b0000_0000_0000_0000_001?_????_????_???? : CLZ_Result = 6'd18;
            32'b0000_0000_0000_0000_0001_????_????_???? : CLZ_Result = 6'd19;
            32'b0000_0000_0000_0000_0000_1???_????_???? : CLZ_Result = 6'd20;
            32'b0000_0000_0000_0000_0000_01??_????_???? : CLZ_Result = 6'd21;
            32'b0000_0000_0000_0000_0000_001?_????_???? : CLZ_Result = 6'd22;
            32'b0000_0000_0000_0000_0000_0001_????_???? : CLZ_Result = 6'd23;
            32'b0000_0000_0000_0000_0000_0000_1???_???? : CLZ_Result = 6'd24;
            32'b0000_0000_0000_0000_0000_0000_01??_???? : CLZ_Result = 6'd25;
            32'b0000_0000_0000_0000_0000_0000_001?_???? : CLZ_Result = 6'd26;
            32'b0000_0000_0000_0000_0000_0000_0001_???? : CLZ_Result = 6'd27;
            32'b0000_0000_0000_0000_0000_0000_0000_1??? : CLZ_Result = 6'd28;
            32'b0000_0000_0000_0000_0000_0000_0000_01?? : CLZ_Result = 6'd29;
            32'b0000_0000_0000_0000_0000_0000_0000_001? : CLZ_Result = 6'd30;
            32'b0000_0000_0000_0000_0000_0000_0000_0001 : CLZ_Result = 6'd31;
            32'b0000_0000_0000_0000_0000_0000_0000_0000 : CLZ_Result = 6'd32;
            default : CLZ_Result = 6'd0;
        endcase
    end

    // Multicycle divide unit
    Divide Divider (
        .clock      (clock),
        .reset      (reset),
        .OP_div     (Div_Start),
        .OP_divu    (Divu_Start),
        .Dividend   (A),
        .Divisor    (B),
        .Quotient   (Quotient),
        .Remainder  (Remainder),
        .Stall      (Div_Stall)
    );

endmodule

