    `timescale 1ns / 1ps
/*
 * File         : Control.v
 * Project      : University of Utah, XUM Project MIPS32 core
 * Creator(s)   : Grant Ayers (ayers@cs.utah.edu)
 *
 * Modification History:
 *   Rev   Date         Initials  Description of Change
 *   1.0    7-Jun-2011  GEA       Initial design.
 *   2.0   26-May-2012  GEA       Release version with CP0.
 *
 * Standards/Formatting:
 *   Verilog 2001, 4 soft tab, wide column.
 *
 * Description:
 *   The Datapath Controller. This module sets the datapath control
 *   bits for an incoming instruction. These control bits follow the
 *   instruction through each pipeline stage as needed, and constitute
 *   the effective operation of the processor through each pipeline stage.
 */
module Control(
    input  ID_Stall,
    input  [5:0] OpCode,
    input  [5:0] Funct,
    input  [4:0] Rs,        // used to differentiate mfc0 and mtc0
    input  [4:0] Rt,        // used to differentiate bgez,bgezal,bltz,bltzal,teqi,tgei,tgeiu,tlti,tltiu,tnei
    input  Cmp_EQ,
    input  Cmp_GZ,
    input  Cmp_GEZ,
    input  Cmp_LZ,
    input  Cmp_LEZ,
    //------------
    output IF_Flush,
    output reg [7:0] DP_Hazards,
    output [1:0] PCSrc,
    output SignExtend,
    output Link,
    output Movn,
    output Movz,
    output Mfc0,
    output Mtc0,
    output CP1,
    output CP2,
    output CP3,
    output Eret,
    output Trap,
    output TrapCond,
    output EXC_Sys,
    output EXC_Bp,
    output EXC_RI,
    output ID_CanErr,
    output EX_CanErr,
    output M_CanErr,
    output NextIsDelay,
    output RegDst,
    output ALUSrcImm,
    output reg [4:0] ALUOp,
    output LLSC,
    output MemWrite,
    output MemRead,
    output MemByte,
    output MemHalf,
    output MemSignExtend,
    output Left,
    output Right,
    output RegWrite,
    output MemtoReg
    );
    
    	parameter [31:0] EXC_Vector_Base_Reset          = 32'h0000_0010;    // MIPS Standard is 0xBFC0_0000
	parameter [31:0] EXC_Vector_Base_Other_NoBoot   = 32'h0000_0000;    // MIPS Standard is 0x8000_0000
	parameter [31:0] EXC_Vector_Base_Other_Boot     = 32'h0000_0000;    // MIPS Standard is 0xBFC0_0200
	parameter [31:0] EXC_Vector_Offset_General      = 32'h0000_0000;    // MIPS Standard is 0x0000_0180
	parameter [31:0] EXC_Vector_Offset_Special      = 32'h0000_0008;    // MIPS Standard is 0x0000_0200
	parameter [31:0] UMem_Lower = 32'h08000000;
	parameter Big_Endian = 1;
	parameter [5:0] Op_Type_R   = 6'b00_0000;  // Standard R-Type instructions
	parameter [5:0] Op_Type_R2  = 6'b01_1100;  // Extended R-Like instructions
	parameter [5:0] Op_Type_BI  = 6'b00_0001;  // Branch/Trap extended instructions
	parameter [5:0] Op_Type_CP0 = 6'b01_0000;  // Coprocessor 0 instructions
	parameter [5:0] Op_Type_CP1 = 6'b01_0001;  // Coprocessor 1 instructions (not implemented)
	parameter [5:0] Op_Type_CP2 = 6'b01_0010;  // Coprocessor 2 instructions (not implemented)
	parameter [5:0] Op_Type_CP3 = 6'b01_0011;  // Coprocessor 3 instructions (not implemented)
	parameter [5:0] Op_Add      = Op_Type_R;
	parameter [5:0] Op_Addi     = 6'b00_1000;
	parameter [5:0] Op_Addiu    = 6'b00_1001;
	parameter [5:0] Op_Addu     = Op_Type_R;
	parameter [5:0] Op_And      = Op_Type_R;
	parameter [5:0] Op_Andi     = 6'b00_1100;
	parameter [5:0] Op_Beq      = 6'b00_0100;
	parameter [5:0] Op_Bgez     = Op_Type_BI;
	parameter [5:0] Op_Bgezal   = Op_Type_BI;
	parameter [5:0] Op_Bgtz     = 6'b00_0111;
	parameter [5:0] Op_Blez     = 6'b00_0110;
	parameter [5:0] Op_Bltz     = Op_Type_BI;
	parameter [5:0] Op_Bltzal   = Op_Type_BI;
	parameter [5:0] Op_Bne      = 6'b00_0101;
	parameter [5:0] Op_Break    = Op_Type_R;
	parameter [5:0] Op_Clo      = Op_Type_R2;
	parameter [5:0] Op_Clz      = Op_Type_R2;
	parameter [5:0] Op_Div      = Op_Type_R;
	parameter [5:0] Op_Divu     = Op_Type_R;
	parameter [5:0] Op_Eret     = Op_Type_CP0;
	parameter [5:0] Op_J        = 6'b00_0010;
	parameter [5:0] Op_Jal      = 6'b00_0011;
	parameter [5:0] Op_Jalr     = Op_Type_R;
	parameter [5:0] Op_Jr       = Op_Type_R;
	parameter [5:0] Op_Lb       = 6'b10_0000;
	parameter [5:0] Op_Lbu      = 6'b10_0100;
	parameter [5:0] Op_Lh       = 6'b10_0001;
	parameter [5:0] Op_Lhu      = 6'b10_0101;
	parameter [5:0] Op_Ll       = 6'b11_0000;
	parameter [5:0] Op_Lui      = 6'b00_1111;
	parameter [5:0] Op_Lw       = 6'b10_0011;
	parameter [5:0] Op_Lwl      = 6'b10_0010;
	parameter [5:0] Op_Lwr      = 6'b10_0110;
	parameter [5:0] Op_Madd     = Op_Type_R2;
	parameter [5:0] Op_Maddu    = Op_Type_R2;
	parameter [5:0] Op_Mfc0     = Op_Type_CP0;
	parameter [5:0] Op_Mfhi     = Op_Type_R;
	parameter [5:0] Op_Mflo     = Op_Type_R;
	parameter [5:0] Op_Movn     = Op_Type_R;
	parameter [5:0] Op_Movz     = Op_Type_R;
	parameter [5:0] Op_Msub     = Op_Type_R2;
	parameter [5:0] Op_Msubu    = Op_Type_R2;
	parameter [5:0] Op_Mtc0     = Op_Type_CP0;
	parameter [5:0] Op_Mthi     = Op_Type_R;
	parameter [5:0] Op_Mtlo     = Op_Type_R;
	parameter [5:0] Op_Mul      = Op_Type_R2;
	parameter [5:0] Op_Mult     = Op_Type_R;
	parameter [5:0] Op_Multu    = Op_Type_R;
	parameter [5:0] Op_Nor      = Op_Type_R;
	parameter [5:0] Op_Or       = Op_Type_R;
	parameter [5:0] Op_Ori      = 6'b00_1101;
	parameter [5:0] Op_Pref     = 6'b11_0011; // Prefetch does nothing in this implementation.
	parameter [5:0] Op_Sb       = 6'b10_1000;
	parameter [5:0] Op_Sc       = 6'b11_1000;
	parameter [5:0] Op_Sh       = 6'b10_1001;
	parameter [5:0] Op_Sll      = Op_Type_R;
	parameter [5:0] Op_Sllv     = Op_Type_R;
	parameter [5:0] Op_Slt      = Op_Type_R;
	parameter [5:0] Op_Slti     = 6'b00_1010;
	parameter [5:0] Op_Sltiu    = 6'b00_1011;
	parameter [5:0] Op_Sltu     = Op_Type_R;
	parameter [5:0] Op_Sra      = Op_Type_R;
	parameter [5:0] Op_Srav     = Op_Type_R;
	parameter [5:0] Op_Srl      = Op_Type_R;
	parameter [5:0] Op_Srlv     = Op_Type_R;
	parameter [5:0] Op_Sub      = Op_Type_R;
	parameter [5:0] Op_Subu     = Op_Type_R;
	parameter [5:0] Op_Sw       = 6'b10_1011;
	parameter [5:0] Op_Swl      = 6'b10_1010;
	parameter [5:0] Op_Swr      = 6'b10_1110;
	parameter [5:0] Op_Syscall  = Op_Type_R;
	parameter [5:0] Op_Teq      = Op_Type_R;
	parameter [5:0] Op_Teqi     = Op_Type_BI;
	parameter [5:0] Op_Tge      = Op_Type_R;
	parameter [5:0] Op_Tgei     = Op_Type_BI;
	parameter [5:0] Op_Tgeiu    = Op_Type_BI;
	parameter [5:0] Op_Tgeu     = Op_Type_R;
	parameter [5:0] Op_Tlt      = Op_Type_R;
	parameter [5:0] Op_Tlti     = Op_Type_BI;
	parameter [5:0] Op_Tltiu    = Op_Type_BI;
	parameter [5:0] Op_Tltu     = Op_Type_R;
	parameter [5:0] Op_Tne      = Op_Type_R;
	parameter [5:0] Op_Tnei     = Op_Type_BI;
	parameter [5:0] Op_Xor      = Op_Type_R;
	parameter [5:0] Op_Xori     = 6'b00_1110;

	/* Op Code Rt fields for Branches & Traps */
	parameter [4:0] OpRt_Bgez   = 5'b00001;
	parameter [4:0] OpRt_Bgezal = 5'b10001;
	parameter [4:0] OpRt_Bltz   = 5'b00000;
	parameter [4:0] OpRt_Bltzal = 5'b10000;
	parameter [4:0] OpRt_Teqi   = 5'b01100;
	parameter [4:0] OpRt_Tgei   = 5'b01000;
	parameter [4:0] OpRt_Tgeiu  = 5'b01001;
	parameter [4:0] OpRt_Tlti   = 5'b01010;
	parameter [4:0] OpRt_Tltiu  = 5'b01011;
	parameter [4:0] OpRt_Tnei   = 5'b01110;

	/* Op Code Rs fields for Coprocessors */
	parameter [4:0] OpRs_MF     = 5'b00000;
	parameter [4:0] OpRs_MT     = 5'b00100;

	/* Special handling for ERET */
	parameter [4:0] OpRs_ERET   = 5'b10000;
	parameter [5:0] Funct_ERET  = 6'b011000;

	/* Function Codes for R-Type Op Codes */
	parameter [5:0] Funct_Add     = 6'b10_0000;
	parameter [5:0] Funct_Addu    = 6'b10_0001;
	parameter [5:0] Funct_And     = 6'b10_0100;
	parameter [5:0] Funct_Break   = 6'b00_1101;
	parameter [5:0] Funct_Clo     = 6'b10_0001; // same as Addu
	parameter [5:0] Funct_Clz     = 6'b10_0000; // same as Add
	parameter [5:0] Funct_Div     = 6'b01_1010;
	parameter [5:0] Funct_Divu    = 6'b01_1011;
	parameter [5:0] Funct_Jr      = 6'b00_1000;
	parameter [5:0] Funct_Jalr    = 6'b00_1001;
	parameter [5:0] Funct_Madd    = 6'b00_0000;
	parameter [5:0] Funct_Maddu   = 6'b00_0001;
	parameter [5:0] Funct_Mfhi    = 6'b01_0000;
	parameter [5:0] Funct_Mflo    = 6'b01_0010;
	parameter [5:0] Funct_Movn    = 6'b00_1011;
	parameter [5:0] Funct_Movz    = 6'b00_1010;
	parameter [5:0] Funct_Msub    = 6'b00_0100; // same as Sllv
	parameter [5:0] Funct_Msubu   = 6'b00_0101;
	parameter [5:0] Funct_Mthi    = 6'b01_0001;
	parameter [5:0] Funct_Mtlo    = 6'b01_0011;
	parameter [5:0] Funct_Mul     = 6'b00_0010; // same as Srl
	parameter [5:0] Funct_Mult    = 6'b01_1000;
	parameter [5:0] Funct_Multu   = 6'b01_1001;
	parameter [5:0] Funct_Nor     = 6'b10_0111;
	parameter [5:0] Funct_Or      = 6'b10_0101;
	parameter [5:0] Funct_Sll     = 6'b00_0000;
	parameter [5:0] Funct_Sllv    = 6'b00_0100;
	parameter [5:0] Funct_Slt     = 6'b10_1010;
	parameter [5:0] Funct_Sltu    = 6'b10_1011;
	parameter [5:0] Funct_Sra     = 6'b00_0011;
	parameter [5:0] Funct_Srav    = 6'b00_0111;
	parameter [5:0] Funct_Srl     = 6'b00_0010;
	parameter [5:0] Funct_Srlv    = 6'b00_0110;
	parameter [5:0] Funct_Sub     = 6'b10_0010;
	parameter [5:0] Funct_Subu    = 6'b10_0011;
	parameter [5:0] Funct_Syscall = 6'b00_1100;
	parameter [5:0] Funct_Teq     = 6'b11_0100;
	parameter [5:0] Funct_Tge     = 6'b11_0000;
	parameter [5:0] Funct_Tgeu    = 6'b11_0001;
	parameter [5:0] Funct_Tlt     = 6'b11_0010;
	parameter [5:0] Funct_Tltu    = 6'b11_0011;
	parameter [5:0] Funct_Tne     = 6'b11_0110;
	parameter [5:0] Funct_Xor     = 6'b10_0110;

	/* ALU Operations (Implementation) */
	parameter [4:0] AluOp_Add    = 5'd1;
	parameter [4:0] AluOp_Addu   = 5'd0;
	parameter [4:0] AluOp_And    = 5'd2;
	parameter [4:0] AluOp_Clo    = 5'd3;
	parameter [4:0] AluOp_Clz    = 5'd4;
	parameter [4:0] AluOp_Div    = 5'd5;
	parameter [4:0] AluOp_Divu   = 5'd6;
	parameter [4:0] AluOp_Madd   = 5'd7;
	parameter [4:0] AluOp_Maddu  = 5'd8;
	parameter [4:0] AluOp_Mfhi   = 5'd9;
	parameter [4:0] AluOp_Mflo   = 5'd10;
	parameter [4:0] AluOp_Msub   = 5'd13;
	parameter [4:0] AluOp_Msubu  = 5'd14;
	parameter [4:0] AluOp_Mthi   = 5'd11;
	parameter [4:0] AluOp_Mtlo   = 5'd12;
	parameter [4:0] AluOp_Mul    = 5'd15;
	parameter [4:0] AluOp_Mult   = 5'd16;
	parameter [4:0] AluOp_Multu  = 5'd17;
	parameter [4:0] AluOp_Nor    = 5'd18;
	parameter [4:0] AluOp_Or     = 5'd19;
	parameter [4:0] AluOp_Sll    = 5'd20;
	parameter [4:0] AluOp_Sllc   = 5'd21;  // Move this if another AluOp is needed
	parameter [4:0] AluOp_Sllv   = 5'd22;
	parameter [4:0] AluOp_Slt    = 5'd23;
	parameter [4:0] AluOp_Sltu   = 5'd24;
	parameter [4:0] AluOp_Sra    = 5'd25;
	parameter [4:0] AluOp_Srav   = 5'd26;
	parameter [4:0] AluOp_Srl    = 5'd27;
	parameter [4:0] AluOp_Srlv   = 5'd28;
	parameter [4:0] AluOp_Sub    = 5'd29;
	parameter [4:0] AluOp_Subu   = 5'd30;
	parameter [4:0] AluOp_Xor    = 5'd31;
	parameter [15:0] DP_None        = 16'b000_00000_000000_00;    // Instructions which require nothing of the main datapath.
	parameter [15:0] DP_RType       = 16'b000_00001_000000_10;    // Standard R-Type
	parameter [15:0] DP_IType       = 16'b000_10000_000000_10;    // Standard I-Type
	parameter [15:0] DP_Branch      = 16'b100_00000_000000_00;    // Standard Branch
	parameter [15:0] DP_BranchLink  = 16'b101_00000_000000_10;    // Branch and Link
	parameter [15:0] DP_HiLoWr      = 16'b000_00000_000000_00;    // Write to Hi/Lo ALU register (Div,Divu,Mult,Multu,Mthi,Mtlo). Currently 'DP_None'.
	parameter [15:0] DP_Jump        = 16'b010_00000_000000_00;    // Standard Jump
	parameter [15:0] DP_JumpLink    = 16'b011_00000_000000_10;    // Jump and Link
	parameter [15:0] DP_JumpLinkReg = 16'b111_00000_000000_10;    // Jump and Link Register
	parameter [15:0] DP_JumpReg     = 16'b110_00000_000000_00;    // Jump Register
	parameter [15:0] DP_LoadByteS   = 16'b000_10000_010011_11;    // Load Byte Signed
	parameter [15:0] DP_LoadByteU   = 16'b000_10000_010010_11;    // Load Byte Unsigned
	parameter [15:0] DP_LoadHalfS   = 16'b000_10000_010101_11;    // Load Half Signed
	parameter [15:0] DP_LoadHalfU   = 16'b000_10000_010100_11;    // Load Half Unsigned
	parameter [15:0] DP_LoadWord    = 16'b000_10000_010000_11;    // Load Word
	parameter [15:0] DP_ExtWrRt     = 16'b000_00000_000000_10;    // A DP-external write to Rt
	parameter [15:0] DP_ExtWrRd     = 16'b000_00001_000000_10;    // A DP-external write to Rd
	parameter [15:0] DP_Movc        = 16'b000_01001_000000_10;    // Conditional Move
	parameter [15:0] DP_LoadLinked  = 16'b000_10000_110000_11;    // Load Linked
	parameter [15:0] DP_StoreCond   = 16'b000_10000_101000_11;    // Store Conditional
	parameter [15:0] DP_StoreByte   = 16'b000_10000_001010_00;    // Store Byte
	parameter [15:0] DP_StoreHalf   = 16'b000_10000_001100_00;    // Store Half
	parameter [15:0] DP_StoreWord   = 16'b000_10000_001000_00;    // Store Word
	parameter [15:0] DP_TrapRegCNZ  = 16'b000_00110_000000_00;    // Trap using Rs and Rt,  non-zero ALU (Tlt,  Tltu,  Tne)
	parameter [15:0] DP_TrapRegCZ   = 16'b000_00100_000000_00;    // Trap using RS and Rt,  zero ALU     (Teq,  Tge,   Tgeu)
	parameter [15:0] DP_TrapImmCNZ  = 16'b000_10110_000000_00;    // Trap using Rs and Imm, non-zero ALU (Tlti, Tltiu, Tnei)
	parameter [15:0] DP_TrapImmCZ   = 16'b000_10100_000000_00;    // Trap using Rs and Imm, zero ALU     (Teqi, Tgei,  Tgeiu)
	//--------------------------------------------------------
	parameter [15:0] DP_Add     = DP_RType;
	parameter [15:0] DP_Addi    = DP_IType;
	parameter [15:0] DP_Addiu   = DP_IType;
	parameter [15:0] DP_Addu    = DP_RType;
	parameter [15:0] DP_And     = DP_RType;
	parameter [15:0] DP_Andi    = DP_IType;
	parameter [15:0] DP_Beq     = DP_Branch;
	parameter [15:0] DP_Bgez    = DP_Branch;
	parameter [15:0] DP_Bgezal  = DP_BranchLink;
	parameter [15:0] DP_Bgtz    = DP_Branch;
	parameter [15:0] DP_Blez    = DP_Branch;
	parameter [15:0] DP_Bltz    = DP_Branch;
	parameter [15:0] DP_Bltzal  = DP_BranchLink;
	parameter [15:0] DP_Bne     = DP_Branch;
	parameter [15:0] DP_Break   = DP_None;
	parameter [15:0] DP_Clo     = DP_RType;
	parameter [15:0] DP_Clz     = DP_RType;
	parameter [15:0] DP_Div     = DP_HiLoWr;
	parameter [15:0] DP_Divu    = DP_HiLoWr;
	parameter [15:0] DP_Eret    = DP_None;
	parameter [15:0] DP_J       = DP_Jump;
	parameter [15:0] DP_Jal     = DP_JumpLink;
	parameter [15:0] DP_Jalr    = DP_JumpLinkReg;
	parameter [15:0] DP_Jr      = DP_JumpReg;
	parameter [15:0] DP_Lb      = DP_LoadByteS;
	parameter [15:0] DP_Lbu     = DP_LoadByteU;
	parameter [15:0] DP_Lh      = DP_LoadHalfS;
	parameter [15:0] DP_Lhu     = DP_LoadHalfU;
	parameter [15:0] DP_Ll      = DP_LoadLinked;
	parameter [15:0] DP_Lui     = DP_IType;
	parameter [15:0] DP_Lw      = DP_LoadWord;
	parameter [15:0] DP_Lwl     = DP_LoadWord;
	parameter [15:0] DP_Lwr     = DP_LoadWord;
	parameter [15:0] DP_Madd    = DP_HiLoWr;
	parameter [15:0] DP_Maddu   = DP_HiLoWr;
	parameter [15:0] DP_Mfc0    = DP_ExtWrRt;
	parameter [15:0] DP_Mfhi    = DP_ExtWrRd;
	parameter [15:0] DP_Mflo    = DP_ExtWrRd;
	parameter [15:0] DP_Movn    = DP_Movc;
	parameter [15:0] DP_Movz    = DP_Movc;
	parameter [15:0] DP_Msub    = DP_HiLoWr;
	parameter [15:0] DP_Msubu   = DP_HiLoWr;
	parameter [15:0] DP_Mtc0    = DP_None;
	parameter [15:0] DP_Mthi    = DP_HiLoWr;
	parameter [15:0] DP_Mtlo    = DP_HiLoWr;
	parameter [15:0] DP_Mul     = DP_RType;
	parameter [15:0] DP_Mult    = DP_HiLoWr;
	parameter [15:0] DP_Multu   = DP_HiLoWr;
	parameter [15:0] DP_Nor     = DP_RType;
	parameter [15:0] DP_Or      = DP_RType;
	parameter [15:0] DP_Ori     = DP_IType;
	parameter [15:0] DP_Pref    = DP_None; // Not Implemented
	parameter [15:0] DP_Sb      = DP_StoreByte;
	parameter [15:0] DP_Sc      = DP_StoreCond;
	parameter [15:0] DP_Sh      = DP_StoreHalf;
	parameter [15:0] DP_Sll     = DP_RType;
	parameter [15:0] DP_Sllv    = DP_RType;
	parameter [15:0] DP_Slt     = DP_RType;
	parameter [15:0] DP_Slti    = DP_IType;
	parameter [15:0] DP_Sltiu   = DP_IType;
	parameter [15:0] DP_Sltu    = DP_RType;
	parameter [15:0] DP_Sra     = DP_RType;
	parameter [15:0] DP_Srav    = DP_RType;
	parameter [15:0] DP_Srl     = DP_RType;
	parameter [15:0] DP_Srlv    = DP_RType;
	parameter [15:0] DP_Sub     = DP_RType;
	parameter [15:0] DP_Subu    = DP_RType;
	parameter [15:0] DP_Sw      = DP_StoreWord;
	parameter [15:0] DP_Swl     = DP_StoreWord;
	parameter [15:0] DP_Swr     = DP_StoreWord;
	parameter [15:0] DP_Syscall = DP_None;
	parameter [15:0] DP_Teq     = DP_TrapRegCZ;
	parameter [15:0] DP_Teqi    = DP_TrapImmCZ;
	parameter [15:0] DP_Tge     = DP_TrapRegCZ;
	parameter [15:0] DP_Tgei    = DP_TrapImmCZ;
	parameter [15:0] DP_Tgeiu   = DP_TrapImmCZ;
	parameter [15:0] DP_Tgeu    = DP_TrapRegCZ;
	parameter [15:0] DP_Tlt     = DP_TrapRegCNZ;
	parameter [15:0] DP_Tlti    = DP_TrapImmCNZ;
	parameter [15:0] DP_Tltiu   = DP_TrapImmCNZ;
	parameter [15:0] DP_Tltu    = DP_TrapRegCNZ;
	parameter [15:0] DP_Tne     = DP_TrapRegCNZ;
	parameter [15:0] DP_Tnei    = DP_TrapImmCNZ;
	parameter [15:0] DP_Xor     = DP_RType;
	parameter [15:0] DP_Xori    = DP_IType;

	parameter [2:0] EXC_None = 3'b000;
	parameter [2:0] EXC_ID   = 3'b100;
	parameter [2:0] EXC_EX   = 3'b010;
	parameter [2:0] EXC_MEM  = 3'b001;
	//--------------------------------
	parameter [2:0] EXC_Add     = EXC_EX;
	parameter [2:0] EXC_Addi    = EXC_EX;
	parameter [2:0] EXC_Addiu   = EXC_None;
	parameter [2:0] EXC_Addu    = EXC_None;
	parameter [2:0] EXC_And     = EXC_None;
	parameter [2:0] EXC_Andi    = EXC_None;
	parameter [2:0] EXC_Beq     = EXC_None;
	parameter [2:0] EXC_Bgez    = EXC_None;
	parameter [2:0] EXC_Bgezal  = EXC_None;
	parameter [2:0] EXC_Bgtz    = EXC_None;
	parameter [2:0] EXC_Blez    = EXC_None;
	parameter [2:0] EXC_Bltz    = EXC_None;
	parameter [2:0] EXC_Bltzal  = EXC_None;
	parameter [2:0] EXC_Bne     = EXC_None;
	parameter [2:0] EXC_Break   = EXC_ID;
	parameter [2:0] EXC_Clo     = EXC_None;
	parameter [2:0] EXC_Clz     = EXC_None;
	parameter [2:0] EXC_Div     = EXC_None;
	parameter [2:0] EXC_Divu    = EXC_None;
	parameter [2:0] EXC_Eret    = EXC_ID;
	parameter [2:0] EXC_J       = EXC_None;
	parameter [2:0] EXC_Jal     = EXC_None;
	parameter [2:0] EXC_Jalr    = EXC_None;
	parameter [2:0] EXC_Jr      = EXC_None;
	parameter [2:0] EXC_Lb      = EXC_MEM;
	parameter [2:0] EXC_Lbu     = EXC_MEM;
	parameter [2:0] EXC_Lh      = EXC_MEM;
	parameter [2:0] EXC_Lhu     = EXC_MEM;
	parameter [2:0] EXC_Ll      = EXC_MEM;
	parameter [2:0] EXC_Lui     = EXC_None;
	parameter [2:0] EXC_Lw      = EXC_MEM;
	parameter [2:0] EXC_Lwl     = EXC_MEM;
	parameter [2:0] EXC_Lwr     = EXC_MEM;
	parameter [2:0] EXC_Madd    = EXC_None;
	parameter [2:0] EXC_Maddu   = EXC_None;
	parameter [2:0] EXC_Mfc0    = EXC_ID;
	parameter [2:0] EXC_Mfhi    = EXC_None;
	parameter [2:0] EXC_Mflo    = EXC_None;
	parameter [2:0] EXC_Movn    = EXC_None;
	parameter [2:0] EXC_Movz    = EXC_None;
	parameter [2:0] EXC_Msub    = EXC_None;
	parameter [2:0] EXC_Msubu   = EXC_None;
	parameter [2:0] EXC_Mtc0    = EXC_ID;
	parameter [2:0] EXC_Mthi    = EXC_None;
	parameter [2:0] EXC_Mtlo    = EXC_None;
	parameter [2:0] EXC_Mul     = EXC_None;
	parameter [2:0] EXC_Mult    = EXC_None;
	parameter [2:0] EXC_Multu   = EXC_None;
	parameter [2:0] EXC_Nor     = EXC_None;
	parameter [2:0] EXC_Or      = EXC_None;
	parameter [2:0] EXC_Ori     = EXC_None;
	parameter [2:0] EXC_Pref    = EXC_None; // XXX
	parameter [2:0] EXC_Sb      = EXC_MEM;
	parameter [2:0] EXC_Sc      = EXC_MEM;
	parameter [2:0] EXC_Sh      = EXC_MEM;
	parameter [2:0] EXC_Sll     = EXC_None;
	parameter [2:0] EXC_Sllv    = EXC_None;
	parameter [2:0] EXC_Slt     = EXC_None;
	parameter [2:0] EXC_Slti    = EXC_None;
	parameter [2:0] EXC_Sltiu   = EXC_None;
	parameter [2:0] EXC_Sltu    = EXC_None;
	parameter [2:0] EXC_Sra     = EXC_None;
	parameter [2:0] EXC_Srav    = EXC_None;
	parameter [2:0] EXC_Srl     = EXC_None;
	parameter [2:0] EXC_Srlv    = EXC_None;
	parameter [2:0] EXC_Sub     = EXC_EX;
	parameter [2:0] EXC_Subu    = EXC_None;
	parameter [2:0] EXC_Sw      = EXC_MEM;
	parameter [2:0] EXC_Swl     = EXC_MEM;
	parameter [2:0] EXC_Swr     = EXC_MEM;
	parameter [2:0] EXC_Syscall = EXC_ID;
	parameter [2:0] EXC_Teq     = EXC_MEM;
	parameter [2:0] EXC_Teqi    = EXC_MEM;
	parameter [2:0] EXC_Tge     = EXC_MEM;
	parameter [2:0] EXC_Tgei    = EXC_MEM;
	parameter [2:0] EXC_Tgeiu   = EXC_MEM;
	parameter [2:0] EXC_Tgeu    = EXC_MEM;
	parameter [2:0] EXC_Tlt     = EXC_MEM;
	parameter [2:0] EXC_Tlti    = EXC_MEM;
	parameter [2:0] EXC_Tltiu   = EXC_MEM;
	parameter [2:0] EXC_Tltu    = EXC_MEM;
	parameter [2:0] EXC_Tne     = EXC_MEM;
	parameter [2:0] EXC_Tnei    = EXC_MEM;
	parameter [2:0] EXC_Xor     = EXC_None;
	parameter [2:0] EXC_Xori    = EXC_None;

	parameter [7:0] HAZ_Nothing  = 8'b00000000; // Jumps, Lui, Mfhi/lo, special, etc.
	parameter [7:0] HAZ_IDRsIDRt = 8'b11110000; // Beq, Bne, Traps
	parameter [7:0] HAZ_IDRs     = 8'b11000000; // Most branches, Jumps to registers
	parameter [7:0] HAZ_IDRt     = 8'b00110000; // Mtc0
	parameter [7:0] HAZ_IDRtEXRs = 8'b10111100; // Movn, Movz
	parameter [7:0] HAZ_EXRsEXRt = 8'b10101111; // Many R-Type ops
	parameter [7:0] HAZ_EXRs     = 8'b10001100; // Immediates: Loads, Clo/z, Mthi/lo, etc.
	parameter [7:0] HAZ_EXRsWRt  = 8'b10101110; // Stores
	parameter [7:0] HAZ_EXRt     = 8'b00100011; // Shifts using Shamt field
	//-----------------------------------------
	parameter [7:0] HAZ_Add     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Addi    = HAZ_EXRs;
	parameter [7:0] HAZ_Addiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Addu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_And     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Andi    = HAZ_EXRs;
	parameter [7:0] HAZ_Beq     = HAZ_IDRsIDRt;
	parameter [7:0] HAZ_Bgez    = HAZ_IDRs;
	parameter [7:0] HAZ_Bgezal  = HAZ_IDRs;
	parameter [7:0] HAZ_Bgtz    = HAZ_IDRs;
	parameter [7:0] HAZ_Blez    = HAZ_IDRs;
	parameter [7:0] HAZ_Bltz    = HAZ_IDRs;
	parameter [7:0] HAZ_Bltzal  = HAZ_IDRs;
	parameter [7:0] HAZ_Bne     = HAZ_IDRsIDRt;
	parameter [7:0] HAZ_Break   = HAZ_Nothing;
	parameter [7:0] HAZ_Clo     = HAZ_EXRs;
	parameter [7:0] HAZ_Clz     = HAZ_EXRs;
	parameter [7:0] HAZ_Div     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Divu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Eret    = HAZ_Nothing;
	parameter [7:0] HAZ_J       = HAZ_Nothing;
	parameter [7:0] HAZ_Jal     = HAZ_Nothing;
	parameter [7:0] HAZ_Jalr    = HAZ_IDRs;
	parameter [7:0] HAZ_Jr      = HAZ_IDRs;
	parameter [7:0] HAZ_Lb      = HAZ_EXRs;
	parameter [7:0] HAZ_Lbu     = HAZ_EXRs;
	parameter [7:0] HAZ_Lh      = HAZ_EXRs;
	parameter [7:0] HAZ_Lhu     = HAZ_EXRs;
	parameter [7:0] HAZ_Ll      = HAZ_EXRs;
	parameter [7:0] HAZ_Lui     = HAZ_Nothing;
	parameter [7:0] HAZ_Lw      = HAZ_EXRs;
	parameter [7:0] HAZ_Lwl     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Lwr     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Madd    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Maddu   = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Mfc0    = HAZ_Nothing;
	parameter [7:0] HAZ_Mfhi    = HAZ_Nothing;
	parameter [7:0] HAZ_Mflo    = HAZ_Nothing;
	parameter [7:0] HAZ_Movn    = HAZ_IDRtEXRs;
	parameter [7:0] HAZ_Movz    = HAZ_IDRtEXRs;
	parameter [7:0] HAZ_Msub    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Msubu   = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Mtc0    = HAZ_IDRt;
	parameter [7:0] HAZ_Mthi    = HAZ_EXRs;
	parameter [7:0] HAZ_Mtlo    = HAZ_EXRs;
	parameter [7:0] HAZ_Mul     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Mult    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Multu   = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Nor     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Or      = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Ori     = HAZ_EXRs;
	parameter [7:0] HAZ_Pref    = HAZ_Nothing; // XXX
	parameter [7:0] HAZ_Sb      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Sc      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Sh      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Sll     = HAZ_EXRt;
	parameter [7:0] HAZ_Sllv    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Slt     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Slti    = HAZ_EXRs;
	parameter [7:0] HAZ_Sltiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Sltu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Sra     = HAZ_EXRt;
	parameter [7:0] HAZ_Srav    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Srl     = HAZ_EXRt;
	parameter [7:0] HAZ_Srlv    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Sub     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Subu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Sw      = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Swl     = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Swr     = HAZ_EXRsWRt;
	parameter [7:0] HAZ_Syscall = HAZ_Nothing;
	parameter [7:0] HAZ_Teq     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Teqi    = HAZ_EXRs;
	parameter [7:0] HAZ_Tge     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tgei    = HAZ_EXRs;
	parameter [7:0] HAZ_Tgeiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Tgeu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tlt     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tlti    = HAZ_EXRs;
	parameter [7:0] HAZ_Tltiu   = HAZ_EXRs;
	parameter [7:0] HAZ_Tltu    = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tne     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Tnei    = HAZ_EXRs;
	parameter [7:0] HAZ_Xor     = HAZ_EXRsEXRt;
	parameter [7:0] HAZ_Xori    = HAZ_EXRs;


    wire Movc;
    wire Branch, Branch_EQ, Branch_GTZ, Branch_LEZ, Branch_NEQ, Branch_GEZ, Branch_LTZ;
    wire Unaligned_Mem;
    
    reg [15:0] Datapath;
    assign PCSrc[0]      = Datapath[14];
    assign Link          = Datapath[13];
    assign ALUSrcImm     = Datapath[12];
    assign Movc          = Datapath[11];
    assign Trap          = Datapath[10];
    assign TrapCond      = Datapath[9];
    assign RegDst        = Datapath[8];
    assign LLSC          = Datapath[7];
    assign MemRead       = Datapath[6];
    assign MemWrite      = Datapath[5];
    assign MemHalf       = Datapath[4];
    assign MemByte       = Datapath[3];
    assign MemSignExtend = Datapath[2];
    assign RegWrite      = Datapath[1];
    assign MemtoReg      = Datapath[0];
       
    reg [2:0] DP_Exceptions;
    assign ID_CanErr = DP_Exceptions[2];
    assign EX_CanErr = DP_Exceptions[1];
    assign  M_CanErr = DP_Exceptions[0];
    
    // Set the main datapath control signals based on the Op Code
    always @(*) begin
        if (ID_Stall)
            Datapath = DP_None;
        else begin
            case (OpCode)
                // R-Type
                Op_Type_R  :
                    begin
                        case (Funct)
                            Funct_Add     : Datapath = DP_Add;
                            Funct_Addu    : Datapath = DP_Addu;
                            Funct_And     : Datapath = DP_And;
                            Funct_Break   : Datapath = DP_Break;
                            Funct_Div     : Datapath = DP_Div;
                            Funct_Divu    : Datapath = DP_Divu;
                            Funct_Jalr    : Datapath = DP_Jalr;
                            Funct_Jr      : Datapath = DP_Jr;
                            Funct_Mfhi    : Datapath = DP_Mfhi;
                            Funct_Mflo    : Datapath = DP_Mflo;
                            Funct_Movn    : Datapath = DP_Movn;
                            Funct_Movz    : Datapath = DP_Movz;
                            Funct_Mthi    : Datapath = DP_Mthi;
                            Funct_Mtlo    : Datapath = DP_Mtlo;
                            Funct_Mult    : Datapath = DP_Mult;
                            Funct_Multu   : Datapath = DP_Multu;
                            Funct_Nor     : Datapath = DP_Nor;
                            Funct_Or      : Datapath = DP_Or;
                            Funct_Sll     : Datapath = DP_Sll;
                            Funct_Sllv    : Datapath = DP_Sllv;
                            Funct_Slt     : Datapath = DP_Slt;
                            Funct_Sltu    : Datapath = DP_Sltu;
                            Funct_Sra     : Datapath = DP_Sra;
                            Funct_Srav    : Datapath = DP_Srav;
                            Funct_Srl     : Datapath = DP_Srl;
                            Funct_Srlv    : Datapath = DP_Srlv;
                            Funct_Sub     : Datapath = DP_Sub;
                            Funct_Subu    : Datapath = DP_Subu;
                            Funct_Syscall : Datapath = DP_Syscall;
                            Funct_Teq     : Datapath = DP_Teq;
                            Funct_Tge     : Datapath = DP_Tge;
                            Funct_Tgeu    : Datapath = DP_Tgeu;
                            Funct_Tlt     : Datapath = DP_Tlt;
                            Funct_Tltu    : Datapath = DP_Tltu;
                            Funct_Tne     : Datapath = DP_Tne;
                            Funct_Xor     : Datapath = DP_Xor;
                            default       : Datapath = DP_None;
                        endcase
                    end
                // R2-Type
                Op_Type_R2 :
                    begin
                        case (Funct)
                            Funct_Clo   : Datapath = DP_Clo;
                            Funct_Clz   : Datapath = DP_Clz;
                            Funct_Madd  : Datapath = DP_Madd;
                            Funct_Maddu : Datapath = DP_Maddu;
                            Funct_Msub  : Datapath = DP_Msub;
                            Funct_Msubu : Datapath = DP_Msubu;
                            Funct_Mul   : Datapath = DP_Mul;
                            default     : Datapath = DP_None;
                        endcase
                    end
                // I-Type
                Op_Addi    : Datapath = DP_Addi;
                Op_Addiu   : Datapath = DP_Addiu;
                Op_Andi    : Datapath = DP_Andi;
                Op_Ori     : Datapath = DP_Ori;
                Op_Pref    : Datapath = DP_Pref;
                Op_Slti    : Datapath = DP_Slti;
                Op_Sltiu   : Datapath = DP_Sltiu;
                Op_Xori    : Datapath = DP_Xori;
                // Jumps (using immediates)
                Op_J       : Datapath = DP_J;
                Op_Jal     : Datapath = DP_Jal;
                // Branches and Traps
                Op_Type_BI :
                    begin
                        case (Rt)
                            OpRt_Bgez   : Datapath = DP_Bgez;
                            OpRt_Bgezal : Datapath = DP_Bgezal;
                            OpRt_Bltz   : Datapath = DP_Bltz;
                            OpRt_Bltzal : Datapath = DP_Bltzal;
                            OpRt_Teqi   : Datapath = DP_Teqi;
                            OpRt_Tgei   : Datapath = DP_Tgei;
                            OpRt_Tgeiu  : Datapath = DP_Tgeiu;
                            OpRt_Tlti   : Datapath = DP_Tlti;
                            OpRt_Tltiu  : Datapath = DP_Tltiu;
                            OpRt_Tnei   : Datapath = DP_Tnei;
                            default     : Datapath = DP_None;
                        endcase
                    end                         
                Op_Beq     : Datapath = DP_Beq;
                Op_Bgtz    : Datapath = DP_Bgtz;
                Op_Blez    : Datapath = DP_Blez;
                Op_Bne     : Datapath = DP_Bne;
                // Coprocessor 0
                Op_Type_CP0 :
                    begin
                        case (Rs)
                            OpRs_MF   : Datapath = DP_Mfc0;
                            OpRs_MT   : Datapath = DP_Mtc0;
                            OpRs_ERET : Datapath = (Funct == Funct_ERET) ? DP_Eret : DP_None;
                            default   : Datapath = DP_None;
                        endcase
                    end
                // Memory
                Op_Lb   : Datapath = DP_Lb;
                Op_Lbu  : Datapath = DP_Lbu;
                Op_Lh   : Datapath = DP_Lh;
                Op_Lhu  : Datapath = DP_Lhu;
                Op_Ll   : Datapath = DP_Ll;
                Op_Lui  : Datapath = DP_Lui;
                Op_Lw   : Datapath = DP_Lw;
                Op_Lwl  : Datapath = DP_Lwl;
                Op_Lwr  : Datapath = DP_Lwr;
                Op_Sb   : Datapath = DP_Sb;
                Op_Sc   : Datapath = DP_Sc;
                Op_Sh   : Datapath = DP_Sh;
                Op_Sw   : Datapath = DP_Sw;
                Op_Swl  : Datapath = DP_Swl;
                Op_Swr  : Datapath = DP_Swr;
                default : Datapath = DP_None;
            endcase
        end
    end

    // Set the Hazard Control Signals and Exception Indicators based on the Op Code
    always @(*) begin
        case (OpCode)
            // R-Type
            Op_Type_R  :
                begin
                    case (Funct)
                        Funct_Add     : begin DP_Hazards = HAZ_Add;     DP_Exceptions = EXC_Add;     end
                        Funct_Addu    : begin DP_Hazards = HAZ_Addu;    DP_Exceptions = EXC_Addu;    end
                        Funct_And     : begin DP_Hazards = HAZ_And;     DP_Exceptions = EXC_And;     end
                        Funct_Break   : begin DP_Hazards = HAZ_Break;   DP_Exceptions = EXC_Break;   end
                        Funct_Div     : begin DP_Hazards = HAZ_Div;     DP_Exceptions = EXC_Div;     end
                        Funct_Divu    : begin DP_Hazards = HAZ_Divu;    DP_Exceptions = EXC_Divu;    end
                        Funct_Jalr    : begin DP_Hazards = HAZ_Jalr;    DP_Exceptions = EXC_Jalr;    end
                        Funct_Jr      : begin DP_Hazards = HAZ_Jr;      DP_Exceptions = EXC_Jr;      end
                        Funct_Mfhi    : begin DP_Hazards = HAZ_Mfhi;    DP_Exceptions = EXC_Mfhi;    end
                        Funct_Mflo    : begin DP_Hazards = HAZ_Mflo;    DP_Exceptions = EXC_Mflo;    end
                        Funct_Movn    : begin DP_Hazards = HAZ_Movn;    DP_Exceptions = EXC_Movn;    end
                        Funct_Movz    : begin DP_Hazards = HAZ_Movz;    DP_Exceptions = EXC_Movz;    end
                        Funct_Mthi    : begin DP_Hazards = HAZ_Mthi;    DP_Exceptions = EXC_Mthi;    end
                        Funct_Mtlo    : begin DP_Hazards = HAZ_Mtlo;    DP_Exceptions = EXC_Mtlo;    end
                        Funct_Mult    : begin DP_Hazards = HAZ_Mult;    DP_Exceptions = EXC_Mult;    end
                        Funct_Multu   : begin DP_Hazards = HAZ_Multu;   DP_Exceptions = EXC_Multu;   end
                        Funct_Nor     : begin DP_Hazards = HAZ_Nor;     DP_Exceptions = EXC_Nor;     end
                        Funct_Or      : begin DP_Hazards = HAZ_Or;      DP_Exceptions = EXC_Or;      end
                        Funct_Sll     : begin DP_Hazards = HAZ_Sll;     DP_Exceptions = EXC_Sll;     end
                        Funct_Sllv    : begin DP_Hazards = HAZ_Sllv;    DP_Exceptions = EXC_Sllv;    end
                        Funct_Slt     : begin DP_Hazards = HAZ_Slt;     DP_Exceptions = EXC_Slt;     end
                        Funct_Sltu    : begin DP_Hazards = HAZ_Sltu;    DP_Exceptions = EXC_Sltu;    end
                        Funct_Sra     : begin DP_Hazards = HAZ_Sra;     DP_Exceptions = EXC_Sra;     end
                        Funct_Srav    : begin DP_Hazards = HAZ_Srav;    DP_Exceptions = EXC_Srav;    end
                        Funct_Srl     : begin DP_Hazards = HAZ_Srl;     DP_Exceptions = EXC_Srl;     end
                        Funct_Srlv    : begin DP_Hazards = HAZ_Srlv;    DP_Exceptions = EXC_Srlv;    end
                        Funct_Sub     : begin DP_Hazards = HAZ_Sub;     DP_Exceptions = EXC_Sub;     end
                        Funct_Subu    : begin DP_Hazards = HAZ_Subu;    DP_Exceptions = EXC_Subu;    end
                        Funct_Syscall : begin DP_Hazards = HAZ_Syscall; DP_Exceptions = EXC_Syscall; end
                        Funct_Teq     : begin DP_Hazards = HAZ_Teq;     DP_Exceptions = EXC_Teq;     end
                        Funct_Tge     : begin DP_Hazards = HAZ_Tge;     DP_Exceptions = EXC_Tge;     end
                        Funct_Tgeu    : begin DP_Hazards = HAZ_Tgeu;    DP_Exceptions = EXC_Tgeu;    end
                        Funct_Tlt     : begin DP_Hazards = HAZ_Tlt;     DP_Exceptions = EXC_Tlt;     end
                        Funct_Tltu    : begin DP_Hazards = HAZ_Tltu;    DP_Exceptions = EXC_Tltu;    end
                        Funct_Tne     : begin DP_Hazards = HAZ_Tne;     DP_Exceptions = EXC_Tne;     end
                        Funct_Xor     : begin DP_Hazards = HAZ_Xor;     DP_Exceptions = EXC_Xor;     end
                        default       : begin DP_Hazards = 8'hxx;       DP_Exceptions = 3'bxxx;      end
                    endcase
                end
            // R2-Type
            Op_Type_R2 :
                begin
                    case (Funct)
                        Funct_Clo   : begin DP_Hazards = HAZ_Clo;   DP_Exceptions = EXC_Clo;   end
                        Funct_Clz   : begin DP_Hazards = HAZ_Clz;   DP_Exceptions = EXC_Clz;   end
                        Funct_Madd  : begin DP_Hazards = HAZ_Madd;  DP_Exceptions = EXC_Madd;  end
                        Funct_Maddu : begin DP_Hazards = HAZ_Maddu; DP_Exceptions = EXC_Maddu; end
                        Funct_Msub  : begin DP_Hazards = HAZ_Msub;  DP_Exceptions = EXC_Msub;  end
                        Funct_Msubu : begin DP_Hazards = HAZ_Msubu; DP_Exceptions = EXC_Msubu; end
                        Funct_Mul   : begin DP_Hazards = HAZ_Mul;   DP_Exceptions = EXC_Mul;   end
                        default     : begin DP_Hazards = 8'hxx;     DP_Exceptions = 3'bxxx;    end
                    endcase
                end
            // I-Type
            Op_Addi    : begin DP_Hazards = HAZ_Addi;  DP_Exceptions = EXC_Addi;  end
            Op_Addiu   : begin DP_Hazards = HAZ_Addiu; DP_Exceptions = EXC_Addiu; end
            Op_Andi    : begin DP_Hazards = HAZ_Andi;  DP_Exceptions = EXC_Andi;  end
            Op_Ori     : begin DP_Hazards = HAZ_Ori;   DP_Exceptions = EXC_Ori;   end
            Op_Pref    : begin DP_Hazards = HAZ_Pref;  DP_Exceptions = EXC_Pref;  end
            Op_Slti    : begin DP_Hazards = HAZ_Slti;  DP_Exceptions = EXC_Slti;  end
            Op_Sltiu   : begin DP_Hazards = HAZ_Sltiu; DP_Exceptions = EXC_Sltiu; end
            Op_Xori    : begin DP_Hazards = HAZ_Xori;  DP_Exceptions = EXC_Xori;  end
            // Jumps
            Op_J       : begin DP_Hazards = HAZ_J;     DP_Exceptions = EXC_J;     end
            Op_Jal     : begin DP_Hazards = HAZ_Jal;   DP_Exceptions = EXC_Jal;   end
            // Branches and Traps
            Op_Type_BI :
                begin
                    case (Rt)
                        OpRt_Bgez   : begin DP_Hazards = HAZ_Bgez;   DP_Exceptions = EXC_Bgez;   end
                        OpRt_Bgezal : begin DP_Hazards = HAZ_Bgezal; DP_Exceptions = EXC_Bgezal; end
                        OpRt_Bltz   : begin DP_Hazards = HAZ_Bltz;   DP_Exceptions = EXC_Bltz;   end
                        OpRt_Bltzal : begin DP_Hazards = HAZ_Bltzal; DP_Exceptions = EXC_Bltzal; end
                        OpRt_Teqi   : begin DP_Hazards = HAZ_Teqi;   DP_Exceptions = EXC_Teqi;   end
                        OpRt_Tgei   : begin DP_Hazards = HAZ_Tgei;   DP_Exceptions = EXC_Tgei;   end
                        OpRt_Tgeiu  : begin DP_Hazards = HAZ_Tgeiu;  DP_Exceptions = EXC_Tgeiu;  end
                        OpRt_Tlti   : begin DP_Hazards = HAZ_Tlti;   DP_Exceptions = EXC_Tlti;   end
                        OpRt_Tltiu  : begin DP_Hazards = HAZ_Tltiu;  DP_Exceptions = EXC_Tltiu;  end
                        OpRt_Tnei   : begin DP_Hazards = HAZ_Tnei;   DP_Exceptions = EXC_Tnei;   end
                        default     : begin DP_Hazards = 8'hxx;      DP_Exceptions = 3'bxxx;     end
                    endcase
                end                         
            Op_Beq     : begin DP_Hazards = HAZ_Beq;  DP_Exceptions = EXC_Beq;  end
            Op_Bgtz    : begin DP_Hazards = HAZ_Bgtz; DP_Exceptions = EXC_Bgtz; end
            Op_Blez    : begin DP_Hazards = HAZ_Blez; DP_Exceptions = EXC_Blez; end
            Op_Bne     : begin DP_Hazards = HAZ_Bne;  DP_Exceptions = EXC_Bne;  end
            // Coprocessor 0
            Op_Type_CP0 :
                begin
                    case (Rs)
                        OpRs_MF   : begin DP_Hazards = HAZ_Mfc0; DP_Exceptions = EXC_Mfc0; end
                        OpRs_MT   : begin DP_Hazards = HAZ_Mtc0; DP_Exceptions = EXC_Mtc0; end
                        OpRs_ERET : begin DP_Hazards = (Funct == Funct_ERET) ? DP_Eret : 8'hxx; DP_Exceptions = EXC_Eret; end
                        default   : begin DP_Hazards = 8'hxx;    DP_Exceptions = 3'bxxx;   end
                    endcase
                end
            // Memory
            Op_Lb   : begin DP_Hazards = HAZ_Lb;  DP_Exceptions = EXC_Lb;  end
            Op_Lbu  : begin DP_Hazards = HAZ_Lbu; DP_Exceptions = EXC_Lbu; end
            Op_Lh   : begin DP_Hazards = HAZ_Lh;  DP_Exceptions = EXC_Lh;  end
            Op_Lhu  : begin DP_Hazards = HAZ_Lhu; DP_Exceptions = EXC_Lhu; end
            Op_Ll   : begin DP_Hazards = HAZ_Ll;  DP_Exceptions = EXC_Ll;  end
            Op_Lui  : begin DP_Hazards = HAZ_Lui; DP_Exceptions = EXC_Lui; end
            Op_Lw   : begin DP_Hazards = HAZ_Lw;  DP_Exceptions = EXC_Lw;  end
            Op_Lwl  : begin DP_Hazards = HAZ_Lwl; DP_Exceptions = EXC_Lwl; end
            Op_Lwr  : begin DP_Hazards = HAZ_Lwr; DP_Exceptions = EXC_Lwr; end
            Op_Sb   : begin DP_Hazards = HAZ_Sb;  DP_Exceptions = EXC_Sb;  end
            Op_Sc   : begin DP_Hazards = HAZ_Sc;  DP_Exceptions = EXC_Sc;  end
            Op_Sh   : begin DP_Hazards = HAZ_Sh;  DP_Exceptions = EXC_Sh;  end
            Op_Sw   : begin DP_Hazards = HAZ_Sw;  DP_Exceptions = EXC_Sw;  end
            Op_Swl  : begin DP_Hazards = HAZ_Swl; DP_Exceptions = EXC_Swl; end
            Op_Swr  : begin DP_Hazards = HAZ_Swr; DP_Exceptions = EXC_Swr; end
            default : begin DP_Hazards = 8'hxx;   DP_Exceptions = 3'bxxx;  end
        endcase
    end

    // ALU Assignment
    always @(*) begin
        if (ID_Stall)
            ALUOp = AluOp_Addu;  // Any Op that doesn't write HILO or cause exceptions
        else begin
            case (OpCode)
                Op_Type_R  :
                    begin
                        case (Funct)
                            Funct_Add     : ALUOp = AluOp_Add;
                            Funct_Addu    : ALUOp = AluOp_Addu;
                            Funct_And     : ALUOp = AluOp_And;
                            Funct_Div     : ALUOp = AluOp_Div;
                            Funct_Divu    : ALUOp = AluOp_Divu;
                            Funct_Jalr    : ALUOp = AluOp_Addu;
                            Funct_Mfhi    : ALUOp = AluOp_Mfhi;
                            Funct_Mflo    : ALUOp = AluOp_Mflo;
                            Funct_Movn    : ALUOp = AluOp_Addu;
                            Funct_Movz    : ALUOp = AluOp_Addu;
                            Funct_Mthi    : ALUOp = AluOp_Mthi;
                            Funct_Mtlo    : ALUOp = AluOp_Mtlo;
                            Funct_Mult    : ALUOp = AluOp_Mult;
                            Funct_Multu   : ALUOp = AluOp_Multu;
                            Funct_Nor     : ALUOp = AluOp_Nor;
                            Funct_Or      : ALUOp = AluOp_Or;
                            Funct_Sll     : ALUOp = AluOp_Sll;
                            Funct_Sllv    : ALUOp = AluOp_Sllv;
                            Funct_Slt     : ALUOp = AluOp_Slt;
                            Funct_Sltu    : ALUOp = AluOp_Sltu;
                            Funct_Sra     : ALUOp = AluOp_Sra;
                            Funct_Srav    : ALUOp = AluOp_Srav;
                            Funct_Srl     : ALUOp = AluOp_Srl;
                            Funct_Srlv    : ALUOp = AluOp_Srlv;
                            Funct_Sub     : ALUOp = AluOp_Sub;
                            Funct_Subu    : ALUOp = AluOp_Subu;
                            Funct_Syscall : ALUOp = AluOp_Addu;
                            Funct_Teq     : ALUOp = AluOp_Subu;
                            Funct_Tge     : ALUOp = AluOp_Slt;
                            Funct_Tgeu    : ALUOp = AluOp_Sltu;
                            Funct_Tlt     : ALUOp = AluOp_Slt;
                            Funct_Tltu    : ALUOp = AluOp_Sltu;
                            Funct_Tne     : ALUOp = AluOp_Subu;
                            Funct_Xor     : ALUOp = AluOp_Xor;
                            default       : ALUOp = AluOp_Addu;
                        endcase
                    end
                Op_Type_R2 :
                    begin
                        case (Funct)
                            Funct_Clo   : ALUOp = AluOp_Clo;
                            Funct_Clz   : ALUOp = AluOp_Clz;
                            Funct_Madd  : ALUOp = AluOp_Madd;
                            Funct_Maddu : ALUOp = AluOp_Maddu;
                            Funct_Msub  : ALUOp = AluOp_Msub;
                            Funct_Msubu : ALUOp = AluOp_Msubu;
                            Funct_Mul   : ALUOp = AluOp_Mul;
                            default     : ALUOp = AluOp_Addu;
                        endcase
                    end
                Op_Type_BI  :
                    begin
                        case (Rt)
                            OpRt_Teqi   : ALUOp = AluOp_Subu;
                            OpRt_Tgei   : ALUOp = AluOp_Slt;
                            OpRt_Tgeiu  : ALUOp = AluOp_Sltu;
                            OpRt_Tlti   : ALUOp = AluOp_Slt;
                            OpRt_Tltiu  : ALUOp = AluOp_Sltu;
                            OpRt_Tnei   : ALUOp = AluOp_Subu;
                            default     : ALUOp = AluOp_Addu;  // Branches don't matter.
                        endcase
                    end
                Op_Type_CP0 : ALUOp = AluOp_Addu;
                Op_Addi     : ALUOp = AluOp_Add;
                Op_Addiu    : ALUOp = AluOp_Addu;
                Op_Andi     : ALUOp = AluOp_And;
                Op_Jal      : ALUOp = AluOp_Addu;
                Op_Lb       : ALUOp = AluOp_Addu;
                Op_Lbu      : ALUOp = AluOp_Addu;
                Op_Lh       : ALUOp = AluOp_Addu;
                Op_Lhu      : ALUOp = AluOp_Addu;
                Op_Ll       : ALUOp = AluOp_Addu;
                Op_Lui      : ALUOp = AluOp_Sllc;
                Op_Lw       : ALUOp = AluOp_Addu;
                Op_Lwl      : ALUOp = AluOp_Addu;
                Op_Lwr      : ALUOp = AluOp_Addu;
                Op_Ori      : ALUOp = AluOp_Or;
                Op_Sb       : ALUOp = AluOp_Addu;
                Op_Sc       : ALUOp = AluOp_Addu;  // XXX Needs HW implement
                Op_Sh       : ALUOp = AluOp_Addu;
                Op_Slti     : ALUOp = AluOp_Slt;
                Op_Sltiu    : ALUOp = AluOp_Sltu;
                Op_Sw       : ALUOp = AluOp_Addu;
                Op_Swl      : ALUOp = AluOp_Addu;
                Op_Swr      : ALUOp = AluOp_Addu;
                Op_Xori     : ALUOp = AluOp_Xor;
                default     : ALUOp = AluOp_Addu;
            endcase
        end
    end

    /*** 
     These remaining options cover portions of the datapath that are not
     controlled directly by the datapath bits. Note that some refer to bits of
     the opcode or other fields, which breaks the otherwise fully-abstracted view
     of instruction encodings. Make sure when adding custom instructions that
     no false positives/negatives are generated here.
     ***/
     
    // Branch Detection: Options are mutually exclusive.
    assign Branch_EQ  =  OpCode[2] & ~OpCode[1] & ~OpCode[0] &  Cmp_EQ;
    assign Branch_GTZ =  OpCode[2] &  OpCode[1] &  OpCode[0] &  Cmp_GZ;
    assign Branch_LEZ =  OpCode[2] &  OpCode[1] & ~OpCode[0] &  Cmp_LEZ;
    assign Branch_NEQ =  OpCode[2] & ~OpCode[1] &  OpCode[0] & ~Cmp_EQ;
    assign Branch_GEZ = ~OpCode[2] &  Rt[0] & Cmp_GEZ;
    assign Branch_LTZ = ~OpCode[2] & ~Rt[0] & Cmp_LZ;
    
    assign Branch = Branch_EQ | Branch_GTZ | Branch_LEZ | Branch_NEQ | Branch_GEZ | Branch_LTZ;
    assign PCSrc[1] = (Datapath[15] & ~Datapath[14]) ? Branch : Datapath[15];
    
    /* In MIPS32, all Branch and Jump operations execute the Branch Delay Slot,
     * or next instruction, regardless if the branch is taken or not. The exception
     * is the "Branch Likely" instruction group. These are deprecated, however, and not
     * implemented here. "IF_Flush" is defined to allow for the cancelation of a
     * Branch Delay Slot should these be implemented later.
     */
    assign IF_Flush = 0;

    // Indicator that next instruction is a Branch Delay Slot.
    assign NextIsDelay = Datapath[15] | Datapath[14];
    
    // Sign- or Zero-Extension Control. The only ops that require zero-extension are
    // Andi, Ori, and Xori. The following also zero-extends 'lui', however it does not alter the effect of lui.
    assign SignExtend = (OpCode[5:2] != 4'b0011);

    // Move Conditional
    assign Movn = Movc &  Funct[0];
    assign Movz = Movc & ~Funct[0];
    
    // Coprocessor 0 (Mfc0, Mtc0) control signals.
    assign Mfc0 = ((OpCode == Op_Type_CP0) && (Rs == OpRs_MF));
    assign Mtc0 = ((OpCode == Op_Type_CP0) && (Rs == OpRs_MT));
    assign Eret = ((OpCode == Op_Type_CP0) && (Rs == OpRs_ERET) && (Funct == Funct_ERET));
    
    // Coprocessor 1,2,3 accesses (not implemented)
    assign CP1 = (OpCode == Op_Type_CP1);
    assign CP2 = (OpCode == Op_Type_CP2);
    assign CP3 = (OpCode == Op_Type_CP3);
    
    // Exceptions found in ID
    assign EXC_Sys = ((OpCode == Op_Type_R) && (Funct == Funct_Syscall));
    assign EXC_Bp  = ((OpCode == Op_Type_R) && (Funct == Funct_Break));
    
    // Unaligned Memory Accesses (lwl, lwr, swl, swr)
    assign Unaligned_Mem = OpCode[5] & ~OpCode[4] & OpCode[1] & ~OpCode[0];
    assign Left  = Unaligned_Mem & ~OpCode[2];
    assign Right = Unaligned_Mem &  OpCode[2];
    
    // TODO: Reserved Instruction Exception must still be implemented
    assign EXC_RI  = 0;
    
endmodule

